﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.AI.NavMeshPath::.ctor()
extern void NavMeshPath__ctor_mEA40BFC2492814FFC97A71C3AEC2154A9415C37F (void);
// 0x00000002 System.Void UnityEngine.AI.NavMeshPath::Finalize()
extern void NavMeshPath_Finalize_mB151BFBD5D7E65C343415B6B332A58504F12AF77 (void);
// 0x00000003 System.IntPtr UnityEngine.AI.NavMeshPath::InitializeNavMeshPath()
extern void NavMeshPath_InitializeNavMeshPath_m91B9A02C11B0C86F33F726A047B11D015230E9C2 (void);
// 0x00000004 System.Void UnityEngine.AI.NavMeshPath::DestroyNavMeshPath(System.IntPtr)
extern void NavMeshPath_DestroyNavMeshPath_mE7FD23F7D0456507277BDC8ED868A7C6888796EB (void);
// 0x00000005 System.Void UnityEngine.AI.NavMeshPath::ClearCornersInternal()
extern void NavMeshPath_ClearCornersInternal_m2310C5CB9B4EB2B3C4685476B2CF8440ED369606 (void);
// 0x00000006 System.Void UnityEngine.AI.NavMeshPath::ClearCorners()
extern void NavMeshPath_ClearCorners_m8633C3989850C01982EBD3D4BC70E85AF461CE5B (void);
// 0x00000007 UnityEngine.AI.NavMeshPathStatus UnityEngine.AI.NavMeshPath::get_status()
extern void NavMeshPath_get_status_m63B0AEDA3149C7053987C4D0A02B3FE8B41BD74B (void);
// 0x00000008 System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern void NavMeshAgent_SetDestination_mD5D960933827F1F14B29CF4A3B6F305C064EBF46 (void);
// 0x00000009 UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_destination()
extern void NavMeshAgent_get_destination_m1BE2C5EEF53F7EB6317449726C99E0D0955C677E (void);
// 0x0000000A System.Void UnityEngine.AI.NavMeshAgent::set_destination(UnityEngine.Vector3)
extern void NavMeshAgent_set_destination_m5F0A8E4C8ED93798D6B9CE496B10FCE5B7461B95 (void);
// 0x0000000B System.Single UnityEngine.AI.NavMeshAgent::get_stoppingDistance()
extern void NavMeshAgent_get_stoppingDistance_mA866A409C59878849D63BEC61517DE4F906BEEC4 (void);
// 0x0000000C System.Void UnityEngine.AI.NavMeshAgent::set_stoppingDistance(System.Single)
extern void NavMeshAgent_set_stoppingDistance_m288A6280B55AAFF8578286747E19AF409C7C177F (void);
// 0x0000000D UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_velocity()
extern void NavMeshAgent_get_velocity_m028219D0E4678D727F00C53AE3DCBCF29AF04DA7 (void);
// 0x0000000E UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_steeringTarget()
extern void NavMeshAgent_get_steeringTarget_mA2DB66413FC7628DCDB1DECF38269A70EC60351C (void);
// 0x0000000F UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_desiredVelocity()
extern void NavMeshAgent_get_desiredVelocity_m8CF3F6E3FA8EE86397DD02146AA6048949B74F52 (void);
// 0x00000010 System.Single UnityEngine.AI.NavMeshAgent::get_remainingDistance()
extern void NavMeshAgent_get_remainingDistance_m051C1B408E2740A95B5A5577C5EC7222311AA73A (void);
// 0x00000011 System.Void UnityEngine.AI.NavMeshAgent::set_baseOffset(System.Single)
extern void NavMeshAgent_set_baseOffset_m6BB6CF6115E987B0A6268D708148723119C9B122 (void);
// 0x00000012 System.Void UnityEngine.AI.NavMeshAgent::set_autoBraking(System.Boolean)
extern void NavMeshAgent_set_autoBraking_m3BBDC74E6ADC3EB8BE0381C97B1D8715C1BC5283 (void);
// 0x00000013 System.Boolean UnityEngine.AI.NavMeshAgent::get_hasPath()
extern void NavMeshAgent_get_hasPath_mE56295431F7774ADC8C39CFD87E13073DDDA178A (void);
// 0x00000014 System.Boolean UnityEngine.AI.NavMeshAgent::get_pathPending()
extern void NavMeshAgent_get_pathPending_mA806A4DC1E06CA32A4E7E71B6846B516EC8C2487 (void);
// 0x00000015 System.Void UnityEngine.AI.NavMeshAgent::set_isStopped(System.Boolean)
extern void NavMeshAgent_set_isStopped_mF374E697F39845233B84D8C4873DEABC3AA490DF (void);
// 0x00000016 System.Void UnityEngine.AI.NavMeshAgent::ResetPath()
extern void NavMeshAgent_ResetPath_mE29D3956C1BFABDB3D6B4B7DF2B376B4EEB24E7F (void);
// 0x00000017 System.Boolean UnityEngine.AI.NavMeshAgent::CalculatePath(UnityEngine.Vector3,UnityEngine.AI.NavMeshPath)
extern void NavMeshAgent_CalculatePath_mF692688572E61C3B9FE776AA3784E14A08C259C2 (void);
// 0x00000018 System.Boolean UnityEngine.AI.NavMeshAgent::CalculatePathInternal(UnityEngine.Vector3,UnityEngine.AI.NavMeshPath)
extern void NavMeshAgent_CalculatePathInternal_m022C56D89B194E8EAD260A2E2CEEA100024AE004 (void);
// 0x00000019 System.Int32 UnityEngine.AI.NavMeshAgent::get_areaMask()
extern void NavMeshAgent_get_areaMask_m7C52D09097A66CC43AA0F1237E59ECE4340F62F7 (void);
// 0x0000001A System.Single UnityEngine.AI.NavMeshAgent::get_speed()
extern void NavMeshAgent_get_speed_m3E4720882ED0C65A4E19AADD53BB6CFFB5BB1345 (void);
// 0x0000001B System.Void UnityEngine.AI.NavMeshAgent::set_speed(System.Single)
extern void NavMeshAgent_set_speed_m820E45289B3AE7DEE16F2F4BF163EAC361E64646 (void);
// 0x0000001C System.Void UnityEngine.AI.NavMeshAgent::set_angularSpeed(System.Single)
extern void NavMeshAgent_set_angularSpeed_mD2AE9EE187EF1C45F519576B5FC8581DC6B7D683 (void);
// 0x0000001D System.Void UnityEngine.AI.NavMeshAgent::set_acceleration(System.Single)
extern void NavMeshAgent_set_acceleration_mEAA92D7837B8D726891846DBBB6114DB13CD597E (void);
// 0x0000001E System.Void UnityEngine.AI.NavMeshAgent::set_updateRotation(System.Boolean)
extern void NavMeshAgent_set_updateRotation_mBF6EDBC9BBAF32490229D7DD6BC821A420C3399D (void);
// 0x0000001F System.Void UnityEngine.AI.NavMeshAgent::set_updateUpAxis(System.Boolean)
extern void NavMeshAgent_set_updateUpAxis_m46E9DEE866A379D46790708F95FE21435C320A70 (void);
// 0x00000020 System.Void UnityEngine.AI.NavMeshAgent::set_radius(System.Single)
extern void NavMeshAgent_set_radius_m34F08DD2BD01A5DDA17FE1B02EE03D361B44F1AC (void);
// 0x00000021 System.Void UnityEngine.AI.NavMeshAgent::set_obstacleAvoidanceType(UnityEngine.AI.ObstacleAvoidanceType)
extern void NavMeshAgent_set_obstacleAvoidanceType_m4EF45925BCFD06A3A0FA64412F91687D40A738EF (void);
// 0x00000022 System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_SetDestination_Injected_mC3EF405F5AAFF9F98C5D5AECAD641525CDF742EA (void);
// 0x00000023 System.Void UnityEngine.AI.NavMeshAgent::get_destination_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_destination_Injected_m864513574C2AD2BA558D85B213CC518C064F893A (void);
// 0x00000024 System.Void UnityEngine.AI.NavMeshAgent::set_destination_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_set_destination_Injected_m7195764B7610A893730EB50F1D9EB70BCDE65BD8 (void);
// 0x00000025 System.Void UnityEngine.AI.NavMeshAgent::get_velocity_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_velocity_Injected_m40A3E476CECB49AE84CA70761FB01FB5644B1735 (void);
// 0x00000026 System.Void UnityEngine.AI.NavMeshAgent::get_steeringTarget_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_steeringTarget_Injected_m00761894AB2F3B67A8775949DBF465506CD78DE1 (void);
// 0x00000027 System.Void UnityEngine.AI.NavMeshAgent::get_desiredVelocity_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_desiredVelocity_Injected_m8793EE07C8733261F15C392B983113CBD225DA41 (void);
// 0x00000028 System.Boolean UnityEngine.AI.NavMeshAgent::CalculatePathInternal_Injected(UnityEngine.Vector3&,UnityEngine.AI.NavMeshPath)
extern void NavMeshAgent_CalculatePathInternal_Injected_m07BFBF7B199DE22CB3AB819BACA56953D2C56C06 (void);
// 0x00000029 UnityEngine.Vector3 UnityEngine.AI.NavMeshHit::get_position()
extern void NavMeshHit_get_position_m09E8FF6DEF5BFA3F30B3C4BCA4642442FF1BCBF1 (void);
// 0x0000002A System.Void UnityEngine.AI.NavMesh::Internal_CallOnNavMeshPreUpdate()
extern void NavMesh_Internal_CallOnNavMeshPreUpdate_m80148CFDD0C6F1DDDE5B3DA67A8D9613043A4233 (void);
// 0x0000002B System.Boolean UnityEngine.AI.NavMesh::SamplePosition(UnityEngine.Vector3,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern void NavMesh_SamplePosition_m51497866E71DD5263425E8572F2232D496E8F65A (void);
// 0x0000002C System.Boolean UnityEngine.AI.NavMesh::SamplePosition_Injected(UnityEngine.Vector3&,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern void NavMesh_SamplePosition_Injected_m59777E11E947B46F60E9BCDF8C7CCFFAA5529E0D (void);
// 0x0000002D System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern void OnNavMeshPreUpdate__ctor_m7142A3AA991BE50B637A16D946AB7604C64EF9BA (void);
// 0x0000002E System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::Invoke()
extern void OnNavMeshPreUpdate_Invoke_mFB224B9BBF9C78B7F39AA91A047F175C69897914 (void);
static Il2CppMethodPointer s_methodPointers[46] = 
{
	NavMeshPath__ctor_mEA40BFC2492814FFC97A71C3AEC2154A9415C37F,
	NavMeshPath_Finalize_mB151BFBD5D7E65C343415B6B332A58504F12AF77,
	NavMeshPath_InitializeNavMeshPath_m91B9A02C11B0C86F33F726A047B11D015230E9C2,
	NavMeshPath_DestroyNavMeshPath_mE7FD23F7D0456507277BDC8ED868A7C6888796EB,
	NavMeshPath_ClearCornersInternal_m2310C5CB9B4EB2B3C4685476B2CF8440ED369606,
	NavMeshPath_ClearCorners_m8633C3989850C01982EBD3D4BC70E85AF461CE5B,
	NavMeshPath_get_status_m63B0AEDA3149C7053987C4D0A02B3FE8B41BD74B,
	NavMeshAgent_SetDestination_mD5D960933827F1F14B29CF4A3B6F305C064EBF46,
	NavMeshAgent_get_destination_m1BE2C5EEF53F7EB6317449726C99E0D0955C677E,
	NavMeshAgent_set_destination_m5F0A8E4C8ED93798D6B9CE496B10FCE5B7461B95,
	NavMeshAgent_get_stoppingDistance_mA866A409C59878849D63BEC61517DE4F906BEEC4,
	NavMeshAgent_set_stoppingDistance_m288A6280B55AAFF8578286747E19AF409C7C177F,
	NavMeshAgent_get_velocity_m028219D0E4678D727F00C53AE3DCBCF29AF04DA7,
	NavMeshAgent_get_steeringTarget_mA2DB66413FC7628DCDB1DECF38269A70EC60351C,
	NavMeshAgent_get_desiredVelocity_m8CF3F6E3FA8EE86397DD02146AA6048949B74F52,
	NavMeshAgent_get_remainingDistance_m051C1B408E2740A95B5A5577C5EC7222311AA73A,
	NavMeshAgent_set_baseOffset_m6BB6CF6115E987B0A6268D708148723119C9B122,
	NavMeshAgent_set_autoBraking_m3BBDC74E6ADC3EB8BE0381C97B1D8715C1BC5283,
	NavMeshAgent_get_hasPath_mE56295431F7774ADC8C39CFD87E13073DDDA178A,
	NavMeshAgent_get_pathPending_mA806A4DC1E06CA32A4E7E71B6846B516EC8C2487,
	NavMeshAgent_set_isStopped_mF374E697F39845233B84D8C4873DEABC3AA490DF,
	NavMeshAgent_ResetPath_mE29D3956C1BFABDB3D6B4B7DF2B376B4EEB24E7F,
	NavMeshAgent_CalculatePath_mF692688572E61C3B9FE776AA3784E14A08C259C2,
	NavMeshAgent_CalculatePathInternal_m022C56D89B194E8EAD260A2E2CEEA100024AE004,
	NavMeshAgent_get_areaMask_m7C52D09097A66CC43AA0F1237E59ECE4340F62F7,
	NavMeshAgent_get_speed_m3E4720882ED0C65A4E19AADD53BB6CFFB5BB1345,
	NavMeshAgent_set_speed_m820E45289B3AE7DEE16F2F4BF163EAC361E64646,
	NavMeshAgent_set_angularSpeed_mD2AE9EE187EF1C45F519576B5FC8581DC6B7D683,
	NavMeshAgent_set_acceleration_mEAA92D7837B8D726891846DBBB6114DB13CD597E,
	NavMeshAgent_set_updateRotation_mBF6EDBC9BBAF32490229D7DD6BC821A420C3399D,
	NavMeshAgent_set_updateUpAxis_m46E9DEE866A379D46790708F95FE21435C320A70,
	NavMeshAgent_set_radius_m34F08DD2BD01A5DDA17FE1B02EE03D361B44F1AC,
	NavMeshAgent_set_obstacleAvoidanceType_m4EF45925BCFD06A3A0FA64412F91687D40A738EF,
	NavMeshAgent_SetDestination_Injected_mC3EF405F5AAFF9F98C5D5AECAD641525CDF742EA,
	NavMeshAgent_get_destination_Injected_m864513574C2AD2BA558D85B213CC518C064F893A,
	NavMeshAgent_set_destination_Injected_m7195764B7610A893730EB50F1D9EB70BCDE65BD8,
	NavMeshAgent_get_velocity_Injected_m40A3E476CECB49AE84CA70761FB01FB5644B1735,
	NavMeshAgent_get_steeringTarget_Injected_m00761894AB2F3B67A8775949DBF465506CD78DE1,
	NavMeshAgent_get_desiredVelocity_Injected_m8793EE07C8733261F15C392B983113CBD225DA41,
	NavMeshAgent_CalculatePathInternal_Injected_m07BFBF7B199DE22CB3AB819BACA56953D2C56C06,
	NavMeshHit_get_position_m09E8FF6DEF5BFA3F30B3C4BCA4642442FF1BCBF1,
	NavMesh_Internal_CallOnNavMeshPreUpdate_m80148CFDD0C6F1DDDE5B3DA67A8D9613043A4233,
	NavMesh_SamplePosition_m51497866E71DD5263425E8572F2232D496E8F65A,
	NavMesh_SamplePosition_Injected_m59777E11E947B46F60E9BCDF8C7CCFFAA5529E0D,
	OnNavMeshPreUpdate__ctor_m7142A3AA991BE50B637A16D946AB7604C64EF9BA,
	OnNavMeshPreUpdate_Invoke_mFB224B9BBF9C78B7F39AA91A047F175C69897914,
};
extern void NavMeshHit_get_position_m09E8FF6DEF5BFA3F30B3C4BCA4642442FF1BCBF1_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000029, NavMeshHit_get_position_m09E8FF6DEF5BFA3F30B3C4BCA4642442FF1BCBF1_AdjustorThunk },
};
static const int32_t s_InvokerIndices[46] = 
{
	6341,
	6341,
	11224,
	10464,
	6341,
	6341,
	6192,
	4331,
	6331,
	5095,
	6281,
	5052,
	6331,
	6331,
	6331,
	6281,
	5052,
	5041,
	6271,
	6271,
	5041,
	6341,
	2087,
	2087,
	6192,
	6281,
	5052,
	5052,
	5052,
	5041,
	5041,
	5052,
	4967,
	4103,
	4888,
	4888,
	4888,
	4888,
	4888,
	1931,
	6331,
	11271,
	7576,
	7532,
	2755,
	6341,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_AIModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AIModule_CodeGenModule = 
{
	"UnityEngine.AIModule.dll",
	46,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
