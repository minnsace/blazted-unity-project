﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Int32[]>
struct Dictionary_2_tCB9019887EB0254D4745B0724BC12327C5B63792;
// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Int32>
struct Dictionary_2_tB41FAC88F07BAB98D6D373F7C94FB0496D1BDA32;
// System.Collections.Generic.IEnumerator`1<System.Action`2<UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.CommandBuffer>>
struct IEnumerator_1_t5926539DBBB2302C569D0A07AF3A95A874CEBE33;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252;
// System.Collections.Generic.List`1<BNG.GrabbableEvents>
struct List_1_tE6D1ED582321C7A7F69E8175188B1EB838421F2A;
// System.Collections.Generic.List`1<BNG.GrabbedControllerBinding>
struct List_1_tAD8C73BB08C3242046D9FF8B9A80F7FE939A02D8;
// System.Collections.Generic.List`1<BNG.Grabber>
struct List_1_tEE63DC26EA202668033FEBEA09A3506D65E2507D;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73;
// System.Collections.Generic.List`1<UnityEngine.Rendering.Universal.ScriptableRenderPass>
struct List_1_t2E485E650BF1E41358CE56A69323E183C5A89CB6;
// System.Collections.Generic.List`1<UnityEngine.Rendering.Universal.ScriptableRendererFeature>
struct List_1_t2121653FB628940E808D105AD2C17E0F20AFB3A6;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tF42FEB6C3B18B7E7C8F2DE1FEBA00D2491736317;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t179D865D5B30EFCBC50F82C9774329C15943466E;
// UnityEngine.Rendering.RenderTargetIdentifier[][]
struct RenderTargetIdentifierU5BU5DU5BU5D_tDB35F8D017FE3AD8BB35E08E323074D47C5A10BB;
// UnityEngine.Rendering.AttachmentDescriptor[]
struct AttachmentDescriptorU5BU5D_tC70107EBD955FE94BA31C7FDC146069EF9C547C1;
// System.Boolean[]
struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// UnityEngine.Rendering.GraphicsDeviceType[]
struct GraphicsDeviceTypeU5BU5D_t4322565F239068C66BF47053B83BD6A9A9D16408;
// UnityEngine.Experimental.Rendering.GraphicsFormat[]
struct GraphicsFormatU5BU5D_tF6A3D90C430FA3F548B77E5D58D25D71F154E6C5;
// UnityEngine.Hash128[]
struct Hash128U5BU5D_tB104E7247B842648E447B7FCF4748077DC1F8C98;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t4EEF66BAA8B0140EFFF34F6183CE7F80546592BE;
// UnityEngine.Rendering.RenderBufferStoreAction[]
struct RenderBufferStoreActionU5BU5D_tFEA8F5DD460573EA9F35FBEC5727D1804C5DCBF5;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t179798C153B7CE381B41C57863F98CB24023C4CE;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tC0F3A7115F85007510F6D173968200CD31BCF7AD;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// UnityEngine.AudioClip
struct AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20;
// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299;
// DrunkMan.AutoRotate
struct AutoRotate_tC91AFA738AEA5EC3E037D7C011A3070113E0FE49;
// BNG.BNGPlayerController
struct BNGPlayerController_t9093D731F0BBC6A4DA623F3127D95A13803945E4;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t8B33AB5A6B8D52493F14C4B8DBDF78A1C94ECB77;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// UnityEngine.Rendering.CullingAllocationInfo
struct CullingAllocationInfo_tB260F5CD0B290F74E145EB16E54B901CC68D9D5A;
// UnityEngine.Rendering.Universal.DebugHandler
struct DebugHandler_t3A09E2CFD1CA6F5C192968A6FF19EE4863F44DA4;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// DrunkMan.BuiltIn.Demo
struct Demo_tB2CE2F3309296F5900575784169917681D1E7D28;
// DrunkMan.DrunkManFeature
struct DrunkManFeature_tFED478401670ECFD6FF972AA05597D399B655C11;
// BNG.FloatEvent
struct FloatEvent_tB5BBD837F83AC37CEE0C4C7087E97829D38315F0;
// DrunkMan.FreeCamera
struct FreeCamera_t17D2C98F0E4AE04AA5D7713B35EA0230081BC088;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// BNG.GrabPoint
struct GrabPoint_tA5EE4F3317CACF01DFEEF4ADCA052D6DD1568F6F;
// BNG.Grabbable
struct Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B;
// BNG.Grabber
struct Grabber_tCCBBC63D0FECBDD434CE54930135C6A1D5C22B98;
// BNG.HandPose
struct HandPose_tD971C9E16E48BF9C54143941B3D05026720345BB;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// BNG.InputBridge
struct InputBridge_tA0CD5FB01685872EBB8C87A5C5FEAB0DDC483A17;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.Rendering.ProfilingSampler
struct ProfilingSampler_t420D4672EDB44E0EF980B31ADFD9E5747200FECE;
// BNG.RaycastHitEvent
struct RaycastHitEvent_t565AB54EC826DC0FB61F834B1E155DA82716F75D;
// BNG.RaycastWeapon
struct RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160;
// UnityEngine.Rendering.RenderPipelineAsset
struct RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E;
// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// UnityEngine.Rendering.Universal.ScriptableRenderPass
struct ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0;
// UnityEngine.Rendering.Universal.ScriptableRenderer
struct ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892;
// UnityEngine.Rendering.Universal.ScriptableRendererFeature
struct ScriptableRendererFeature_tF2ED08AD2498105378D4DAB0386A0F8A144EF4C6;
// StatsManager
struct StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// System.Type
struct Type_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977;
// BNG.VRUtils
struct VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3;
// BNG.WeaponSlide
struct WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2;
// UnityEngine.Rendering.Universal.XRPass
struct XRPass_t0A618D61DBC9E3F8BC970B7C9D2679375C6C8A24;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072;
// DrunkMan.DrunkManFeature/BlitPass
struct BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8;
// DrunkMan.DrunkManFeature/Settings
struct Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463;
// BNG.PlayerTeleport/OnAfterTeleportAction
struct OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808;
// BNG.PlayerTeleport/OnBeforeTeleportAction
struct OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8;
// BNG.RaycastWeapon/<animateSlideAndEject>d__75
struct U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8;
// BNG.RaycastWeapon/<doMuzzleFlash>d__74
struct U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F;
// UnityEngine.Rendering.Universal.ScriptableRenderer/RenderingFeatures
struct RenderingFeatures_t31044CBDCDC2F05194BFA2A2122FBD937D78A371;
// BNG.WeaponSlide/<UnlockSlideRoutine>d__27
struct U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245;

IL2CPP_EXTERN_C RuntimeClass* BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CommandBufferPool_t88CACA06AB445EE4743F5C4D742C73761A2DEF0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral005930A9597B0BEA2250DA712041C3035CEB8736;
IL2CPP_EXTERN_C String_t* _stringLiteral019188D0A51A0F9C7D99991CF3646D685A087B71;
IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral21627BBE8EBCFC824DEF784AC25AB0E7B675D01D;
IL2CPP_EXTERN_C String_t* _stringLiteral2CFD5C1DB1662C9ACCEC5B8AE7260D552F5A83AC;
IL2CPP_EXTERN_C String_t* _stringLiteral405CCEE21C770033E6F661F8E266E281B6A6E0FE;
IL2CPP_EXTERN_C String_t* _stringLiteral486F1BA05E3312BE67C1B74F652C6E2D7A691600;
IL2CPP_EXTERN_C String_t* _stringLiteral5B3FA2417F9F161F82E54814DD98BF6B7ACE3837;
IL2CPP_EXTERN_C String_t* _stringLiteral5B983EEC590B83119333B9EC9CE00399B0CFFDD1;
IL2CPP_EXTERN_C String_t* _stringLiteral5D2DBA484FA2F0A6149D2EF4C993EA43125C8E31;
IL2CPP_EXTERN_C String_t* _stringLiteral63B88C4EFDF4F8C73E03469100CADA32CD8BD835;
IL2CPP_EXTERN_C String_t* _stringLiteral8714C9231CFAC05EA788784A479528F0DBA10DA9;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E;
IL2CPP_EXTERN_C String_t* _stringLiteral98DF8830FA14DA365C6B54CB182D990F6F2F2A0E;
IL2CPP_EXTERN_C String_t* _stringLiteral9C40692ED77768D578327A3E71988DFF501534A4;
IL2CPP_EXTERN_C String_t* _stringLiteralA897552B9939370E6487698DB3A96C96CEE06466;
IL2CPP_EXTERN_C String_t* _stringLiteralBA0CE45FD5A657EDBA8FB9361FC9EACBFD7C6E87;
IL2CPP_EXTERN_C String_t* _stringLiteralC34E69D38B35322888754CDF598BC35F2852041E;
IL2CPP_EXTERN_C String_t* _stringLiteralD0420C099ECAC4F9F3312E56381F79F3B4CE7BEA;
IL2CPP_EXTERN_C String_t* _stringLiteralEE06B53F9B6FDDA6A65F38986E1BA6B03495562F;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCollider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76_m820398EDBF1D3766C3166A0C323A127662A29A14_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisGrabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B_m50588DDDB54F724D3F63C020A51049BC4128BB24_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160_mF95EF20FB03601694578076A5D3C3E2FEA9A4FA7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisStatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87_m6D7084B39D7647C73A971160764AD450B56326F1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_Reset_m080CAB29CBCEA62C742FB8CCDCD01008A73AAF38_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_Reset_m042CF81A8EDB791453EADF7B62CEBBB35821BA90_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_Reset_mC91A6508DC6552A176052DC75E80915C65800083_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct Il2CppArrayBounds;

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// BNG.RaycastWeapon/<doMuzzleFlash>d__74
struct U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F  : public RuntimeObject
{
	// System.Int32 BNG.RaycastWeapon/<doMuzzleFlash>d__74::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BNG.RaycastWeapon/<doMuzzleFlash>d__74::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// BNG.RaycastWeapon BNG.RaycastWeapon/<doMuzzleFlash>d__74::<>4__this
	RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* ___U3CU3E4__this_2;
};

// BNG.WeaponSlide/<UnlockSlideRoutine>d__27
struct U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245  : public RuntimeObject
{
	// System.Int32 BNG.WeaponSlide/<UnlockSlideRoutine>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BNG.WeaponSlide/<UnlockSlideRoutine>d__27::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// BNG.WeaponSlide BNG.WeaponSlide/<UnlockSlideRoutine>d__27::<>4__this
	WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* ___U3CU3E4__this_2;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};

struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_StaticFields
{
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___enumSeperatorCharArray_0;
};
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.LayerMask
struct LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB 
{
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// UnityEngine.Rendering.ShaderTagId
struct ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0 
{
	// System.Int32 UnityEngine.Rendering.ShaderTagId::m_Id
	int32_t ___m_Id_1;
};

struct ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0_StaticFields
{
	// UnityEngine.Rendering.ShaderTagId UnityEngine.Rendering.ShaderTagId::none
	ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0 ___none_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
};

// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	float ___m_Seconds_0;
};

// Unity.Collections.Allocator
struct Allocator_t996642592271AAD9EE688F142741D512C07B5824 
{
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.Universal.AntialiasingMode
struct AntialiasingMode_tDF75AC7BDAF51FA550F528F7B798416ACB8D3487 
{
	// System.Int32 UnityEngine.Rendering.Universal.AntialiasingMode::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.Universal.AntialiasingQuality
struct AntialiasingQuality_t45B2A050F79EB8B705FED3F3F30A70942E71D605 
{
	// System.Int32 UnityEngine.Rendering.Universal.AntialiasingQuality::value__
	int32_t ___value___2;
};

// System.Reflection.BindingFlags
struct BindingFlags_t5DC2835E4AE9C1862B3AD172EF35B6A5F4F1812C 
{
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.BuiltinRenderTextureType
struct BuiltinRenderTextureType_t3D56813CAC7C6E4AC3B438039BD1CE7E62FE7C4E 
{
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.Universal.CameraRenderType
struct CameraRenderType_tC686ABD18F67CA30E6DF217007744F509606A41D 
{
	// System.Int32 UnityEngine.Rendering.Universal.CameraRenderType::value__
	int32_t ___value___2;
};

// UnityEngine.CameraType
struct CameraType_tCA1017DBE96964E1D967942FB98F152F14121FCD 
{
	// System.Int32 UnityEngine.CameraType::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.ClearFlag
struct ClearFlag_t0B57BE5A60AA0EE7CC0DAE7E7DF82EB993A59ADD 
{
	// System.Int32 UnityEngine.Rendering.ClearFlag::value__
	int32_t ___value___2;
};

// UnityEngine.CollisionDetectionMode
struct CollisionDetectionMode_tE78B6425ECA33250872A4B624D2B03A976163459 
{
	// System.Int32 UnityEngine.CollisionDetectionMode::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.Universal.ColorGradingMode
struct ColorGradingMode_t980B9396D20213763F23C4D474BC079FC68BF83E 
{
	// System.Int32 UnityEngine.Rendering.Universal.ColorGradingMode::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.ConfigurableJointMotion
struct ConfigurableJointMotion_t724E5894546E82409B2EC8916ABEB87A262D3610 
{
	// System.Int32 UnityEngine.ConfigurableJointMotion::value__
	int32_t ___value___2;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.CubemapFace
struct CubemapFace_t300D6E2CD7DF60D44AA28338748B607677ED1D1B 
{
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.CullingResults
struct CullingResults_tD6B7EF20B68D47DFF3A99EB2EA73F47F1D460267 
{
	// System.IntPtr UnityEngine.Rendering.CullingResults::ptr
	intptr_t ___ptr_0;
	// UnityEngine.Rendering.CullingAllocationInfo* UnityEngine.Rendering.CullingResults::m_AllocationInfo
	CullingAllocationInfo_tB260F5CD0B290F74E145EB16E54B901CC68D9D5A* ___m_AllocationInfo_1;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.FilterMode
struct FilterMode_t4AD57F1A3FE272D650E0E688BA044AE872BD2A34 
{
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;
};

// BNG.FiringType
struct FiringType_t0011FDF8C0AFCF055C886C631986B2CDF5EA0A07 
{
	// System.Int32 BNG.FiringType::value__
	int32_t ___value___2;
};

// BNG.GrabButton
struct GrabButton_t7B3FEB5411AFC135FBAF6963432C193F6AAF56C7 
{
	// System.Int32 BNG.GrabButton::value__
	int32_t ___value___2;
};

// BNG.GrabPhysics
struct GrabPhysics_t9B57DBB746B921FB26412C3A300A21E9148CD9E4 
{
	// System.Int32 BNG.GrabPhysics::value__
	int32_t ___value___2;
};

// BNG.GrabType
struct GrabType_t5C9D6F5AC978E4D21782F0C4B64CE72806261A37 
{
	// System.Int32 BNG.GrabType::value__
	int32_t ___value___2;
};

// UnityEngine.Experimental.Rendering.GraphicsFormat
struct GraphicsFormat_tC3D1898F3F3F1F57256C7F3FFD6BA9A37AE7E713 
{
	// System.Int32 UnityEngine.Experimental.Rendering.GraphicsFormat::value__
	int32_t ___value___2;
};

// BNG.HandPoseId
struct HandPoseId_tFB6C5504567AABABBEC7CCE49E111800E6C4B21A 
{
	// System.Int32 BNG.HandPoseId::value__
	int32_t ___value___2;
};

// BNG.HandPoseType
struct HandPoseType_t5D17777AE5F49735117ED50DE10E2C1B2895A7FB 
{
	// System.Int32 BNG.HandPoseType::value__
	int32_t ___value___2;
};

// BNG.HoldType
struct HoldType_t0CC77337106C80751F5766B37C38522DDF4B8B42 
{
	// System.Int32 BNG.HoldType::value__
	int32_t ___value___2;
};

// UnityEngine.KeyCode
struct KeyCode_t75B9ECCC26D858F55040DDFF9523681E996D17E9 
{
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// BNG.OtherGrabBehavior
struct OtherGrabBehavior_t9C6619BAC68AFEFD95FD520DE938142DA0B33272 
{
	// System.Int32 BNG.OtherGrabBehavior::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.PerObjectData
struct PerObjectData_t04DDCBE9ABF1113E8F9BAFCF4A7F94DD841B9CC9 
{
	// System.Int32 UnityEngine.Rendering.PerObjectData::value__
	int32_t ___value___2;
};

// BNG.ReloadType
struct ReloadType_tDB6DF86DD04957C2F52EF7649B12ABE0DE178447 
{
	// System.Int32 BNG.ReloadType::value__
	int32_t ___value___2;
};

// BNG.RemoteGrabMovement
struct RemoteGrabMovement_t6904FCA07FF10EC45A1CBC472605BADEA9F0511D 
{
	// System.Int32 BNG.RemoteGrabMovement::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.RenderBufferLoadAction
struct RenderBufferLoadAction_t3333B2CABABAC39DA0CDC25602E5E4FD93C2CB0E 
{
	// System.Int32 UnityEngine.Rendering.RenderBufferLoadAction::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.RenderBufferStoreAction
struct RenderBufferStoreAction_t87683F22C09634E24A574F21F42037C953A2C8B7 
{
	// System.Int32 UnityEngine.Rendering.RenderBufferStoreAction::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.Universal.RenderPassEvent
struct RenderPassEvent_t65FBDDF314AC831A598C794FD81BB61AD3930353 
{
	// System.Int32 UnityEngine.Rendering.Universal.RenderPassEvent::value__
	int32_t ___value___2;
};

// UnityEngine.RenderTextureCreationFlags
struct RenderTextureCreationFlags_t1C01993691E5BA956575134696509089FE852F50 
{
	// System.Int32 UnityEngine.RenderTextureCreationFlags::value__
	int32_t ___value___2;
};

// UnityEngine.RenderTextureMemoryless
struct RenderTextureMemoryless_tE3B7F3AE353C3E9ACF86076376EB862131D19A69 
{
	// System.Int32 UnityEngine.RenderTextureMemoryless::value__
	int32_t ___value___2;
};

// UnityEngine.RigidbodyInterpolation
struct RigidbodyInterpolation_tE2BE80352B0D72DB26B81EFD5A0845DEFEE994F8 
{
	// System.Int32 UnityEngine.RigidbodyInterpolation::value__
	int32_t ___value___2;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// UnityEngine.Rendering.ScriptableRenderContext
struct ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 
{
	// System.IntPtr UnityEngine.Rendering.ScriptableRenderContext::m_Ptr
	intptr_t ___m_Ptr_1;
};

struct ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36_StaticFields
{
	// UnityEngine.Rendering.ShaderTagId UnityEngine.Rendering.ScriptableRenderContext::kRenderTypeTag
	ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0 ___kRenderTypeTag_0;
};

// UnityEngine.Rendering.Universal.ScriptableRenderPassInput
struct ScriptableRenderPassInput_t2E28A5DE1B3B8001EE14298E0133EFF3204DE645 
{
	// System.Int32 UnityEngine.Rendering.Universal.ScriptableRenderPassInput::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.Universal.ShadowData
struct ShadowData_tA165FDF7CA4CE6BEA8B649FFAB91C59ED684D832 
{
	// System.Boolean UnityEngine.Rendering.Universal.ShadowData::supportsMainLightShadows
	bool ___supportsMainLightShadows_0;
	// System.Boolean UnityEngine.Rendering.Universal.ShadowData::requiresScreenSpaceShadowResolve
	bool ___requiresScreenSpaceShadowResolve_1;
	// System.Int32 UnityEngine.Rendering.Universal.ShadowData::mainLightShadowmapWidth
	int32_t ___mainLightShadowmapWidth_2;
	// System.Int32 UnityEngine.Rendering.Universal.ShadowData::mainLightShadowmapHeight
	int32_t ___mainLightShadowmapHeight_3;
	// System.Int32 UnityEngine.Rendering.Universal.ShadowData::mainLightShadowCascadesCount
	int32_t ___mainLightShadowCascadesCount_4;
	// UnityEngine.Vector3 UnityEngine.Rendering.Universal.ShadowData::mainLightShadowCascadesSplit
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___mainLightShadowCascadesSplit_5;
	// System.Single UnityEngine.Rendering.Universal.ShadowData::mainLightShadowCascadeBorder
	float ___mainLightShadowCascadeBorder_6;
	// System.Boolean UnityEngine.Rendering.Universal.ShadowData::supportsAdditionalLightShadows
	bool ___supportsAdditionalLightShadows_7;
	// System.Int32 UnityEngine.Rendering.Universal.ShadowData::additionalLightsShadowmapWidth
	int32_t ___additionalLightsShadowmapWidth_8;
	// System.Int32 UnityEngine.Rendering.Universal.ShadowData::additionalLightsShadowmapHeight
	int32_t ___additionalLightsShadowmapHeight_9;
	// System.Boolean UnityEngine.Rendering.Universal.ShadowData::supportsSoftShadows
	bool ___supportsSoftShadows_10;
	// System.Int32 UnityEngine.Rendering.Universal.ShadowData::shadowmapDepthBufferBits
	int32_t ___shadowmapDepthBufferBits_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.Rendering.Universal.ShadowData::bias
	List_1_tF42FEB6C3B18B7E7C8F2DE1FEBA00D2491736317* ___bias_12;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Rendering.Universal.ShadowData::resolution
	List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* ___resolution_13;
	// System.Boolean UnityEngine.Rendering.Universal.ShadowData::isKeywordAdditionalLightShadowsEnabled
	bool ___isKeywordAdditionalLightShadowsEnabled_14;
	// System.Boolean UnityEngine.Rendering.Universal.ShadowData::isKeywordSoftShadowsEnabled
	bool ___isKeywordSoftShadowsEnabled_15;
};
// Native definition for P/Invoke marshalling of UnityEngine.Rendering.Universal.ShadowData
struct ShadowData_tA165FDF7CA4CE6BEA8B649FFAB91C59ED684D832_marshaled_pinvoke
{
	int32_t ___supportsMainLightShadows_0;
	int32_t ___requiresScreenSpaceShadowResolve_1;
	int32_t ___mainLightShadowmapWidth_2;
	int32_t ___mainLightShadowmapHeight_3;
	int32_t ___mainLightShadowCascadesCount_4;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___mainLightShadowCascadesSplit_5;
	float ___mainLightShadowCascadeBorder_6;
	int32_t ___supportsAdditionalLightShadows_7;
	int32_t ___additionalLightsShadowmapWidth_8;
	int32_t ___additionalLightsShadowmapHeight_9;
	int32_t ___supportsSoftShadows_10;
	int32_t ___shadowmapDepthBufferBits_11;
	List_1_tF42FEB6C3B18B7E7C8F2DE1FEBA00D2491736317* ___bias_12;
	List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* ___resolution_13;
	int32_t ___isKeywordAdditionalLightShadowsEnabled_14;
	int32_t ___isKeywordSoftShadowsEnabled_15;
};
// Native definition for COM marshalling of UnityEngine.Rendering.Universal.ShadowData
struct ShadowData_tA165FDF7CA4CE6BEA8B649FFAB91C59ED684D832_marshaled_com
{
	int32_t ___supportsMainLightShadows_0;
	int32_t ___requiresScreenSpaceShadowResolve_1;
	int32_t ___mainLightShadowmapWidth_2;
	int32_t ___mainLightShadowmapHeight_3;
	int32_t ___mainLightShadowCascadesCount_4;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___mainLightShadowCascadesSplit_5;
	float ___mainLightShadowCascadeBorder_6;
	int32_t ___supportsAdditionalLightShadows_7;
	int32_t ___additionalLightsShadowmapWidth_8;
	int32_t ___additionalLightsShadowmapHeight_9;
	int32_t ___supportsSoftShadows_10;
	int32_t ___shadowmapDepthBufferBits_11;
	List_1_tF42FEB6C3B18B7E7C8F2DE1FEBA00D2491736317* ___bias_12;
	List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* ___resolution_13;
	int32_t ___isKeywordAdditionalLightShadowsEnabled_14;
	int32_t ___isKeywordSoftShadowsEnabled_15;
};

// UnityEngine.Rendering.ShadowSamplingMode
struct ShadowSamplingMode_t8BE740C4258CFEDDBAC01FDC0438D8EE3F776BA8 
{
	// System.Int32 UnityEngine.Rendering.ShadowSamplingMode::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.SortingCriteria
struct SortingCriteria_t4907D221CB6E6AA4A32C1ED7B5D17103FD3E7C39 
{
	// System.Int32 UnityEngine.Rendering.SortingCriteria::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.Universal.StoreActionsOptimization
struct StoreActionsOptimization_tB5EB82E81175365B1DF3C5DE71F35E77E3B38B4A 
{
	// System.Int32 UnityEngine.Rendering.Universal.StoreActionsOptimization::value__
	int32_t ___value___2;
};

// UnityEngine.Rendering.TextureDimension
struct TextureDimension_t8D7148B9168256EE1E9AF91378ABA148888CE642 
{
	// System.Int32 UnityEngine.Rendering.TextureDimension::value__
	int32_t ___value___2;
};

// BNG.TwoHandedDropMechanic
struct TwoHandedDropMechanic_t83F5456042D43A4112A804A3C8F90F6DCEDB73D8 
{
	// System.Int32 BNG.TwoHandedDropMechanic::value__
	int32_t ___value___2;
};

// BNG.TwoHandedLookDirection
struct TwoHandedLookDirection_t114989FFB97B30F1928684EA4FBF7063AF6ED014 
{
	// System.Int32 BNG.TwoHandedLookDirection::value__
	int32_t ___value___2;
};

// BNG.TwoHandedPositionType
struct TwoHandedPositionType_t340832DA44E971103B339223529599EF095C9DC7 
{
	// System.Int32 BNG.TwoHandedPositionType::value__
	int32_t ___value___2;
};

// BNG.TwoHandedRotationType
struct TwoHandedRotationType_tEE4847136C961FAD98C6BC0615C906B7F4ED1583 
{
	// System.Int32 BNG.TwoHandedRotationType::value__
	int32_t ___value___2;
};

// UnityEngine.VRTextureUsage
struct VRTextureUsage_t57FAA0077810142A461D74EDC5E33FC3D78BD2E8 
{
	// System.Int32 UnityEngine.VRTextureUsage::value__
	int32_t ___value___2;
};

// DrunkMan.BuiltIn.Demo/EType
struct EType_t0EC232CDE758DBC5425D7B0B8E3DC34CBFF92542 
{
	// System.Int32 DrunkMan.BuiltIn.Demo/EType::value__
	int32_t ___value___2;
};

// DrunkMan.DrunkManFeature/EType
struct EType_t8115FE9F72AC79487723BAE62299E2942F15854A 
{
	// System.Int32 DrunkMan.DrunkManFeature/EType::value__
	int32_t ___value___2;
};

// BNG.RaycastWeapon/<animateSlideAndEject>d__75
struct U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8  : public RuntimeObject
{
	// System.Int32 BNG.RaycastWeapon/<animateSlideAndEject>d__75::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BNG.RaycastWeapon/<animateSlideAndEject>d__75::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// BNG.RaycastWeapon BNG.RaycastWeapon/<animateSlideAndEject>d__75::<>4__this
	RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* ___U3CU3E4__this_2;
	// System.Int32 BNG.RaycastWeapon/<animateSlideAndEject>d__75::<frames>5__2
	int32_t ___U3CframesU3E5__2_3;
	// System.Boolean BNG.RaycastWeapon/<animateSlideAndEject>d__75::<slideEndReached>5__3
	bool ___U3CslideEndReachedU3E5__3_4;
	// UnityEngine.Vector3 BNG.RaycastWeapon/<animateSlideAndEject>d__75::<slideDestination>5__4
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CslideDestinationU3E5__4_5;
	// System.Boolean BNG.RaycastWeapon/<animateSlideAndEject>d__75::<slideBeginningReached>5__5
	bool ___U3CslideBeginningReachedU3E5__5_6;
};

// Unity.Collections.NativeArray`1<System.Int32>
struct NativeArray_1_tA833EB7E3E1C9AF82C37976AD964B8D4BAC38B2C 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// Unity.Collections.NativeArray`1<UnityEngine.Rendering.VisibleLight>
struct NativeArray_1_t71485A1E60B31CCAD3E525C907CF172E8B804468 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// UnityEngine.AudioClip
struct AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E* ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072* ___m_PCMSetPositionCallback_5;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.Rendering.Universal.PostProcessingData
struct PostProcessingData_tFA75BF22951C600258B2707AF7A113E4EDA49BD4 
{
	// UnityEngine.Rendering.Universal.ColorGradingMode UnityEngine.Rendering.Universal.PostProcessingData::gradingMode
	int32_t ___gradingMode_0;
	// System.Int32 UnityEngine.Rendering.Universal.PostProcessingData::lutSize
	int32_t ___lutSize_1;
	// System.Boolean UnityEngine.Rendering.Universal.PostProcessingData::useFastSRGBLinearConversion
	bool ___useFastSRGBLinearConversion_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Rendering.Universal.PostProcessingData
struct PostProcessingData_tFA75BF22951C600258B2707AF7A113E4EDA49BD4_marshaled_pinvoke
{
	int32_t ___gradingMode_0;
	int32_t ___lutSize_1;
	int32_t ___useFastSRGBLinearConversion_2;
};
// Native definition for COM marshalling of UnityEngine.Rendering.Universal.PostProcessingData
struct PostProcessingData_tFA75BF22951C600258B2707AF7A113E4EDA49BD4_marshaled_com
{
	int32_t ___gradingMode_0;
	int32_t ___lutSize_1;
	int32_t ___useFastSRGBLinearConversion_2;
};

// UnityEngine.Rendering.RenderTargetIdentifier
struct RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B 
{
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_2;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_3;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_4;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_5;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_6;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_7;
};

// UnityEngine.RenderTextureDescriptor
struct RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 
{
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<mipCount>k__BackingField
	int32_t ___U3CmipCountU3Ek__BackingField_4;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::_graphicsFormat
	int32_t ____graphicsFormat_5;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::<stencilFormat>k__BackingField
	int32_t ___U3CstencilFormatU3Ek__BackingField_6;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::<depthStencilFormat>k__BackingField
	int32_t ___U3CdepthStencilFormatU3Ek__BackingField_7;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_8;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_9;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_10;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_11;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_12;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// DrunkMan.DrunkManFeature/Settings
struct Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463  : public RuntimeObject
{
	// UnityEngine.Material DrunkMan.DrunkManFeature/Settings::m_Mat
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Mat_0;
	// DrunkMan.DrunkManFeature/EType DrunkMan.DrunkManFeature/Settings::m_Type
	int32_t ___m_Type_1;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_Intensity
	float ___m_Intensity_2;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_RGBShiftFactor
	float ___m_RGBShiftFactor_3;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_RGBShiftPower
	float ___m_RGBShiftPower_4;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_GhostSeeRadius
	float ___m_GhostSeeRadius_5;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_GhostSeeMix
	float ___m_GhostSeeMix_6;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_GhostSeeAmplitude
	float ___m_GhostSeeAmplitude_7;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_Frequency
	float ___m_Frequency_8;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_Period
	float ___m_Period_9;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_Amplitude
	float ___m_Amplitude_10;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_BlurMin
	float ___m_BlurMin_11;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_BlurMax
	float ___m_BlurMax_12;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_BlurSpeed
	float ___m_BlurSpeed_13;
	// System.Boolean DrunkMan.DrunkManFeature/Settings::m_SleepyEye
	bool ___m_SleepyEye_14;
	// System.Single DrunkMan.DrunkManFeature/Settings::m_EyeClose
	float ___m_EyeClose_15;
};

// UnityEngine.Rendering.AttachmentDescriptor
struct AttachmentDescriptor_tBAC9B26B50BB0838C5C0CC22BB296F9DFF41276E 
{
	// UnityEngine.Rendering.RenderBufferLoadAction UnityEngine.Rendering.AttachmentDescriptor::m_LoadAction
	int32_t ___m_LoadAction_0;
	// UnityEngine.Rendering.RenderBufferStoreAction UnityEngine.Rendering.AttachmentDescriptor::m_StoreAction
	int32_t ___m_StoreAction_1;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Rendering.AttachmentDescriptor::m_Format
	int32_t ___m_Format_2;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.AttachmentDescriptor::m_LoadStoreTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_LoadStoreTarget_3;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.AttachmentDescriptor::m_ResolveTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_ResolveTarget_4;
	// UnityEngine.Color UnityEngine.Rendering.AttachmentDescriptor::m_ClearColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_ClearColor_5;
	// System.Single UnityEngine.Rendering.AttachmentDescriptor::m_ClearDepth
	float ___m_ClearDepth_6;
	// System.UInt32 UnityEngine.Rendering.AttachmentDescriptor::m_ClearStencil
	uint32_t ___m_ClearStencil_7;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Rendering.Universal.CameraData
struct CameraData_tC27AE109CD20677486A4AC19C0CF014AE0F50C3E 
{
	// UnityEngine.Matrix4x4 UnityEngine.Rendering.Universal.CameraData::m_ViewMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_ViewMatrix_0;
	// UnityEngine.Matrix4x4 UnityEngine.Rendering.Universal.CameraData::m_ProjectionMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_ProjectionMatrix_1;
	// UnityEngine.Camera UnityEngine.Rendering.Universal.CameraData::camera
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_2;
	// UnityEngine.Rendering.Universal.CameraRenderType UnityEngine.Rendering.Universal.CameraData::renderType
	int32_t ___renderType_3;
	// UnityEngine.RenderTexture UnityEngine.Rendering.Universal.CameraData::targetTexture
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___targetTexture_4;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Rendering.Universal.CameraData::cameraTargetDescriptor
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___cameraTargetDescriptor_5;
	// UnityEngine.Rect UnityEngine.Rendering.Universal.CameraData::pixelRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___pixelRect_6;
	// System.Int32 UnityEngine.Rendering.Universal.CameraData::pixelWidth
	int32_t ___pixelWidth_7;
	// System.Int32 UnityEngine.Rendering.Universal.CameraData::pixelHeight
	int32_t ___pixelHeight_8;
	// System.Single UnityEngine.Rendering.Universal.CameraData::aspectRatio
	float ___aspectRatio_9;
	// System.Single UnityEngine.Rendering.Universal.CameraData::renderScale
	float ___renderScale_10;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::clearDepth
	bool ___clearDepth_11;
	// UnityEngine.CameraType UnityEngine.Rendering.Universal.CameraData::cameraType
	int32_t ___cameraType_12;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::isDefaultViewport
	bool ___isDefaultViewport_13;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::isHdrEnabled
	bool ___isHdrEnabled_14;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::requiresDepthTexture
	bool ___requiresDepthTexture_15;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::requiresOpaqueTexture
	bool ___requiresOpaqueTexture_16;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::postProcessingRequiresDepthTexture
	bool ___postProcessingRequiresDepthTexture_17;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::xrRendering
	bool ___xrRendering_18;
	// UnityEngine.Rendering.SortingCriteria UnityEngine.Rendering.Universal.CameraData::defaultOpaqueSortFlags
	int32_t ___defaultOpaqueSortFlags_19;
	// UnityEngine.Rendering.Universal.XRPass UnityEngine.Rendering.Universal.CameraData::xr
	XRPass_t0A618D61DBC9E3F8BC970B7C9D2679375C6C8A24* ___xr_20;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::isStereoEnabled
	bool ___isStereoEnabled_21;
	// System.Single UnityEngine.Rendering.Universal.CameraData::maxShadowDistance
	float ___maxShadowDistance_22;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::postProcessEnabled
	bool ___postProcessEnabled_23;
	// System.Collections.Generic.IEnumerator`1<System.Action`2<UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.CommandBuffer>> UnityEngine.Rendering.Universal.CameraData::captureActions
	RuntimeObject* ___captureActions_24;
	// UnityEngine.LayerMask UnityEngine.Rendering.Universal.CameraData::volumeLayerMask
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___volumeLayerMask_25;
	// UnityEngine.Transform UnityEngine.Rendering.Universal.CameraData::volumeTrigger
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___volumeTrigger_26;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::isStopNaNEnabled
	bool ___isStopNaNEnabled_27;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::isDitheringEnabled
	bool ___isDitheringEnabled_28;
	// UnityEngine.Rendering.Universal.AntialiasingMode UnityEngine.Rendering.Universal.CameraData::antialiasing
	int32_t ___antialiasing_29;
	// UnityEngine.Rendering.Universal.AntialiasingQuality UnityEngine.Rendering.Universal.CameraData::antialiasingQuality
	int32_t ___antialiasingQuality_30;
	// UnityEngine.Rendering.Universal.ScriptableRenderer UnityEngine.Rendering.Universal.CameraData::renderer
	ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* ___renderer_31;
	// System.Boolean UnityEngine.Rendering.Universal.CameraData::resolveFinalTarget
	bool ___resolveFinalTarget_32;
	// UnityEngine.Vector3 UnityEngine.Rendering.Universal.CameraData::worldSpaceCameraPos
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldSpaceCameraPos_33;
};
// Native definition for P/Invoke marshalling of UnityEngine.Rendering.Universal.CameraData
struct CameraData_tC27AE109CD20677486A4AC19C0CF014AE0F50C3E_marshaled_pinvoke
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_ViewMatrix_0;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_ProjectionMatrix_1;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_2;
	int32_t ___renderType_3;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___targetTexture_4;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___cameraTargetDescriptor_5;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___pixelRect_6;
	int32_t ___pixelWidth_7;
	int32_t ___pixelHeight_8;
	float ___aspectRatio_9;
	float ___renderScale_10;
	int32_t ___clearDepth_11;
	int32_t ___cameraType_12;
	int32_t ___isDefaultViewport_13;
	int32_t ___isHdrEnabled_14;
	int32_t ___requiresDepthTexture_15;
	int32_t ___requiresOpaqueTexture_16;
	int32_t ___postProcessingRequiresDepthTexture_17;
	int32_t ___xrRendering_18;
	int32_t ___defaultOpaqueSortFlags_19;
	XRPass_t0A618D61DBC9E3F8BC970B7C9D2679375C6C8A24* ___xr_20;
	int32_t ___isStereoEnabled_21;
	float ___maxShadowDistance_22;
	int32_t ___postProcessEnabled_23;
	RuntimeObject* ___captureActions_24;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___volumeLayerMask_25;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___volumeTrigger_26;
	int32_t ___isStopNaNEnabled_27;
	int32_t ___isDitheringEnabled_28;
	int32_t ___antialiasing_29;
	int32_t ___antialiasingQuality_30;
	ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* ___renderer_31;
	int32_t ___resolveFinalTarget_32;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldSpaceCameraPos_33;
};
// Native definition for COM marshalling of UnityEngine.Rendering.Universal.CameraData
struct CameraData_tC27AE109CD20677486A4AC19C0CF014AE0F50C3E_marshaled_com
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_ViewMatrix_0;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_ProjectionMatrix_1;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_2;
	int32_t ___renderType_3;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___targetTexture_4;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___cameraTargetDescriptor_5;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___pixelRect_6;
	int32_t ___pixelWidth_7;
	int32_t ___pixelHeight_8;
	float ___aspectRatio_9;
	float ___renderScale_10;
	int32_t ___clearDepth_11;
	int32_t ___cameraType_12;
	int32_t ___isDefaultViewport_13;
	int32_t ___isHdrEnabled_14;
	int32_t ___requiresDepthTexture_15;
	int32_t ___requiresOpaqueTexture_16;
	int32_t ___postProcessingRequiresDepthTexture_17;
	int32_t ___xrRendering_18;
	int32_t ___defaultOpaqueSortFlags_19;
	XRPass_t0A618D61DBC9E3F8BC970B7C9D2679375C6C8A24* ___xr_20;
	int32_t ___isStereoEnabled_21;
	float ___maxShadowDistance_22;
	int32_t ___postProcessEnabled_23;
	RuntimeObject* ___captureActions_24;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___volumeLayerMask_25;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___volumeTrigger_26;
	int32_t ___isStopNaNEnabled_27;
	int32_t ___isDitheringEnabled_28;
	int32_t ___antialiasing_29;
	int32_t ___antialiasingQuality_30;
	ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* ___renderer_31;
	int32_t ___resolveFinalTarget_32;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldSpaceCameraPos_33;
};

// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Rendering.Universal.LightData
struct LightData_t6A82F1C9AA97327A5EE9C16A3E949918F3A55470 
{
	// System.Int32 UnityEngine.Rendering.Universal.LightData::mainLightIndex
	int32_t ___mainLightIndex_0;
	// System.Int32 UnityEngine.Rendering.Universal.LightData::additionalLightsCount
	int32_t ___additionalLightsCount_1;
	// System.Int32 UnityEngine.Rendering.Universal.LightData::maxPerObjectAdditionalLightsCount
	int32_t ___maxPerObjectAdditionalLightsCount_2;
	// Unity.Collections.NativeArray`1<UnityEngine.Rendering.VisibleLight> UnityEngine.Rendering.Universal.LightData::visibleLights
	NativeArray_1_t71485A1E60B31CCAD3E525C907CF172E8B804468 ___visibleLights_3;
	// Unity.Collections.NativeArray`1<System.Int32> UnityEngine.Rendering.Universal.LightData::originalIndices
	NativeArray_1_tA833EB7E3E1C9AF82C37976AD964B8D4BAC38B2C ___originalIndices_4;
	// System.Boolean UnityEngine.Rendering.Universal.LightData::shadeAdditionalLightsPerVertex
	bool ___shadeAdditionalLightsPerVertex_5;
	// System.Boolean UnityEngine.Rendering.Universal.LightData::supportsMixedLighting
	bool ___supportsMixedLighting_6;
	// System.Boolean UnityEngine.Rendering.Universal.LightData::reflectionProbeBoxProjection
	bool ___reflectionProbeBoxProjection_7;
	// System.Boolean UnityEngine.Rendering.Universal.LightData::reflectionProbeBlending
	bool ___reflectionProbeBlending_8;
	// System.Boolean UnityEngine.Rendering.Universal.LightData::supportsLightLayers
	bool ___supportsLightLayers_9;
};
// Native definition for P/Invoke marshalling of UnityEngine.Rendering.Universal.LightData
struct LightData_t6A82F1C9AA97327A5EE9C16A3E949918F3A55470_marshaled_pinvoke
{
	int32_t ___mainLightIndex_0;
	int32_t ___additionalLightsCount_1;
	int32_t ___maxPerObjectAdditionalLightsCount_2;
	NativeArray_1_t71485A1E60B31CCAD3E525C907CF172E8B804468 ___visibleLights_3;
	NativeArray_1_tA833EB7E3E1C9AF82C37976AD964B8D4BAC38B2C ___originalIndices_4;
	int32_t ___shadeAdditionalLightsPerVertex_5;
	int32_t ___supportsMixedLighting_6;
	int32_t ___reflectionProbeBoxProjection_7;
	int32_t ___reflectionProbeBlending_8;
	int32_t ___supportsLightLayers_9;
};
// Native definition for COM marshalling of UnityEngine.Rendering.Universal.LightData
struct LightData_t6A82F1C9AA97327A5EE9C16A3E949918F3A55470_marshaled_com
{
	int32_t ___mainLightIndex_0;
	int32_t ___additionalLightsCount_1;
	int32_t ___maxPerObjectAdditionalLightsCount_2;
	NativeArray_1_t71485A1E60B31CCAD3E525C907CF172E8B804468 ___visibleLights_3;
	NativeArray_1_tA833EB7E3E1C9AF82C37976AD964B8D4BAC38B2C ___originalIndices_4;
	int32_t ___shadeAdditionalLightsPerVertex_5;
	int32_t ___supportsMixedLighting_6;
	int32_t ___reflectionProbeBoxProjection_7;
	int32_t ___reflectionProbeBlending_8;
	int32_t ___supportsLightLayers_9;
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Rendering.RenderPipelineAsset
struct RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
};

// UnityEngine.Rendering.Universal.RenderTargetHandle
struct RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66 
{
	// System.Int32 UnityEngine.Rendering.Universal.RenderTargetHandle::<id>k__BackingField
	int32_t ___U3CidU3Ek__BackingField_0;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.RenderTargetHandle::<rtid>k__BackingField
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___U3CrtidU3Ek__BackingField_1;
};

struct RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66_StaticFields
{
	// UnityEngine.Rendering.Universal.RenderTargetHandle UnityEngine.Rendering.Universal.RenderTargetHandle::CameraTarget
	RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66 ___CameraTarget_2;
};

// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Rendering.Universal.ScriptableRenderPass
struct ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0  : public RuntimeObject
{
	// UnityEngine.Rendering.Universal.RenderPassEvent UnityEngine.Rendering.Universal.ScriptableRenderPass::<renderPassEvent>k__BackingField
	int32_t ___U3CrenderPassEventU3Ek__BackingField_0;
	// UnityEngine.Rendering.RenderBufferStoreAction[] UnityEngine.Rendering.Universal.ScriptableRenderPass::m_ColorStoreActions
	RenderBufferStoreActionU5BU5D_tFEA8F5DD460573EA9F35FBEC5727D1804C5DCBF5* ___m_ColorStoreActions_1;
	// UnityEngine.Rendering.RenderBufferStoreAction UnityEngine.Rendering.Universal.ScriptableRenderPass::m_DepthStoreAction
	int32_t ___m_DepthStoreAction_2;
	// System.Boolean[] UnityEngine.Rendering.Universal.ScriptableRenderPass::m_OverriddenColorStoreActions
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___m_OverriddenColorStoreActions_3;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderPass::m_OverriddenDepthStoreAction
	bool ___m_OverriddenDepthStoreAction_4;
	// UnityEngine.Rendering.ProfilingSampler UnityEngine.Rendering.Universal.ScriptableRenderPass::<profilingSampler>k__BackingField
	ProfilingSampler_t420D4672EDB44E0EF980B31ADFD9E5747200FECE* ___U3CprofilingSamplerU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderPass::<overrideCameraTarget>k__BackingField
	bool ___U3CoverrideCameraTargetU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderPass::<isBlitRenderPass>k__BackingField
	bool ___U3CisBlitRenderPassU3Ek__BackingField_7;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderPass::<useNativeRenderPass>k__BackingField
	bool ___U3CuseNativeRenderPassU3Ek__BackingField_8;
	// System.Int32 UnityEngine.Rendering.Universal.ScriptableRenderPass::<renderTargetWidth>k__BackingField
	int32_t ___U3CrenderTargetWidthU3Ek__BackingField_9;
	// System.Int32 UnityEngine.Rendering.Universal.ScriptableRenderPass::<renderTargetHeight>k__BackingField
	int32_t ___U3CrenderTargetHeightU3Ek__BackingField_10;
	// System.Int32 UnityEngine.Rendering.Universal.ScriptableRenderPass::<renderTargetSampleCount>k__BackingField
	int32_t ___U3CrenderTargetSampleCountU3Ek__BackingField_11;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderPass::<depthOnly>k__BackingField
	bool ___U3CdepthOnlyU3Ek__BackingField_12;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderPass::<isLastPass>k__BackingField
	bool ___U3CisLastPassU3Ek__BackingField_13;
	// System.Int32 UnityEngine.Rendering.Universal.ScriptableRenderPass::<renderPassQueueIndex>k__BackingField
	int32_t ___U3CrenderPassQueueIndexU3Ek__BackingField_14;
	// Unity.Collections.NativeArray`1<System.Int32> UnityEngine.Rendering.Universal.ScriptableRenderPass::m_ColorAttachmentIndices
	NativeArray_1_tA833EB7E3E1C9AF82C37976AD964B8D4BAC38B2C ___m_ColorAttachmentIndices_15;
	// Unity.Collections.NativeArray`1<System.Int32> UnityEngine.Rendering.Universal.ScriptableRenderPass::m_InputAttachmentIndices
	NativeArray_1_tA833EB7E3E1C9AF82C37976AD964B8D4BAC38B2C ___m_InputAttachmentIndices_16;
	// UnityEngine.Experimental.Rendering.GraphicsFormat[] UnityEngine.Rendering.Universal.ScriptableRenderPass::<renderTargetFormat>k__BackingField
	GraphicsFormatU5BU5D_tF6A3D90C430FA3F548B77E5D58D25D71F154E6C5* ___U3CrenderTargetFormatU3Ek__BackingField_17;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.Universal.ScriptableRenderPass::m_ColorAttachments
	RenderTargetIdentifierU5BU5D_t179798C153B7CE381B41C57863F98CB24023C4CE* ___m_ColorAttachments_18;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.Universal.ScriptableRenderPass::m_InputAttachments
	RenderTargetIdentifierU5BU5D_t179798C153B7CE381B41C57863F98CB24023C4CE* ___m_InputAttachments_19;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.ScriptableRenderPass::m_DepthAttachment
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_DepthAttachment_20;
	// UnityEngine.Rendering.Universal.ScriptableRenderPassInput UnityEngine.Rendering.Universal.ScriptableRenderPass::m_Input
	int32_t ___m_Input_21;
	// UnityEngine.Rendering.ClearFlag UnityEngine.Rendering.Universal.ScriptableRenderPass::m_ClearFlag
	int32_t ___m_ClearFlag_22;
	// UnityEngine.Color UnityEngine.Rendering.Universal.ScriptableRenderPass::m_ClearColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_ClearColor_23;
};

// UnityEngine.Rendering.Universal.ScriptableRendererFeature
struct ScriptableRendererFeature_tF2ED08AD2498105378D4DAB0386A0F8A144EF4C6  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRendererFeature::m_Active
	bool ___m_Active_4;
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// BNG.PlayerTeleport/OnAfterTeleportAction
struct OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808  : public MulticastDelegate_t
{
};

// BNG.PlayerTeleport/OnBeforeTeleportAction
struct OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8  : public MulticastDelegate_t
{
};

// UnityEngine.AudioBehaviour
struct AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// DrunkMan.DrunkManFeature
struct DrunkManFeature_tFED478401670ECFD6FF972AA05597D399B655C11  : public ScriptableRendererFeature_tF2ED08AD2498105378D4DAB0386A0F8A144EF4C6
{
	// StatsManager DrunkMan.DrunkManFeature::sm
	StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* ___sm_5;
	// UnityEngine.GameObject DrunkMan.DrunkManFeature::manager
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___manager_6;
	// System.Single DrunkMan.DrunkManFeature::abv
	float ___abv_7;
	// System.Boolean DrunkMan.DrunkManFeature::m_Enable
	bool ___m_Enable_8;
	// DrunkMan.DrunkManFeature/Settings DrunkMan.DrunkManFeature::m_Settings
	Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* ___m_Settings_9;
	// DrunkMan.DrunkManFeature/BlitPass DrunkMan.DrunkManFeature::m_BlitPass
	BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* ___m_BlitPass_10;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.Rendering.Universal.RenderingData
struct RenderingData_tAAA01190551D6D5954314E3E1E85B58DD45EED71 
{
	// UnityEngine.Rendering.CullingResults UnityEngine.Rendering.Universal.RenderingData::cullResults
	CullingResults_tD6B7EF20B68D47DFF3A99EB2EA73F47F1D460267 ___cullResults_0;
	// UnityEngine.Rendering.Universal.CameraData UnityEngine.Rendering.Universal.RenderingData::cameraData
	CameraData_tC27AE109CD20677486A4AC19C0CF014AE0F50C3E ___cameraData_1;
	// UnityEngine.Rendering.Universal.LightData UnityEngine.Rendering.Universal.RenderingData::lightData
	LightData_t6A82F1C9AA97327A5EE9C16A3E949918F3A55470 ___lightData_2;
	// UnityEngine.Rendering.Universal.ShadowData UnityEngine.Rendering.Universal.RenderingData::shadowData
	ShadowData_tA165FDF7CA4CE6BEA8B649FFAB91C59ED684D832 ___shadowData_3;
	// UnityEngine.Rendering.Universal.PostProcessingData UnityEngine.Rendering.Universal.RenderingData::postProcessingData
	PostProcessingData_tFA75BF22951C600258B2707AF7A113E4EDA49BD4 ___postProcessingData_4;
	// System.Boolean UnityEngine.Rendering.Universal.RenderingData::supportsDynamicBatching
	bool ___supportsDynamicBatching_5;
	// UnityEngine.Rendering.PerObjectData UnityEngine.Rendering.Universal.RenderingData::perObjectData
	int32_t ___perObjectData_6;
	// System.Boolean UnityEngine.Rendering.Universal.RenderingData::postProcessingEnabled
	bool ___postProcessingEnabled_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.Rendering.Universal.RenderingData
struct RenderingData_tAAA01190551D6D5954314E3E1E85B58DD45EED71_marshaled_pinvoke
{
	CullingResults_tD6B7EF20B68D47DFF3A99EB2EA73F47F1D460267 ___cullResults_0;
	CameraData_tC27AE109CD20677486A4AC19C0CF014AE0F50C3E_marshaled_pinvoke ___cameraData_1;
	LightData_t6A82F1C9AA97327A5EE9C16A3E949918F3A55470_marshaled_pinvoke ___lightData_2;
	ShadowData_tA165FDF7CA4CE6BEA8B649FFAB91C59ED684D832_marshaled_pinvoke ___shadowData_3;
	PostProcessingData_tFA75BF22951C600258B2707AF7A113E4EDA49BD4_marshaled_pinvoke ___postProcessingData_4;
	int32_t ___supportsDynamicBatching_5;
	int32_t ___perObjectData_6;
	int32_t ___postProcessingEnabled_7;
};
// Native definition for COM marshalling of UnityEngine.Rendering.Universal.RenderingData
struct RenderingData_tAAA01190551D6D5954314E3E1E85B58DD45EED71_marshaled_com
{
	CullingResults_tD6B7EF20B68D47DFF3A99EB2EA73F47F1D460267 ___cullResults_0;
	CameraData_tC27AE109CD20677486A4AC19C0CF014AE0F50C3E_marshaled_com ___cameraData_1;
	LightData_t6A82F1C9AA97327A5EE9C16A3E949918F3A55470_marshaled_com ___lightData_2;
	ShadowData_tA165FDF7CA4CE6BEA8B649FFAB91C59ED684D832_marshaled_com ___shadowData_3;
	PostProcessingData_tFA75BF22951C600258B2707AF7A113E4EDA49BD4_marshaled_com ___postProcessingData_4;
	int32_t ___supportsDynamicBatching_5;
	int32_t ___perObjectData_6;
	int32_t ___postProcessingEnabled_7;
};

// UnityEngine.Rendering.Universal.ScriptableRenderer
struct ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892  : public RuntimeObject
{
	// System.Int32 UnityEngine.Rendering.Universal.ScriptableRenderer::m_LastBeginSubpassPassIndex
	int32_t ___m_LastBeginSubpassPassIndex_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Int32[]> UnityEngine.Rendering.Universal.ScriptableRenderer::m_MergeableRenderPassesMap
	Dictionary_2_tCB9019887EB0254D4745B0724BC12327C5B63792* ___m_MergeableRenderPassesMap_3;
	// System.Int32[][] UnityEngine.Rendering.Universal.ScriptableRenderer::m_MergeableRenderPassesMapArrays
	Int32U5BU5DU5BU5D_t179D865D5B30EFCBC50F82C9774329C15943466E* ___m_MergeableRenderPassesMapArrays_4;
	// UnityEngine.Hash128[] UnityEngine.Rendering.Universal.ScriptableRenderer::m_PassIndexToPassHash
	Hash128U5BU5D_tB104E7247B842648E447B7FCF4748077DC1F8C98* ___m_PassIndexToPassHash_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Int32> UnityEngine.Rendering.Universal.ScriptableRenderer::m_RenderPassesAttachmentCount
	Dictionary_2_tB41FAC88F07BAB98D6D373F7C94FB0496D1BDA32* ___m_RenderPassesAttachmentCount_6;
	// UnityEngine.Rendering.AttachmentDescriptor[] UnityEngine.Rendering.Universal.ScriptableRenderer::m_ActiveColorAttachmentDescriptors
	AttachmentDescriptorU5BU5D_tC70107EBD955FE94BA31C7FDC146069EF9C547C1* ___m_ActiveColorAttachmentDescriptors_7;
	// UnityEngine.Rendering.AttachmentDescriptor UnityEngine.Rendering.Universal.ScriptableRenderer::m_ActiveDepthAttachmentDescriptor
	AttachmentDescriptor_tBAC9B26B50BB0838C5C0CC22BB296F9DFF41276E ___m_ActiveDepthAttachmentDescriptor_8;
	// UnityEngine.Rendering.RenderBufferStoreAction[] UnityEngine.Rendering.Universal.ScriptableRenderer::m_FinalColorStoreAction
	RenderBufferStoreActionU5BU5D_tFEA8F5DD460573EA9F35FBEC5727D1804C5DCBF5* ___m_FinalColorStoreAction_9;
	// UnityEngine.Rendering.RenderBufferStoreAction UnityEngine.Rendering.Universal.ScriptableRenderer::m_FinalDepthStoreAction
	int32_t ___m_FinalDepthStoreAction_10;
	// UnityEngine.Rendering.ProfilingSampler UnityEngine.Rendering.Universal.ScriptableRenderer::<profilingExecute>k__BackingField
	ProfilingSampler_t420D4672EDB44E0EF980B31ADFD9E5747200FECE* ___U3CprofilingExecuteU3Ek__BackingField_11;
	// UnityEngine.Rendering.Universal.DebugHandler UnityEngine.Rendering.Universal.ScriptableRenderer::<DebugHandler>k__BackingField
	DebugHandler_t3A09E2CFD1CA6F5C192968A6FF19EE4863F44DA4* ___U3CDebugHandlerU3Ek__BackingField_12;
	// UnityEngine.Rendering.Universal.ScriptableRenderer/RenderingFeatures UnityEngine.Rendering.Universal.ScriptableRenderer::<supportedRenderingFeatures>k__BackingField
	RenderingFeatures_t31044CBDCDC2F05194BFA2A2122FBD937D78A371* ___U3CsupportedRenderingFeaturesU3Ek__BackingField_14;
	// UnityEngine.Rendering.GraphicsDeviceType[] UnityEngine.Rendering.Universal.ScriptableRenderer::<unsupportedGraphicsDeviceTypes>k__BackingField
	GraphicsDeviceTypeU5BU5D_t4322565F239068C66BF47053B83BD6A9A9D16408* ___U3CunsupportedGraphicsDeviceTypesU3Ek__BackingField_15;
	// UnityEngine.Rendering.Universal.StoreActionsOptimization UnityEngine.Rendering.Universal.ScriptableRenderer::m_StoreActionsOptimizationSetting
	int32_t ___m_StoreActionsOptimizationSetting_16;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.Universal.ScriptableRenderPass> UnityEngine.Rendering.Universal.ScriptableRenderer::m_ActiveRenderPassQueue
	List_1_t2E485E650BF1E41358CE56A69323E183C5A89CB6* ___m_ActiveRenderPassQueue_19;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.Universal.ScriptableRendererFeature> UnityEngine.Rendering.Universal.ScriptableRenderer::m_RendererFeatures
	List_1_t2121653FB628940E808D105AD2C17E0F20AFB3A6* ___m_RendererFeatures_20;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.ScriptableRenderer::m_CameraColorTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_CameraColorTarget_21;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.ScriptableRenderer::m_CameraDepthTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_CameraDepthTarget_22;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.ScriptableRenderer::m_CameraResolveTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_CameraResolveTarget_23;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::m_FirstTimeCameraColorTargetIsBound
	bool ___m_FirstTimeCameraColorTargetIsBound_24;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::m_FirstTimeCameraDepthTargetIsBound
	bool ___m_FirstTimeCameraDepthTargetIsBound_25;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::m_IsPipelineExecuting
	bool ___m_IsPipelineExecuting_26;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::isCameraColorTargetValid
	bool ___isCameraColorTargetValid_27;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::disableNativeRenderPassInFeatures
	bool ___disableNativeRenderPassInFeatures_28;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::useRenderPassEnabled
	bool ___useRenderPassEnabled_29;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::<useDepthPriming>k__BackingField
	bool ___U3CuseDepthPrimingU3Ek__BackingField_37;
};

struct ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892_StaticFields
{
	// UnityEngine.Rendering.Universal.ScriptableRenderer UnityEngine.Rendering.Universal.ScriptableRenderer::current
	ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* ___current_13;
	// System.Boolean UnityEngine.Rendering.Universal.ScriptableRenderer::m_UseOptimizedStoreActions
	bool ___m_UseOptimizedStoreActions_17;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.Universal.ScriptableRenderer::m_ActiveColorAttachments
	RenderTargetIdentifierU5BU5D_t179798C153B7CE381B41C57863F98CB24023C4CE* ___m_ActiveColorAttachments_30;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.ScriptableRenderer::m_ActiveDepthAttachment
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_ActiveDepthAttachment_31;
	// UnityEngine.Rendering.RenderBufferStoreAction[] UnityEngine.Rendering.Universal.ScriptableRenderer::m_ActiveColorStoreActions
	RenderBufferStoreActionU5BU5D_tFEA8F5DD460573EA9F35FBEC5727D1804C5DCBF5* ___m_ActiveColorStoreActions_32;
	// UnityEngine.Rendering.RenderBufferStoreAction UnityEngine.Rendering.Universal.ScriptableRenderer::m_ActiveDepthStoreAction
	int32_t ___m_ActiveDepthStoreAction_33;
	// UnityEngine.Rendering.RenderTargetIdentifier[][] UnityEngine.Rendering.Universal.ScriptableRenderer::m_TrimmedColorAttachmentCopies
	RenderTargetIdentifierU5BU5DU5BU5D_tDB35F8D017FE3AD8BB35E08E323074D47C5A10BB* ___m_TrimmedColorAttachmentCopies_34;
	// UnityEngine.Plane[] UnityEngine.Rendering.Universal.ScriptableRenderer::s_Planes
	PlaneU5BU5D_t4EEF66BAA8B0140EFFF34F6183CE7F80546592BE* ___s_Planes_35;
	// UnityEngine.Vector4[] UnityEngine.Rendering.Universal.ScriptableRenderer::s_VectorPlanes
	Vector4U5BU5D_tC0F3A7115F85007510F6D173968200CD31BCF7AD* ___s_VectorPlanes_36;
};

// DrunkMan.DrunkManFeature/BlitPass
struct BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8  : public ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0
{
	// UnityEngine.Material DrunkMan.DrunkManFeature/BlitPass::m_Mat
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Mat_24;
	// UnityEngine.Rendering.RenderTargetIdentifier DrunkMan.DrunkManFeature/BlitPass::m_Source
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___m_Source_25;
	// UnityEngine.Rendering.Universal.RenderTargetHandle DrunkMan.DrunkManFeature/BlitPass::m_RtHandle1
	RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66 ___m_RtHandle1_26;
	// UnityEngine.Rendering.Universal.RenderTargetHandle DrunkMan.DrunkManFeature/BlitPass::m_RtHandle2
	RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66 ___m_RtHandle2_27;
	// System.String DrunkMan.DrunkManFeature/BlitPass::m_Tag
	String_t* ___m_Tag_28;
	// System.Boolean DrunkMan.DrunkManFeature/BlitPass::m_SleepyEye
	bool ___m_SleepyEye_29;
	// DrunkMan.DrunkManFeature/EType DrunkMan.DrunkManFeature/BlitPass::m_Type
	int32_t ___m_Type_30;
	// System.Single DrunkMan.DrunkManFeature/BlitPass::m_Intensity
	float ___m_Intensity_31;
};

// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299  : public AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941
{
};

// DrunkMan.AutoRotate
struct AutoRotate_tC91AFA738AEA5EC3E037D7C011A3070113E0FE49  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single DrunkMan.AutoRotate::m_Speed
	float ___m_Speed_4;
};

// DrunkMan.BuiltIn.Demo
struct Demo_tB2CE2F3309296F5900575784169917681D1E7D28  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Material DrunkMan.BuiltIn.Demo::m_Mat
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Mat_4;
	// DrunkMan.BuiltIn.Demo/EType DrunkMan.BuiltIn.Demo::m_Type
	int32_t ___m_Type_5;
	// System.Single DrunkMan.BuiltIn.Demo::m_DrunkIntensity
	float ___m_DrunkIntensity_6;
	// System.Single DrunkMan.BuiltIn.Demo::m_RGBShiftFactor
	float ___m_RGBShiftFactor_7;
	// System.Single DrunkMan.BuiltIn.Demo::m_RGBShiftPower
	float ___m_RGBShiftPower_8;
	// System.Single DrunkMan.BuiltIn.Demo::m_GhostSeeRadius
	float ___m_GhostSeeRadius_9;
	// System.Single DrunkMan.BuiltIn.Demo::m_GhostSeeMix
	float ___m_GhostSeeMix_10;
	// System.Single DrunkMan.BuiltIn.Demo::m_GhostSeeAmplitude
	float ___m_GhostSeeAmplitude_11;
	// System.Single DrunkMan.BuiltIn.Demo::m_Frequency
	float ___m_Frequency_12;
	// System.Single DrunkMan.BuiltIn.Demo::m_Period
	float ___m_Period_13;
	// System.Single DrunkMan.BuiltIn.Demo::m_Amplitude
	float ___m_Amplitude_14;
	// System.Single DrunkMan.BuiltIn.Demo::m_BlurMin
	float ___m_BlurMin_15;
	// System.Single DrunkMan.BuiltIn.Demo::m_BlurMax
	float ___m_BlurMax_16;
	// System.Single DrunkMan.BuiltIn.Demo::m_BlurSpeed
	float ___m_BlurSpeed_17;
	// System.Boolean DrunkMan.BuiltIn.Demo::m_SleepyEye
	bool ___m_SleepyEye_18;
	// System.Single DrunkMan.BuiltIn.Demo::m_EyeClose
	float ___m_EyeClose_19;
};

// DrunkMan.FreeCamera
struct FreeCamera_t17D2C98F0E4AE04AA5D7713B35EA0230081BC088  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Rendering.RenderPipelineAsset DrunkMan.FreeCamera::m_Pipeline
	RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E* ___m_Pipeline_4;
	// System.Single DrunkMan.FreeCamera::m_MoveSpeed
	float ___m_MoveSpeed_5;
	// System.Single DrunkMan.FreeCamera::m_RotateSpeed
	float ___m_RotateSpeed_6;
	// UnityEngine.KeyCode DrunkMan.FreeCamera::m_ForwardButton
	int32_t ___m_ForwardButton_7;
	// UnityEngine.KeyCode DrunkMan.FreeCamera::m_BackwardButton
	int32_t ___m_BackwardButton_8;
	// UnityEngine.KeyCode DrunkMan.FreeCamera::m_RightButton
	int32_t ___m_RightButton_9;
	// UnityEngine.KeyCode DrunkMan.FreeCamera::m_LeftButton
	int32_t ___m_LeftButton_10;
	// UnityEngine.KeyCode DrunkMan.FreeCamera::m_UpButton
	int32_t ___m_UpButton_11;
	// UnityEngine.KeyCode DrunkMan.FreeCamera::m_DownButton
	int32_t ___m_DownButton_12;
};

// BNG.Grabbable
struct Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean BNG.Grabbable::BeingHeld
	bool ___BeingHeld_4;
	// System.Collections.Generic.List`1<BNG.Grabber> BNG.Grabbable::validGrabbers
	List_1_tEE63DC26EA202668033FEBEA09A3506D65E2507D* ___validGrabbers_5;
	// System.Collections.Generic.List`1<BNG.Grabber> BNG.Grabbable::heldByGrabbers
	List_1_tEE63DC26EA202668033FEBEA09A3506D65E2507D* ___heldByGrabbers_6;
	// System.Boolean BNG.Grabbable::wasKinematic
	bool ___wasKinematic_7;
	// System.Boolean BNG.Grabbable::usedGravity
	bool ___usedGravity_8;
	// UnityEngine.CollisionDetectionMode BNG.Grabbable::initialCollisionMode
	int32_t ___initialCollisionMode_9;
	// UnityEngine.RigidbodyInterpolation BNG.Grabbable::initialInterpolationMode
	int32_t ___initialInterpolationMode_10;
	// System.Boolean BNG.Grabbable::remoteGrabbing
	bool ___remoteGrabbing_11;
	// BNG.GrabButton BNG.Grabbable::GrabButton
	int32_t ___GrabButton_12;
	// BNG.HoldType BNG.Grabbable::Grabtype
	int32_t ___Grabtype_13;
	// BNG.GrabPhysics BNG.Grabbable::GrabPhysics
	int32_t ___GrabPhysics_14;
	// BNG.GrabType BNG.Grabbable::GrabMechanic
	int32_t ___GrabMechanic_15;
	// System.Single BNG.Grabbable::GrabSpeed
	float ___GrabSpeed_16;
	// System.Boolean BNG.Grabbable::RemoteGrabbable
	bool ___RemoteGrabbable_17;
	// BNG.RemoteGrabMovement BNG.Grabbable::RemoteGrabMechanic
	int32_t ___RemoteGrabMechanic_18;
	// System.Single BNG.Grabbable::RemoteGrabDistance
	float ___RemoteGrabDistance_19;
	// System.Single BNG.Grabbable::ThrowForceMultiplier
	float ___ThrowForceMultiplier_20;
	// System.Single BNG.Grabbable::ThrowForceMultiplierAngular
	float ___ThrowForceMultiplierAngular_21;
	// System.Single BNG.Grabbable::BreakDistance
	float ___BreakDistance_22;
	// System.Boolean BNG.Grabbable::HideHandGraphics
	bool ___HideHandGraphics_23;
	// System.Boolean BNG.Grabbable::ParentToHands
	bool ___ParentToHands_24;
	// System.Boolean BNG.Grabbable::ParentHandModel
	bool ___ParentHandModel_25;
	// System.Boolean BNG.Grabbable::SnapHandModel
	bool ___SnapHandModel_26;
	// System.Boolean BNG.Grabbable::CanBeDropped
	bool ___CanBeDropped_27;
	// System.Boolean BNG.Grabbable::CanBeSnappedToSnapZone
	bool ___CanBeSnappedToSnapZone_28;
	// System.Boolean BNG.Grabbable::ForceDisableKinematicOnDrop
	bool ___ForceDisableKinematicOnDrop_29;
	// System.Boolean BNG.Grabbable::InstantMovement
	bool ___InstantMovement_30;
	// System.Boolean BNG.Grabbable::MakeChildCollidersGrabbable
	bool ___MakeChildCollidersGrabbable_31;
	// BNG.HandPoseType BNG.Grabbable::handPoseType
	int32_t ___handPoseType_32;
	// BNG.HandPoseType BNG.Grabbable::initialHandPoseType
	int32_t ___initialHandPoseType_33;
	// BNG.HandPose BNG.Grabbable::SelectedHandPose
	HandPose_tD971C9E16E48BF9C54143941B3D05026720345BB* ___SelectedHandPose_34;
	// BNG.HandPose BNG.Grabbable::initialHandPose
	HandPose_tD971C9E16E48BF9C54143941B3D05026720345BB* ___initialHandPose_35;
	// BNG.HandPoseId BNG.Grabbable::CustomHandPose
	int32_t ___CustomHandPose_36;
	// BNG.HandPoseId BNG.Grabbable::initialHandPoseId
	int32_t ___initialHandPoseId_37;
	// BNG.OtherGrabBehavior BNG.Grabbable::SecondaryGrabBehavior
	int32_t ___SecondaryGrabBehavior_38;
	// BNG.TwoHandedPositionType BNG.Grabbable::TwoHandedPosition
	int32_t ___TwoHandedPosition_39;
	// System.Single BNG.Grabbable::TwoHandedPostionLerpAmount
	float ___TwoHandedPostionLerpAmount_40;
	// BNG.TwoHandedRotationType BNG.Grabbable::TwoHandedRotation
	int32_t ___TwoHandedRotation_41;
	// System.Single BNG.Grabbable::TwoHandedRotationLerpAmount
	float ___TwoHandedRotationLerpAmount_42;
	// BNG.TwoHandedDropMechanic BNG.Grabbable::TwoHandedDropBehavior
	int32_t ___TwoHandedDropBehavior_43;
	// BNG.TwoHandedLookDirection BNG.Grabbable::TwoHandedLookVector
	int32_t ___TwoHandedLookVector_44;
	// System.Single BNG.Grabbable::SecondHandLookSpeed
	float ___SecondHandLookSpeed_45;
	// BNG.Grabbable BNG.Grabbable::SecondaryGrabbable
	Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* ___SecondaryGrabbable_46;
	// BNG.Grabbable BNG.Grabbable::OtherGrabbableMustBeGrabbed
	Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* ___OtherGrabbableMustBeGrabbed_47;
	// System.Single BNG.Grabbable::CollisionSpring
	float ___CollisionSpring_48;
	// System.Single BNG.Grabbable::CollisionSlerp
	float ___CollisionSlerp_49;
	// UnityEngine.ConfigurableJointMotion BNG.Grabbable::CollisionLinearMotionX
	int32_t ___CollisionLinearMotionX_50;
	// UnityEngine.ConfigurableJointMotion BNG.Grabbable::CollisionLinearMotionY
	int32_t ___CollisionLinearMotionY_51;
	// UnityEngine.ConfigurableJointMotion BNG.Grabbable::CollisionLinearMotionZ
	int32_t ___CollisionLinearMotionZ_52;
	// UnityEngine.ConfigurableJointMotion BNG.Grabbable::CollisionAngularMotionX
	int32_t ___CollisionAngularMotionX_53;
	// UnityEngine.ConfigurableJointMotion BNG.Grabbable::CollisionAngularMotionY
	int32_t ___CollisionAngularMotionY_54;
	// UnityEngine.ConfigurableJointMotion BNG.Grabbable::CollisionAngularMotionZ
	int32_t ___CollisionAngularMotionZ_55;
	// System.Boolean BNG.Grabbable::ApplyCorrectiveForce
	bool ___ApplyCorrectiveForce_56;
	// System.Single BNG.Grabbable::MoveVelocityForce
	float ___MoveVelocityForce_57;
	// System.Single BNG.Grabbable::MoveAngularVelocityForce
	float ___MoveAngularVelocityForce_58;
	// System.Single BNG.Grabbable::LastGrabTime
	float ___LastGrabTime_59;
	// System.Single BNG.Grabbable::LastDropTime
	float ___LastDropTime_60;
	// System.Boolean BNG.Grabbable::AddControllerVelocityOnDrop
	bool ___AddControllerVelocityOnDrop_61;
	// System.Single BNG.Grabbable::journeyLength
	float ___journeyLength_62;
	// UnityEngine.Vector3 BNG.Grabbable::<OriginalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3COriginalScaleU3Ek__BackingField_63;
	// System.Collections.Generic.List`1<UnityEngine.Collider> BNG.Grabbable::collisions
	List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* ___collisions_64;
	// System.Single BNG.Grabbable::<lastCollisionSeconds>k__BackingField
	float ___U3ClastCollisionSecondsU3Ek__BackingField_65;
	// System.Single BNG.Grabbable::<lastNoCollisionSeconds>k__BackingField
	float ___U3ClastNoCollisionSecondsU3Ek__BackingField_66;
	// System.Single BNG.Grabbable::<requestSpringTime>k__BackingField
	float ___U3CrequestSpringTimeU3Ek__BackingField_67;
	// UnityEngine.Transform BNG.Grabbable::primaryGrabOffset
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___primaryGrabOffset_68;
	// UnityEngine.Transform BNG.Grabbable::secondaryGrabOffset
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___secondaryGrabOffset_69;
	// BNG.GrabPoint BNG.Grabbable::ActiveGrabPoint
	GrabPoint_tA5EE4F3317CACF01DFEEF4ADCA052D6DD1568F6F* ___ActiveGrabPoint_70;
	// UnityEngine.Vector3 BNG.Grabbable::SecondaryLookOffset
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___SecondaryLookOffset_71;
	// UnityEngine.Transform BNG.Grabbable::SecondaryLookAtTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___SecondaryLookAtTransform_72;
	// UnityEngine.Transform BNG.Grabbable::LocalOffsetTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___LocalOffsetTransform_73;
	// UnityEngine.Transform BNG.Grabbable::_grabTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ____grabTransform_74;
	// UnityEngine.Transform BNG.Grabbable::_grabTransformSecondary
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ____grabTransformSecondary_75;
	// System.Collections.Generic.List`1<UnityEngine.Transform> BNG.Grabbable::GrabPoints
	List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* ___GrabPoints_76;
	// System.Boolean BNG.Grabbable::_canBeMoved
	bool ____canBeMoved_77;
	// UnityEngine.Transform BNG.Grabbable::originalParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___originalParent_78;
	// BNG.InputBridge BNG.Grabbable::input
	InputBridge_tA0CD5FB01685872EBB8C87A5C5FEAB0DDC483A17* ___input_79;
	// UnityEngine.ConfigurableJoint BNG.Grabbable::connectedJoint
	ConfigurableJoint_t8B33AB5A6B8D52493F14C4B8DBDF78A1C94ECB77* ___connectedJoint_80;
	// UnityEngine.Vector3 BNG.Grabbable::previousPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___previousPosition_81;
	// System.Single BNG.Grabbable::lastItemTeleportTime
	float ___lastItemTeleportTime_82;
	// System.Boolean BNG.Grabbable::recentlyTeleported
	bool ___recentlyTeleported_83;
	// System.Boolean BNG.Grabbable::UseCustomInspector
	bool ___UseCustomInspector_84;
	// BNG.BNGPlayerController BNG.Grabbable::_player
	BNGPlayerController_t9093D731F0BBC6A4DA623F3127D95A13803945E4* ____player_85;
	// UnityEngine.Collider BNG.Grabbable::col
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___col_86;
	// UnityEngine.Rigidbody BNG.Grabbable::rigid
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___rigid_87;
	// BNG.Grabber BNG.Grabbable::flyingTo
	Grabber_tCCBBC63D0FECBDD434CE54930135C6A1D5C22B98* ___flyingTo_88;
	// System.Collections.Generic.List`1<BNG.GrabbableEvents> BNG.Grabbable::events
	List_1_tE6D1ED582321C7A7F69E8175188B1EB838421F2A* ___events_89;
	// System.Boolean BNG.Grabbable::didParentHands
	bool ___didParentHands_90;
	// System.Boolean BNG.Grabbable::initiatedFlick
	bool ___initiatedFlick_91;
	// System.Single BNG.Grabbable::flickStartVelocity
	float ___flickStartVelocity_92;
	// System.Single BNG.Grabbable::FlickSpeed
	float ___FlickSpeed_93;
	// System.Single BNG.Grabbable::lastFlickTime
	float ___lastFlickTime_94;
	// System.Single BNG.Grabbable::FlickForce
	float ___FlickForce_95;
	// System.Single BNG.Grabbable::angle
	float ___angle_96;
	// UnityEngine.Vector3 BNG.Grabbable::axis
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___axis_97;
	// UnityEngine.Vector3 BNG.Grabbable::angularTarget
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___angularTarget_98;
	// UnityEngine.Vector3 BNG.Grabbable::angularMovement
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___angularMovement_99;
	// System.Boolean BNG.Grabbable::subscribedToEvents
	bool ___subscribedToEvents_100;
	// System.Boolean BNG.Grabbable::grabbableIsLocked
	bool ___grabbableIsLocked_101;
	// UnityEngine.Transform BNG.Grabbable::_priorParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ____priorParent_102;
	// UnityEngine.Vector3 BNG.Grabbable::_priorLocalOffsetPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____priorLocalOffsetPosition_103;
	// UnityEngine.Quaternion BNG.Grabbable::_priorLocalOffsetRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ____priorLocalOffsetRotation_104;
	// BNG.Grabber BNG.Grabbable::_priorPrimaryGrabber
	Grabber_tCCBBC63D0FECBDD434CE54930135C6A1D5C22B98* ____priorPrimaryGrabber_105;
	// System.Boolean BNG.Grabbable::lockPos
	bool ___lockPos_106;
	// System.Boolean BNG.Grabbable::lockRot
	bool ___lockRot_107;
	// System.Int32 BNG.Grabbable::lockRequests
	int32_t ___lockRequests_108;
	// System.Boolean BNG.Grabbable::quitting
	bool ___quitting_109;
};

// BNG.GrabbableEvents
struct GrabbableEvents_t41DE687F96D29CBCA222FCBA34DE0134B5D87FF6  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// BNG.Grabbable BNG.GrabbableEvents::grab
	Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* ___grab_4;
	// BNG.Grabber BNG.GrabbableEvents::thisGrabber
	Grabber_tCCBBC63D0FECBDD434CE54930135C6A1D5C22B98* ___thisGrabber_5;
	// BNG.InputBridge BNG.GrabbableEvents::input
	InputBridge_tA0CD5FB01685872EBB8C87A5C5FEAB0DDC483A17* ___input_6;
};

// StatsManager
struct StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 StatsManager::currentCash
	int32_t ___currentCash_4;
	// System.Int32 StatsManager::earnedCash
	int32_t ___earnedCash_5;
	// System.Int32 StatsManager::zombiesKilled
	int32_t ___zombiesKilled_6;
	// System.Single StatsManager::abv
	float ___abv_7;
	// UnityEngine.UI.Text StatsManager::abvCounter
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___abvCounter_8;
	// UnityEngine.UI.Text StatsManager::textCurrentCash
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textCurrentCash_9;
	// UnityEngine.UI.Text StatsManager::textEarnedCash
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textEarnedCash_10;
	// UnityEngine.UI.Text StatsManager::textZombiesKilled
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textZombiesKilled_11;
};

// BNG.VRUtils
struct VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Color BNG.VRUtils::LogTextColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___LogTextColor_5;
	// UnityEngine.Color BNG.VRUtils::WarnTextColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___WarnTextColor_6;
	// UnityEngine.Color BNG.VRUtils::ErrTextColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___ErrTextColor_7;
	// UnityEngine.Transform BNG.VRUtils::DebugTextHolder
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___DebugTextHolder_8;
	// System.Single BNG.VRUtils::MaxTextEntries
	float ___MaxTextEntries_9;
	// System.String BNG.VRUtils::LastDebugMsg
	String_t* ___LastDebugMsg_10;
	// System.Int32 BNG.VRUtils::lastDebugMsgCount
	int32_t ___lastDebugMsgCount_11;
};

struct VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469_StaticFields
{
	// BNG.VRUtils BNG.VRUtils::_instance
	VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469* ____instance_4;
};

// BNG.WeaponSlide
struct WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single BNG.WeaponSlide::MinLocalZ
	float ___MinLocalZ_4;
	// System.Single BNG.WeaponSlide::MaxLocalZ
	float ___MaxLocalZ_5;
	// System.Boolean BNG.WeaponSlide::slidingBack
	bool ___slidingBack_6;
	// System.Boolean BNG.WeaponSlide::LockedBack
	bool ___LockedBack_7;
	// UnityEngine.AudioClip BNG.WeaponSlide::SlideReleaseSound
	AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20* ___SlideReleaseSound_8;
	// UnityEngine.AudioClip BNG.WeaponSlide::LockedBackSound
	AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20* ___LockedBackSound_9;
	// System.Boolean BNG.WeaponSlide::ZeroMassWhenNotHeld
	bool ___ZeroMassWhenNotHeld_10;
	// BNG.RaycastWeapon BNG.WeaponSlide::parentWeapon
	RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* ___parentWeapon_11;
	// BNG.Grabbable BNG.WeaponSlide::parentGrabbable
	Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* ___parentGrabbable_12;
	// UnityEngine.Vector3 BNG.WeaponSlide::initialLocalPos
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___initialLocalPos_13;
	// BNG.Grabbable BNG.WeaponSlide::thisGrabbable
	Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* ___thisGrabbable_14;
	// UnityEngine.AudioSource BNG.WeaponSlide::audioSource
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___audioSource_15;
	// UnityEngine.Rigidbody BNG.WeaponSlide::rigid
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___rigid_16;
	// System.Single BNG.WeaponSlide::initialMass
	float ___initialMass_17;
	// UnityEngine.Vector3 BNG.WeaponSlide::_lockPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____lockPosition_18;
	// System.Boolean BNG.WeaponSlide::lockSlidePosition
	bool ___lockSlidePosition_19;
};

// BNG.RaycastWeapon
struct RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160  : public GrabbableEvents_t41DE687F96D29CBCA222FCBA34DE0134B5D87FF6
{
	// System.Single BNG.RaycastWeapon::MaxRange
	float ___MaxRange_7;
	// System.Single BNG.RaycastWeapon::Damage
	float ___Damage_8;
	// BNG.FiringType BNG.RaycastWeapon::FiringMethod
	int32_t ___FiringMethod_9;
	// BNG.ReloadType BNG.RaycastWeapon::ReloadMethod
	int32_t ___ReloadMethod_10;
	// System.Single BNG.RaycastWeapon::FiringRate
	float ___FiringRate_11;
	// System.Single BNG.RaycastWeapon::lastShotTime
	float ___lastShotTime_12;
	// System.Single BNG.RaycastWeapon::BulletImpactForce
	float ___BulletImpactForce_13;
	// System.Single BNG.RaycastWeapon::InternalAmmo
	float ___InternalAmmo_14;
	// System.Single BNG.RaycastWeapon::MaxInternalAmmo
	float ___MaxInternalAmmo_15;
	// System.Boolean BNG.RaycastWeapon::AutoChamberRounds
	bool ___AutoChamberRounds_16;
	// System.Boolean BNG.RaycastWeapon::MustChamberRounds
	bool ___MustChamberRounds_17;
	// System.Boolean BNG.RaycastWeapon::AlwaysFireProjectile
	bool ___AlwaysFireProjectile_18;
	// System.Boolean BNG.RaycastWeapon::FireProjectileInSlowMo
	bool ___FireProjectileInSlowMo_19;
	// System.Single BNG.RaycastWeapon::SlowMoRateOfFire
	float ___SlowMoRateOfFire_20;
	// System.Single BNG.RaycastWeapon::ShotForce
	float ___ShotForce_21;
	// System.Single BNG.RaycastWeapon::BulletCasingForce
	float ___BulletCasingForce_22;
	// UnityEngine.Vector3 BNG.RaycastWeapon::RecoilForce
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___RecoilForce_23;
	// System.Single BNG.RaycastWeapon::RecoilDuration
	float ___RecoilDuration_24;
	// UnityEngine.Rigidbody BNG.RaycastWeapon::weaponRigid
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___weaponRigid_25;
	// UnityEngine.LayerMask BNG.RaycastWeapon::ValidLayers
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___ValidLayers_26;
	// UnityEngine.Transform BNG.RaycastWeapon::TriggerTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___TriggerTransform_27;
	// UnityEngine.Transform BNG.RaycastWeapon::SlideTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___SlideTransform_28;
	// UnityEngine.Transform BNG.RaycastWeapon::MuzzlePointTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___MuzzlePointTransform_29;
	// UnityEngine.Transform BNG.RaycastWeapon::EjectPointTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___EjectPointTransform_30;
	// UnityEngine.Transform BNG.RaycastWeapon::ChamberedBullet
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___ChamberedBullet_31;
	// UnityEngine.GameObject BNG.RaycastWeapon::MuzzleFlashObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___MuzzleFlashObject_32;
	// UnityEngine.GameObject BNG.RaycastWeapon::BulletCasingPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___BulletCasingPrefab_33;
	// UnityEngine.GameObject BNG.RaycastWeapon::ProjectilePrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ProjectilePrefab_34;
	// UnityEngine.GameObject BNG.RaycastWeapon::HitFXPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___HitFXPrefab_35;
	// UnityEngine.AudioClip BNG.RaycastWeapon::GunShotSound
	AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20* ___GunShotSound_36;
	// System.Single BNG.RaycastWeapon::GunShotVolume
	float ___GunShotVolume_37;
	// UnityEngine.AudioClip BNG.RaycastWeapon::EmptySound
	AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20* ___EmptySound_38;
	// System.Single BNG.RaycastWeapon::EmptySoundVolume
	float ___EmptySoundVolume_39;
	// System.Single BNG.RaycastWeapon::SlideDistance
	float ___SlideDistance_40;
	// System.Boolean BNG.RaycastWeapon::ForceSlideBackOnLastShot
	bool ___ForceSlideBackOnLastShot_41;
	// System.Single BNG.RaycastWeapon::slideSpeed
	float ___slideSpeed_42;
	// System.Single BNG.RaycastWeapon::minSlideDistance
	float ___minSlideDistance_43;
	// System.Collections.Generic.List`1<BNG.GrabbedControllerBinding> BNG.RaycastWeapon::EjectInput
	List_1_tAD8C73BB08C3242046D9FF8B9A80F7FE939A02D8* ___EjectInput_44;
	// System.Collections.Generic.List`1<BNG.GrabbedControllerBinding> BNG.RaycastWeapon::ReleaseSlideInput
	List_1_tAD8C73BB08C3242046D9FF8B9A80F7FE939A02D8* ___ReleaseSlideInput_45;
	// System.Collections.Generic.List`1<BNG.GrabbedControllerBinding> BNG.RaycastWeapon::ReloadInput
	List_1_tAD8C73BB08C3242046D9FF8B9A80F7FE939A02D8* ___ReloadInput_46;
	// System.Boolean BNG.RaycastWeapon::BulletInChamber
	bool ___BulletInChamber_47;
	// System.Boolean BNG.RaycastWeapon::EmptyBulletInChamber
	bool ___EmptyBulletInChamber_48;
	// UnityEngine.Events.UnityEvent BNG.RaycastWeapon::onShootEvent
	UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* ___onShootEvent_49;
	// UnityEngine.Events.UnityEvent BNG.RaycastWeapon::onAttachedAmmoEvent
	UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* ___onAttachedAmmoEvent_50;
	// UnityEngine.Events.UnityEvent BNG.RaycastWeapon::onDetachedAmmoEvent
	UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* ___onDetachedAmmoEvent_51;
	// UnityEngine.Events.UnityEvent BNG.RaycastWeapon::onWeaponChargedEvent
	UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* ___onWeaponChargedEvent_52;
	// BNG.FloatEvent BNG.RaycastWeapon::onDealtDamageEvent
	FloatEvent_tB5BBD837F83AC37CEE0C4C7087E97829D38315F0* ___onDealtDamageEvent_53;
	// BNG.RaycastHitEvent BNG.RaycastWeapon::onRaycastHitEvent
	RaycastHitEvent_t565AB54EC826DC0FB61F834B1E155DA82716F75D* ___onRaycastHitEvent_54;
	// System.Boolean BNG.RaycastWeapon::slideForcedBack
	bool ___slideForcedBack_55;
	// BNG.WeaponSlide BNG.RaycastWeapon::ws
	WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* ___ws_56;
	// System.Boolean BNG.RaycastWeapon::readyToShoot
	bool ___readyToShoot_57;
	// System.Boolean BNG.RaycastWeapon::playedEmptySound
	bool ___playedEmptySound_58;
	// System.Collections.IEnumerator BNG.RaycastWeapon::shotRoutine
	RuntimeObject* ___shotRoutine_59;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* __this, float ___seconds0, const RuntimeMethod* method) ;
// System.Void BNG.RaycastWeapon::randomizeMuzzleFlashScaleRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RaycastWeapon_randomizeMuzzleFlashScaleRotation_mCAF0E4D6FD14E729B47F86786CE1B0F313EAA6FF (RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* __this, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___exists0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m3E2E4E94093F49D09DEB34CA97BF8A632B27F1AD_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___current0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline (const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<BNG.RaycastWeapon>()
inline RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* Component_GetComponent_TisRaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160_mF95EF20FB03601694578076A5D3C3E2FEA9A4FA7 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponent<BNG.Grabbable>()
inline Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* Component_GetComponent_TisGrabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B_m50588DDDB54F724D3F63C020A51049BC4128BB24 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Single UnityEngine.Rigidbody::get_mass()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rigidbody_get_mass_m09DDDDC437499B83B3BD0D77C134BFDC3E667054 (Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.Collider>()
inline Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* Component_GetComponent_TisCollider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76_m820398EDBF1D3766C3166A0C323A127662A29A14 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_IgnoreCollision_m274D5D55AE8A07CD416D2C4147B4CAA3DF3C806A (Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___collider10, Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___collider21, const RuntimeMethod* method) ;
// System.Void BNG.PlayerTeleport/OnBeforeTeleportAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnBeforeTeleportAction__ctor_m7933C0CE981D771BF0BBB37FBF4FF7C14D0CCE35 (OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void BNG.PlayerTeleport::add_OnBeforeTeleport(BNG.PlayerTeleport/OnBeforeTeleportAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerTeleport_add_OnBeforeTeleport_mADD5036E955F131399261947C6BB1885283AF1F0 (OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8* ___value0, const RuntimeMethod* method) ;
// System.Void BNG.PlayerTeleport/OnAfterTeleportAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnAfterTeleportAction__ctor_m981EB16D1E3F1CDD181FED4E7D5A30F2DA3B8AEF (OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void BNG.PlayerTeleport::add_OnAfterTeleport(BNG.PlayerTeleport/OnAfterTeleportAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerTeleport_add_OnAfterTeleport_mA89DC1B2A5721EC3990DCBA3C02BE92BD219D718 (OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808* ___value0, const RuntimeMethod* method) ;
// System.Void BNG.PlayerTeleport::remove_OnBeforeTeleport(BNG.PlayerTeleport/OnBeforeTeleportAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerTeleport_remove_OnBeforeTeleport_m00615730F0C789127763E8C79CB6B70CC8EE421D (OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8* ___value0, const RuntimeMethod* method) ;
// System.Void BNG.PlayerTeleport::remove_OnAfterTeleport(BNG.PlayerTeleport/OnAfterTeleportAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerTeleport_remove_OnAfterTeleport_mB4ABB6321EDB753429829E2742CE7131A82C14EB (OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808* ___value0, const RuntimeMethod* method) ;
// System.Void BNG.WeaponSlide::onSlideBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_onSlideBack_mE014720A70E1C58447D3322E3680D2ADCB9115C4 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) ;
// System.Void BNG.WeaponSlide::onSlideForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_onSlideForward_m4D0BD2AF7091C4BC01A4325210DA5E977C3F2AAB (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_mass_mC7F886DEDB57C742A16F8B6B779F69AFE164CA4B (Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* __this, float ___value0, const RuntimeMethod* method) ;
// BNG.VRUtils BNG.VRUtils::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469* VRUtils_get_Instance_m94182A01A486BC0F2AD143CF401F556BD13B434B (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.AudioSource BNG.VRUtils::PlaySpatialClipAt(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* VRUtils_PlaySpatialClipAt_mEDE911D70037C4CBBD46D1C35F33A5DFFA9A130F (VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469* __this, AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20* ___clip0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___pos1, float ___volume2, float ___spatialBlend3, float ___randomizePitch4, const RuntimeMethod* method) ;
// System.Void BNG.WeaponSlide::playSoundInterval(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_playSoundInterval_m941C21C22FA99E5C7972C6BDA65D973B810D9504 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, float ___fromSeconds0, float ___toSeconds1, float ___volume2, const RuntimeMethod* method) ;
// System.Collections.IEnumerator BNG.WeaponSlide::UnlockSlideRoutine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WeaponSlide_UnlockSlideRoutine_m91E0F77885CA06F79595EC40712EF1469F0F4427 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUnlockSlideRoutineU3Ed__27__ctor_m2CBFD2C282CB8F21315E20BAD2F9AB8E9BC336E7 (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AudioSource_get_isPlaying_mC203303F2F7146B2C056CB47B9391463FDF408FC (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.AudioSource::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Stop_m318F17F17A147C77FF6E0A5A7A6BE057DB90F537 (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_timeScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_timeScale_m99F3D47F45286D6DA73ADB2662B63451A632D413 (const RuntimeMethod* method) ;
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_pitch_mD14631FC99BF38AAFB356D9C45546BC16CF9E811 (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, float ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.AudioSource::set_time(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_time_m6670372FD9C494978B7B3E01B7F4D220616F6204 (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, float ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_volume_mD902BBDBBDE0E3C148609BF3C05096148E90F2C0 (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, float ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3 (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, const RuntimeMethod* method) ;
// System.Double UnityEngine.AudioSettings::get_dspTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double AudioSettings_get_dspTime_m811568DA82A4831AFB4E4A6527C01EA29E719538 (const RuntimeMethod* method) ;
// System.Void UnityEngine.AudioSource::SetScheduledEndTime(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_SetScheduledEndTime_mC9BF39919029A6C6CB8981B09A792D45A60A3730 (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, double ___time0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m7EA47AD57F43D478CCB0523D179950EE49CDA3E2 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.GraphicsSettings::set_renderPipelineAsset(UnityEngine.Rendering.RenderPipelineAsset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicsSettings_set_renderPipelineAsset_mAD34EC54DF2B76755B88F38568F88A474B31FB79 (RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E* ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void DrunkMan.FreeCamera::Move(UnityEngine.KeyCode,UnityEngine.Vector3&,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88 (FreeCamera_t17D2C98F0E4AE04AA5D7713B35EA0230081BC088* __this, int32_t ___key0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___moveTo1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___dir2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_UnaryNegation_m3AC523A7BED6E843165BDF598690F0560D8CAA63_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_right_mC6DC057C23313802E2186A9E0DB760D795A758A4 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_up_mE47A9D9D96422224DD0539AA5524DA5440145BB2 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButton_mE545CF4B790C6E202808B827E3141BEC3330DB70 (int32_t ___button0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_eulerAngles_mCAAF48EFCF628F1ED91C2FFE75A4FD19C039DD6A (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4 (String_t* ___axisName0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_eulerAngles_m9F0BC484A7915A51FAB87230644229B75BACA004 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_m0BF0499CADC378F02B6BEE2399FB945AB929B81A (int32_t ___key0, const RuntimeMethod* method) ;
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, const RuntimeMethod* method) ;
// System.Void DrunkMan.DrunkManFeature/BlitPass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlitPass__ctor_m96534D9B89F7E34A0AADA29783F6F315D54F6F06 (BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* __this, String_t* ___tag0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_FindGameObjectWithTag_m17078A0823CA9699710251C617B95D04D57098A9 (String_t* ___tag0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<StatsManager>()
inline StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* GameObject_GetComponent_TisStatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87_m6D7084B39D7647C73A971160764AD450B56326F1 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Single StatsManager::GetABV()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float StatsManager_GetABV_mEA7946CB824D7AE211EF9B8AD560C64AEFEFF8CD_inline (StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* __this, const RuntimeMethod* method) ;
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.ScriptableRenderer::get_cameraColorTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ScriptableRenderer_get_cameraColorTarget_mC2C0353A178726FC82413A458A34496280AFB4D4 (ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* __this, const RuntimeMethod* method) ;
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogWarningFormat(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarningFormat_m0D4A31935564D0FA042103C1231DBBD2ED0BC20A (String_t* ___format0, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___name0, float ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetVector_m69444B8040D955821F241113446CC8713C9E12D1 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___name0, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___value1, const RuntimeMethod* method) ;
// System.Void DrunkMan.DrunkManFeature/BlitPass::Setup(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material,System.Boolean,DrunkMan.DrunkManFeature/EType,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlitPass_Setup_mBBA957EC6EE0D9F34629B37432FE068557DBCE9B (BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* __this, RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___src0, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat1, bool ___sleepyEye2, int32_t ___tp3, float ___intensity4, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.Universal.ScriptableRenderer::EnqueuePass(UnityEngine.Rendering.Universal.ScriptableRenderPass)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableRenderer_EnqueuePass_m62AC5EFBA8DECFD514CAFC4EFDCFBF88C710954F (ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* __this, ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0* ___pass0, const RuntimeMethod* method) ;
// System.Void DrunkMan.DrunkManFeature/Settings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Settings__ctor_mA5D76ECE115DEE0E6F4879C3772A4C79E274ECED (Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.Universal.ScriptableRendererFeature::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableRendererFeature__ctor_mA05EC9569A5DCF48CDD98E1FC5838857E2C4C001 (ScriptableRendererFeature_tF2ED08AD2498105378D4DAB0386A0F8A144EF4C6* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.Universal.ScriptableRenderPass::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableRenderPass__ctor_mE49D4FF8E68A854367A4081E664B8DBA74E6B752 (ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.Universal.ScriptableRenderPass::set_renderPassEvent(UnityEngine.Rendering.Universal.RenderPassEvent)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ScriptableRenderPass_set_renderPassEvent_m63FA581FFDE1C69C2E1358BD0B8DB30275334960_inline (ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.Universal.RenderTargetHandle::Init(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTargetHandle_Init_mDF9383A0DB5E0B56577BA43CC56CD659F8970646 (RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* __this, String_t* ___shaderProperty0, const RuntimeMethod* method) ;
// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.CommandBufferPool::Get(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* CommandBufferPool_Get_mC33780CD170099A0E396A2F3A9AFB46509B31625 (String_t* ___name0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderTextureDescriptor::set_depthBufferBits(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTextureDescriptor_set_depthBufferBits_mA3710C0D6E485BA6465B328CD8B1954F0E4C5819 (RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Rendering.Universal.RenderTargetHandle::get_id()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t RenderTargetHandle_get_id_m4D50FDA4A486E05D07A54ABFC04BD96C1CE7D7BE_inline (RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,UnityEngine.RenderTextureDescriptor,UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_GetTemporaryRT_m98BCBFF670DDD3AC8657664F8252A9DF64D49FA5 (CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* __this, int32_t ___nameID0, RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___desc1, int32_t ___filter2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalTexture(System.String,UnityEngine.Rendering.RenderTargetIdentifier)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_SetGlobalTexture_mD6F1CC7E87FA88B5838D5EDAFBA602EF94FE1F69 (CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* __this, String_t* ___name0, RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_SetGlobalFloat_mECD0FBFDF115D9150B5D1DB66010B17F6213419B (CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* __this, String_t* ___name0, float ___value1, const RuntimeMethod* method) ;
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.Universal.RenderTargetHandle::Identifier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA (RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.Universal.ScriptableRenderPass::Blit(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF (ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0* __this, CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* ___cmd0, RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___source1, RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___destination2, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material3, int32_t ___passIndex4, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.ScriptableRenderContext::ExecuteCommandBuffer(UnityEngine.Rendering.CommandBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableRenderContext_ExecuteCommandBuffer_mBAE37DFC699B7167A6E2C59012066C44A31E9896 (ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36* __this, CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* ___commandBuffer0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.CommandBufferPool::Release(UnityEngine.Rendering.CommandBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBufferPool_Release_mEC46D8373A95DEC68F1FBD2D77FF3F76917631BF (CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* ___buffer0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseTemporaryRT(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_ReleaseTemporaryRT_m4651A4B373DF432AA44F06A6F20852ED5996CC8E (CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* __this, int32_t ___nameID0, const RuntimeMethod* method) ;
// System.Void UnityEngine.QualitySettings::set_antiAliasing(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QualitySettings_set_antiAliasing_m1C7D6F2CFB2EC09BAE3081EFD1BF3EAAE1597B1A (int32_t ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Shader::SetGlobalTexture(System.String,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_SetGlobalTexture_mABB6E994E67D083BEBE142B4CC8FA77137C2D021 (String_t* ___name0, Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_SetGlobalFloat_mFA8F651003951E6319C952475148713F521243BF (String_t* ___name0, float ___value1, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8 (const RuntimeMethod* method) ;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* RenderTexture_GetTemporary_m47DF6AA5AB3A4360AF9CB62BE0180AE9505C6C66 (int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50 (Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___dest1, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat2, int32_t ___pass3, const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___temp0, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CdoMuzzleFlashU3Ed__74__ctor_m262ED95928F86FE308BCA36503B1CF72BDEAA9B2 (U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CdoMuzzleFlashU3Ed__74_System_IDisposable_Dispose_mF05C246FB314668C8D3D88CFB4CAD633245D5E6A (U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean BNG.RaycastWeapon/<doMuzzleFlash>d__74::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CdoMuzzleFlashU3Ed__74_MoveNext_m8CE63ADF91C750A9B67FD036B7FBFB5B88707EEE (U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_004e;
			}
			case 2:
			{
				goto IL_0074;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// MuzzleFlashObject.SetActive(true);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_3 = V_1;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = L_3->___MuzzleFlashObject_32;
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// yield return new  WaitForSeconds(0.05f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_5 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_5, (0.0500000007f), NULL);
		__this->___U3CU3E2__current_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_5);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_004e:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// randomizeMuzzleFlashScaleRotation();
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_6 = V_1;
		NullCheck(L_6);
		RaycastWeapon_randomizeMuzzleFlashScaleRotation_mCAF0E4D6FD14E729B47F86786CE1B0F313EAA6FF(L_6, NULL);
		// yield return new WaitForSeconds(0.05f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_7 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_7, (0.0500000007f), NULL);
		__this->___U3CU3E2__current_1 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_7);
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0074:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// MuzzleFlashObject.SetActive(false);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_8 = V_1;
		NullCheck(L_8);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = L_8->___MuzzleFlashObject_32;
		NullCheck(L_9);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_9, (bool)0, NULL);
		// }
		return (bool)0;
	}
}
// System.Object BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CdoMuzzleFlashU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC85468C57AC5E5B2FD0C5AB9153624FFD3397FF1 (U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_Reset_mC91A6508DC6552A176052DC75E80915C65800083 (U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_Reset_mC91A6508DC6552A176052DC75E80915C65800083_RuntimeMethod_var)));
	}
}
// System.Object BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_get_Current_m73BED4A42F6DB4BD0EC43421316E9DB09AC9A3C3 (U3CdoMuzzleFlashU3Ed__74_t5950ADAB300D6062CBC0F2FA4BAB9DA8D36F188F* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CanimateSlideAndEjectU3Ed__75__ctor_m52D4C4F0D32166633F5800ED0FA2F9102D9573FD (U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CanimateSlideAndEjectU3Ed__75_System_IDisposable_Dispose_m73F851FA17249440950F54E0E3D12DCC41E3C881 (U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean BNG.RaycastWeapon/<animateSlideAndEject>d__75::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CanimateSlideAndEjectU3Ed__75_MoveNext_mB2D1D8CBE242984B74CD60C88933D77C1252A0F7 (U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0032;
			}
			case 1:
			{
				goto IL_011d;
			}
			case 2:
			{
				goto IL_0145;
			}
			case 3:
			{
				goto IL_0166;
			}
			case 4:
			{
				goto IL_01b2;
			}
			case 5:
			{
				goto IL_01df;
			}
			case 6:
			{
				goto IL_0286;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0032:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// MuzzleFlashObject.SetActive(true);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_3 = V_1;
		NullCheck(L_3);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = L_3->___MuzzleFlashObject_32;
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// int frames = 0;
		__this->___U3CframesU3E5__2_3 = 0;
		// bool slideEndReached = false;
		__this->___U3CslideEndReachedU3E5__3_4 = (bool)0;
		// Vector3 slideDestination = new Vector3(0, 0, SlideDistance);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_5 = V_1;
		NullCheck(L_5);
		float L_6 = L_5->___SlideDistance_40;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_7), (0.0f), (0.0f), L_6, /*hidden argument*/NULL);
		__this->___U3CslideDestinationU3E5__4_5 = L_7;
		// if(SlideTransform) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_8 = V_1;
		NullCheck(L_8);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_9 = L_8->___SlideTransform_28;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_9, NULL);
		if (!L_10)
		{
			goto IL_0131;
		}
	}
	{
		goto IL_0124;
	}

IL_0083:
	{
		// SlideTransform.localPosition = Vector3.MoveTowards(SlideTransform.localPosition, slideDestination, Time.deltaTime * slideSpeed);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_11 = V_1;
		NullCheck(L_11);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_12 = L_11->___SlideTransform_28;
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_13 = V_1;
		NullCheck(L_13);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_14 = L_13->___SlideTransform_28;
		NullCheck(L_14);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95(L_14, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = __this->___U3CslideDestinationU3E5__4_5;
		float L_17;
		L_17 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_18 = V_1;
		NullCheck(L_18);
		float L_19 = L_18->___slideSpeed_42;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20;
		L_20 = Vector3_MoveTowards_m3E2E4E94093F49D09DEB34CA97BF8A632B27F1AD_inline(L_15, L_16, ((float)il2cpp_codegen_multiply(L_17, L_19)), NULL);
		NullCheck(L_12);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_12, L_20, NULL);
		// float distance = Vector3.Distance(SlideTransform.localPosition, slideDestination);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_21 = V_1;
		NullCheck(L_21);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_22 = L_21->___SlideTransform_28;
		NullCheck(L_22);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23;
		L_23 = Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95(L_22, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = __this->___U3CslideDestinationU3E5__4_5;
		float L_25;
		L_25 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_23, L_24, NULL);
		// if (distance <= minSlideDistance) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_26 = V_1;
		NullCheck(L_26);
		float L_27 = L_26->___minSlideDistance_43;
		if ((!(((float)L_25) <= ((float)L_27))))
		{
			goto IL_00d5;
		}
	}
	{
		// slideEndReached = true;
		__this->___U3CslideEndReachedU3E5__3_4 = (bool)1;
	}

IL_00d5:
	{
		// frames++;
		int32_t L_28 = __this->___U3CframesU3E5__2_3;
		V_2 = L_28;
		int32_t L_29 = V_2;
		__this->___U3CframesU3E5__2_3 = ((int32_t)il2cpp_codegen_add(L_29, 1));
		// if (frames < 2) {
		int32_t L_30 = __this->___U3CframesU3E5__2_3;
		if ((((int32_t)L_30) >= ((int32_t)2)))
		{
			goto IL_00f6;
		}
	}
	{
		// randomizeMuzzleFlashScaleRotation();
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_31 = V_1;
		NullCheck(L_31);
		RaycastWeapon_randomizeMuzzleFlashScaleRotation_mCAF0E4D6FD14E729B47F86786CE1B0F313EAA6FF(L_31, NULL);
		goto IL_0109;
	}

IL_00f6:
	{
		// slideEndReached = true;
		__this->___U3CslideEndReachedU3E5__3_4 = (bool)1;
		// MuzzleFlashObject.SetActive(false);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_32 = V_1;
		NullCheck(L_32);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_33 = L_32->___MuzzleFlashObject_32;
		NullCheck(L_33);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_33, (bool)0, NULL);
	}

IL_0109:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_34 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_34);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_34, NULL);
		__this->___U3CU3E2__current_1 = L_34;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_34);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_011d:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0124:
	{
		// while (!slideEndReached) {
		bool L_35 = __this->___U3CslideEndReachedU3E5__3_4;
		if (!L_35)
		{
			goto IL_0083;
		}
	}
	{
		goto IL_0180;
	}

IL_0131:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_36 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_36);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_36, NULL);
		__this->___U3CU3E2__current_1 = L_36;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_36);
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0145:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// randomizeMuzzleFlashScaleRotation();
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_37 = V_1;
		NullCheck(L_37);
		RaycastWeapon_randomizeMuzzleFlashScaleRotation_mCAF0E4D6FD14E729B47F86786CE1B0F313EAA6FF(L_37, NULL);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_38 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_38);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_38, NULL);
		__this->___U3CU3E2__current_1 = L_38;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_38);
		__this->___U3CU3E1__state_0 = 3;
		return (bool)1;
	}

IL_0166:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// MuzzleFlashObject.SetActive(false);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_39 = V_1;
		NullCheck(L_39);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = L_39->___MuzzleFlashObject_32;
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)0, NULL);
		// slideEndReached = true;
		__this->___U3CslideEndReachedU3E5__3_4 = (bool)1;
	}

IL_0180:
	{
		// if(SlideTransform) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_41 = V_1;
		NullCheck(L_41);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_42 = L_41->___SlideTransform_28;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_43;
		L_43 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_42, NULL);
		if (!L_43)
		{
			goto IL_019e;
		}
	}
	{
		// SlideTransform.localPosition = slideDestination;
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_44 = V_1;
		NullCheck(L_44);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_45 = L_44->___SlideTransform_28;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_46 = __this->___U3CslideDestinationU3E5__4_5;
		NullCheck(L_45);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_45, L_46, NULL);
	}

IL_019e:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_47 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_47);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_47, NULL);
		__this->___U3CU3E2__current_1 = L_47;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_47);
		__this->___U3CU3E1__state_0 = 4;
		return (bool)1;
	}

IL_01b2:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// MuzzleFlashObject.SetActive(false);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_48 = V_1;
		NullCheck(L_48);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_49 = L_48->___MuzzleFlashObject_32;
		NullCheck(L_49);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_49, (bool)0, NULL);
		// ejectCasing();
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_50 = V_1;
		NullCheck(L_50);
		VirtualActionInvoker0::Invoke(40 /* System.Void BNG.RaycastWeapon::ejectCasing() */, L_50);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_51 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_51);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_51, NULL);
		__this->___U3CU3E2__current_1 = L_51;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_51);
		__this->___U3CU3E1__state_0 = 5;
		return (bool)1;
	}

IL_01df:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if(!slideForcedBack && SlideTransform != null) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_52 = V_1;
		NullCheck(L_52);
		bool L_53 = L_52->___slideForcedBack_55;
		if (L_53)
		{
			goto IL_0298;
		}
	}
	{
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_54 = V_1;
		NullCheck(L_54);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_55 = L_54->___SlideTransform_28;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_56;
		L_56 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_55, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_56)
		{
			goto IL_0298;
		}
	}
	{
		// frames = 0;
		__this->___U3CframesU3E5__2_3 = 0;
		// bool slideBeginningReached = false;
		__this->___U3CslideBeginningReachedU3E5__5_6 = (bool)0;
		goto IL_028d;
	}

IL_0212:
	{
		// SlideTransform.localPosition = Vector3.MoveTowards(SlideTransform.localPosition, Vector3.zero, Time.deltaTime * slideSpeed);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_57 = V_1;
		NullCheck(L_57);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_58 = L_57->___SlideTransform_28;
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_59 = V_1;
		NullCheck(L_59);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_60 = L_59->___SlideTransform_28;
		NullCheck(L_60);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_61;
		L_61 = Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95(L_60, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_62;
		L_62 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		float L_63;
		L_63 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_64 = V_1;
		NullCheck(L_64);
		float L_65 = L_64->___slideSpeed_42;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_66;
		L_66 = Vector3_MoveTowards_m3E2E4E94093F49D09DEB34CA97BF8A632B27F1AD_inline(L_61, L_62, ((float)il2cpp_codegen_multiply(L_63, L_65)), NULL);
		NullCheck(L_58);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_58, L_66, NULL);
		// float distance = Vector3.Distance(SlideTransform.localPosition, Vector3.zero);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_67 = V_1;
		NullCheck(L_67);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_68 = L_67->___SlideTransform_28;
		NullCheck(L_68);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_69;
		L_69 = Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95(L_68, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_70;
		L_70 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		float L_71;
		L_71 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_69, L_70, NULL);
		// if (distance <= minSlideDistance) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_72 = V_1;
		NullCheck(L_72);
		float L_73 = L_72->___minSlideDistance_43;
		if ((!(((float)L_71) <= ((float)L_73))))
		{
			goto IL_0262;
		}
	}
	{
		// slideBeginningReached = true;
		__this->___U3CslideBeginningReachedU3E5__5_6 = (bool)1;
	}

IL_0262:
	{
		// if (frames > 2) {
		int32_t L_74 = __this->___U3CframesU3E5__2_3;
		if ((((int32_t)L_74) <= ((int32_t)2)))
		{
			goto IL_0272;
		}
	}
	{
		// slideBeginningReached = true;
		__this->___U3CslideBeginningReachedU3E5__5_6 = (bool)1;
	}

IL_0272:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_75 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_75);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_75, NULL);
		__this->___U3CU3E2__current_1 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_75);
		__this->___U3CU3E1__state_0 = 6;
		return (bool)1;
	}

IL_0286:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_028d:
	{
		// while (!slideBeginningReached) {
		bool L_76 = __this->___U3CslideBeginningReachedU3E5__5_6;
		if (!L_76)
		{
			goto IL_0212;
		}
	}

IL_0298:
	{
		// }
		return (bool)0;
	}
}
// System.Object BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CanimateSlideAndEjectU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B556AEA6646698B14C88F72971D1F0E22CB7506 (U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_Reset_m042CF81A8EDB791453EADF7B62CEBBB35821BA90 (U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_Reset_m042CF81A8EDB791453EADF7B62CEBBB35821BA90_RuntimeMethod_var)));
	}
}
// System.Object BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_get_Current_m62899C28A5BC0B43CF714B0E21DFDC21EF13D318 (U3CanimateSlideAndEjectU3Ed__75_t82AFB9B54786B601FFD381E2E190A8AA5AB14EF8* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BNG.WeaponSlide::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_Start_mC64514A27178319B231CF2DC78A2F4745C1B6848 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCollider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76_m820398EDBF1D3766C3166A0C323A127662A29A14_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisGrabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B_m50588DDDB54F724D3F63C020A51049BC4128BB24_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160_mF95EF20FB03601694578076A5D3C3E2FEA9A4FA7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// initialLocalPos = transform.localPosition;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95(L_0, NULL);
		__this->___initialLocalPos_13 = L_1;
		// audioSource = GetComponent<AudioSource>();
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_2;
		L_2 = Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B(__this, Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		__this->___audioSource_15 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___audioSource_15), (void*)L_2);
		// parentWeapon = transform.parent.GetComponent<RaycastWeapon>();
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_3);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E(L_3, NULL);
		NullCheck(L_4);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_5;
		L_5 = Component_GetComponent_TisRaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160_mF95EF20FB03601694578076A5D3C3E2FEA9A4FA7(L_4, Component_GetComponent_TisRaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160_mF95EF20FB03601694578076A5D3C3E2FEA9A4FA7_RuntimeMethod_var);
		__this->___parentWeapon_11 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___parentWeapon_11), (void*)L_5);
		// parentGrabbable = transform.parent.GetComponent<Grabbable>();
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_6;
		L_6 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_6);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7;
		L_7 = Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E(L_6, NULL);
		NullCheck(L_7);
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_8;
		L_8 = Component_GetComponent_TisGrabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B_m50588DDDB54F724D3F63C020A51049BC4128BB24(L_7, Component_GetComponent_TisGrabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B_m50588DDDB54F724D3F63C020A51049BC4128BB24_RuntimeMethod_var);
		__this->___parentGrabbable_12 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___parentGrabbable_12), (void*)L_8);
		// thisGrabbable = GetComponent<Grabbable>();
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_9;
		L_9 = Component_GetComponent_TisGrabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B_m50588DDDB54F724D3F63C020A51049BC4128BB24(__this, Component_GetComponent_TisGrabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B_m50588DDDB54F724D3F63C020A51049BC4128BB24_RuntimeMethod_var);
		__this->___thisGrabbable_14 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___thisGrabbable_14), (void*)L_9);
		// rigid = GetComponent<Rigidbody>();
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_10;
		L_10 = Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8(__this, Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8_RuntimeMethod_var);
		__this->___rigid_16 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___rigid_16), (void*)L_10);
		// initialMass = rigid.mass;
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_11 = __this->___rigid_16;
		NullCheck(L_11);
		float L_12;
		L_12 = Rigidbody_get_mass_m09DDDDC437499B83B3BD0D77C134BFDC3E667054(L_11, NULL);
		__this->___initialMass_17 = L_12;
		// if (parentWeapon != null) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_13 = __this->___parentWeapon_11;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_13, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_14)
		{
			goto IL_0096;
		}
	}
	{
		// Physics.IgnoreCollision(GetComponent<Collider>(), parentWeapon.GetComponent<Collider>());
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_15;
		L_15 = Component_GetComponent_TisCollider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76_m820398EDBF1D3766C3166A0C323A127662A29A14(__this, Component_GetComponent_TisCollider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76_m820398EDBF1D3766C3166A0C323A127662A29A14_RuntimeMethod_var);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_16 = __this->___parentWeapon_11;
		NullCheck(L_16);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_17;
		L_17 = Component_GetComponent_TisCollider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76_m820398EDBF1D3766C3166A0C323A127662A29A14(L_16, Component_GetComponent_TisCollider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76_m820398EDBF1D3766C3166A0C323A127662A29A14_RuntimeMethod_var);
		Physics_IgnoreCollision_m274D5D55AE8A07CD416D2C4147B4CAA3DF3C806A(L_15, L_17, NULL);
	}

IL_0096:
	{
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_OnEnable_mF2E8830A64E46555E5D33B4F0D26A28C41B7C6B5 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PlayerTeleport.OnBeforeTeleport += LockSlidePosition;
		OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8* L_0 = (OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8*)il2cpp_codegen_object_new(OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		OnBeforeTeleportAction__ctor_m7933C0CE981D771BF0BBB37FBF4FF7C14D0CCE35(L_0, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 8)), NULL);
		PlayerTeleport_add_OnBeforeTeleport_mADD5036E955F131399261947C6BB1885283AF1F0(L_0, NULL);
		// PlayerTeleport.OnAfterTeleport += UnlockSlidePosition;
		OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808* L_1 = (OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808*)il2cpp_codegen_object_new(OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		OnAfterTeleportAction__ctor_m981EB16D1E3F1CDD181FED4E7D5A30F2DA3B8AEF(L_1, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 9)), NULL);
		PlayerTeleport_add_OnAfterTeleport_mA89DC1B2A5721EC3990DCBA3C02BE92BD219D718(L_1, NULL);
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_OnDisable_m648C81D73CFADEA65B4C3934D21671A5C100A34C (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PlayerTeleport.OnBeforeTeleport -= LockSlidePosition;
		OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8* L_0 = (OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8*)il2cpp_codegen_object_new(OnBeforeTeleportAction_t1B9CF5EBE3C3D9656C293767E698D0392FB145E8_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		OnBeforeTeleportAction__ctor_m7933C0CE981D771BF0BBB37FBF4FF7C14D0CCE35(L_0, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 8)), NULL);
		PlayerTeleport_remove_OnBeforeTeleport_m00615730F0C789127763E8C79CB6B70CC8EE421D(L_0, NULL);
		// PlayerTeleport.OnAfterTeleport -= UnlockSlidePosition;
		OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808* L_1 = (OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808*)il2cpp_codegen_object_new(OnAfterTeleportAction_tFC04EC1DD2DF77998682718F2A62F3AFBD764808_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		OnAfterTeleportAction__ctor_m981EB16D1E3F1CDD181FED4E7D5A30F2DA3B8AEF(L_1, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 9)), NULL);
		PlayerTeleport_remove_OnAfterTeleport_mB4ABB6321EDB753429829E2742CE7131A82C14EB(L_1, NULL);
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_Update_m374A94637F686151EECA902CF09F34B3F1646E87 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if(lockSlidePosition) {
		bool L_0 = __this->___lockSlidePosition_19;
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		// transform.localPosition = _lockPosition;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = __this->____lockPosition_18;
		NullCheck(L_1);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_1, L_2, NULL);
		// return;
		return;
	}

IL_001a:
	{
		// float localZ = transform.localPosition.z;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_3);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95(L_3, NULL);
		float L_5 = L_4.___z_4;
		V_0 = L_5;
		// if (LockedBack) {
		bool L_6 = __this->___LockedBack_7;
		if (!L_6)
		{
			goto IL_0080;
		}
	}
	{
		// transform.localPosition = new Vector3(initialLocalPos.x, initialLocalPos.y, MinLocalZ);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7;
		L_7 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_8 = (&__this->___initialLocalPos_13);
		float L_9 = L_8->___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_10 = (&__this->___initialLocalPos_13);
		float L_11 = L_10->___y_3;
		float L_12 = __this->___MinLocalZ_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_13), L_9, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_7, L_13, NULL);
		// if (thisGrabbable != null && thisGrabbable.BeingHeld) {
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_14 = __this->___thisGrabbable_14;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_14, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_15)
		{
			goto IL_0080;
		}
	}
	{
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_16 = __this->___thisGrabbable_14;
		NullCheck(L_16);
		bool L_17 = L_16->___BeingHeld_4;
		if (!L_17)
		{
			goto IL_0080;
		}
	}
	{
		// UnlockBack();
		VirtualActionInvoker0::Invoke(7 /* System.Void BNG.WeaponSlide::UnlockBack() */, __this);
	}

IL_0080:
	{
		// if (!LockedBack) {
		bool L_18 = __this->___LockedBack_7;
		if (L_18)
		{
			goto IL_0112;
		}
	}
	{
		// if (localZ <= MinLocalZ) {
		float L_19 = V_0;
		float L_20 = __this->___MinLocalZ_4;
		if ((!(((float)L_19) <= ((float)L_20))))
		{
			goto IL_00cf;
		}
	}
	{
		// transform.localPosition = new Vector3(initialLocalPos.x, initialLocalPos.y, MinLocalZ);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_21;
		L_21 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_22 = (&__this->___initialLocalPos_13);
		float L_23 = L_22->___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_24 = (&__this->___initialLocalPos_13);
		float L_25 = L_24->___y_3;
		float L_26 = __this->___MinLocalZ_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27;
		memset((&L_27), 0, sizeof(L_27));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_27), L_23, L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_21, L_27, NULL);
		// if (slidingBack) {
		bool L_28 = __this->___slidingBack_6;
		if (!L_28)
		{
			goto IL_0112;
		}
	}
	{
		// onSlideBack();
		WeaponSlide_onSlideBack_mE014720A70E1C58447D3322E3680D2ADCB9115C4(__this, NULL);
		return;
	}

IL_00cf:
	{
		// else if (localZ >= MaxLocalZ) {
		float L_29 = V_0;
		float L_30 = __this->___MaxLocalZ_5;
		if ((!(((float)L_29) >= ((float)L_30))))
		{
			goto IL_0112;
		}
	}
	{
		// transform.localPosition = new Vector3(initialLocalPos.x, initialLocalPos.y, MaxLocalZ);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_31;
		L_31 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_32 = (&__this->___initialLocalPos_13);
		float L_33 = L_32->___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_34 = (&__this->___initialLocalPos_13);
		float L_35 = L_34->___y_3;
		float L_36 = __this->___MaxLocalZ_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37;
		memset((&L_37), 0, sizeof(L_37));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_37), L_33, L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_31, L_37, NULL);
		// if (!slidingBack) {
		bool L_38 = __this->___slidingBack_6;
		if (L_38)
		{
			goto IL_0112;
		}
	}
	{
		// onSlideForward();
		WeaponSlide_onSlideForward_m4D0BD2AF7091C4BC01A4325210DA5E977C3F2AAB(__this, NULL);
	}

IL_0112:
	{
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_FixedUpdate_mA417C3F95941F3137D7BEAAC4D7ED0FA59B00071 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (ZeroMassWhenNotHeld && parentGrabbable.BeingHeld && rigid) {
		bool L_0 = __this->___ZeroMassWhenNotHeld_10;
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_1 = __this->___parentGrabbable_12;
		NullCheck(L_1);
		bool L_2 = L_1->___BeingHeld_4;
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_3 = __this->___rigid_16;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_3, NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		// rigid.mass = initialMass;
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_5 = __this->___rigid_16;
		float L_6 = __this->___initialMass_17;
		NullCheck(L_5);
		Rigidbody_set_mass_mC7F886DEDB57C742A16F8B6B779F69AFE164CA4B(L_5, L_6, NULL);
		return;
	}

IL_0034:
	{
		// else if (ZeroMassWhenNotHeld && rigid) {
		bool L_7 = __this->___ZeroMassWhenNotHeld_10;
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_8 = __this->___rigid_16;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_8, NULL);
		if (!L_9)
		{
			goto IL_0059;
		}
	}
	{
		// rigid.mass = 0.0001f;
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_10 = __this->___rigid_16;
		NullCheck(L_10);
		Rigidbody_set_mass_mC7F886DEDB57C742A16F8B6B779F69AFE164CA4B(L_10, (9.99999975E-05f), NULL);
	}

IL_0059:
	{
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::LockBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_LockBack_m9599590FBD04AB3210D29383C537D6BBF719673C (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	{
		// if (!LockedBack) {
		bool L_0 = __this->___LockedBack_7;
		if (L_0)
		{
			goto IL_0054;
		}
	}
	{
		// if (thisGrabbable.BeingHeld || parentGrabbable.BeingHeld) {
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_1 = __this->___thisGrabbable_14;
		NullCheck(L_1);
		bool L_2 = L_1->___BeingHeld_4;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_3 = __this->___parentGrabbable_12;
		NullCheck(L_3);
		bool L_4 = L_3->___BeingHeld_4;
		if (!L_4)
		{
			goto IL_004d;
		}
	}

IL_0022:
	{
		// VRUtils.Instance.PlaySpatialClipAt(LockedBackSound, transform.position, 1f, 0.8f);
		VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469* L_5;
		L_5 = VRUtils_get_Instance_m94182A01A486BC0F2AD143CF401F556BD13B434B(NULL);
		AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20* L_6 = __this->___LockedBackSound_9;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7;
		L_7 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_7);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_7, NULL);
		NullCheck(L_5);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_9;
		L_9 = VRUtils_PlaySpatialClipAt_mEDE911D70037C4CBBD46D1C35F33A5DFFA9A130F(L_5, L_6, L_8, (1.0f), (0.800000012f), (0.0f), NULL);
	}

IL_004d:
	{
		// LockedBack = true;
		__this->___LockedBack_7 = (bool)1;
	}

IL_0054:
	{
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::UnlockBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_UnlockBack_m1752A520C33A66C8071658D02E9270490E8E41B6 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (LockedBack) {
		bool L_0 = __this->___LockedBack_7;
		if (!L_0)
		{
			goto IL_006e;
		}
	}
	{
		// if (thisGrabbable.BeingHeld || parentGrabbable.BeingHeld) {
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_1 = __this->___thisGrabbable_14;
		NullCheck(L_1);
		bool L_2 = L_1->___BeingHeld_4;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_3 = __this->___parentGrabbable_12;
		NullCheck(L_3);
		bool L_4 = L_3->___BeingHeld_4;
		if (!L_4)
		{
			goto IL_004d;
		}
	}

IL_0022:
	{
		// VRUtils.Instance.PlaySpatialClipAt(SlideReleaseSound, transform.position, 1f, 0.9f);
		VRUtils_t70D0EDBE86D95E5267801427235BF933D8A94469* L_5;
		L_5 = VRUtils_get_Instance_m94182A01A486BC0F2AD143CF401F556BD13B434B(NULL);
		AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20* L_6 = __this->___SlideReleaseSound_8;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7;
		L_7 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_7);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_7, NULL);
		NullCheck(L_5);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_9;
		L_9 = VRUtils_PlaySpatialClipAt_mEDE911D70037C4CBBD46D1C35F33A5DFFA9A130F(L_5, L_6, L_8, (1.0f), (0.899999976f), (0.0f), NULL);
	}

IL_004d:
	{
		// LockedBack = false;
		__this->___LockedBack_7 = (bool)0;
		// if (parentWeapon != null) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_10 = __this->___parentWeapon_11;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_10, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_11)
		{
			goto IL_006e;
		}
	}
	{
		// parentWeapon.OnWeaponCharged(false);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_12 = __this->___parentWeapon_11;
		NullCheck(L_12);
		VirtualActionInvoker1< bool >::Invoke(39 /* System.Void BNG.RaycastWeapon::OnWeaponCharged(System.Boolean) */, L_12, (bool)0);
	}

IL_006e:
	{
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::onSlideBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_onSlideBack_mE014720A70E1C58447D3322E3680D2ADCB9115C4 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (thisGrabbable.BeingHeld || parentGrabbable.BeingHeld) {
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_0 = __this->___thisGrabbable_14;
		NullCheck(L_0);
		bool L_1 = L_0->___BeingHeld_4;
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_2 = __this->___parentGrabbable_12;
		NullCheck(L_2);
		bool L_3 = L_2->___BeingHeld_4;
		if (!L_3)
		{
			goto IL_002f;
		}
	}

IL_001a:
	{
		// playSoundInterval(0, 0.2f, 0.9f);
		WeaponSlide_playSoundInterval_m941C21C22FA99E5C7972C6BDA65D973B810D9504(__this, (0.0f), (0.200000003f), (0.899999976f), NULL);
	}

IL_002f:
	{
		// if (parentWeapon != null) {
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_4 = __this->___parentWeapon_11;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		// parentWeapon.OnWeaponCharged(true);
		RaycastWeapon_tE6CA7DE0198C91F246721AF51AA79070E4DBF160* L_6 = __this->___parentWeapon_11;
		NullCheck(L_6);
		VirtualActionInvoker1< bool >::Invoke(39 /* System.Void BNG.RaycastWeapon::OnWeaponCharged(System.Boolean) */, L_6, (bool)1);
	}

IL_0049:
	{
		// slidingBack = false;
		__this->___slidingBack_6 = (bool)0;
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::onSlideForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_onSlideForward_m4D0BD2AF7091C4BC01A4325210DA5E977C3F2AAB (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	{
		// if (thisGrabbable.BeingHeld || parentGrabbable.BeingHeld) {
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_0 = __this->___thisGrabbable_14;
		NullCheck(L_0);
		bool L_1 = L_0->___BeingHeld_4;
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_2 = __this->___parentGrabbable_12;
		NullCheck(L_2);
		bool L_3 = L_2->___BeingHeld_4;
		if (!L_3)
		{
			goto IL_002f;
		}
	}

IL_001a:
	{
		// playSoundInterval(0.2f, 0.35f, 1f);
		WeaponSlide_playSoundInterval_m941C21C22FA99E5C7972C6BDA65D973B810D9504(__this, (0.200000003f), (0.349999994f), (1.0f), NULL);
	}

IL_002f:
	{
		// slidingBack = true;
		__this->___slidingBack_6 = (bool)1;
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::LockSlidePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_LockSlidePosition_m21C4B6EAE4D89B04CAA8AB8CCE296FD50CAD7E39 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	{
		// if (parentGrabbable.BeingHeld && !thisGrabbable.BeingHeld && !lockSlidePosition) {
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_0 = __this->___parentGrabbable_12;
		NullCheck(L_0);
		bool L_1 = L_0->___BeingHeld_4;
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		Grabbable_tFB74479A02DEB046F7DAE97F0349232AC3456E5B* L_2 = __this->___thisGrabbable_14;
		NullCheck(L_2);
		bool L_3 = L_2->___BeingHeld_4;
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		bool L_4 = __this->___lockSlidePosition_19;
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		// _lockPosition = transform.localPosition;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5;
		L_5 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_5);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Transform_get_localPosition_mA9C86B990DF0685EA1061A120218993FDCC60A95(L_5, NULL);
		__this->____lockPosition_18 = L_6;
		// lockSlidePosition = true;
		__this->___lockSlidePosition_19 = (bool)1;
	}

IL_003a:
	{
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::UnlockSlidePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_UnlockSlidePosition_mCA73C09C492CC1C5C7ABA8172FE9EFD03D20A1E8 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	{
		// if (lockSlidePosition) {
		bool L_0 = __this->___lockSlidePosition_19;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// StartCoroutine(UnlockSlideRoutine());
		RuntimeObject* L_1;
		L_1 = WeaponSlide_UnlockSlideRoutine_m91E0F77885CA06F79595EC40712EF1469F0F4427(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_2;
		L_2 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_1, NULL);
	}

IL_0015:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator BNG.WeaponSlide::UnlockSlideRoutine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WeaponSlide_UnlockSlideRoutine_m91E0F77885CA06F79595EC40712EF1469F0F4427 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* L_0 = (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245*)il2cpp_codegen_object_new(U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CUnlockSlideRoutineU3Ed__27__ctor_m2CBFD2C282CB8F21315E20BAD2F9AB8E9BC336E7(L_0, 0, NULL);
		U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void BNG.WeaponSlide::playSoundInterval(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide_playSoundInterval_m941C21C22FA99E5C7972C6BDA65D973B810D9504 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, float ___fromSeconds0, float ___toSeconds1, float ___volume2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (audioSource) {
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0 = __this->___audioSource_15;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_0, NULL);
		if (!L_1)
		{
			goto IL_006d;
		}
	}
	{
		// if (audioSource.isPlaying) {
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_2 = __this->___audioSource_15;
		NullCheck(L_2);
		bool L_3;
		L_3 = AudioSource_get_isPlaying_mC203303F2F7146B2C056CB47B9391463FDF408FC(L_2, NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		// audioSource.Stop();
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_4 = __this->___audioSource_15;
		NullCheck(L_4);
		AudioSource_Stop_m318F17F17A147C77FF6E0A5A7A6BE057DB90F537(L_4, NULL);
	}

IL_0025:
	{
		// audioSource.pitch = Time.timeScale;
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_5 = __this->___audioSource_15;
		float L_6;
		L_6 = Time_get_timeScale_m99F3D47F45286D6DA73ADB2662B63451A632D413(NULL);
		NullCheck(L_5);
		AudioSource_set_pitch_mD14631FC99BF38AAFB356D9C45546BC16CF9E811(L_5, L_6, NULL);
		// audioSource.time = fromSeconds;
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_7 = __this->___audioSource_15;
		float L_8 = ___fromSeconds0;
		NullCheck(L_7);
		AudioSource_set_time_m6670372FD9C494978B7B3E01B7F4D220616F6204(L_7, L_8, NULL);
		// audioSource.volume = volume;
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_9 = __this->___audioSource_15;
		float L_10 = ___volume2;
		NullCheck(L_9);
		AudioSource_set_volume_mD902BBDBBDE0E3C148609BF3C05096148E90F2C0(L_9, L_10, NULL);
		// audioSource.Play();
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_11 = __this->___audioSource_15;
		NullCheck(L_11);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_11, NULL);
		// audioSource.SetScheduledEndTime(AudioSettings.dspTime + (toSeconds - fromSeconds));
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_12 = __this->___audioSource_15;
		double L_13;
		L_13 = AudioSettings_get_dspTime_m811568DA82A4831AFB4E4A6527C01EA29E719538(NULL);
		float L_14 = ___toSeconds1;
		float L_15 = ___fromSeconds0;
		NullCheck(L_12);
		AudioSource_SetScheduledEndTime_mC9BF39919029A6C6CB8981B09A792D45A60A3730(L_12, ((double)il2cpp_codegen_add(L_13, ((double)((float)il2cpp_codegen_subtract(L_14, L_15))))), NULL);
	}

IL_006d:
	{
		// }
		return;
	}
}
// System.Void BNG.WeaponSlide::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponSlide__ctor_mD9256125174C4D278F697F9FC6E1A11E7F8CD189 (WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* __this, const RuntimeMethod* method) 
{
	{
		// public float MinLocalZ = -0.03f;
		__this->___MinLocalZ_4 = (-0.0299999993f);
		// bool slidingBack = true;
		__this->___slidingBack_6 = (bool)1;
		// public bool ZeroMassWhenNotHeld = true;
		__this->___ZeroMassWhenNotHeld_10 = (bool)1;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUnlockSlideRoutineU3Ed__27__ctor_m2CBFD2C282CB8F21315E20BAD2F9AB8E9BC336E7 (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUnlockSlideRoutineU3Ed__27_System_IDisposable_Dispose_m5257B510051CF3C319CADFB95624EB081D04AC7C (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean BNG.WeaponSlide/<UnlockSlideRoutine>d__27::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CUnlockSlideRoutineU3Ed__27_MoveNext_m1816811DF0963EEDFFBA6832607FB36A2CE09678 (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return new WaitForSeconds(0.2f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_4 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_4, (0.200000003f), NULL);
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0037:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// lockSlidePosition = false;
		WeaponSlide_t4D8D93DBB6A884D1EFB180AFCF8D5F5DA4A233A2* L_5 = V_1;
		NullCheck(L_5);
		L_5->___lockSlidePosition_19 = (bool)0;
		// }
		return (bool)0;
	}
}
// System.Object BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CUnlockSlideRoutineU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28675E321BDCDA94330AAFB67629EC1178E1B882 (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_Reset_m080CAB29CBCEA62C742FB8CCDCD01008A73AAF38 (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_Reset_m080CAB29CBCEA62C742FB8CCDCD01008A73AAF38_RuntimeMethod_var)));
	}
}
// System.Object BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_get_Current_m287CA3CCBB45A3B2F5E3F4F656C74EFBABA460AD (U3CUnlockSlideRoutineU3Ed__27_t9D911AC2B19EDBF49135E58AC7EAB62478E44245* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DrunkMan.AutoRotate::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoRotate_Update_mB4CC663D513969C85FFA56286927A5F23A4E0B75 (AutoRotate_tC91AFA738AEA5EC3E037D7C011A3070113E0FE49* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		// float angle = Time.deltaTime * m_Speed;
		float L_0;
		L_0 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		float L_1 = __this->___m_Speed_4;
		V_0 = ((float)il2cpp_codegen_multiply(L_0, L_1));
		// transform.Rotate(angle, angle, 0f);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2;
		L_2 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		float L_3 = V_0;
		float L_4 = V_0;
		NullCheck(L_2);
		Transform_Rotate_m7EA47AD57F43D478CCB0523D179950EE49CDA3E2(L_2, L_3, L_4, (0.0f), NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.AutoRotate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoRotate__ctor_mB488C37BA5EC247F5F4E8BA1BE95F04F27B6F7AB (AutoRotate_tC91AFA738AEA5EC3E037D7C011A3070113E0FE49* __this, const RuntimeMethod* method) 
{
	{
		// public float m_Speed = 30f;
		__this->___m_Speed_4 = (30.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DrunkMan.FreeCamera::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCamera_Start_m884ACDAD9E4BFEFEAFEA38093E783F755B7CF56E (FreeCamera_t17D2C98F0E4AE04AA5D7713B35EA0230081BC088* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Pipeline != null)
		RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E* L_0 = __this->___m_Pipeline_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// GraphicsSettings.renderPipelineAsset = m_Pipeline;   // URP
		RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E* L_2 = __this->___m_Pipeline_4;
		GraphicsSettings_set_renderPipelineAsset_mAD34EC54DF2B76755B88F38568F88A474B31FB79(L_2, NULL);
		return;
	}

IL_001a:
	{
		// GraphicsSettings.renderPipelineAsset = null;   // Built-in
		GraphicsSettings_set_renderPipelineAsset_mAD34EC54DF2B76755B88F38568F88A474B31FB79((RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E*)NULL, NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.FreeCamera::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCamera_Update_m45E25D344D55A52640F18034FFC27D7CE829D7B5 (FreeCamera_t17D2C98F0E4AE04AA5D7713B35EA0230081BC088* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector3 dir = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		V_0 = L_0;
		// Move(m_ForwardButton, ref dir, transform.forward);
		int32_t L_1 = __this->___m_ForwardButton_7;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2;
		L_2 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F(L_2, NULL);
		FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88(__this, L_1, (&V_0), L_3, NULL);
		// Move(m_BackwardButton, ref dir, -transform.forward);
		int32_t L_4 = __this->___m_BackwardButton_8;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5;
		L_5 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_5);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F(L_5, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_op_UnaryNegation_m3AC523A7BED6E843165BDF598690F0560D8CAA63_inline(L_6, NULL);
		FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88(__this, L_4, (&V_0), L_7, NULL);
		// Move(m_RightButton, ref dir, transform.right);
		int32_t L_8 = __this->___m_RightButton_9;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_9;
		L_9 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_9);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		L_10 = Transform_get_right_mC6DC057C23313802E2186A9E0DB760D795A758A4(L_9, NULL);
		FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88(__this, L_8, (&V_0), L_10, NULL);
		// Move(m_LeftButton, ref dir, -transform.right);
		int32_t L_11 = __this->___m_LeftButton_10;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_12;
		L_12 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_12);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Transform_get_right_mC6DC057C23313802E2186A9E0DB760D795A758A4(L_12, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		L_14 = Vector3_op_UnaryNegation_m3AC523A7BED6E843165BDF598690F0560D8CAA63_inline(L_13, NULL);
		FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88(__this, L_11, (&V_0), L_14, NULL);
		// Move(m_UpButton, ref dir, transform.up);
		int32_t L_15 = __this->___m_UpButton_11;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_16;
		L_16 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_16);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17;
		L_17 = Transform_get_up_mE47A9D9D96422224DD0539AA5524DA5440145BB2(L_16, NULL);
		FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88(__this, L_15, (&V_0), L_17, NULL);
		// Move(m_DownButton, ref dir, -transform.up);
		int32_t L_18 = __this->___m_DownButton_12;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_19;
		L_19 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_19);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20;
		L_20 = Transform_get_up_mE47A9D9D96422224DD0539AA5524DA5440145BB2(L_19, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		L_21 = Vector3_op_UnaryNegation_m3AC523A7BED6E843165BDF598690F0560D8CAA63_inline(L_20, NULL);
		FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88(__this, L_18, (&V_0), L_21, NULL);
		// transform.position += dir * m_MoveSpeed * Time.deltaTime;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_22;
		L_22 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_23 = L_22;
		NullCheck(L_23);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		L_24 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_23, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25 = V_0;
		float L_26 = __this->___m_MoveSpeed_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27;
		L_27 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_25, L_26, NULL);
		float L_28;
		L_28 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
		L_29 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_27, L_28, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30;
		L_30 = Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline(L_24, L_29, NULL);
		NullCheck(L_23);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_23, L_30, NULL);
		// if (Input.GetMouseButton(0))
		bool L_31;
		L_31 = Input_GetMouseButton_mE545CF4B790C6E202808B827E3141BEC3330DB70(0, NULL);
		if (!L_31)
		{
			goto IL_013c;
		}
	}
	{
		// Vector3 eulerAngles = transform.eulerAngles;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_32;
		L_32 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_32);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Transform_get_eulerAngles_mCAAF48EFCF628F1ED91C2FFE75A4FD19C039DD6A(L_32, NULL);
		V_1 = L_33;
		// eulerAngles.x += -Input.GetAxis("Mouse Y") * 359f * m_RotateSpeed;
		float* L_34 = (&(&V_1)->___x_2);
		float* L_35 = L_34;
		float L_36 = *((float*)L_35);
		float L_37;
		L_37 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, NULL);
		float L_38 = __this->___m_RotateSpeed_6;
		*((float*)L_35) = (float)((float)il2cpp_codegen_add(L_36, ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(((-L_37)), (359.0f))), L_38))));
		// eulerAngles.y += Input.GetAxis("Mouse X") * 359f * m_RotateSpeed;
		float* L_39 = (&(&V_1)->___y_3);
		float* L_40 = L_39;
		float L_41 = *((float*)L_40);
		float L_42;
		L_42 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, NULL);
		float L_43 = __this->___m_RotateSpeed_6;
		*((float*)L_40) = (float)((float)il2cpp_codegen_add(L_41, ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_42, (359.0f))), L_43))));
		// transform.eulerAngles = eulerAngles;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_44;
		L_44 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_45 = V_1;
		NullCheck(L_44);
		Transform_set_eulerAngles_m9F0BC484A7915A51FAB87230644229B75BACA004(L_44, L_45, NULL);
	}

IL_013c:
	{
		// }
		return;
	}
}
// System.Void DrunkMan.FreeCamera::Move(UnityEngine.KeyCode,UnityEngine.Vector3&,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88 (FreeCamera_t17D2C98F0E4AE04AA5D7713B35EA0230081BC088* __this, int32_t ___key0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___moveTo1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___dir2, const RuntimeMethod* method) 
{
	{
		// if (Input.GetKey(key))
		int32_t L_0 = ___key0;
		bool L_1;
		L_1 = Input_GetKey_m0BF0499CADC378F02B6BEE2399FB945AB929B81A(L_0, NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// moveTo = dir;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_2 = ___moveTo1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___dir2;
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_2 = L_3;
	}

IL_000f:
	{
		// }
		return;
	}
}
// System.Void DrunkMan.FreeCamera::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCamera__ctor_m88DE606C2BE2621710E591509AD05962E68571AB (FreeCamera_t17D2C98F0E4AE04AA5D7713B35EA0230081BC088* __this, const RuntimeMethod* method) 
{
	{
		// public KeyCode m_ForwardButton = KeyCode.W;
		__this->___m_ForwardButton_7 = ((int32_t)119);
		// public KeyCode m_BackwardButton = KeyCode.S;
		__this->___m_BackwardButton_8 = ((int32_t)115);
		// public KeyCode m_RightButton = KeyCode.D;
		__this->___m_RightButton_9 = ((int32_t)100);
		// public KeyCode m_LeftButton = KeyCode.A;
		__this->___m_LeftButton_10 = ((int32_t)97);
		// public KeyCode m_UpButton = KeyCode.Q;
		__this->___m_UpButton_11 = ((int32_t)113);
		// public KeyCode m_DownButton = KeyCode.E;
		__this->___m_DownButton_12 = ((int32_t)101);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DrunkMan.DrunkManFeature::Create()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkManFeature_Create_mE58D6E22C2001E91170E6EF35D37AD220D984D5A (DrunkManFeature_tFED478401670ECFD6FF972AA05597D399B655C11* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_BlitPass = new BlitPass(name);
		String_t* L_0;
		L_0 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(__this, NULL);
		BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* L_1 = (BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8*)il2cpp_codegen_object_new(BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		BlitPass__ctor_m96534D9B89F7E34A0AADA29783F6F315D54F6F06(L_1, L_0, NULL);
		__this->___m_BlitPass_10 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_BlitPass_10), (void*)L_1);
		// }
		return;
	}
}
// System.Void DrunkMan.DrunkManFeature::AddRenderPasses(UnityEngine.Rendering.Universal.ScriptableRenderer,UnityEngine.Rendering.Universal.RenderingData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkManFeature_AddRenderPasses_m62F67A9447ED874707E7E1852E3C4E4470F11F2E (DrunkManFeature_tFED478401670ECFD6FF972AA05597D399B655C11* __this, ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* ___renderer0, RenderingData_tAAA01190551D6D5954314E3E1E85B58DD45EED71* ___renderingData1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisStatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87_m6D7084B39D7647C73A971160764AD450B56326F1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral019188D0A51A0F9C7D99991CF3646D685A087B71);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21627BBE8EBCFC824DEF784AC25AB0E7B675D01D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2CFD5C1DB1662C9ACCEC5B8AE7260D552F5A83AC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral405CCEE21C770033E6F661F8E266E281B6A6E0FE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral486F1BA05E3312BE67C1B74F652C6E2D7A691600);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B983EEC590B83119333B9EC9CE00399B0CFFDD1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2DBA484FA2F0A6149D2EF4C993EA43125C8E31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral63B88C4EFDF4F8C73E03469100CADA32CD8BD835);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8714C9231CFAC05EA788784A479528F0DBA10DA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral98DF8830FA14DA365C6B54CB182D990F6F2F2A0E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C40692ED77768D578327A3E71988DFF501534A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA897552B9939370E6487698DB3A96C96CEE06466);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBA0CE45FD5A657EDBA8FB9361FC9EACBFD7C6E87);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE06B53F9B6FDDA6A65F38986E1BA6B03495562F);
		s_Il2CppMethodInitialized = true;
	}
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (manager == null) manager = GameObject.FindGameObjectWithTag("Bartender");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___manager_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		// if (manager == null) manager = GameObject.FindGameObjectWithTag("Bartender");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_FindGameObjectWithTag_m17078A0823CA9699710251C617B95D04D57098A9(_stringLiteral5D2DBA484FA2F0A6149D2EF4C993EA43125C8E31, NULL);
		__this->___manager_6 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___manager_6), (void*)L_2);
	}

IL_001e:
	{
		// if (manager != null && sm == null) sm = manager.GetComponent<StatsManager>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___manager_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* L_5 = __this->___sm_5;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_5, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_6)
		{
			goto IL_004b;
		}
	}
	{
		// if (manager != null && sm == null) sm = manager.GetComponent<StatsManager>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___manager_6;
		NullCheck(L_7);
		StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* L_8;
		L_8 = GameObject_GetComponent_TisStatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87_m6D7084B39D7647C73A971160764AD450B56326F1(L_7, GameObject_GetComponent_TisStatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87_m6D7084B39D7647C73A971160764AD450B56326F1_RuntimeMethod_var);
		__this->___sm_5 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___sm_5), (void*)L_8);
	}

IL_004b:
	{
		// if (sm != null) abv = sm.GetABV();
		StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* L_9 = __this->___sm_5;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_9, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		// if (sm != null) abv = sm.GetABV();
		StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* L_11 = __this->___sm_5;
		NullCheck(L_11);
		float L_12;
		L_12 = StatsManager_GetABV_mEA7946CB824D7AE211EF9B8AD560C64AEFEFF8CD_inline(L_11, NULL);
		__this->___abv_7 = L_12;
	}

IL_006a:
	{
		// if (abv < 9.0f)
		float L_13 = __this->___abv_7;
		if ((!(((float)L_13) < ((float)(9.0f)))))
		{
			goto IL_0078;
		}
	}
	{
		// return;
		return;
	}

IL_0078:
	{
		// var src = renderer.cameraColorTarget;
		ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* L_14 = ___renderer0;
		NullCheck(L_14);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_15;
		L_15 = ScriptableRenderer_get_cameraColorTarget_mC2C0353A178726FC82413A458A34496280AFB4D4(L_14, NULL);
		V_0 = L_15;
		// if (m_Settings.m_Mat == null)
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_16 = __this->___m_Settings_9;
		NullCheck(L_16);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_17 = L_16->___m_Mat_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_18;
		L_18 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_17, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_18)
		{
			goto IL_00b1;
		}
	}
	{
		// Debug.LogWarningFormat("Missing material. {0} blit pass will not execute. Check for missing reference in the assigned renderer.", GetType().Name);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_19 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_20 = L_19;
		Type_t* L_21;
		L_21 = Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3(__this, NULL);
		NullCheck(L_21);
		String_t* L_22;
		L_22 = VirtualFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_22);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarningFormat_m0D4A31935564D0FA042103C1231DBBD2ED0BC20A(_stringLiteral2CFD5C1DB1662C9ACCEC5B8AE7260D552F5A83AC, L_20, NULL);
		// return;
		return;
	}

IL_00b1:
	{
		// Material m = m_Settings.m_Mat;
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_23 = __this->___m_Settings_9;
		NullCheck(L_23);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_24 = L_23->___m_Mat_0;
		// m.SetFloat("_RGBShiftFactor", 0.0f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_25 = L_24;
		NullCheck(L_25);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_25, _stringLiteral8714C9231CFAC05EA788784A479528F0DBA10DA9, (0.0f), NULL);
		// m.SetFloat("_RGBShiftPower", 1.0f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_26 = L_25;
		NullCheck(L_26);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_26, _stringLiteral486F1BA05E3312BE67C1B74F652C6E2D7A691600, (1.0f), NULL);
		// m.SetFloat("_GhostSeeRadius", abv * .000371f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_27 = L_26;
		float L_28 = __this->___abv_7;
		NullCheck(L_27);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_27, _stringLiteral63B88C4EFDF4F8C73E03469100CADA32CD8BD835, ((float)il2cpp_codegen_multiply(L_28, (0.000371000002f))), NULL);
		// m.SetFloat("_GhostSeeMix", abv * .00796f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_29 = L_27;
		float L_30 = __this->___abv_7;
		NullCheck(L_29);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_29, _stringLiteral9C40692ED77768D578327A3E71988DFF501534A4, ((float)il2cpp_codegen_multiply(L_30, (0.00796000008f))), NULL);
		// m.SetFloat("_GhostSeeAmplitude", abv * .00559f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_31 = L_29;
		float L_32 = __this->___abv_7;
		NullCheck(L_31);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_31, _stringLiteral98DF8830FA14DA365C6B54CB182D990F6F2F2A0E, ((float)il2cpp_codegen_multiply(L_32, (0.00559000019f))), NULL);
		// m.SetVector("_Dimensions", new Vector4(0.8f, m_Settings.m_EyeClose, 0f, 0f));
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_33 = L_31;
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_34 = __this->___m_Settings_9;
		NullCheck(L_34);
		float L_35 = L_34->___m_EyeClose_15;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_36;
		memset((&L_36), 0, sizeof(L_36));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_36), (0.800000012f), L_35, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Material_SetVector_m69444B8040D955821F241113446CC8713C9E12D1(L_33, _stringLiteral21627BBE8EBCFC824DEF784AC25AB0E7B675D01D, L_36, NULL);
		// m.SetFloat("_Frequency", abv * .00323f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_37 = L_33;
		float L_38 = __this->___abv_7;
		NullCheck(L_37);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_37, _stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E, ((float)il2cpp_codegen_multiply(L_38, (0.00322999991f))), NULL);
		// m.SetFloat("_Period", abv * .00242f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_39 = L_37;
		float L_40 = __this->___abv_7;
		NullCheck(L_39);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_39, _stringLiteralBA0CE45FD5A657EDBA8FB9361FC9EACBFD7C6E87, ((float)il2cpp_codegen_multiply(L_40, (0.00242000003f))), NULL);
		// m.SetFloat("_RandomNumber", 1f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_41 = L_39;
		NullCheck(L_41);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_41, _stringLiteral5B983EEC590B83119333B9EC9CE00399B0CFFDD1, (1.0f), NULL);
		// m.SetFloat("_Amplitude", abv * .00645f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_42 = L_41;
		float L_43 = __this->___abv_7;
		NullCheck(L_42);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_42, _stringLiteralEE06B53F9B6FDDA6A65F38986E1BA6B03495562F, ((float)il2cpp_codegen_multiply(L_43, (0.00645000022f))), NULL);
		// m.SetFloat("_BlurMin", abv * .000741f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_44 = L_42;
		float L_45 = __this->___abv_7;
		NullCheck(L_44);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_44, _stringLiteralA897552B9939370E6487698DB3A96C96CEE06466, ((float)il2cpp_codegen_multiply(L_45, (0.000740999996f))), NULL);
		// m.SetFloat("_BlurMax", abv * .00259f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_46 = L_44;
		float L_47 = __this->___abv_7;
		NullCheck(L_46);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_46, _stringLiteral405CCEE21C770033E6F661F8E266E281B6A6E0FE, ((float)il2cpp_codegen_multiply(L_47, (0.00258999993f))), NULL);
		// m.SetFloat("_BlurSpeed", abv * .00926f);
		float L_48 = __this->___abv_7;
		NullCheck(L_46);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_46, _stringLiteral019188D0A51A0F9C7D99991CF3646D685A087B71, ((float)il2cpp_codegen_multiply(L_48, (0.00925999973f))), NULL);
		// m_BlitPass.Setup(src, m_Settings.m_Mat, m_Settings.m_SleepyEye, m_Settings.m_Type, m_Settings.m_Intensity);
		BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* L_49 = __this->___m_BlitPass_10;
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_50 = V_0;
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_51 = __this->___m_Settings_9;
		NullCheck(L_51);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_52 = L_51->___m_Mat_0;
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_53 = __this->___m_Settings_9;
		NullCheck(L_53);
		bool L_54 = L_53->___m_SleepyEye_14;
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_55 = __this->___m_Settings_9;
		NullCheck(L_55);
		int32_t L_56 = L_55->___m_Type_1;
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_57 = __this->___m_Settings_9;
		NullCheck(L_57);
		float L_58 = L_57->___m_Intensity_2;
		NullCheck(L_49);
		BlitPass_Setup_mBBA957EC6EE0D9F34629B37432FE068557DBCE9B(L_49, L_50, L_52, L_54, L_56, L_58, NULL);
		// renderer.EnqueuePass(m_BlitPass);
		ScriptableRenderer_tF15B95BB85F26BE4B4719901D909831B89DC8892* L_59 = ___renderer0;
		BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* L_60 = __this->___m_BlitPass_10;
		NullCheck(L_59);
		ScriptableRenderer_EnqueuePass_m62AC5EFBA8DECFD514CAFC4EFDCFBF88C710954F(L_59, L_60, NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.DrunkManFeature::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkManFeature__ctor_m54EFBB210A79E0A49EE8D6E84333A0AF5A42658A (DrunkManFeature_tFED478401670ECFD6FF972AA05597D399B655C11* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Settings m_Settings = new Settings();
		Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* L_0 = (Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463*)il2cpp_codegen_object_new(Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Settings__ctor_mA5D76ECE115DEE0E6F4879C3772A4C79E274ECED(L_0, NULL);
		__this->___m_Settings_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Settings_9), (void*)L_0);
		ScriptableRendererFeature__ctor_mA05EC9569A5DCF48CDD98E1FC5838857E2C4C001(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DrunkMan.DrunkManFeature/BlitPass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlitPass__ctor_m96534D9B89F7E34A0AADA29783F6F315D54F6F06 (BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* __this, String_t* ___tag0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC34E69D38B35322888754CDF598BC35F2852041E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD0420C099ECAC4F9F3312E56381F79F3B4CE7BEA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bool m_SleepyEye = true;
		__this->___m_SleepyEye_29 = (bool)1;
		// public BlitPass(string tag)
		ScriptableRenderPass__ctor_mE49D4FF8E68A854367A4081E664B8DBA74E6B752(__this, NULL);
		// this.renderPassEvent = RenderPassEvent.AfterRenderingTransparents;
		ScriptableRenderPass_set_renderPassEvent_m63FA581FFDE1C69C2E1358BD0B8DB30275334960_inline(__this, ((int32_t)500), NULL);
		// m_Tag = tag;
		String_t* L_0 = ___tag0;
		__this->___m_Tag_28 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Tag_28), (void*)L_0);
		// m_RtHandle1.Init("_Rt1");
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_1 = (&__this->___m_RtHandle1_26);
		RenderTargetHandle_Init_mDF9383A0DB5E0B56577BA43CC56CD659F8970646(L_1, _stringLiteralC34E69D38B35322888754CDF598BC35F2852041E, NULL);
		// m_RtHandle2.Init("_Rt2");
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_2 = (&__this->___m_RtHandle2_27);
		RenderTargetHandle_Init_mDF9383A0DB5E0B56577BA43CC56CD659F8970646(L_2, _stringLiteralD0420C099ECAC4F9F3312E56381F79F3B4CE7BEA, NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.DrunkManFeature/BlitPass::Setup(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material,System.Boolean,DrunkMan.DrunkManFeature/EType,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlitPass_Setup_mBBA957EC6EE0D9F34629B37432FE068557DBCE9B (BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* __this, RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___src0, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat1, bool ___sleepyEye2, int32_t ___tp3, float ___intensity4, const RuntimeMethod* method) 
{
	{
		// m_Source = src;
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_0 = ___src0;
		__this->___m_Source_25 = L_0;
		// m_Mat = mat;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_1 = ___mat1;
		__this->___m_Mat_24 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Mat_24), (void*)L_1);
		// m_SleepyEye = sleepyEye;
		bool L_2 = ___sleepyEye2;
		__this->___m_SleepyEye_29 = L_2;
		// m_Type = tp;
		int32_t L_3 = ___tp3;
		__this->___m_Type_30 = L_3;
		// m_Intensity = intensity;
		float L_4 = ___intensity4;
		__this->___m_Intensity_31 = L_4;
		// }
		return;
	}
}
// System.Void DrunkMan.DrunkManFeature/BlitPass::Execute(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Rendering.Universal.RenderingData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlitPass_Execute_m95032E68BFEF0AF661662E86B18DDC9F069E33B3 (BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* __this, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 ___context0, RenderingData_tAAA01190551D6D5954314E3E1E85B58DD45EED71* ___renderingData1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CommandBufferPool_t88CACA06AB445EE4743F5C4D742C73761A2DEF0F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral005930A9597B0BEA2250DA712041C3035CEB8736);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B3FA2417F9F161F82E54814DD98BF6B7ACE3837);
		s_Il2CppMethodInitialized = true;
	}
	CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* V_0 = NULL;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// CommandBuffer cmd = CommandBufferPool.Get(m_Tag);
		String_t* L_0 = __this->___m_Tag_28;
		il2cpp_codegen_runtime_class_init_inline(CommandBufferPool_t88CACA06AB445EE4743F5C4D742C73761A2DEF0F_il2cpp_TypeInfo_var);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_1;
		L_1 = CommandBufferPool_Get_mC33780CD170099A0E396A2F3A9AFB46509B31625(L_0, NULL);
		V_0 = L_1;
		// RenderTextureDescriptor desc = renderingData.cameraData.cameraTargetDescriptor;
		RenderingData_tAAA01190551D6D5954314E3E1E85B58DD45EED71* L_2 = ___renderingData1;
		CameraData_tC27AE109CD20677486A4AC19C0CF014AE0F50C3E* L_3 = (&L_2->___cameraData_1);
		RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 L_4 = L_3->___cameraTargetDescriptor_5;
		V_1 = L_4;
		// desc.depthBufferBits = 0;
		RenderTextureDescriptor_set_depthBufferBits_mA3710C0D6E485BA6465B328CD8B1954F0E4C5819((&V_1), 0, NULL);
		// cmd.GetTemporaryRT(m_RtHandle1.id, desc, FilterMode.Bilinear);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_5 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_6 = (&__this->___m_RtHandle1_26);
		int32_t L_7;
		L_7 = RenderTargetHandle_get_id_m4D50FDA4A486E05D07A54ABFC04BD96C1CE7D7BE_inline(L_6, NULL);
		RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 L_8 = V_1;
		NullCheck(L_5);
		CommandBuffer_GetTemporaryRT_m98BCBFF670DDD3AC8657664F8252A9DF64D49FA5(L_5, L_7, L_8, 1, NULL);
		// cmd.GetTemporaryRT(m_RtHandle2.id, desc, FilterMode.Bilinear);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_9 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_10 = (&__this->___m_RtHandle2_27);
		int32_t L_11;
		L_11 = RenderTargetHandle_get_id_m4D50FDA4A486E05D07A54ABFC04BD96C1CE7D7BE_inline(L_10, NULL);
		RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 L_12 = V_1;
		NullCheck(L_9);
		CommandBuffer_GetTemporaryRT_m98BCBFF670DDD3AC8657664F8252A9DF64D49FA5(L_9, L_11, L_12, 1, NULL);
		// cmd.SetGlobalTexture("_Global_OrigScene", m_Source);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_13 = V_0;
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_14 = __this->___m_Source_25;
		NullCheck(L_13);
		CommandBuffer_SetGlobalTexture_mD6F1CC7E87FA88B5838D5EDAFBA602EF94FE1F69(L_13, _stringLiteral5B3FA2417F9F161F82E54814DD98BF6B7ACE3837, L_14, NULL);
		// cmd.SetGlobalFloat("_Global_Fade", m_Intensity);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_15 = V_0;
		float L_16 = __this->___m_Intensity_31;
		NullCheck(L_15);
		CommandBuffer_SetGlobalFloat_mECD0FBFDF115D9150B5D1DB66010B17F6213419B(L_15, _stringLiteral005930A9597B0BEA2250DA712041C3035CEB8736, L_16, NULL);
		// Blit(cmd, m_Source, m_RtHandle1.Identifier(), m_Mat, 0);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_17 = V_0;
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_18 = __this->___m_Source_25;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_19 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_20;
		L_20 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_19, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_17, L_18, L_20, L_21, 0, NULL);
		// if (EType.Rotated == m_Type)
		int32_t L_22 = __this->___m_Type_30;
		if (L_22)
		{
			goto IL_0191;
		}
	}
	{
		// if (m_SleepyEye)
		bool L_23 = __this->___m_SleepyEye_29;
		if (!L_23)
		{
			goto IL_012a;
		}
	}
	{
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 1);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_24 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_25 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_26;
		L_26 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_25, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_27 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_28;
		L_28 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_27, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_29 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_24, L_26, L_28, L_29, 1, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_RtHandle1.Identifier(), m_Mat, 4);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_30 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_31 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_32;
		L_32 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_31, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_33 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_34;
		L_34 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_33, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_35 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_30, L_32, L_34, L_35, 4, NULL);
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 5);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_36 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_37 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_38;
		L_38 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_37, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_39 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_40;
		L_40 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_39, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_41 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_36, L_38, L_40, L_41, 5, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_Source, m_Mat, 3);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_42 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_43 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_44;
		L_44 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_43, NULL);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_45 = __this->___m_Source_25;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_46 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_42, L_44, L_45, L_46, 3, NULL);
		goto IL_0191;
	}

IL_012a:
	{
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 1);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_47 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_48 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_49;
		L_49 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_48, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_50 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_51;
		L_51 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_50, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_52 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_47, L_49, L_51, L_52, 1, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_RtHandle1.Identifier(), m_Mat, 4);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_53 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_54 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_55;
		L_55 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_54, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_56 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_57;
		L_57 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_56, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_58 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_53, L_55, L_57, L_58, 4, NULL);
		// Blit(cmd, m_RtHandle1.Identifier(), m_Source, m_Mat, 5);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_59 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_60 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_61;
		L_61 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_60, NULL);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_62 = __this->___m_Source_25;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_63 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_59, L_61, L_62, L_63, 5, NULL);
	}

IL_0191:
	{
		// if (EType.Splitted == m_Type)
		int32_t L_64 = __this->___m_Type_30;
		if ((!(((uint32_t)1) == ((uint32_t)L_64))))
		{
			goto IL_029c;
		}
	}
	{
		// if (m_SleepyEye)
		bool L_65 = __this->___m_SleepyEye_29;
		if (!L_65)
		{
			goto IL_0235;
		}
	}
	{
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 2);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_66 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_67 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_68;
		L_68 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_67, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_69 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_70;
		L_70 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_69, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_71 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_66, L_68, L_70, L_71, 2, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_RtHandle1.Identifier(), m_Mat, 4);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_72 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_73 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_74;
		L_74 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_73, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_75 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_76;
		L_76 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_75, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_77 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_72, L_74, L_76, L_77, 4, NULL);
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 5);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_78 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_79 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_80;
		L_80 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_79, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_81 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_82;
		L_82 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_81, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_83 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_78, L_80, L_82, L_83, 5, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_Source, m_Mat, 3);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_84 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_85 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_86;
		L_86 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_85, NULL);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_87 = __this->___m_Source_25;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_88 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_84, L_86, L_87, L_88, 3, NULL);
		goto IL_029c;
	}

IL_0235:
	{
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 2);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_89 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_90 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_91;
		L_91 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_90, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_92 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_93;
		L_93 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_92, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_94 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_89, L_91, L_93, L_94, 2, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_RtHandle1.Identifier(), m_Mat, 4);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_95 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_96 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_97;
		L_97 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_96, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_98 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_99;
		L_99 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_98, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_100 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_95, L_97, L_99, L_100, 4, NULL);
		// Blit(cmd, m_RtHandle1.Identifier(), m_Source, m_Mat, 5);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_101 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_102 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_103;
		L_103 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_102, NULL);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_104 = __this->___m_Source_25;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_105 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_101, L_103, L_104, L_105, 5, NULL);
	}

IL_029c:
	{
		// if (EType.Waggle == m_Type)
		int32_t L_106 = __this->___m_Type_30;
		if ((!(((uint32_t)2) == ((uint32_t)L_106))))
		{
			goto IL_035c;
		}
	}
	{
		// if (m_SleepyEye)
		bool L_107 = __this->___m_SleepyEye_29;
		if (!L_107)
		{
			goto IL_0319;
		}
	}
	{
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 4);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_108 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_109 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_110;
		L_110 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_109, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_111 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_112;
		L_112 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_111, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_113 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_108, L_110, L_112, L_113, 4, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_RtHandle1.Identifier(), m_Mat, 6);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_114 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_115 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_116;
		L_116 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_115, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_117 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_118;
		L_118 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_117, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_119 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_114, L_116, L_118, L_119, 6, NULL);
		// Blit(cmd, m_RtHandle1.Identifier(), m_Source, m_Mat, 3);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_120 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_121 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_122;
		L_122 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_121, NULL);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_123 = __this->___m_Source_25;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_124 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_120, L_122, L_123, L_124, 3, NULL);
		goto IL_035c;
	}

IL_0319:
	{
		// Blit(cmd, m_RtHandle1.Identifier(), m_RtHandle2.Identifier(), m_Mat, 4);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_125 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_126 = (&__this->___m_RtHandle1_26);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_127;
		L_127 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_126, NULL);
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_128 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_129;
		L_129 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_128, NULL);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_130 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_125, L_127, L_129, L_130, 4, NULL);
		// Blit(cmd, m_RtHandle2.Identifier(), m_Source, m_Mat, 6);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_131 = V_0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_132 = (&__this->___m_RtHandle2_27);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_133;
		L_133 = RenderTargetHandle_Identifier_mE7715B58419BC3E157BDCC906E92605F76BD4FBA(L_132, NULL);
		RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B L_134 = __this->___m_Source_25;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_135 = __this->___m_Mat_24;
		ScriptableRenderPass_Blit_m51EFEA549568C64221EFC6FFF66EC9078B290BEF(__this, L_131, L_133, L_134, L_135, 6, NULL);
	}

IL_035c:
	{
		// context.ExecuteCommandBuffer(cmd);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_136 = V_0;
		ScriptableRenderContext_ExecuteCommandBuffer_mBAE37DFC699B7167A6E2C59012066C44A31E9896((&___context0), L_136, NULL);
		// CommandBufferPool.Release(cmd);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_137 = V_0;
		il2cpp_codegen_runtime_class_init_inline(CommandBufferPool_t88CACA06AB445EE4743F5C4D742C73761A2DEF0F_il2cpp_TypeInfo_var);
		CommandBufferPool_Release_mEC46D8373A95DEC68F1FBD2D77FF3F76917631BF(L_137, NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.DrunkManFeature/BlitPass::FrameCleanup(UnityEngine.Rendering.CommandBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlitPass_FrameCleanup_m199FA46C75B328692D0070A4B846747D032F547C (BlitPass_t9EB7C2501FA911133DB97AD2C55CF64DD189BDB8* __this, CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* ___cmd0, const RuntimeMethod* method) 
{
	{
		// cmd.ReleaseTemporaryRT(m_RtHandle1.id);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_0 = ___cmd0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_1 = (&__this->___m_RtHandle1_26);
		int32_t L_2;
		L_2 = RenderTargetHandle_get_id_m4D50FDA4A486E05D07A54ABFC04BD96C1CE7D7BE_inline(L_1, NULL);
		NullCheck(L_0);
		CommandBuffer_ReleaseTemporaryRT_m4651A4B373DF432AA44F06A6F20852ED5996CC8E(L_0, L_2, NULL);
		// cmd.ReleaseTemporaryRT(m_RtHandle2.id);
		CommandBuffer_tB56007DC84EF56296C325EC32DD12AC1E3DC91F7* L_3 = ___cmd0;
		RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* L_4 = (&__this->___m_RtHandle2_27);
		int32_t L_5;
		L_5 = RenderTargetHandle_get_id_m4D50FDA4A486E05D07A54ABFC04BD96C1CE7D7BE_inline(L_4, NULL);
		NullCheck(L_3);
		CommandBuffer_ReleaseTemporaryRT_m4651A4B373DF432AA44F06A6F20852ED5996CC8E(L_3, L_5, NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DrunkMan.DrunkManFeature/Settings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Settings__ctor_mA5D76ECE115DEE0E6F4879C3772A4C79E274ECED (Settings_tAF4864691C6FD09A564DFF579F83D84BA08CB463* __this, const RuntimeMethod* method) 
{
	{
		// [Range(0f, 1f)] public float m_Intensity = 1f;
		__this->___m_Intensity_2 = (1.0f);
		// [Range(1f, 16f)] public float m_RGBShiftPower = 3f;
		__this->___m_RGBShiftPower_4 = (3.0f);
		// [Range(0f, 0.06f)] public float m_GhostSeeRadius = 0.01f;
		__this->___m_GhostSeeRadius_5 = (0.00999999978f);
		// [Range(0.01f, 1f)] public float m_GhostSeeMix = 0.5f;
		__this->___m_GhostSeeMix_6 = (0.5f);
		// [Range(0.01f, 0.2f)] public float m_GhostSeeAmplitude = 0.05f;
		__this->___m_GhostSeeAmplitude_7 = (0.0500000007f);
		// [Range(0.5f, 8f)] public float m_Frequency = 1f;
		__this->___m_Frequency_8 = (1.0f);
		// [Range(0.1f, 4f)] public float m_Period = 1.5f;
		__this->___m_Period_9 = (1.5f);
		// [Range(1f, 16f)] public float m_Amplitude = 1f;
		__this->___m_Amplitude_10 = (1.0f);
		// [Range(0f, 1f)] public float m_BlurMin = 0.1f;
		__this->___m_BlurMin_11 = (0.100000001f);
		// [Range(0f, 1f)] public float m_BlurMax = 0.3f;
		__this->___m_BlurMax_12 = (0.300000012f);
		// [Range(1f, 6f)] public float m_BlurSpeed = 3f;
		__this->___m_BlurSpeed_13 = (3.0f);
		// [Range(0f, 0.9f)] public float m_EyeClose = 0.4f;
		__this->___m_EyeClose_15 = (0.400000006f);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DrunkMan.BuiltIn.Demo::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Start_mCA0735C8ED3F0D68DB44B56D443BADEEC881EDE2 (Demo_tB2CE2F3309296F5900575784169917681D1E7D28* __this, const RuntimeMethod* method) 
{
	{
		// GraphicsSettings.renderPipelineAsset = null;   // disable URP
		GraphicsSettings_set_renderPipelineAsset_mAD34EC54DF2B76755B88F38568F88A474B31FB79((RenderPipelineAsset_t5F9BF815BF931E1314B184E7F9070FB649C7054E*)NULL, NULL);
		// QualitySettings.antiAliasing = 8;
		QualitySettings_set_antiAliasing_m1C7D6F2CFB2EC09BAE3081EFD1BF3EAAE1597B1A(8, NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.BuiltIn.Demo::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Update_m730C8AD00DCC914C56699CAEAE5ED493B5CE5FCD (Demo_tB2CE2F3309296F5900575784169917681D1E7D28* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral019188D0A51A0F9C7D99991CF3646D685A087B71);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21627BBE8EBCFC824DEF784AC25AB0E7B675D01D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral405CCEE21C770033E6F661F8E266E281B6A6E0FE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral486F1BA05E3312BE67C1B74F652C6E2D7A691600);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B983EEC590B83119333B9EC9CE00399B0CFFDD1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral63B88C4EFDF4F8C73E03469100CADA32CD8BD835);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8714C9231CFAC05EA788784A479528F0DBA10DA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral98DF8830FA14DA365C6B54CB182D990F6F2F2A0E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C40692ED77768D578327A3E71988DFF501534A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA897552B9939370E6487698DB3A96C96CEE06466);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBA0CE45FD5A657EDBA8FB9361FC9EACBFD7C6E87);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE06B53F9B6FDDA6A65F38986E1BA6B03495562F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Mat.SetFloat("_RGBShiftFactor", m_RGBShiftFactor);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = __this->___m_Mat_4;
		float L_1 = __this->___m_RGBShiftFactor_7;
		NullCheck(L_0);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_0, _stringLiteral8714C9231CFAC05EA788784A479528F0DBA10DA9, L_1, NULL);
		// m_Mat.SetFloat("_RGBShiftPower", m_RGBShiftPower);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_2 = __this->___m_Mat_4;
		float L_3 = __this->___m_RGBShiftPower_8;
		NullCheck(L_2);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_2, _stringLiteral486F1BA05E3312BE67C1B74F652C6E2D7A691600, L_3, NULL);
		// m_Mat.SetFloat("_GhostSeeRadius", m_GhostSeeRadius);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_4 = __this->___m_Mat_4;
		float L_5 = __this->___m_GhostSeeRadius_9;
		NullCheck(L_4);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_4, _stringLiteral63B88C4EFDF4F8C73E03469100CADA32CD8BD835, L_5, NULL);
		// m_Mat.SetFloat("_GhostSeeMix", m_GhostSeeMix);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = __this->___m_Mat_4;
		float L_7 = __this->___m_GhostSeeMix_10;
		NullCheck(L_6);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_6, _stringLiteral9C40692ED77768D578327A3E71988DFF501534A4, L_7, NULL);
		// m_Mat.SetFloat("_GhostSeeAmplitude", m_GhostSeeAmplitude);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_8 = __this->___m_Mat_4;
		float L_9 = __this->___m_GhostSeeAmplitude_11;
		NullCheck(L_8);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_8, _stringLiteral98DF8830FA14DA365C6B54CB182D990F6F2F2A0E, L_9, NULL);
		// m_Mat.SetVector("_Dimensions", new Vector4(0.8f, m_EyeClose, 0f, 0f));
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_10 = __this->___m_Mat_4;
		float L_11 = __this->___m_EyeClose_19;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_12), (0.800000012f), L_11, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		Material_SetVector_m69444B8040D955821F241113446CC8713C9E12D1(L_10, _stringLiteral21627BBE8EBCFC824DEF784AC25AB0E7B675D01D, L_12, NULL);
		// m_Mat.SetFloat("_Frequency", m_Frequency);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_13 = __this->___m_Mat_4;
		float L_14 = __this->___m_Frequency_12;
		NullCheck(L_13);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_13, _stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E, L_14, NULL);
		// m_Mat.SetFloat("_Period", m_Period);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = __this->___m_Mat_4;
		float L_16 = __this->___m_Period_13;
		NullCheck(L_15);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_15, _stringLiteralBA0CE45FD5A657EDBA8FB9361FC9EACBFD7C6E87, L_16, NULL);
		// m_Mat.SetFloat("_RandomNumber", 1f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_17 = __this->___m_Mat_4;
		NullCheck(L_17);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_17, _stringLiteral5B983EEC590B83119333B9EC9CE00399B0CFFDD1, (1.0f), NULL);
		// m_Mat.SetFloat("_Amplitude", m_Amplitude);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = __this->___m_Mat_4;
		float L_19 = __this->___m_Amplitude_14;
		NullCheck(L_18);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_18, _stringLiteralEE06B53F9B6FDDA6A65F38986E1BA6B03495562F, L_19, NULL);
		// m_Mat.SetFloat("_BlurMin", m_BlurMin);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_20 = __this->___m_Mat_4;
		float L_21 = __this->___m_BlurMin_15;
		NullCheck(L_20);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_20, _stringLiteralA897552B9939370E6487698DB3A96C96CEE06466, L_21, NULL);
		// m_Mat.SetFloat("_BlurMax", m_BlurMax);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_22 = __this->___m_Mat_4;
		float L_23 = __this->___m_BlurMax_16;
		NullCheck(L_22);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_22, _stringLiteral405CCEE21C770033E6F661F8E266E281B6A6E0FE, L_23, NULL);
		// m_Mat.SetFloat("_BlurSpeed", m_BlurSpeed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_24 = __this->___m_Mat_4;
		float L_25 = __this->___m_BlurSpeed_17;
		NullCheck(L_24);
		Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_24, _stringLiteral019188D0A51A0F9C7D99991CF3646D685A087B71, L_25, NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.BuiltIn.Demo::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_OnRenderImage_mC8C1BEDC5055E92AD38455EE24150D20E46CDBD6 (Demo_tB2CE2F3309296F5900575784169917681D1E7D28* __this, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___src0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___dst1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral005930A9597B0BEA2250DA712041C3035CEB8736);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B3FA2417F9F161F82E54814DD98BF6B7ACE3837);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_0 = NULL;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_1 = NULL;
	{
		// Shader.SetGlobalTexture("_Global_OrigScene", src);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_0 = ___src0;
		Shader_SetGlobalTexture_mABB6E994E67D083BEBE142B4CC8FA77137C2D021(_stringLiteral5B3FA2417F9F161F82E54814DD98BF6B7ACE3837, L_0, NULL);
		// Shader.SetGlobalFloat("_Global_Fade", m_DrunkIntensity);
		float L_1 = __this->___m_DrunkIntensity_6;
		Shader_SetGlobalFloat_mFA8F651003951E6319C952475148713F521243BF(_stringLiteral005930A9597B0BEA2250DA712041C3035CEB8736, L_1, NULL);
		// RenderTexture rt1 = RenderTexture.GetTemporary(Screen.width, Screen.height, 0);
		int32_t L_2;
		L_2 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		int32_t L_3;
		L_3 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_4;
		L_4 = RenderTexture_GetTemporary_m47DF6AA5AB3A4360AF9CB62BE0180AE9505C6C66(L_2, L_3, 0, NULL);
		V_0 = L_4;
		// RenderTexture rt2 = RenderTexture.GetTemporary(Screen.width, Screen.height, 0);
		int32_t L_5;
		L_5 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		int32_t L_6;
		L_6 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_7;
		L_7 = RenderTexture_GetTemporary_m47DF6AA5AB3A4360AF9CB62BE0180AE9505C6C66(L_5, L_6, 0, NULL);
		V_1 = L_7;
		// Graphics.Blit(src, rt1, m_Mat, 0);   // RGB split pass
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_8 = ___src0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_9 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_10 = __this->___m_Mat_4;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_8, L_9, L_10, 0, NULL);
		// if (EType.Rotated == m_Type)
		int32_t L_11 = __this->___m_Type_5;
		if (L_11)
		{
			goto IL_00bf;
		}
	}
	{
		// if (m_SleepyEye)
		bool L_12 = __this->___m_SleepyEye_18;
		if (!L_12)
		{
			goto IL_0095;
		}
	}
	{
		// Graphics.Blit(rt1, rt2, m_Mat, 1);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_13 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_14 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = __this->___m_Mat_4;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_13, L_14, L_15, 1, NULL);
		// Graphics.Blit(rt2, rt1, m_Mat, 4);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_16 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_17 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_16, L_17, L_18, 4, NULL);
		// Graphics.Blit(rt1, rt2, m_Mat, 5);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_19 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_20 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_19, L_20, L_21, 5, NULL);
		// Graphics.Blit(rt2, dst, m_Mat, 3);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_22 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_23 = ___dst1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_24 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_22, L_23, L_24, 3, NULL);
		goto IL_00bf;
	}

IL_0095:
	{
		// Graphics.Blit(rt1, rt2, m_Mat, 1);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_25 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_26 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_27 = __this->___m_Mat_4;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_25, L_26, L_27, 1, NULL);
		// Graphics.Blit(rt2, rt1, m_Mat, 4);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_28 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_29 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_30 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_28, L_29, L_30, 4, NULL);
		// Graphics.Blit(rt1, dst, m_Mat, 5);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_31 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_32 = ___dst1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_33 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_31, L_32, L_33, 5, NULL);
	}

IL_00bf:
	{
		// if (EType.Splitted == m_Type)
		int32_t L_34 = __this->___m_Type_5;
		if ((!(((uint32_t)1) == ((uint32_t)L_34))))
		{
			goto IL_0134;
		}
	}
	{
		// if (m_SleepyEye)
		bool L_35 = __this->___m_SleepyEye_18;
		if (!L_35)
		{
			goto IL_010a;
		}
	}
	{
		// Graphics.Blit(rt1, rt2, m_Mat, 2);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_36 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_37 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_38 = __this->___m_Mat_4;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_36, L_37, L_38, 2, NULL);
		// Graphics.Blit(rt2, rt1, m_Mat, 4);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_39 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_40 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_41 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_39, L_40, L_41, 4, NULL);
		// Graphics.Blit(rt1, rt2, m_Mat, 5);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_42 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_43 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_44 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_42, L_43, L_44, 5, NULL);
		// Graphics.Blit(rt2, dst, m_Mat, 3);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_45 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_46 = ___dst1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_47 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_45, L_46, L_47, 3, NULL);
		goto IL_0134;
	}

IL_010a:
	{
		// Graphics.Blit(rt1, rt2, m_Mat, 2);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_48 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_49 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_50 = __this->___m_Mat_4;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_48, L_49, L_50, 2, NULL);
		// Graphics.Blit(rt2, rt1, m_Mat, 4);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_51 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_52 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_53 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_51, L_52, L_53, 4, NULL);
		// Graphics.Blit(rt1, dst, m_Mat, 5);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_54 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_55 = ___dst1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_56 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_54, L_55, L_56, 5, NULL);
	}

IL_0134:
	{
		// if (EType.Waggle == m_Type)
		int32_t L_57 = __this->___m_Type_5;
		if ((!(((uint32_t)2) == ((uint32_t)L_57))))
		{
			goto IL_018d;
		}
	}
	{
		// if (m_SleepyEye)
		bool L_58 = __this->___m_SleepyEye_18;
		if (!L_58)
		{
			goto IL_0171;
		}
	}
	{
		// Graphics.Blit(rt1, rt2, m_Mat, 4);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_59 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_60 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_61 = __this->___m_Mat_4;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_59, L_60, L_61, 4, NULL);
		// Graphics.Blit(rt2, rt1, m_Mat, 6);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_62 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_63 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_64 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_62, L_63, L_64, 6, NULL);
		// Graphics.Blit(rt1, dst, m_Mat, 3);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_65 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_66 = ___dst1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_67 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_65, L_66, L_67, 3, NULL);
		goto IL_018d;
	}

IL_0171:
	{
		// Graphics.Blit(rt1, rt2, m_Mat, 4);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_68 = V_0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_69 = V_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_70 = __this->___m_Mat_4;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_68, L_69, L_70, 4, NULL);
		// Graphics.Blit(rt2, dst, m_Mat, 6);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_71 = V_1;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_72 = ___dst1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73 = __this->___m_Mat_4;
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_71, L_72, L_73, 6, NULL);
	}

IL_018d:
	{
		// RenderTexture.ReleaseTemporary(rt1);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_74 = V_0;
		RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F(L_74, NULL);
		// RenderTexture.ReleaseTemporary(rt2);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_75 = V_1;
		RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F(L_75, NULL);
		// }
		return;
	}
}
// System.Void DrunkMan.BuiltIn.Demo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo__ctor_mF247B44EB31DF71EA3ADB2E19A06917B1A1512C2 (Demo_tB2CE2F3309296F5900575784169917681D1E7D28* __this, const RuntimeMethod* method) 
{
	{
		// [Range(1f, 16f)] public float m_RGBShiftPower = 3f;
		__this->___m_RGBShiftPower_8 = (3.0f);
		// [Range(0f, 0.06f)] public float m_GhostSeeRadius = 0.01f;
		__this->___m_GhostSeeRadius_9 = (0.00999999978f);
		// [Range(0.01f, 1f)] public float m_GhostSeeMix = 0.5f;
		__this->___m_GhostSeeMix_10 = (0.5f);
		// [Range(0.01f, 0.2f)] public float m_GhostSeeAmplitude = 0.05f;
		__this->___m_GhostSeeAmplitude_11 = (0.0500000007f);
		// [Range(0.5f, 8f)] public float m_Frequency = 1f;
		__this->___m_Frequency_12 = (1.0f);
		// [Range(0.1f, 4f)] public float m_Period = 1.5f;
		__this->___m_Period_13 = (1.5f);
		// [Range(1f, 16f)] public float m_Amplitude = 1f;
		__this->___m_Amplitude_14 = (1.0f);
		// [Range(0f, 1f)] public float m_BlurMin = 0.1f;
		__this->___m_BlurMin_15 = (0.100000001f);
		// [Range(0f, 1f)] public float m_BlurMax = 0.3f;
		__this->___m_BlurMax_16 = (0.300000012f);
		// [Range(1f, 6f)] public float m_BlurSpeed = 3f;
		__this->___m_BlurSpeed_17 = (3.0f);
		// [Range(0f, 0.9f)] public float m_EyeClose = 0.4f;
		__this->___m_EyeClose_19 = (0.400000006f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m3E2E4E94093F49D09DEB34CA97BF8A632B27F1AD_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___current0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	bool V_5 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___target1;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___current0;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___target1;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___current0;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___target1;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___current0;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		V_3 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))));
		float L_18 = V_3;
		if ((((float)L_18) == ((float)(0.0f))))
		{
			goto IL_0055;
		}
	}
	{
		float L_19 = ___maxDistanceDelta2;
		if ((!(((float)L_19) >= ((float)(0.0f)))))
		{
			goto IL_0052;
		}
	}
	{
		float L_20 = V_3;
		float L_21 = ___maxDistanceDelta2;
		float L_22 = ___maxDistanceDelta2;
		G_B4_0 = ((((int32_t)((!(((float)L_20) <= ((float)((float)il2cpp_codegen_multiply(L_21, L_22)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0053;
	}

IL_0052:
	{
		G_B4_0 = 0;
	}

IL_0053:
	{
		G_B6_0 = G_B4_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B6_0 = 1;
	}

IL_0056:
	{
		V_5 = (bool)G_B6_0;
		bool L_23 = V_5;
		if (!L_23)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = ___target1;
		V_6 = L_24;
		goto IL_009b;
	}

IL_0061:
	{
		float L_25 = V_3;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_26;
		L_26 = sqrt(((double)L_25));
		V_4 = ((float)L_26);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27 = ___current0;
		float L_28 = L_27.___x_2;
		float L_29 = V_0;
		float L_30 = V_4;
		float L_31 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = ___current0;
		float L_33 = L_32.___y_3;
		float L_34 = V_1;
		float L_35 = V_4;
		float L_36 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37 = ___current0;
		float L_38 = L_37.___z_4;
		float L_39 = V_2;
		float L_40 = V_4;
		float L_41 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_42), ((float)il2cpp_codegen_add(L_28, ((float)il2cpp_codegen_multiply(((float)(L_29/L_30)), L_31)))), ((float)il2cpp_codegen_add(L_33, ((float)il2cpp_codegen_multiply(((float)(L_34/L_35)), L_36)))), ((float)il2cpp_codegen_add(L_38, ((float)il2cpp_codegen_multiply(((float)(L_39/L_40)), L_41)))), /*hidden argument*/NULL);
		V_6 = L_42;
		goto IL_009b;
	}

IL_009b:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_43 = V_6;
		return L_43;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___b1;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___b1;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___a0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___b1;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_18;
		L_18 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))))));
		V_3 = ((float)L_18);
		goto IL_0040;
	}

IL_0040:
	{
		float L_19 = V_3;
		return L_19;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_UnaryNegation_m3AC523A7BED6E843165BDF598690F0560D8CAA63_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___a0;
		float L_3 = L_2.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_6), ((-L_1)), ((-L_3)), ((-L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___b1;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___b1;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___a0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___b1;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), ((float)il2cpp_codegen_add(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float StatsManager_GetABV_mEA7946CB824D7AE211EF9B8AD560C64AEFEFF8CD_inline (StatsManager_tC4FC040D87839B5C03060ACC2B3A01B02F165F87* __this, const RuntimeMethod* method) 
{
	{
		// return abv;
		float L_0 = __this->___abv_7;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_1 = L_0;
		float L_1 = ___y1;
		__this->___y_2 = L_1;
		float L_2 = ___z2;
		__this->___z_3 = L_2;
		float L_3 = ___w3;
		__this->___w_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ScriptableRenderPass_set_renderPassEvent_m63FA581FFDE1C69C2E1358BD0B8DB30275334960_inline (ScriptableRenderPass_tEA38F6C7AD8D111A2251E4C2A7530BCEE7D6D2B0* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// public RenderPassEvent renderPassEvent { get; set; }
		int32_t L_0 = ___value0;
		__this->___U3CrenderPassEventU3Ek__BackingField_0 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t RenderTargetHandle_get_id_m4D50FDA4A486E05D07A54ABFC04BD96C1CE7D7BE_inline (RenderTargetHandle_tB5C2670041BF377223D41FDF9290F6D8BFB7BA66* __this, const RuntimeMethod* method) 
{
	{
		// public int id { set; get; }
		int32_t L_0 = __this->___U3CidU3Ek__BackingField_0;
		return L_0;
	}
}
