﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CameraSettingFix::Start()
extern void CameraSettingFix_Start_mC20F9C0076E98D3409D20453516D3131EF51A1C0 (void);
// 0x00000002 System.Void CameraSettingFix::Update()
extern void CameraSettingFix_Update_m7A3616E042BD697FF33B78299D046930E795A728 (void);
// 0x00000003 System.Void CameraSettingFix::.ctor()
extern void CameraSettingFix__ctor_m3B04AFCB1646D580558F608DE0E3B1150E3A3C3F (void);
// 0x00000004 System.Void DamageSystem::Start()
extern void DamageSystem_Start_mE92E95B82B6A1A3766CC08FA5A02BB526CA1D933 (void);
// 0x00000005 System.Void DamageSystem::Update()
extern void DamageSystem_Update_mC86FE409E436877828A465DBFB4B0EE2458C7059 (void);
// 0x00000006 System.Void DamageSystem::Died()
extern void DamageSystem_Died_mBC78BBBD505774B8B3F2BC3380648E7C89DAA51F (void);
// 0x00000007 System.Void DamageSystem::ZombieHandDestroyed(System.String)
extern void DamageSystem_ZombieHandDestroyed_mECB9F201A90AD49C3D37E38F69260826CD75EA62 (void);
// 0x00000008 System.Void DamageSystem::ZombieArmDestroyed(System.String)
extern void DamageSystem_ZombieArmDestroyed_m30CE36CAA7AA9AC28FC3523C20A965E6623B2F43 (void);
// 0x00000009 System.Void DamageSystem::.ctor()
extern void DamageSystem__ctor_mC25F7D4C1BF8158098AE9611C4C0D15FB0A7466A (void);
// 0x0000000A System.Void Drink::Start()
extern void Drink_Start_m780C841889C98DAF0618FCDEF4030A4F7E129063 (void);
// 0x0000000B System.Void Drink::Update()
extern void Drink_Update_mEB3A001CBAAB97ACA0609E1B1A67BD93C46A09DE (void);
// 0x0000000C System.Void Drink::GetDrunk()
extern void Drink_GetDrunk_m52D613B6181F0FAA01857356C449D66CBD11726B (void);
// 0x0000000D System.Void Drink::.ctor()
extern void Drink__ctor_m3A80DF38BA38D5706650D564B7D60496DB9A0720 (void);
// 0x0000000E System.Void MerchandiseManager::Start()
extern void MerchandiseManager_Start_m4C748531388613B6A252D72D5192CFDB53D9E0C3 (void);
// 0x0000000F System.Void MerchandiseManager::Update()
extern void MerchandiseManager_Update_m70E6438D6CCD10C7BE3D59D062A3F0F1568FA9AA (void);
// 0x00000010 System.Void MerchandiseManager::OrderLightBeer(System.Int32)
extern void MerchandiseManager_OrderLightBeer_m85B2F56168FB43C2A6A993D91156BC58699E78FD (void);
// 0x00000011 System.Void MerchandiseManager::OrderDarkBeer(System.Int32)
extern void MerchandiseManager_OrderDarkBeer_mBE154564BCD1BD0E92AD6673C809E8BF4E5B6627 (void);
// 0x00000012 System.Void MerchandiseManager::OrderRedWine(System.Int32)
extern void MerchandiseManager_OrderRedWine_m5A74D9D37E580C8391468273A35409D79FD42D5B (void);
// 0x00000013 System.Void MerchandiseManager::OrderDessertWine(System.Int32)
extern void MerchandiseManager_OrderDessertWine_m42D85F5E942C64EC36BF6CD885987386D7757967 (void);
// 0x00000014 System.Void MerchandiseManager::OrderPistol(System.Int32)
extern void MerchandiseManager_OrderPistol_m5CD0EC0A2A3921B8A0AD85157B7EDBE47166B4B5 (void);
// 0x00000015 System.Void MerchandiseManager::OrderRifle(System.Int32)
extern void MerchandiseManager_OrderRifle_mA989587C20E5AA4C6642A51B53B4527AB0E7177A (void);
// 0x00000016 System.Void MerchandiseManager::OrderShotgun(System.Int32)
extern void MerchandiseManager_OrderShotgun_mA40E54F39B678811694B9358AC4AEFA82F8A4980 (void);
// 0x00000017 System.Void MerchandiseManager::OrderAutoRifle(System.Int32)
extern void MerchandiseManager_OrderAutoRifle_mDB2E3C0204E574D1450E4BB15FA0026FD7A74A4E (void);
// 0x00000018 System.Void MerchandiseManager::OrderPistolAmmo(System.Int32)
extern void MerchandiseManager_OrderPistolAmmo_mD11816C72E59CED181A077C8E408DCFE515D5479 (void);
// 0x00000019 System.Void MerchandiseManager::OrderRifleAmmo(System.Int32)
extern void MerchandiseManager_OrderRifleAmmo_mD6650E5088D300909CC3F80168F45B3761BB68AF (void);
// 0x0000001A System.Void MerchandiseManager::OrderShotgunAmmo(System.Int32)
extern void MerchandiseManager_OrderShotgunAmmo_mF0C129C97C4FC5A198BBA03FDFAF83112969211E (void);
// 0x0000001B System.Void MerchandiseManager::OrderAutoRifleAmmo(System.Int32)
extern void MerchandiseManager_OrderAutoRifleAmmo_m9AC893260AD5B40196770A6D57500322DD1C6FE6 (void);
// 0x0000001C System.Void MerchandiseManager::.ctor()
extern void MerchandiseManager__ctor_m7C20B7A581065B72808FA32B36CA041D17370836 (void);
// 0x0000001D System.Void PlayerHealth::Awake()
extern void PlayerHealth_Awake_m694ED0820668275A13034B552675DA946A50B9B6 (void);
// 0x0000001E System.Void PlayerHealth::Update()
extern void PlayerHealth_Update_m5645A1624A67A8F9332B3DB67A4C2BAABF3DCD5A (void);
// 0x0000001F System.Void PlayerHealth::Damage(System.Single)
extern void PlayerHealth_Damage_m8C47E7E1C7E05412767C89110A0F4594A0CE9588 (void);
// 0x00000020 System.Void PlayerHealth::Heal(System.Int32)
extern void PlayerHealth_Heal_mD3007EA4440F16CA78A148830089AF07CFEAE9F0 (void);
// 0x00000021 System.Void PlayerHealth::Die()
extern void PlayerHealth_Die_mB6EE0EAE72C9522BD349F87EB61AEDDFE7F0C44D (void);
// 0x00000022 System.Void PlayerHealth::.ctor()
extern void PlayerHealth__ctor_m6A07958FCBF285AA65AB66D48C3EB198068F37BE (void);
// 0x00000023 System.Void rateRefresh::Start()
extern void rateRefresh_Start_m1A991442FF97EC78A3BB4FD84370FDE03E777C98 (void);
// 0x00000024 System.Void rateRefresh::Update()
extern void rateRefresh_Update_mB78885C08A68A43E1A85065B0AD80FE5428706BD (void);
// 0x00000025 System.Collections.IEnumerator rateRefresh::RefreshFrameRate(System.Single)
extern void rateRefresh_RefreshFrameRate_m55B4CD49635057014734696F641695424E90D21F (void);
// 0x00000026 System.Void rateRefresh::.ctor()
extern void rateRefresh__ctor_m044D158A12ABFAC6FE9E75661DEBCF13DC2722AF (void);
// 0x00000027 System.Void rateRefresh/<RefreshFrameRate>d__3::.ctor(System.Int32)
extern void U3CRefreshFrameRateU3Ed__3__ctor_m6BFEA59A53082AF0E44416F315C921971B1C2F37 (void);
// 0x00000028 System.Void rateRefresh/<RefreshFrameRate>d__3::System.IDisposable.Dispose()
extern void U3CRefreshFrameRateU3Ed__3_System_IDisposable_Dispose_mC89D056F16A46DB9F4F0E96F8438D6C4D1C1F57C (void);
// 0x00000029 System.Boolean rateRefresh/<RefreshFrameRate>d__3::MoveNext()
extern void U3CRefreshFrameRateU3Ed__3_MoveNext_m0715111D25C0A2CCA372E43510D443AAEFB48C1B (void);
// 0x0000002A System.Object rateRefresh/<RefreshFrameRate>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRefreshFrameRateU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95122FB755946EAE5EFE02635F57A341276449B (void);
// 0x0000002B System.Void rateRefresh/<RefreshFrameRate>d__3::System.Collections.IEnumerator.Reset()
extern void U3CRefreshFrameRateU3Ed__3_System_Collections_IEnumerator_Reset_m34983B0C531DD9C3AD5A6FFD849A38803097E0F4 (void);
// 0x0000002C System.Object rateRefresh/<RefreshFrameRate>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CRefreshFrameRateU3Ed__3_System_Collections_IEnumerator_get_Current_m27F7153420E1351D2BC5553E2A2AA47ABEF70E69 (void);
// 0x0000002D System.Void RaycastPaddingHelper::.ctor()
extern void RaycastPaddingHelper__ctor_m7F99736727823DBAD45CAE3752F82149660451C9 (void);
// 0x0000002E System.Void SoberUp::Start()
extern void SoberUp_Start_m6E24D684A5B86538FA8F2B914E85FADFC2F88D0A (void);
// 0x0000002F System.Void SoberUp::Update()
extern void SoberUp_Update_m580595E2EFCC84D569AE939FABF62B304CCF0B71 (void);
// 0x00000030 System.Void SoberUp::GetSober()
extern void SoberUp_GetSober_mFDEB99943378C6D6FC6AF5380F47E769C275F126 (void);
// 0x00000031 System.Void SoberUp::.ctor()
extern void SoberUp__ctor_m39CB212A0098C13896D7934946CD2680726026B9 (void);
// 0x00000032 System.Void SpawnManager::Start()
extern void SpawnManager_Start_m65C9EA67649948222CFF4FBBF77BE2319D813DAF (void);
// 0x00000033 System.Void SpawnManager::Update()
extern void SpawnManager_Update_mD714BA3EADCC182FB7A93B9DE347D727E3A1BC53 (void);
// 0x00000034 System.Void SpawnManager::SpawnZombie(UnityEngine.GameObject,UnityEngine.Transform)
extern void SpawnManager_SpawnZombie_m2446C92EFF16E7B194941482D313077F2F6AD1DE (void);
// 0x00000035 System.Void SpawnManager::ToggleGame()
extern void SpawnManager_ToggleGame_mDE3F11F210E0702BDAB79EEDCE0E6C5723B41DFB (void);
// 0x00000036 System.Void SpawnManager::ZombieDie()
extern void SpawnManager_ZombieDie_m28DD8A967105A431DE692EF4FCF45158BCC8FA79 (void);
// 0x00000037 System.Void SpawnManager::.ctor()
extern void SpawnManager__ctor_m8DD503A0FFE79FA38CF0B7F013E54D24A04D166A (void);
// 0x00000038 System.Void StatsManager::Start()
extern void StatsManager_Start_m3DD113A1A278962CE9E455BDA7F1A1A470EAF61F (void);
// 0x00000039 System.Void StatsManager::Update()
extern void StatsManager_Update_m9A68EDC589B37447C635D102DCF09405A4AEC14A (void);
// 0x0000003A System.Void StatsManager::EarnMoney(System.Int32)
extern void StatsManager_EarnMoney_m6671D0FCBBEC1C4573C903B3A962C01FC07A4AB0 (void);
// 0x0000003B System.Void StatsManager::SpendMoney(System.Int32)
extern void StatsManager_SpendMoney_m0F43759B158995CDBB23CF33911A5E6AED4F7740 (void);
// 0x0000003C System.Void StatsManager::KillZombie(System.Int32)
extern void StatsManager_KillZombie_m0F37952653461680946190BD73625E9B4C32A383 (void);
// 0x0000003D System.Void StatsManager::ResetStats()
extern void StatsManager_ResetStats_mF86A1AA707677E557D302606DBBF1FB4138D8BBB (void);
// 0x0000003E System.Void StatsManager::SetABV(System.Single)
extern void StatsManager_SetABV_m839CA12BA14F066BDEB7EEB3ED381BC3186A64DF (void);
// 0x0000003F System.Single StatsManager::GetABV()
extern void StatsManager_GetABV_mEA7946CB824D7AE211EF9B8AD560C64AEFEFF8CD (void);
// 0x00000040 System.Void StatsManager::.ctor()
extern void StatsManager__ctor_mD0251ABB61521F5DE033975F0CB59C7E9F240564 (void);
// 0x00000041 System.Void TextFPSCounter::Start()
extern void TextFPSCounter_Start_mD48FD89899FFF6E0114499E47416C01BDCA1F9FB (void);
// 0x00000042 System.Void TextFPSCounter::Update()
extern void TextFPSCounter_Update_mC01ED6AACF4266657E65ED6DE5D8F8A8FD776C13 (void);
// 0x00000043 System.Void TextFPSCounter::.ctor()
extern void TextFPSCounter__ctor_m7B261FE8D41B9C885E459962AF2884283453C388 (void);
// 0x00000044 System.Void idle_mocap_follow_player::Update()
extern void idle_mocap_follow_player_Update_m555155208246194CF4F181C1239BF681A6F1DDF8 (void);
// 0x00000045 System.Void idle_mocap_follow_player::.ctor()
extern void idle_mocap_follow_player__ctor_m33728B1BC494E53D794FF61D1DCE5CEF2BDBCC85 (void);
// 0x00000046 System.Void FixNonUniformScale::OnDrawGizmosSelected()
extern void FixNonUniformScale_OnDrawGizmosSelected_mD29B982DC4CF3E56B4C1425D6F9962CE124AEE1C (void);
// 0x00000047 System.Void FixNonUniformScale::MakeUniform()
extern void FixNonUniformScale_MakeUniform_mBDE38273898560DAA90A8DC2CFB58048AA54D9F5 (void);
// 0x00000048 System.Void FixNonUniformScale::.ctor()
extern void FixNonUniformScale__ctor_mA1ECA5647190B013D52C2E4AFDDCE9173D8DFA73 (void);
// 0x00000049 System.Void DamageAIByExplosion::Start()
extern void DamageAIByExplosion_Start_m676B4F9FD98A7F526EF16C90307EA1F5A47C06A4 (void);
// 0x0000004A System.Void DamageAIByExplosion::Explode()
extern void DamageAIByExplosion_Explode_m7A08FF951561E246578CB21AE324A11394C0F0CA (void);
// 0x0000004B System.Void DamageAIByExplosion::SpawnExplosionSound()
extern void DamageAIByExplosion_SpawnExplosionSound_m52521C5D9772E7D67272C7B6EFC5140BEB74DB3A (void);
// 0x0000004C System.Void DamageAIByExplosion::.ctor()
extern void DamageAIByExplosion__ctor_m61277900686F59D0383C1D3098AC398EB1BC990C (void);
// 0x0000004D System.Void DemoVisionFX::OnEnable()
extern void DemoVisionFX_OnEnable_m202BA5155EA8D952022F4EE3BFA3A86FC20A1C37 (void);
// 0x0000004E System.Void DemoVisionFX::Update()
extern void DemoVisionFX_Update_m41A12E55E3FB611473EAA2573A719F13232D6A37 (void);
// 0x0000004F System.Void DemoVisionFX::OnGUI()
extern void DemoVisionFX_OnGUI_m43B673A5C222FD566780393E51C25D8E4A537BE4 (void);
// 0x00000050 System.Void DemoVisionFX::DrawCustomAttributes(Nephasto.VisionFXAsset.BaseVision)
extern void DemoVisionFX_DrawCustomAttributes_m369189C4A8DE28A5126CF7E5FD6AF85A6A60F886 (void);
// 0x00000051 System.Int32 DemoVisionFX::HorizontalSlider(System.String,System.Int32,System.Int32,System.Int32)
extern void DemoVisionFX_HorizontalSlider_mC6E2866BC60316129D6649A73AF0A8F0E257F46A (void);
// 0x00000052 System.Single DemoVisionFX::HorizontalSlider(System.String,System.Single,System.Single,System.Single)
extern void DemoVisionFX_HorizontalSlider_m37475E52CB6808E6FA95208419DDD892F1FB0F20 (void);
// 0x00000053 UnityEngine.Vector2 DemoVisionFX::HorizontalSlider(System.String,UnityEngine.Vector2,System.Single,System.Single)
extern void DemoVisionFX_HorizontalSlider_m9E658A021E97CEC6ED78E55AE0FC07D731EFEBE3 (void);
// 0x00000054 UnityEngine.Color DemoVisionFX::ColorSlider(System.String,UnityEngine.Color)
extern void DemoVisionFX_ColorSlider_m093E50C267EDF651FF3012E7E4ABD1095DADD1AD (void);
// 0x00000055 UnityEngine.Texture2D DemoVisionFX::MakeTex(System.Int32,System.Int32,UnityEngine.Color)
extern void DemoVisionFX_MakeTex_m4D73A977C38516F324B9301E9C52DA570C93F9E7 (void);
// 0x00000056 System.Void DemoVisionFX::ChangeEffect(System.Int32)
extern void DemoVisionFX_ChangeEffect_m26841A72045AC047013985E0480CADBB9D5732C0 (void);
// 0x00000057 System.Collections.IEnumerator DemoVisionFX::StarDelayCoroutine()
extern void DemoVisionFX_StarDelayCoroutine_m4E023D0E0E97D910CEB5736DBF149A006188A755 (void);
// 0x00000058 System.Collections.IEnumerator DemoVisionFX::ChangeEffectCoroutine(System.Int32)
extern void DemoVisionFX_ChangeEffectCoroutine_m545013A4CDF7D4F9A931C5DD35CE64506B166EE6 (void);
// 0x00000059 System.String DemoVisionFX::EffectName(System.String)
extern void DemoVisionFX_EffectName_mE9387E6BCE76E44B7714611962DF63171C445D80 (void);
// 0x0000005A System.Void DemoVisionFX::.ctor()
extern void DemoVisionFX__ctor_mBB30AA019B7BC7B1E86EEDD03A516945CFF4FEBC (void);
// 0x0000005B System.Void DemoVisionFX/<StarDelayCoroutine>d__37::.ctor(System.Int32)
extern void U3CStarDelayCoroutineU3Ed__37__ctor_mD9A49D668D1674E5C1F1C6B5846E13AE869E9DD3 (void);
// 0x0000005C System.Void DemoVisionFX/<StarDelayCoroutine>d__37::System.IDisposable.Dispose()
extern void U3CStarDelayCoroutineU3Ed__37_System_IDisposable_Dispose_m9577C27866FF94A549E234E7284A04035327F2DA (void);
// 0x0000005D System.Boolean DemoVisionFX/<StarDelayCoroutine>d__37::MoveNext()
extern void U3CStarDelayCoroutineU3Ed__37_MoveNext_m10DC61933276D8BAD540C7C3D41F5F18ECDC1448 (void);
// 0x0000005E System.Object DemoVisionFX/<StarDelayCoroutine>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStarDelayCoroutineU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61C98399065F3BBDC9ABF1A8C89A479A1D9C00B1 (void);
// 0x0000005F System.Void DemoVisionFX/<StarDelayCoroutine>d__37::System.Collections.IEnumerator.Reset()
extern void U3CStarDelayCoroutineU3Ed__37_System_Collections_IEnumerator_Reset_m81491E24DB7541256303DBC631809771555E0BA4 (void);
// 0x00000060 System.Object DemoVisionFX/<StarDelayCoroutine>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CStarDelayCoroutineU3Ed__37_System_Collections_IEnumerator_get_Current_mF942584EA9B87D4D294D6D464928F95DF2701E90 (void);
// 0x00000061 System.Void DemoVisionFX/<ChangeEffectCoroutine>d__38::.ctor(System.Int32)
extern void U3CChangeEffectCoroutineU3Ed__38__ctor_m5D72F352E28860931FD618DC81CE2F261715410F (void);
// 0x00000062 System.Void DemoVisionFX/<ChangeEffectCoroutine>d__38::System.IDisposable.Dispose()
extern void U3CChangeEffectCoroutineU3Ed__38_System_IDisposable_Dispose_mFFEBCC8B2F91FCEF661DD53A736C2909853506A7 (void);
// 0x00000063 System.Boolean DemoVisionFX/<ChangeEffectCoroutine>d__38::MoveNext()
extern void U3CChangeEffectCoroutineU3Ed__38_MoveNext_m1445536ED3AD8588908C4501C04BDB89673ED77D (void);
// 0x00000064 System.Object DemoVisionFX/<ChangeEffectCoroutine>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeEffectCoroutineU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD33B0EED7303C11A669EB01DA79AA6BCBC5A714 (void);
// 0x00000065 System.Void DemoVisionFX/<ChangeEffectCoroutine>d__38::System.Collections.IEnumerator.Reset()
extern void U3CChangeEffectCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_m70852CB17099F795E077C6892EAE57A8A3527A0E (void);
// 0x00000066 System.Object DemoVisionFX/<ChangeEffectCoroutine>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CChangeEffectCoroutineU3Ed__38_System_Collections_IEnumerator_get_Current_mF3B082D0285BC657CB8B09D8C9011B912F5C171D (void);
// 0x00000067 System.Void SimpleFirstPersonController::add_OnStartMoving(SimpleFirstPersonController/StartMoving)
extern void SimpleFirstPersonController_add_OnStartMoving_mF4B4FEEE5A2E41FA1BE060AD85A323A2520A7844 (void);
// 0x00000068 System.Void SimpleFirstPersonController::remove_OnStartMoving(SimpleFirstPersonController/StartMoving)
extern void SimpleFirstPersonController_remove_OnStartMoving_m3F255E89C90E7CAC67FDCF1635B531D801DC9D89 (void);
// 0x00000069 System.Collections.IEnumerator SimpleFirstPersonController::Footsteps()
extern void SimpleFirstPersonController_Footsteps_m50353376165FDB03457113D517B295BA5F90F63C (void);
// 0x0000006A System.Void SimpleFirstPersonController::Awake()
extern void SimpleFirstPersonController_Awake_mD626E29CE91B41C73E18611A611F25F3605D439E (void);
// 0x0000006B System.Void SimpleFirstPersonController::Update()
extern void SimpleFirstPersonController_Update_m1D6DA8CA57099523BB78E89883264CABE5F334B4 (void);
// 0x0000006C System.Void SimpleFirstPersonController::.ctor()
extern void SimpleFirstPersonController__ctor_m7B936AF21C67A108A3F4CFE63CF58BABEC28715B (void);
// 0x0000006D System.Void SimpleFirstPersonController/StartMoving::.ctor(System.Object,System.IntPtr)
extern void StartMoving__ctor_mB127F7E935A5A96CBCDAFB28B90FFC70F79D6D27 (void);
// 0x0000006E System.Void SimpleFirstPersonController/StartMoving::Invoke()
extern void StartMoving_Invoke_m5A084C820070E39B2C8B194C6EFB647F1A2EE52C (void);
// 0x0000006F System.IAsyncResult SimpleFirstPersonController/StartMoving::BeginInvoke(System.AsyncCallback,System.Object)
extern void StartMoving_BeginInvoke_m725FDB6897AD3C81F0C8593614D8241D0DE36EC0 (void);
// 0x00000070 System.Void SimpleFirstPersonController/StartMoving::EndInvoke(System.IAsyncResult)
extern void StartMoving_EndInvoke_m1806B2CB83CB0C81EE69288F0AB5F2091C81B113 (void);
// 0x00000071 System.Void SimpleFirstPersonController/<Footsteps>d__22::.ctor(System.Int32)
extern void U3CFootstepsU3Ed__22__ctor_m84B7716BA424E4483D50E631E72DA19E4324ABB2 (void);
// 0x00000072 System.Void SimpleFirstPersonController/<Footsteps>d__22::System.IDisposable.Dispose()
extern void U3CFootstepsU3Ed__22_System_IDisposable_Dispose_m8AC6C573ACCA30B5C24CCC3082CCB6730C6A2483 (void);
// 0x00000073 System.Boolean SimpleFirstPersonController/<Footsteps>d__22::MoveNext()
extern void U3CFootstepsU3Ed__22_MoveNext_m77C0C036A9A9853F2558A87C240C6761944BECE8 (void);
// 0x00000074 System.Object SimpleFirstPersonController/<Footsteps>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFootstepsU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36ABB485129AACDD1CCE6EC155030D592CECFB70 (void);
// 0x00000075 System.Void SimpleFirstPersonController/<Footsteps>d__22::System.Collections.IEnumerator.Reset()
extern void U3CFootstepsU3Ed__22_System_Collections_IEnumerator_Reset_m0A9DEECD934C4653E84EFD62956D0FF8970DC537 (void);
// 0x00000076 System.Object SimpleFirstPersonController/<Footsteps>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CFootstepsU3Ed__22_System_Collections_IEnumerator_get_Current_m4122A5D4AF8575305CD0462FB5587F90DA51B285 (void);
// 0x00000077 System.Void EmeraldAI.CombatTextSystem::Awake()
extern void CombatTextSystem_Awake_mA056C2F3F39A60DDA50DE1E9DC2D99C2001E2D8A (void);
// 0x00000078 System.Void EmeraldAI.CombatTextSystem::Initialize()
extern void CombatTextSystem_Initialize_m91656A5411AD8241C831E0DBEBC3A6B58F0CAFA0 (void);
// 0x00000079 System.Void EmeraldAI.CombatTextSystem::CreateCombatText(System.Int32,UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void CombatTextSystem_CreateCombatText_m3AB738EC5241DE33F5874AEBB33C020C777C7AE0 (void);
// 0x0000007A System.Void EmeraldAI.CombatTextSystem::CreateCombatTextAI(System.Int32,UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void CombatTextSystem_CreateCombatTextAI_m12DC6C97BA19E9FEDD47B72FB382F9A615D11A6D (void);
// 0x0000007B System.Collections.IEnumerator EmeraldAI.CombatTextSystem::AnimateBounceText(UnityEngine.UI.Text,UnityEngine.Color,UnityEngine.Color,UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void CombatTextSystem_AnimateBounceText_m12DC07A40F918EA9B16B26818FD364B0851C93B8 (void);
// 0x0000007C System.Collections.IEnumerator EmeraldAI.CombatTextSystem::AnimateUpwardsText(UnityEngine.UI.Text,UnityEngine.Color,UnityEngine.Color,UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void CombatTextSystem_AnimateUpwardsText_m1067CE954306F070CF038A7ECA034C56E2DE6443 (void);
// 0x0000007D System.Collections.IEnumerator EmeraldAI.CombatTextSystem::AnimateOutwardsText(UnityEngine.UI.Text,UnityEngine.Color,UnityEngine.Color,UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void CombatTextSystem_AnimateOutwardsText_mA6F589AAEC150DD6F5929523936531D9F0ED9E4F (void);
// 0x0000007E System.Collections.IEnumerator EmeraldAI.CombatTextSystem::AnimateStationaryText(UnityEngine.UI.Text,UnityEngine.Color,UnityEngine.Color,UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void CombatTextSystem_AnimateStationaryText_mB793B304EEFA64E626EAA595F2E0F7FD254EA65E (void);
// 0x0000007F System.Void EmeraldAI.CombatTextSystem::.ctor()
extern void CombatTextSystem__ctor_mF2DCFE4E48AB43760EC8F72A18CCD5FBD64C757E (void);
// 0x00000080 System.Void EmeraldAI.CombatTextSystem/<AnimateBounceText>d__11::.ctor(System.Int32)
extern void U3CAnimateBounceTextU3Ed__11__ctor_m774BA79859F86B4D3CD0C146F9ED281D94A7844F (void);
// 0x00000081 System.Void EmeraldAI.CombatTextSystem/<AnimateBounceText>d__11::System.IDisposable.Dispose()
extern void U3CAnimateBounceTextU3Ed__11_System_IDisposable_Dispose_m2A0B8B6F18229F8CEFCF4AC9CDE586A463F5C1B4 (void);
// 0x00000082 System.Boolean EmeraldAI.CombatTextSystem/<AnimateBounceText>d__11::MoveNext()
extern void U3CAnimateBounceTextU3Ed__11_MoveNext_m9727187E1FC4209661C49502783D922DA95A08F7 (void);
// 0x00000083 System.Object EmeraldAI.CombatTextSystem/<AnimateBounceText>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateBounceTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m555E6986491A7E0023957F576B4C34D81731098F (void);
// 0x00000084 System.Void EmeraldAI.CombatTextSystem/<AnimateBounceText>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateBounceTextU3Ed__11_System_Collections_IEnumerator_Reset_mFFFE6A69D12A0C2C841577BF716DDD8344897F12 (void);
// 0x00000085 System.Object EmeraldAI.CombatTextSystem/<AnimateBounceText>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateBounceTextU3Ed__11_System_Collections_IEnumerator_get_Current_m666485FFA10569ED13D8F285EAAE0814DE144598 (void);
// 0x00000086 System.Void EmeraldAI.CombatTextSystem/<AnimateUpwardsText>d__12::.ctor(System.Int32)
extern void U3CAnimateUpwardsTextU3Ed__12__ctor_m2F07B6DCA11150CDEBB897B9B6D2B23ADDB5FD0F (void);
// 0x00000087 System.Void EmeraldAI.CombatTextSystem/<AnimateUpwardsText>d__12::System.IDisposable.Dispose()
extern void U3CAnimateUpwardsTextU3Ed__12_System_IDisposable_Dispose_m15DC41DAA5C0E976E41320206E41B36BB23371FA (void);
// 0x00000088 System.Boolean EmeraldAI.CombatTextSystem/<AnimateUpwardsText>d__12::MoveNext()
extern void U3CAnimateUpwardsTextU3Ed__12_MoveNext_m96E92C7148EEAB92A4DA4C6EAD76D98653E1A3D0 (void);
// 0x00000089 System.Object EmeraldAI.CombatTextSystem/<AnimateUpwardsText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateUpwardsTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE51A357754807E733BD1162616584EF94560D261 (void);
// 0x0000008A System.Void EmeraldAI.CombatTextSystem/<AnimateUpwardsText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CAnimateUpwardsTextU3Ed__12_System_Collections_IEnumerator_Reset_m3FEAE8B5746BA901CC49221B5C3D1B55700B5EC7 (void);
// 0x0000008B System.Object EmeraldAI.CombatTextSystem/<AnimateUpwardsText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateUpwardsTextU3Ed__12_System_Collections_IEnumerator_get_Current_mB5510E9288777C46505B7D08C120DDCD51410760 (void);
// 0x0000008C System.Void EmeraldAI.CombatTextSystem/<AnimateOutwardsText>d__13::.ctor(System.Int32)
extern void U3CAnimateOutwardsTextU3Ed__13__ctor_m32D9E13011D13A517956E5343B148ECB8C3A8BC1 (void);
// 0x0000008D System.Void EmeraldAI.CombatTextSystem/<AnimateOutwardsText>d__13::System.IDisposable.Dispose()
extern void U3CAnimateOutwardsTextU3Ed__13_System_IDisposable_Dispose_mAA1D18F6927C7418C43A235966C8A00DD6D8CBF3 (void);
// 0x0000008E System.Boolean EmeraldAI.CombatTextSystem/<AnimateOutwardsText>d__13::MoveNext()
extern void U3CAnimateOutwardsTextU3Ed__13_MoveNext_m9EA59D4137D404747518188C33A69489FBC5C159 (void);
// 0x0000008F System.Object EmeraldAI.CombatTextSystem/<AnimateOutwardsText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateOutwardsTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EE8D3F1E11CA19E1550DE025F5D5EE8CA2E2898 (void);
// 0x00000090 System.Void EmeraldAI.CombatTextSystem/<AnimateOutwardsText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CAnimateOutwardsTextU3Ed__13_System_Collections_IEnumerator_Reset_mCFE69A34A072AC5F10DD943604D0CBC43EFE17E1 (void);
// 0x00000091 System.Object EmeraldAI.CombatTextSystem/<AnimateOutwardsText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateOutwardsTextU3Ed__13_System_Collections_IEnumerator_get_Current_mBD6EB1815B0DCF661AAC698F8C4D95EBFF543190 (void);
// 0x00000092 System.Void EmeraldAI.CombatTextSystem/<AnimateStationaryText>d__14::.ctor(System.Int32)
extern void U3CAnimateStationaryTextU3Ed__14__ctor_m38616AC4D420F6DC6EFFFACECF472ADE084D2B9A (void);
// 0x00000093 System.Void EmeraldAI.CombatTextSystem/<AnimateStationaryText>d__14::System.IDisposable.Dispose()
extern void U3CAnimateStationaryTextU3Ed__14_System_IDisposable_Dispose_m307AD6022E61DFDE1AC2A263B68DE62416965E8E (void);
// 0x00000094 System.Boolean EmeraldAI.CombatTextSystem/<AnimateStationaryText>d__14::MoveNext()
extern void U3CAnimateStationaryTextU3Ed__14_MoveNext_m0D4A2382CAD00B16F14D5B30F2CB2773B89EFA65 (void);
// 0x00000095 System.Object EmeraldAI.CombatTextSystem/<AnimateStationaryText>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateStationaryTextU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B92791CE1F3958F999A44EB988F38D7C7AE92BE (void);
// 0x00000096 System.Void EmeraldAI.CombatTextSystem/<AnimateStationaryText>d__14::System.Collections.IEnumerator.Reset()
extern void U3CAnimateStationaryTextU3Ed__14_System_Collections_IEnumerator_Reset_m61C21D94108BCB00F2E9FDA1A552D0921B72B2D5 (void);
// 0x00000097 System.Object EmeraldAI.CombatTextSystem/<AnimateStationaryText>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateStationaryTextU3Ed__14_System_Collections_IEnumerator_get_Current_mB41CDE7E7F0FB0CFEDB2811B0E2C43FBC6AF04F0 (void);
// 0x00000098 System.Void EmeraldAI.EmeraldAIAbility::.ctor()
extern void EmeraldAIAbility__ctor_mF33E0D1438F4C87481AF79C77D5D1216E8D4F004 (void);
// 0x00000099 System.Void EmeraldAI.EmeraldAIBehaviors::Start()
extern void EmeraldAIBehaviors_Start_m8B32DF5A3D43B089868CC7A973CD4C261A6651F9 (void);
// 0x0000009A System.Void EmeraldAI.EmeraldAIBehaviors::AggressiveBehavior()
extern void EmeraldAIBehaviors_AggressiveBehavior_m5DDCFD61A955F4CC835BD0E2940643D03B7A2B5B (void);
// 0x0000009B System.Void EmeraldAI.EmeraldAIBehaviors::CheckForTargetDeath()
extern void EmeraldAIBehaviors_CheckForTargetDeath_m664819F0C78F6A5C9A45F27C956073F79ED615C1 (void);
// 0x0000009C System.Void EmeraldAI.EmeraldAIBehaviors::DeathReset()
extern void EmeraldAIBehaviors_DeathReset_m38785CD878E5AEAAFFAF1FE8395702D3F56768B9 (void);
// 0x0000009D System.Void EmeraldAI.EmeraldAIBehaviors::CompanionBehavior()
extern void EmeraldAIBehaviors_CompanionBehavior_mDB41869E69CFD6765CB9F353D02112D07A58C6C4 (void);
// 0x0000009E System.Void EmeraldAI.EmeraldAIBehaviors::CowardBehavior()
extern void EmeraldAIBehaviors_CowardBehavior_mB689D61F6221DAA0C064E13D7FEBAA3F80A1E43D (void);
// 0x0000009F System.Void EmeraldAI.EmeraldAIBehaviors::CautiousBehavior()
extern void EmeraldAIBehaviors_CautiousBehavior_m4CE224C98010502DEEF994C63A8740E997316DA5 (void);
// 0x000000A0 System.Void EmeraldAI.EmeraldAIBehaviors::DeadState()
extern void EmeraldAIBehaviors_DeadState_m062BD32E943F4F022A199457199A361661CF9162 (void);
// 0x000000A1 System.Void EmeraldAI.EmeraldAIBehaviors::FleeState()
extern void EmeraldAIBehaviors_FleeState_m4FC6DE05A898F07675B002767581FA4D283D8BB4 (void);
// 0x000000A2 System.Void EmeraldAI.EmeraldAIBehaviors::CheckAnimationStates()
extern void EmeraldAIBehaviors_CheckAnimationStates_m35B6C55ACE320021197141B84CBF66FCAB39DCCB (void);
// 0x000000A3 System.Void EmeraldAI.EmeraldAIBehaviors::DefaultState()
extern void EmeraldAIBehaviors_DefaultState_m48B28B99B3CCFDFB8C9971DF7C0CFDFA44279070 (void);
// 0x000000A4 System.Void EmeraldAI.EmeraldAIBehaviors::TransitionIK()
extern void EmeraldAIBehaviors_TransitionIK_m7CEC3DAF85FBF535EBEAE50B1093891CC7D686B5 (void);
// 0x000000A5 System.Collections.IEnumerator EmeraldAI.EmeraldAIBehaviors::DelayEquipAnimations()
extern void EmeraldAIBehaviors_DelayEquipAnimations_mC07369172B62213C0FE5CFB8BE9B72EF518A6BE4 (void);
// 0x000000A6 System.Void EmeraldAI.EmeraldAIBehaviors::DelaySearch()
extern void EmeraldAIBehaviors_DelaySearch_mA91BE9D4E9C780E9965B39A46799221B028DEF43 (void);
// 0x000000A7 System.Collections.IEnumerator EmeraldAI.EmeraldAIBehaviors::ReturnToStartComplete()
extern void EmeraldAIBehaviors_ReturnToStartComplete_m6581E603253BF8736A8CB196C148C32B9A671D4A (void);
// 0x000000A8 System.Collections.IEnumerator EmeraldAI.EmeraldAIBehaviors::RefillHeathOverTime()
extern void EmeraldAIBehaviors_RefillHeathOverTime_m3339F3251B6DAC14E7B475B80CF9217FD436E31E (void);
// 0x000000A9 System.Collections.IEnumerator EmeraldAI.EmeraldAIBehaviors::DelayReturnToDestination(System.Single)
extern void EmeraldAIBehaviors_DelayReturnToDestination_mEC7825D55D658132A2F7AED474844DDACBCB83C1 (void);
// 0x000000AA System.Void EmeraldAI.EmeraldAIBehaviors::ActivateCombatState()
extern void EmeraldAIBehaviors_ActivateCombatState_m2892705D803491AC3D1324AA856583CCABB3B4D7 (void);
// 0x000000AB System.Void EmeraldAI.EmeraldAIBehaviors::AttackState()
extern void EmeraldAIBehaviors_AttackState_mB5DDF71D29B3C83977362DD8118AB3097EEDD51E (void);
// 0x000000AC System.Void EmeraldAI.EmeraldAIBehaviors::ResetAttackGeneration()
extern void EmeraldAIBehaviors_ResetAttackGeneration_m65802B58C8515FE8FFE06400ABF44ACF63F29346 (void);
// 0x000000AD System.Void EmeraldAI.EmeraldAIBehaviors::BlockState()
extern void EmeraldAIBehaviors_BlockState_m78C2398EB9D4D06132CBD4727A2E3FAE485F3504 (void);
// 0x000000AE System.Void EmeraldAI.EmeraldAIBehaviors::ActivateBlockCooldown()
extern void EmeraldAIBehaviors_ActivateBlockCooldown_m859A5E2C44D7B30B98B6466AD677082E08BA7F44 (void);
// 0x000000AF System.Void EmeraldAI.EmeraldAIBehaviors::BackupState()
extern void EmeraldAIBehaviors_BackupState_m0F15793142F3232B07F79A1DA0C09C308A3A74A1 (void);
// 0x000000B0 System.Void EmeraldAI.EmeraldAIBehaviors::BackupDelay()
extern void EmeraldAIBehaviors_BackupDelay_m8293D9B933BE738315E528CAAA2922EB0E603030 (void);
// 0x000000B1 System.Void EmeraldAI.EmeraldAIBehaviors::StopBackingUp()
extern void EmeraldAIBehaviors_StopBackingUp_m803261C36EE176ABBC450F12B2135BD72FA29304 (void);
// 0x000000B2 System.Void EmeraldAI.EmeraldAIBehaviors::CalculateBackupState()
extern void EmeraldAIBehaviors_CalculateBackupState_mA6B1C22CC659253963186DE9435E3B02717C2F78 (void);
// 0x000000B3 System.Void EmeraldAI.EmeraldAIBehaviors::.ctor()
extern void EmeraldAIBehaviors__ctor_mDD4255615CA687A7B1519B21CA1F013924BF2792 (void);
// 0x000000B4 System.Boolean EmeraldAI.EmeraldAIBehaviors::<DelayEquipAnimations>b__20_0()
extern void EmeraldAIBehaviors_U3CDelayEquipAnimationsU3Eb__20_0_m2D0580FA86EE405FE65877E445F21EC4206A5AC8 (void);
// 0x000000B5 System.Boolean EmeraldAI.EmeraldAIBehaviors::<ReturnToStartComplete>b__22_0()
extern void EmeraldAIBehaviors_U3CReturnToStartCompleteU3Eb__22_0_m6CFAF1250E9641ECC90D10E41ADA5D0F48D6C8B1 (void);
// 0x000000B6 System.Boolean EmeraldAI.EmeraldAIBehaviors::<DelayReturnToDestination>b__24_0()
extern void EmeraldAIBehaviors_U3CDelayReturnToDestinationU3Eb__24_0_m2E13073FFF5E047880D7A5F896986EC113B41D86 (void);
// 0x000000B7 System.Void EmeraldAI.EmeraldAIBehaviors/<DelayEquipAnimations>d__20::.ctor(System.Int32)
extern void U3CDelayEquipAnimationsU3Ed__20__ctor_mC28F7B9A0932814CA8067735C44FA22B7C9329E0 (void);
// 0x000000B8 System.Void EmeraldAI.EmeraldAIBehaviors/<DelayEquipAnimations>d__20::System.IDisposable.Dispose()
extern void U3CDelayEquipAnimationsU3Ed__20_System_IDisposable_Dispose_mADB3B4F59F1F08139264A1F746518957D507D214 (void);
// 0x000000B9 System.Boolean EmeraldAI.EmeraldAIBehaviors/<DelayEquipAnimations>d__20::MoveNext()
extern void U3CDelayEquipAnimationsU3Ed__20_MoveNext_m63DF577CF141F1FAAEC056A97827A9A2BBC0F0F4 (void);
// 0x000000BA System.Object EmeraldAI.EmeraldAIBehaviors/<DelayEquipAnimations>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayEquipAnimationsU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABF3F18C6DB36C3DF4685A2CE2ECC2B912F9D7C2 (void);
// 0x000000BB System.Void EmeraldAI.EmeraldAIBehaviors/<DelayEquipAnimations>d__20::System.Collections.IEnumerator.Reset()
extern void U3CDelayEquipAnimationsU3Ed__20_System_Collections_IEnumerator_Reset_mB783909125A81C8270D311F1D34F5A3EBB135C79 (void);
// 0x000000BC System.Object EmeraldAI.EmeraldAIBehaviors/<DelayEquipAnimations>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CDelayEquipAnimationsU3Ed__20_System_Collections_IEnumerator_get_Current_m379CDD31E2F4C677D9140A4D6AB989F289CD2050 (void);
// 0x000000BD System.Void EmeraldAI.EmeraldAIBehaviors/<ReturnToStartComplete>d__22::.ctor(System.Int32)
extern void U3CReturnToStartCompleteU3Ed__22__ctor_mCBD78CE7279158D92DF62D02ACB56E5F6FEC9545 (void);
// 0x000000BE System.Void EmeraldAI.EmeraldAIBehaviors/<ReturnToStartComplete>d__22::System.IDisposable.Dispose()
extern void U3CReturnToStartCompleteU3Ed__22_System_IDisposable_Dispose_m773ACEA685C87F6AE5F33BE84876E7FD56569F8A (void);
// 0x000000BF System.Boolean EmeraldAI.EmeraldAIBehaviors/<ReturnToStartComplete>d__22::MoveNext()
extern void U3CReturnToStartCompleteU3Ed__22_MoveNext_m8F6EFF4EF3497B9D7287D88A6343389B788048F5 (void);
// 0x000000C0 System.Object EmeraldAI.EmeraldAIBehaviors/<ReturnToStartComplete>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReturnToStartCompleteU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8BBFAFEF25ACD59187759170F0FC043BB2EBDE0 (void);
// 0x000000C1 System.Void EmeraldAI.EmeraldAIBehaviors/<ReturnToStartComplete>d__22::System.Collections.IEnumerator.Reset()
extern void U3CReturnToStartCompleteU3Ed__22_System_Collections_IEnumerator_Reset_mAFF1F5A6A2A7E33BB20C0271AAA40A226B0FAB97 (void);
// 0x000000C2 System.Object EmeraldAI.EmeraldAIBehaviors/<ReturnToStartComplete>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CReturnToStartCompleteU3Ed__22_System_Collections_IEnumerator_get_Current_m613A88DE4C81417931AD51CB11E9CBC7F545B211 (void);
// 0x000000C3 System.Void EmeraldAI.EmeraldAIBehaviors/<RefillHeathOverTime>d__23::.ctor(System.Int32)
extern void U3CRefillHeathOverTimeU3Ed__23__ctor_mF7AACF292E855BE82BCD5D3BC6D2DE45F0CBD944 (void);
// 0x000000C4 System.Void EmeraldAI.EmeraldAIBehaviors/<RefillHeathOverTime>d__23::System.IDisposable.Dispose()
extern void U3CRefillHeathOverTimeU3Ed__23_System_IDisposable_Dispose_m6DE30C83C3B577C316307E8FF810F51164A00B10 (void);
// 0x000000C5 System.Boolean EmeraldAI.EmeraldAIBehaviors/<RefillHeathOverTime>d__23::MoveNext()
extern void U3CRefillHeathOverTimeU3Ed__23_MoveNext_m34F6BC72752F56E06279F6A9F53165ABC50E240D (void);
// 0x000000C6 System.Object EmeraldAI.EmeraldAIBehaviors/<RefillHeathOverTime>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRefillHeathOverTimeU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC5CA0AF7C6D7A8383D424D028EBA6543C9691D2 (void);
// 0x000000C7 System.Void EmeraldAI.EmeraldAIBehaviors/<RefillHeathOverTime>d__23::System.Collections.IEnumerator.Reset()
extern void U3CRefillHeathOverTimeU3Ed__23_System_Collections_IEnumerator_Reset_m8B2EDED2F2588D715046E33E6A6DADD12FC774EA (void);
// 0x000000C8 System.Object EmeraldAI.EmeraldAIBehaviors/<RefillHeathOverTime>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CRefillHeathOverTimeU3Ed__23_System_Collections_IEnumerator_get_Current_mA072318E5B58EB87E77579E4C065308B5459A341 (void);
// 0x000000C9 System.Void EmeraldAI.EmeraldAIBehaviors/<DelayReturnToDestination>d__24::.ctor(System.Int32)
extern void U3CDelayReturnToDestinationU3Ed__24__ctor_mA731A0F0E2B51D70B20698A336F663A9DF1EA914 (void);
// 0x000000CA System.Void EmeraldAI.EmeraldAIBehaviors/<DelayReturnToDestination>d__24::System.IDisposable.Dispose()
extern void U3CDelayReturnToDestinationU3Ed__24_System_IDisposable_Dispose_m961588FCFE99030517001764E2F0D2F6EAF71DAC (void);
// 0x000000CB System.Boolean EmeraldAI.EmeraldAIBehaviors/<DelayReturnToDestination>d__24::MoveNext()
extern void U3CDelayReturnToDestinationU3Ed__24_MoveNext_m431106726FA63DF16E527F62A2E21A3573047544 (void);
// 0x000000CC System.Object EmeraldAI.EmeraldAIBehaviors/<DelayReturnToDestination>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayReturnToDestinationU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB453FF707E50EA919CCECF9B0FD2A8B003A7FE9 (void);
// 0x000000CD System.Void EmeraldAI.EmeraldAIBehaviors/<DelayReturnToDestination>d__24::System.Collections.IEnumerator.Reset()
extern void U3CDelayReturnToDestinationU3Ed__24_System_Collections_IEnumerator_Reset_mDC6B63C00D570C96E7B90E7EEF967EB016562BDD (void);
// 0x000000CE System.Object EmeraldAI.EmeraldAIBehaviors/<DelayReturnToDestination>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CDelayReturnToDestinationU3Ed__24_System_Collections_IEnumerator_get_Current_m6E38B155A688B382A9DC4928D6FA90582783DCC1 (void);
// 0x000000CF System.Void EmeraldAI.EmeraldAICombatTextData::.ctor()
extern void EmeraldAICombatTextData__ctor_m9D5AF700019E873B6C6CED9CBC02D665FCA9D153 (void);
// 0x000000D0 System.Void EmeraldAI.EmeraldAIEventsManager::Awake()
extern void EmeraldAIEventsManager_Awake_m86247D8999FCCAB8FBCF401E1D37E8010575EFEB (void);
// 0x000000D1 System.Void EmeraldAI.EmeraldAIEventsManager::PlaySoundClip(UnityEngine.AudioClip)
extern void EmeraldAIEventsManager_PlaySoundClip_m7B85028D1E8E5DAC76594F9CB2503B4C49E7951D (void);
// 0x000000D2 System.Void EmeraldAI.EmeraldAIEventsManager::PlayIdleSound()
extern void EmeraldAIEventsManager_PlayIdleSound_m427D11FCC5EE576517FB6CC17EF0D84507E0A79C (void);
// 0x000000D3 System.Void EmeraldAI.EmeraldAIEventsManager::PlayAttackSound()
extern void EmeraldAIEventsManager_PlayAttackSound_m78317319CDE6B1C99B7A1976794ECBA23BEB09C0 (void);
// 0x000000D4 System.Void EmeraldAI.EmeraldAIEventsManager::PlayWarningSound()
extern void EmeraldAIEventsManager_PlayWarningSound_m21F9264BC9AE517F20FFB0D14121F3033EA6E21F (void);
// 0x000000D5 System.Void EmeraldAI.EmeraldAIEventsManager::PlayImpactSound()
extern void EmeraldAIEventsManager_PlayImpactSound_mC5809D0B592632F118AC79B82A6E3633AF68020C (void);
// 0x000000D6 System.Void EmeraldAI.EmeraldAIEventsManager::PlayCriticalHitSound()
extern void EmeraldAIEventsManager_PlayCriticalHitSound_mA9F1DF9095F4E83C587137ED05513C9FB7F4A64F (void);
// 0x000000D7 System.Void EmeraldAI.EmeraldAIEventsManager::PlayBlockSound()
extern void EmeraldAIEventsManager_PlayBlockSound_m2C3AED6CD4F21748585037993A5804AA1071D4A2 (void);
// 0x000000D8 System.Void EmeraldAI.EmeraldAIEventsManager::PlayInjuredSound()
extern void EmeraldAIEventsManager_PlayInjuredSound_mA068DF9A68F7F6D7E7E45AA268A2931A33116AB5 (void);
// 0x000000D9 System.Void EmeraldAI.EmeraldAIEventsManager::PlayDeathSound()
extern void EmeraldAIEventsManager_PlayDeathSound_m3FED169B0D07C2D8694268FCDB2230E37073B0B1 (void);
// 0x000000DA System.Void EmeraldAI.EmeraldAIEventsManager::WalkFootstepSound()
extern void EmeraldAIEventsManager_WalkFootstepSound_mE3D74DDEF3C21E2A33B7D38BECEE481B95E14BBE (void);
// 0x000000DB System.Void EmeraldAI.EmeraldAIEventsManager::RunFootstepSound()
extern void EmeraldAIEventsManager_RunFootstepSound_m9DDE618D9C449229AB50546027151C1E87ABA2BF (void);
// 0x000000DC System.Void EmeraldAI.EmeraldAIEventsManager::PlayRandomSoundEffect()
extern void EmeraldAIEventsManager_PlayRandomSoundEffect_m6F27BCCD47E9E569A911826502ABBA95E6002860 (void);
// 0x000000DD System.Void EmeraldAI.EmeraldAIEventsManager::PlaySoundEffect(System.Int32)
extern void EmeraldAIEventsManager_PlaySoundEffect_m3774F2EC2AA22B5E238DFAC8326C87FD4FFBB45B (void);
// 0x000000DE System.Void EmeraldAI.EmeraldAIEventsManager::EnableItem(System.Int32)
extern void EmeraldAIEventsManager_EnableItem_m756A8A6B617A4A5D5A422E0E4300D4BC0F79BA5E (void);
// 0x000000DF System.Void EmeraldAI.EmeraldAIEventsManager::DisableItem(System.Int32)
extern void EmeraldAIEventsManager_DisableItem_m4EC753151D2FF2C105F900A810A89EEA4871AE18 (void);
// 0x000000E0 System.Void EmeraldAI.EmeraldAIEventsManager::DisableAllItems()
extern void EmeraldAIEventsManager_DisableAllItems_m2928471527E6C91C08515A2C621770605C6A7FFE (void);
// 0x000000E1 System.Void EmeraldAI.EmeraldAIEventsManager::EquipWeapon(System.String)
extern void EmeraldAIEventsManager_EquipWeapon_m56E545884430522B1A2BA2995BC3EAA6A7139406 (void);
// 0x000000E2 System.Void EmeraldAI.EmeraldAIEventsManager::UnequipWeapon(System.String)
extern void EmeraldAIEventsManager_UnequipWeapon_m25B683C08DC00890EA9D6966402EB902ED119534 (void);
// 0x000000E3 System.Void EmeraldAI.EmeraldAIEventsManager::CancelAttackAnimation()
extern void EmeraldAIEventsManager_CancelAttackAnimation_m52483F86B3AC8CF79C256CA4513D09A6069F0641 (void);
// 0x000000E4 System.Void EmeraldAI.EmeraldAIEventsManager::PlayEmoteAnimation(System.Int32)
extern void EmeraldAIEventsManager_PlayEmoteAnimation_mAE9EF0BA5F32BDD79DCF8F896C2B10FB3F750D8C (void);
// 0x000000E5 System.Void EmeraldAI.EmeraldAIEventsManager::LoopEmoteAnimation(System.Int32)
extern void EmeraldAIEventsManager_LoopEmoteAnimation_mB8693D0A6293EF4EFA323F9C44E9398D74744F1A (void);
// 0x000000E6 System.Void EmeraldAI.EmeraldAIEventsManager::StopLoopEmoteAnimation(System.Int32)
extern void EmeraldAIEventsManager_StopLoopEmoteAnimation_mF1E65CE5EB4647FC0466EA6C49EAEB25427F6BE7 (void);
// 0x000000E7 System.Void EmeraldAI.EmeraldAIEventsManager::SpawnAdditionalEffect(UnityEngine.GameObject)
extern void EmeraldAIEventsManager_SpawnAdditionalEffect_mD181A8FE21B8EB7E3437800AD6AB66BDF5732833 (void);
// 0x000000E8 System.Void EmeraldAI.EmeraldAIEventsManager::SpawnEffectOnTarget(UnityEngine.GameObject)
extern void EmeraldAIEventsManager_SpawnEffectOnTarget_m7B263068D49445DAAA5C5AE6566E8FCB318B3BFB (void);
// 0x000000E9 System.Void EmeraldAI.EmeraldAIEventsManager::SpawnBloodSplatEffect(UnityEngine.GameObject)
extern void EmeraldAIEventsManager_SpawnBloodSplatEffect_mBE5B767EC330CCABF41F541B6B507679EEF736CA (void);
// 0x000000EA System.Void EmeraldAI.EmeraldAIEventsManager::KillAI()
extern void EmeraldAIEventsManager_KillAI_mB23CD78E5D436AD31C6B4004D52D7607FAB844F8 (void);
// 0x000000EB System.Void EmeraldAI.EmeraldAIEventsManager::OverrideIdleAnimation(System.Int32)
extern void EmeraldAIEventsManager_OverrideIdleAnimation_m5C0DC0B58806AD6FB032B049FCDB642110D4F67A (void);
// 0x000000EC System.Void EmeraldAI.EmeraldAIEventsManager::DisableOverrideIdleAnimation()
extern void EmeraldAIEventsManager_DisableOverrideIdleAnimation_mF7AC9AF8A9CD8C73285B65B96B700755856795B4 (void);
// 0x000000ED System.Void EmeraldAI.EmeraldAIEventsManager::ChangeBehavior(EmeraldAI.EmeraldAISystem/CurrentBehavior,System.Boolean)
extern void EmeraldAIEventsManager_ChangeBehavior_m84795F14D5C26885C699EF9E808D3298038FDE12 (void);
// 0x000000EE System.Void EmeraldAI.EmeraldAIEventsManager::ChangeConfidence(EmeraldAI.EmeraldAISystem/ConfidenceType,System.Boolean)
extern void EmeraldAIEventsManager_ChangeConfidence_m59C67437A9C818BAFE590853CC1E8524EF3AD314 (void);
// 0x000000EF System.Void EmeraldAI.EmeraldAIEventsManager::ChangeWanderType(EmeraldAI.EmeraldAISystem/WanderType)
extern void EmeraldAIEventsManager_ChangeWanderType_m63C8ADBDCEC905C72E310D5FDEC6CB6AF1C6C64D (void);
// 0x000000F0 System.Void EmeraldAI.EmeraldAIEventsManager::CreateDroppableWeapon()
extern void EmeraldAIEventsManager_CreateDroppableWeapon_m8D55033132AA00FDBA6BEE4E85A34BC8DE0B5EE2 (void);
// 0x000000F1 System.Void EmeraldAI.EmeraldAIEventsManager::ClearTarget(System.Nullable`1<System.Boolean>)
extern void EmeraldAIEventsManager_ClearTarget_mA76C9CBEB93218FA1A25E61BA67FB3C5C7B1E754 (void);
// 0x000000F2 System.Void EmeraldAI.EmeraldAIEventsManager::ClearAllIgnoredTargets()
extern void EmeraldAIEventsManager_ClearAllIgnoredTargets_m7B9926C565672DD3880256DB31C99162991DFA38 (void);
// 0x000000F3 System.Void EmeraldAI.EmeraldAIEventsManager::SetIgnoredTarget(UnityEngine.Transform)
extern void EmeraldAIEventsManager_SetIgnoredTarget_m05E874DB8E6A9D9FBB7D5F62A4D5B39E0F86E394 (void);
// 0x000000F4 System.Void EmeraldAI.EmeraldAIEventsManager::ClearIgnoredTarget(UnityEngine.Transform)
extern void EmeraldAIEventsManager_ClearIgnoredTarget_m4F673E3A6E9F6D8490A18EEE7C2EC9711D5BABA5 (void);
// 0x000000F5 System.Void EmeraldAI.EmeraldAIEventsManager::ReturnToStart()
extern void EmeraldAIEventsManager_ReturnToStart_mB1A5D6F37F2CC0562CB240A3A83618DF7897B245 (void);
// 0x000000F6 System.Void EmeraldAI.EmeraldAIEventsManager::DelayTargetReset()
extern void EmeraldAIEventsManager_DelayTargetReset_mDFFE09D09985396A60D95AC7693D11739C001B40 (void);
// 0x000000F7 System.Single EmeraldAI.EmeraldAIEventsManager::GetDistanceFromTarget()
extern void EmeraldAIEventsManager_GetDistanceFromTarget_m5E260BB8162380E9195D0082D023E7F5CCF462C0 (void);
// 0x000000F8 UnityEngine.Transform EmeraldAI.EmeraldAIEventsManager::GetCombatTarget()
extern void EmeraldAIEventsManager_GetCombatTarget_m6B2D38B6E1DBAC00B64DCD233612728A7424603F (void);
// 0x000000F9 System.Void EmeraldAI.EmeraldAIEventsManager::SetCombatTarget(UnityEngine.Transform)
extern void EmeraldAIEventsManager_SetCombatTarget_mF3AD01033C4D8C64164AB703F23A3A484BDC6D77 (void);
// 0x000000FA System.Void EmeraldAI.EmeraldAIEventsManager::OverrideCombatTarget(UnityEngine.Transform)
extern void EmeraldAIEventsManager_OverrideCombatTarget_m4CBC8FDC9AD3022F91FA6320D01CC699CF27DBB5 (void);
// 0x000000FB System.Void EmeraldAI.EmeraldAIEventsManager::FleeFromTarget(UnityEngine.Transform)
extern void EmeraldAIEventsManager_FleeFromTarget_m51918897B76DACB5FB330163D372341A27958B21 (void);
// 0x000000FC System.Void EmeraldAI.EmeraldAIEventsManager::SetFollowerTarget(UnityEngine.Transform)
extern void EmeraldAIEventsManager_SetFollowerTarget_mF09475C63A6592FE45EF00ABF8ADB16F30CA46A3 (void);
// 0x000000FD System.Void EmeraldAI.EmeraldAIEventsManager::TameAI(UnityEngine.Transform)
extern void EmeraldAIEventsManager_TameAI_mEC09149A9834F8430BCE5C1CA21EAAC42CF24DE6 (void);
// 0x000000FE UnityEngine.Transform EmeraldAI.EmeraldAIEventsManager::GetAttacker()
extern void EmeraldAIEventsManager_GetAttacker_m8C215C39C2B4555CD22CA55DBA320693917300C1 (void);
// 0x000000FF System.Void EmeraldAI.EmeraldAIEventsManager::ChangeDetectionType(EmeraldAI.EmeraldAISystem/DetectionType)
extern void EmeraldAIEventsManager_ChangeDetectionType_mE9047D9E5151A3598156BCEE264FA4724659854D (void);
// 0x00000100 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateUIHealthBarColor(UnityEngine.Color)
extern void EmeraldAIEventsManager_UpdateUIHealthBarColor_m1242F8636A5188BAE63716AA14F8F99EB9A049EB (void);
// 0x00000101 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateUIHealthBarBackgroundColor(UnityEngine.Color)
extern void EmeraldAIEventsManager_UpdateUIHealthBarBackgroundColor_m4F4D14B8C40C0403E0E2C34D733A482900B4A2DF (void);
// 0x00000102 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateUINameColor(UnityEngine.Color)
extern void EmeraldAIEventsManager_UpdateUINameColor_m69225FA94BA69A93BEBCEA7AFCDB73C544A91FDB (void);
// 0x00000103 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateUINameText(System.String)
extern void EmeraldAIEventsManager_UpdateUINameText_mAC618D101EBD21729BEFE79DD6A73E505801F51D (void);
// 0x00000104 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateHealth(System.Int32,System.Int32)
extern void EmeraldAIEventsManager_UpdateHealth_m3345BDC1BB74C7CEBA43BE6612CF19198BA7BB0F (void);
// 0x00000105 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateDynamicWanderPosition()
extern void EmeraldAIEventsManager_UpdateDynamicWanderPosition_m2B96552D5844BAD1226B6E162ED8A73365C4C22B (void);
// 0x00000106 System.Void EmeraldAI.EmeraldAIEventsManager::SetDynamicWanderPosition(UnityEngine.Transform)
extern void EmeraldAIEventsManager_SetDynamicWanderPosition_m475ACFCAC27831FA004F59C7EB986680CE41BBCD (void);
// 0x00000107 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateStartingPosition()
extern void EmeraldAIEventsManager_UpdateStartingPosition_m3D71E887A5E79F4E9178984C3246DF5D31A079B5 (void);
// 0x00000108 System.Void EmeraldAI.EmeraldAIEventsManager::SetDestination(UnityEngine.Transform)
extern void EmeraldAIEventsManager_SetDestination_mCD9B689C6FCF34ABA09AE7F54EDA807F9C5B0D80 (void);
// 0x00000109 System.Void EmeraldAI.EmeraldAIEventsManager::SetDestinationPosition(UnityEngine.Vector3)
extern void EmeraldAIEventsManager_SetDestinationPosition_m65E3987FEF6495A9CDFE1440D359C5155FF6F118 (void);
// 0x0000010A System.Void EmeraldAI.EmeraldAIEventsManager::AddWaypoint(UnityEngine.Transform)
extern void EmeraldAIEventsManager_AddWaypoint_mF6355664E4C40E4366DFB8B49D13EA987201C2F0 (void);
// 0x0000010B System.Void EmeraldAI.EmeraldAIEventsManager::RemoveWaypoint(System.Int32)
extern void EmeraldAIEventsManager_RemoveWaypoint_m142C0B5AEA0109915C013BB88641411C9BFCF912 (void);
// 0x0000010C System.Void EmeraldAI.EmeraldAIEventsManager::ClearAllWaypoints()
extern void EmeraldAIEventsManager_ClearAllWaypoints_m60A2ADD5FC7DC66286D989142EE9DE235F77541D (void);
// 0x0000010D System.Void EmeraldAI.EmeraldAIEventsManager::InstantlyRefillAIHealth()
extern void EmeraldAIEventsManager_InstantlyRefillAIHealth_m49AE4C668EADDF5206A7F4F1EACB383166F51BED (void);
// 0x0000010E System.Void EmeraldAI.EmeraldAIEventsManager::StopMovement()
extern void EmeraldAIEventsManager_StopMovement_m0CB67B845482AE89F77D86FE7B9B3D071C40BCB3 (void);
// 0x0000010F System.Void EmeraldAI.EmeraldAIEventsManager::ResumeMovement()
extern void EmeraldAIEventsManager_ResumeMovement_m19355C358E099909F5416A838C271A7C8734816A (void);
// 0x00000110 System.Void EmeraldAI.EmeraldAIEventsManager::StopFollowing()
extern void EmeraldAIEventsManager_StopFollowing_m5C9FBFCD199E447A07742EE4A17B9B7DB1DC0658 (void);
// 0x00000111 System.Void EmeraldAI.EmeraldAIEventsManager::ResumeFollowing()
extern void EmeraldAIEventsManager_ResumeFollowing_m5F7835725A32B26B3CABD55145416E46CA1E2974 (void);
// 0x00000112 System.Void EmeraldAI.EmeraldAIEventsManager::CompanionGuardPosition(UnityEngine.Vector3)
extern void EmeraldAIEventsManager_CompanionGuardPosition_mE328AB3384C8D9C3B6322195B3B19F6BF57CA07A (void);
// 0x00000113 System.Void EmeraldAI.EmeraldAIEventsManager::SearchForClosestTarget()
extern void EmeraldAIEventsManager_SearchForClosestTarget_mB43011770D083070EA70E7FCBAD5F932000BCF09 (void);
// 0x00000114 System.Void EmeraldAI.EmeraldAIEventsManager::SearchForRandomTarget()
extern void EmeraldAIEventsManager_SearchForRandomTarget_m410CC79342F8B1868B4004C5DF112C0A62891159 (void);
// 0x00000115 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateAIMeleeDamage(System.Int32,System.Int32,System.Int32)
extern void EmeraldAIEventsManager_UpdateAIMeleeDamage_m70AAED957438A5DD58E125D4B50FEE8B92AFBFD2 (void);
// 0x00000116 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateAIMeleeAttackSpeed(System.Int32,System.Int32)
extern void EmeraldAIEventsManager_UpdateAIMeleeAttackSpeed_m9E3B2C659A8E50725E575D518C2C9E3C5FD7CF3A (void);
// 0x00000117 System.Void EmeraldAI.EmeraldAIEventsManager::UpdateAIRangedAttackSpeed(System.Int32,System.Int32)
extern void EmeraldAIEventsManager_UpdateAIRangedAttackSpeed_m0FA1C185ED0D860419D984F22CE5258E78DCD985 (void);
// 0x00000118 System.Void EmeraldAI.EmeraldAIEventsManager::RotateAITowardsTarget(UnityEngine.Transform,System.Int32)
extern void EmeraldAIEventsManager_RotateAITowardsTarget_m69028ABA7E850FDD04803DB8CAD57B7975D3757E (void);
// 0x00000119 System.Void EmeraldAI.EmeraldAIEventsManager::CancelRotateAITowardsTarget()
extern void EmeraldAIEventsManager_CancelRotateAITowardsTarget_mB0427BDCE6C83E8BE99DAEE0C8EB92A3324781B6 (void);
// 0x0000011A System.Void EmeraldAI.EmeraldAIEventsManager::RotateAIAwayFromTarget(UnityEngine.Transform,System.Int32)
extern void EmeraldAIEventsManager_RotateAIAwayFromTarget_m147A0774AB353348CF0E27555EB8F90F00347849 (void);
// 0x0000011B System.Void EmeraldAI.EmeraldAIEventsManager::CancelRotateAIAwayFromTarget()
extern void EmeraldAIEventsManager_CancelRotateAIAwayFromTarget_m71B26B10AB85F013CE4C6C725B24173C0156BE18 (void);
// 0x0000011C System.Void EmeraldAI.EmeraldAIEventsManager::RotateAITowardsPosition(UnityEngine.Vector3,System.Int32)
extern void EmeraldAIEventsManager_RotateAITowardsPosition_m65775B788D6B40B62404B4EBBEECFD1483CB8E32 (void);
// 0x0000011D System.Void EmeraldAI.EmeraldAIEventsManager::CancelRotateAITowardsPosition()
extern void EmeraldAIEventsManager_CancelRotateAITowardsPosition_m0DD37540500CA6D3E2C37510AAF12F2F3D0156B8 (void);
// 0x0000011E System.Void EmeraldAI.EmeraldAIEventsManager::RotateAIAwayFromPosition(UnityEngine.Vector3,System.Int32)
extern void EmeraldAIEventsManager_RotateAIAwayFromPosition_m73501DA6647D09C85550071D93E4F0B076555BCF (void);
// 0x0000011F System.Void EmeraldAI.EmeraldAIEventsManager::CancelRotateAIAwayFromPosition()
extern void EmeraldAIEventsManager_CancelRotateAIAwayFromPosition_mEC05AE9E6CBB729E80E37FC3A9354FD36B210884 (void);
// 0x00000120 System.Collections.IEnumerator EmeraldAI.EmeraldAIEventsManager::RotateTowards(UnityEngine.Transform,System.Int32,UnityEngine.Vector3,System.Boolean)
extern void EmeraldAIEventsManager_RotateTowards_mEDF8F201EF306ECF05FC674D04A7B648F8C29E7F (void);
// 0x00000121 System.Void EmeraldAI.EmeraldAIEventsManager::SetFactionLevel(System.String,System.String)
extern void EmeraldAIEventsManager_SetFactionLevel_m32AA9F7FBFBBBEDBBE4C8B95D603278A2C4A1825 (void);
// 0x00000122 System.Void EmeraldAI.EmeraldAIEventsManager::AddFactionRelation(System.String,System.String)
extern void EmeraldAIEventsManager_AddFactionRelation_m1B9B0CBA9378806A973CB70FB9743801201AD510 (void);
// 0x00000123 EmeraldAI.EmeraldAISystem/RelationType EmeraldAI.EmeraldAIEventsManager::GetAIRelation(EmeraldAI.EmeraldAISystem)
extern void EmeraldAIEventsManager_GetAIRelation_m30E821DEAD0A7C9204D4DDC4FBFDBED42AA49C6C (void);
// 0x00000124 EmeraldAI.EmeraldAISystem/RelationType EmeraldAI.EmeraldAIEventsManager::GetPlayerRelation()
extern void EmeraldAIEventsManager_GetPlayerRelation_mC3D0AEA72B2200312340DED9713E741C228A7CE9 (void);
// 0x00000125 System.Void EmeraldAI.EmeraldAIEventsManager::SetPlayerRelation(EmeraldAI.EmeraldAISystem/PlayerFactionClass/RelationType)
extern void EmeraldAIEventsManager_SetPlayerRelation_m11DF2C5E290A4C499992BA35CC02A0EA4BD0E1C8 (void);
// 0x00000126 System.Void EmeraldAI.EmeraldAIEventsManager::ChangeFaction(System.String)
extern void EmeraldAIEventsManager_ChangeFaction_mADCB7005EBF13E9E2771E91B7621FF66685E5BC8 (void);
// 0x00000127 System.Boolean EmeraldAI.EmeraldAIEventsManager::CheckForPlayerDetection()
extern void EmeraldAIEventsManager_CheckForPlayerDetection_m3766237587DD89A753B53783FA1DA9F518CB0947 (void);
// 0x00000128 System.String EmeraldAI.EmeraldAIEventsManager::GetFaction()
extern void EmeraldAIEventsManager_GetFaction_mD403D17CD4045A10F8539022F6428B9978A8521C (void);
// 0x00000129 System.Void EmeraldAI.EmeraldAIEventsManager::ChangeOffsensiveAbilityObject(System.Int32,EmeraldAI.EmeraldAIAbility)
extern void EmeraldAIEventsManager_ChangeOffsensiveAbilityObject_m8354A63DF46427AC77A878F443D00D31DD05FFCD (void);
// 0x0000012A System.Void EmeraldAI.EmeraldAIEventsManager::ChangeSupportAbilityObject(System.Int32,EmeraldAI.EmeraldAIAbility)
extern void EmeraldAIEventsManager_ChangeSupportAbilityObject_m51699FCD4D5DB5D60EC7858B5FEFF57C68241614 (void);
// 0x0000012B System.Void EmeraldAI.EmeraldAIEventsManager::ChangeSummoningAbilityObject(System.Int32,EmeraldAI.EmeraldAIAbility)
extern void EmeraldAIEventsManager_ChangeSummoningAbilityObject_m0306386D2FCDB6D15F351DF5A4DBAFA918D4CF3B (void);
// 0x0000012C System.Void EmeraldAI.EmeraldAIEventsManager::ReturnToDefaultState()
extern void EmeraldAIEventsManager_ReturnToDefaultState_m9A75250B89682EE5598673B62362200DEC54FD90 (void);
// 0x0000012D System.Void EmeraldAI.EmeraldAIEventsManager::DebugLogMessage(System.String)
extern void EmeraldAIEventsManager_DebugLogMessage_mB50FD2000B2FFDDEAB79FB84B9C2E76677FCF4A7 (void);
// 0x0000012E System.Void EmeraldAI.EmeraldAIEventsManager::EnableObject(UnityEngine.GameObject)
extern void EmeraldAIEventsManager_EnableObject_m073A78A9BC02A445E6D429548B3597D76CB4E4E5 (void);
// 0x0000012F System.Void EmeraldAI.EmeraldAIEventsManager::DisableObject(UnityEngine.GameObject)
extern void EmeraldAIEventsManager_DisableObject_mE8CEFD888716D23DA3C6E2A17E192CD2FF8530BD (void);
// 0x00000130 System.Void EmeraldAI.EmeraldAIEventsManager::ResetAI()
extern void EmeraldAIEventsManager_ResetAI_mC11E557AF8ECF144DDAE4C5BE9A92677F52C780A (void);
// 0x00000131 System.Void EmeraldAI.EmeraldAIEventsManager::HealOverTimeAbility(EmeraldAI.EmeraldAIAbility)
extern void EmeraldAIEventsManager_HealOverTimeAbility_m07B7D4489FD90C63181E8127B309A05ECF844DBA (void);
// 0x00000132 System.Collections.IEnumerator EmeraldAI.EmeraldAIEventsManager::HealOverTimeCoroutine(System.Int32,System.Int32,EmeraldAI.EmeraldAIAbility)
extern void EmeraldAIEventsManager_HealOverTimeCoroutine_m196DB237B4A9E292AFB6BA5ACAC100439C319EE8 (void);
// 0x00000133 System.Void EmeraldAI.EmeraldAIEventsManager::.ctor()
extern void EmeraldAIEventsManager__ctor_m181C3FDE2ACCBCFE53C66ADB436F663D93F899AB (void);
// 0x00000134 System.Void EmeraldAI.EmeraldAIEventsManager/<RotateTowards>d__82::.ctor(System.Int32)
extern void U3CRotateTowardsU3Ed__82__ctor_mB8363BE9CEAFA35F7E9D48B5F134D1C2AB811AC8 (void);
// 0x00000135 System.Void EmeraldAI.EmeraldAIEventsManager/<RotateTowards>d__82::System.IDisposable.Dispose()
extern void U3CRotateTowardsU3Ed__82_System_IDisposable_Dispose_m83400FFA02C47A3B2009ADFDCBDE28BC6E024A28 (void);
// 0x00000136 System.Boolean EmeraldAI.EmeraldAIEventsManager/<RotateTowards>d__82::MoveNext()
extern void U3CRotateTowardsU3Ed__82_MoveNext_mA9007CC5400608FD7468189F7C9778978F13684A (void);
// 0x00000137 System.Object EmeraldAI.EmeraldAIEventsManager/<RotateTowards>d__82::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotateTowardsU3Ed__82_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D92741BEB6563498FEA1C6FB68C5088F85D974 (void);
// 0x00000138 System.Void EmeraldAI.EmeraldAIEventsManager/<RotateTowards>d__82::System.Collections.IEnumerator.Reset()
extern void U3CRotateTowardsU3Ed__82_System_Collections_IEnumerator_Reset_mE09B4461BB700C19A39E4F823DEA3D296764CF28 (void);
// 0x00000139 System.Object EmeraldAI.EmeraldAIEventsManager/<RotateTowards>d__82::System.Collections.IEnumerator.get_Current()
extern void U3CRotateTowardsU3Ed__82_System_Collections_IEnumerator_get_Current_mDA5816D2D0C0E13C44476B787F4C8E29596F1A6D (void);
// 0x0000013A System.Void EmeraldAI.EmeraldAIEventsManager/<HealOverTimeCoroutine>d__100::.ctor(System.Int32)
extern void U3CHealOverTimeCoroutineU3Ed__100__ctor_m0447B4035A88E2621F9012ABD48468E8B0F2ED01 (void);
// 0x0000013B System.Void EmeraldAI.EmeraldAIEventsManager/<HealOverTimeCoroutine>d__100::System.IDisposable.Dispose()
extern void U3CHealOverTimeCoroutineU3Ed__100_System_IDisposable_Dispose_mAB3E040CF0DEDFC3225E821E850848AE3FE4EAA8 (void);
// 0x0000013C System.Boolean EmeraldAI.EmeraldAIEventsManager/<HealOverTimeCoroutine>d__100::MoveNext()
extern void U3CHealOverTimeCoroutineU3Ed__100_MoveNext_m0EC92384110CB5084FB566A27655CD0D707FC8B6 (void);
// 0x0000013D System.Object EmeraldAI.EmeraldAIEventsManager/<HealOverTimeCoroutine>d__100::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHealOverTimeCoroutineU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65D7B155EC73E69155DCEAE837E3563BDC9A8D80 (void);
// 0x0000013E System.Void EmeraldAI.EmeraldAIEventsManager/<HealOverTimeCoroutine>d__100::System.Collections.IEnumerator.Reset()
extern void U3CHealOverTimeCoroutineU3Ed__100_System_Collections_IEnumerator_Reset_mB7EE9E4C800DE4F09773EA5B589DE99CB8A6DB41 (void);
// 0x0000013F System.Object EmeraldAI.EmeraldAIEventsManager/<HealOverTimeCoroutine>d__100::System.Collections.IEnumerator.get_Current()
extern void U3CHealOverTimeCoroutineU3Ed__100_System_Collections_IEnumerator_get_Current_m99B141F7F3F8B826DB8CDCFE3F5BAD53D0E6AB9F (void);
// 0x00000140 System.Void EmeraldAI.EmeraldAIFactionData::.ctor()
extern void EmeraldAIFactionData__ctor_mCB3FD267EB15776070339A6EC5A8B144B27EEA6E (void);
// 0x00000141 System.Void EmeraldAI.EmeraldAINeedsSystem::Start()
extern void EmeraldAINeedsSystem_Start_m84AAC58D673860CF625BD2D02E1299668D4A791F (void);
// 0x00000142 System.Void EmeraldAI.EmeraldAINeedsSystem::UpdateWaypoints()
extern void EmeraldAINeedsSystem_UpdateWaypoints_mC107395B67276F5D42AC05D019DEE021814F5183 (void);
// 0x00000143 System.Void EmeraldAI.EmeraldAINeedsSystem::UpdateNeeds()
extern void EmeraldAINeedsSystem_UpdateNeeds_mD9C2EFD4E744397F31968E3ED97F71FADA982C72 (void);
// 0x00000144 System.Void EmeraldAI.EmeraldAINeedsSystem::Update()
extern void EmeraldAINeedsSystem_Update_mE9C16C4FDCB50F2167A7E48D72FB0C597CABBF54 (void);
// 0x00000145 System.Void EmeraldAI.EmeraldAINeedsSystem::.ctor()
extern void EmeraldAINeedsSystem__ctor_m5424C12C5535817FE42D5F82704F668B7B420282 (void);
// 0x00000146 System.Void EmeraldAI.EmeraldAINonAIDamage::Start()
extern void EmeraldAINonAIDamage_Start_mBAF0635B20866E81CFBAFAC37C0A5102347AAFCC (void);
// 0x00000147 System.Void EmeraldAI.EmeraldAINonAIDamage::SendNonAIDamage(System.Int32,UnityEngine.Transform,System.Boolean)
extern void EmeraldAINonAIDamage_SendNonAIDamage_m65611DBD1CF751BB873C9BDBAB3826E443586204 (void);
// 0x00000148 System.Void EmeraldAI.EmeraldAINonAIDamage::DefaultDamage(System.Int32,UnityEngine.Transform)
extern void EmeraldAINonAIDamage_DefaultDamage_mBA2F86897F4C7B41B4C19336D350F0793B7E27E7 (void);
// 0x00000149 System.Void EmeraldAI.EmeraldAINonAIDamage::ResetNonAITarget()
extern void EmeraldAINonAIDamage_ResetNonAITarget_mE0A4BB61BF4A610A8389BD962EDEB2E3F2970831 (void);
// 0x0000014A System.Void EmeraldAI.EmeraldAINonAIDamage::.ctor()
extern void EmeraldAINonAIDamage__ctor_m383EC7C8571779669967488506A26CADF677C8A8 (void);
// 0x0000014B System.Void EmeraldAI.EmeraldAIPlayerDamage::SendPlayerDamage(System.Int32,UnityEngine.Transform,EmeraldAI.EmeraldAISystem,System.Boolean)
extern void EmeraldAIPlayerDamage_SendPlayerDamage_mA3A71D6DD02B3A628BCC32A8AFB52B04A2725F0A (void);
// 0x0000014C System.Void EmeraldAI.EmeraldAIPlayerDamage::DamagePlayerStandard(System.Int32)
extern void EmeraldAIPlayerDamage_DamagePlayerStandard_m4B3192C143C708F366F074C0A63DD4507EE4E612 (void);
// 0x0000014D System.Void EmeraldAI.EmeraldAIPlayerDamage::.ctor()
extern void EmeraldAIPlayerDamage__ctor_m9F84562342A9F354A208CBAFBB08E0583ACD3475 (void);
// 0x0000014E System.Void EmeraldAI.LocationBasedDamage::InitializeLocationBasedDamage()
extern void LocationBasedDamage_InitializeLocationBasedDamage_m05DC961850FF639319964A2992C7ED98825A159D (void);
// 0x0000014F System.Void EmeraldAI.LocationBasedDamage::.ctor()
extern void LocationBasedDamage__ctor_m696C8475F1037CBEB1BB1761F47AE21AA1740955 (void);
// 0x00000150 System.Void EmeraldAI.LocationBasedDamage/LocationBasedDamageClass::.ctor(UnityEngine.Collider,System.Int32)
extern void LocationBasedDamageClass__ctor_mFC1B3EAB2B6A26AC96208E248AD345B74BAC0990 (void);
// 0x00000151 System.Boolean EmeraldAI.LocationBasedDamage/LocationBasedDamageClass::Contains(System.Collections.Generic.List`1<EmeraldAI.LocationBasedDamage/LocationBasedDamageClass>,EmeraldAI.LocationBasedDamage/LocationBasedDamageClass)
extern void LocationBasedDamageClass_Contains_m8FCB31DEBC3C3CBE7613287C9265A68E956D8A87 (void);
// 0x00000152 System.Void EmeraldAI.LocationBasedDamageArea::DamageArea(System.Int32,System.Nullable`1<EmeraldAI.EmeraldAISystem/TargetType>,UnityEngine.Transform,System.Int32,System.Boolean)
extern void LocationBasedDamageArea_DamageArea_m3F17D0ED30BC77484FFFCCACF663E157131BD32B (void);
// 0x00000153 System.Void EmeraldAI.LocationBasedDamageArea::CreateImpactEffect(UnityEngine.Vector3)
extern void LocationBasedDamageArea_CreateImpactEffect_m84AB60EF9ED0712E8D95D1E0859ACA948EC53AF9 (void);
// 0x00000154 System.Void EmeraldAI.LocationBasedDamageArea::.ctor()
extern void LocationBasedDamageArea__ctor_m5A4BCBFDE715CD9FFB8C62E3BB2CDA2EA5C2AEDB (void);
// 0x00000155 System.Void EmeraldAI.CameraShake::Awake()
extern void CameraShake_Awake_m70375D5FD523C97A5D139BD1C82ED86EC5FADCA3 (void);
// 0x00000156 System.Void EmeraldAI.CameraShake::ShakeCamera(System.Single,System.Single)
extern void CameraShake_ShakeCamera_mF94E989FA4062A7CA63CAB82A357FBAC780DAD2B (void);
// 0x00000157 System.Collections.IEnumerator EmeraldAI.CameraShake::CameraShakeSequence(System.Single,System.Single)
extern void CameraShake_CameraShakeSequence_m4D4C8D8F04B1B2422067D73C500E09098946978D (void);
// 0x00000158 System.Void EmeraldAI.CameraShake::.ctor()
extern void CameraShake__ctor_mDE62F0B29C78575A2EC71860091E575B0A32799E (void);
// 0x00000159 System.Void EmeraldAI.CameraShake/<CameraShakeSequence>d__6::.ctor(System.Int32)
extern void U3CCameraShakeSequenceU3Ed__6__ctor_m2E0E7E3322E5C59CCD18196F187F931331A14A97 (void);
// 0x0000015A System.Void EmeraldAI.CameraShake/<CameraShakeSequence>d__6::System.IDisposable.Dispose()
extern void U3CCameraShakeSequenceU3Ed__6_System_IDisposable_Dispose_m29854744E5496ABECF9BBEE1AEA52D8A7FA3A4ED (void);
// 0x0000015B System.Boolean EmeraldAI.CameraShake/<CameraShakeSequence>d__6::MoveNext()
extern void U3CCameraShakeSequenceU3Ed__6_MoveNext_m1CA4204CEDB7D7AED6E6585334D438249056646D (void);
// 0x0000015C System.Object EmeraldAI.CameraShake/<CameraShakeSequence>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCameraShakeSequenceU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6407F666190A1039A848156BE8AC376AA8E3C112 (void);
// 0x0000015D System.Void EmeraldAI.CameraShake/<CameraShakeSequence>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCameraShakeSequenceU3Ed__6_System_Collections_IEnumerator_Reset_mB8EB7F1CB4D826A388A6D987EDF8772039AE2970 (void);
// 0x0000015E System.Object EmeraldAI.CameraShake/<CameraShakeSequence>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCameraShakeSequenceU3Ed__6_System_Collections_IEnumerator_get_Current_m5DBF667B8D29F5B278A8133D3B18D9AB319EF0A9 (void);
// 0x0000015F System.Void EmeraldAI.EmeraldAISystem::Awake()
extern void EmeraldAISystem_Awake_m45B3D4F989520C76C60BA9413AFD25173F99F0DC (void);
// 0x00000160 System.Void EmeraldAI.EmeraldAISystem::CheckPath(UnityEngine.Vector3)
extern void EmeraldAISystem_CheckPath_mBCD46C7C03BF7230CA8C4E7E57893D7F33C78DFF (void);
// 0x00000161 System.Void EmeraldAI.EmeraldAISystem::CheckAIRenderers()
extern void EmeraldAISystem_CheckAIRenderers_mE9DBC0D3A48D5E6E563E2C7219D4BC612347696D (void);
// 0x00000162 System.Void EmeraldAI.EmeraldAISystem::Update()
extern void EmeraldAISystem_Update_mD094F518566CF23487A437095470453A46A7DF25 (void);
// 0x00000163 System.Void EmeraldAI.EmeraldAISystem::WeaponSwitchCooldown()
extern void EmeraldAISystem_WeaponSwitchCooldown_m91248A675D9489D044FF8DB0BAAD6D3CA2D175EA (void);
// 0x00000164 System.Void EmeraldAI.EmeraldAISystem::UpdateWeaponTypeState()
extern void EmeraldAISystem_UpdateWeaponTypeState_mAA66FF07A82EDACEF4F1715F13F365899BCF4CD7 (void);
// 0x00000165 System.Collections.IEnumerator EmeraldAI.EmeraldAISystem::SwitchCurrentWeaponType(System.String)
extern void EmeraldAISystem_SwitchCurrentWeaponType_m9180394306E771B01631C5E6EF83F901F9C30EFC (void);
// 0x00000166 System.Void EmeraldAI.EmeraldAISystem::MoveAINavMesh()
extern void EmeraldAISystem_MoveAINavMesh_m1A50B653AF82034E05C034C9750C40A92B9A5966 (void);
// 0x00000167 System.Void EmeraldAI.EmeraldAISystem::MoveAIRootMotion()
extern void EmeraldAISystem_MoveAIRootMotion_m611969DC25407979553936A39AC017BFF125205B (void);
// 0x00000168 System.Void EmeraldAI.EmeraldAISystem::AlignAIMoving()
extern void EmeraldAISystem_AlignAIMoving_mBBF7F563218B6FADCDF6C149EBC508D6E4EC8842 (void);
// 0x00000169 System.Void EmeraldAI.EmeraldAISystem::AlignAIStationary()
extern void EmeraldAISystem_AlignAIStationary_m98021C3FF5786937FF6E5F4175570C3B0596E8B5 (void);
// 0x0000016A System.Void EmeraldAI.EmeraldAISystem::RotateAIStationary()
extern void EmeraldAISystem_RotateAIStationary_mDEBB79150240E8703DD98C0A958AA0849DF09276 (void);
// 0x0000016B System.Void EmeraldAI.EmeraldAISystem::Wander()
extern void EmeraldAISystem_Wander_m3974BB1EAF8D62EE91329E1FB0F024E7EA3CB669 (void);
// 0x0000016C System.Void EmeraldAI.EmeraldAISystem::ResetWanderTimer()
extern void EmeraldAISystem_ResetWanderTimer_m7E5FADCBE4286E74DC1B5D9CA701116A05F3FF5B (void);
// 0x0000016D System.Void EmeraldAI.EmeraldAISystem::FollowCompanionTarget()
extern void EmeraldAISystem_FollowCompanionTarget_mD2CA859337BE2121FCBA011275AE65F143369DAB (void);
// 0x0000016E System.Void EmeraldAI.EmeraldAISystem::ReverseDelay()
extern void EmeraldAISystem_ReverseDelay_m77214AFAA2D035CDC78556A22BA185C3F8E62F64 (void);
// 0x0000016F System.Void EmeraldAI.EmeraldAISystem::NextWaypoint()
extern void EmeraldAISystem_NextWaypoint_m115040B952728C1E6910DDFA5B187288481728E4 (void);
// 0x00000170 System.Void EmeraldAI.EmeraldAISystem::GenerateWaypoint()
extern void EmeraldAISystem_GenerateWaypoint_m6E03635A06282A52684A309A53CC0042E949C4F1 (void);
// 0x00000171 System.Void EmeraldAI.EmeraldAISystem::CreateEmeraldProjectile()
extern void EmeraldAISystem_CreateEmeraldProjectile_m4AA4035E50376D01A83749B22F79326797EA2E66 (void);
// 0x00000172 System.Collections.IEnumerator EmeraldAI.EmeraldAISystem::InitializeSummonedAI(EmeraldAI.EmeraldAISystem)
extern void EmeraldAISystem_InitializeSummonedAI_mFD0FD121AF6F3D50394C13DA1633689F282137F2 (void);
// 0x00000173 System.Void EmeraldAI.EmeraldAISystem::InitializeAbility(EmeraldAI.Utility.EmeraldAIProjectile,EmeraldAI.EmeraldAIAbility)
extern void EmeraldAISystem_InitializeAbility_mBD8390CE84825AD7F1106E6BB58300D2FA0E1495 (void);
// 0x00000174 System.Void EmeraldAI.EmeraldAISystem::EmeraldAttackEvent()
extern void EmeraldAISystem_EmeraldAttackEvent_mA94173550D5137D87E982264120A6B96EC323551 (void);
// 0x00000175 System.Void EmeraldAI.EmeraldAISystem::SendEmeraldDamage()
extern void EmeraldAISystem_SendEmeraldDamage_mE1A8B6FB20BD879F3392D228C2A0C1A869FE4BA0 (void);
// 0x00000176 System.Void EmeraldAI.EmeraldAISystem::EndAttackEvent()
extern void EmeraldAISystem_EndAttackEvent_m1F6AEFFFC557E445DF9B90E2192C36EFAC2C7640 (void);
// 0x00000177 System.Void EmeraldAI.EmeraldAISystem::Damage(System.Int32,System.Nullable`1<EmeraldAI.EmeraldAISystem/TargetType>,UnityEngine.Transform,System.Int32,System.Boolean)
extern void EmeraldAISystem_Damage_m8E80B7DA340B257B668FD34548304F7A9349084E (void);
// 0x00000178 System.Void EmeraldAI.EmeraldAISystem::Deactivate()
extern void EmeraldAISystem_Deactivate_m4679E47301E4B0A64AF37008C8CBDD3F7F227905 (void);
// 0x00000179 System.Void EmeraldAI.EmeraldAISystem::Activate()
extern void EmeraldAISystem_Activate_m25174B262F175AC2EE04566C8A1BE148BC66698E (void);
// 0x0000017A System.Void EmeraldAI.EmeraldAISystem::GetDamageAmount()
extern void EmeraldAISystem_GetDamageAmount_m6559B147A0BF83FCE7AC39C2A6A363359331D169 (void);
// 0x0000017B System.Boolean EmeraldAI.EmeraldAISystem::GenerateCritOdds()
extern void EmeraldAISystem_GenerateCritOdds_m4DE750CC1795C14DF45211E77930B82BC11AFF7E (void);
// 0x0000017C System.Void EmeraldAI.EmeraldAISystem::SetUI(System.Boolean)
extern void EmeraldAISystem_SetUI_m6A7D3756A5E33BD2B97EB74531820585C2FF297D (void);
// 0x0000017D System.Single EmeraldAI.EmeraldAISystem::TargetAngle()
extern void EmeraldAISystem_TargetAngle_mE345C67AB20E2BA84C7B64D1359DDA5DBEABC209 (void);
// 0x0000017E System.Single EmeraldAI.EmeraldAISystem::DestinationAngle()
extern void EmeraldAISystem_DestinationAngle_mB58D51507640012FB126C213C0B6CDE0052EE22D (void);
// 0x0000017F System.Single EmeraldAI.EmeraldAISystem::TransformAngle(UnityEngine.Transform)
extern void EmeraldAISystem_TransformAngle_m21AD2AC0B6E2277D8FFBF5125DB78F29D4285D24 (void);
// 0x00000180 System.Single EmeraldAI.EmeraldAISystem::HeadLookAngle()
extern void EmeraldAISystem_HeadLookAngle_m391B90F477A6221B6E1B75081B8365853E160518 (void);
// 0x00000181 System.Void EmeraldAI.EmeraldAISystem::.ctor()
extern void EmeraldAISystem__ctor_mC98D210619C37BE50F7ADE362C80640F7B0CF11F (void);
// 0x00000182 System.Void EmeraldAI.EmeraldAISystem::.cctor()
extern void EmeraldAISystem__cctor_m480FBAAF94DAD1353D237C7D43F640DE31389C0B (void);
// 0x00000183 System.Void EmeraldAI.EmeraldAISystem/MeleeAttackClass::.ctor()
extern void MeleeAttackClass__ctor_m40974F2D359B676AD0DF5A57D6438F03E17CE2E1 (void);
// 0x00000184 System.Void EmeraldAI.EmeraldAISystem/OffensiveAbilitiesClass::.ctor()
extern void OffensiveAbilitiesClass__ctor_m89F7284516BF3A2D569F70B4F090E356590D8462 (void);
// 0x00000185 System.Void EmeraldAI.EmeraldAISystem/SupportAbilitiesClass::.ctor()
extern void SupportAbilitiesClass__ctor_mAD4310C6B30D69DE5B125F968F4048FDE4DF0BFB (void);
// 0x00000186 System.Void EmeraldAI.EmeraldAISystem/SummoningAbilitiesClass::.ctor()
extern void SummoningAbilitiesClass__ctor_m6AC5C9025EC5DBE3A9B58CE38EB01A4DE84FF30B (void);
// 0x00000187 System.Void EmeraldAI.EmeraldAISystem/FactionsList::.ctor(System.Int32,System.Int32)
extern void FactionsList__ctor_m9691B451DD02EC3C3D0289D56F077D29C6557F99 (void);
// 0x00000188 System.Void EmeraldAI.EmeraldAISystem/PlayerFactionClass::.ctor(System.String,System.Int32)
extern void PlayerFactionClass__ctor_m68BD6F7813BF7FF5B0D9BFAAA6A4F645BFAAF2CC (void);
// 0x00000189 System.Void EmeraldAI.EmeraldAISystem/InteractSoundClass::.ctor()
extern void InteractSoundClass__ctor_mEA1489AC1FB3C2BC21EE638F3BA88CE480BBFFAE (void);
// 0x0000018A System.Void EmeraldAI.EmeraldAISystem/ItemClass::.ctor()
extern void ItemClass__ctor_m1BCCC38C7EEED0DA1113EF676167EB04F2C2A8D8 (void);
// 0x0000018B System.Void EmeraldAI.EmeraldAISystem/EmoteAnimationClass::.ctor(System.Int32,UnityEngine.AnimationClip)
extern void EmoteAnimationClass__ctor_m6221F073B5AED04C2984BBD2782528B097D2C1D2 (void);
// 0x0000018C System.Void EmeraldAI.EmeraldAISystem/AnimationClass::.ctor(System.Single,UnityEngine.AnimationClip)
extern void AnimationClass__ctor_m6B33A31E564D7A8AE23CC80EDE27B0E521A9D4F0 (void);
// 0x0000018D System.Void EmeraldAI.EmeraldAISystem/<SwitchCurrentWeaponType>d__814::.ctor(System.Int32)
extern void U3CSwitchCurrentWeaponTypeU3Ed__814__ctor_mF6774B9139279CCFAB6B1863145E63FF405F78AB (void);
// 0x0000018E System.Void EmeraldAI.EmeraldAISystem/<SwitchCurrentWeaponType>d__814::System.IDisposable.Dispose()
extern void U3CSwitchCurrentWeaponTypeU3Ed__814_System_IDisposable_Dispose_mDB098D8EE8FEBBBCF37C1B3C1641F726B1D93ABD (void);
// 0x0000018F System.Boolean EmeraldAI.EmeraldAISystem/<SwitchCurrentWeaponType>d__814::MoveNext()
extern void U3CSwitchCurrentWeaponTypeU3Ed__814_MoveNext_mB6197CFB988EC5DCAAE0904D1512D8C494C3CFC0 (void);
// 0x00000190 System.Object EmeraldAI.EmeraldAISystem/<SwitchCurrentWeaponType>d__814::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSwitchCurrentWeaponTypeU3Ed__814_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FA538ED4DAEA1AFCB781E2FE2B57F54E8A80305 (void);
// 0x00000191 System.Void EmeraldAI.EmeraldAISystem/<SwitchCurrentWeaponType>d__814::System.Collections.IEnumerator.Reset()
extern void U3CSwitchCurrentWeaponTypeU3Ed__814_System_Collections_IEnumerator_Reset_mA751413854C58655AF7400A81AD358BF4643663C (void);
// 0x00000192 System.Object EmeraldAI.EmeraldAISystem/<SwitchCurrentWeaponType>d__814::System.Collections.IEnumerator.get_Current()
extern void U3CSwitchCurrentWeaponTypeU3Ed__814_System_Collections_IEnumerator_get_Current_m17D5938563CC1519E066D82FE7B09AF1764AF673 (void);
// 0x00000193 System.Void EmeraldAI.EmeraldAISystem/<InitializeSummonedAI>d__827::.ctor(System.Int32)
extern void U3CInitializeSummonedAIU3Ed__827__ctor_mDB62F81516374233006055D2327D9C1766C8E351 (void);
// 0x00000194 System.Void EmeraldAI.EmeraldAISystem/<InitializeSummonedAI>d__827::System.IDisposable.Dispose()
extern void U3CInitializeSummonedAIU3Ed__827_System_IDisposable_Dispose_mA0B7889CAEFDD84A3739116AAE5E84294BABBFE4 (void);
// 0x00000195 System.Boolean EmeraldAI.EmeraldAISystem/<InitializeSummonedAI>d__827::MoveNext()
extern void U3CInitializeSummonedAIU3Ed__827_MoveNext_mCDE54FF5C901069E404C9B6BFEF7EB5C4ECCE2B3 (void);
// 0x00000196 System.Object EmeraldAI.EmeraldAISystem/<InitializeSummonedAI>d__827::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeSummonedAIU3Ed__827_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57CEE7975CE173FFCDDC16C1AB4575D6ED5D8769 (void);
// 0x00000197 System.Void EmeraldAI.EmeraldAISystem/<InitializeSummonedAI>d__827::System.Collections.IEnumerator.Reset()
extern void U3CInitializeSummonedAIU3Ed__827_System_Collections_IEnumerator_Reset_m1E63611C5F7DE140142E34E0B0506EE26735D5F4 (void);
// 0x00000198 System.Object EmeraldAI.EmeraldAISystem/<InitializeSummonedAI>d__827::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeSummonedAIU3Ed__827_System_Collections_IEnumerator_get_Current_m243D6382187CB5228D5CC8BAD88FE93ECF1FE787 (void);
// 0x00000199 System.Void EmeraldAI.Example.AISchedule::Start()
extern void AISchedule_Start_mA149953BD378744FB288500D17FAF7C93EAF7190 (void);
// 0x0000019A System.Void EmeraldAI.Example.AISchedule::UpdateAISchedule()
extern void AISchedule_UpdateAISchedule_m880E519C129C769550D016CA4E06331EED7FEF98 (void);
// 0x0000019B System.Void EmeraldAI.Example.AISchedule::.ctor()
extern void AISchedule__ctor_mF33B7CA399AF066314D0D335BAB9039A9ACF7033 (void);
// 0x0000019C System.Void EmeraldAI.Example.BloodSplatterManager::Start()
extern void BloodSplatterManager_Start_m1332EB40A6A0775D2E8F603CFC4CE15F37A0E99B (void);
// 0x0000019D System.Void EmeraldAI.Example.BloodSplatterManager::CreateBloodSplatter()
extern void BloodSplatterManager_CreateBloodSplatter_mB64BC21BA105D20F9EA8F5432DCB1B35ECB5009B (void);
// 0x0000019E System.Void EmeraldAI.Example.BloodSplatterManager::DelayCreateBloodSplatter()
extern void BloodSplatterManager_DelayCreateBloodSplatter_mF9911AFA23B83EAF5F5795C895C178B94E625469 (void);
// 0x0000019F System.Void EmeraldAI.Example.BloodSplatterManager::.ctor()
extern void BloodSplatterManager__ctor_mE352AB978A71F814000082E591D71A09534E92EF (void);
// 0x000001A0 System.Void EmeraldAI.Example.BloodSplatterManager::<Start>b__6_0()
extern void BloodSplatterManager_U3CStartU3Eb__6_0_mB00B8FB853AFD8CCAB48AAFF8FF0B7E8D458D5A4 (void);
// 0x000001A1 System.Void EmeraldAI.Example.DamageAIByCollision::OnCollisionEnter(UnityEngine.Collision)
extern void DamageAIByCollision_OnCollisionEnter_mC4E5FF771621A760C0C4C474F20F761569F71841 (void);
// 0x000001A2 System.Void EmeraldAI.Example.DamageAIByCollision::.ctor()
extern void DamageAIByCollision__ctor_m5B2BC311B6893F9AF3E9427FBB82A851708797D3 (void);
// 0x000001A3 System.Void EmeraldAI.Example.HUDHealthBar::Start()
extern void HUDHealthBar_Start_m461CB14EC9731E000398D76C93DC74E57AB4AB03 (void);
// 0x000001A4 System.Void EmeraldAI.Example.HUDHealthBar::FixedUpdate()
extern void HUDHealthBar_FixedUpdate_m0EB68D6B1C9966BC944C84B2B1DCCC5297E48C6B (void);
// 0x000001A5 System.Void EmeraldAI.Example.HUDHealthBar::.ctor()
extern void HUDHealthBar__ctor_m43F12653740E18603B1E3597F5EC0407EB447BC7 (void);
// 0x000001A6 System.Void EmeraldAI.Example.HUDHealthBarTopDown::Start()
extern void HUDHealthBarTopDown_Start_mAC2270FB8161D88390640F0A75E8693A29BC3E23 (void);
// 0x000001A7 System.Void EmeraldAI.Example.HUDHealthBarTopDown::FixedUpdate()
extern void HUDHealthBarTopDown_FixedUpdate_m8D2362D060980BC0E1435A4E53CDA48BE125D83F (void);
// 0x000001A8 System.Void EmeraldAI.Example.HUDHealthBarTopDown::.ctor()
extern void HUDHealthBarTopDown__ctor_mDE2458CF745352E970958BEDD88AA65D6810BE64 (void);
// 0x000001A9 System.Void EmeraldAI.Example.MoveToMousePosition::Start()
extern void MoveToMousePosition_Start_m3CE289ABF0FACA937420532B0A44E3963149DDAF (void);
// 0x000001AA System.Void EmeraldAI.Example.MoveToMousePosition::Update()
extern void MoveToMousePosition_Update_m38E30BCDA7DEB17048FC35FF539D04F0690EE109 (void);
// 0x000001AB System.Void EmeraldAI.Example.MoveToMousePosition::.ctor()
extern void MoveToMousePosition__ctor_m99F70414B0783BE302A974AA8194616A45D59C94 (void);
// 0x000001AC System.Void EmeraldAI.Example.NeedsSystemDisplay::Start()
extern void NeedsSystemDisplay_Start_m502AD38F4EE7D766E56DC961234EEC16EB471997 (void);
// 0x000001AD System.Void EmeraldAI.Example.NeedsSystemDisplay::UpdateTextDisplay()
extern void NeedsSystemDisplay_UpdateTextDisplay_m45625FBB001A2640F5C85ABCFD5CFE63CAB8F7B1 (void);
// 0x000001AE System.Void EmeraldAI.Example.NeedsSystemDisplay::.ctor()
extern void NeedsSystemDisplay__ctor_m539636C21CB4F3A9C857B7EF76921C8F2C125D98 (void);
// 0x000001AF System.Void EmeraldAI.Example.PlayAIsEmote::Start()
extern void PlayAIsEmote_Start_m5A9C64B7ACB55282385568521010AAD71628705E (void);
// 0x000001B0 System.Void EmeraldAI.Example.PlayAIsEmote::Update()
extern void PlayAIsEmote_Update_mF1479F71BDB2FB7509EE954265BAD64610961AD3 (void);
// 0x000001B1 System.Void EmeraldAI.Example.PlayAIsEmote::.ctor()
extern void PlayAIsEmote__ctor_mDFE8DA8180648C626D00CA168787C01872F40DA5 (void);
// 0x000001B2 System.Void EmeraldAI.Example.RaycastHitAI::Start()
extern void RaycastHitAI_Start_m5DAE8B6850E9B4E45D4C0EC406809B8E25D7D83C (void);
// 0x000001B3 System.Void EmeraldAI.Example.RaycastHitAI::Update()
extern void RaycastHitAI_Update_m53F51D9B89BF35766D3DC3378FD5771E792F66FE (void);
// 0x000001B4 System.Void EmeraldAI.Example.RaycastHitAI::.ctor()
extern void RaycastHitAI__ctor_m10337320A94B93FC43033FFEEFE5EA8FFE643C22 (void);
// 0x000001B5 System.Void EmeraldAI.Example.SpawnCompanion::Update()
extern void SpawnCompanion_Update_m73F5CC3D3426E758DAEEAF99026335FF54055B4C (void);
// 0x000001B6 System.Void EmeraldAI.Example.SpawnCompanion::RemoveAI()
extern void SpawnCompanion_RemoveAI_m6137362AEAA906838853C55F1A7F3C199D6F4EE1 (void);
// 0x000001B7 System.Void EmeraldAI.Example.SpawnCompanion::.ctor()
extern void SpawnCompanion__ctor_mE0678C1B4D782BE74B7CAA057DAF0247F74EF84F (void);
// 0x000001B8 System.Void EmeraldAI.Example.SpawnCompanion::<Update>b__3_0()
extern void SpawnCompanion_U3CUpdateU3Eb__3_0_m063284D43EBC9BB9401A9DE0A22728189B50D45F (void);
// 0x000001B9 System.Void EmeraldAI.Example.SpawnObjectEvent::SpawnObject()
extern void SpawnObjectEvent_SpawnObject_m80C488D295A241FC2ACDD5485F2BCAC018C480DE (void);
// 0x000001BA System.Void EmeraldAI.Example.SpawnObjectEvent::.ctor()
extern void SpawnObjectEvent__ctor_m1D4F7FC399DC161B2C66FE7AE41517CA449AFB63 (void);
// 0x000001BB System.Void EmeraldAI.Example.TameAI::Update()
extern void TameAI_Update_m3CB43810123B342D16692AEA5F6A8D000D856A64 (void);
// 0x000001BC System.Void EmeraldAI.Example.TameAI::.ctor()
extern void TameAI__ctor_m08D2962707CFC257C116325213032C10FD8A6D41 (void);
// 0x000001BD System.Void EmeraldAI.Example.UpdatePlayerHealthOrbUI::Start()
extern void UpdatePlayerHealthOrbUI_Start_m71F262F218BF63FF49F0FE6AAC74CD4ABF1F2E4C (void);
// 0x000001BE System.Void EmeraldAI.Example.UpdatePlayerHealthOrbUI::UpdateHealthUI()
extern void UpdatePlayerHealthOrbUI_UpdateHealthUI_mDA48AB2E01F9C989C5011B252C6DD7674C1CF797 (void);
// 0x000001BF System.Void EmeraldAI.Example.UpdatePlayerHealthOrbUI::.ctor()
extern void UpdatePlayerHealthOrbUI__ctor_m8D4CCA6FEF4952876490F5D74DDE6E4E5C1E3A25 (void);
// 0x000001C0 System.Void EmeraldAI.Example.UpdatePlayerHealthUI::Start()
extern void UpdatePlayerHealthUI_Start_m55E381E3B827EBC56FD9F21D2D00F2E8F862ACA2 (void);
// 0x000001C1 System.Void EmeraldAI.Example.UpdatePlayerHealthUI::UpdateHealthUI()
extern void UpdatePlayerHealthUI_UpdateHealthUI_m683DD5CAF23AF187C5741F55C47EE9033FD3D460 (void);
// 0x000001C2 System.Void EmeraldAI.Example.UpdatePlayerHealthUI::.ctor()
extern void UpdatePlayerHealthUI__ctor_mF7CE0757FCD893C564EAF5697124A450D4239103 (void);
// 0x000001C3 System.Void EmeraldAI.Example.EmeraldAIPlayerHealth::Start()
extern void EmeraldAIPlayerHealth_Start_mADD10B14D4CDC23C3662EE2A8FBD0A296028B42E (void);
// 0x000001C4 System.Void EmeraldAI.Example.EmeraldAIPlayerHealth::Update()
extern void EmeraldAIPlayerHealth_Update_mFAF2A7517BCE1D9000D40812F4CB496D800B3E61 (void);
// 0x000001C5 System.Void EmeraldAI.Example.EmeraldAIPlayerHealth::DamagePlayer(System.Int32)
extern void EmeraldAIPlayerHealth_DamagePlayer_mCB554B3C94D15543B530960B059448F25B28CEED (void);
// 0x000001C6 System.Void EmeraldAI.Example.EmeraldAIPlayerHealth::HealPlayer(System.Int32)
extern void EmeraldAIPlayerHealth_HealPlayer_m1FFF385050E437281BB1DE1C1F23C5C3B4DB9FE0 (void);
// 0x000001C7 System.Void EmeraldAI.Example.EmeraldAIPlayerHealth::ResetPlayer()
extern void EmeraldAIPlayerHealth_ResetPlayer_mEF6DA44E8B82A4B5A5B40184416C9CC0FB6E3D94 (void);
// 0x000001C8 System.Void EmeraldAI.Example.EmeraldAIPlayerHealth::PlayerDeath()
extern void EmeraldAIPlayerHealth_PlayerDeath_m77D92DBAE0CF2513209EEE34F3B256F944F66657 (void);
// 0x000001C9 System.Void EmeraldAI.Example.EmeraldAIPlayerHealth::.ctor()
extern void EmeraldAIPlayerHealth__ctor_mA04B1422593D5B5B60DD4EA7C0EB9098E6D1863F (void);
// 0x000001CA System.Void EmeraldAI.Utility.AnimationProfile::.ctor()
extern void AnimationProfile__ctor_mDEE65B2258B5C14133F201E3E7A6D8B6DBA14592 (void);
// 0x000001CB System.Void EmeraldAI.Utility.AnimationProfile/EmoteAnimationClass::.ctor(System.Int32,UnityEngine.AnimationClip)
extern void EmoteAnimationClass__ctor_mC4B79C25A13E9F4882655BE35762CCE97C7B361F (void);
// 0x000001CC System.Void EmeraldAI.Utility.AnimationProfile/AnimationClass::.ctor(System.Single,UnityEngine.AnimationClip)
extern void AnimationClass__ctor_mBBDBFF565C0BE751837A9AECA58E7935013949F0 (void);
// 0x000001CD System.Void EmeraldAI.Utility.EmeraldAICombatManager::GenerateMeleeAttack(EmeraldAI.EmeraldAISystem)
extern void EmeraldAICombatManager_GenerateMeleeAttack_m2131824A860204CA187851525A92EAAA32255720 (void);
// 0x000001CE System.Void EmeraldAI.Utility.EmeraldAICombatManager::GenerateMeleeRunAttack(EmeraldAI.EmeraldAISystem)
extern void EmeraldAICombatManager_GenerateMeleeRunAttack_m87D7AEDC6F91DB141EC9B0149A51C3D5093333E7 (void);
// 0x000001CF System.Void EmeraldAI.Utility.EmeraldAICombatManager::GenerateOffensiveAbility(EmeraldAI.EmeraldAISystem)
extern void EmeraldAICombatManager_GenerateOffensiveAbility_m6B96C56ACF9B0CBA08026564E3A194F1D11533F4 (void);
// 0x000001D0 System.Void EmeraldAI.Utility.EmeraldAICombatManager::GenerateSupportAbility(EmeraldAI.EmeraldAISystem)
extern void EmeraldAICombatManager_GenerateSupportAbility_m0149028E9F2954FBCB23B35EA478206F8DED82EF (void);
// 0x000001D1 System.Void EmeraldAI.Utility.EmeraldAICombatManager::GenerateSummoningAbility(EmeraldAI.EmeraldAISystem)
extern void EmeraldAICombatManager_GenerateSummoningAbility_mF1B52F48362564B24A707F3FE4978A9C5694320D (void);
// 0x000001D2 System.Single EmeraldAI.Utility.EmeraldAICombatManager::GenerateProbability(System.Single[])
extern void EmeraldAICombatManager_GenerateProbability_m9212DC8E8CE726D88324125611E584E3CEEB4FED (void);
// 0x000001D3 System.Void EmeraldAI.Utility.EmeraldAIDamageOverTime::Start()
extern void EmeraldAIDamageOverTime_Start_m97CB1873981E6B5890FAD923E48E291FF042D3F1 (void);
// 0x000001D4 System.Void EmeraldAI.Utility.EmeraldAIDamageOverTime::Initialize(System.String,System.Int32,System.Single,System.Single,UnityEngine.GameObject,System.Single,UnityEngine.AudioClip,EmeraldAI.EmeraldAINonAIDamage,EmeraldAI.EmeraldAIPlayerDamage,EmeraldAI.EmeraldAISystem,EmeraldAI.EmeraldAISystem,UnityEngine.Transform,EmeraldAI.EmeraldAISystem/TargetType)
extern void EmeraldAIDamageOverTime_Initialize_m479893F5BB8190FF17E80A31A0BAF9547CDC158B (void);
// 0x000001D5 System.Void EmeraldAI.Utility.EmeraldAIDamageOverTime::OnEnable()
extern void EmeraldAIDamageOverTime_OnEnable_mD24F5CBFA0FF1DED40CC3D239F596413461C079C (void);
// 0x000001D6 System.Void EmeraldAI.Utility.EmeraldAIDamageOverTime::Update()
extern void EmeraldAIDamageOverTime_Update_m602C5D4C17A8C549E85506BB520B8DCEE86A2164 (void);
// 0x000001D7 System.Void EmeraldAI.Utility.EmeraldAIDamageOverTime::.ctor()
extern void EmeraldAIDamageOverTime__ctor_mBE333A71ACAF03835E38B9E3E88E815ED53955AD (void);
// 0x000001D8 System.Void EmeraldAI.Utility.EmeraldAIDetection::Start()
extern void EmeraldAIDetection_Start_m0B70E2AC3C7A92BEBDF4145BE8EF7EC5DD8526FF (void);
// 0x000001D9 System.Void EmeraldAI.Utility.EmeraldAIDetection::Update()
extern void EmeraldAIDetection_Update_mDFD4D60BFBB1FA164603AA9ED1FFA188A390E6AF (void);
// 0x000001DA System.Void EmeraldAI.Utility.EmeraldAIDetection::FixedUpdate()
extern void EmeraldAIDetection_FixedUpdate_mD8875B1C1925BC8131EC9869A72AFB84950EEA2E (void);
// 0x000001DB System.Void EmeraldAI.Utility.EmeraldAIDetection::DetectTarget(UnityEngine.GameObject)
extern void EmeraldAIDetection_DetectTarget_m205A232FDC39864B962F2DEF5D476F20C82620DB (void);
// 0x000001DC System.Void EmeraldAI.Utility.EmeraldAIDetection::AIAvoidance()
extern void EmeraldAIDetection_AIAvoidance_m15273A4A6DA3F748C869DA2BA56D23D9AB432A39 (void);
// 0x000001DD System.Void EmeraldAI.Utility.EmeraldAIDetection::UpdateAIUI()
extern void EmeraldAIDetection_UpdateAIUI_mFDD04E833CCAE2494483D0566F64E5DAF1A566ED (void);
// 0x000001DE System.Void EmeraldAI.Utility.EmeraldAIDetection::UpdateAIDetection()
extern void EmeraldAIDetection_UpdateAIDetection_m5B2AEA29C55E22CB5396C3CE20F7F9591FA048B0 (void);
// 0x000001DF System.Void EmeraldAI.Utility.EmeraldAIDetection::LineOfSightDetection()
extern void EmeraldAIDetection_LineOfSightDetection_m9A44C352F0397A0CD6BC8A51F731C02476500224 (void);
// 0x000001E0 System.Void EmeraldAI.Utility.EmeraldAIDetection::SetLineOfSightTarget(UnityEngine.Transform)
extern void EmeraldAIDetection_SetLineOfSightTarget_m8973F95A89D522B2291B5963D050E95EAF115281 (void);
// 0x000001E1 System.Void EmeraldAI.Utility.EmeraldAIDetection::CheckForObstructions()
extern void EmeraldAIDetection_CheckForObstructions_m73802B5F48C752E771AF2E7FF5D52D4B8EB53D06 (void);
// 0x000001E2 System.Void EmeraldAI.Utility.EmeraldAIDetection::SearchForTarget()
extern void EmeraldAIDetection_SearchForTarget_mBA9CF85E510519243E1661E2362CE53145A7A675 (void);
// 0x000001E3 System.Void EmeraldAI.Utility.EmeraldAIDetection::SetDetectedTarget(UnityEngine.Transform)
extern void EmeraldAIDetection_SetDetectedTarget_mEED5CB3A3A5769248F2F2356B6B91539F0A56757 (void);
// 0x000001E4 System.Void EmeraldAI.Utility.EmeraldAIDetection::DetectTargetType(UnityEngine.Transform,System.Nullable`1<System.Boolean>)
extern void EmeraldAIDetection_DetectTargetType_m0973A3D11A3B0ECB09ADB43C9919B13E861821F8 (void);
// 0x000001E5 System.Void EmeraldAI.Utility.EmeraldAIDetection::StartRandomTarget()
extern void EmeraldAIDetection_StartRandomTarget_m081BC53F0ABD7CE3BEFB822B5CDACED451F27A1B (void);
// 0x000001E6 System.Void EmeraldAI.Utility.EmeraldAIDetection::GetTargetFaction(UnityEngine.Transform)
extern void EmeraldAIDetection_GetTargetFaction_m1767C2E90C36503953F404B1EDA79FDFB209FF31 (void);
// 0x000001E7 System.Void EmeraldAI.Utility.EmeraldAIDetection::GetTargetPositionModifier(UnityEngine.Transform)
extern void EmeraldAIDetection_GetTargetPositionModifier_mBF6D21537B2C1B28E77D102879E26532B2B9AA97 (void);
// 0x000001E8 System.Void EmeraldAI.Utility.EmeraldAIDetection::.ctor()
extern void EmeraldAIDetection__ctor_m1660AEBA5CEBAAD0036A5FBDFC4826B4BEF9BF9F (void);
// 0x000001E9 System.Void EmeraldAI.Utility.EmeraldAIHandIK::Start()
extern void EmeraldAIHandIK_Start_m5267EA51AA023BF51C27A94325314F837AF1CA74 (void);
// 0x000001EA System.Void EmeraldAI.Utility.EmeraldAIHandIK::OnDrawGizmos()
extern void EmeraldAIHandIK_OnDrawGizmos_mBD04BBAF653A6605DE2CE175618C58E70EE42128 (void);
// 0x000001EB System.Void EmeraldAI.Utility.EmeraldAIHandIK::InstantlyFadeInWeights()
extern void EmeraldAIHandIK_InstantlyFadeInWeights_m0A9EB26C2C28FE38A8E0DD4D332A4CED47690150 (void);
// 0x000001EC System.Void EmeraldAI.Utility.EmeraldAIHandIK::FadeInHandWeights()
extern void EmeraldAIHandIK_FadeInHandWeights_m1F83B8BDA6D801B599B3A82A63252BBFCC104CD2 (void);
// 0x000001ED System.Collections.IEnumerator EmeraldAI.Utility.EmeraldAIHandIK::FadeInHandWeightsInternal()
extern void EmeraldAIHandIK_FadeInHandWeightsInternal_m7A5AF1D5A2BBCA241EF7E45E34C6BDA691893CB7 (void);
// 0x000001EE System.Void EmeraldAI.Utility.EmeraldAIHandIK::FadeOutHandWeights()
extern void EmeraldAIHandIK_FadeOutHandWeights_mCFD9B9C43A53F6D2505F7F9CB155154C446F79CB (void);
// 0x000001EF System.Collections.IEnumerator EmeraldAI.Utility.EmeraldAIHandIK::FadeOutHandWeightsInternal()
extern void EmeraldAIHandIK_FadeOutHandWeightsInternal_m74A3842B06B72350B7B5C0F9B208E6497F797C8D (void);
// 0x000001F0 System.Void EmeraldAI.Utility.EmeraldAIHandIK::OnAnimatorIK(System.Int32)
extern void EmeraldAIHandIK_OnAnimatorIK_m97BC7049D567C9298A6C0AC1FA880ACDAEFBA026 (void);
// 0x000001F1 System.Void EmeraldAI.Utility.EmeraldAIHandIK::.ctor()
extern void EmeraldAIHandIK__ctor_mFF382942E5E25DF98A4ADA6FB844249742B8516E (void);
// 0x000001F2 System.Void EmeraldAI.Utility.EmeraldAIHandIK/<FadeInHandWeightsInternal>d__14::.ctor(System.Int32)
extern void U3CFadeInHandWeightsInternalU3Ed__14__ctor_mAE88A729F149B283C61C6DE22446A4BE4F719040 (void);
// 0x000001F3 System.Void EmeraldAI.Utility.EmeraldAIHandIK/<FadeInHandWeightsInternal>d__14::System.IDisposable.Dispose()
extern void U3CFadeInHandWeightsInternalU3Ed__14_System_IDisposable_Dispose_m9E3A977CEEF76C8D48B070E1D4F13606833AA263 (void);
// 0x000001F4 System.Boolean EmeraldAI.Utility.EmeraldAIHandIK/<FadeInHandWeightsInternal>d__14::MoveNext()
extern void U3CFadeInHandWeightsInternalU3Ed__14_MoveNext_mE51620361ABBEB680EB780309E9DEF3518D40701 (void);
// 0x000001F5 System.Object EmeraldAI.Utility.EmeraldAIHandIK/<FadeInHandWeightsInternal>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeInHandWeightsInternalU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78BD2815DF875FB11125488CC9B332D4C30CDED9 (void);
// 0x000001F6 System.Void EmeraldAI.Utility.EmeraldAIHandIK/<FadeInHandWeightsInternal>d__14::System.Collections.IEnumerator.Reset()
extern void U3CFadeInHandWeightsInternalU3Ed__14_System_Collections_IEnumerator_Reset_m91F6F5E0C6DF95D4B8C94F3D93A251630A1DEAAD (void);
// 0x000001F7 System.Object EmeraldAI.Utility.EmeraldAIHandIK/<FadeInHandWeightsInternal>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CFadeInHandWeightsInternalU3Ed__14_System_Collections_IEnumerator_get_Current_m1636832462E8490262CEDE81A697671DA6B899E9 (void);
// 0x000001F8 System.Void EmeraldAI.Utility.EmeraldAIHandIK/<FadeOutHandWeightsInternal>d__16::.ctor(System.Int32)
extern void U3CFadeOutHandWeightsInternalU3Ed__16__ctor_mB5F6CBF6D36BF6F3E74A0F1438253951B2179374 (void);
// 0x000001F9 System.Void EmeraldAI.Utility.EmeraldAIHandIK/<FadeOutHandWeightsInternal>d__16::System.IDisposable.Dispose()
extern void U3CFadeOutHandWeightsInternalU3Ed__16_System_IDisposable_Dispose_m605826D9DD1548895B738577068FDCC9AC4B5EF8 (void);
// 0x000001FA System.Boolean EmeraldAI.Utility.EmeraldAIHandIK/<FadeOutHandWeightsInternal>d__16::MoveNext()
extern void U3CFadeOutHandWeightsInternalU3Ed__16_MoveNext_m8CC08E3B61E44A788849FE58A4D9E55035101E9F (void);
// 0x000001FB System.Object EmeraldAI.Utility.EmeraldAIHandIK/<FadeOutHandWeightsInternal>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutHandWeightsInternalU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F163CE5139D1E7E64A1017FD003ED4D151BD8CA (void);
// 0x000001FC System.Void EmeraldAI.Utility.EmeraldAIHandIK/<FadeOutHandWeightsInternal>d__16::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutHandWeightsInternalU3Ed__16_System_Collections_IEnumerator_Reset_m656C7506885341887E64696E50ED405AC8EE657A (void);
// 0x000001FD System.Object EmeraldAI.Utility.EmeraldAIHandIK/<FadeOutHandWeightsInternal>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutHandWeightsInternalU3Ed__16_System_Collections_IEnumerator_get_Current_mA2BED98C71D68703D224C5BA0E54A586ACA57D85 (void);
// 0x000001FE System.Void EmeraldAI.Utility.EmeraldAIInitializer::Initialize()
extern void EmeraldAIInitializer_Initialize_m23B501440489AD2196BC13409CAE9D2C80E8CB2D (void);
// 0x000001FF System.Void EmeraldAI.Utility.EmeraldAIInitializer::Start()
extern void EmeraldAIInitializer_Start_m4BEE8D8330B227B23C68CA9C372CE5ECDBCE711D (void);
// 0x00000200 System.Void EmeraldAI.Utility.EmeraldAIInitializer::CheckFactionRelations()
extern void EmeraldAIInitializer_CheckFactionRelations_mF77FA3D93644C5791CFB8A6C4244EDFBB299270B (void);
// 0x00000201 System.Void EmeraldAI.Utility.EmeraldAIInitializer::OnEnable()
extern void EmeraldAIInitializer_OnEnable_m823C469A760687C3976A67874909D25C15FDB93F (void);
// 0x00000202 System.Void EmeraldAI.Utility.EmeraldAIInitializer::AlignOnStart()
extern void EmeraldAIInitializer_AlignOnStart_m49CFA22A5F034AB64AECBD542BAF3DE1A23119CC (void);
// 0x00000203 System.Void EmeraldAI.Utility.EmeraldAIInitializer::IntializeLookAtController()
extern void EmeraldAIInitializer_IntializeLookAtController_mC16ABC247748846C2E00D4C360D677E89EFACE80 (void);
// 0x00000204 System.Void EmeraldAI.Utility.EmeraldAIInitializer::DisableRagdoll()
extern void EmeraldAIInitializer_DisableRagdoll_m1C3D832C82D87B439FE1F47FD10453180255953B (void);
// 0x00000205 System.Void EmeraldAI.Utility.EmeraldAIInitializer::EnableRagdoll()
extern void EmeraldAIInitializer_EnableRagdoll_m8538E505C379FB63C0239A5711E64D12454873CC (void);
// 0x00000206 System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupEmeraldAIObjectPool()
extern void EmeraldAIInitializer_SetupEmeraldAIObjectPool_m2757B58199DBD1520E8F4D73C6C3223D12C18282 (void);
// 0x00000207 System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupAudio()
extern void EmeraldAIInitializer_SetupAudio_m4069AA45C16E2D2FCF809299618F0D3E5F637428 (void);
// 0x00000208 System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupOptimizationSettings()
extern void EmeraldAIInitializer_SetupOptimizationSettings_mE034912071A81D179FA25B8E2C82F924289E6761 (void);
// 0x00000209 System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupFactions()
extern void EmeraldAIInitializer_SetupFactions_m21DF4C92FD5648A92B4863FE20471A456B272D07 (void);
// 0x0000020A System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupAdditionalComponents()
extern void EmeraldAIInitializer_SetupAdditionalComponents_mA983E9A6C8EC07B0EF53DB2DF880F9025C7E6FA7 (void);
// 0x0000020B System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupNavMeshAgent()
extern void EmeraldAIInitializer_SetupNavMeshAgent_m7638E20841D89D94C2AC13472A81F0D7A2A0A3B0 (void);
// 0x0000020C System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupAnimator()
extern void EmeraldAIInitializer_SetupAnimator_m006B0F85530D496B6057D3AE7C06209527C77820 (void);
// 0x0000020D System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupHealthBar()
extern void EmeraldAIInitializer_SetupHealthBar_m80952BB09E142B45707D0A4D2A55D5BA0D27E298 (void);
// 0x0000020E System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupCombatText()
extern void EmeraldAIInitializer_SetupCombatText_mE2D0B36F5E53CF750625A2FAA30FC26997C221F7 (void);
// 0x0000020F System.Void EmeraldAI.Utility.EmeraldAIInitializer::SetupEmeraldAISettings()
extern void EmeraldAIInitializer_SetupEmeraldAISettings_mD45DAEF20520F0884B56AE0BB5EF7AD05B8D850F (void);
// 0x00000210 System.Void EmeraldAI.Utility.EmeraldAIInitializer::CheckAnimationEvents()
extern void EmeraldAIInitializer_CheckAnimationEvents_m24314877516694B59521D5296A36C8C6880A527E (void);
// 0x00000211 System.Void EmeraldAI.Utility.EmeraldAIInitializer::CheckForMissingAnimations()
extern void EmeraldAIInitializer_CheckForMissingAnimations_m0AFAC4373FADFBE5763C5C86F9799D5CCED42BAB (void);
// 0x00000212 System.Void EmeraldAI.Utility.EmeraldAIInitializer::InitializeWeaponTypeAnimationAndSettings()
extern void EmeraldAIInitializer_InitializeWeaponTypeAnimationAndSettings_mD7432EA3C91D5E813F3F51CE344ADA82AC32914F (void);
// 0x00000213 System.Void EmeraldAI.Utility.EmeraldAIInitializer::IniializeHandIK()
extern void EmeraldAIInitializer_IniializeHandIK_m1F3B897364BDB0B88FEC91BC15F7FFB106E57400 (void);
// 0x00000214 System.Void EmeraldAI.Utility.EmeraldAIInitializer::InitializeDroppableWeapon()
extern void EmeraldAIInitializer_InitializeDroppableWeapon_m7B0DFE6E9B3844EAF58DEBE43DA40FCE478CE1E9 (void);
// 0x00000215 System.Void EmeraldAI.Utility.EmeraldAIInitializer::InitializeAIDeath()
extern void EmeraldAIInitializer_InitializeAIDeath_mC39B43212A6B87C9A12DB1CB558CB538A5C128F3 (void);
// 0x00000216 System.Void EmeraldAI.Utility.EmeraldAIInitializer::Update()
extern void EmeraldAIInitializer_Update_m03A7B9B5B6E62331DC8633A4233F369FFBDA067C (void);
// 0x00000217 System.Void EmeraldAI.Utility.EmeraldAIInitializer::RagdollDeath()
extern void EmeraldAIInitializer_RagdollDeath_m2ADFD701F62BDB8A8B69CE256F77BAB31F4C43AF (void);
// 0x00000218 System.Collections.IEnumerator EmeraldAI.Utility.EmeraldAIInitializer::AnimationDeath()
extern void EmeraldAIInitializer_AnimationDeath_mC892B63C345DAED627EE7E091DD336AEF38E7467 (void);
// 0x00000219 System.Void EmeraldAI.Utility.EmeraldAIInitializer::.ctor()
extern void EmeraldAIInitializer__ctor_m15C4B84A22899125856698518B030965BC73EDDC (void);
// 0x0000021A System.Void EmeraldAI.Utility.EmeraldAIInitializer/<AnimationDeath>d__28::.ctor(System.Int32)
extern void U3CAnimationDeathU3Ed__28__ctor_m98C8ACB372B8DD77893A1E3109B15BE60AD42927 (void);
// 0x0000021B System.Void EmeraldAI.Utility.EmeraldAIInitializer/<AnimationDeath>d__28::System.IDisposable.Dispose()
extern void U3CAnimationDeathU3Ed__28_System_IDisposable_Dispose_mBF2CE1D597149BB8992C20F80F6E44F69FB7F41C (void);
// 0x0000021C System.Boolean EmeraldAI.Utility.EmeraldAIInitializer/<AnimationDeath>d__28::MoveNext()
extern void U3CAnimationDeathU3Ed__28_MoveNext_mFB517346497CB8AEE5E4F633E9A9F2DD522EB9CE (void);
// 0x0000021D System.Object EmeraldAI.Utility.EmeraldAIInitializer/<AnimationDeath>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimationDeathU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B81F7CEB128D88A50C0A0E33BCB1A7C2790FF0A (void);
// 0x0000021E System.Void EmeraldAI.Utility.EmeraldAIInitializer/<AnimationDeath>d__28::System.Collections.IEnumerator.Reset()
extern void U3CAnimationDeathU3Ed__28_System_Collections_IEnumerator_Reset_m9AA4B65FE39E42F947F2FB003F55F7114085A1E6 (void);
// 0x0000021F System.Object EmeraldAI.Utility.EmeraldAIInitializer/<AnimationDeath>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CAnimationDeathU3Ed__28_System_Collections_IEnumerator_get_Current_mD7ABD5C2B1EE8E19B8557582F4742656AFC57D3D (void);
// 0x00000220 System.Void EmeraldAI.Utility.EmeraldAILookAtController::Initialize()
extern void EmeraldAILookAtController_Initialize_m849E9A9C23BDBE1840EC7103C4BCD71FD96BBC67 (void);
// 0x00000221 System.Void EmeraldAI.Utility.EmeraldAILookAtController::ResetSettings()
extern void EmeraldAILookAtController_ResetSettings_m9810EB8015E317D617098DAD7F26A8422EAD59A6 (void);
// 0x00000222 System.Void EmeraldAI.Utility.EmeraldAILookAtController::CalculateDistances()
extern void EmeraldAILookAtController_CalculateDistances_m5711271F63A2F8E9024B199F97B605C514AAEA0C (void);
// 0x00000223 System.Void EmeraldAI.Utility.EmeraldAILookAtController::OnDrawGizmos()
extern void EmeraldAILookAtController_OnDrawGizmos_m5551353CEADDF8422A004E8E08FA99E66CA7531D (void);
// 0x00000224 System.Single EmeraldAI.Utility.EmeraldAILookAtController::LookAtDirectionAngle()
extern void EmeraldAILookAtController_LookAtDirectionAngle_mDD1C577E70D9778B0954B4F886B93D2E30D4C3BB (void);
// 0x00000225 System.Single EmeraldAI.Utility.EmeraldAILookAtController::UnityIKAngle()
extern void EmeraldAILookAtController_UnityIKAngle_m3D7F807C948D8341534AB2475C8E33D871EC8262 (void);
// 0x00000226 System.Void EmeraldAI.Utility.EmeraldAILookAtController::OnAnimatorIK(System.Int32)
extern void EmeraldAILookAtController_OnAnimatorIK_mD9A6CE9C4E342543B8E62A9E00D563D91DF39311 (void);
// 0x00000227 System.Collections.IEnumerator EmeraldAI.Utility.EmeraldAILookAtController::FadeLookWeightInternal()
extern void EmeraldAILookAtController_FadeLookWeightInternal_m6209EB7582B76950A081CCB69FB71FDDA9348B8A (void);
// 0x00000228 System.Void EmeraldAI.Utility.EmeraldAILookAtController::FadeLookWeight()
extern void EmeraldAILookAtController_FadeLookWeight_m12D805BACC9DAB7D5B6724661BEF380C13D11DB4 (void);
// 0x00000229 System.Void EmeraldAI.Utility.EmeraldAILookAtController::FadeInBodyIK()
extern void EmeraldAILookAtController_FadeInBodyIK_m1E5053BA8EBDBD6F2786BCA542ED5E33B2600C41 (void);
// 0x0000022A System.Void EmeraldAI.Utility.EmeraldAILookAtController::FadeOutBodyIK()
extern void EmeraldAILookAtController_FadeOutBodyIK_m6BE3D633AC3BF5FB70BCBBDCB97B4EAB6CF877C8 (void);
// 0x0000022B System.Void EmeraldAI.Utility.EmeraldAILookAtController::LateUpdate()
extern void EmeraldAILookAtController_LateUpdate_m2F1FC102AD73B807B81B5FAC14E0C9B95EB899E7 (void);
// 0x0000022C System.Collections.IEnumerator EmeraldAI.Utility.EmeraldAILookAtController::FadeInBodyIKInternal()
extern void EmeraldAILookAtController_FadeInBodyIKInternal_mEF97E6C7E22B1FF85A385D8DDC693C64DAF264F7 (void);
// 0x0000022D System.Collections.IEnumerator EmeraldAI.Utility.EmeraldAILookAtController::FadeOutBodyIKInternal()
extern void EmeraldAILookAtController_FadeOutBodyIKInternal_m96A83BD97E45B12A8B99B91061D2CA8BAB968B4C (void);
// 0x0000022E System.Void EmeraldAI.Utility.EmeraldAILookAtController::LookAtTarget(UnityEngine.Vector3,UnityEngine.Transform,System.Single)
extern void EmeraldAILookAtController_LookAtTarget_m6C04068C2CFA266191A478E974CD1AFF3B06A8AC (void);
// 0x0000022F System.Void EmeraldAI.Utility.EmeraldAILookAtController::SetPreviousLookAtInfo()
extern void EmeraldAILookAtController_SetPreviousLookAtInfo_m74C27F5EEC9A002548134204E55D451BB3A00BEC (void);
// 0x00000230 System.Void EmeraldAI.Utility.EmeraldAILookAtController::.ctor()
extern void EmeraldAILookAtController__ctor_m6B396A4AC0CD4CEFC5E4D52A7D8E7F0C3C93D58E (void);
// 0x00000231 System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeLookWeightInternal>d__37::.ctor(System.Int32)
extern void U3CFadeLookWeightInternalU3Ed__37__ctor_m9316952730CEED787A2E2BF8837BC1824BBAD2A1 (void);
// 0x00000232 System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeLookWeightInternal>d__37::System.IDisposable.Dispose()
extern void U3CFadeLookWeightInternalU3Ed__37_System_IDisposable_Dispose_mAC5C657408E5BDA0CBCC2AB6E8AEE9279C31D715 (void);
// 0x00000233 System.Boolean EmeraldAI.Utility.EmeraldAILookAtController/<FadeLookWeightInternal>d__37::MoveNext()
extern void U3CFadeLookWeightInternalU3Ed__37_MoveNext_mCFBF22903E0BAD2F4347C3781CE616A4A7037A64 (void);
// 0x00000234 System.Object EmeraldAI.Utility.EmeraldAILookAtController/<FadeLookWeightInternal>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeLookWeightInternalU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m017B1AA4B4FEE165E432A9EA7210B02FAAD8D7F8 (void);
// 0x00000235 System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeLookWeightInternal>d__37::System.Collections.IEnumerator.Reset()
extern void U3CFadeLookWeightInternalU3Ed__37_System_Collections_IEnumerator_Reset_mED04AE8C60F4F7B1AB48938083704EFA9BD7F2B6 (void);
// 0x00000236 System.Object EmeraldAI.Utility.EmeraldAILookAtController/<FadeLookWeightInternal>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CFadeLookWeightInternalU3Ed__37_System_Collections_IEnumerator_get_Current_mB6BD28C668E28C9FDDC98304DF032C0FBA253AF8 (void);
// 0x00000237 System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeInBodyIKInternal>d__42::.ctor(System.Int32)
extern void U3CFadeInBodyIKInternalU3Ed__42__ctor_m03838ED0C1B393F8B9151DFE20FB63DD71DC1DFA (void);
// 0x00000238 System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeInBodyIKInternal>d__42::System.IDisposable.Dispose()
extern void U3CFadeInBodyIKInternalU3Ed__42_System_IDisposable_Dispose_mFE7497423ADE5E90DE7A065A5C188956DBA19788 (void);
// 0x00000239 System.Boolean EmeraldAI.Utility.EmeraldAILookAtController/<FadeInBodyIKInternal>d__42::MoveNext()
extern void U3CFadeInBodyIKInternalU3Ed__42_MoveNext_m7E7957B0F70495F9802E0C894C29F5E94E7446D9 (void);
// 0x0000023A System.Object EmeraldAI.Utility.EmeraldAILookAtController/<FadeInBodyIKInternal>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeInBodyIKInternalU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC203A9141392AC0EBE3808D0CC010AFE4269228A (void);
// 0x0000023B System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeInBodyIKInternal>d__42::System.Collections.IEnumerator.Reset()
extern void U3CFadeInBodyIKInternalU3Ed__42_System_Collections_IEnumerator_Reset_mE0AEDF727A0B3E44D7C3B5E6AD9A5BEF39BFBF4F (void);
// 0x0000023C System.Object EmeraldAI.Utility.EmeraldAILookAtController/<FadeInBodyIKInternal>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CFadeInBodyIKInternalU3Ed__42_System_Collections_IEnumerator_get_Current_mBF9903AD75A042EC01EE79F97D8600F1B21FA355 (void);
// 0x0000023D System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeOutBodyIKInternal>d__43::.ctor(System.Int32)
extern void U3CFadeOutBodyIKInternalU3Ed__43__ctor_mD5AC451D67E5514DE006705439032ED511E3300C (void);
// 0x0000023E System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeOutBodyIKInternal>d__43::System.IDisposable.Dispose()
extern void U3CFadeOutBodyIKInternalU3Ed__43_System_IDisposable_Dispose_mD06050A80C059F2D098ECFF6597547122AC0CC37 (void);
// 0x0000023F System.Boolean EmeraldAI.Utility.EmeraldAILookAtController/<FadeOutBodyIKInternal>d__43::MoveNext()
extern void U3CFadeOutBodyIKInternalU3Ed__43_MoveNext_m957D7A7140614A6E4BCFDE5FA1ABE3C515846C07 (void);
// 0x00000240 System.Object EmeraldAI.Utility.EmeraldAILookAtController/<FadeOutBodyIKInternal>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutBodyIKInternalU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6F021F70E667BFE3A62E4D90E6254FD24DB2201 (void);
// 0x00000241 System.Void EmeraldAI.Utility.EmeraldAILookAtController/<FadeOutBodyIKInternal>d__43::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutBodyIKInternalU3Ed__43_System_Collections_IEnumerator_Reset_m4C2130811612412D90C8D0579F9F0EC19DA940B5 (void);
// 0x00000242 System.Object EmeraldAI.Utility.EmeraldAILookAtController/<FadeOutBodyIKInternal>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutBodyIKInternalU3Ed__43_System_Collections_IEnumerator_get_Current_m01FE9D1887D16B1E70E78A162332B46F4CD3F968 (void);
// 0x00000243 System.Void EmeraldAI.Utility.EmeraldAIObjectPool::Init(UnityEngine.GameObject,System.Int32)
extern void EmeraldAIObjectPool_Init_m331E2EEC5E8B14ABF3305E12BEF55E5F889632DA (void);
// 0x00000244 System.Void EmeraldAI.Utility.EmeraldAIObjectPool::Preload(UnityEngine.GameObject,System.Int32)
extern void EmeraldAIObjectPool_Preload_m6ACC7E4F8B713BE0FE98BBE386D0B53B82188DC0 (void);
// 0x00000245 UnityEngine.GameObject EmeraldAI.Utility.EmeraldAIObjectPool::Spawn(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void EmeraldAIObjectPool_Spawn_m521DF838B00EE821A161311689AC4AFA7188FDD2 (void);
// 0x00000246 UnityEngine.GameObject EmeraldAI.Utility.EmeraldAIObjectPool::SpawnEffect(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single)
extern void EmeraldAIObjectPool_SpawnEffect_mDF3F7B87B95F6375481F08983C4C2488F37C1979 (void);
// 0x00000247 System.Void EmeraldAI.Utility.EmeraldAIObjectPool::Despawn(UnityEngine.GameObject)
extern void EmeraldAIObjectPool_Despawn_m8801CF2179E3D123AE022EB6D1E8251D53DB86BD (void);
// 0x00000248 System.Void EmeraldAI.Utility.EmeraldAIObjectPool/Pool::.ctor(UnityEngine.GameObject,System.Int32)
extern void Pool__ctor_mA26737048C9787C8684DC871C7859F55616E770E (void);
// 0x00000249 UnityEngine.GameObject EmeraldAI.Utility.EmeraldAIObjectPool/Pool::Spawn(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Pool_Spawn_m4BB445962F7E1123C8D0C60DEB2F78B0642E2CD6 (void);
// 0x0000024A UnityEngine.GameObject EmeraldAI.Utility.EmeraldAIObjectPool/Pool::SpawnEffect(UnityEngine.Vector3,UnityEngine.Quaternion,System.Single)
extern void Pool_SpawnEffect_m17F8514E1C282BE7CD0234F4E8491C4183109E71 (void);
// 0x0000024B System.Void EmeraldAI.Utility.EmeraldAIObjectPool/Pool::Despawn(UnityEngine.GameObject)
extern void Pool_Despawn_m91FAB15F5A6674AD5C0A71A277535A1B256C7EFD (void);
// 0x0000024C System.Void EmeraldAI.Utility.EmeraldAIObjectPool/PoolMember::.ctor()
extern void PoolMember__ctor_mDFEEF36BE3B683923DCDF5A8F0BC4CEB1270ACAA (void);
// 0x0000024D System.Void EmeraldAI.Utility.EmeraldAIProjectile::Awake()
extern void EmeraldAIProjectile_Awake_m541F97261D9A5E7F0F636C638C74D60870B5ECAD (void);
// 0x0000024E System.Void EmeraldAI.Utility.EmeraldAIProjectile::Start()
extern void EmeraldAIProjectile_Start_mD9EE46B200DDBFC51DD189A762A2B9F8E5FEC3D8 (void);
// 0x0000024F System.Void EmeraldAI.Utility.EmeraldAIProjectile::InitailizeAudioSource()
extern void EmeraldAIProjectile_InitailizeAudioSource_m8F96232150BE06CADF9DDFD43D056E8F0F9AA5DF (void);
// 0x00000250 System.Void EmeraldAI.Utility.EmeraldAIProjectile::InitializeProjectile(EmeraldAI.EmeraldAISystem,EmeraldAI.EmeraldAIAbility)
extern void EmeraldAIProjectile_InitializeProjectile_m31943F9BE26A9BAC154D81D03F055F9E021DCACC (void);
// 0x00000251 System.Void EmeraldAI.Utility.EmeraldAIProjectile::GetHeatSeekingAngle()
extern void EmeraldAIProjectile_GetHeatSeekingAngle_m3F8D2295DB55A7353F178D8DA8C17A5A2E4FDD6D (void);
// 0x00000252 System.Void EmeraldAI.Utility.EmeraldAIProjectile::GetAngle()
extern void EmeraldAIProjectile_GetAngle_m13E3119AD654E0A7E93A50E8C1CFDEB989C6F00A (void);
// 0x00000253 System.Void EmeraldAI.Utility.EmeraldAIProjectile::Update()
extern void EmeraldAIProjectile_Update_m9F37882F9B1E67481458ABB19C27F2DD9713C3FC (void);
// 0x00000254 System.Void EmeraldAI.Utility.EmeraldAIProjectile::OnTriggerEnter(UnityEngine.Collider)
extern void EmeraldAIProjectile_OnTriggerEnter_m30D3C7CE1F6D7085EB12C452DFCCCDA81FB6457F (void);
// 0x00000255 System.Void EmeraldAI.Utility.EmeraldAIProjectile::DamagePlayer(System.Int32)
extern void EmeraldAIProjectile_DamagePlayer_m55D0CC68AF0F507ABF53A131A7B40251F3393CBB (void);
// 0x00000256 System.Void EmeraldAI.Utility.EmeraldAIProjectile::DamageNonAITarget(System.Int32)
extern void EmeraldAIProjectile_DamageNonAITarget_m15A216B5451A0AF10F97200EDC797DC39ACF2B36 (void);
// 0x00000257 System.Void EmeraldAI.Utility.EmeraldAIProjectile::AttachToCollider(UnityEngine.Collider)
extern void EmeraldAIProjectile_AttachToCollider_m6854C6F2EFFC615F61EC7F3FAA8A14B682120AFD (void);
// 0x00000258 System.Void EmeraldAI.Utility.EmeraldAIProjectile::PlayCollisionSound()
extern void EmeraldAIProjectile_PlayCollisionSound_m3B64DFF6F27432D2B868628BF88A2A211EC8F7F5 (void);
// 0x00000259 System.Void EmeraldAI.Utility.EmeraldAIProjectile::.ctor()
extern void EmeraldAIProjectile__ctor_mAFE905AF86297F135315668ACC2C455D386E2490 (void);
// 0x0000025A System.Void EmeraldAI.Utility.EmeraldAIProjectileTimeout::Update()
extern void EmeraldAIProjectileTimeout_Update_m76C9A02421304DC1873487B22E09670596EBC1C8 (void);
// 0x0000025B System.Void EmeraldAI.Utility.EmeraldAIProjectileTimeout::OnDisable()
extern void EmeraldAIProjectileTimeout_OnDisable_mCF2FC08C5AEB844050FF60FBBB58E3B924CE8828 (void);
// 0x0000025C System.Void EmeraldAI.Utility.EmeraldAIProjectileTimeout::.ctor()
extern void EmeraldAIProjectileTimeout__ctor_mC179FFB97BF8E636751ADE406041F943584AEC1B (void);
// 0x0000025D System.Void EmeraldAI.Utility.EmeraldAITimedDespawn::Update()
extern void EmeraldAITimedDespawn_Update_m7987E2D0F93BA6DBADF4086D581F64A556A3146D (void);
// 0x0000025E System.Void EmeraldAI.Utility.EmeraldAITimedDespawn::OnDisable()
extern void EmeraldAITimedDespawn_OnDisable_m897938B04CED2EC2CE10775A01C240DD473A2F4D (void);
// 0x0000025F System.Void EmeraldAI.Utility.EmeraldAITimedDespawn::.ctor()
extern void EmeraldAITimedDespawn__ctor_m20EC36AD7E747093B712F6B9FF8F722464B4D32D (void);
// 0x00000260 System.Void EmeraldAI.Utility.HandIKProfile::.ctor()
extern void HandIKProfile__ctor_m80AAE7993F21F3A639EB5ADB3AD82A53032EC9AC (void);
// 0x00000261 System.Void EmeraldAI.Utility.SummonedAIComponent::IntitializeSummon(System.Int32)
extern void SummonedAIComponent_IntitializeSummon_m3BEC35559E3A5D326F2AFFA0284317F03CAF1013 (void);
// 0x00000262 System.Collections.IEnumerator EmeraldAI.Utility.SummonedAIComponent::DespawnSummonedAI()
extern void SummonedAIComponent_DespawnSummonedAI_mDC649AE08FB1354483ADEDF497A74430E978688B (void);
// 0x00000263 System.Void EmeraldAI.Utility.SummonedAIComponent::.ctor()
extern void SummonedAIComponent__ctor_mA9E9829D3429684BC4473422611E5585C2CA3A2C (void);
// 0x00000264 System.Void EmeraldAI.Utility.SummonedAIComponent/<DespawnSummonedAI>d__2::.ctor(System.Int32)
extern void U3CDespawnSummonedAIU3Ed__2__ctor_m3C7C61502E288F877E17FC32C81C11C9EB7D9118 (void);
// 0x00000265 System.Void EmeraldAI.Utility.SummonedAIComponent/<DespawnSummonedAI>d__2::System.IDisposable.Dispose()
extern void U3CDespawnSummonedAIU3Ed__2_System_IDisposable_Dispose_mAE67CE37DCF3763CCF53736F9E3BDF59B444144C (void);
// 0x00000266 System.Boolean EmeraldAI.Utility.SummonedAIComponent/<DespawnSummonedAI>d__2::MoveNext()
extern void U3CDespawnSummonedAIU3Ed__2_MoveNext_mDC8B1CB467937234B3EB0E3EB4DF5071E9F691A9 (void);
// 0x00000267 System.Object EmeraldAI.Utility.SummonedAIComponent/<DespawnSummonedAI>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDespawnSummonedAIU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m631E76C46F5D7BC0457C3F9EA5F76D45D72BE2D1 (void);
// 0x00000268 System.Void EmeraldAI.Utility.SummonedAIComponent/<DespawnSummonedAI>d__2::System.Collections.IEnumerator.Reset()
extern void U3CDespawnSummonedAIU3Ed__2_System_Collections_IEnumerator_Reset_m1DFBE0EECED137260A1091251AC834AB73C8C18F (void);
// 0x00000269 System.Object EmeraldAI.Utility.SummonedAIComponent/<DespawnSummonedAI>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CDespawnSummonedAIU3Ed__2_System_Collections_IEnumerator_get_Current_m42F097180D3AB62A3D2201945A84ABC0356A9563 (void);
// 0x0000026A System.Void EmeraldAI.Utility.TargetPositionModifier::OnDrawGizmosSelected()
extern void TargetPositionModifier_OnDrawGizmosSelected_mF508F24E8FED5CCCF3FA1C2A6321565D289192E6 (void);
// 0x0000026B System.Void EmeraldAI.Utility.TargetPositionModifier::.ctor()
extern void TargetPositionModifier__ctor_mB44157621A03D1EF7099720B011FBEC5CD952363 (void);
// 0x0000026C System.Void EmeraldAI.Utility.TrailRendererHelper::Awake()
extern void TrailRendererHelper_Awake_m024630BD346F83071721B0E49DDFAC7B775FEB75 (void);
// 0x0000026D System.Void EmeraldAI.Utility.TrailRendererHelper::OnDisable()
extern void TrailRendererHelper_OnDisable_m82DC24FBE41E970CC5A219C14F1FE9E6921B39AB (void);
// 0x0000026E System.Void EmeraldAI.Utility.TrailRendererHelper::.ctor()
extern void TrailRendererHelper__ctor_m95C55E0CC7AAFC3BB7060BE5A8F3106A2C46E5C5 (void);
// 0x0000026F System.Void EmeraldAI.Utility.VisibilityCheck::Start()
extern void VisibilityCheck_Start_m14755D832F04E93C262C5699CE893D9AB2155702 (void);
// 0x00000270 System.Void EmeraldAI.Utility.VisibilityCheck::InitializeDelay()
extern void VisibilityCheck_InitializeDelay_mDD8B25C008F5AA0E5B3BF9DEC65613F40E09EC3A (void);
// 0x00000271 System.Void EmeraldAI.Utility.VisibilityCheck::OnBecameInvisible()
extern void VisibilityCheck_OnBecameInvisible_mDDEA72303F2BB64E2280410A3471E8703F6E8083 (void);
// 0x00000272 System.Void EmeraldAI.Utility.VisibilityCheck::OnWillRenderObject()
extern void VisibilityCheck_OnWillRenderObject_m0177F90B78EBF0FC80D070C52A975918A2DC6804 (void);
// 0x00000273 System.Void EmeraldAI.Utility.VisibilityCheck::OnBecameVisible()
extern void VisibilityCheck_OnBecameVisible_m46671C26EAC77F739D8DBDC048296D4C4A9BC526 (void);
// 0x00000274 System.Void EmeraldAI.Utility.VisibilityCheck::.ctor()
extern void VisibilityCheck__ctor_mDF6E2E7F00A67EE72B71C75C1E15DA909AC1A9D9 (void);
// 0x00000275 System.Void EmeraldAI.Utility.VisibilityCheckDelay::Start()
extern void VisibilityCheckDelay_Start_m6566BFFF81A848DE93D177879814E2BC317D4EA5 (void);
// 0x00000276 System.Void EmeraldAI.Utility.VisibilityCheckDelay::InitializeDelay()
extern void VisibilityCheckDelay_InitializeDelay_mF0AAD81F8F44D122A60E2B1BFCB211FBD3B90795 (void);
// 0x00000277 System.Void EmeraldAI.Utility.VisibilityCheckDelay::OnWillRenderObject()
extern void VisibilityCheckDelay_OnWillRenderObject_mB9EF14F29A06C0C50BE99687B7CDCBDA7D85AFFC (void);
// 0x00000278 System.Void EmeraldAI.Utility.VisibilityCheckDelay::OnBecameInvisible()
extern void VisibilityCheckDelay_OnBecameInvisible_m5FBA52667CF0CEB200F872EEE19A5DA8346D6220 (void);
// 0x00000279 System.Void EmeraldAI.Utility.VisibilityCheckDelay::DeactivateDelay()
extern void VisibilityCheckDelay_DeactivateDelay_m0A142AAFFA1CE1493CD49254FF38D77547B24133 (void);
// 0x0000027A System.Void EmeraldAI.Utility.VisibilityCheckDelay::OnBecameVisible()
extern void VisibilityCheckDelay_OnBecameVisible_m60F68642AA4A95054CC0F7665C9B7AB969C63948 (void);
// 0x0000027B System.Void EmeraldAI.Utility.VisibilityCheckDelay::.ctor()
extern void VisibilityCheckDelay__ctor_mA9922B51BB372A768F3A84AB807455BAAFC7857D (void);
// 0x0000027C System.Void EmeraldAI.Utility.DebugEvent::DebugEventTest()
extern void DebugEvent_DebugEventTest_m9687F4ECBD86DDB94272EBD5F11D6567F45405DD (void);
// 0x0000027D System.Void EmeraldAI.Utility.DebugEvent::.ctor()
extern void DebugEvent__ctor_mDA8E8290C8C13A2E43E4F28158864889C18D3B24 (void);
// 0x0000027E System.Void EmeraldAI.Utility.CombatText::Start()
extern void CombatText_Start_m7F41117168280E0A2A540B854CC9AF4D2F5A7C2B (void);
// 0x0000027F System.Void EmeraldAI.Utility.CombatText::Update()
extern void CombatText_Update_mD5F47CCEE359B4EAA5516E6FF54AE744A7FECC69 (void);
// 0x00000280 System.Void EmeraldAI.Utility.CombatText::.ctor()
extern void CombatText__ctor_mB5C74740D05B0926F0830F42C3F7A020E83C6655 (void);
// 0x00000281 System.Void EmeraldAI.Utility.CombatTextAnimator::Start()
extern void CombatTextAnimator_Start_m9DAE84B71E4EC259B67D2081D3EC0F5FE94DE2FE (void);
// 0x00000282 System.Void EmeraldAI.Utility.CombatTextAnimator::Update()
extern void CombatTextAnimator_Update_m00035DA5AE643E1752F37BC1B72F3600E771AC1E (void);
// 0x00000283 System.Void EmeraldAI.Utility.CombatTextAnimator::OnEnable()
extern void CombatTextAnimator_OnEnable_m59E4C59CEF8D80772DE2468F831C3FD5C659F674 (void);
// 0x00000284 System.Void EmeraldAI.Utility.CombatTextAnimator::OnDisable()
extern void CombatTextAnimator_OnDisable_m0F65903702349916C7627A678E385DBE175764D3 (void);
// 0x00000285 System.Void EmeraldAI.Utility.CombatTextAnimator::.ctor()
extern void CombatTextAnimator__ctor_mCC4A40AFF93E6FEBFD2F873617C0012104EB87E2 (void);
// 0x00000286 System.Void EmeraldAI.Utility.EmeraldAIHealthBar::Start()
extern void EmeraldAIHealthBar_Start_m5A8E7AD4B16A6A69EDF2A762B5E347FA9A532633 (void);
// 0x00000287 System.Void EmeraldAI.Utility.EmeraldAIHealthBar::UpdateUI()
extern void EmeraldAIHealthBar_UpdateUI_m8AFF5911F35780D0EA4D4F754FBD1EDA36F30B34 (void);
// 0x00000288 System.Void EmeraldAI.Utility.EmeraldAIHealthBar::Update()
extern void EmeraldAIHealthBar_Update_mBC659A0EBE8C01095E4FF8450D929ED3978C95E6 (void);
// 0x00000289 System.Void EmeraldAI.Utility.EmeraldAIHealthBar::CalculateUI()
extern void EmeraldAIHealthBar_CalculateUI_m8AD4E711C6DB491D0092F37FE6033B852F7FD450 (void);
// 0x0000028A System.Void EmeraldAI.Utility.EmeraldAIHealthBar::FadeOut()
extern void EmeraldAIHealthBar_FadeOut_mE6C5172F84A440AC570CB4E28A75426876A9D456 (void);
// 0x0000028B System.Void EmeraldAI.Utility.EmeraldAIHealthBar::OnDisable()
extern void EmeraldAIHealthBar_OnDisable_m2FAC2AAF8915FE3D0B0AED8C3994302B4AF6A9C3 (void);
// 0x0000028C System.Collections.IEnumerator EmeraldAI.Utility.EmeraldAIHealthBar::FadeTo(System.Single,System.Single)
extern void EmeraldAIHealthBar_FadeTo_m94D5050A5724399C2015F5524BD946C15C040C58 (void);
// 0x0000028D System.Void EmeraldAI.Utility.EmeraldAIHealthBar::.ctor()
extern void EmeraldAIHealthBar__ctor_mEE8414DB8B5D45780DCC517D22EDE3C2C7089CDA (void);
// 0x0000028E System.Void EmeraldAI.Utility.EmeraldAIHealthBar::.cctor()
extern void EmeraldAIHealthBar__cctor_m1BF6F0CF01191FBDC9FF5A7A4A4E9D7A8D7A322F (void);
// 0x0000028F System.Void EmeraldAI.Utility.EmeraldAIHealthBar/<FadeTo>d__17::.ctor(System.Int32)
extern void U3CFadeToU3Ed__17__ctor_m014EAF78D1A6721837081920CA2688C0889B80F9 (void);
// 0x00000290 System.Void EmeraldAI.Utility.EmeraldAIHealthBar/<FadeTo>d__17::System.IDisposable.Dispose()
extern void U3CFadeToU3Ed__17_System_IDisposable_Dispose_m66BDEB12928B226D6125BE8B132D80138A217452 (void);
// 0x00000291 System.Boolean EmeraldAI.Utility.EmeraldAIHealthBar/<FadeTo>d__17::MoveNext()
extern void U3CFadeToU3Ed__17_MoveNext_m7C7D09DEC81384148BDB653D409DF1EA3BC10E8E (void);
// 0x00000292 System.Object EmeraldAI.Utility.EmeraldAIHealthBar/<FadeTo>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeToU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65F555CB65E12F7EC1A62C836AFA6B97DEC9FF95 (void);
// 0x00000293 System.Void EmeraldAI.Utility.EmeraldAIHealthBar/<FadeTo>d__17::System.Collections.IEnumerator.Reset()
extern void U3CFadeToU3Ed__17_System_Collections_IEnumerator_Reset_mA5C040D683D2E9552EA50C4DB1F8B5B933E04CCB (void);
// 0x00000294 System.Object EmeraldAI.Utility.EmeraldAIHealthBar/<FadeTo>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CFadeToU3Ed__17_System_Collections_IEnumerator_get_Current_mA3592636DFFDFE800DB48C7FB222B6A56A58F5CA (void);
// 0x00000295 System.Void EmeraldAI.CharacterController.EmeraldAICharacterController::Awake()
extern void EmeraldAICharacterController_Awake_mE86036FB0B6BA4CCF0F1E1B4BD04EF193A6E0195 (void);
// 0x00000296 System.Void EmeraldAI.CharacterController.EmeraldAICharacterController::FixedUpdate()
extern void EmeraldAICharacterController_FixedUpdate_m2C8ACF38845D3ECFE3C1176F3CAA01710BFA6BA3 (void);
// 0x00000297 System.Void EmeraldAI.CharacterController.EmeraldAICharacterController::OnCollisionStay(UnityEngine.Collision)
extern void EmeraldAICharacterController_OnCollisionStay_mEE2335E9337A80ED2D2145453A8C0594CFF24508 (void);
// 0x00000298 System.Single EmeraldAI.CharacterController.EmeraldAICharacterController::CalculateJumpVerticalwalkSpeed()
extern void EmeraldAICharacterController_CalculateJumpVerticalwalkSpeed_m6E82CABD6074603F0225E2F2871BC2EF87C22CCB (void);
// 0x00000299 System.Void EmeraldAI.CharacterController.EmeraldAICharacterController::.ctor()
extern void EmeraldAICharacterController__ctor_m06156EA548F49A766331FCFC384FD298C116C447 (void);
// 0x0000029A System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::Start()
extern void EmeraldAICharacterControllerTopDown_Start_m434980E84DC0CCF7775F3CD124071F05DD17997D (void);
// 0x0000029B System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::Update()
extern void EmeraldAICharacterControllerTopDown_Update_mFF499F17DF21B7EBE8ECC780FFE9B565A69AF050 (void);
// 0x0000029C System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::UpdateHUD()
extern void EmeraldAICharacterControllerTopDown_UpdateHUD_m6415AB9783BFC24CF7EB011B83A0151A2147E07B (void);
// 0x0000029D System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::StopHUDFade()
extern void EmeraldAICharacterControllerTopDown_StopHUDFade_m3B83475A9B273D19551654F23E2B96AA57DB16F3 (void);
// 0x0000029E System.Collections.IEnumerator EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::FadeHUD(System.Single,System.Single)
extern void EmeraldAICharacterControllerTopDown_FadeHUD_m71679D8B6A1656C20CB682A3578CEE3EC5DA75D8 (void);
// 0x0000029F System.Boolean EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::GenerateCritOdds()
extern void EmeraldAICharacterControllerTopDown_GenerateCritOdds_mB15DAA42F71EAFA2CE7CC9701128FCA72F0A6157 (void);
// 0x000002A0 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::DamagePlayer(System.Int32)
extern void EmeraldAICharacterControllerTopDown_DamagePlayer_m000849138FC62351E8E9A67CC81B3D8E3AD5B11F (void);
// 0x000002A1 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::PlayHitAnimation()
extern void EmeraldAICharacterControllerTopDown_PlayHitAnimation_m0C196B9438B0298EC08B4B5F90B79FD3699298C1 (void);
// 0x000002A2 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::UpdateAttackSpeed()
extern void EmeraldAICharacterControllerTopDown_UpdateAttackSpeed_m6A0BDB147F4CB8D412A7D282F323CB67C5AC2BBC (void);
// 0x000002A3 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::PlayImpactSound()
extern void EmeraldAICharacterControllerTopDown_PlayImpactSound_mD545FEABFBD85CCF5851890991448248512F7A98 (void);
// 0x000002A4 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::PlayCriticalHitSound()
extern void EmeraldAICharacterControllerTopDown_PlayCriticalHitSound_m571560124DAD33A65F47C61AB4C4AB7A42DC865C (void);
// 0x000002A5 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::PlayAttackSound()
extern void EmeraldAICharacterControllerTopDown_PlayAttackSound_mDBA9E600D50CF87B558231C7A7C8E3344FD19886 (void);
// 0x000002A6 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::PlayInjuredSound()
extern void EmeraldAICharacterControllerTopDown_PlayInjuredSound_m424BC3D98C45FC207DF3A005DC0115A6D19D72C4 (void);
// 0x000002A7 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::PlayFootstepSound()
extern void EmeraldAICharacterControllerTopDown_PlayFootstepSound_mD2DFB307FB4345D6DF448B302FC8633F477B22F7 (void);
// 0x000002A8 System.Collections.IEnumerator EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::DamageFlash(UnityEngine.Renderer)
extern void EmeraldAICharacterControllerTopDown_DamageFlash_m8C07E808E8601607D4FFE12861B58CF0D38B0B33 (void);
// 0x000002A9 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::AttackTarget()
extern void EmeraldAICharacterControllerTopDown_AttackTarget_m4ED7442C6D20183A0E29397D5E7D50F7119B54C3 (void);
// 0x000002AA System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown::.ctor()
extern void EmeraldAICharacterControllerTopDown__ctor_m0123EA7F80AAE6234E973CCDD608C85BF41646C5 (void);
// 0x000002AB System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<FadeHUD>d__53::.ctor(System.Int32)
extern void U3CFadeHUDU3Ed__53__ctor_m1F22327858A076BE7DADBA6076EE9B15B3CBF2B9 (void);
// 0x000002AC System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<FadeHUD>d__53::System.IDisposable.Dispose()
extern void U3CFadeHUDU3Ed__53_System_IDisposable_Dispose_m840376B660F810C3DABDD355D5247CDD5D91E726 (void);
// 0x000002AD System.Boolean EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<FadeHUD>d__53::MoveNext()
extern void U3CFadeHUDU3Ed__53_MoveNext_m46CB8E798845424F1FC7F74C1306182E8641B3E8 (void);
// 0x000002AE System.Object EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<FadeHUD>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeHUDU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66E53E0BD358A1085020F7D19B0C058651066DD1 (void);
// 0x000002AF System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<FadeHUD>d__53::System.Collections.IEnumerator.Reset()
extern void U3CFadeHUDU3Ed__53_System_Collections_IEnumerator_Reset_m83C1788731B5BD4DBAE484741BCB2ED1D51C288E (void);
// 0x000002B0 System.Object EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<FadeHUD>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CFadeHUDU3Ed__53_System_Collections_IEnumerator_get_Current_mCA148FDB72D0FF869E08C24AD64F01F275B26711 (void);
// 0x000002B1 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<DamageFlash>d__63::.ctor(System.Int32)
extern void U3CDamageFlashU3Ed__63__ctor_m780BD742ED2DE2338314A5DBD2C246B0B3684EA0 (void);
// 0x000002B2 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<DamageFlash>d__63::System.IDisposable.Dispose()
extern void U3CDamageFlashU3Ed__63_System_IDisposable_Dispose_mDA06D3301EB76A42C39293B7EFCA7D4358ED7095 (void);
// 0x000002B3 System.Boolean EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<DamageFlash>d__63::MoveNext()
extern void U3CDamageFlashU3Ed__63_MoveNext_m71A7B91950273B3EE5ED7A545E34C0DAD3CE9DBF (void);
// 0x000002B4 System.Object EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<DamageFlash>d__63::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDamageFlashU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m046AE2B4611065C4EEA43F822CF0D39DAA6460C0 (void);
// 0x000002B5 System.Void EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<DamageFlash>d__63::System.Collections.IEnumerator.Reset()
extern void U3CDamageFlashU3Ed__63_System_Collections_IEnumerator_Reset_m866486D3113D700E944CDF0C0EA3C4C07D54C16A (void);
// 0x000002B6 System.Object EmeraldAI.CharacterController.EmeraldAICharacterControllerTopDown/<DamageFlash>d__63::System.Collections.IEnumerator.get_Current()
extern void U3CDamageFlashU3Ed__63_System_Collections_IEnumerator_get_Current_mF7EB101309A7B58D50BF053AC6B0720110945C0D (void);
// 0x000002B7 System.Void EmeraldAI.CharacterController.EmeraldAIHideMouse::Start()
extern void EmeraldAIHideMouse_Start_m02F80F68F17D4E0D93FFE124DC0BD713D869C508 (void);
// 0x000002B8 System.Void EmeraldAI.CharacterController.EmeraldAIHideMouse::Update()
extern void EmeraldAIHideMouse_Update_mF2227B14D3C8F9B275969D945456C73F0ECE27BB (void);
// 0x000002B9 System.Void EmeraldAI.CharacterController.EmeraldAIHideMouse::.ctor()
extern void EmeraldAIHideMouse__ctor_mE0C076A08E8846FD583B99576217CFB207B8995A (void);
// 0x000002BA System.Void EmeraldAI.CharacterController.EmeraldAIMouseLook::Start()
extern void EmeraldAIMouseLook_Start_mCAFF03EF8493638C229901963AF321E23E24E67F (void);
// 0x000002BB System.Void EmeraldAI.CharacterController.EmeraldAIMouseLook::Update()
extern void EmeraldAIMouseLook_Update_mB1CE642BB82495B250C54E0EED69DBD9FE6ED136 (void);
// 0x000002BC System.Void EmeraldAI.CharacterController.EmeraldAIMouseLook::.ctor()
extern void EmeraldAIMouseLook__ctor_m7BA05B512870C3DACBB026A665EE85DD4AEB7455 (void);
// 0x000002BD System.Void EmeraldAI.CharacterController.EmeraldAISmoothFollow::Update()
extern void EmeraldAISmoothFollow_Update_m2DBC137B5BE3365A7CC98C9A92B4A99C3306AF80 (void);
// 0x000002BE System.Void EmeraldAI.CharacterController.EmeraldAISmoothFollow::.ctor()
extern void EmeraldAISmoothFollow__ctor_m2E4C593FD8B775A92953C32BB80E654430D58777 (void);
// 0x000002BF System.Void EmeraldAI.CharacterController.DamageAIByAngle::Update()
extern void DamageAIByAngle_Update_mF39EB9806A52CEBA6F571A1C578EAD4369FC71C0 (void);
// 0x000002C0 System.Void EmeraldAI.CharacterController.DamageAIByAngle::.ctor()
extern void DamageAIByAngle__ctor_m6E3D92229711034E67164A864D5903DDA25FFDF7 (void);
// 0x000002C1 System.Void EmeraldAI.CharacterController.DamageAISystem::Update()
extern void DamageAISystem_Update_m61754498ED6FA6AAA7022DAE520B75484A90A115 (void);
// 0x000002C2 System.Void EmeraldAI.CharacterController.DamageAISystem::.ctor()
extern void DamageAISystem__ctor_mF252380CC616DD1F01FF9A4D664FE583CF924685 (void);
// 0x000002C3 System.Void EmeraldAI.CharacterController.PlayerWeapon::Start()
extern void PlayerWeapon_Start_m93E59AA1E94422FE5794BEAA75A49972133A49AC (void);
// 0x000002C4 System.Void EmeraldAI.CharacterController.PlayerWeapon::FixedUpdate()
extern void PlayerWeapon_FixedUpdate_m7CB71E473715B4B554706F26F6F2D9BEB921456E (void);
// 0x000002C5 System.Void EmeraldAI.CharacterController.PlayerWeapon::Update()
extern void PlayerWeapon_Update_m03AE70B30EE48C1EB7CFD07ACC9FFF8A7AA45F3A (void);
// 0x000002C6 System.Void EmeraldAI.CharacterController.PlayerWeapon::.ctor()
extern void PlayerWeapon__ctor_m1781F1ABB848314457CD8DEE1E86ABD50E12BE3F (void);
// 0x000002C7 System.Void EmeraldAI.CharacterController.PlayerWeapon3rdPerson::Start()
extern void PlayerWeapon3rdPerson_Start_mE072C768439080D908DC4C2C507AC518CF9C93CE (void);
// 0x000002C8 System.Void EmeraldAI.CharacterController.PlayerWeapon3rdPerson::FixedUpdate()
extern void PlayerWeapon3rdPerson_FixedUpdate_m650DD8F9B34F33B0F9121D5EA077B3F7D0405E00 (void);
// 0x000002C9 System.Void EmeraldAI.CharacterController.PlayerWeapon3rdPerson::Update()
extern void PlayerWeapon3rdPerson_Update_m27A19FBD3F32B5E7557435BA3D85C4D15EF7408F (void);
// 0x000002CA System.Void EmeraldAI.CharacterController.PlayerWeapon3rdPerson::.ctor()
extern void PlayerWeapon3rdPerson__ctor_m00BA5506DCF8555D2ECAAB7681B93E89F2A98EA6 (void);
// 0x000002CB System.Void EmeraldAI.CharacterController.SwordAnimation::Start()
extern void SwordAnimation_Start_m5F1971FF366E85A6F46ABECABAB6521595AB0434 (void);
// 0x000002CC System.Void EmeraldAI.CharacterController.SwordAnimation::Update()
extern void SwordAnimation_Update_mD97DEE29589C086FE1C275205ED0086E65126231 (void);
// 0x000002CD System.Void EmeraldAI.CharacterController.SwordAnimation::.ctor()
extern void SwordAnimation__ctor_mF92C7F59AC5E0DDAB1AA75EF41E1010B32D23143 (void);
// 0x000002CE System.Void BNG.HandPoseDefinitions::.ctor()
extern void HandPoseDefinitions__ctor_m5F7626E8103E354315DC7D044D49C82AD5B57D3E (void);
// 0x000002CF BNG.HandPoseDefinition BNG.AutoPoser::get_CollisionPose()
extern void AutoPoser_get_CollisionPose_mFD7CEBC5C26472A5CAE7DF6BE545260F5D0A88FC (void);
// 0x000002D0 System.Boolean BNG.AutoPoser::get_CollisionDetected()
extern void AutoPoser_get_CollisionDetected_m76D17038180FC3E2D4213FE71ACAB8B9EFF36E55 (void);
// 0x000002D1 System.Void BNG.AutoPoser::Start()
extern void AutoPoser_Start_m2E28E95650BC124EB9D7931791BE2EE6863920B6 (void);
// 0x000002D2 System.Void BNG.AutoPoser::OnEnable()
extern void AutoPoser_OnEnable_m07EAE503AA2E612BA41987692E771192B8D9462C (void);
// 0x000002D3 System.Void BNG.AutoPoser::Update()
extern void AutoPoser_Update_m97880FCD52B42261E35E5F83587DD4100D8750BA (void);
// 0x000002D4 System.Void BNG.AutoPoser::UpdateAutoPose(System.Boolean)
extern void AutoPoser_UpdateAutoPose_m7C1F7931694B4C0BC8AFBDAE2ECC9BC443BAC73D (void);
// 0x000002D5 System.Void BNG.AutoPoser::UpdateAutoPoseOnce()
extern void AutoPoser_UpdateAutoPoseOnce_mE331050A984A06921D5A5C75E154F98636CFB4F8 (void);
// 0x000002D6 System.Collections.IEnumerator BNG.AutoPoser::updateAutoPoseRoutine()
extern void AutoPoser_updateAutoPoseRoutine_mA568AFF75852EABC143310F452A324D39717EA76 (void);
// 0x000002D7 BNG.HandPoseDefinition BNG.AutoPoser::GetAutoPose()
extern void AutoPoser_GetAutoPose_mC49251C2895DD178E3A0EC4D5CFC89A7F687D7F3 (void);
// 0x000002D8 BNG.HandPoseDefinition BNG.AutoPoser::CopyHandDefinition(BNG.HandPoseDefinition)
extern void AutoPoser_CopyHandDefinition_mD138424B8DC3C157CDFF362ECC7F00325699B64F (void);
// 0x000002D9 BNG.FingerJoint BNG.AutoPoser::GetJointCopy(BNG.FingerJoint)
extern void AutoPoser_GetJointCopy_m32A3FC2E1F082832CF33952CACEDB6A1E7CEB1BF (void);
// 0x000002DA System.Collections.Generic.List`1<BNG.FingerJoint> BNG.AutoPoser::GetJointsCopy(System.Collections.Generic.List`1<BNG.FingerJoint>)
extern void AutoPoser_GetJointsCopy_mD188028C70433B512BAEF56DD4A009E627D76B04 (void);
// 0x000002DB System.Boolean BNG.AutoPoser::GetThumbHit(BNG.HandPoser)
extern void AutoPoser_GetThumbHit_mA165431EBAAB69A7437A6D4C2EC35143E574ABD1 (void);
// 0x000002DC System.Boolean BNG.AutoPoser::GetIndexHit(BNG.HandPoser)
extern void AutoPoser_GetIndexHit_m787A56FDC1C5D93EFFC067EA62E8C44836A8E7D0 (void);
// 0x000002DD System.Boolean BNG.AutoPoser::GetMiddleHit(BNG.HandPoser)
extern void AutoPoser_GetMiddleHit_m749DFDA04D272FAF895B4CBD33F5A771DADB7153 (void);
// 0x000002DE System.Boolean BNG.AutoPoser::GetRingHit(BNG.HandPoser)
extern void AutoPoser_GetRingHit_m4315BC9CAFD0594E5861E9CC454653B7D8B6CE4A (void);
// 0x000002DF System.Boolean BNG.AutoPoser::GetPinkyHit(BNG.HandPoser)
extern void AutoPoser_GetPinkyHit_mF7BD80119D14A688CA69448E537C83B8810EDFEC (void);
// 0x000002E0 System.Boolean BNG.AutoPoser::LoopThroughJoints(System.Collections.Generic.List`1<UnityEngine.Transform>,System.Collections.Generic.List`1<BNG.FingerJoint>,UnityEngine.Vector3,System.Single)
extern void AutoPoser_LoopThroughJoints_m0731B3269A6A93D9295D20D6788ECF67AD823662 (void);
// 0x000002E1 System.Boolean BNG.AutoPoser::IsValidCollision(UnityEngine.Collider)
extern void AutoPoser_IsValidCollision_mEA38253B10A562E2F811881AE419B599CB0B5AAD (void);
// 0x000002E2 System.Void BNG.AutoPoser::OnDrawGizmos()
extern void AutoPoser_OnDrawGizmos_mB6FAC34D4D160AAF00179C159EAFB62053DD9C65 (void);
// 0x000002E3 System.Void BNG.AutoPoser::DrawJointGizmo(BNG.FingerTipCollider,UnityEngine.Vector3,System.Single,BNG.GizmoDisplayType)
extern void AutoPoser_DrawJointGizmo_m2272814DA7D218E2AB05EE278389AB25DBF7E274 (void);
// 0x000002E4 System.Void BNG.AutoPoser::.ctor()
extern void AutoPoser__ctor_mB858FC79D1DCD13D16FDD6C999FA31C5907E4C0E (void);
// 0x000002E5 System.Void BNG.AutoPoser/<updateAutoPoseRoutine>d__33::.ctor(System.Int32)
extern void U3CupdateAutoPoseRoutineU3Ed__33__ctor_mAD8A88603C59CF96DB30FC6E119E824F9084E39A (void);
// 0x000002E6 System.Void BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.IDisposable.Dispose()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_IDisposable_Dispose_m0395046D9F98E17B064A10F1AABA37F3AC73659B (void);
// 0x000002E7 System.Boolean BNG.AutoPoser/<updateAutoPoseRoutine>d__33::MoveNext()
extern void U3CupdateAutoPoseRoutineU3Ed__33_MoveNext_m311AFDB791EC1D8DA200C36E1F3E5F36ED298B71 (void);
// 0x000002E8 System.Object BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17C7AC941F42D773378C8ABDB9293529E40042E0 (void);
// 0x000002E9 System.Void BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.Collections.IEnumerator.Reset()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_Reset_m8B6E046C21B47A3A95C74D2F6BAC596F9D029CE8 (void);
// 0x000002EA System.Object BNG.AutoPoser/<updateAutoPoseRoutine>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_get_Current_m3297388C60BA9B5948CF0248A00BF1DEBC7A789A (void);
// 0x000002EB System.Void BNG.BoneMapping::Update()
extern void BoneMapping_Update_m262A5EB1ABF18C300259EE01D3825A6C3C3566AF (void);
// 0x000002EC System.Void BNG.BoneMapping::OnDrawGizmos()
extern void BoneMapping_OnDrawGizmos_mD7C23755C243C10085499CA7E571B8D3A85FC3A9 (void);
// 0x000002ED System.Void BNG.BoneMapping::.ctor()
extern void BoneMapping__ctor_mD81099307ADD9D81FD3C4C201E0AC92804063188 (void);
// 0x000002EE System.Void BNG.BoneObject::.ctor()
extern void BoneObject__ctor_m3BBE42EC01D28044BB7BA83A4AA6FDE45EAC7C7F (void);
// 0x000002EF System.Void BNG.EditorHandle::.ctor()
extern void EditorHandle__ctor_m243E8ED26997679667C4EF20744FECD79E827887 (void);
// 0x000002F0 System.Void BNG.FingerJoint::.ctor()
extern void FingerJoint__ctor_m7C5DFEA8731BEEDE399163380C530900681DD271 (void);
// 0x000002F1 System.Void BNG.FingerTipCollider::.ctor()
extern void FingerTipCollider__ctor_m5F2805B4BE9C1709172F9F39351F7AE6E1ED278B (void);
// 0x000002F2 System.Void BNG.HandPose::.ctor()
extern void HandPose__ctor_m27023493DEE451197034ECF17135BF050E03C57D (void);
// 0x000002F3 System.Void BNG.HandPoseDefinition::.ctor()
extern void HandPoseDefinition__ctor_m4FE4530ED633AFA30C46B0AF444A07CDD673448F (void);
// 0x000002F4 BNG.HandPoseDefinition BNG.HandPoser::get_HandPoseJoints()
extern void HandPoser_get_HandPoseJoints_mFF4BA9CB1DD1BEB64DCDD2302881CEFA6033194F (void);
// 0x000002F5 System.Void BNG.HandPoser::Start()
extern void HandPoser_Start_mD2A1DD6D58AAD36876B0BCF68F7DAA483D5D1812 (void);
// 0x000002F6 System.Void BNG.HandPoser::Update()
extern void HandPoser_Update_mB1413A3CD7A40D7EFD67CDDA6FFDCF2300E57842 (void);
// 0x000002F7 System.Void BNG.HandPoser::CheckForPoseChange()
extern void HandPoser_CheckForPoseChange_m23B022D36A72D4AD70CC0E7B912645A0506216A9 (void);
// 0x000002F8 System.Void BNG.HandPoser::OnPoseChanged()
extern void HandPoser_OnPoseChanged_m1F8824EF7EA8CC831A3D242EC22FE70935C4E798 (void);
// 0x000002F9 BNG.FingerJoint BNG.HandPoser::GetWristJoint()
extern void HandPoser_GetWristJoint_m79236962D3CA973430FBA5C7C8E6108C45667FCF (void);
// 0x000002FA System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetThumbJoints()
extern void HandPoser_GetThumbJoints_m80E981DD695196885BE6474F6B1E3BF17241644E (void);
// 0x000002FB System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetIndexJoints()
extern void HandPoser_GetIndexJoints_m30055F79008F819D787D9708A7C94F3E66503B9E (void);
// 0x000002FC System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetMiddleJoints()
extern void HandPoser_GetMiddleJoints_mB4BF924D4B0DB49DCF5266AAEB8596402B6149F9 (void);
// 0x000002FD System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetRingJoints()
extern void HandPoser_GetRingJoints_m258285FF09BFF6A7CBF202AAAADBBB1662908B00 (void);
// 0x000002FE System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetPinkyJoints()
extern void HandPoser_GetPinkyJoints_mF0534631B99F329FCD8098B1A4DE2BE95B564014 (void);
// 0x000002FF System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetOtherJoints()
extern void HandPoser_GetOtherJoints_mBE74E98EFE03A5FABA162BF7132ECE50174FADAF (void);
// 0x00000300 UnityEngine.Transform BNG.HandPoser::GetTip(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void HandPoser_GetTip_m2C746D611E6D9B51D977960391F7E3D9440E37E0 (void);
// 0x00000301 UnityEngine.Transform BNG.HandPoser::GetThumbTip()
extern void HandPoser_GetThumbTip_mBB0CC092BBB8FAAD637A65381F4A117EDB38C7AF (void);
// 0x00000302 UnityEngine.Transform BNG.HandPoser::GetIndexTip()
extern void HandPoser_GetIndexTip_mD08B86F82BE63F0C805BADDC2A51C151E75A5FF2 (void);
// 0x00000303 UnityEngine.Transform BNG.HandPoser::GetMiddleTip()
extern void HandPoser_GetMiddleTip_mDF0BAB0D869336B7816B07D4B49EDA79DA2416EC (void);
// 0x00000304 UnityEngine.Transform BNG.HandPoser::GetRingTip()
extern void HandPoser_GetRingTip_m755A492BDC5A42ED426019462D6D66E6C121DC1C (void);
// 0x00000305 UnityEngine.Transform BNG.HandPoser::GetPinkyTip()
extern void HandPoser_GetPinkyTip_m8261E2DFF930A4868663D9E85FDBAD35E692A968 (void);
// 0x00000306 UnityEngine.Vector3 BNG.HandPoser::GetFingerTipPositionWithOffset(System.Collections.Generic.List`1<UnityEngine.Transform>,System.Single)
extern void HandPoser_GetFingerTipPositionWithOffset_m240B26211B19ED7B001645EE349B35E67F1AC7CE (void);
// 0x00000307 System.Collections.Generic.List`1<BNG.FingerJoint> BNG.HandPoser::GetJointsFromTransforms(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void HandPoser_GetJointsFromTransforms_mCF222AC89736CA720D66939BB3CE7AF69B0E5902 (void);
// 0x00000308 BNG.FingerJoint BNG.HandPoser::GetJointFromTransform(UnityEngine.Transform)
extern void HandPoser_GetJointFromTransform_mE8BE23DB2CD68AC9E5A70E13A51E6BD4B6B467AE (void);
// 0x00000309 System.Void BNG.HandPoser::UpdateHandPose(BNG.HandPoseDefinition,System.Boolean)
extern void HandPoser_UpdateHandPose_m769FFD5987253D4A49A7DC5B308CB0114D9CDBB3 (void);
// 0x0000030A System.Void BNG.HandPoser::UpdateJoint(BNG.FingerJoint,UnityEngine.Transform,System.Boolean)
extern void HandPoser_UpdateJoint_m2F353A515F1EEA080BDADF8DC7836AD54654BE6C (void);
// 0x0000030B System.Void BNG.HandPoser::UpdateJoints(System.Collections.Generic.List`1<BNG.FingerJoint>,System.Collections.Generic.List`1<UnityEngine.Transform>,System.Boolean)
extern void HandPoser_UpdateJoints_m716D0080DD20E6A5C155E0219656712113EBA1FC (void);
// 0x0000030C BNG.HandPoseDefinition BNG.HandPoser::GetHandPoseDefinition()
extern void HandPoser_GetHandPoseDefinition_m6D98794EA288C14A65E8D46ED00DED79ABA25ABC (void);
// 0x0000030D System.Void BNG.HandPoser::SavePoseAsScriptablObject(System.String)
extern void HandPoser_SavePoseAsScriptablObject_m294F471982D20BBADC8B19FAE02181A19AD0DE9E (void);
// 0x0000030E System.Void BNG.HandPoser::CreateUniquePose(System.String)
extern void HandPoser_CreateUniquePose_m39C74C01787DEB5D523614C5775D8B8528A677DD (void);
// 0x0000030F BNG.HandPose BNG.HandPoser::GetHandPoseScriptableObject()
extern void HandPoser_GetHandPoseScriptableObject_mBA80CD5D9AE936F0A0581F4F97E1D1B2D51DD1E6 (void);
// 0x00000310 System.Void BNG.HandPoser::DoPoseUpdate()
extern void HandPoser_DoPoseUpdate_mB759A824D26D65C6060C7D673F8E163BAD2C1BC1 (void);
// 0x00000311 System.Void BNG.HandPoser::ResetEditorHandles()
extern void HandPoser_ResetEditorHandles_mD86EE4E9B9E0701D1320D200F36FD2D5AA1566D7 (void);
// 0x00000312 System.Void BNG.HandPoser::OnDrawGizmos()
extern void HandPoser_OnDrawGizmos_m7350BA582A8FAABF28E1FB8A370A60737D72DB2C (void);
// 0x00000313 System.Void BNG.HandPoser::.ctor()
extern void HandPoser__ctor_m0226DE25A0034E85FEE8A702A4A20AA736CA76AE (void);
// 0x00000314 System.Void BNG.SkeletonVisualizer::OnApplicationQuit()
extern void SkeletonVisualizer_OnApplicationQuit_m8DEF88D1FEC64014FC20EC3473F945DF93AB1941 (void);
// 0x00000315 System.Void BNG.SkeletonVisualizer::OnDestroy()
extern void SkeletonVisualizer_OnDestroy_m6BA90A48FB063156329CC12ECD7F690D86E51BDD (void);
// 0x00000316 System.Boolean BNG.SkeletonVisualizer::IsTipOfBone(UnityEngine.Transform)
extern void SkeletonVisualizer_IsTipOfBone_mD8C8D3693D6C081E5C8CDC0B1A5EAEF8A9FB0E0C (void);
// 0x00000317 System.Void BNG.SkeletonVisualizer::ResetEditorHandles()
extern void SkeletonVisualizer_ResetEditorHandles_m580893A2A886BDF9877DDAE9AB6D37F696FE9FBA (void);
// 0x00000318 System.Void BNG.SkeletonVisualizer::.ctor()
extern void SkeletonVisualizer__ctor_m2B2F83013081AF7D0448C71AD67CF48187F825E6 (void);
// 0x00000319 System.Void BNG.DemoCube::Start()
extern void DemoCube_Start_m313786E00669508DDCEC5F8496A1255F6885203E (void);
// 0x0000031A System.Void BNG.DemoCube::SetActive(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_SetActive_m2CB83F2B7FED427D33CD8322DBC5FAE000A03AF0 (void);
// 0x0000031B System.Void BNG.DemoCube::SetInactive(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_SetInactive_m5C733DABF7E321A94471FA3CB06EAD43510D89E9 (void);
// 0x0000031C System.Void BNG.DemoCube::SetHovering(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_SetHovering_mECC5166A9870D62227EADBECE3E49589459662E6 (void);
// 0x0000031D System.Void BNG.DemoCube::ResetHovering(UnityEngine.EventSystems.PointerEventData)
extern void DemoCube_ResetHovering_mD2CFBE4466421186349E1EA554564F38395A6C14 (void);
// 0x0000031E System.Void BNG.DemoCube::UpdateMaterial()
extern void DemoCube_UpdateMaterial_mD217B75914DA2D93FCF48B8546AAD628EF1B5CFA (void);
// 0x0000031F System.Void BNG.DemoCube::.ctor()
extern void DemoCube__ctor_m0A65014D4B3AB8D7ED3276CA1C6F0CEC0F019D56 (void);
// 0x00000320 System.Void BNG.DemoScript::Start()
extern void DemoScript_Start_m6086A23CE32B7BBB8EC92184AD8C3B406249A544 (void);
// 0x00000321 System.Void BNG.DemoScript::Update()
extern void DemoScript_Update_mB801A2C9FF309EB7191C0FD6A0E19170A3918FB5 (void);
// 0x00000322 System.Void BNG.DemoScript::UpdateSliderText(System.Single)
extern void DemoScript_UpdateSliderText_m16E308DC4F6BBC004FEC981824C05265F8B2E81C (void);
// 0x00000323 System.Void BNG.DemoScript::UpdateJoystickText(System.Single,System.Single)
extern void DemoScript_UpdateJoystickText_m84A465C5613699B0B8201FB184B58A4C75E49E64 (void);
// 0x00000324 System.Void BNG.DemoScript::ResetGrabbables()
extern void DemoScript_ResetGrabbables_m08DD75495CC88A96167154DF1B6A467244436F95 (void);
// 0x00000325 System.Void BNG.DemoScript::GrabAmmo(BNG.Grabber)
extern void DemoScript_GrabAmmo_m2E5D68E61645AD6A61DAD0E0974024AB09E139A8 (void);
// 0x00000326 System.Void BNG.DemoScript::ShootLauncher()
extern void DemoScript_ShootLauncher_mC5F844768B7311DBCFD80A33CC5C988F39E18E09 (void);
// 0x00000327 System.Void BNG.DemoScript::initGravityCubes()
extern void DemoScript_initGravityCubes_m86C6C7943E08631271F7792DBA461453AA1F5A36 (void);
// 0x00000328 System.Void BNG.DemoScript::rotateGravityCubes()
extern void DemoScript_rotateGravityCubes_mF007314B2F0447795EE5A1C8AE7B47E2EB21434A (void);
// 0x00000329 System.Void BNG.DemoScript::.ctor()
extern void DemoScript__ctor_mB4444BF3CF1ACF39A6840CC3451743A87775D7D4 (void);
// 0x0000032A System.Void BNG.PosRot::.ctor()
extern void PosRot__ctor_m2B0C6FFE427D3B17A2539E3E25928EC17774BA5B (void);
// 0x0000032B System.Void BNG.CharacterConstraint::Awake()
extern void CharacterConstraint_Awake_m7C6C3568C74B35FBDDEEA74FA973B48763864B28 (void);
// 0x0000032C System.Void BNG.CharacterConstraint::Update()
extern void CharacterConstraint_Update_mFF296DDA0A335CB1CD9EA20BD49C0453FE7C62B6 (void);
// 0x0000032D System.Void BNG.CharacterConstraint::CheckCharacterCollisionMove()
extern void CharacterConstraint_CheckCharacterCollisionMove_m384C622B943355759BA2EF867B50FF4086C8CEFF (void);
// 0x0000032E System.Void BNG.CharacterConstraint::.ctor()
extern void CharacterConstraint__ctor_m80E38C975DB824973B6B37EDC4877E121A6C40D7 (void);
// 0x0000032F System.Void BNG.CharacterIK::Start()
extern void CharacterIK_Start_mDB9B36E187B68B36904E56F3D7B0EEB5FEA89C80 (void);
// 0x00000330 System.Void BNG.CharacterIK::Update()
extern void CharacterIK_Update_m45C111F5634216F35CCB8B3D91B6A277DDAB4EFC (void);
// 0x00000331 System.Void BNG.CharacterIK::OnAnimatorIK()
extern void CharacterIK_OnAnimatorIK_mC5E2E712264877D00280CDF79AC4D84EF7A91CD1 (void);
// 0x00000332 System.Void BNG.CharacterIK::.ctor()
extern void CharacterIK__ctor_m3825AC98059EAFACED0118FF2FA39BF31C501215 (void);
// 0x00000333 System.Void BNG.CharacterYOffset::LateUpdate()
extern void CharacterYOffset_LateUpdate_mC861A3D20C9B3FB556CF3562AED584470C16E49A (void);
// 0x00000334 System.Void BNG.CharacterYOffset::.ctor()
extern void CharacterYOffset__ctor_m493A4601749D1B934C0783F0FAC4EDA03E4920A7 (void);
// 0x00000335 System.Void BNG.Climbable::Start()
extern void Climbable_Start_mCD2823B417C51EB85186DDDE9F8DE5F011F496F7 (void);
// 0x00000336 System.Void BNG.Climbable::GrabItem(BNG.Grabber)
extern void Climbable_GrabItem_mE1E573204754708E90B5BFEF23598F4E4C7B6325 (void);
// 0x00000337 System.Void BNG.Climbable::DropItem(BNG.Grabber)
extern void Climbable_DropItem_mE6B7692CE7ECFEBFDACA59A252966030EB1C9615 (void);
// 0x00000338 System.Void BNG.Climbable::.ctor()
extern void Climbable__ctor_mD6CDB5061FE37E248994811638930404452A7F4E (void);
// 0x00000339 System.Void BNG.CollisionSound::Start()
extern void CollisionSound_Start_mEB3F0F8BB2665E1152682C2A84A1C9C6FE543792 (void);
// 0x0000033A System.Void BNG.CollisionSound::OnCollisionEnter(UnityEngine.Collision)
extern void CollisionSound_OnCollisionEnter_mD426770D458A0C4161EA49FAF538D65D97565D3C (void);
// 0x0000033B System.Void BNG.CollisionSound::resetLastPlayedSound()
extern void CollisionSound_resetLastPlayedSound_m3E7A945045A9196E2EBA975643E16CEB9212ADDB (void);
// 0x0000033C System.Void BNG.CollisionSound::.ctor()
extern void CollisionSound__ctor_m779A5A39DD793BB0BFD8013BAF8032C55269E06E (void);
// 0x0000033D System.Void BNG.ConstrainLocalPosition::Update()
extern void ConstrainLocalPosition_Update_mA68B159667C818C087A19996FC4A42E356275CFA (void);
// 0x0000033E System.Void BNG.ConstrainLocalPosition::doConstrain()
extern void ConstrainLocalPosition_doConstrain_m2C55CAA127CA23581C36DE52142F57F789E3A0BB (void);
// 0x0000033F System.Void BNG.ConstrainLocalPosition::.ctor()
extern void ConstrainLocalPosition__ctor_mC9EDAC4AD6994C5B59AF1C7C88D1DB312AB6AC7B (void);
// 0x00000340 System.Void BNG.Damageable::Start()
extern void Damageable_Start_mDDBFEEEA339690F430A65798C42704CF87BB9B0E (void);
// 0x00000341 System.Void BNG.Damageable::DealDamage(System.Single)
extern void Damageable_DealDamage_mB1ED4BE3B744F38C5E5E68C08B7F1C40A72F36A1 (void);
// 0x00000342 System.Void BNG.Damageable::DealDamage(System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,UnityEngine.GameObject,UnityEngine.GameObject)
extern void Damageable_DealDamage_m0FE0B42AFAB7A234BC3DE1C14813C89F00E8D818 (void);
// 0x00000343 System.Void BNG.Damageable::DestroyThis()
extern void Damageable_DestroyThis_m68C850E89EC69A2D42C800804A6757865EE8FE3B (void);
// 0x00000344 System.Collections.IEnumerator BNG.Damageable::RespawnRoutine(System.Single)
extern void Damageable_RespawnRoutine_m78721F293EA64A14BC1F33C41FB2AFC6C0320EEC (void);
// 0x00000345 System.Void BNG.Damageable::.ctor()
extern void Damageable__ctor_m38FF94C20302CA97C5515231E56F267255056F90 (void);
// 0x00000346 System.Void BNG.Damageable/<RespawnRoutine>d__22::.ctor(System.Int32)
extern void U3CRespawnRoutineU3Ed__22__ctor_m4A5B5E34C2F9EAF1CE215064BCC9FC3CFABCC2A1 (void);
// 0x00000347 System.Void BNG.Damageable/<RespawnRoutine>d__22::System.IDisposable.Dispose()
extern void U3CRespawnRoutineU3Ed__22_System_IDisposable_Dispose_m1D9DF8E34DEE7A3450A0CCC830AD93B578A3784D (void);
// 0x00000348 System.Boolean BNG.Damageable/<RespawnRoutine>d__22::MoveNext()
extern void U3CRespawnRoutineU3Ed__22_MoveNext_m35CC953F738999C8DF50A5B6B0019B14AB839214 (void);
// 0x00000349 System.Object BNG.Damageable/<RespawnRoutine>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnRoutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD29EAE95323403A13217BF4204D82AC2D6D64B1E (void);
// 0x0000034A System.Void BNG.Damageable/<RespawnRoutine>d__22::System.Collections.IEnumerator.Reset()
extern void U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_Reset_mE3E4292F55A1F6AF6248602B5DEE96E3D0D4775D (void);
// 0x0000034B System.Object BNG.Damageable/<RespawnRoutine>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_get_Current_m54DDFF628D211B2D245A5498C5300C6FE84B88BC (void);
// 0x0000034C System.Void BNG.DamageCollider::Start()
extern void DamageCollider_Start_mA062404FA3F5421B29084E4C51F53E95D3362724 (void);
// 0x0000034D System.Void BNG.DamageCollider::OnCollisionEnter(UnityEngine.Collision)
extern void DamageCollider_OnCollisionEnter_m8E98F9B9E1E59E2A7AE217EE9A29D2C93464B396 (void);
// 0x0000034E System.Void BNG.DamageCollider::OnCollisionEvent(UnityEngine.Collision)
extern void DamageCollider_OnCollisionEvent_mA4D571674AB89DA471C8FA222F85B7CB9C2F299F (void);
// 0x0000034F System.Void BNG.DamageCollider::.ctor()
extern void DamageCollider__ctor_m1B3D697CB98E5DCB5F8BDDFF6B2A7295964EE361 (void);
// 0x00000350 System.Void BNG.DestroyObjectWithDelay::Start()
extern void DestroyObjectWithDelay_Start_m1D8CAE2D18ABF5162D6DE075E60F3E87F95CA385 (void);
// 0x00000351 System.Void BNG.DestroyObjectWithDelay::.ctor()
extern void DestroyObjectWithDelay__ctor_m14F8A04ED3281EAD3EF016993869903AC477C23D (void);
// 0x00000352 System.Void BNG.FollowRigidbody::Start()
extern void FollowRigidbody_Start_m213EDF53A0A66BED921F404806AB868BB358642E (void);
// 0x00000353 System.Void BNG.FollowRigidbody::FixedUpdate()
extern void FollowRigidbody_FixedUpdate_mD5CFA6B1C9B17184E4FB63558041B4362D276622 (void);
// 0x00000354 System.Void BNG.FollowRigidbody::.ctor()
extern void FollowRigidbody__ctor_m998ED9260CE6B74AF881B59E48836D87313705C1 (void);
// 0x00000355 System.Void BNG.FollowTransform::Update()
extern void FollowTransform_Update_m34E8C4C8D774820597029A9F6450AEF14DFD507F (void);
// 0x00000356 System.Void BNG.FollowTransform::.ctor()
extern void FollowTransform__ctor_m033AEC8C823F4F0C42DD06AC1605D0C1C9D993BD (void);
// 0x00000357 System.Void BNG.GrabAction::OnGrab(BNG.Grabber)
extern void GrabAction_OnGrab_m6DB40FFF2F909945AE0CB8A5AFF9E5F1FA1381C6 (void);
// 0x00000358 System.Void BNG.GrabAction::.ctor()
extern void GrabAction__ctor_m0FA35E5F134F677C075E16157C3E340774E29496 (void);
// 0x00000359 System.Void BNG.GrabbableBezierLine::Start()
extern void GrabbableBezierLine_Start_mBA5EB14C7B8ADA00382D9FC60C303EA5045009D2 (void);
// 0x0000035A System.Void BNG.GrabbableBezierLine::LateUpdate()
extern void GrabbableBezierLine_LateUpdate_m29C1CD4EFBC8231C9806286FB51A542A1DEA3B4E (void);
// 0x0000035B System.Void BNG.GrabbableBezierLine::OnGrab(BNG.Grabber)
extern void GrabbableBezierLine_OnGrab_m557867C6E4FB2994248CB91A68747128D3FB8941 (void);
// 0x0000035C System.Void BNG.GrabbableBezierLine::OnBecomesClosestGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnBecomesClosestGrabbable_mA4C8D69BE0545F0466674A9DD2F843B73261AEF4 (void);
// 0x0000035D System.Void BNG.GrabbableBezierLine::OnNoLongerClosestGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnNoLongerClosestGrabbable_m56BB8FD91619BE83040079FF85D0C421DDF805B6 (void);
// 0x0000035E System.Void BNG.GrabbableBezierLine::OnBecomesClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnBecomesClosestRemoteGrabbable_mFD620FD3D1E347796D4E852F1273066E66B638AB (void);
// 0x0000035F System.Void BNG.GrabbableBezierLine::OnNoLongerClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableBezierLine_OnNoLongerClosestRemoteGrabbable_m110F1275EEB561436D1D8424698B98002DB1B232 (void);
// 0x00000360 System.Void BNG.GrabbableBezierLine::HighlightItem(BNG.Grabber)
extern void GrabbableBezierLine_HighlightItem_m6CA30504AA1FC651C2E6A33B255C51FF5E73CA1F (void);
// 0x00000361 System.Void BNG.GrabbableBezierLine::UnhighlightItem()
extern void GrabbableBezierLine_UnhighlightItem_m19DB822A03D76B6CB83A06735EF26B5631986FD8 (void);
// 0x00000362 System.Void BNG.GrabbableBezierLine::DrawBezierCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.LineRenderer)
extern void GrabbableBezierLine_DrawBezierCurve_m31FB9992D949FEAEB0F7B9EA690E145AC2ABE23C (void);
// 0x00000363 System.Void BNG.GrabbableBezierLine::.ctor()
extern void GrabbableBezierLine__ctor_m232F783D66D478AE60F2BA2214FE1D204635A39A (void);
// 0x00000364 System.Void BNG.GrabbableHaptics::OnGrab(BNG.Grabber)
extern void GrabbableHaptics_OnGrab_m657CC7C1758ADAE62A8DA13DFF7FCB86BC71A051 (void);
// 0x00000365 System.Void BNG.GrabbableHaptics::OnRelease()
extern void GrabbableHaptics_OnRelease_mA2C8EF8C254EA3DBA6240DD210DFD7CB7EDE6A93 (void);
// 0x00000366 System.Void BNG.GrabbableHaptics::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHaptics_OnBecomesClosestGrabbable_mFAB2A23E2D1624F043D47815B0048A425A1CDCD4 (void);
// 0x00000367 System.Void BNG.GrabbableHaptics::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHaptics_OnBecomesClosestRemoteGrabbable_mF67D0802E835D8155D49289DEE6CBD126799ABF6 (void);
// 0x00000368 System.Void BNG.GrabbableHaptics::doHaptics(BNG.ControllerHand)
extern void GrabbableHaptics_doHaptics_mDCDA0424680C71B564582FB0B637C1E3BE4A52B4 (void);
// 0x00000369 System.Void BNG.GrabbableHaptics::OnCollisionEnter(UnityEngine.Collision)
extern void GrabbableHaptics_OnCollisionEnter_mE64B9EEA00CCBA0B5C3075C0439D3D92DAE40C59 (void);
// 0x0000036A System.Void BNG.GrabbableHaptics::.ctor()
extern void GrabbableHaptics__ctor_mE068B84CE38B924CE8CFD7342A905691224C2D39 (void);
// 0x0000036B System.Void BNG.GrabbableHighlight::OnGrab(BNG.Grabber)
extern void GrabbableHighlight_OnGrab_m1007CF6D05470B73586BA373B3256CBAE13C93B2 (void);
// 0x0000036C System.Void BNG.GrabbableHighlight::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnBecomesClosestGrabbable_m1A95A8178B943D1443B22C83AC5D5477CDB6705F (void);
// 0x0000036D System.Void BNG.GrabbableHighlight::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnNoLongerClosestGrabbable_m9F648A500ABDAEC06E8F83E63A90EE3FF8590EB4 (void);
// 0x0000036E System.Void BNG.GrabbableHighlight::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnBecomesClosestRemoteGrabbable_mE1015F14C621104BD43DB182BD0ADB4FC31740CE (void);
// 0x0000036F System.Void BNG.GrabbableHighlight::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlight_OnNoLongerClosestRemoteGrabbable_mD7FD1D66D63D0ED0FC525C8D52C8E4BD23F59B01 (void);
// 0x00000370 System.Void BNG.GrabbableHighlight::HighlightItem()
extern void GrabbableHighlight_HighlightItem_mE6F301DFDAEFC849E75769E13B5F437F6E5B731B (void);
// 0x00000371 System.Void BNG.GrabbableHighlight::UnhighlightItem()
extern void GrabbableHighlight_UnhighlightItem_m0FF547BF4491BC93757633B3E4CDDEBD3E4F44CA (void);
// 0x00000372 System.Void BNG.GrabbableHighlight::.ctor()
extern void GrabbableHighlight__ctor_m987C4356A00DCCBFC422B7271B5F328BD2EB69A3 (void);
// 0x00000373 System.Void BNG.GrabbableHighlightMaterial::Start()
extern void GrabbableHighlightMaterial_Start_mB5E864253382767E0FE1C2E50A47011C0FC191AF (void);
// 0x00000374 System.Void BNG.GrabbableHighlightMaterial::OnGrab(BNG.Grabber)
extern void GrabbableHighlightMaterial_OnGrab_mB325CC85EBB8A6E5B25281EF741595E40792AF14 (void);
// 0x00000375 System.Void BNG.GrabbableHighlightMaterial::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnBecomesClosestGrabbable_m0F2BFD390E29160BE704AD98EA1453CAF2195BE0 (void);
// 0x00000376 System.Void BNG.GrabbableHighlightMaterial::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnNoLongerClosestGrabbable_mC5A4C2540873C646806FCBDF7F690DEC2247B8B7 (void);
// 0x00000377 System.Void BNG.GrabbableHighlightMaterial::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnBecomesClosestRemoteGrabbable_m354C395FD96A370645FA997D579F86337EA32F56 (void);
// 0x00000378 System.Void BNG.GrabbableHighlightMaterial::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableHighlightMaterial_OnNoLongerClosestRemoteGrabbable_m753936C06F20E6D419AF2552B4C2B4EC57B41694 (void);
// 0x00000379 System.Void BNG.GrabbableHighlightMaterial::HighlightItem()
extern void GrabbableHighlightMaterial_HighlightItem_mBB397D9A2FD8BCEC774239B7468D406833A43C95 (void);
// 0x0000037A System.Void BNG.GrabbableHighlightMaterial::UnhighlightItem()
extern void GrabbableHighlightMaterial_UnhighlightItem_mCDBF9F4FEF83E66C60701C23DDAB3DF421BC6DBC (void);
// 0x0000037B System.Void BNG.GrabbableHighlightMaterial::.ctor()
extern void GrabbableHighlightMaterial__ctor_mCA74737F36D4B64DA227D9C48AA55C6DC74E2014 (void);
// 0x0000037C System.Void BNG.GrabbableRingHelper::Start()
extern void GrabbableRingHelper_Start_m89D91650357F443715677D7DBBFC0E42B73C4810 (void);
// 0x0000037D System.Void BNG.GrabbableRingHelper::.ctor()
extern void GrabbableRingHelper__ctor_m8280A192D2E0CEB3DB8F716E591AA8C82C6058FB (void);
// 0x0000037E System.Void BNG.GrabPointTrigger::Start()
extern void GrabPointTrigger_Start_mEC96AA3097CB2240FBC78545983CAA5529CC99BE (void);
// 0x0000037F System.Void BNG.GrabPointTrigger::Update()
extern void GrabPointTrigger_Update_m908400E88683C44F04F22AA5A6AB111BD40A1077 (void);
// 0x00000380 System.Void BNG.GrabPointTrigger::UpdateGrabPoint(BNG.GrabPoint)
extern void GrabPointTrigger_UpdateGrabPoint_m6EAE83D2266F823610ED1D4DF643968C4C485DE6 (void);
// 0x00000381 System.Void BNG.GrabPointTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void GrabPointTrigger_OnTriggerEnter_m31C6E967847EB13139FD9D3641A53B14176FF5B5 (void);
// 0x00000382 System.Void BNG.GrabPointTrigger::OnTriggerExit(UnityEngine.Collider)
extern void GrabPointTrigger_OnTriggerExit_m743CFC285B50019F37518AAAA24F2B79F8992B0B (void);
// 0x00000383 System.Void BNG.GrabPointTrigger::setGrabber(BNG.Grabber)
extern void GrabPointTrigger_setGrabber_m47B41C158C6316EB8C6B2D45C0FBB58690C9ECF5 (void);
// 0x00000384 System.Void BNG.GrabPointTrigger::ReleaseGrabber()
extern void GrabPointTrigger_ReleaseGrabber_m8306CB460C423DB77E80F8BC162CDC5CB7218D14 (void);
// 0x00000385 System.Void BNG.GrabPointTrigger::.ctor()
extern void GrabPointTrigger__ctor_m0471170C6C07C9739F91FD4BCC60D9429654F9AE (void);
// 0x00000386 System.Void BNG.LineToTransform::Start()
extern void LineToTransform_Start_m8286C2D8B6FAFC3E08A8B099207B46AB0BE79064 (void);
// 0x00000387 System.Void BNG.LineToTransform::LateUpdate()
extern void LineToTransform_LateUpdate_m6459380B1536ED84621C708544CDEB544FE30452 (void);
// 0x00000388 System.Void BNG.LineToTransform::UpdateLine()
extern void LineToTransform_UpdateLine_m3D9C76EDBC4469F81E9CB62846EB2131932C8723 (void);
// 0x00000389 System.Void BNG.LineToTransform::OnDrawGizmos()
extern void LineToTransform_OnDrawGizmos_m18EC7BDE763DAA6F62AA4EF494EE38CE9FEC9B97 (void);
// 0x0000038A System.Void BNG.LineToTransform::.ctor()
extern void LineToTransform__ctor_m1E6F978DFC78C2B31CEC9946FD03E5786CCECCD8 (void);
// 0x0000038B System.Void BNG.LookAtTransform::Update()
extern void LookAtTransform_Update_m4FFCB9239285D9AE9C39151499C1AF77A959D7DF (void);
// 0x0000038C System.Void BNG.LookAtTransform::LateUpdate()
extern void LookAtTransform_LateUpdate_m396F34BE195131626C5FD3D972CE854A1AB71D10 (void);
// 0x0000038D System.Void BNG.LookAtTransform::lookAt()
extern void LookAtTransform_lookAt_m4A9F640EEFCF15C4C3A5755BFAD2C8D3A400A5CD (void);
// 0x0000038E System.Void BNG.LookAtTransform::.ctor()
extern void LookAtTransform__ctor_m156B84A52A633C83347FE1212706947B7F2C1B9D (void);
// 0x0000038F System.Void BNG.PlaySoundOnGrab::OnGrab(BNG.Grabber)
extern void PlaySoundOnGrab_OnGrab_m620659C1A6A8A9D2D241D8B426A4D1170D6F9C1D (void);
// 0x00000390 System.Void BNG.PlaySoundOnGrab::.ctor()
extern void PlaySoundOnGrab__ctor_m9C859F36210B3A0723BAE0D5697560C6A8A71524 (void);
// 0x00000391 System.Void BNG.PunctureCollider::Start()
extern void PunctureCollider_Start_mAA9A5474461BA993284C8C15774D7BDFC02E31F6 (void);
// 0x00000392 System.Void BNG.PunctureCollider::FixedUpdate()
extern void PunctureCollider_FixedUpdate_mB32D20C3C71707D8755443702560845B3E0E1883 (void);
// 0x00000393 System.Void BNG.PunctureCollider::UpdatePunctureValue()
extern void PunctureCollider_UpdatePunctureValue_m5B83D98DF912194DA12D41F9E429345DC2EC1C83 (void);
// 0x00000394 System.Void BNG.PunctureCollider::MovePunctureUp()
extern void PunctureCollider_MovePunctureUp_m33ABDFA2DFB980BD526E3123A38DB564A49752F8 (void);
// 0x00000395 System.Void BNG.PunctureCollider::MovePunctureDown()
extern void PunctureCollider_MovePunctureDown_mB1558A30E372466E5898C6700B6A6664E6C337CA (void);
// 0x00000396 System.Void BNG.PunctureCollider::CheckBreakDistance()
extern void PunctureCollider_CheckBreakDistance_mEF21698DD5FD5AFF1D618628011B97A0FA638D51 (void);
// 0x00000397 System.Void BNG.PunctureCollider::CheckPunctureRelease()
extern void PunctureCollider_CheckPunctureRelease_m25CEDA062B8F873F5CE04DDA37E53523A59878AD (void);
// 0x00000398 System.Void BNG.PunctureCollider::AdjustJointMass()
extern void PunctureCollider_AdjustJointMass_mE6F4BCEA30552A6CA2179E500FE6534781F5158B (void);
// 0x00000399 System.Void BNG.PunctureCollider::ApplyResistanceForce()
extern void PunctureCollider_ApplyResistanceForce_m80FC1F8941C0403CB84BB3735B9D8CA7AEC3A8A2 (void);
// 0x0000039A System.Void BNG.PunctureCollider::DoPuncture(UnityEngine.Collider,UnityEngine.Vector3)
extern void PunctureCollider_DoPuncture_m757BB2B1A4A6845FCA7F61B5A4FD34AC06458115 (void);
// 0x0000039B System.Void BNG.PunctureCollider::SetPenetration(System.Single)
extern void PunctureCollider_SetPenetration_m04968869D12F3F4CE8BFF599B68B2798EBC42E26 (void);
// 0x0000039C System.Void BNG.PunctureCollider::ReleasePuncture()
extern void PunctureCollider_ReleasePuncture_mCA96E2D7FB9F919026A2C1091F1E0C52DD961880 (void);
// 0x0000039D System.Boolean BNG.PunctureCollider::CanPunctureObject(UnityEngine.GameObject)
extern void PunctureCollider_CanPunctureObject_mA892F4D38F17822D455EDEC5F93660AD637288AB (void);
// 0x0000039E System.Void BNG.PunctureCollider::OnCollisionEnter(UnityEngine.Collision)
extern void PunctureCollider_OnCollisionEnter_m048DCC12B182C72D2B75E63F6D1E57E60ABEC62C (void);
// 0x0000039F System.Void BNG.PunctureCollider::.ctor()
extern void PunctureCollider__ctor_m34FDCED85315549A0348DABAAB1AC1DC36980DD9 (void);
// 0x000003A0 System.Void BNG.ReturnToSnapZone::Start()
extern void ReturnToSnapZone_Start_m684139D3B2BB571D5486DCCA1CDD0BFB095FD5A5 (void);
// 0x000003A1 System.Void BNG.ReturnToSnapZone::Update()
extern void ReturnToSnapZone_Update_m08CAAA0D8698A3E45DF081DE21577FA98212320B (void);
// 0x000003A2 System.Void BNG.ReturnToSnapZone::moveToSnapZone()
extern void ReturnToSnapZone_moveToSnapZone_m5E3761692D8A728E7D491B9F1FCD7F99A779C753 (void);
// 0x000003A3 System.Void BNG.ReturnToSnapZone::.ctor()
extern void ReturnToSnapZone__ctor_m57B90DA376011F73D778E732C4D785FE3463FF40 (void);
// 0x000003A4 System.Void BNG.RotateTowards::Update()
extern void RotateTowards_Update_m4BA61079B9006BC99C91F0C039CCCFB420AE83CB (void);
// 0x000003A5 System.Void BNG.RotateTowards::.ctor()
extern void RotateTowards__ctor_m386D3EEDB7ED85012D955727A2BB726BF0638C49 (void);
// 0x000003A6 System.Void BNG.RotateWithHMD::Start()
extern void RotateWithHMD_Start_m09F20AC0C5C98B97D3EEDB8642BA9ADC6091F576 (void);
// 0x000003A7 System.Void BNG.RotateWithHMD::LateUpdate()
extern void RotateWithHMD_LateUpdate_mA89BE3ED774F9558A7A5B66179F273D3F2250F75 (void);
// 0x000003A8 System.Void BNG.RotateWithHMD::UpdatePosition()
extern void RotateWithHMD_UpdatePosition_m0B512FDBC70EA87102AB1A3C1A79911E6619918D (void);
// 0x000003A9 System.Void BNG.RotateWithHMD::.ctor()
extern void RotateWithHMD__ctor_m723FACC9F67608168090DF84852BC197697C8472 (void);
// 0x000003AA System.Void BNG.ScaleBetweenPoints::Update()
extern void ScaleBetweenPoints_Update_mF3178776C587EBEFE1464CC9BC4B4926AA1EE50D (void);
// 0x000003AB System.Void BNG.ScaleBetweenPoints::LateUpdate()
extern void ScaleBetweenPoints_LateUpdate_mE9C3F17E522BC41C3A72CA5F29BE7D7F88D8600B (void);
// 0x000003AC System.Void BNG.ScaleBetweenPoints::doScale()
extern void ScaleBetweenPoints_doScale_m473720A6EE5C5B0FAF50117244F9E43E2B4EEDF8 (void);
// 0x000003AD System.Void BNG.ScaleBetweenPoints::.ctor()
extern void ScaleBetweenPoints__ctor_mC077DFB6E85BFD134EA0DBDBE3C716B2A3243A04 (void);
// 0x000003AE System.Void BNG.ScreenFader::Awake()
extern void ScreenFader_Awake_mFBEA23FF00BFB040351ED4F86380E33D9A7D88A2 (void);
// 0x000003AF System.Void BNG.ScreenFader::initialize()
extern void ScreenFader_initialize_m431121577A642714C65D9096615B0EAC5037F170 (void);
// 0x000003B0 System.Void BNG.ScreenFader::OnEnable()
extern void ScreenFader_OnEnable_m6A362F2D0E13B0D58F63521A805BADC766B372E4 (void);
// 0x000003B1 System.Void BNG.ScreenFader::OnDisable()
extern void ScreenFader_OnDisable_mDEA5F108C9D216D6D0F9A4AE9010B6ED5C126398 (void);
// 0x000003B2 System.Void BNG.ScreenFader::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void ScreenFader_OnSceneLoaded_m95DDFAB82D3F2FDF756C6B040BC1F7B3E98E79C8 (void);
// 0x000003B3 System.Collections.IEnumerator BNG.ScreenFader::fadeOutWithDelay(System.Single)
extern void ScreenFader_fadeOutWithDelay_m53951A18F2152E8B1E0F1951A2523DAB2A756D20 (void);
// 0x000003B4 System.Void BNG.ScreenFader::DoFadeIn()
extern void ScreenFader_DoFadeIn_mC07232114D4939255F1D654402DCF6C279302723 (void);
// 0x000003B5 System.Void BNG.ScreenFader::DoFadeOut()
extern void ScreenFader_DoFadeOut_m43AD39BE869DEE70983EF4B39FC0151A2EF01FA7 (void);
// 0x000003B6 System.Void BNG.ScreenFader::SetFadeLevel(System.Single)
extern void ScreenFader_SetFadeLevel_m042672C736D5015E3E4A97D249C96F61D0AF4F93 (void);
// 0x000003B7 System.Collections.IEnumerator BNG.ScreenFader::doFade(System.Single,System.Single)
extern void ScreenFader_doFade_m3E285F49F89908024BDAE0F053AFB57961A506A8 (void);
// 0x000003B8 System.Void BNG.ScreenFader::updateImageAlpha(System.Single)
extern void ScreenFader_updateImageAlpha_mA5F6B6D255BDE8ECEA3CF27C64AD3CB0E9EC3AAE (void);
// 0x000003B9 System.Void BNG.ScreenFader::.ctor()
extern void ScreenFader__ctor_m89793487BB8F1D2AB01A65C56993AB7BAADFDAED (void);
// 0x000003BA System.Void BNG.ScreenFader/<fadeOutWithDelay>d__17::.ctor(System.Int32)
extern void U3CfadeOutWithDelayU3Ed__17__ctor_m0783290B8F5D44EA9845212C9310F78765D4E9E4 (void);
// 0x000003BB System.Void BNG.ScreenFader/<fadeOutWithDelay>d__17::System.IDisposable.Dispose()
extern void U3CfadeOutWithDelayU3Ed__17_System_IDisposable_Dispose_mD9137679F58B4DD95181EEA4FDDEDCACDC553900 (void);
// 0x000003BC System.Boolean BNG.ScreenFader/<fadeOutWithDelay>d__17::MoveNext()
extern void U3CfadeOutWithDelayU3Ed__17_MoveNext_m56EAF744D3A1841F297CDB7392893CA1E50319D6 (void);
// 0x000003BD System.Object BNG.ScreenFader/<fadeOutWithDelay>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CfadeOutWithDelayU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m097EC0B0E7F7C1BD449AE4A65A2D0EBF7DC4C724 (void);
// 0x000003BE System.Void BNG.ScreenFader/<fadeOutWithDelay>d__17::System.Collections.IEnumerator.Reset()
extern void U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_Reset_mDEE33FD796CFC26C9FB4F8682E7B86568959C357 (void);
// 0x000003BF System.Object BNG.ScreenFader/<fadeOutWithDelay>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_get_Current_mBB5F6F8948BFD52938304D68962FBC5EB95ACCD2 (void);
// 0x000003C0 System.Void BNG.ScreenFader/<doFade>d__21::.ctor(System.Int32)
extern void U3CdoFadeU3Ed__21__ctor_m0EF25937570F9BFE4B01F9E86C0E3C759A1B3915 (void);
// 0x000003C1 System.Void BNG.ScreenFader/<doFade>d__21::System.IDisposable.Dispose()
extern void U3CdoFadeU3Ed__21_System_IDisposable_Dispose_m1134553ECACC5F99C96D0C23C7BE4CC071354522 (void);
// 0x000003C2 System.Boolean BNG.ScreenFader/<doFade>d__21::MoveNext()
extern void U3CdoFadeU3Ed__21_MoveNext_m49CF2CFFBFCF58DE026E503C4C731EB6B41FC399 (void);
// 0x000003C3 System.Object BNG.ScreenFader/<doFade>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoFadeU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B7FCDF797CE5D2C923EEA7964B24E79B39C7910 (void);
// 0x000003C4 System.Void BNG.ScreenFader/<doFade>d__21::System.Collections.IEnumerator.Reset()
extern void U3CdoFadeU3Ed__21_System_Collections_IEnumerator_Reset_mECF69CF3CB1269B1834EAAD41D9F690E7D188F20 (void);
// 0x000003C5 System.Object BNG.ScreenFader/<doFade>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CdoFadeU3Ed__21_System_Collections_IEnumerator_get_Current_m678822C431A5AE4415DF5C2FE0FCF7104467F101 (void);
// 0x000003C6 System.Void BNG.SnapZoneRingHelper::Start()
extern void SnapZoneRingHelper_Start_mB1DD77D8E8019698CF71C26F0F2A7AAF0E67F1D2 (void);
// 0x000003C7 System.Void BNG.SnapZoneRingHelper::Update()
extern void SnapZoneRingHelper_Update_m61176AA35113D533B379E9426CBC0AF6AA39263C (void);
// 0x000003C8 System.Boolean BNG.SnapZoneRingHelper::checkIsValidSnap()
extern void SnapZoneRingHelper_checkIsValidSnap_mC107FF00CE560244A4AEE1A3654D0E7BFFD128BD (void);
// 0x000003C9 System.Void BNG.SnapZoneRingHelper::.ctor()
extern void SnapZoneRingHelper__ctor_mC3AFC5A0A2188776A8CCAC63EA71B98A7B3C1205 (void);
// 0x000003CA System.Void BNG.Tooltip::Start()
extern void Tooltip_Start_mDA858ED921604E444B5FA5B0B6B2174B2598B495 (void);
// 0x000003CB System.Void BNG.Tooltip::Update()
extern void Tooltip_Update_mC93EA8B2F37017C93BD91D5AE9F6DEDD01CB34D9 (void);
// 0x000003CC System.Void BNG.Tooltip::UpdateTooltipPosition()
extern void Tooltip_UpdateTooltipPosition_m383A805D317F65CE7C55FA6ED04F893D265B7C53 (void);
// 0x000003CD System.Void BNG.Tooltip::.ctor()
extern void Tooltip__ctor_m221DA5EA4042FA4485400C3531A13CB4222C8882 (void);
// 0x000003CE System.Void BNG.VelocityTracker::Start()
extern void VelocityTracker_Start_mCB78D075052557F3A40569B485C0E14D8CDFA6F2 (void);
// 0x000003CF System.Void BNG.VelocityTracker::FixedUpdate()
extern void VelocityTracker_FixedUpdate_mFC66EA58877D11CCF5D94F9039E923E4BF2BD164 (void);
// 0x000003D0 System.Void BNG.VelocityTracker::UpdateVelocities()
extern void VelocityTracker_UpdateVelocities_m322E72CC27DA11D596DA4FB587515007D6506392 (void);
// 0x000003D1 System.Void BNG.VelocityTracker::UpdateVelocity()
extern void VelocityTracker_UpdateVelocity_m862DDDED1F081EA8B79B007D3A3796D5DAA7BB96 (void);
// 0x000003D2 System.Void BNG.VelocityTracker::UpdateAngularVelocity()
extern void VelocityTracker_UpdateAngularVelocity_mBA86A767624FCDAC8271216028EC11E8BA8F9F53 (void);
// 0x000003D3 UnityEngine.Vector3 BNG.VelocityTracker::GetVelocity()
extern void VelocityTracker_GetVelocity_m7A8692F37D0E9A97BF083493054C56AFC0328600 (void);
// 0x000003D4 UnityEngine.Vector3 BNG.VelocityTracker::GetAveragedVelocity()
extern void VelocityTracker_GetAveragedVelocity_mAF452490E36912E0EC202E4914D82EAE562E797F (void);
// 0x000003D5 UnityEngine.Vector3 BNG.VelocityTracker::GetAngularVelocity()
extern void VelocityTracker_GetAngularVelocity_m085429A90A9EB6C446461D10BB94F928074D91AF (void);
// 0x000003D6 UnityEngine.Vector3 BNG.VelocityTracker::GetAveragedAngularVelocity()
extern void VelocityTracker_GetAveragedAngularVelocity_m88ED73DA87BC7263E7DC9CFB5E36A343CC44BEA0 (void);
// 0x000003D7 UnityEngine.Vector3 BNG.VelocityTracker::GetAveragedVector(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void VelocityTracker_GetAveragedVector_mE85850F479A56C5E9B804D10C257DFCA50FEA386 (void);
// 0x000003D8 System.Void BNG.VelocityTracker::.ctor()
extern void VelocityTracker__ctor_mB68141CBCD3E0689FC1EACC01AC3D5149BEB93B1 (void);
// 0x000003D9 System.Void BNG.BNGPlayerController::Start()
extern void BNGPlayerController_Start_m6EDBDD17752CCD6B2C5248E5135F49B2CE69905B (void);
// 0x000003DA System.Void BNG.BNGPlayerController::Update()
extern void BNGPlayerController_Update_m20473CB812FE8FDAD5FFB3662056FBAA15910C4E (void);
// 0x000003DB System.Void BNG.BNGPlayerController::FixedUpdate()
extern void BNGPlayerController_FixedUpdate_mB1EB011C83E5752645F0573F7B6E10D15A13456C (void);
// 0x000003DC System.Void BNG.BNGPlayerController::CheckPlayerElevationRespawn()
extern void BNGPlayerController_CheckPlayerElevationRespawn_m2A9597AB63A47FE58D3BD47070A180BA8B1924F3 (void);
// 0x000003DD System.Void BNG.BNGPlayerController::UpdateDistanceFromGround()
extern void BNGPlayerController_UpdateDistanceFromGround_m578B1D5175D1FF660940B7FD8A091207CB26C1C9 (void);
// 0x000003DE System.Void BNG.BNGPlayerController::RotateTrackingSpaceToCamera()
extern void BNGPlayerController_RotateTrackingSpaceToCamera_m495686B2227222E0CB5DF751BCB3D9546BFBD6ED (void);
// 0x000003DF System.Void BNG.BNGPlayerController::UpdateCameraRigPosition()
extern void BNGPlayerController_UpdateCameraRigPosition_m56A6E6A93A4FB974AC3B4B2DF46561108523C375 (void);
// 0x000003E0 System.Void BNG.BNGPlayerController::UpdateCharacterHeight()
extern void BNGPlayerController_UpdateCharacterHeight_mE20AEDBFA50913B3112761F4EA02B6BD6A0567A4 (void);
// 0x000003E1 System.Void BNG.BNGPlayerController::UpdateCameraHeight()
extern void BNGPlayerController_UpdateCameraHeight_m22E91B9FFBDF0C5DBC4844DA207375B4BE771984 (void);
// 0x000003E2 System.Void BNG.BNGPlayerController::CheckCharacterCollisionMove()
extern void BNGPlayerController_CheckCharacterCollisionMove_m689BBBDC605822CEF6D49F8C4CB9BEA910BF9340 (void);
// 0x000003E3 System.Void BNG.BNGPlayerController::CheckRigidbodyCapsuleMove(UnityEngine.Vector3)
extern void BNGPlayerController_CheckRigidbodyCapsuleMove_mB7E084B62CBDB9CC6AD6D7EEE7A47D0ED806EE7B (void);
// 0x000003E4 System.Boolean BNG.BNGPlayerController::IsGrounded()
extern void BNGPlayerController_IsGrounded_m5CD2FF88A68DFB712B6CA47599655A038D1A6D7A (void);
// 0x000003E5 System.Void BNG.BNGPlayerController::OnClimbingChange()
extern void BNGPlayerController_OnClimbingChange_mBAACC863919E35207DEAAC58E7072DB7CCF96A94 (void);
// 0x000003E6 System.Void BNG.BNGPlayerController::.ctor()
extern void BNGPlayerController__ctor_m914FA631CF3FF5B3FE5D1A6376A1405C61ADDF84 (void);
// 0x000003E7 System.Void BNG.Button::Start()
extern void Button_Start_mA14EEEAB2816FB74E88B9D5ED95D412C8AC0DE1D (void);
// 0x000003E8 System.Void BNG.Button::Update()
extern void Button_Update_m6B1D87839FB5B2B5DF9A49C709B05CC13BF10EE7 (void);
// 0x000003E9 UnityEngine.Vector3 BNG.Button::GetButtonUpPosition()
extern void Button_GetButtonUpPosition_m90F904314D0BA76F9C9446579F5AD27A49A07F01 (void);
// 0x000003EA UnityEngine.Vector3 BNG.Button::GetButtonDownPosition()
extern void Button_GetButtonDownPosition_m3A85D9C7C84B518315EC900C7F6B219A0A2FE4EF (void);
// 0x000003EB System.Void BNG.Button::OnButtonDown()
extern void Button_OnButtonDown_m73D79A1537F3245E5016D725AF7F61BB4739176C (void);
// 0x000003EC System.Void BNG.Button::OnButtonUp()
extern void Button_OnButtonUp_m68BDC9B1A9B1BFCBA8CEA00D8754B64B68324BB8 (void);
// 0x000003ED System.Void BNG.Button::OnTriggerEnter(UnityEngine.Collider)
extern void Button_OnTriggerEnter_m96231825E1BEBDD4F1A4B1A04F372991D968C990 (void);
// 0x000003EE System.Void BNG.Button::OnTriggerExit(UnityEngine.Collider)
extern void Button_OnTriggerExit_m2D8A06688B9A3071E61ED183A1166DFE646130EE (void);
// 0x000003EF System.Void BNG.Button::OnDrawGizmosSelected()
extern void Button_OnDrawGizmosSelected_m32D1D52E748ADE78130A55F939FAD2C5D904A58F (void);
// 0x000003F0 System.Void BNG.Button::.ctor()
extern void Button__ctor_m17AD44130FC0B8C753C07E65730CCCFC04AA390F (void);
// 0x000003F1 System.Void BNG.ControllerModelSelector::OnEnable()
extern void ControllerModelSelector_OnEnable_m1CEB672F47478EF99E20B9BA7B96F0A1F9D0590D (void);
// 0x000003F2 System.Void BNG.ControllerModelSelector::UpdateControllerModel()
extern void ControllerModelSelector_UpdateControllerModel_m4BC219B9F58619AC59EAD57A2E26737EE1FF0C17 (void);
// 0x000003F3 System.Void BNG.ControllerModelSelector::EnableChildController(System.Int32)
extern void ControllerModelSelector_EnableChildController_m0E3B7C085CA3AB2C9088B74FCA257716831CAC68 (void);
// 0x000003F4 System.Void BNG.ControllerModelSelector::OnDisable()
extern void ControllerModelSelector_OnDisable_m62D25AE6B82D7F716C5A27E9B69E91CD663E3B5E (void);
// 0x000003F5 System.Void BNG.ControllerModelSelector::OnApplicationQuit()
extern void ControllerModelSelector_OnApplicationQuit_m16F8BA315E31F3A5FDD3171FB349BF4E14BB4D0E (void);
// 0x000003F6 System.Void BNG.ControllerModelSelector::.ctor()
extern void ControllerModelSelector__ctor_m1AC85F7E5617A6416CE1979243D102D70526EB4B (void);
// 0x000003F7 System.Boolean BNG.Grabbable::get_BeingHeldWithTwoHands()
extern void Grabbable_get_BeingHeldWithTwoHands_mC18CCBC40B9AC5F770BDEBC2304B575FDEA25AE2 (void);
// 0x000003F8 System.Collections.Generic.List`1<BNG.Grabber> BNG.Grabbable::get_HeldByGrabbers()
extern void Grabbable_get_HeldByGrabbers_mE545511C8BFF53688B9ECF33D8A1B70425F86D6F (void);
// 0x000003F9 System.Boolean BNG.Grabbable::get_RemoteGrabbing()
extern void Grabbable_get_RemoteGrabbing_m57E2C699C07A06750C1DDF062B6BD541A158F8C7 (void);
// 0x000003FA UnityEngine.Vector3 BNG.Grabbable::get_OriginalScale()
extern void Grabbable_get_OriginalScale_m62D7A8D44E23ED0A9C83B1BAFB360A7602B74374 (void);
// 0x000003FB System.Void BNG.Grabbable::set_OriginalScale(UnityEngine.Vector3)
extern void Grabbable_set_OriginalScale_mE3925D35C030F8D3F87A0E5816D2EA1EDFF82A84 (void);
// 0x000003FC System.Single BNG.Grabbable::get_lastCollisionSeconds()
extern void Grabbable_get_lastCollisionSeconds_mA62EC9A7CD525C6B70FC2A2C82697E00FCD6BEBF (void);
// 0x000003FD System.Void BNG.Grabbable::set_lastCollisionSeconds(System.Single)
extern void Grabbable_set_lastCollisionSeconds_m247A4E5CA70448320002CF49E370E66B2C1D80CC (void);
// 0x000003FE System.Single BNG.Grabbable::get_lastNoCollisionSeconds()
extern void Grabbable_get_lastNoCollisionSeconds_m7DA91F99CE1FFF88AC7626C8E8541B0AEBAFFFD9 (void);
// 0x000003FF System.Void BNG.Grabbable::set_lastNoCollisionSeconds(System.Single)
extern void Grabbable_set_lastNoCollisionSeconds_m83842BABB5A49F0D9C5348A166A2BDBC61B79D05 (void);
// 0x00000400 System.Boolean BNG.Grabbable::get_RecentlyCollided()
extern void Grabbable_get_RecentlyCollided_m8F523BD48F54A127E4D717F0E1343E5FD991786B (void);
// 0x00000401 System.Single BNG.Grabbable::get_requestSpringTime()
extern void Grabbable_get_requestSpringTime_m173B51A48CBB098BC2CBCA0A41275DA005678870 (void);
// 0x00000402 System.Void BNG.Grabbable::set_requestSpringTime(System.Single)
extern void Grabbable_set_requestSpringTime_m9893F9325AED1E9A35967B39D4808BCC9F298565 (void);
// 0x00000403 UnityEngine.Vector3 BNG.Grabbable::get_grabPosition()
extern void Grabbable_get_grabPosition_mFCA90DBABB3F7BA17D81AC722A00750772317C10 (void);
// 0x00000404 UnityEngine.Vector3 BNG.Grabbable::get_GrabPositionOffset()
extern void Grabbable_get_GrabPositionOffset_m335314B6B9C382A0946840A7E045F2DCA44E6180 (void);
// 0x00000405 UnityEngine.Vector3 BNG.Grabbable::get_GrabRotationOffset()
extern void Grabbable_get_GrabRotationOffset_m52679902E615DEED3781E7F06599C3E2094F9CA3 (void);
// 0x00000406 UnityEngine.Transform BNG.Grabbable::get_grabTransform()
extern void Grabbable_get_grabTransform_mC7EC3681D1E1B89F3C7F70F8E012CC6209FB8992 (void);
// 0x00000407 UnityEngine.Transform BNG.Grabbable::get_grabTransformSecondary()
extern void Grabbable_get_grabTransformSecondary_m1EACE30F9FB97AD855DBCDC18C2B938CEF69AF4F (void);
// 0x00000408 System.Boolean BNG.Grabbable::get_CanBeMoved()
extern void Grabbable_get_CanBeMoved_mC711A75961F68542A92322BEDF8742BD6E207439 (void);
// 0x00000409 BNG.BNGPlayerController BNG.Grabbable::get_player()
extern void Grabbable_get_player_m2A56382D2CF331C0261CC5FB7F8AAF3FAAECB365 (void);
// 0x0000040A BNG.Grabber BNG.Grabbable::get_FlyingToGrabber()
extern void Grabbable_get_FlyingToGrabber_mCD936AAE88112B52238C5A8934A500BADB45B5EA (void);
// 0x0000040B System.Boolean BNG.Grabbable::get_DidParentHands()
extern void Grabbable_get_DidParentHands_m5D20F8F41E0376B84A18211BD0EC0CBB1B8E1630 (void);
// 0x0000040C System.Void BNG.Grabbable::Awake()
extern void Grabbable_Awake_m8613B3E5DE0448B9FA77A424431262896088614C (void);
// 0x0000040D System.Void BNG.Grabbable::Update()
extern void Grabbable_Update_m3DA69708B84A8C900D27D84FFECBBD5E801847D7 (void);
// 0x0000040E System.Void BNG.Grabbable::FixedUpdate()
extern void Grabbable_FixedUpdate_m544B6AA86BA5688CFDF4A7CBA2AFD0E028E6B50B (void);
// 0x0000040F UnityEngine.Vector3 BNG.Grabbable::GetGrabberWithGrabPointOffset(BNG.Grabber,UnityEngine.Transform)
extern void Grabbable_GetGrabberWithGrabPointOffset_mFF0B39CEB08A3594AADD3386510901191580A497 (void);
// 0x00000410 UnityEngine.Quaternion BNG.Grabbable::GetGrabberWithOffsetWorldRotation(BNG.Grabber)
extern void Grabbable_GetGrabberWithOffsetWorldRotation_m7C402D851DD472E2B2AB4388002B548E6B3A0E44 (void);
// 0x00000411 System.Void BNG.Grabbable::positionHandGraphics(BNG.Grabber)
extern void Grabbable_positionHandGraphics_m443B79E6F8C82F1702443423B9F87CB22B7B3C7B (void);
// 0x00000412 System.Boolean BNG.Grabbable::IsGrabbable()
extern void Grabbable_IsGrabbable_m8DCFCA00877BA811D7C3712137D24A72AEF00E86 (void);
// 0x00000413 System.Void BNG.Grabbable::UpdateRemoteGrab()
extern void Grabbable_UpdateRemoteGrab_m4319AA4E71F6C191A9343A8EEC17E0723A798F49 (void);
// 0x00000414 System.Void BNG.Grabbable::CheckRemoteGrabLinear()
extern void Grabbable_CheckRemoteGrabLinear_m1A97B016176E6815E60F7289D6525418816E7611 (void);
// 0x00000415 System.Void BNG.Grabbable::CheckRemoteGrabVelocity()
extern void Grabbable_CheckRemoteGrabVelocity_m7A0E4917A8B5533E974DE228586017BE0A70C69C (void);
// 0x00000416 System.Void BNG.Grabbable::InitiateFlick()
extern void Grabbable_InitiateFlick_mE654EF5AD24B11FC8BF96DC80E50E9D568C75124 (void);
// 0x00000417 UnityEngine.Vector3 BNG.Grabbable::GetVelocityToHitTargetByTime(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Grabbable_GetVelocityToHitTargetByTime_m369C6FB2E90B62DF8D3DD6423B98F77E36A74EE0 (void);
// 0x00000418 System.Void BNG.Grabbable::CheckRemoteGrabFlick()
extern void Grabbable_CheckRemoteGrabFlick_m1144E6D457DA70D14987D666FB4F74938B4E466D (void);
// 0x00000419 System.Void BNG.Grabbable::UpdateFixedJoints()
extern void Grabbable_UpdateFixedJoints_mAFA24656A763CA61C7A14538F7167513C293F4CC (void);
// 0x0000041A System.Void BNG.Grabbable::UpdatePhysicsJoints()
extern void Grabbable_UpdatePhysicsJoints_m378607DF0E6AF2C1ABE277548004D467E3716C4A (void);
// 0x0000041B System.Void BNG.Grabbable::setPositionSpring(System.Single,System.Single)
extern void Grabbable_setPositionSpring_mC78CDEBC62E6DA40CF754580A790DA3EA89659E5 (void);
// 0x0000041C System.Void BNG.Grabbable::setSlerpDrive(System.Single,System.Single)
extern void Grabbable_setSlerpDrive_m6DBB17C5883C5443CE2EA52212756AC327B432DC (void);
// 0x0000041D UnityEngine.Vector3 BNG.Grabbable::GetGrabberVector3(BNG.Grabber,System.Boolean)
extern void Grabbable_GetGrabberVector3_mE3681753AE82CCB5892999850BD4BED9CC104209 (void);
// 0x0000041E UnityEngine.Quaternion BNG.Grabbable::GetGrabberQuaternion(BNG.Grabber,System.Boolean)
extern void Grabbable_GetGrabberQuaternion_mBAF84AB737154C95B3A17E7F9D8248AAE1F4D99F (void);
// 0x0000041F System.Void BNG.Grabbable::moveWithVelocity()
extern void Grabbable_moveWithVelocity_mE2F33FD8767EBF2D4CA2F4C7CC217933EF30804D (void);
// 0x00000420 System.Void BNG.Grabbable::rotateWithVelocity()
extern void Grabbable_rotateWithVelocity_mE0EED11ABE78A8DD7580215B157279F39BFAEF41 (void);
// 0x00000421 UnityEngine.Vector3 BNG.Grabbable::GetGrabbersAveragedPosition()
extern void Grabbable_GetGrabbersAveragedPosition_mE32868EFC544B300510958968312BC571D48EAF2 (void);
// 0x00000422 UnityEngine.Quaternion BNG.Grabbable::GetGrabbersAveragedRotation()
extern void Grabbable_GetGrabbersAveragedRotation_m912C277D9F6A1783088F03E6468EF19B2BC3A115 (void);
// 0x00000423 System.Void BNG.Grabbable::UpdateKinematicPhysics()
extern void Grabbable_UpdateKinematicPhysics_mDCD7C9C19A83E73901E5B6FFFB779CDDED041AB0 (void);
// 0x00000424 System.Void BNG.Grabbable::UpdateVelocityPhysics()
extern void Grabbable_UpdateVelocityPhysics_mD3ACAAD7CAE06AFBDDF50BFA8DF5530D61FEEE39 (void);
// 0x00000425 System.Void BNG.Grabbable::checkParentHands(BNG.Grabber)
extern void Grabbable_checkParentHands_m1FA00F63137ACABB60E905B63CF3674563B2304F (void);
// 0x00000426 System.Boolean BNG.Grabbable::canBeMoved()
extern void Grabbable_canBeMoved_m9166115CBD421F06FDD6F0AE99D42F4DFA8434F5 (void);
// 0x00000427 System.Void BNG.Grabbable::checkSecondaryLook()
extern void Grabbable_checkSecondaryLook_mBB63E80172DDEF59FADA6D75D35D507F69107095 (void);
// 0x00000428 System.Void BNG.Grabbable::rotateGrabber(System.Boolean)
extern void Grabbable_rotateGrabber_m8DE0E7671A4FB60E4223F7ED6606F05754DCC5B7 (void);
// 0x00000429 UnityEngine.Transform BNG.Grabbable::GetGrabPoint()
extern void Grabbable_GetGrabPoint_m9430BEBA2EE684508B2F0C21E02FB5641A958535 (void);
// 0x0000042A System.Void BNG.Grabbable::GrabItem(BNG.Grabber)
extern void Grabbable_GrabItem_m23150BBF0F3A40C53389D9768745D4C3C3026EB5 (void);
// 0x0000042B System.Void BNG.Grabbable::setupConfigJointGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupConfigJointGrab_mFA2DFD0D954A1AFD469880CDA4DF76D53C2F6157 (void);
// 0x0000042C System.Void BNG.Grabbable::setupFixedJointGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupFixedJointGrab_m814B344CA1E8801AF9F294AB8C993F1220AC88D7 (void);
// 0x0000042D System.Void BNG.Grabbable::setupKinematicGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupKinematicGrab_m29834371E98B9CC5E1C704CD725718BD186D9EE1 (void);
// 0x0000042E System.Void BNG.Grabbable::setupVelocityGrab(BNG.Grabber,BNG.GrabType)
extern void Grabbable_setupVelocityGrab_mF7C687CE0BF79B495FF1AF4E52F08B8BD46B2076 (void);
// 0x0000042F System.Void BNG.Grabbable::GrabRemoteItem(BNG.Grabber)
extern void Grabbable_GrabRemoteItem_m4BA3581762428C7AD2F632D832A5ECE0DC24B8CA (void);
// 0x00000430 System.Void BNG.Grabbable::ResetGrabbing()
extern void Grabbable_ResetGrabbing_mFD0B9740C847D32312376AD2704FEA56155C1CA3 (void);
// 0x00000431 System.Void BNG.Grabbable::DropItem(BNG.Grabber,System.Boolean,System.Boolean)
extern void Grabbable_DropItem_m0F47D9C7A603DA97F801A86512C14B87E00F1F08 (void);
// 0x00000432 System.Void BNG.Grabbable::clearLookAtTransform()
extern void Grabbable_clearLookAtTransform_m0387FDD6740102691EFA1DF230A26BE995BB412F (void);
// 0x00000433 System.Void BNG.Grabbable::callEvents(BNG.Grabber)
extern void Grabbable_callEvents_mD8E194F7ACAF616B7CAA5C419C5E0BB26B897FF6 (void);
// 0x00000434 System.Void BNG.Grabbable::DropItem(BNG.Grabber)
extern void Grabbable_DropItem_m384A9F9A279E21DCB1F36F0F70C48CF81B0409C6 (void);
// 0x00000435 System.Void BNG.Grabbable::DropItem(System.Boolean,System.Boolean)
extern void Grabbable_DropItem_m355347FAD6DC4B575179DDB966C58AF3DBEAD5F7 (void);
// 0x00000436 System.Void BNG.Grabbable::ResetScale()
extern void Grabbable_ResetScale_m21E155F7F720AFEBD9D0BAF851E89A6D5B4B5494 (void);
// 0x00000437 System.Void BNG.Grabbable::ResetParent()
extern void Grabbable_ResetParent_mF0A8EE1688D6F46DC78DEF921E798BF8B6595304 (void);
// 0x00000438 System.Void BNG.Grabbable::UpdateOriginalParent(UnityEngine.Transform)
extern void Grabbable_UpdateOriginalParent_m93401AED4044FF34834C363FB1421EF3779BAAB0 (void);
// 0x00000439 System.Void BNG.Grabbable::UpdateOriginalParent()
extern void Grabbable_UpdateOriginalParent_m4F41DB593626979EDF61900CCF8467C12401B0AF (void);
// 0x0000043A BNG.ControllerHand BNG.Grabbable::GetControllerHand(BNG.Grabber)
extern void Grabbable_GetControllerHand_mB4866ABE69BFB3609C49CC659A43273CA26B801B (void);
// 0x0000043B BNG.Grabber BNG.Grabbable::GetPrimaryGrabber()
extern void Grabbable_GetPrimaryGrabber_mF6744E89E67311D08D9E681D9B2B44037AA32C4D (void);
// 0x0000043C BNG.Grabber BNG.Grabbable::GetClosestGrabber()
extern void Grabbable_GetClosestGrabber_m18B4C9D309A64435E1725DB26D4423F7D2571BBE (void);
// 0x0000043D UnityEngine.Transform BNG.Grabbable::GetClosestGrabPoint(BNG.Grabber)
extern void Grabbable_GetClosestGrabPoint_m605E6CB6ABE609C29225A591CD05140FDEAD8929 (void);
// 0x0000043E System.Void BNG.Grabbable::Release(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Grabbable_Release_m30C12E3213524FF6A2DF25CB4885135C5878A112 (void);
// 0x0000043F System.Boolean BNG.Grabbable::IsValidCollision(UnityEngine.Collision)
extern void Grabbable_IsValidCollision_mBB75D1F99676A97A4DF5D7741728ED8594F299C0 (void);
// 0x00000440 System.Boolean BNG.Grabbable::IsValidCollision(UnityEngine.Collider)
extern void Grabbable_IsValidCollision_m1AB863644F9D2C3B60A7C13A52E46A4DAA47E676 (void);
// 0x00000441 System.Void BNG.Grabbable::parentHandGraphics(BNG.Grabber)
extern void Grabbable_parentHandGraphics_mF05CA45822C4BED94076E3B55604C8833509E912 (void);
// 0x00000442 System.Void BNG.Grabbable::setupConfigJoint(BNG.Grabber)
extern void Grabbable_setupConfigJoint_m2F66E76C8E7352711A8BE0D80EF4D8FDB42E5D22 (void);
// 0x00000443 System.Void BNG.Grabbable::removeConfigJoint()
extern void Grabbable_removeConfigJoint_m99EF5F30E4384EB745E6FC2777537A48BAF3811F (void);
// 0x00000444 System.Void BNG.Grabbable::addGrabber(BNG.Grabber)
extern void Grabbable_addGrabber_mA7B27DD37C5255C77CC13765F6F7191D97B8FF91 (void);
// 0x00000445 System.Void BNG.Grabbable::removeGrabber(BNG.Grabber)
extern void Grabbable_removeGrabber_m3AB1DE4F60BB064930FA3F0A7E9D82CEA7B5B79C (void);
// 0x00000446 System.Void BNG.Grabbable::movePosition(UnityEngine.Vector3)
extern void Grabbable_movePosition_m8606A65DA48C20B1C9CDE33814591BE227CA6F90 (void);
// 0x00000447 System.Void BNG.Grabbable::moveRotation(UnityEngine.Quaternion)
extern void Grabbable_moveRotation_mBB552B728A898A5650A9A3B7C817415F601F6EBF (void);
// 0x00000448 UnityEngine.Vector3 BNG.Grabbable::getRemotePosition(BNG.Grabber)
extern void Grabbable_getRemotePosition_m117ABD661B5ACC76386DA16E6E894F240082CC19 (void);
// 0x00000449 UnityEngine.Quaternion BNG.Grabbable::getRemoteRotation(BNG.Grabber)
extern void Grabbable_getRemoteRotation_m40378F0BF5A43AB4171851E03F84E5A348771F6C (void);
// 0x0000044A System.Void BNG.Grabbable::filterCollisions()
extern void Grabbable_filterCollisions_mE9F39A1C1FBC8BB2C3E113B4DB51FDFD8104B374 (void);
// 0x0000044B BNG.BNGPlayerController BNG.Grabbable::GetBNGPlayerController()
extern void Grabbable_GetBNGPlayerController_mD601344D1822FA255A88656594A0558C16E3FED6 (void);
// 0x0000044C System.Void BNG.Grabbable::RequestSpringTime(System.Single)
extern void Grabbable_RequestSpringTime_mC449467620555F5B22740FE1EF9C8F30717CFBBD (void);
// 0x0000044D System.Void BNG.Grabbable::AddValidGrabber(BNG.Grabber)
extern void Grabbable_AddValidGrabber_m93D4B048D299E1CA250A356B5E8C53AD05A871FA (void);
// 0x0000044E System.Void BNG.Grabbable::RemoveValidGrabber(BNG.Grabber)
extern void Grabbable_RemoveValidGrabber_mE5313EAA945177A039E02417D8E1C7342542C491 (void);
// 0x0000044F System.Void BNG.Grabbable::SubscribeToMoveEvents()
extern void Grabbable_SubscribeToMoveEvents_m3737ABD04A901900C6F83809221143B063E0F8CF (void);
// 0x00000450 System.Void BNG.Grabbable::UnsubscribeFromMoveEvents()
extern void Grabbable_UnsubscribeFromMoveEvents_mE71F12FA5609AEDA8292A65A058033B6FB1D1CB5 (void);
// 0x00000451 System.Void BNG.Grabbable::LockGrabbable()
extern void Grabbable_LockGrabbable_mC96CCD4767739F5E7E4A7EA401B97496087E5646 (void);
// 0x00000452 System.Void BNG.Grabbable::LockGrabbableWithRotation()
extern void Grabbable_LockGrabbableWithRotation_m6E85F925DE19FFA4D932732717FDBAD885524B72 (void);
// 0x00000453 System.Void BNG.Grabbable::RequestLockGrabbable()
extern void Grabbable_RequestLockGrabbable_mB7481FDB608BB75A0BDF844FDA8CEFC79805D776 (void);
// 0x00000454 System.Void BNG.Grabbable::RequestUnlockGrabbable()
extern void Grabbable_RequestUnlockGrabbable_mBC04E1DFC1574BA3374889EB192DD69C8B28D42D (void);
// 0x00000455 System.Void BNG.Grabbable::ResetLockResets()
extern void Grabbable_ResetLockResets_m916C34D0310DDF1AFECF721C0C18AA519F87F39C (void);
// 0x00000456 System.Void BNG.Grabbable::LockGrabbable(System.Boolean,System.Boolean,System.Boolean)
extern void Grabbable_LockGrabbable_m45DE61A4332033199D3467ABB78C00EA359FDB42 (void);
// 0x00000457 System.Void BNG.Grabbable::UnlockGrabbable()
extern void Grabbable_UnlockGrabbable_mAF8A92DCAE1B1906DC8FE92592815623D39C13A9 (void);
// 0x00000458 System.Void BNG.Grabbable::OnCollisionStay(UnityEngine.Collision)
extern void Grabbable_OnCollisionStay_m6359C76365E207B2651C63DC6753A911FAA14FEF (void);
// 0x00000459 System.Void BNG.Grabbable::OnCollisionEnter(UnityEngine.Collision)
extern void Grabbable_OnCollisionEnter_m1FAF3B24CD8B1D6C979022EEBFEA702706E4C8F2 (void);
// 0x0000045A System.Void BNG.Grabbable::OnCollisionExit(UnityEngine.Collision)
extern void Grabbable_OnCollisionExit_m3E3A47175938D17E6BAE1239468A1C8FE743F973 (void);
// 0x0000045B System.Void BNG.Grabbable::OnApplicationQuit()
extern void Grabbable_OnApplicationQuit_m6BE9944040ED03B0393FEAB97CFCDF9778773171 (void);
// 0x0000045C System.Void BNG.Grabbable::OnDestroy()
extern void Grabbable_OnDestroy_m2191279974FB96A50B50C51E7BCEB59D1EA2BDAD (void);
// 0x0000045D System.Void BNG.Grabbable::OnDrawGizmosSelected()
extern void Grabbable_OnDrawGizmosSelected_m4F72287E81C37A12B2989FEF6F282FDDC9885A58 (void);
// 0x0000045E System.Void BNG.Grabbable::.ctor()
extern void Grabbable__ctor_mDD5DA95466F9823C086B131AABAE3974F0BDB7D3 (void);
// 0x0000045F System.Void BNG.GrabbableChild::.ctor()
extern void GrabbableChild__ctor_m10BF1B9419913890AB68C58B4C6E3B4582ED88FD (void);
// 0x00000460 System.Void BNG.GrabbableEvents::Awake()
extern void GrabbableEvents_Awake_m9F5618AD8F3966F75E93CA7639F63C9E4E95F309 (void);
// 0x00000461 System.Void BNG.GrabbableEvents::OnGrab(BNG.Grabber)
extern void GrabbableEvents_OnGrab_mDF1C020E9A4D31F92A34ADC84C2B0291690407C8 (void);
// 0x00000462 System.Void BNG.GrabbableEvents::OnRelease()
extern void GrabbableEvents_OnRelease_m698B868AA5D18F08E617397B5BE529A79C8F024C (void);
// 0x00000463 System.Void BNG.GrabbableEvents::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnBecomesClosestGrabbable_m47FECAB518361C6100F4F154F2C3574A9D5E3C9F (void);
// 0x00000464 System.Void BNG.GrabbableEvents::OnBecomesClosestGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnBecomesClosestGrabbable_m5857AA36EA45690C32CF062DBAD43E3862F56E8F (void);
// 0x00000465 System.Void BNG.GrabbableEvents::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnNoLongerClosestGrabbable_mE70BD10C77000C137432ACAFBD2390FF32FC49D4 (void);
// 0x00000466 System.Void BNG.GrabbableEvents::OnNoLongerClosestGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnNoLongerClosestGrabbable_mC2ACCA1D4822F371F12FBC9B4B6CE91D1136AF2E (void);
// 0x00000467 System.Void BNG.GrabbableEvents::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnBecomesClosestRemoteGrabbable_mB421DEF316E15D87A52946CDE39F3A5A483E6D85 (void);
// 0x00000468 System.Void BNG.GrabbableEvents::OnBecomesClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnBecomesClosestRemoteGrabbable_mC43C260A790DA63F1F4955472490903BDF7A694D (void);
// 0x00000469 System.Void BNG.GrabbableEvents::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableEvents_OnNoLongerClosestRemoteGrabbable_m0C4E8DBA9655A6C86FB56D321615BFA0F25FB70E (void);
// 0x0000046A System.Void BNG.GrabbableEvents::OnNoLongerClosestRemoteGrabbable(BNG.Grabber)
extern void GrabbableEvents_OnNoLongerClosestRemoteGrabbable_m5395F8747359099AEDF5101D92DEBEC83B588B3B (void);
// 0x0000046B System.Void BNG.GrabbableEvents::OnGrip(System.Single)
extern void GrabbableEvents_OnGrip_m78DE466A19AB3F13FE6DA22E452160DA6CC90CAB (void);
// 0x0000046C System.Void BNG.GrabbableEvents::OnTrigger(System.Single)
extern void GrabbableEvents_OnTrigger_m5D42C47CA379E1E8502D7DD01E0C4F8D5C058E56 (void);
// 0x0000046D System.Void BNG.GrabbableEvents::OnTriggerDown()
extern void GrabbableEvents_OnTriggerDown_m46C0199A200DB3BD7C6B5984C75DB783AFC26517 (void);
// 0x0000046E System.Void BNG.GrabbableEvents::OnTriggerUp()
extern void GrabbableEvents_OnTriggerUp_m6F390D3DA419400E9125EBBECF3D9BAF76876A0C (void);
// 0x0000046F System.Void BNG.GrabbableEvents::OnButton1()
extern void GrabbableEvents_OnButton1_mB9B488C584EF6CE0A01C59520E9756DDC06B513D (void);
// 0x00000470 System.Void BNG.GrabbableEvents::OnButton1Down()
extern void GrabbableEvents_OnButton1Down_m0050426182DBF5ADE2BABF3129587B8F073F13A9 (void);
// 0x00000471 System.Void BNG.GrabbableEvents::OnButton1Up()
extern void GrabbableEvents_OnButton1Up_mB1FAAC2F28447B04C82E4054F2E279F906C538A6 (void);
// 0x00000472 System.Void BNG.GrabbableEvents::OnButton2()
extern void GrabbableEvents_OnButton2_m400115103B7ACA2AE5CD43EAF7C9479A505198DF (void);
// 0x00000473 System.Void BNG.GrabbableEvents::OnButton2Down()
extern void GrabbableEvents_OnButton2Down_m03F239CE72DE4F42DB6ABC95EF54EC9D7291667F (void);
// 0x00000474 System.Void BNG.GrabbableEvents::OnButton2Up()
extern void GrabbableEvents_OnButton2Up_m1F459E0C89F43083A36F1F9DB8C2D5CBD3ED17C1 (void);
// 0x00000475 System.Void BNG.GrabbableEvents::OnSnapZoneEnter()
extern void GrabbableEvents_OnSnapZoneEnter_m35A896C3EBC854791C0CDDFC448A186EAB309C83 (void);
// 0x00000476 System.Void BNG.GrabbableEvents::OnSnapZoneExit()
extern void GrabbableEvents_OnSnapZoneExit_m1FFB3F76C5E4459069FAD33187C8FC2A3B72184F (void);
// 0x00000477 System.Void BNG.GrabbableEvents::.ctor()
extern void GrabbableEvents__ctor_mF47EE9A8DE1824F2DC070FE888F6201056419298 (void);
// 0x00000478 System.Void BNG.FloatEvent::.ctor()
extern void FloatEvent__ctor_mD92724B159D5101666EA300A0F68A05873B6527E (void);
// 0x00000479 System.Void BNG.FloatFloatEvent::.ctor()
extern void FloatFloatEvent__ctor_m635B39D4D22058C9ED99E337A5C28106EA2B8B5A (void);
// 0x0000047A System.Void BNG.GrabberEvent::.ctor()
extern void GrabberEvent__ctor_m4633675758D48B26B0EAD1382E6B53DB553107C7 (void);
// 0x0000047B System.Void BNG.GrabbableEvent::.ctor()
extern void GrabbableEvent__ctor_m51134B6B1BA88079C1F5681E2B03EF0EAE416A5A (void);
// 0x0000047C System.Void BNG.RaycastHitEvent::.ctor()
extern void RaycastHitEvent__ctor_mDEC456179F740D20446E6EA198E87C2A5CA192CC (void);
// 0x0000047D System.Void BNG.Vector2Event::.ctor()
extern void Vector2Event__ctor_mBD14A9AE398A8BFAE8421690F3DDB5EB712C095C (void);
// 0x0000047E System.Void BNG.Vector3Event::.ctor()
extern void Vector3Event__ctor_m1127B8209FFD7D0A973CDD8CFCEC75EF1C3F932D (void);
// 0x0000047F System.Void BNG.PointerEventDataEvent::.ctor()
extern void PointerEventDataEvent__ctor_mE0D65709D4A0794036D0C2B1B97B4580C2B158D1 (void);
// 0x00000480 System.Void BNG.GrabbablesInTrigger::Start()
extern void GrabbablesInTrigger_Start_m3FCEBEBD82F7E7AC216AEA09E99DE70A1C4D13FF (void);
// 0x00000481 System.Void BNG.GrabbablesInTrigger::Update()
extern void GrabbablesInTrigger_Update_mFF090C29C7519A8AAE48D69B857762B0946F42AD (void);
// 0x00000482 System.Void BNG.GrabbablesInTrigger::updateClosestGrabbable()
extern void GrabbablesInTrigger_updateClosestGrabbable_mCC7B9AB1923AC3802851DAE51E4BA5B70ACBCC10 (void);
// 0x00000483 System.Void BNG.GrabbablesInTrigger::updateClosestRemoteGrabbables()
extern void GrabbablesInTrigger_updateClosestRemoteGrabbables_m7A0E9C7EA63726B95E82561B5444685592E061D3 (void);
// 0x00000484 BNG.Grabbable BNG.GrabbablesInTrigger::GetClosestGrabbable(System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable>,System.Boolean)
extern void GrabbablesInTrigger_GetClosestGrabbable_m8C03DD9E306F50E2388A3A055AC9B560AB2EC3D4 (void);
// 0x00000485 System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable> BNG.GrabbablesInTrigger::GetValidGrabbables(System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable>)
extern void GrabbablesInTrigger_GetValidGrabbables_m9461614902302A9F04A4CFDF0117F351FA35202B (void);
// 0x00000486 System.Boolean BNG.GrabbablesInTrigger::isValidGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_isValidGrabbable_m4DD4B8B9975780239B7E7BDBAE488AD7440068E5 (void);
// 0x00000487 System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable> BNG.GrabbablesInTrigger::SanitizeGrabbables(System.Collections.Generic.Dictionary`2<UnityEngine.Collider,BNG.Grabbable>)
extern void GrabbablesInTrigger_SanitizeGrabbables_m4417EA2A0D79EC77547BB28EF7D58DCD5F41C12D (void);
// 0x00000488 System.Void BNG.GrabbablesInTrigger::AddNearbyGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_AddNearbyGrabbable_m5BA1B342F70C7ACB1702F1D839F5429FCA80223F (void);
// 0x00000489 System.Void BNG.GrabbablesInTrigger::RemoveNearbyGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_RemoveNearbyGrabbable_m2416DA65AE7FCC9F1F8D35BB80941A1FADB0F9D3 (void);
// 0x0000048A System.Void BNG.GrabbablesInTrigger::RemoveNearbyGrabbable(BNG.Grabbable)
extern void GrabbablesInTrigger_RemoveNearbyGrabbable_m84A308414BF1A6EEB5FD23A465A08E7A4247C48F (void);
// 0x0000048B System.Void BNG.GrabbablesInTrigger::AddValidRemoteGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_AddValidRemoteGrabbable_mD4AAC2F22CE0433F2AC12CD8AB0BC9F76CDA0051 (void);
// 0x0000048C System.Void BNG.GrabbablesInTrigger::RemoveValidRemoteGrabbable(UnityEngine.Collider,BNG.Grabbable)
extern void GrabbablesInTrigger_RemoveValidRemoteGrabbable_mB8712ED87647A4F9D30D88A0FD24EAA0760A5FB4 (void);
// 0x0000048D System.Void BNG.GrabbablesInTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void GrabbablesInTrigger_OnTriggerEnter_m9AD0BF63351800ECD6370B51529B270524AA8025 (void);
// 0x0000048E System.Void BNG.GrabbablesInTrigger::OnTriggerExit(UnityEngine.Collider)
extern void GrabbablesInTrigger_OnTriggerExit_mAA56D6386A6E9D813DF6DF296A03C342F6207969 (void);
// 0x0000048F System.Void BNG.GrabbablesInTrigger::.ctor()
extern void GrabbablesInTrigger__ctor_m55CD46A49A024F2D12BCE73C8BBE8D5AF2B27ACF (void);
// 0x00000490 System.Void BNG.GrabbableUnityEvents::OnGrab(BNG.Grabber)
extern void GrabbableUnityEvents_OnGrab_m7238A79C74E453D47C46A984EB56322471694E08 (void);
// 0x00000491 System.Void BNG.GrabbableUnityEvents::OnRelease()
extern void GrabbableUnityEvents_OnRelease_m601EACA1DC17418A44F9318418B73502B673F82E (void);
// 0x00000492 System.Void BNG.GrabbableUnityEvents::OnBecomesClosestGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnBecomesClosestGrabbable_mF03823B3307C28E89580D13EF5A17CD16940554D (void);
// 0x00000493 System.Void BNG.GrabbableUnityEvents::OnNoLongerClosestGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnNoLongerClosestGrabbable_mA05388792BC4B5B638D490EA71F74CB8A42A5457 (void);
// 0x00000494 System.Void BNG.GrabbableUnityEvents::OnBecomesClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnBecomesClosestRemoteGrabbable_mF4F5050553C6C7155CECE7F6ED50AB3F3AD95C0B (void);
// 0x00000495 System.Void BNG.GrabbableUnityEvents::OnNoLongerClosestRemoteGrabbable(BNG.ControllerHand)
extern void GrabbableUnityEvents_OnNoLongerClosestRemoteGrabbable_m4DAE6BEBBFF6D44142EF5105765AF77BFB3179FF (void);
// 0x00000496 System.Void BNG.GrabbableUnityEvents::OnGrip(System.Single)
extern void GrabbableUnityEvents_OnGrip_mAA2572A476B835A9F8E91903CD2CCF0074DD0A7C (void);
// 0x00000497 System.Void BNG.GrabbableUnityEvents::OnTrigger(System.Single)
extern void GrabbableUnityEvents_OnTrigger_m66CAA020EB5DFEAFE3018946FD7C972C22718A6E (void);
// 0x00000498 System.Void BNG.GrabbableUnityEvents::OnTriggerDown()
extern void GrabbableUnityEvents_OnTriggerDown_m4F26B42BE20FEAD6F72812A6DB25E9F999461F0F (void);
// 0x00000499 System.Void BNG.GrabbableUnityEvents::OnTriggerUp()
extern void GrabbableUnityEvents_OnTriggerUp_m02B69ADBAE6C1F292F8A441BF2D7EF049FB953AD (void);
// 0x0000049A System.Void BNG.GrabbableUnityEvents::OnButton1()
extern void GrabbableUnityEvents_OnButton1_m07AC70A2D1960FC0B1AA4C92CBB3844FFF76638B (void);
// 0x0000049B System.Void BNG.GrabbableUnityEvents::OnButton1Down()
extern void GrabbableUnityEvents_OnButton1Down_m34799B35B8B878BFFC3084FBD66631A583CC1A29 (void);
// 0x0000049C System.Void BNG.GrabbableUnityEvents::OnButton1Up()
extern void GrabbableUnityEvents_OnButton1Up_m1AEF1BFC531FB76A6502E6C7BDF6FB7CA0FB2BB7 (void);
// 0x0000049D System.Void BNG.GrabbableUnityEvents::OnButton2()
extern void GrabbableUnityEvents_OnButton2_mB6CA75B90B57CCC41515DABA3EF7619075329036 (void);
// 0x0000049E System.Void BNG.GrabbableUnityEvents::OnButton2Down()
extern void GrabbableUnityEvents_OnButton2Down_mE807900C19BC8AE4A3D8110AEEB7D80C8CAA4913 (void);
// 0x0000049F System.Void BNG.GrabbableUnityEvents::OnButton2Up()
extern void GrabbableUnityEvents_OnButton2Up_m443018BCC2793373D9CF643FA3F65CB77A625EE6 (void);
// 0x000004A0 System.Void BNG.GrabbableUnityEvents::OnSnapZoneEnter()
extern void GrabbableUnityEvents_OnSnapZoneEnter_m17B815D9BB390D7191A580A7FE919A98BB1FDBEF (void);
// 0x000004A1 System.Void BNG.GrabbableUnityEvents::OnSnapZoneExit()
extern void GrabbableUnityEvents_OnSnapZoneExit_m0B787CD68EE44210183215796C6384D508E12141 (void);
// 0x000004A2 System.Void BNG.GrabbableUnityEvents::.ctor()
extern void GrabbableUnityEvents__ctor_m469444FB3D8E0C64B5115F33D76393A271A9C4F2 (void);
// 0x000004A3 System.Boolean BNG.Grabber::get_HoldingItem()
extern void Grabber_get_HoldingItem_m0DA4989F8955BE513E91B01587BF156C8B6DBEA3 (void);
// 0x000004A4 System.Boolean BNG.Grabber::get_RemoteGrabbingItem()
extern void Grabber_get_RemoteGrabbingItem_m28DAB10406322D385C288BE21E3F11E6C146C2A2 (void);
// 0x000004A5 BNG.GrabbablesInTrigger BNG.Grabber::get_GrabsInTrigger()
extern void Grabber_get_GrabsInTrigger_mA64C03645067D8DF722A9E3D2D784692794CAF23 (void);
// 0x000004A6 BNG.Grabbable BNG.Grabber::get_RemoteGrabbingGrabbable()
extern void Grabber_get_RemoteGrabbingGrabbable_m5959DAD8E93885DFE52C404C724D16C187BE05FE (void);
// 0x000004A7 UnityEngine.Vector3 BNG.Grabber::get_handsGraphicsGrabberOffset()
extern void Grabber_get_handsGraphicsGrabberOffset_m2C77CA6C5965FACCDD7A3AA5EE0075E01D122E3C (void);
// 0x000004A8 System.Void BNG.Grabber::set_handsGraphicsGrabberOffset(UnityEngine.Vector3)
extern void Grabber_set_handsGraphicsGrabberOffset_mF31F3471BF3236A494F5FEB44E807CAE4EB6B4D3 (void);
// 0x000004A9 UnityEngine.Vector3 BNG.Grabber::get_handsGraphicsGrabberOffsetRotation()
extern void Grabber_get_handsGraphicsGrabberOffsetRotation_m4A5D929FB9B1BC22AE0F65125310B7DCF07AE4DD (void);
// 0x000004AA System.Void BNG.Grabber::set_handsGraphicsGrabberOffsetRotation(UnityEngine.Vector3)
extern void Grabber_set_handsGraphicsGrabberOffsetRotation_m747E203057A64DDF2CF0FBEF13A90E31317544A4 (void);
// 0x000004AB System.Void BNG.Grabber::Start()
extern void Grabber_Start_m684DC290F24114C73D06E5591254713FE5CE6A74 (void);
// 0x000004AC System.Void BNG.Grabber::Update()
extern void Grabber_Update_m6752AE1F01A0AA0C9A14B8DA486A5166CBEACBCC (void);
// 0x000004AD System.Void BNG.Grabber::updateFreshGrabStatus()
extern void Grabber_updateFreshGrabStatus_mE21C92B5D1ECA9F439AA17BC83B9CEE7C334B31A (void);
// 0x000004AE System.Void BNG.Grabber::checkGrabbableEvents()
extern void Grabber_checkGrabbableEvents_m1D2EFD21D8CD6C508C5DBBDC80EE696704EEFBCD (void);
// 0x000004AF System.Boolean BNG.Grabber::InputCheckGrab()
extern void Grabber_InputCheckGrab_mAF353C801FA6C8E50530BC97F8D68857172571B3 (void);
// 0x000004B0 System.Boolean BNG.Grabber::GetInputDownForGrabbable(BNG.Grabbable)
extern void Grabber_GetInputDownForGrabbable_m19F28C060CD4EAA3BAB24ED36CA157335A7BAA35 (void);
// 0x000004B1 BNG.HoldType BNG.Grabber::getHoldType(BNG.Grabbable)
extern void Grabber_getHoldType_mE3D5AE3185E8C447CD6AB0436AB86FB40158F5E3 (void);
// 0x000004B2 BNG.GrabButton BNG.Grabber::GetGrabButton(BNG.Grabbable)
extern void Grabber_GetGrabButton_m3B65A72752AA4C0C7E46AF167BA81CA10A6A382E (void);
// 0x000004B3 BNG.Grabbable BNG.Grabber::getClosestOrRemote()
extern void Grabber_getClosestOrRemote_m2938A5C903F25486053626A8984CB36F7015B3FD (void);
// 0x000004B4 System.Boolean BNG.Grabber::inputCheckRelease()
extern void Grabber_inputCheckRelease_m1E23B0744934A7F94E62EB802BD51F5D0CB9823D (void);
// 0x000004B5 System.Single BNG.Grabber::getGrabInput(BNG.GrabButton)
extern void Grabber_getGrabInput_m3FE1A9AE32FE9A25B9AC5FBE92F6545724851ED5 (void);
// 0x000004B6 System.Boolean BNG.Grabber::getToggleInput(BNG.GrabButton)
extern void Grabber_getToggleInput_m22148348474B18138FEF8F70E65F2ED84BD11819 (void);
// 0x000004B7 System.Boolean BNG.Grabber::TryGrab()
extern void Grabber_TryGrab_m374781F158E1370646BEA336F4173622CDEA5402 (void);
// 0x000004B8 System.Void BNG.Grabber::GrabGrabbable(BNG.Grabbable)
extern void Grabber_GrabGrabbable_m032064C125E640CBA2E10780B8D0D388918792EC (void);
// 0x000004B9 System.Void BNG.Grabber::DidDrop()
extern void Grabber_DidDrop_m3D8737763E432EB8615F3BA285E5F7ED2EFD7579 (void);
// 0x000004BA System.Void BNG.Grabber::HideHandGraphics()
extern void Grabber_HideHandGraphics_mE422C482325E80ED7CF37B2B3DAD3B72FD0550BF (void);
// 0x000004BB System.Void BNG.Grabber::ResetHandGraphics()
extern void Grabber_ResetHandGraphics_m59C551B05EB5BE037B99943CE71CBD14C55702D9 (void);
// 0x000004BC System.Void BNG.Grabber::TryRelease()
extern void Grabber_TryRelease_m3217B651CE7F54031B3A770D2D39A526A9B8AC7B (void);
// 0x000004BD System.Void BNG.Grabber::resetFlyingGrabbable()
extern void Grabber_resetFlyingGrabbable_m18AE676E06A2A1EDC2E319A9515A30E93C3C3568 (void);
// 0x000004BE UnityEngine.Vector3 BNG.Grabber::GetGrabberAveragedVelocity()
extern void Grabber_GetGrabberAveragedVelocity_mB146CA6AB378DB424F53900E56E6F52B68E3850C (void);
// 0x000004BF UnityEngine.Vector3 BNG.Grabber::GetGrabberAveragedAngularVelocity()
extern void Grabber_GetGrabberAveragedAngularVelocity_m55F8779AC4C25FC662774813098098DFCBD64147 (void);
// 0x000004C0 System.Void BNG.Grabber::.ctor()
extern void Grabber__ctor_m9C4BEA7B6E56E1A96E1186F0B1514D832A28BA3F (void);
// 0x000004C1 System.Void BNG.GrabPoint::UpdatePreviewTransforms()
extern void GrabPoint_UpdatePreviewTransforms_m7F07C7AA56EBAFCEC4E7EFA40C4C7596CF39FC59 (void);
// 0x000004C2 System.Void BNG.GrabPoint::UpdateHandPosePreview()
extern void GrabPoint_UpdateHandPosePreview_mD4B9F7CBC4BB854CE72D43B754C7CAE26F0580D3 (void);
// 0x000004C3 System.Void BNG.GrabPoint::UpdateAutoPoserPreview()
extern void GrabPoint_UpdateAutoPoserPreview_m4621AEB68A646D7322A7FBF2111734AD787522D6 (void);
// 0x000004C4 System.Void BNG.GrabPoint::UpdateChildAnimators()
extern void GrabPoint_UpdateChildAnimators_m481BF02DBE265B03ED365F1CA3956E874F329EDA (void);
// 0x000004C5 System.Void BNG.GrabPoint::.ctor()
extern void GrabPoint__ctor_m38804D16AEA4EEDBBE09CECF95A7F5CAE7DC1F7B (void);
// 0x000004C6 System.Void BNG.HandModelSelector::Start()
extern void HandModelSelector_Start_mD8EDDE9C873ED33D6ED83F7354CC089555566E2D (void);
// 0x000004C7 System.Void BNG.HandModelSelector::Update()
extern void HandModelSelector_Update_mAA81420297D421F7507CDA8E017BF15E53818520 (void);
// 0x000004C8 System.Void BNG.HandModelSelector::CacheHandModels()
extern void HandModelSelector_CacheHandModels_m602B5BF04DD6FD4F06F7D955DA1D0169D13326C5 (void);
// 0x000004C9 System.Void BNG.HandModelSelector::ChangeHandsModel(System.Int32,System.Boolean)
extern void HandModelSelector_ChangeHandsModel_mBEBD227C11A42E104E34277A707C050465D664E8 (void);
// 0x000004CA System.Void BNG.HandModelSelector::.ctor()
extern void HandModelSelector__ctor_m8E24F5EA91FC99C5A0CB590861C390CA3F049876 (void);
// 0x000004CB System.Void BNG.HeadCollisionFade::Start()
extern void HeadCollisionFade_Start_mC22778EFAFB26879A103FCCD185925D66425F843 (void);
// 0x000004CC System.Void BNG.HeadCollisionFade::LateUpdate()
extern void HeadCollisionFade_LateUpdate_m31086264798C8FC41593DEA71FF7908747293F74 (void);
// 0x000004CD System.Void BNG.HeadCollisionFade::OnCollisionEnter(UnityEngine.Collision)
extern void HeadCollisionFade_OnCollisionEnter_m65C01DE4F25F92899881FC2A2320B500C3E48707 (void);
// 0x000004CE System.Void BNG.HeadCollisionFade::OnCollisionExit(UnityEngine.Collision)
extern void HeadCollisionFade_OnCollisionExit_m24368A5065376C7914EFA7C77B96A2C322790EDF (void);
// 0x000004CF System.Void BNG.HeadCollisionFade::.ctor()
extern void HeadCollisionFade__ctor_m0B21DCE83E4493284529A06D740E4A307EB8C28C (void);
// 0x000004D0 System.Void BNG.HingeHelper::Start()
extern void HingeHelper_Start_m3A2AAD126115AAC504036ECCBA5B550D55F205A8 (void);
// 0x000004D1 System.Void BNG.HingeHelper::Update()
extern void HingeHelper_Update_m45DB334B8242D80FC778C4593764DE00FB5E4D86 (void);
// 0x000004D2 System.Void BNG.HingeHelper::OnSnapChange(System.Single)
extern void HingeHelper_OnSnapChange_m1712E0DDA56A6669524C262A107551B6828B9636 (void);
// 0x000004D3 System.Void BNG.HingeHelper::OnRelease()
extern void HingeHelper_OnRelease_m4408ED7693C96F0E015241586D41DDE18DD60620 (void);
// 0x000004D4 System.Void BNG.HingeHelper::OnHingeChange(System.Single)
extern void HingeHelper_OnHingeChange_mBE3B1CB9BF349C8BD0B83134BBF325D7DE75C5AF (void);
// 0x000004D5 System.Single BNG.HingeHelper::getSmoothedValue(System.Single)
extern void HingeHelper_getSmoothedValue_m602CD65F8572131C63F85B0B8F0D99EB5D6D1B13 (void);
// 0x000004D6 System.Void BNG.HingeHelper::.ctor()
extern void HingeHelper__ctor_mEFB877A986D040A9D69992E043A1ACFE31482DC3 (void);
// 0x000004D7 BNG.InputBridge BNG.InputBridge::get_Instance()
extern void InputBridge_get_Instance_m71E90D14104F9FBD48661F41B85E6D289634128D (void);
// 0x000004D8 System.Single BNG.InputBridge::get_DownThreshold()
extern void InputBridge_get_DownThreshold_m0A12769107BA70D3E1A092EE251EA4B7B43BE533 (void);
// 0x000004D9 BNG.SDKProvider BNG.InputBridge::get_LoadedSDK()
extern void InputBridge_get_LoadedSDK_mA7B15361CE284E7BFDC726F36F7680AF16B095A9 (void);
// 0x000004DA System.Void BNG.InputBridge::set_LoadedSDK(BNG.SDKProvider)
extern void InputBridge_set_LoadedSDK_mD3A957E9643B1987DB89DF8B01FCC1B8CA7C3FFC (void);
// 0x000004DB System.Boolean BNG.InputBridge::get_IsOculusDevice()
extern void InputBridge_get_IsOculusDevice_mB548727A7ED803DF3D54FD9018498E86620E08BC (void);
// 0x000004DC System.Void BNG.InputBridge::set_IsOculusDevice(System.Boolean)
extern void InputBridge_set_IsOculusDevice_m4E6810E32B1335BD0567F9C510C62F9EEB2ECFF2 (void);
// 0x000004DD System.Boolean BNG.InputBridge::get_IsOculusQuest()
extern void InputBridge_get_IsOculusQuest_mC6EA6FD0E4F2B9FA1DDEEC8A306C06FD681A1802 (void);
// 0x000004DE System.Void BNG.InputBridge::set_IsOculusQuest(System.Boolean)
extern void InputBridge_set_IsOculusQuest_m4C98364A9F557FBDB8E15A450825737674625A5E (void);
// 0x000004DF System.Boolean BNG.InputBridge::get_IsHTCDevice()
extern void InputBridge_get_IsHTCDevice_m226ED6AC3933788701F8A6C4CB9704879B22A157 (void);
// 0x000004E0 System.Void BNG.InputBridge::set_IsHTCDevice(System.Boolean)
extern void InputBridge_set_IsHTCDevice_mE97AA136CA066F7F54C59CD5D7FC1B4CC9FB3066 (void);
// 0x000004E1 System.Boolean BNG.InputBridge::get_IsPicoDevice()
extern void InputBridge_get_IsPicoDevice_mF8983C3C0341E0BA4C615A8356C3E9E15484B57A (void);
// 0x000004E2 System.Void BNG.InputBridge::set_IsPicoDevice(System.Boolean)
extern void InputBridge_set_IsPicoDevice_m62297968B8E4F002FB37C1FB8209865BC32EDCE1 (void);
// 0x000004E3 System.Boolean BNG.InputBridge::get_IsValveIndexController()
extern void InputBridge_get_IsValveIndexController_m361F985A424F5F0B2E256311F10112B0515E7578 (void);
// 0x000004E4 System.Void BNG.InputBridge::set_IsValveIndexController(System.Boolean)
extern void InputBridge_set_IsValveIndexController_m3E18AD4E7352CD921AF7EFEE20BFDC8487F4FB94 (void);
// 0x000004E5 System.Void BNG.InputBridge::add_OnInputsUpdated(BNG.InputBridge/InputsUpdatedAction)
extern void InputBridge_add_OnInputsUpdated_m47936B75C00042DCE00BA573C9F81593C236D2A3 (void);
// 0x000004E6 System.Void BNG.InputBridge::remove_OnInputsUpdated(BNG.InputBridge/InputsUpdatedAction)
extern void InputBridge_remove_OnInputsUpdated_m1FD0FCE9A53727914562F42B6235D62141175F14 (void);
// 0x000004E7 System.Void BNG.InputBridge::add_OnControllerFound(BNG.InputBridge/ControllerFoundAction)
extern void InputBridge_add_OnControllerFound_mB2E45D6082E721F037F02CF175850148EF913091 (void);
// 0x000004E8 System.Void BNG.InputBridge::remove_OnControllerFound(BNG.InputBridge/ControllerFoundAction)
extern void InputBridge_remove_OnControllerFound_mA448CD69EB6C68FBDB1AA4A7A5EA1CB6DF1E23DE (void);
// 0x000004E9 System.Void BNG.InputBridge::Awake()
extern void InputBridge_Awake_mA2D9499A389185B92C561BC9C083B6743535E29D (void);
// 0x000004EA System.Void BNG.InputBridge::Start()
extern void InputBridge_Start_mCA1B8F4AAA3F32C24F4129A6F7DCBFC0DCC057DE (void);
// 0x000004EB System.Void BNG.InputBridge::OnEnable()
extern void InputBridge_OnEnable_mE0090169C1789A8065226D8ECB1847C34D6E24B8 (void);
// 0x000004EC System.Void BNG.InputBridge::OnDisable()
extern void InputBridge_OnDisable_mFD2F5D92145A91C970C9FA42BA25F01EC32EE956 (void);
// 0x000004ED System.Void BNG.InputBridge::Update()
extern void InputBridge_Update_mE1791849A3908E37C3946C15AEE3924082232A53 (void);
// 0x000004EE System.Void BNG.InputBridge::UpdateInputs()
extern void InputBridge_UpdateInputs_mCFE1CF0F0147793036EDA78FCE445EE47F9A0A2E (void);
// 0x000004EF System.Void BNG.InputBridge::UpdateSteamInput()
extern void InputBridge_UpdateSteamInput_m12A08CA2F5269D1D8C70A8B29B62DE6F15B68FFD (void);
// 0x000004F0 System.Void BNG.InputBridge::UpdateXRInput()
extern void InputBridge_UpdateXRInput_m1A87D75C6F55A672C368603CFEA4160BC4314C92 (void);
// 0x000004F1 System.Void BNG.InputBridge::UpdateUnityInput()
extern void InputBridge_UpdateUnityInput_m9EC65681307756857CBA54933CDB249E405EFBC4 (void);
// 0x000004F2 System.Void BNG.InputBridge::CreateUnityInputActions()
extern void InputBridge_CreateUnityInputActions_m5A8F5241C3641D078500FADCE49C51ACD377BD73 (void);
// 0x000004F3 System.Void BNG.InputBridge::EnableActions()
extern void InputBridge_EnableActions_m07F2744EF6B4A1235C87364B2248A8D99F497071 (void);
// 0x000004F4 System.Void BNG.InputBridge::DisableActions()
extern void InputBridge_DisableActions_m7BDD89C2BF319485D30315FD2CB4DCE843E13BD5 (void);
// 0x000004F5 UnityEngine.InputSystem.InputAction BNG.InputBridge::CreateInputAction(System.String,System.String,System.Boolean)
extern void InputBridge_CreateInputAction_m25683D4A7E07054EEC1440682FB6847A9934CFF8 (void);
// 0x000004F6 System.Void BNG.InputBridge::UpdateOVRInput()
extern void InputBridge_UpdateOVRInput_m56DA1124B336C9156E50244C31FCA46444AD7DAA (void);
// 0x000004F7 System.Void BNG.InputBridge::UpdatePicoInput()
extern void InputBridge_UpdatePicoInput_m6DAFF4C75A5C0CDD1BBA6D76E0BAD6C7F674E373 (void);
// 0x000004F8 System.Void BNG.InputBridge::UpdateDeviceActive()
extern void InputBridge_UpdateDeviceActive_m9352C145C5C95F1770C10DCF8AEE85D43335F0C5 (void);
// 0x000004F9 System.Single BNG.InputBridge::correctValue(System.Single)
extern void InputBridge_correctValue_mA5FBFF1AB93AC7021FF359ED128B12E5587CA2A7 (void);
// 0x000004FA System.Boolean BNG.InputBridge::GetControllerBindingValue(BNG.ControllerBinding)
extern void InputBridge_GetControllerBindingValue_m07A3152C0B22E686A900F0757B661030D455AE26 (void);
// 0x000004FB System.Boolean BNG.InputBridge::GetGrabbedControllerBinding(BNG.GrabbedControllerBinding,BNG.ControllerHand)
extern void InputBridge_GetGrabbedControllerBinding_m159190D760A8D3F454DEBB245A3DD042C478E73A (void);
// 0x000004FC UnityEngine.Vector2 BNG.InputBridge::GetInputAxisValue(BNG.InputAxis)
extern void InputBridge_GetInputAxisValue_mC39B86EC8C4D42A4E5A2A1BD8CB1DFBB205D7D4E (void);
// 0x000004FD UnityEngine.Vector2 BNG.InputBridge::ApplyDeadZones(UnityEngine.Vector2,System.Single,System.Single)
extern void InputBridge_ApplyDeadZones_m5B904E35A029F47B6D66EDB033E8E81619FDB9C6 (void);
// 0x000004FE System.Void BNG.InputBridge::onDeviceChanged(UnityEngine.XR.InputDevice)
extern void InputBridge_onDeviceChanged_m18ED7F8F2E84929CB1407F8F030ED7E66D8F4B76 (void);
// 0x000004FF System.Void BNG.InputBridge::setDeviceProperties()
extern void InputBridge_setDeviceProperties_mBA8E3BCCFF994C4448D38570453A973D021B50F7 (void);
// 0x00000500 System.Boolean BNG.InputBridge::GetSupportsIndexTouch()
extern void InputBridge_GetSupportsIndexTouch_mAAA457A6BFE26952595924F9066EED1301F93B56 (void);
// 0x00000501 BNG.SDKProvider BNG.InputBridge::GetLoadedSDK()
extern void InputBridge_GetLoadedSDK_mA7070D8B563CED25B49CB228E7422FA93195AB04 (void);
// 0x00000502 System.Boolean BNG.InputBridge::GetSupportsThumbTouch()
extern void InputBridge_GetSupportsThumbTouch_mE9167083B163BFCD8B20B2699D7435454F995ADA (void);
// 0x00000503 System.Boolean BNG.InputBridge::GetIsOculusDevice()
extern void InputBridge_GetIsOculusDevice_mC3E917E2ABE12517F7A791F7FDCA9D382E0886FD (void);
// 0x00000504 System.Boolean BNG.InputBridge::GetIsOculusQuest()
extern void InputBridge_GetIsOculusQuest_m6ECF1DEA3FC5D019B89091804F05DCA5C4F879C4 (void);
// 0x00000505 System.Boolean BNG.InputBridge::GetIsHTCDevice()
extern void InputBridge_GetIsHTCDevice_mFC6024745F11A84B64473C4F5960BC44D242C906 (void);
// 0x00000506 System.Boolean BNG.InputBridge::GetIsPicoDevice()
extern void InputBridge_GetIsPicoDevice_m49CE872BA505B7019C729F515D387D657A837A16 (void);
// 0x00000507 UnityEngine.XR.InputDevice BNG.InputBridge::GetHMD()
extern void InputBridge_GetHMD_m49D54E850CFE44ED97900BEFA52C582E2D5222E0 (void);
// 0x00000508 System.String BNG.InputBridge::GetHMDName()
extern void InputBridge_GetHMDName_mF2D89D4B9B70CC5F8B4278861813BCF254B7385D (void);
// 0x00000509 UnityEngine.Vector3 BNG.InputBridge::GetHMDLocalPosition()
extern void InputBridge_GetHMDLocalPosition_m7647C28BCDD62809323C54285AA1E0A8B91F70EF (void);
// 0x0000050A UnityEngine.Quaternion BNG.InputBridge::GetHMDLocalRotation()
extern void InputBridge_GetHMDLocalRotation_mD279A46F7E9AD50D6CC5BB59E8E6AE4BD11F18C4 (void);
// 0x0000050B UnityEngine.XR.InputDevice BNG.InputBridge::GetLeftController()
extern void InputBridge_GetLeftController_m989E19A30F39B5E160A84BB8DA0CCEC40A649C97 (void);
// 0x0000050C UnityEngine.XR.InputDevice BNG.InputBridge::GetRightController()
extern void InputBridge_GetRightController_m42FDF08DEF79C178A34EC6FEBA21189BFDC48582 (void);
// 0x0000050D UnityEngine.Vector3 BNG.InputBridge::GetControllerLocalPosition(BNG.ControllerHand)
extern void InputBridge_GetControllerLocalPosition_mB4B0FF0E1E1A45D92B7A4DA72A759FA2D80A3504 (void);
// 0x0000050E UnityEngine.Quaternion BNG.InputBridge::GetControllerLocalRotation(BNG.ControllerHand)
extern void InputBridge_GetControllerLocalRotation_m67D864C04E0BE31773F0C66268769DD3B780D71C (void);
// 0x0000050F BNG.ControllerType BNG.InputBridge::GetControllerType()
extern void InputBridge_GetControllerType_mAED7CEF5C409788CA13E9AC6B312B426A6FCF9E8 (void);
// 0x00000510 UnityEngine.Vector3 BNG.InputBridge::GetControllerVelocity(BNG.ControllerHand)
extern void InputBridge_GetControllerVelocity_m7A01A62BD88C2BED3673812B6FB79CA4A2DF07F7 (void);
// 0x00000511 UnityEngine.Vector3 BNG.InputBridge::GetControllerAngularVelocity(BNG.ControllerHand)
extern void InputBridge_GetControllerAngularVelocity_m7ED306C1388381F07FCFE334628DC8043D4497AD (void);
// 0x00000512 System.String BNG.InputBridge::GetControllerName()
extern void InputBridge_GetControllerName_mE862E78ADC082FCCE4C7477318D83D4F617C0BED (void);
// 0x00000513 System.Boolean BNG.InputBridge::GetIsValveIndexController()
extern void InputBridge_GetIsValveIndexController_m56DC783A7F85FD1D822335723F62E2FCE3FFF6E5 (void);
// 0x00000514 System.Single BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<System.Single>,System.Boolean)
extern void InputBridge_getFeatureUsage_m434F9D60BFC69445F8D4A53DA4E6CD0A392AF80B (void);
// 0x00000515 System.Boolean BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<System.Boolean>)
extern void InputBridge_getFeatureUsage_m9E70F7B8F42E8CE97908AE5055531FBD908EB6F1 (void);
// 0x00000516 UnityEngine.Vector2 BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>)
extern void InputBridge_getFeatureUsage_mA997A4BFF2FDE7B3374AE6E9A2CC3B6B69D8836A (void);
// 0x00000517 UnityEngine.Vector3 BNG.InputBridge::getFeatureUsage(UnityEngine.XR.InputDevice,UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>)
extern void InputBridge_getFeatureUsage_mFE17C292F940C0B11AC498566EF2479F360641F5 (void);
// 0x00000518 System.Void BNG.InputBridge::SetTrackingOriginMode(UnityEngine.XR.TrackingOriginModeFlags)
extern void InputBridge_SetTrackingOriginMode_m6DC2D27EA9383AB706A3EDF487F373E7B09D59FC (void);
// 0x00000519 System.Collections.IEnumerator BNG.InputBridge::changeOriginModeRoutine(UnityEngine.XR.TrackingOriginModeFlags)
extern void InputBridge_changeOriginModeRoutine_mE049B21FD5040978BC79B40F7F2F3A9D4B3F7D0D (void);
// 0x0000051A System.Void BNG.InputBridge::VibrateController(System.Single,System.Single,System.Single,BNG.ControllerHand)
extern void InputBridge_VibrateController_mEBABD84505D3B27040CD1270A944B1CDB1370D24 (void);
// 0x0000051B System.Collections.IEnumerator BNG.InputBridge::Vibrate(System.Single,System.Single,System.Single,BNG.ControllerHand)
extern void InputBridge_Vibrate_mA6AE668E28F7566738991738A550094BF13FAB31 (void);
// 0x0000051C System.Void BNG.InputBridge::.ctor()
extern void InputBridge__ctor_m74F6352F4E3583619D9080EAF20B2941143CDB16 (void);
// 0x0000051D System.Void BNG.InputBridge::.cctor()
extern void InputBridge__cctor_m332B8E441C603AB40A24606C699DC397DAECEAB5 (void);
// 0x0000051E System.Void BNG.InputBridge/InputsUpdatedAction::.ctor(System.Object,System.IntPtr)
extern void InputsUpdatedAction__ctor_m360F6975296F536063F73AB29E0C63300882A3A5 (void);
// 0x0000051F System.Void BNG.InputBridge/InputsUpdatedAction::Invoke()
extern void InputsUpdatedAction_Invoke_mC2B73312B9B94114BE87444C4E24D62CC452B73E (void);
// 0x00000520 System.IAsyncResult BNG.InputBridge/InputsUpdatedAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void InputsUpdatedAction_BeginInvoke_m9C82DDA52ABFB7A0A422FC7A0E73322E28A6DADA (void);
// 0x00000521 System.Void BNG.InputBridge/InputsUpdatedAction::EndInvoke(System.IAsyncResult)
extern void InputsUpdatedAction_EndInvoke_mF72AEF555E870969FC90239B53173F3A225A3501 (void);
// 0x00000522 System.Void BNG.InputBridge/ControllerFoundAction::.ctor(System.Object,System.IntPtr)
extern void ControllerFoundAction__ctor_m21AE2636C135E39538AD2E38C2F417F3EC78954E (void);
// 0x00000523 System.Void BNG.InputBridge/ControllerFoundAction::Invoke()
extern void ControllerFoundAction_Invoke_m7F67E9C2FF2391C3186F120510962E66DCC399D4 (void);
// 0x00000524 System.IAsyncResult BNG.InputBridge/ControllerFoundAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void ControllerFoundAction_BeginInvoke_mB34A751473298D43E61F581649805881217CC815 (void);
// 0x00000525 System.Void BNG.InputBridge/ControllerFoundAction::EndInvoke(System.IAsyncResult)
extern void ControllerFoundAction_EndInvoke_mDF8EC6F114A3558E0EB14D258AFA3A5519B88A85 (void);
// 0x00000526 System.Void BNG.InputBridge/<changeOriginModeRoutine>d__167::.ctor(System.Int32)
extern void U3CchangeOriginModeRoutineU3Ed__167__ctor_m19D66D7A27ED416DA759633845159E97D0DCEA02 (void);
// 0x00000527 System.Void BNG.InputBridge/<changeOriginModeRoutine>d__167::System.IDisposable.Dispose()
extern void U3CchangeOriginModeRoutineU3Ed__167_System_IDisposable_Dispose_m09B2DCD72D31BDB49B74BB60C06D01502E535701 (void);
// 0x00000528 System.Boolean BNG.InputBridge/<changeOriginModeRoutine>d__167::MoveNext()
extern void U3CchangeOriginModeRoutineU3Ed__167_MoveNext_m8BFD7892CB70501AE3F9C8B3AB8918C15F3CC2EE (void);
// 0x00000529 System.Object BNG.InputBridge/<changeOriginModeRoutine>d__167::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CchangeOriginModeRoutineU3Ed__167_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m571780F0E4B72EA3A9D605ABE2795B135A4C4740 (void);
// 0x0000052A System.Void BNG.InputBridge/<changeOriginModeRoutine>d__167::System.Collections.IEnumerator.Reset()
extern void U3CchangeOriginModeRoutineU3Ed__167_System_Collections_IEnumerator_Reset_mC66980BCC2C2CBFEDF6327C33AB04DF84C0B0520 (void);
// 0x0000052B System.Object BNG.InputBridge/<changeOriginModeRoutine>d__167::System.Collections.IEnumerator.get_Current()
extern void U3CchangeOriginModeRoutineU3Ed__167_System_Collections_IEnumerator_get_Current_m8A6A271228407362740AD6981CEDB8BA72ADFCC4 (void);
// 0x0000052C System.Void BNG.InputBridge/<Vibrate>d__169::.ctor(System.Int32)
extern void U3CVibrateU3Ed__169__ctor_m28E5CB17BA3E444881EA7F13B4EA29FAA1979595 (void);
// 0x0000052D System.Void BNG.InputBridge/<Vibrate>d__169::System.IDisposable.Dispose()
extern void U3CVibrateU3Ed__169_System_IDisposable_Dispose_mEEDC5671731499C44319ACADD3B1BF1D424DA293 (void);
// 0x0000052E System.Boolean BNG.InputBridge/<Vibrate>d__169::MoveNext()
extern void U3CVibrateU3Ed__169_MoveNext_m24E772D3ACBA5C8464AC6FA008475F8B97022F93 (void);
// 0x0000052F System.Object BNG.InputBridge/<Vibrate>d__169::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVibrateU3Ed__169_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A3A6BA47441B7C74B19B37610E845E49CB0587B (void);
// 0x00000530 System.Void BNG.InputBridge/<Vibrate>d__169::System.Collections.IEnumerator.Reset()
extern void U3CVibrateU3Ed__169_System_Collections_IEnumerator_Reset_m025EF535DCCA4475D4DD979DF4E520B9E3FFCD4A (void);
// 0x00000531 System.Object BNG.InputBridge/<Vibrate>d__169::System.Collections.IEnumerator.get_Current()
extern void U3CVibrateU3Ed__169_System_Collections_IEnumerator_get_Current_mB0B6650017C2A3833AEEFE87CA9185456EDFFB81 (void);
// 0x00000532 System.Void BNG.JoystickControl::Start()
extern void JoystickControl_Start_m0820A032E321BA7CDEC9E6D3DE5092E51F236AE0 (void);
// 0x00000533 System.Void BNG.JoystickControl::Update()
extern void JoystickControl_Update_m897F2A367792D6D8016142D89BC49994D8D74CAB (void);
// 0x00000534 System.Void BNG.JoystickControl::FixedUpdate()
extern void JoystickControl_FixedUpdate_m1766D5FC87C6F42FD7017F6DFC6DF85664B10779 (void);
// 0x00000535 System.Void BNG.JoystickControl::doJoystickLook()
extern void JoystickControl_doJoystickLook_m442D08E200B8BB5F2BE718CE40ED4EA693ECF79E (void);
// 0x00000536 System.Void BNG.JoystickControl::OnJoystickChange(System.Single,System.Single)
extern void JoystickControl_OnJoystickChange_m382439CBB64D911B225FD13E935540686D17C724 (void);
// 0x00000537 System.Void BNG.JoystickControl::OnJoystickChange(UnityEngine.Vector2)
extern void JoystickControl_OnJoystickChange_mCCBE1BD07BE518145C1A93E861D83F5DABF12A39 (void);
// 0x00000538 System.Void BNG.JoystickControl::.ctor()
extern void JoystickControl__ctor_m31B70DA765DB49D71B12256F9AD3E000AED9AD87 (void);
// 0x00000539 System.Void BNG.JoystickVehicleControl::Update()
extern void JoystickVehicleControl_Update_mC2F3B1AB33A32C5D4B1F0EBEA85E95535B0518BA (void);
// 0x0000053A System.Void BNG.JoystickVehicleControl::CallJoystickEvents()
extern void JoystickVehicleControl_CallJoystickEvents_m79E1BA73723D2CB1E2A5AC22A792B435911011A3 (void);
// 0x0000053B System.Void BNG.JoystickVehicleControl::OnJoystickChange(System.Single,System.Single)
extern void JoystickVehicleControl_OnJoystickChange_mB80DADB5AC389C4F53160523F827ABADD7A01673 (void);
// 0x0000053C System.Void BNG.JoystickVehicleControl::OnJoystickChange(UnityEngine.Vector2)
extern void JoystickVehicleControl_OnJoystickChange_mC6FF2E24344D9E49774ABDD2269727649B784704 (void);
// 0x0000053D System.Void BNG.JoystickVehicleControl::.ctor()
extern void JoystickVehicleControl__ctor_m8BB283D15C374BDD82A95BD117A363F090C2C759 (void);
// 0x0000053E System.Void BNG.Lever::Start()
extern void Lever_Start_m53227282DEF0F2B6C4BB951B8AE9693F36985D2B (void);
// 0x0000053F System.Void BNG.Lever::Awake()
extern void Lever_Awake_mD541F2BD89B857B774B139DAE81A6180E2A8FB81 (void);
// 0x00000540 System.Void BNG.Lever::Update()
extern void Lever_Update_m43BD025BFB2F66B5CC63CEFE65B919FEFA677730 (void);
// 0x00000541 System.Single BNG.Lever::GetAnglePercentage(System.Single)
extern void Lever_GetAnglePercentage_m7512BABDF0E90207A7C715EED7C76CAC52AEBF65 (void);
// 0x00000542 System.Void BNG.Lever::FixedUpdate()
extern void Lever_FixedUpdate_mE36457E118FA726A62951D85056EC60FA3F87302 (void);
// 0x00000543 System.Void BNG.Lever::doLeverLook()
extern void Lever_doLeverLook_m348D3FBF91F66463124BD3547773BCB18F541E30 (void);
// 0x00000544 System.Void BNG.Lever::SetLeverAngle(System.Single)
extern void Lever_SetLeverAngle_m37A96D5D1BF8DB364BD9C45A6DC71F4A9EC4D44E (void);
// 0x00000545 System.Void BNG.Lever::OnLeverChange(System.Single)
extern void Lever_OnLeverChange_m21DF343D12C33DA5B9AA19D66F009D4ED6FEF196 (void);
// 0x00000546 System.Void BNG.Lever::OnLeverDown()
extern void Lever_OnLeverDown_m4D76CB7DA3C5A2C614ADA934F889347CF5C645D5 (void);
// 0x00000547 System.Void BNG.Lever::OnLeverUp()
extern void Lever_OnLeverUp_m0B3290543E96A2DBBB9D9132801D015EA6F41F87 (void);
// 0x00000548 System.Void BNG.Lever::.ctor()
extern void Lever__ctor_mD33714113CDDCF44D4D3504C4ACCBC37EE583AD3 (void);
// 0x00000549 BNG.LocomotionType BNG.LocomotionManager::get_SelectedLocomotion()
extern void LocomotionManager_get_SelectedLocomotion_mB295CB8B23987D006F23175C0FFF9AE19F1A835B (void);
// 0x0000054A System.Void BNG.LocomotionManager::Start()
extern void LocomotionManager_Start_mE4A8CC4B5FB1300E38258BBBEF10A21DF82311BF (void);
// 0x0000054B System.Void BNG.LocomotionManager::Update()
extern void LocomotionManager_Update_m7C73D1369E0CB7C3E31539046D02897BEC65A962 (void);
// 0x0000054C System.Void BNG.LocomotionManager::CheckControllerToggleInput()
extern void LocomotionManager_CheckControllerToggleInput_m7CDB086FCA4CDDC39273B4EE9472D3BD2686F98E (void);
// 0x0000054D System.Void BNG.LocomotionManager::OnEnable()
extern void LocomotionManager_OnEnable_mA3F2167FA7A93427F07C8B60B3C27D78725DE8D0 (void);
// 0x0000054E System.Void BNG.LocomotionManager::OnDisable()
extern void LocomotionManager_OnDisable_m0AA68FFC363A95A5C73F1AC1D0C39441CB2A1B0C (void);
// 0x0000054F System.Void BNG.LocomotionManager::OnLocomotionToggle(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void LocomotionManager_OnLocomotionToggle_mA52DEE8F160FFE374CAACC4B282954A91B2CF9C0 (void);
// 0x00000550 System.Void BNG.LocomotionManager::LocomotionToggle()
extern void LocomotionManager_LocomotionToggle_m9E1306FB627284F2DC98D1EF825C1DDF05B7C350 (void);
// 0x00000551 System.Void BNG.LocomotionManager::UpdateTeleportStatus()
extern void LocomotionManager_UpdateTeleportStatus_m3E860ED4E78B3E055E1C77B47AFD2BABF4B83680 (void);
// 0x00000552 System.Void BNG.LocomotionManager::ChangeLocomotion(BNG.LocomotionType,System.Boolean)
extern void LocomotionManager_ChangeLocomotion_m299799C1A7780B923CE56F4C64CD280046874EAA (void);
// 0x00000553 System.Void BNG.LocomotionManager::ChangeLocomotionType(BNG.LocomotionType)
extern void LocomotionManager_ChangeLocomotionType_m0FA767E9D8CD6538565236B55AA1DB4F1769BC9E (void);
// 0x00000554 System.Void BNG.LocomotionManager::toggleTeleport(System.Boolean)
extern void LocomotionManager_toggleTeleport_m0AFC12FBDD60ED900A503A28D10DCCC72DB185D7 (void);
// 0x00000555 System.Void BNG.LocomotionManager::toggleSmoothLocomotion(System.Boolean)
extern void LocomotionManager_toggleSmoothLocomotion_mD27E85771E24CDADC2C0D9E8D6E1D5EB66C4EB2B (void);
// 0x00000556 System.Void BNG.LocomotionManager::ToggleLocomotionType()
extern void LocomotionManager_ToggleLocomotionType_m7302FFF1199E9C9CBA4F3EC8D1B4E4BBBFBF9C7A (void);
// 0x00000557 System.Void BNG.LocomotionManager::.ctor()
extern void LocomotionManager__ctor_mE330809BA37C393E52AE3818F16B918B34FC5769 (void);
// 0x00000558 System.Boolean BNG.PlayerClimbing::get_IsRigidbodyPlayer()
extern void PlayerClimbing_get_IsRigidbodyPlayer_m6396F6BF644D9E7AC298C5CA6D41664337D03588 (void);
// 0x00000559 System.Void BNG.PlayerClimbing::Start()
extern void PlayerClimbing_Start_mB8FF07D00D98FE89FB7E54FACCFE7EBD4D4D20D4 (void);
// 0x0000055A System.Void BNG.PlayerClimbing::LateUpdate()
extern void PlayerClimbing_LateUpdate_m88A9D6C21F6A4DED6A8B6E7A11D1F0EFF945A173 (void);
// 0x0000055B System.Void BNG.PlayerClimbing::AddClimber(BNG.Climbable,BNG.Grabber)
extern void PlayerClimbing_AddClimber_m179B7830F814B556D7A20765BBE9C402E76A4BC9 (void);
// 0x0000055C System.Void BNG.PlayerClimbing::RemoveClimber(BNG.Grabber)
extern void PlayerClimbing_RemoveClimber_mA994DB6BDEAF53BFAE21E37E935243248033A3DE (void);
// 0x0000055D System.Boolean BNG.PlayerClimbing::GrippingAtLeastOneClimbable()
extern void PlayerClimbing_GrippingAtLeastOneClimbable_m06F651E863E090AF5D8F254607A2189BABA5A970 (void);
// 0x0000055E System.Void BNG.PlayerClimbing::checkClimbing()
extern void PlayerClimbing_checkClimbing_mA3B785215BEE062E080E57CDD8EBB4E5ED506BC1 (void);
// 0x0000055F System.Void BNG.PlayerClimbing::DoPhysicalClimbing()
extern void PlayerClimbing_DoPhysicalClimbing_m9FB735E387E3B1AC6B5A1E31E5BF27C3808296B4 (void);
// 0x00000560 System.Void BNG.PlayerClimbing::onGrabbedClimbable()
extern void PlayerClimbing_onGrabbedClimbable_m4D61F329DF96A6C9A5FF04F0764D7C66C79B82EE (void);
// 0x00000561 System.Void BNG.PlayerClimbing::onReleasedClimbable()
extern void PlayerClimbing_onReleasedClimbable_m74F8FC514F1B81371EB627A4F166FCF2F4877082 (void);
// 0x00000562 System.Void BNG.PlayerClimbing::.ctor()
extern void PlayerClimbing__ctor_m3A86793E9DCE779DCAED703E73670B86F19481D8 (void);
// 0x00000563 System.Void BNG.PlayerGravity::Start()
extern void PlayerGravity_Start_m9F4334F375B7C8AD2BCE8B30228E53AA2D5EDC87 (void);
// 0x00000564 System.Void BNG.PlayerGravity::LateUpdate()
extern void PlayerGravity_LateUpdate_mCD21DFCF41BC9F1A59B46CD193A2277491B8A868 (void);
// 0x00000565 System.Void BNG.PlayerGravity::FixedUpdate()
extern void PlayerGravity_FixedUpdate_m9F6A208F1E74C46DD094DADAD07EF91F7EA5C1FA (void);
// 0x00000566 System.Void BNG.PlayerGravity::ToggleGravity(System.Boolean)
extern void PlayerGravity_ToggleGravity_mE2B524AA026443B4733C23593D3978304CA6419C (void);
// 0x00000567 System.Void BNG.PlayerGravity::.ctor()
extern void PlayerGravity__ctor_mAA2A73A9AE36928E8769896B26E1FE26BED4AF84 (void);
// 0x00000568 System.Void BNG.PlayerMovingPlatformSupport::Start()
extern void PlayerMovingPlatformSupport_Start_mC59934B81E2B7C5231CD337BD717EF934FF09C20 (void);
// 0x00000569 System.Void BNG.PlayerMovingPlatformSupport::Update()
extern void PlayerMovingPlatformSupport_Update_m0EE2883881477E2B6B737BEA8C215181B8695504 (void);
// 0x0000056A System.Void BNG.PlayerMovingPlatformSupport::FixedUpdate()
extern void PlayerMovingPlatformSupport_FixedUpdate_m4AE812CFF6D278B044E21CAC02D09D2C7D83BA46 (void);
// 0x0000056B System.Void BNG.PlayerMovingPlatformSupport::CheckMovingPlatform()
extern void PlayerMovingPlatformSupport_CheckMovingPlatform_m1979E87F9D9019EE0DE446C9E19B863A1CFD3778 (void);
// 0x0000056C System.Void BNG.PlayerMovingPlatformSupport::UpdateCurrentPlatform()
extern void PlayerMovingPlatformSupport_UpdateCurrentPlatform_m750B525028BD41EB74826C7AD79B0C70209EE73A (void);
// 0x0000056D System.Void BNG.PlayerMovingPlatformSupport::UpdateDistanceFromGround()
extern void PlayerMovingPlatformSupport_UpdateDistanceFromGround_m7D4A2E502702929F247B354A706F54A573528278 (void);
// 0x0000056E System.Void BNG.PlayerMovingPlatformSupport::.ctor()
extern void PlayerMovingPlatformSupport__ctor_m22F084302C7E5A9DD1CA709D9F3C6717D14765C6 (void);
// 0x0000056F System.Void BNG.PlayerRotation::add_OnBeforeRotate(BNG.PlayerRotation/OnBeforeRotateAction)
extern void PlayerRotation_add_OnBeforeRotate_mF0223343545F987770547880405BE64F32C0D7E5 (void);
// 0x00000570 System.Void BNG.PlayerRotation::remove_OnBeforeRotate(BNG.PlayerRotation/OnBeforeRotateAction)
extern void PlayerRotation_remove_OnBeforeRotate_mE41DCBB9998C196033359C547F1CB69184E39F7B (void);
// 0x00000571 System.Void BNG.PlayerRotation::add_OnAfterRotate(BNG.PlayerRotation/OnAfterRotateAction)
extern void PlayerRotation_add_OnAfterRotate_m09A85C75DA4B2B38F1B3D4B7406EBC9CCE5F757D (void);
// 0x00000572 System.Void BNG.PlayerRotation::remove_OnAfterRotate(BNG.PlayerRotation/OnAfterRotateAction)
extern void PlayerRotation_remove_OnAfterRotate_m390BBB4B34F913B77180F0419E3CF8FDB274E5AD (void);
// 0x00000573 System.Void BNG.PlayerRotation::Update()
extern void PlayerRotation_Update_m92ADD5BA99D93CBFD61B6A1BF05093A0A5686730 (void);
// 0x00000574 System.Single BNG.PlayerRotation::GetAxisInput()
extern void PlayerRotation_GetAxisInput_m7096600D15E9350D8A8ECE421701AD64BEB6BB67 (void);
// 0x00000575 System.Void BNG.PlayerRotation::DoSnapRotation(System.Single)
extern void PlayerRotation_DoSnapRotation_m5DCD72E35D4DD75FA2BCA71FA7F24ACBBFF1539A (void);
// 0x00000576 System.Boolean BNG.PlayerRotation::RecentlySnapTurned()
extern void PlayerRotation_RecentlySnapTurned_m13E4CC485969CC38B8A253EDF3D1681B8F23D629 (void);
// 0x00000577 System.Void BNG.PlayerRotation::DoSmoothRotation(System.Single)
extern void PlayerRotation_DoSmoothRotation_m730E3BFB00ED2DB3A491717A3BBD84C4A1E29B9E (void);
// 0x00000578 System.Void BNG.PlayerRotation::.ctor()
extern void PlayerRotation__ctor_mBB17C60513064A3B901DDA4D9E14C2702D481318 (void);
// 0x00000579 System.Void BNG.PlayerRotation/OnBeforeRotateAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeRotateAction__ctor_m1EF78B6B9790E23C8DCB93C51E8C59714FEF2ECF (void);
// 0x0000057A System.Void BNG.PlayerRotation/OnBeforeRotateAction::Invoke()
extern void OnBeforeRotateAction_Invoke_m4DEEFF738582D5E9664D4D2376E99229361FB259 (void);
// 0x0000057B System.IAsyncResult BNG.PlayerRotation/OnBeforeRotateAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeRotateAction_BeginInvoke_m76CF95B37C5930D8E594AF978513E3F9AB665C70 (void);
// 0x0000057C System.Void BNG.PlayerRotation/OnBeforeRotateAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeRotateAction_EndInvoke_m40D2450181DA0D1E110888C76537A8C53BFA1239 (void);
// 0x0000057D System.Void BNG.PlayerRotation/OnAfterRotateAction::.ctor(System.Object,System.IntPtr)
extern void OnAfterRotateAction__ctor_m5A2321B790478FA7068CE835FAF1A465C85C2F7D (void);
// 0x0000057E System.Void BNG.PlayerRotation/OnAfterRotateAction::Invoke()
extern void OnAfterRotateAction_Invoke_m7C5013254BB6103D77AAA90044C05F5B827B02AF (void);
// 0x0000057F System.IAsyncResult BNG.PlayerRotation/OnAfterRotateAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAfterRotateAction_BeginInvoke_m6EA0E6C2CD692AF66B377D7433A293DA429A3B7E (void);
// 0x00000580 System.Void BNG.PlayerRotation/OnAfterRotateAction::EndInvoke(System.IAsyncResult)
extern void OnAfterRotateAction_EndInvoke_m06864B0931CCFC57BE6AA827955AD7E9691AEBCB (void);
// 0x00000581 UnityEngine.Transform BNG.PlayerTeleport::get_teleportTransform()
extern void PlayerTeleport_get_teleportTransform_mE9570AB261F46715F6592FE7E40F98E492FED84B (void);
// 0x00000582 UnityEngine.Vector2 BNG.PlayerTeleport::get_handedThumbstickAxis()
extern void PlayerTeleport_get_handedThumbstickAxis_m7FEC1BBB2859464779C62524C8334536A92616C6 (void);
// 0x00000583 System.Boolean BNG.PlayerTeleport::get_AimingTeleport()
extern void PlayerTeleport_get_AimingTeleport_m6F40D70E70E6C17C8DB873A4DDBCAFF2DAD6DA84 (void);
// 0x00000584 System.Void BNG.PlayerTeleport::add_OnBeforeTeleportFade(BNG.PlayerTeleport/OnBeforeTeleportFadeAction)
extern void PlayerTeleport_add_OnBeforeTeleportFade_m6E90AFAA42EC45F096A28BA098B6A44661325A4F (void);
// 0x00000585 System.Void BNG.PlayerTeleport::remove_OnBeforeTeleportFade(BNG.PlayerTeleport/OnBeforeTeleportFadeAction)
extern void PlayerTeleport_remove_OnBeforeTeleportFade_m2B21D90D43482C6125CEF69A41D00863B7B64308 (void);
// 0x00000586 System.Void BNG.PlayerTeleport::add_OnBeforeTeleport(BNG.PlayerTeleport/OnBeforeTeleportAction)
extern void PlayerTeleport_add_OnBeforeTeleport_mADD5036E955F131399261947C6BB1885283AF1F0 (void);
// 0x00000587 System.Void BNG.PlayerTeleport::remove_OnBeforeTeleport(BNG.PlayerTeleport/OnBeforeTeleportAction)
extern void PlayerTeleport_remove_OnBeforeTeleport_m00615730F0C789127763E8C79CB6B70CC8EE421D (void);
// 0x00000588 System.Void BNG.PlayerTeleport::add_OnAfterTeleport(BNG.PlayerTeleport/OnAfterTeleportAction)
extern void PlayerTeleport_add_OnAfterTeleport_mA89DC1B2A5721EC3990DCBA3C02BE92BD219D718 (void);
// 0x00000589 System.Void BNG.PlayerTeleport::remove_OnAfterTeleport(BNG.PlayerTeleport/OnAfterTeleportAction)
extern void PlayerTeleport_remove_OnAfterTeleport_mB4ABB6321EDB753429829E2742CE7131A82C14EB (void);
// 0x0000058A System.Void BNG.PlayerTeleport::Start()
extern void PlayerTeleport_Start_m8A19BF3BF47D1F80219F2E35F55960BCDD875388 (void);
// 0x0000058B System.Void BNG.PlayerTeleport::OnEnable()
extern void PlayerTeleport_OnEnable_m77933504C92F801BDB848EECFF7578992E36DB70 (void);
// 0x0000058C System.Void BNG.PlayerTeleport::setupVariables()
extern void PlayerTeleport_setupVariables_mFE3F78B783231F0511D18518AAC0AFD7770433E8 (void);
// 0x0000058D System.Void BNG.PlayerTeleport::LateUpdate()
extern void PlayerTeleport_LateUpdate_mB347E25FA62B083213CFA887A8E7ECD0A5F8F5B1 (void);
// 0x0000058E System.Void BNG.PlayerTeleport::DoCheckTeleport()
extern void PlayerTeleport_DoCheckTeleport_mD5912FFD81D75164880411E7B58C29BD383F0CBF (void);
// 0x0000058F System.Void BNG.PlayerTeleport::TryOrHideTeleport()
extern void PlayerTeleport_TryOrHideTeleport_mB3D6C91B337E5D40D37630E748ABDCCDE5C13277 (void);
// 0x00000590 System.Void BNG.PlayerTeleport::EnableTeleportation()
extern void PlayerTeleport_EnableTeleportation_m34940426FC024639A9C699675D551913CD88A3CC (void);
// 0x00000591 System.Void BNG.PlayerTeleport::DisableTeleportation()
extern void PlayerTeleport_DisableTeleportation_mEAE2FCF6F28F1D66FAC11654C9F98E9F35164914 (void);
// 0x00000592 System.Void BNG.PlayerTeleport::calculateParabola()
extern void PlayerTeleport_calculateParabola_mF3021B306ADE88A709D53FECCCEB2B084FA0D27C (void);
// 0x00000593 System.Boolean BNG.PlayerTeleport::teleportClear()
extern void PlayerTeleport_teleportClear_m1186D6435563C80ED32D6A064350F42DAD270817 (void);
// 0x00000594 System.Void BNG.PlayerTeleport::hideTeleport()
extern void PlayerTeleport_hideTeleport_m3FF93CC9322500ECC69E4D82672C6D8F82586DE6 (void);
// 0x00000595 System.Void BNG.PlayerTeleport::updateTeleport()
extern void PlayerTeleport_updateTeleport_mE35F2659925341B052DAF63D340D8391563F3B7F (void);
// 0x00000596 System.Void BNG.PlayerTeleport::rotateMarker()
extern void PlayerTeleport_rotateMarker_m005A3D67D39F97380DDCFCBAD3D661CA0B4DF794 (void);
// 0x00000597 System.Void BNG.PlayerTeleport::tryTeleport()
extern void PlayerTeleport_tryTeleport_mD912EE5D43441CA0582940B7CD8B1860613737AC (void);
// 0x00000598 System.Void BNG.PlayerTeleport::BeforeTeleportFade()
extern void PlayerTeleport_BeforeTeleportFade_m595A747488784286EE9BC2F88711DF3EE910B8A5 (void);
// 0x00000599 System.Void BNG.PlayerTeleport::BeforeTeleport()
extern void PlayerTeleport_BeforeTeleport_mC805DA63FB6FB339A754A7728CFD46963F153EB4 (void);
// 0x0000059A System.Void BNG.PlayerTeleport::AfterTeleport()
extern void PlayerTeleport_AfterTeleport_m5D91A8A2534550AF02B935E0401146EDDCB4D0DF (void);
// 0x0000059B System.Collections.IEnumerator BNG.PlayerTeleport::doTeleport(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean)
extern void PlayerTeleport_doTeleport_m7D3C540F4C773ADD52EA928D57BEFEAB3E6AE388 (void);
// 0x0000059C System.Void BNG.PlayerTeleport::TeleportPlayer(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void PlayerTeleport_TeleportPlayer_m04B08166527888A474A4DE65B71958952172A318 (void);
// 0x0000059D System.Void BNG.PlayerTeleport::TeleportPlayerToTransform(UnityEngine.Transform)
extern void PlayerTeleport_TeleportPlayerToTransform_m01628DF190E72B1EF425D94171E427C5602173F6 (void);
// 0x0000059E System.Boolean BNG.PlayerTeleport::KeyDownForTeleport()
extern void PlayerTeleport_KeyDownForTeleport_m82AAEBF761EC19213A8CA35AA6590E0FDD0F3D70 (void);
// 0x0000059F System.Boolean BNG.PlayerTeleport::KeyUpFromTeleport()
extern void PlayerTeleport_KeyUpFromTeleport_m55480200F029950509EFC7CDC0A6765311AB5148 (void);
// 0x000005A0 System.Void BNG.PlayerTeleport::OnDrawGizmosSelected()
extern void PlayerTeleport_OnDrawGizmosSelected_m440EB558303BA1223B48213462B302A2A5658EA7 (void);
// 0x000005A1 System.Void BNG.PlayerTeleport::.ctor()
extern void PlayerTeleport__ctor_m0160DD2F0975AB7432D418537525A954712F4BBC (void);
// 0x000005A2 System.Void BNG.PlayerTeleport/OnBeforeTeleportFadeAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeTeleportFadeAction__ctor_m58E84EF8F920BFB61F4BF4FE14F2503E8589870B (void);
// 0x000005A3 System.Void BNG.PlayerTeleport/OnBeforeTeleportFadeAction::Invoke()
extern void OnBeforeTeleportFadeAction_Invoke_m3D6B92F5B8EE8E2704A52FF215EF6964585AD575 (void);
// 0x000005A4 System.IAsyncResult BNG.PlayerTeleport/OnBeforeTeleportFadeAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeTeleportFadeAction_BeginInvoke_m39902D62F50A3385BC148E12512626EFD8CE836E (void);
// 0x000005A5 System.Void BNG.PlayerTeleport/OnBeforeTeleportFadeAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeTeleportFadeAction_EndInvoke_m24771503B84C0CC01DA7C9105952F05DD125051E (void);
// 0x000005A6 System.Void BNG.PlayerTeleport/OnBeforeTeleportAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeTeleportAction__ctor_m7933C0CE981D771BF0BBB37FBF4FF7C14D0CCE35 (void);
// 0x000005A7 System.Void BNG.PlayerTeleport/OnBeforeTeleportAction::Invoke()
extern void OnBeforeTeleportAction_Invoke_m25262B7301500D484F305403BB82B376E0E54C90 (void);
// 0x000005A8 System.IAsyncResult BNG.PlayerTeleport/OnBeforeTeleportAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeTeleportAction_BeginInvoke_m019C9567AB49D9D5645EDB1C5B6EC8FB656AF75F (void);
// 0x000005A9 System.Void BNG.PlayerTeleport/OnBeforeTeleportAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeTeleportAction_EndInvoke_mD0FB8661E01DFF8B60C954E68029E5A4F02760BD (void);
// 0x000005AA System.Void BNG.PlayerTeleport/OnAfterTeleportAction::.ctor(System.Object,System.IntPtr)
extern void OnAfterTeleportAction__ctor_m981EB16D1E3F1CDD181FED4E7D5A30F2DA3B8AEF (void);
// 0x000005AB System.Void BNG.PlayerTeleport/OnAfterTeleportAction::Invoke()
extern void OnAfterTeleportAction_Invoke_mDAAD110BCAA810D51D72339FD48854846798F50B (void);
// 0x000005AC System.IAsyncResult BNG.PlayerTeleport/OnAfterTeleportAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAfterTeleportAction_BeginInvoke_mF4BD7E7F8CC8DD09820763EC70FB320EC6EEA305 (void);
// 0x000005AD System.Void BNG.PlayerTeleport/OnAfterTeleportAction::EndInvoke(System.IAsyncResult)
extern void OnAfterTeleportAction_EndInvoke_m56720E4B0A578197EF411B2F2007C9CE95F6AE75 (void);
// 0x000005AE System.Void BNG.PlayerTeleport/<doTeleport>d__83::.ctor(System.Int32)
extern void U3CdoTeleportU3Ed__83__ctor_mEDE331D0F4E85B954678CDA880D1DE450BD2FE1F (void);
// 0x000005AF System.Void BNG.PlayerTeleport/<doTeleport>d__83::System.IDisposable.Dispose()
extern void U3CdoTeleportU3Ed__83_System_IDisposable_Dispose_m11A5D2437156E9F49F3E4D5A9B532054992CFAF3 (void);
// 0x000005B0 System.Boolean BNG.PlayerTeleport/<doTeleport>d__83::MoveNext()
extern void U3CdoTeleportU3Ed__83_MoveNext_m925233F800A515CB7BBEEBA2CF36AC9BDDD7AAEA (void);
// 0x000005B1 System.Object BNG.PlayerTeleport/<doTeleport>d__83::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoTeleportU3Ed__83_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0842B151A77FAD1DEBB5C7B33D5AFAB99CE92DC (void);
// 0x000005B2 System.Void BNG.PlayerTeleport/<doTeleport>d__83::System.Collections.IEnumerator.Reset()
extern void U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_Reset_mEC4A1F9D50285E26078D76CF648C4A5C6570A9DC (void);
// 0x000005B3 System.Object BNG.PlayerTeleport/<doTeleport>d__83::System.Collections.IEnumerator.get_Current()
extern void U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_get_Current_m9A8D085E00E487C0F81CBB6825FAD59A4B5CEDBF (void);
// 0x000005B4 System.Void BNG.PointerEvents::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerClick_m14DBC43D0859A50E2B74486AD406E58271DF1463 (void);
// 0x000005B5 System.Void BNG.PointerEvents::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerEnter_m9433D65B24BDD23F4ADEDC6F639AAC2CB9CE6F2A (void);
// 0x000005B6 System.Void BNG.PointerEvents::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerExit_mB6C97F3D1BFFD7336D3A439B8C70FB90D1E6971F (void);
// 0x000005B7 System.Void BNG.PointerEvents::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerDown_mE9CD08107B76FA6B6130B04ED0D5ADD7B6DBA4FA (void);
// 0x000005B8 System.Void BNG.PointerEvents::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerUp_m8279725F17687544AF0850B80B729F55499C7DC6 (void);
// 0x000005B9 System.Boolean BNG.PointerEvents::DistanceExceeded(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_DistanceExceeded_m0735A61C4D72234D3831B1843791C789C66663D4 (void);
// 0x000005BA System.Void BNG.PointerEvents::.ctor()
extern void PointerEvents__ctor_m4C675AD8A01C5367543C75942A7A8776ED1EB5C2 (void);
// 0x000005BB System.Void BNG.RemoteGrabber::Start()
extern void RemoteGrabber_Start_m2D6F32E14B84D9285176CD7E1E95AE0CAFDB0C78 (void);
// 0x000005BC System.Void BNG.RemoteGrabber::Update()
extern void RemoteGrabber_Update_m5215B10FE1DC83CA2B4B7C50282CC1EA15E41445 (void);
// 0x000005BD System.Void BNG.RemoteGrabber::ObjectHit(UnityEngine.Collider)
extern void RemoteGrabber_ObjectHit_mC54104C19B29069449CDC6A1CDB801D297622545 (void);
// 0x000005BE System.Void BNG.RemoteGrabber::RemovePreviousHitObject()
extern void RemoteGrabber_RemovePreviousHitObject_mD5B70C28DE6CFB59EDCCECAC58EFDF48EA8EE3BA (void);
// 0x000005BF System.Void BNG.RemoteGrabber::OnTriggerEnter(UnityEngine.Collider)
extern void RemoteGrabber_OnTriggerEnter_m65FB61A488F74554051038ABA996942546D155DB (void);
// 0x000005C0 System.Void BNG.RemoteGrabber::OnTriggerExit(UnityEngine.Collider)
extern void RemoteGrabber_OnTriggerExit_m19E38A5EA061205A5C29F155118D15E2E2FCBC75 (void);
// 0x000005C1 System.Void BNG.RemoteGrabber::.ctor()
extern void RemoteGrabber__ctor_mDCDA505857F7EB0EB5DEB4D383B89581D1BDD37F (void);
// 0x000005C2 System.Single BNG.Slider::get_SlidePercentage()
extern void Slider_get_SlidePercentage_mF29A1EE4A3164F73347B05E6D542CA2D27CBBE95 (void);
// 0x000005C3 System.Void BNG.Slider::Start()
extern void Slider_Start_m812EAC6E84AB2C0948D43DF13235107269C49DED (void);
// 0x000005C4 System.Void BNG.Slider::Update()
extern void Slider_Update_m22DA18F86E9D3FF3F0032C92FFC1B371166DB058 (void);
// 0x000005C5 System.Void BNG.Slider::OnSliderChange(System.Single)
extern void Slider_OnSliderChange_m3CEB58E364A82769A0A913F1E52476DF8574F946 (void);
// 0x000005C6 System.Void BNG.Slider::.ctor()
extern void Slider__ctor_mE178070494FD1A1F478CECE79E0E60F49BEB7EBE (void);
// 0x000005C7 System.Void BNG.SmoothLocomotion::add_OnBeforeMove(BNG.SmoothLocomotion/OnBeforeMoveAction)
extern void SmoothLocomotion_add_OnBeforeMove_m63EE22A194AA2ECD3237B4321D83D484BDC55825 (void);
// 0x000005C8 System.Void BNG.SmoothLocomotion::remove_OnBeforeMove(BNG.SmoothLocomotion/OnBeforeMoveAction)
extern void SmoothLocomotion_remove_OnBeforeMove_mED6FEEFF2505156E24FD9CED269047AE32189DD2 (void);
// 0x000005C9 System.Void BNG.SmoothLocomotion::add_OnAfterMove(BNG.SmoothLocomotion/OnAfterMoveAction)
extern void SmoothLocomotion_add_OnAfterMove_mDC172891FD65D0B7C4335C8778B4411856DB08E2 (void);
// 0x000005CA System.Void BNG.SmoothLocomotion::remove_OnAfterMove(BNG.SmoothLocomotion/OnAfterMoveAction)
extern void SmoothLocomotion_remove_OnAfterMove_m4486DDD989B77E04EF1AF92261EE7075C1A6DB5C (void);
// 0x000005CB System.Void BNG.SmoothLocomotion::Update()
extern void SmoothLocomotion_Update_m90B6B8D91AFB2CDCF2D5B9B3262D1076AD6737DB (void);
// 0x000005CC System.Void BNG.SmoothLocomotion::FixedUpdate()
extern void SmoothLocomotion_FixedUpdate_mD9E67A34E5F0AD29159A876312316D376087228B (void);
// 0x000005CD System.Void BNG.SmoothLocomotion::CheckControllerReferences()
extern void SmoothLocomotion_CheckControllerReferences_m4F807A80DC3D4A602DCBDB1995CC126C619E3D45 (void);
// 0x000005CE System.Void BNG.SmoothLocomotion::UpdateInputs()
extern void SmoothLocomotion_UpdateInputs_m168FC2356C4BA9C07E54429815BF49843988C383 (void);
// 0x000005CF System.Void BNG.SmoothLocomotion::DoRigidBodyJump()
extern void SmoothLocomotion_DoRigidBodyJump_m8B8605781AD73FEEC552C930F9035E2C9A502166 (void);
// 0x000005D0 UnityEngine.Vector2 BNG.SmoothLocomotion::GetMovementAxis()
extern void SmoothLocomotion_GetMovementAxis_m4ACC5905C9EBDD80BFFB59A05238C56AA4FA5B2A (void);
// 0x000005D1 System.Void BNG.SmoothLocomotion::MoveCharacter()
extern void SmoothLocomotion_MoveCharacter_m959EC6BD930EFA8FCFB641AECA7A88F8DA6AD33A (void);
// 0x000005D2 System.Void BNG.SmoothLocomotion::MoveRigidCharacter(UnityEngine.Vector3)
extern void SmoothLocomotion_MoveRigidCharacter_m6474FC7BC3526EC3AA29D51CFE32A0AC075A82A6 (void);
// 0x000005D3 System.Void BNG.SmoothLocomotion::MoveRigidCharacter()
extern void SmoothLocomotion_MoveRigidCharacter_mF1FDE03E2A079C1EE40CE4857E0FC733874A59B7 (void);
// 0x000005D4 System.Void BNG.SmoothLocomotion::MoveCharacter(UnityEngine.Vector3)
extern void SmoothLocomotion_MoveCharacter_m66F9DE70BBAC765420C46E5227B297CC74CD193C (void);
// 0x000005D5 System.Boolean BNG.SmoothLocomotion::CheckJump()
extern void SmoothLocomotion_CheckJump_m5EB58D0BC6DDECCE8AB3F4F6BC14DC62D16F93D9 (void);
// 0x000005D6 System.Boolean BNG.SmoothLocomotion::CheckSprint()
extern void SmoothLocomotion_CheckSprint_m840017CDF6575B8BBE14EDF7BB0A89799598ED6E (void);
// 0x000005D7 System.Boolean BNG.SmoothLocomotion::IsGrounded()
extern void SmoothLocomotion_IsGrounded_mC758B1DDE98E2070390E3EE4586211FD8697434B (void);
// 0x000005D8 System.Void BNG.SmoothLocomotion::SetupCharacterController()
extern void SmoothLocomotion_SetupCharacterController_m561A6C9485CEEBF86E68784A1BC93AA899E60CBF (void);
// 0x000005D9 System.Void BNG.SmoothLocomotion::SetupRigidbodyPlayer()
extern void SmoothLocomotion_SetupRigidbodyPlayer_mBCD7B93D7D402855230E8989AEDE79AFD80EFC24 (void);
// 0x000005DA System.Void BNG.SmoothLocomotion::EnableMovement()
extern void SmoothLocomotion_EnableMovement_mE76DB7F298DDF877664703D16E90036AA303E1A8 (void);
// 0x000005DB System.Void BNG.SmoothLocomotion::DisableMovement()
extern void SmoothLocomotion_DisableMovement_m02F086195A6023B44C0877C81EFF9BA42FD64FD6 (void);
// 0x000005DC System.Void BNG.SmoothLocomotion::OnCollisionStay(UnityEngine.Collision)
extern void SmoothLocomotion_OnCollisionStay_m0A48BB66E362D621A7DF6D3CDE6FE9397169279D (void);
// 0x000005DD System.Void BNG.SmoothLocomotion::.ctor()
extern void SmoothLocomotion__ctor_mCF65BDBF089B640DE01DB611914BA1DF249EDA3D (void);
// 0x000005DE System.Void BNG.SmoothLocomotion/OnBeforeMoveAction::.ctor(System.Object,System.IntPtr)
extern void OnBeforeMoveAction__ctor_mE8A0AD160F79C84EBEAD9C25DAAD9EABA079EC10 (void);
// 0x000005DF System.Void BNG.SmoothLocomotion/OnBeforeMoveAction::Invoke()
extern void OnBeforeMoveAction_Invoke_m243908BA1879AF8C9BB598CAA32877C4E3359C99 (void);
// 0x000005E0 System.IAsyncResult BNG.SmoothLocomotion/OnBeforeMoveAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnBeforeMoveAction_BeginInvoke_m973305C8B6BF2E55D2ECB0ED489B2E440EFB39A2 (void);
// 0x000005E1 System.Void BNG.SmoothLocomotion/OnBeforeMoveAction::EndInvoke(System.IAsyncResult)
extern void OnBeforeMoveAction_EndInvoke_mC4DC77E6CBE41260558A4A0383E239D3C9A6ACFE (void);
// 0x000005E2 System.Void BNG.SmoothLocomotion/OnAfterMoveAction::.ctor(System.Object,System.IntPtr)
extern void OnAfterMoveAction__ctor_m97AA891F1437CE4D6FAFBF3C8ED260BF22BD19D6 (void);
// 0x000005E3 System.Void BNG.SmoothLocomotion/OnAfterMoveAction::Invoke()
extern void OnAfterMoveAction_Invoke_m3F4653B0C0C12CA77DCA73A19766342DABCF8942 (void);
// 0x000005E4 System.IAsyncResult BNG.SmoothLocomotion/OnAfterMoveAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAfterMoveAction_BeginInvoke_mDC227426388B1073A1A280FAAC5A88B835E23610 (void);
// 0x000005E5 System.Void BNG.SmoothLocomotion/OnAfterMoveAction::EndInvoke(System.IAsyncResult)
extern void OnAfterMoveAction_EndInvoke_mBCA95530CD02462DB25951379626A613E70D2A5A (void);
// 0x000005E6 System.Void BNG.SnapZone::Start()
extern void SnapZone_Start_m266BDC7277ECD25647B1D5C711BDA170C78AC91D (void);
// 0x000005E7 System.Void BNG.SnapZone::Update()
extern void SnapZone_Update_m38CF27C99424E8FDC86178BCEC4408C6041A9051 (void);
// 0x000005E8 BNG.Grabbable BNG.SnapZone::getClosestGrabbable()
extern void SnapZone_getClosestGrabbable_mD26711F454AAF0A501D529050D39CE0B6457588D (void);
// 0x000005E9 System.Void BNG.SnapZone::GrabGrabbable(BNG.Grabbable)
extern void SnapZone_GrabGrabbable_mB2AEF06DF0D2BFA1DA9F54BA7EE07BCEADA6A7DA (void);
// 0x000005EA System.Void BNG.SnapZone::disableGrabbable(BNG.Grabbable)
extern void SnapZone_disableGrabbable_mC3D0216677F185C655DA1FC506B4B211165BA663 (void);
// 0x000005EB System.Void BNG.SnapZone::GrabEquipped(BNG.Grabber)
extern void SnapZone_GrabEquipped_mAA6C7941786AA87B354DBBC226F987A2F65D9731 (void);
// 0x000005EC System.Boolean BNG.SnapZone::CanBeRemoved()
extern void SnapZone_CanBeRemoved_m235EB1685E7C9AA0764B5FDB973EA819F06206DC (void);
// 0x000005ED System.Void BNG.SnapZone::ReleaseAll()
extern void SnapZone_ReleaseAll_mA3E67BD987899E7ADB02B57A4A723F7F5B3B3761 (void);
// 0x000005EE System.Void BNG.SnapZone::.ctor()
extern void SnapZone__ctor_m0122511BDF7C8D2CBCFECCA151F15D703E2061AC (void);
// 0x000005EF System.Void BNG.SnapZoneOffset::.ctor()
extern void SnapZoneOffset__ctor_m6BA00E9C75C814BCA94A21643BC0D5022DF9F044 (void);
// 0x000005F0 System.Void BNG.SnapZoneScale::.ctor()
extern void SnapZoneScale__ctor_m96101E1B18646B6D657D5E468390B7710DDCF71E (void);
// 0x000005F1 System.Single BNG.SteeringWheel::get_Angle()
extern void SteeringWheel_get_Angle_m54BEB74DD3F6D5529BA5637D037C0D5701219C02 (void);
// 0x000005F2 System.Single BNG.SteeringWheel::get_RawAngle()
extern void SteeringWheel_get_RawAngle_mE346528B562FD1CF1B3D116A1ABEB388BCCD8C74 (void);
// 0x000005F3 System.Single BNG.SteeringWheel::get_ScaleValue()
extern void SteeringWheel_get_ScaleValue_m57B7CF1532C54D66F1C99531B6051C8AF6385344 (void);
// 0x000005F4 System.Single BNG.SteeringWheel::get_ScaleValueInverted()
extern void SteeringWheel_get_ScaleValueInverted_mECADF6E592C0FE3983665D7B409D13B9F7629A24 (void);
// 0x000005F5 System.Single BNG.SteeringWheel::get_AngleInverted()
extern void SteeringWheel_get_AngleInverted_mAC6494C71788DEEF51969E3EDFCA165E1E9FE53B (void);
// 0x000005F6 BNG.Grabber BNG.SteeringWheel::get_PrimaryGrabber()
extern void SteeringWheel_get_PrimaryGrabber_mC77AAABA4A10A3971DBBFCE1774BB31991F58425 (void);
// 0x000005F7 BNG.Grabber BNG.SteeringWheel::get_SecondaryGrabber()
extern void SteeringWheel_get_SecondaryGrabber_m661DB062D94D9E28FDC5C601214110B6D0581283 (void);
// 0x000005F8 System.Void BNG.SteeringWheel::Update()
extern void SteeringWheel_Update_mB5092DE9E146883FE1E0D273D8AF272778896A06 (void);
// 0x000005F9 System.Void BNG.SteeringWheel::UpdateAngleCalculations()
extern void SteeringWheel_UpdateAngleCalculations_mFBC23E1B3F5500AEB52D1107E1E8FB39BF8EFFD2 (void);
// 0x000005FA System.Single BNG.SteeringWheel::GetRelativeAngle(UnityEngine.Vector3,UnityEngine.Vector3)
extern void SteeringWheel_GetRelativeAngle_m72DF665C5F2C1145CECB7760A57703568033E453 (void);
// 0x000005FB System.Void BNG.SteeringWheel::ApplyAngleToSteeringWheel(System.Single)
extern void SteeringWheel_ApplyAngleToSteeringWheel_mBB0DBAD23F6D557A1E327571BF94711215936062 (void);
// 0x000005FC System.Void BNG.SteeringWheel::UpdatePreviewText()
extern void SteeringWheel_UpdatePreviewText_m64A80BB563C5647A6949BB1565D06CE2CF737FCD (void);
// 0x000005FD System.Void BNG.SteeringWheel::CallEvents()
extern void SteeringWheel_CallEvents_m178FF63BE16181B3083C39BB4F2CA3DB1A56AB06 (void);
// 0x000005FE System.Void BNG.SteeringWheel::OnGrab(BNG.Grabber)
extern void SteeringWheel_OnGrab_m76B48382769A6693CE17BA15D191FFD6CD88F7B7 (void);
// 0x000005FF System.Void BNG.SteeringWheel::ReturnToCenterAngle()
extern void SteeringWheel_ReturnToCenterAngle_m4264C0C18B5B40608A78E5F9918F2E6DF684CFF2 (void);
// 0x00000600 BNG.Grabber BNG.SteeringWheel::GetPrimaryGrabber()
extern void SteeringWheel_GetPrimaryGrabber_mED43B391BE5F8CA81C1DBB0E6C91622F81614348 (void);
// 0x00000601 BNG.Grabber BNG.SteeringWheel::GetSecondaryGrabber()
extern void SteeringWheel_GetSecondaryGrabber_mE213281DFF0CAAA48A00C223C4B017DBBCE592EB (void);
// 0x00000602 System.Void BNG.SteeringWheel::UpdatePreviousAngle(System.Single)
extern void SteeringWheel_UpdatePreviousAngle_m22539AB9F2E5A7A709DFEADE6E6CB79CC27928C1 (void);
// 0x00000603 System.Single BNG.SteeringWheel::GetScaledValue(System.Single,System.Single,System.Single)
extern void SteeringWheel_GetScaledValue_m65F2272C8FD3C836E5E66301311A8D532F8730EE (void);
// 0x00000604 System.Void BNG.SteeringWheel::.ctor()
extern void SteeringWheel__ctor_m3E00C3A01B62F18A224CD4652A2F5CC3D339F476 (void);
// 0x00000605 System.Void BNG.TrackedDevice::Awake()
extern void TrackedDevice_Awake_m94E6139828EE42F9AB29B2B8E1AD89F8B7265B3E (void);
// 0x00000606 System.Void BNG.TrackedDevice::OnEnable()
extern void TrackedDevice_OnEnable_mD350292B3867A08371AC93AD6DBC02A7AE17B53F (void);
// 0x00000607 System.Void BNG.TrackedDevice::OnDisable()
extern void TrackedDevice_OnDisable_m13F0416CC075753E06CABE4C17590489F1558CDF (void);
// 0x00000608 System.Void BNG.TrackedDevice::Update()
extern void TrackedDevice_Update_m192EE24FF4B7349ECD2A1530160DA59BDA491E8C (void);
// 0x00000609 System.Void BNG.TrackedDevice::FixedUpdate()
extern void TrackedDevice_FixedUpdate_mC8425C0F3DA6C50C291323151E802B01C024D545 (void);
// 0x0000060A System.Void BNG.TrackedDevice::RefreshDeviceStatus()
extern void TrackedDevice_RefreshDeviceStatus_m0D28613283A67F4D5293F613DA57C392308528DC (void);
// 0x0000060B System.Void BNG.TrackedDevice::UpdateDevice()
extern void TrackedDevice_UpdateDevice_m8957BFEC907C8133873A935432F72C98DAC0805F (void);
// 0x0000060C System.Void BNG.TrackedDevice::OnBeforeRender()
extern void TrackedDevice_OnBeforeRender_m7F859DBEFABF62FEC8A97E54827EE861FB99332F (void);
// 0x0000060D System.Void BNG.TrackedDevice::.ctor()
extern void TrackedDevice__ctor_mFF55F50204D1E31B44AEC513B331EA44D5AAE67C (void);
// 0x0000060E System.Void BNG.Arrow::Start()
extern void Arrow_Start_m369BD4F9440CAC6554556E92A4208552B43DBD33 (void);
// 0x0000060F System.Void BNG.Arrow::FixedUpdate()
extern void Arrow_FixedUpdate_m0ED2616B69589E5EFC61C2DC887E8BD768C8390D (void);
// 0x00000610 System.Void BNG.Arrow::ShootArrow(UnityEngine.Vector3)
extern void Arrow_ShootArrow_mDFBADFAF67FF0D13876A20F484F9574E720DD0CB (void);
// 0x00000611 System.Collections.IEnumerator BNG.Arrow::QueueDestroy()
extern void Arrow_QueueDestroy_m6962FB6EDF77C45137857703E4D31E8C7259BE1B (void);
// 0x00000612 System.Collections.IEnumerator BNG.Arrow::ReEnableCollider()
extern void Arrow_ReEnableCollider_mF9B6965D9E1050D3B20585D6C4DD42C60FB2C0F6 (void);
// 0x00000613 System.Void BNG.Arrow::OnCollisionEnter(UnityEngine.Collision)
extern void Arrow_OnCollisionEnter_mC1C2911D92E0B9CA1245F292FABF1A5ACDB45A13 (void);
// 0x00000614 System.Void BNG.Arrow::tryStickArrow(UnityEngine.Collision)
extern void Arrow_tryStickArrow_m120FAF98726B446DC5F7D0645D6EAD81B0B16316 (void);
// 0x00000615 System.Void BNG.Arrow::playSoundInterval(System.Single,System.Single)
extern void Arrow_playSoundInterval_m736F604F188A2E2C8E72F24551D04ACE185C341D (void);
// 0x00000616 System.Void BNG.Arrow::.ctor()
extern void Arrow__ctor_m5A6B7526907DCB77A7D58B8BE24537DB3C5FB6FC (void);
// 0x00000617 System.Void BNG.Arrow/<QueueDestroy>d__14::.ctor(System.Int32)
extern void U3CQueueDestroyU3Ed__14__ctor_m057E16209611CDEA313CB571B315E6E3872C11EE (void);
// 0x00000618 System.Void BNG.Arrow/<QueueDestroy>d__14::System.IDisposable.Dispose()
extern void U3CQueueDestroyU3Ed__14_System_IDisposable_Dispose_m136CC33066329DA31B7262BC5701195EAB1A1489 (void);
// 0x00000619 System.Boolean BNG.Arrow/<QueueDestroy>d__14::MoveNext()
extern void U3CQueueDestroyU3Ed__14_MoveNext_m7610D645B5E65F72B83DC4DD265221CB13C9FEB7 (void);
// 0x0000061A System.Object BNG.Arrow/<QueueDestroy>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CQueueDestroyU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DCA8900F7C5819CB797E7545B564A3A4F257F76 (void);
// 0x0000061B System.Void BNG.Arrow/<QueueDestroy>d__14::System.Collections.IEnumerator.Reset()
extern void U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_Reset_mE8F44E19DD9E1BCAC0A7451D209CCB7DFF7A71EF (void);
// 0x0000061C System.Object BNG.Arrow/<QueueDestroy>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_get_Current_m51A9FB84AACEFC7A261528C4F8674A7B6663F3A9 (void);
// 0x0000061D System.Void BNG.Arrow/<ReEnableCollider>d__15::.ctor(System.Int32)
extern void U3CReEnableColliderU3Ed__15__ctor_mBE7E20FF9BDF79A3A1F76E37AD657AB43DC0F97C (void);
// 0x0000061E System.Void BNG.Arrow/<ReEnableCollider>d__15::System.IDisposable.Dispose()
extern void U3CReEnableColliderU3Ed__15_System_IDisposable_Dispose_mFFC6AA53994F19B3B9E075466435F6CE18E57425 (void);
// 0x0000061F System.Boolean BNG.Arrow/<ReEnableCollider>d__15::MoveNext()
extern void U3CReEnableColliderU3Ed__15_MoveNext_m032E3ED6B97D11B351DC9806B080B91743A8BAF5 (void);
// 0x00000620 System.Object BNG.Arrow/<ReEnableCollider>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReEnableColliderU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E00FF427DF499A881DB0AD4F010EE6AB738DD1C (void);
// 0x00000621 System.Void BNG.Arrow/<ReEnableCollider>d__15::System.Collections.IEnumerator.Reset()
extern void U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_Reset_m3750244D88E0EEC37C9305D8D06EF31F468EA722 (void);
// 0x00000622 System.Object BNG.Arrow/<ReEnableCollider>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_get_Current_mFE1B99262B023F12F689D3513CF3C71B6041FE80 (void);
// 0x00000623 System.Void BNG.ArrowGrabArea::Start()
extern void ArrowGrabArea_Start_m89D77DE14E477B9D4E6AD6AF022A4705A1D1DF2F (void);
// 0x00000624 System.Void BNG.ArrowGrabArea::OnTriggerEnter(UnityEngine.Collider)
extern void ArrowGrabArea_OnTriggerEnter_m6CB8396ECB37DDCA56E0765CD6F568825BB114C3 (void);
// 0x00000625 System.Void BNG.ArrowGrabArea::OnTriggerExit(UnityEngine.Collider)
extern void ArrowGrabArea_OnTriggerExit_m5C01C75B92D4CC55A7654A410AF44DD6FDE190C2 (void);
// 0x00000626 System.Void BNG.ArrowGrabArea::.ctor()
extern void ArrowGrabArea__ctor_mF367590A0BD62435E258A3F66DD7573A83F00059 (void);
// 0x00000627 System.Void BNG.AutoGrabGrabbable::OnBecomesClosestGrabbable(BNG.Grabber)
extern void AutoGrabGrabbable_OnBecomesClosestGrabbable_m649BFD8E3A7F6213B6BBFA1B7539FB12C6FE0BDE (void);
// 0x00000628 System.Void BNG.AutoGrabGrabbable::.ctor()
extern void AutoGrabGrabbable__ctor_m4570A045DA6F1F36BA22838BE0ED410576AE52F8 (void);
// 0x00000629 System.Single BNG.Bow::get_DrawPercent()
extern void Bow_get_DrawPercent_mA245CBCDF2220863A29B460693226910906F1117 (void);
// 0x0000062A System.Void BNG.Bow::set_DrawPercent(System.Single)
extern void Bow_set_DrawPercent_m6AB36EBAFE115379B50791DB25BF21FBAC730C9E (void);
// 0x0000062B System.Void BNG.Bow::Start()
extern void Bow_Start_m54F6E59BBDBFE28F31F9D74A493E4EA0ABCEAF99 (void);
// 0x0000062C System.Void BNG.Bow::Update()
extern void Bow_Update_mC12D334E86156C8A1095458BAC1F5F2E04D36015 (void);
// 0x0000062D UnityEngine.Transform BNG.Bow::getArrowRest()
extern void Bow_getArrowRest_mFE77E644948CAC7FB0A946B0F7EF7440133060A1 (void);
// 0x0000062E System.Boolean BNG.Bow::canGrabArrowFromKnock()
extern void Bow_canGrabArrowFromKnock_m8CF5E8AD032C44EEE41B58C067235F4A8F7996D7 (void);
// 0x0000062F System.Single BNG.Bow::getGrabArrowInput()
extern void Bow_getGrabArrowInput_m04831F34F8581BB2286A9489385C303AE70206F8 (void);
// 0x00000630 System.Single BNG.Bow::getGripInput(BNG.ControllerHand)
extern void Bow_getGripInput_mC5EA1264E90304F5B127BE75190571063275D357 (void);
// 0x00000631 System.Single BNG.Bow::getTriggerInput(BNG.ControllerHand)
extern void Bow_getTriggerInput_mFEE68D5650460F97114C997D548F0989EDA95D5A (void);
// 0x00000632 System.Void BNG.Bow::setKnockPosition()
extern void Bow_setKnockPosition_m72544009E619E031706CD90553A2A306C615FF5A (void);
// 0x00000633 System.Void BNG.Bow::checkDrawSound()
extern void Bow_checkDrawSound_m94D28BC594B0CE5DFC97651B7FE31ED16254BC18 (void);
// 0x00000634 System.Void BNG.Bow::updateDrawDistance()
extern void Bow_updateDrawDistance_m274400F1DC2A47445A077DE465B47F65711DD23A (void);
// 0x00000635 System.Void BNG.Bow::checkBowHaptics()
extern void Bow_checkBowHaptics_mDE4AB7398E84BC97A72BDB461282CD51E9A423B6 (void);
// 0x00000636 System.Void BNG.Bow::resetStringPosition()
extern void Bow_resetStringPosition_mCA5AB4BE04E2A4058F425F975D85DE2640597D88 (void);
// 0x00000637 System.Void BNG.Bow::alignArrow()
extern void Bow_alignArrow_m18411182E83DC77FC8DAB583D3A28E9D80411044 (void);
// 0x00000638 System.Void BNG.Bow::alignBow()
extern void Bow_alignBow_mBBE700C5FCC2C02DE7B5BC4EDB9AE4FB3A243568 (void);
// 0x00000639 System.Void BNG.Bow::ResetBowAlignment()
extern void Bow_ResetBowAlignment_m21BCC814ECF6282DC4BBB39FB8E506271A2F7C92 (void);
// 0x0000063A System.Void BNG.Bow::GrabArrow(BNG.Arrow)
extern void Bow_GrabArrow_m0A26EE98CFCFED8708B9C1973B853ACE6D0CE5E7 (void);
// 0x0000063B System.Void BNG.Bow::ReleaseArrow()
extern void Bow_ReleaseArrow_mF5E6F59FD2003E28092E8715BEBFF87D3CC07EB3 (void);
// 0x0000063C System.Void BNG.Bow::OnRelease()
extern void Bow_OnRelease_m09E3793BEE259881BF7DB831C1894D02F63F40E0 (void);
// 0x0000063D System.Void BNG.Bow::resetArrowValues()
extern void Bow_resetArrowValues_mFF4D39A10576D8D8A8563CBF7CA0C7758E0E15CE (void);
// 0x0000063E System.Void BNG.Bow::playSoundInterval(System.Single,System.Single,System.Single)
extern void Bow_playSoundInterval_m0F1E0F14D3D4CEE7B34CCB8B061D6594A67277E5 (void);
// 0x0000063F System.Void BNG.Bow::playBowDraw()
extern void Bow_playBowDraw_m6F9E91E654628E17057FF0842CAC7A4F681B1618 (void);
// 0x00000640 System.Void BNG.Bow::playBowRelease()
extern void Bow_playBowRelease_m4C636519D1D539A15FE72D856E98152BE307081F (void);
// 0x00000641 System.Void BNG.Bow::.ctor()
extern void Bow__ctor_mB4B86E7ED02F4EF59CFC06E0AC302E57750AF4D2 (void);
// 0x00000642 System.Boolean BNG.Bow::<checkBowHaptics>b__43_0(BNG.DrawDefinition)
extern void Bow_U3CcheckBowHapticsU3Eb__43_0_m0A2987A1491DAA22CF361F901134358DAAB72063 (void);
// 0x00000643 System.Single BNG.DrawDefinition::get_DrawPercentage()
extern void DrawDefinition_get_DrawPercentage_m45C2F217150EC4067DA9BBC16CE60005F18CAF07 (void);
// 0x00000644 System.Void BNG.DrawDefinition::set_DrawPercentage(System.Single)
extern void DrawDefinition_set_DrawPercentage_mA6AC50D291294845783DB7D157509782169F96CB (void);
// 0x00000645 System.Single BNG.DrawDefinition::get_HapticAmplitude()
extern void DrawDefinition_get_HapticAmplitude_m5446166ED809012EF8ED5D450630181B0D58D5BB (void);
// 0x00000646 System.Void BNG.DrawDefinition::set_HapticAmplitude(System.Single)
extern void DrawDefinition_set_HapticAmplitude_m8CBD7DF51921E5D2641ACCAB11F7ED891D6E517A (void);
// 0x00000647 System.Single BNG.DrawDefinition::get_HapticFrequency()
extern void DrawDefinition_get_HapticFrequency_m6A87F9C470190D7B4FC1B3C883D1A7009C705F10 (void);
// 0x00000648 System.Void BNG.DrawDefinition::set_HapticFrequency(System.Single)
extern void DrawDefinition_set_HapticFrequency_m9BB7B82B8C1E4089F8E3A9B07B5A560E590BF41E (void);
// 0x00000649 System.Void BNG.DrawDefinition::.ctor()
extern void DrawDefinition__ctor_mBC1D4F1F54F067D5792EF99D4089F223359D71B8 (void);
// 0x0000064A System.Void BNG.BowArm::Start()
extern void BowArm_Start_m78188325B43D03799CB97D09E3FCD16373F1DADC (void);
// 0x0000064B System.Void BNG.BowArm::Update()
extern void BowArm_Update_m1CCA3BE0DF637FB9643D35BC9478DC0C6EAB7BE1 (void);
// 0x0000064C System.Void BNG.BowArm::.ctor()
extern void BowArm__ctor_mF4A99CAF8AF4E6DCD8CDA06C7CF3BD11BB0B96DC (void);
// 0x0000064D System.Void BNG.BulletHole::Start()
extern void BulletHole_Start_mCA0F04EF922F06AC48087742F1BB26BCBDE2C5F3 (void);
// 0x0000064E System.Void BNG.BulletHole::TryAttachTo(UnityEngine.Collider)
extern void BulletHole_TryAttachTo_m1C7BE563FAD2622D9A268E906218C7685DF4611E (void);
// 0x0000064F System.Boolean BNG.BulletHole::transformIsEqualScale(UnityEngine.Transform)
extern void BulletHole_transformIsEqualScale_mC150EF6449AC1745934CDB45C262D6BA3DB08815 (void);
// 0x00000650 System.Void BNG.BulletHole::DestroySelf()
extern void BulletHole_DestroySelf_mF1246C09F12114C0791B366CCDE73B4CF3DCC63F (void);
// 0x00000651 System.Void BNG.BulletHole::.ctor()
extern void BulletHole__ctor_mD25368D634CD8FC2ECA858A5064A9739667D3FFA (void);
// 0x00000652 System.Void BNG.CalibratePlayerHeight::Start()
extern void CalibratePlayerHeight_Start_m15E64735188427890BDD77BA5749AE92565226EC (void);
// 0x00000653 System.Void BNG.CalibratePlayerHeight::CalibrateHeight()
extern void CalibratePlayerHeight_CalibrateHeight_m05476ACE240D3FD3A5A495A8E7920CC79DAA00A4 (void);
// 0x00000654 System.Void BNG.CalibratePlayerHeight::CalibrateHeight(System.Single)
extern void CalibratePlayerHeight_CalibrateHeight_mA0F48DEF8D0422E3D8EE6CF8965B406C30C58735 (void);
// 0x00000655 System.Void BNG.CalibratePlayerHeight::ResetPlayerHeight()
extern void CalibratePlayerHeight_ResetPlayerHeight_m69CAAEFE48BC9FA2AD44F7365105D236F0FD228D (void);
// 0x00000656 System.Single BNG.CalibratePlayerHeight::GetCurrentPlayerHeight()
extern void CalibratePlayerHeight_GetCurrentPlayerHeight_m0E3F317D85D6802DDBAA2FAEEFD998CAF68E6A4B (void);
// 0x00000657 System.Void BNG.CalibratePlayerHeight::.ctor()
extern void CalibratePlayerHeight__ctor_m4E89FB31114BC031971534089DF1628A847BB35B (void);
// 0x00000658 System.Void BNG.CalibratePlayerHeight::<Start>b__5_0(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void CalibratePlayerHeight_U3CStartU3Eb__5_0_mAD6086CF5F1AA804CF53225063D065F9B405E105 (void);
// 0x00000659 System.Void BNG.CustomCenterOfMass::Start()
extern void CustomCenterOfMass_Start_m7382A37160CBC4754645DC037131A5BB33D60977 (void);
// 0x0000065A System.Void BNG.CustomCenterOfMass::SetCenterOfMass(UnityEngine.Vector3)
extern void CustomCenterOfMass_SetCenterOfMass_m9C08DB2EB067AA41CF83F38A7FEE711043139C76 (void);
// 0x0000065B UnityEngine.Vector3 BNG.CustomCenterOfMass::getThisCenterOfMass()
extern void CustomCenterOfMass_getThisCenterOfMass_m27E1146D17189D57E82A4D46CDEA8E2950DF057B (void);
// 0x0000065C System.Void BNG.CustomCenterOfMass::OnDrawGizmos()
extern void CustomCenterOfMass_OnDrawGizmos_m0CBCF0E8A8B05AC4A974D72058CE272B1B98D78C (void);
// 0x0000065D System.Void BNG.CustomCenterOfMass::.ctor()
extern void CustomCenterOfMass__ctor_m989EE81CB480B4964216950488D4C69BB787A40C (void);
// 0x0000065E System.Void BNG.DrawerSound::OnDrawerUpdate(System.Single)
extern void DrawerSound_OnDrawerUpdate_m7D3E5A843DD67757C24195826BF4A75057D53421 (void);
// 0x0000065F System.Void BNG.DrawerSound::.ctor()
extern void DrawerSound__ctor_mD69A625B82C2F0E1E850BAEC20C949731F68EBA1 (void);
// 0x00000660 System.Void BNG.Explosive::DoExplosion()
extern void Explosive_DoExplosion_mE1DBADF2293CFE036F5B2E7FF0E3BBF9BC909DF9 (void);
// 0x00000661 System.Collections.IEnumerator BNG.Explosive::explosionRoutine()
extern void Explosive_explosionRoutine_mD239D37C5BA2FE8902EEA9467F16EAB1B31C1DF5 (void);
// 0x00000662 System.Collections.IEnumerator BNG.Explosive::dealDelayedDamaged(BNG.Damageable,System.Single)
extern void Explosive_dealDelayedDamaged_mAB7DFA3D4E6DBB480595FABD1B3EA677D2CF59C8 (void);
// 0x00000663 System.Void BNG.Explosive::OnDrawGizmosSelected()
extern void Explosive_OnDrawGizmosSelected_m4075C422BFB651736C7DA5F199701E316BEEE8A9 (void);
// 0x00000664 System.Void BNG.Explosive::.ctor()
extern void Explosive__ctor_mA4CBFD2D7CAEACDEAA379EC6991530E0FB4EE4BA (void);
// 0x00000665 System.Void BNG.Explosive/<explosionRoutine>d__6::.ctor(System.Int32)
extern void U3CexplosionRoutineU3Ed__6__ctor_mDFFAB2903F51377E6BCCF588589922D9D443F5D8 (void);
// 0x00000666 System.Void BNG.Explosive/<explosionRoutine>d__6::System.IDisposable.Dispose()
extern void U3CexplosionRoutineU3Ed__6_System_IDisposable_Dispose_m982D56CAA96DF3801A98927040399DA7BF911760 (void);
// 0x00000667 System.Boolean BNG.Explosive/<explosionRoutine>d__6::MoveNext()
extern void U3CexplosionRoutineU3Ed__6_MoveNext_m9EC54D6B0D8CD3CDC3CA08268907D59BFCF92E72 (void);
// 0x00000668 System.Object BNG.Explosive/<explosionRoutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CexplosionRoutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08EE4D79C46D01492D59547363DF06AB6EBA4C87 (void);
// 0x00000669 System.Void BNG.Explosive/<explosionRoutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_Reset_m86C3EFCC9BC2B0CD9122FC7E70D9E23DF66D50C8 (void);
// 0x0000066A System.Object BNG.Explosive/<explosionRoutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_get_Current_m39F65CDA7A4B4AB69025BAF99683C7A949C0E5F4 (void);
// 0x0000066B System.Void BNG.Explosive/<dealDelayedDamaged>d__7::.ctor(System.Int32)
extern void U3CdealDelayedDamagedU3Ed__7__ctor_m9FC5588AB1474D3FAA8CC048F86F7144BDB7F241 (void);
// 0x0000066C System.Void BNG.Explosive/<dealDelayedDamaged>d__7::System.IDisposable.Dispose()
extern void U3CdealDelayedDamagedU3Ed__7_System_IDisposable_Dispose_m0026A42505DA57837E245BFF8B8DC40C7CE59A1D (void);
// 0x0000066D System.Boolean BNG.Explosive/<dealDelayedDamaged>d__7::MoveNext()
extern void U3CdealDelayedDamagedU3Ed__7_MoveNext_m281425012A04C638E86655F3B20ABFB6451F2158 (void);
// 0x0000066E System.Object BNG.Explosive/<dealDelayedDamaged>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdealDelayedDamagedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11224EB9E0AB95E1461DB17BC8EDE5911C6665E9 (void);
// 0x0000066F System.Void BNG.Explosive/<dealDelayedDamaged>d__7::System.Collections.IEnumerator.Reset()
extern void U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_Reset_mF6145B9EC048A323E19921E1D615D1AE50453896 (void);
// 0x00000670 System.Object BNG.Explosive/<dealDelayedDamaged>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_get_Current_mAF5AE639BD37D5D8696882067634ACC9E167C579 (void);
// 0x00000671 System.Boolean BNG.ExtensionMethods::GetDown(BNG.ControllerBinding)
extern void ExtensionMethods_GetDown_mB04A7DFE714826178D5F37C40D39610916EA63B1 (void);
// 0x00000672 System.Void BNG.Flashlight::Start()
extern void Flashlight_Start_m840D3AE413832CE48CA0B065B3076C235F81326E (void);
// 0x00000673 System.Void BNG.Flashlight::OnTrigger(System.Single)
extern void Flashlight_OnTrigger_m27B6A84F910866A6E9AFFFA6ADEBEDC3623D8294 (void);
// 0x00000674 System.Void BNG.Flashlight::OnTriggerUp()
extern void Flashlight_OnTriggerUp_mDD2D00C6998F0338702D9ABFAA9381937C9A2FF9 (void);
// 0x00000675 System.Void BNG.Flashlight::.ctor()
extern void Flashlight__ctor_mE2574722386836E68FB2E290B41EC1FEAE494BE1 (void);
// 0x00000676 System.Void BNG.FPSText::Start()
extern void FPSText_Start_mB1780EE03B114F325A3FA4DE74641E2054F98C13 (void);
// 0x00000677 System.Void BNG.FPSText::Update()
extern void FPSText_Update_m1547E69868AF63D941563349F6A1E3FE661D436D (void);
// 0x00000678 System.Void BNG.FPSText::OnGUI()
extern void FPSText_OnGUI_m162B8B566846D404FAAD216A89AC73BB074D6460 (void);
// 0x00000679 System.Void BNG.FPSText::.ctor()
extern void FPSText__ctor_m9021403420ABB413253B55A1F1342F8DD834A6FA (void);
// 0x0000067A System.Void BNG.GrappleShot::Start()
extern void GrappleShot_Start_m3D4FCE0211DB8E2FBD55C40E059F1867AC922126 (void);
// 0x0000067B System.Void BNG.GrappleShot::LateUpdate()
extern void GrappleShot_LateUpdate_m6CA39EF1F06FDD21E92BBB3DDAB27F0314C78AD7 (void);
// 0x0000067C System.Void BNG.GrappleShot::OnTrigger(System.Single)
extern void GrappleShot_OnTrigger_m11F1A2371CE9AC3BB76ABF9ECEA42C44ED6D48B7 (void);
// 0x0000067D System.Void BNG.GrappleShot::updateGrappleDistance()
extern void GrappleShot_updateGrappleDistance_m22F3AF9099E92EF77BA5E8B25E8E7B8829D2433D (void);
// 0x0000067E System.Void BNG.GrappleShot::OnGrab(BNG.Grabber)
extern void GrappleShot_OnGrab_m7F51C29363FE21C5D9BF4493F8CF1E3FC64FF99E (void);
// 0x0000067F System.Void BNG.GrappleShot::OnRelease()
extern void GrappleShot_OnRelease_mF5AEE21BC815D486525189E43FC2E0F58C700FD8 (void);
// 0x00000680 System.Void BNG.GrappleShot::onReleaseGrapple()
extern void GrappleShot_onReleaseGrapple_m7E071B0705D9E50511511445EB24CEDF668066F5 (void);
// 0x00000681 System.Void BNG.GrappleShot::drawGrappleHelper()
extern void GrappleShot_drawGrappleHelper_m24E7FE45C431CEB7E71BFC6481A9EF67F862F500 (void);
// 0x00000682 System.Void BNG.GrappleShot::drawGrappleLine()
extern void GrappleShot_drawGrappleLine_mB39BAAB494ADC21FCB5ADDF39DFD5192F18AFF00 (void);
// 0x00000683 System.Void BNG.GrappleShot::hideGrappleLine()
extern void GrappleShot_hideGrappleLine_m9E2CC5D14A3662CF9E66364FADFF085BE7E53249 (void);
// 0x00000684 System.Void BNG.GrappleShot::showGrappleHelper(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void GrappleShot_showGrappleHelper_mF9D2592FC007732EEB15FC4679688B458DBDA7FD (void);
// 0x00000685 System.Void BNG.GrappleShot::hideGrappleHelper()
extern void GrappleShot_hideGrappleHelper_m48BF55228AE59BB81590626D219A9EB84F054A44 (void);
// 0x00000686 System.Void BNG.GrappleShot::reelInGrapple(System.Single)
extern void GrappleShot_reelInGrapple_mDB9753E051EC9171BD3D1693150864941837D515 (void);
// 0x00000687 System.Void BNG.GrappleShot::shootGrapple()
extern void GrappleShot_shootGrapple_mD37E4C93B130D771C40B5846B86F4207CC8F77DA (void);
// 0x00000688 System.Void BNG.GrappleShot::dropGrapple()
extern void GrappleShot_dropGrapple_m5FE7B8BBE4E950349B3867301D8318DCE5C49C5B (void);
// 0x00000689 System.Void BNG.GrappleShot::changeGravity(System.Boolean)
extern void GrappleShot_changeGravity_m37DF3AA60C317C78A7DFC1635A332CA8482B067E (void);
// 0x0000068A System.Void BNG.GrappleShot::.ctor()
extern void GrappleShot__ctor_mEB6466849CB0FF9751181E867F08DC458C2655F4 (void);
// 0x0000068B System.Void BNG.HandJet::Start()
extern void HandJet_Start_mFA64BC0916E0D9B302BF4E1912EC5E73DA12069E (void);
// 0x0000068C System.Void BNG.HandJet::OnTrigger(System.Single)
extern void HandJet_OnTrigger_mB5803926FDF3C4BEF696A58CD3A559F204AA289F (void);
// 0x0000068D System.Void BNG.HandJet::FixedUpdate()
extern void HandJet_FixedUpdate_m1C59F2FCBF6A0CA781624AF83B7D9BE15A1A15C0 (void);
// 0x0000068E System.Void BNG.HandJet::OnGrab(BNG.Grabber)
extern void HandJet_OnGrab_mE3A546F7C9BB718C8C6E2E4E5071F7481CFFA3C3 (void);
// 0x0000068F System.Void BNG.HandJet::ChangeGravity(System.Boolean)
extern void HandJet_ChangeGravity_m7F2C6636EAFCC89A89C0E85074DBBA0F0C1F471D (void);
// 0x00000690 System.Void BNG.HandJet::OnRelease()
extern void HandJet_OnRelease_m9CD2992F2A3100E2328ABAA22E4468B987584986 (void);
// 0x00000691 System.Void BNG.HandJet::doJet(System.Single)
extern void HandJet_doJet_m7D6762F945ACFF9386A318A9C3B379ABCAD98A12 (void);
// 0x00000692 System.Void BNG.HandJet::stopJet()
extern void HandJet_stopJet_m004E0E556254782B8887C4201B1BCAAA81EE6C55 (void);
// 0x00000693 System.Void BNG.HandJet::OnTriggerUp()
extern void HandJet_OnTriggerUp_m0186AB6828D80207AC9B6A1809E0469E6C7B20EA (void);
// 0x00000694 System.Void BNG.HandJet::.ctor()
extern void HandJet__ctor_m759B0EF93A96CDA7C693172E6683DD34F9286011 (void);
// 0x00000695 System.Void BNG.IKDummy::Start()
extern void IKDummy_Start_m1A716BF2C9890778F08A9064C84BD88FA98E85D9 (void);
// 0x00000696 UnityEngine.Transform BNG.IKDummy::SetParentAndLocalPosRot(System.String,UnityEngine.Transform)
extern void IKDummy_SetParentAndLocalPosRot_m0616A319EE14C9F8BCBEFFC5913D0CB14DE4120E (void);
// 0x00000697 System.Void BNG.IKDummy::LateUpdate()
extern void IKDummy_LateUpdate_m6168FFF503B5486B8261AA525164C63091EAB9EE (void);
// 0x00000698 System.Void BNG.IKDummy::OnAnimatorIK()
extern void IKDummy_OnAnimatorIK_m8E9652DD068AE2E2E7C016EA45B0FBD7A161BE18 (void);
// 0x00000699 System.Void BNG.IKDummy::.ctor()
extern void IKDummy__ctor_mE22F8474C654644D32B7D213CD8445EDF215F90D (void);
// 0x0000069A System.Void BNG.LaserPointer::Start()
extern void LaserPointer_Start_m878EC89A9404C56A5CFA69B7E64364BE79A75637 (void);
// 0x0000069B System.Void BNG.LaserPointer::LateUpdate()
extern void LaserPointer_LateUpdate_m4ECDAA616AB03645E38983F08035470BF3B94595 (void);
// 0x0000069C System.Void BNG.LaserPointer::.ctor()
extern void LaserPointer__ctor_mD9CDDD18BCA03A53D66AA3C2BBEABAA11414AF2A (void);
// 0x0000069D System.Void BNG.LaserSword::Start()
extern void LaserSword_Start_mF4900540460CFA3583134B6CDDCB3D7CB52024A5 (void);
// 0x0000069E System.Void BNG.LaserSword::Update()
extern void LaserSword_Update_mC71FF04F7E2BE402F60347710B7417618E180ACD (void);
// 0x0000069F System.Void BNG.LaserSword::OnTrigger(System.Single)
extern void LaserSword_OnTrigger_m764D84D6111D9CEEE9AA60D95465672DF6847DBC (void);
// 0x000006A0 System.Void BNG.LaserSword::checkCollision()
extern void LaserSword_checkCollision_m7EA288CC9837341CCF62984680BE157D4C921C6A (void);
// 0x000006A1 System.Void BNG.LaserSword::OnDrawGizmosSelected()
extern void LaserSword_OnDrawGizmosSelected_m4F8AB66F80ED17EB3EF2B03C6DD1E4B02D98A55D (void);
// 0x000006A2 System.Void BNG.LaserSword::.ctor()
extern void LaserSword__ctor_m0220DEB403117548EE4BD7F6DA057A97A284571A (void);
// 0x000006A3 System.Void BNG.LiquidWobble::Start()
extern void LiquidWobble_Start_mEC1A98B89387F125EFB2FD4BF350BBBBBE94E26C (void);
// 0x000006A4 System.Void BNG.LiquidWobble::Update()
extern void LiquidWobble_Update_m32F7596D730529AB2E6585DB8ABF2A5968D86C43 (void);
// 0x000006A5 System.Void BNG.LiquidWobble::.ctor()
extern void LiquidWobble__ctor_m5A74CC12F505E2FEDAD9F119DDD9E474194C465D (void);
// 0x000006A6 System.Void BNG.Marker::OnGrab(BNG.Grabber)
extern void Marker_OnGrab_m2B17F7233B9DEABEA387B6F582885A8A404AA593 (void);
// 0x000006A7 System.Void BNG.Marker::OnRelease()
extern void Marker_OnRelease_mC9D284802C6EB33152F12F40EF0DEF963B606541 (void);
// 0x000006A8 System.Collections.IEnumerator BNG.Marker::WriteRoutine()
extern void Marker_WriteRoutine_m2BB16CBFA35A54D722A2A0BC8DC34DDEE0EDA69B (void);
// 0x000006A9 System.Void BNG.Marker::InitDraw(UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,UnityEngine.Color)
extern void Marker_InitDraw_mD4989A7B7FC63B0BAB3D7741B2E03292A37DFFFD (void);
// 0x000006AA UnityEngine.Vector3 BNG.Marker::DrawPoint(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Color,UnityEngine.Quaternion)
extern void Marker_DrawPoint_mC41778597EE4E90C27BA01F6ACDFFB807B841488 (void);
// 0x000006AB System.Void BNG.Marker::OnDrawGizmosSelected()
extern void Marker_OnDrawGizmosSelected_m545931102EA6AE9ABF2B293D5505D46193A51A89 (void);
// 0x000006AC System.Void BNG.Marker::.ctor()
extern void Marker__ctor_mA5B6DEA9968C34A296D2572099F1F6FBA8B5D10A (void);
// 0x000006AD System.Void BNG.Marker/<WriteRoutine>d__18::.ctor(System.Int32)
extern void U3CWriteRoutineU3Ed__18__ctor_m79949B4EC19837A5E5A2488DCA069DBC715A9C0E (void);
// 0x000006AE System.Void BNG.Marker/<WriteRoutine>d__18::System.IDisposable.Dispose()
extern void U3CWriteRoutineU3Ed__18_System_IDisposable_Dispose_m51140FDF31E4331F1977F044C38FF8D024FEF503 (void);
// 0x000006AF System.Boolean BNG.Marker/<WriteRoutine>d__18::MoveNext()
extern void U3CWriteRoutineU3Ed__18_MoveNext_mD918569D82B5E2C0695E17E9A4D150A7BEC01DA8 (void);
// 0x000006B0 System.Object BNG.Marker/<WriteRoutine>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWriteRoutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C1B917AA6EC6397CD43B141C80FC7031463C59A (void);
// 0x000006B1 System.Void BNG.Marker/<WriteRoutine>d__18::System.Collections.IEnumerator.Reset()
extern void U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_Reset_m7B13C3D754A65F48DACEDB9EC2E0B6B977A4F478 (void);
// 0x000006B2 System.Object BNG.Marker/<WriteRoutine>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_get_Current_mDB6DA0CADC4FB49EA0C8F9BA9820E22AD0192489 (void);
// 0x000006B3 System.Void BNG.MoveToWaypoint::Start()
extern void MoveToWaypoint_Start_mAC357B32496B636BB76AAF4F76E118ACE9F4D418 (void);
// 0x000006B4 System.Void BNG.MoveToWaypoint::Update()
extern void MoveToWaypoint_Update_mA212DA37A43C8D1923F29332DA10ACE06747594B (void);
// 0x000006B5 System.Void BNG.MoveToWaypoint::FixedUpdate()
extern void MoveToWaypoint_FixedUpdate_m4362C8E42BB68AAF17EE77EC021C4C6DDB7677BD (void);
// 0x000006B6 System.Void BNG.MoveToWaypoint::movePlatform(System.Single)
extern void MoveToWaypoint_movePlatform_m985DC153D7858EEDF11440905AE1190A57C23E08 (void);
// 0x000006B7 System.Void BNG.MoveToWaypoint::resetDelayStatus()
extern void MoveToWaypoint_resetDelayStatus_m6B1FABCBF4C6048CDB43969289AFAA3C8D22D46A (void);
// 0x000006B8 System.Void BNG.MoveToWaypoint::.ctor()
extern void MoveToWaypoint__ctor_m83AC7BBE1D86798B66E38B7B4B16362D87870E6C (void);
// 0x000006B9 System.Void BNG.MovingPlatform::Update()
extern void MovingPlatform_Update_mF782E2421E8034BCDF9EF2DC98C82BB2FABF5F65 (void);
// 0x000006BA System.Void BNG.MovingPlatform::.ctor()
extern void MovingPlatform__ctor_m7E5675E146175CA4E4C8724AAD34DC80C8EAFF64 (void);
// 0x000006BB System.Void BNG.PlayerScaler::Update()
extern void PlayerScaler_Update_m2A659831B2BC4EAFB4D627874D8FFC6E22457017 (void);
// 0x000006BC System.Void BNG.PlayerScaler::.ctor()
extern void PlayerScaler__ctor_mC1FC8E00688DF5332B614A2B84D84876A3A04663 (void);
// 0x000006BD System.Void BNG.ProjectileLauncher::Start()
extern void ProjectileLauncher_Start_m18F0A290E26697740B7C0E872D6D525AB7ED4034 (void);
// 0x000006BE UnityEngine.GameObject BNG.ProjectileLauncher::ShootProjectile(System.Single)
extern void ProjectileLauncher_ShootProjectile_mC4A2D2FC97C2BA225D47CEA3BF20902C4B7EC5F5 (void);
// 0x000006BF System.Void BNG.ProjectileLauncher::ShootProjectile()
extern void ProjectileLauncher_ShootProjectile_m2805359B11D15DC572D9BA314BD2E74A4852FE42 (void);
// 0x000006C0 System.Void BNG.ProjectileLauncher::SetForce(System.Single)
extern void ProjectileLauncher_SetForce_mC511FDD1F4B5FDF2F502A017FD02A2C7657BF77A (void);
// 0x000006C1 System.Single BNG.ProjectileLauncher::GetInitialProjectileForce()
extern void ProjectileLauncher_GetInitialProjectileForce_m176BC6F5F91AACC18412AC666C336EA0503FDD94 (void);
// 0x000006C2 System.Void BNG.ProjectileLauncher::.ctor()
extern void ProjectileLauncher__ctor_m1AAC5DD23769DD4A4717AB6334984C6375A3B148 (void);
// 0x000006C3 System.Void BNG.SceneLoader::LoadScene(System.String)
extern void SceneLoader_LoadScene_m98601994EBDB10A5D38BF6333B1DADCC4974E925 (void);
// 0x000006C4 System.Collections.IEnumerator BNG.SceneLoader::FadeThenLoadScene()
extern void SceneLoader_FadeThenLoadScene_mFE49A2505629FD3F890A10C18B36CF2C1FD73DFC (void);
// 0x000006C5 System.Void BNG.SceneLoader::.ctor()
extern void SceneLoader__ctor_m6018485AA6CA6C83A5B8196B80A24234D1864522 (void);
// 0x000006C6 System.Void BNG.SceneLoader/<FadeThenLoadScene>d__6::.ctor(System.Int32)
extern void U3CFadeThenLoadSceneU3Ed__6__ctor_m665C34B2C21B1D96F8A3B5F9D98425F5E28BD43B (void);
// 0x000006C7 System.Void BNG.SceneLoader/<FadeThenLoadScene>d__6::System.IDisposable.Dispose()
extern void U3CFadeThenLoadSceneU3Ed__6_System_IDisposable_Dispose_mBCAAA635FF9B8DFDBA4FAC2260C25FEF0BEF2C75 (void);
// 0x000006C8 System.Boolean BNG.SceneLoader/<FadeThenLoadScene>d__6::MoveNext()
extern void U3CFadeThenLoadSceneU3Ed__6_MoveNext_m70063270612CC214CAB02F71475D26627CAD1FC7 (void);
// 0x000006C9 System.Object BNG.SceneLoader/<FadeThenLoadScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeThenLoadSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25BB244B140EF45517B4AAFBFF18A348C61C2BD2 (void);
// 0x000006CA System.Void BNG.SceneLoader/<FadeThenLoadScene>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_Reset_mD45EED76F0AD1FD50A915769767D948D9BC1B69C (void);
// 0x000006CB System.Object BNG.SceneLoader/<FadeThenLoadScene>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m1F25442075E510F0DF886C2541CC13C4DCE4CF86 (void);
// 0x000006CC System.Boolean BNG.TimeController::get_TimeSlowing()
extern void TimeController_get_TimeSlowing_m2D71DC4C2F25132E2F54C550FFC83EB18A15CFE1 (void);
// 0x000006CD System.Void BNG.TimeController::Start()
extern void TimeController_Start_m60A442E254217802FE43165960EFB93150A87F8A (void);
// 0x000006CE System.Void BNG.TimeController::Update()
extern void TimeController_Update_m6CD501992FAE8DDB0397616BF71B53B56FA57638 (void);
// 0x000006CF System.Boolean BNG.TimeController::SlowTimeInputDown()
extern void TimeController_SlowTimeInputDown_m9E58D0863941623B0B3D1C4A1AD166BE6846ACF7 (void);
// 0x000006D0 System.Void BNG.TimeController::SlowTime()
extern void TimeController_SlowTime_m3D84A9D9964FB3415258F31216B4678A76815A7D (void);
// 0x000006D1 System.Void BNG.TimeController::ResumeTime()
extern void TimeController_ResumeTime_m1F02A3BC2EBE44BB70AE55EA2E8C2968F1163973 (void);
// 0x000006D2 System.Collections.IEnumerator BNG.TimeController::resumeTimeRoutine()
extern void TimeController_resumeTimeRoutine_mBDFC67C33FCED200CC1EF9619972E7B0040F9E0A (void);
// 0x000006D3 System.Void BNG.TimeController::.ctor()
extern void TimeController__ctor_m92C119A246CE1E0FD41CA362B2C8BB900E06E994 (void);
// 0x000006D4 System.Void BNG.TimeController/<resumeTimeRoutine>d__20::.ctor(System.Int32)
extern void U3CresumeTimeRoutineU3Ed__20__ctor_mF0D28EB0D68B68DD84B4698F8849E6499ED1F0EC (void);
// 0x000006D5 System.Void BNG.TimeController/<resumeTimeRoutine>d__20::System.IDisposable.Dispose()
extern void U3CresumeTimeRoutineU3Ed__20_System_IDisposable_Dispose_m04F5E277DA40AFB0328AE08C4B20DDCC732D04A3 (void);
// 0x000006D6 System.Boolean BNG.TimeController/<resumeTimeRoutine>d__20::MoveNext()
extern void U3CresumeTimeRoutineU3Ed__20_MoveNext_m77A449D4B9AF21B62FC12AC2784AB33452059A77 (void);
// 0x000006D7 System.Object BNG.TimeController/<resumeTimeRoutine>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CresumeTimeRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9B46623FC41BDD8E513BC5B0E0EE3BC62C5CE2F (void);
// 0x000006D8 System.Void BNG.TimeController/<resumeTimeRoutine>d__20::System.Collections.IEnumerator.Reset()
extern void U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m32735CF094C2902B7FD912B720B44A13FA70C667 (void);
// 0x000006D9 System.Object BNG.TimeController/<resumeTimeRoutine>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_mF4DFE8F97966AADE13487E79DEADD188707C1DD5 (void);
// 0x000006DA System.Void BNG.ToggleActiveOnInputAction::OnEnable()
extern void ToggleActiveOnInputAction_OnEnable_mEC56DA25F61A422AED70E4C2997342A97062F350 (void);
// 0x000006DB System.Void BNG.ToggleActiveOnInputAction::OnDisable()
extern void ToggleActiveOnInputAction_OnDisable_m4F718CC6CE4214BFF7CE6E98C5679AB2B5AA2D28 (void);
// 0x000006DC System.Void BNG.ToggleActiveOnInputAction::ToggleActive(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void ToggleActiveOnInputAction_ToggleActive_mA83C7DD52B158734B4C1E345B47043886E3A36C5 (void);
// 0x000006DD System.Void BNG.ToggleActiveOnInputAction::.ctor()
extern void ToggleActiveOnInputAction__ctor_mD17C2FD97A9945C0AD8537258CFD8FC1F055CC5F (void);
// 0x000006DE System.Void BNG.VehicleController::Start()
extern void VehicleController_Start_mEB27CB47857651B6EC4232E30C84E9ACBF81370B (void);
// 0x000006DF System.Void BNG.VehicleController::Update()
extern void VehicleController_Update_m495738FF24BD0EA87263656018BABE9EA23362AC (void);
// 0x000006E0 System.Void BNG.VehicleController::CrankEngine()
extern void VehicleController_CrankEngine_mBBF4C7DAABE814056286B98F81265D2645943D66 (void);
// 0x000006E1 System.Collections.IEnumerator BNG.VehicleController::crankEngine()
extern void VehicleController_crankEngine_m26A4833D4BB24F8B6D7B77C411A92E14B1D6B1AD (void);
// 0x000006E2 System.Void BNG.VehicleController::CheckOutOfBounds()
extern void VehicleController_CheckOutOfBounds_m42DCCB3AE2529411F9ED36BAFB8AAC6FAE185675 (void);
// 0x000006E3 System.Void BNG.VehicleController::GetTorqueInputFromTriggers()
extern void VehicleController_GetTorqueInputFromTriggers_m98E32C51065E03FCCF0A5631E42E4E8E7251DCBA (void);
// 0x000006E4 System.Void BNG.VehicleController::FixedUpdate()
extern void VehicleController_FixedUpdate_m1CFD9BFE3DFFF764AC650DBC7C5B8D81BFA6906F (void);
// 0x000006E5 System.Void BNG.VehicleController::UpdateWheelTorque()
extern void VehicleController_UpdateWheelTorque_m9EB1F7A9995950FFD3D4F40629124E686FF4BA83 (void);
// 0x000006E6 System.Void BNG.VehicleController::SetSteeringAngle(System.Single)
extern void VehicleController_SetSteeringAngle_mD9D0EFBB489F406841751A408FCF63A81F1FAEE9 (void);
// 0x000006E7 System.Void BNG.VehicleController::SetSteeringAngleInverted(System.Single)
extern void VehicleController_SetSteeringAngleInverted_mBD0C48F0A8336C48412B4A6F7D5325317BF9F1C6 (void);
// 0x000006E8 System.Void BNG.VehicleController::SetSteeringAngle(UnityEngine.Vector2)
extern void VehicleController_SetSteeringAngle_m75064814F5786DC8193CFC3C0A269587097A95A4 (void);
// 0x000006E9 System.Void BNG.VehicleController::SetSteeringAngleInverted(UnityEngine.Vector2)
extern void VehicleController_SetSteeringAngleInverted_m2505183062A25DD59225127C0A9A5781CF7C54F7 (void);
// 0x000006EA System.Void BNG.VehicleController::SetMotorTorqueInput(System.Single)
extern void VehicleController_SetMotorTorqueInput_m9F734E6BB8E359B80A572E75B515BDAF922001B4 (void);
// 0x000006EB System.Void BNG.VehicleController::SetMotorTorqueInputInverted(System.Single)
extern void VehicleController_SetMotorTorqueInputInverted_m64FBDEFBA4136778282BC8BD9D2CFDB1D93A3649 (void);
// 0x000006EC System.Void BNG.VehicleController::SetMotorTorqueInput(UnityEngine.Vector2)
extern void VehicleController_SetMotorTorqueInput_mB9D29EB1190E4622F43FF3DBC7648D07151CEC99 (void);
// 0x000006ED System.Void BNG.VehicleController::SetMotorTorqueInputInverted(UnityEngine.Vector2)
extern void VehicleController_SetMotorTorqueInputInverted_mDFA7FBB51DE28BF7824F8A7F4D81CE7E1F27D8C0 (void);
// 0x000006EE System.Void BNG.VehicleController::UpdateWheelVisuals(BNG.WheelObject)
extern void VehicleController_UpdateWheelVisuals_m8FA49D0C72D108FF58ACBFFA76B7879DB890A2B4 (void);
// 0x000006EF System.Void BNG.VehicleController::UpdateEngineAudio()
extern void VehicleController_UpdateEngineAudio_mA62644C7CE31FC08A4258B299D7E6FE7C218CDC6 (void);
// 0x000006F0 System.Void BNG.VehicleController::OnCollisionEnter(UnityEngine.Collision)
extern void VehicleController_OnCollisionEnter_m7BE2465CB14AF491A323C6A21A8E8B5C6E61DC11 (void);
// 0x000006F1 System.Single BNG.VehicleController::correctValue(System.Single)
extern void VehicleController_correctValue_m76CAEE280C812D358729DAD6BAC7F645497B99E0 (void);
// 0x000006F2 System.Void BNG.VehicleController::.ctor()
extern void VehicleController__ctor_mB12845EFB05AADD403D7C17E397EA3F29CAE8329 (void);
// 0x000006F3 System.Void BNG.VehicleController/<crankEngine>d__24::.ctor(System.Int32)
extern void U3CcrankEngineU3Ed__24__ctor_m19A7F161AB0546D91A6E0134A4F7934DDCF3E766 (void);
// 0x000006F4 System.Void BNG.VehicleController/<crankEngine>d__24::System.IDisposable.Dispose()
extern void U3CcrankEngineU3Ed__24_System_IDisposable_Dispose_mE5E8694978801629B264AEF3F4F5881F2EF1F506 (void);
// 0x000006F5 System.Boolean BNG.VehicleController/<crankEngine>d__24::MoveNext()
extern void U3CcrankEngineU3Ed__24_MoveNext_mFFB22F12A1E8E8E2D31212D4068FE1926B0C7A16 (void);
// 0x000006F6 System.Object BNG.VehicleController/<crankEngine>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcrankEngineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD339DAF69F45B1291E701E6820564476DDA27D61 (void);
// 0x000006F7 System.Void BNG.VehicleController/<crankEngine>d__24::System.Collections.IEnumerator.Reset()
extern void U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_Reset_m7C0127B190B227D41B70C140B3537089E4C9556E (void);
// 0x000006F8 System.Object BNG.VehicleController/<crankEngine>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_get_Current_mC101083271693C5DE2B78C90392A29468AF958A5 (void);
// 0x000006F9 System.Void BNG.WheelObject::.ctor()
extern void WheelObject__ctor_m809E73E8227DE93BF89BD56230E3E81FFCB55EBC (void);
// 0x000006FA System.Void BNG.VREmulator::Start()
extern void VREmulator_Start_mACAFF1935D3CAAA0996C3E088E638E69D340E1BE (void);
// 0x000006FB System.Void BNG.VREmulator::OnBeforeRender()
extern void VREmulator_OnBeforeRender_m7C7ABAE1C4F491CF4EEB2FDD5CE127617C429CA6 (void);
// 0x000006FC System.Void BNG.VREmulator::onFirstActivate()
extern void VREmulator_onFirstActivate_m23E23241F5ADF0E09824E3303A5F116DC7B35D51 (void);
// 0x000006FD System.Void BNG.VREmulator::Update()
extern void VREmulator_Update_m6FD432970596CC5C9161BA9824A98A9F3F1517EC (void);
// 0x000006FE System.Boolean BNG.VREmulator::HasRequiredFocus()
extern void VREmulator_HasRequiredFocus_m837C60992E30438A6E7FF90E65434488714E9EDE (void);
// 0x000006FF System.Void BNG.VREmulator::CheckHeadControls()
extern void VREmulator_CheckHeadControls_m76CDCBA761303DA6FFA1035BEAB982AC2D753708 (void);
// 0x00000700 System.Void BNG.VREmulator::UpdateInputs()
extern void VREmulator_UpdateInputs_m21B3409C1172CC4294927360A686E6F7ABE1825F (void);
// 0x00000701 System.Void BNG.VREmulator::CheckPlayerControls()
extern void VREmulator_CheckPlayerControls_m002E1FF243A0622091E507CE2C35EA4B4CCDCF15 (void);
// 0x00000702 System.Void BNG.VREmulator::FixedUpdate()
extern void VREmulator_FixedUpdate_m6F618997B974D8FA1F2C83D15357A6866800A158 (void);
// 0x00000703 System.Void BNG.VREmulator::UpdateControllerPositions()
extern void VREmulator_UpdateControllerPositions_mAC2FF5D486A274D57DAD9653E13B10B4405AD530 (void);
// 0x00000704 System.Void BNG.VREmulator::checkGrabbers()
extern void VREmulator_checkGrabbers_mE6EF4B55FA0744E2967F4EE737B39FAA22B86CD4 (void);
// 0x00000705 System.Void BNG.VREmulator::ResetHands()
extern void VREmulator_ResetHands_m53A8C649005241519C40A92977CBB5F90DC36AFD (void);
// 0x00000706 System.Void BNG.VREmulator::ResetAll()
extern void VREmulator_ResetAll_m775EF3B5E68E6000CCC2BEAB17416D9544977B81 (void);
// 0x00000707 System.Void BNG.VREmulator::OnEnable()
extern void VREmulator_OnEnable_m27BAFA120768B64BD0A0CC0B54E717738ECCD66B (void);
// 0x00000708 System.Void BNG.VREmulator::OnDisable()
extern void VREmulator_OnDisable_m114B7AEA6E1ADB70B981E752F0065EA603AAB9E2 (void);
// 0x00000709 System.Void BNG.VREmulator::OnApplicationQuit()
extern void VREmulator_OnApplicationQuit_m016DE469111FB8BE29A200BA176CEC1044CE44C2 (void);
// 0x0000070A System.Void BNG.VREmulator::.ctor()
extern void VREmulator__ctor_m4FF677FD505F2C2563486429695C04F3ACC9BDBD (void);
// 0x0000070B System.Void BNG.Waypoint::OnDrawGizmosSelected()
extern void Waypoint_OnDrawGizmosSelected_m89DBE667FE7A8D3A67F755D15BA0C9FC244C6D03 (void);
// 0x0000070C System.Void BNG.Waypoint::.ctor()
extern void Waypoint__ctor_m0E4B80942B70F603D7BCEC137EBDC3C9143707F8 (void);
// 0x0000070D System.Void BNG.Zipline::Start()
extern void Zipline_Start_mA62FDD07845CE2A513F416A91E2CA41B6C0A71E8 (void);
// 0x0000070E System.Void BNG.Zipline::Update()
extern void Zipline_Update_m01749931F4689A7720DFA6E1157C900499322E2B (void);
// 0x0000070F System.Void BNG.Zipline::OnDrawGizmosSelected()
extern void Zipline_OnDrawGizmosSelected_m1C1AE3F98C10B329E2B3836A59E23BC616664410 (void);
// 0x00000710 System.Void BNG.Zipline::OnTrigger(System.Single)
extern void Zipline_OnTrigger_m493C37F23897EC3818AFC925E44592A1E94C9C1A (void);
// 0x00000711 System.Void BNG.Zipline::OnButton1()
extern void Zipline_OnButton1_mB8EE2F0E69BA858F539551264306FC8677B85E47 (void);
// 0x00000712 System.Void BNG.Zipline::OnButton2()
extern void Zipline_OnButton2_mA6963DD9741A11FDD5EFFF67C48307D6F65226A9 (void);
// 0x00000713 System.Void BNG.Zipline::moveTowards(UnityEngine.Vector3,System.Boolean)
extern void Zipline_moveTowards_mFB2EE2434AD8B92C964C42A17ABD89E1DEDCDC60 (void);
// 0x00000714 System.Void BNG.Zipline::.ctor()
extern void Zipline__ctor_m5EE67BC7402CD1B1654B3910D5638E204B48CBC5 (void);
// 0x00000715 System.Void BNG.ControllerOffsetHelper::Start()
extern void ControllerOffsetHelper_Start_mB803A95118152DA6B6C10A4FE16B7605718782C8 (void);
// 0x00000716 System.Collections.IEnumerator BNG.ControllerOffsetHelper::checkForController()
extern void ControllerOffsetHelper_checkForController_mDEF21DA6D2CD0C9DA8DA6E5565558E78085FFAEC (void);
// 0x00000717 System.Void BNG.ControllerOffsetHelper::OnControllerFound()
extern void ControllerOffsetHelper_OnControllerFound_m09EFBC996D68CD3095263DCE7BD232BFFED00DAD (void);
// 0x00000718 BNG.ControllerOffset BNG.ControllerOffsetHelper::GetControllerOffset(System.String)
extern void ControllerOffsetHelper_GetControllerOffset_m10CDC2657E9B6D3C2D6A8F998EC6B089BE97F196 (void);
// 0x00000719 System.Void BNG.ControllerOffsetHelper::DefineControllerOffsets()
extern void ControllerOffsetHelper_DefineControllerOffsets_m1AA9BCDC96702EF7405E65B49A40DAA3D1A87917 (void);
// 0x0000071A BNG.ControllerOffset BNG.ControllerOffsetHelper::GetOpenXROffset()
extern void ControllerOffsetHelper_GetOpenXROffset_mD35E74417870CD0B5B6AEACFE70D5A429D6B7347 (void);
// 0x0000071B System.Void BNG.ControllerOffsetHelper::.ctor()
extern void ControllerOffsetHelper__ctor_m2FD11B657A0D7CC44F946D42597F2F8472A6A93C (void);
// 0x0000071C System.Boolean BNG.ControllerOffsetHelper::<GetControllerOffset>b__9_0(BNG.ControllerOffset)
extern void ControllerOffsetHelper_U3CGetControllerOffsetU3Eb__9_0_m65D2E817A0C9AB3FCF5FA9BEC21BAD2282891630 (void);
// 0x0000071D System.Void BNG.ControllerOffsetHelper/<checkForController>d__7::.ctor(System.Int32)
extern void U3CcheckForControllerU3Ed__7__ctor_mFC051541FAA77CEA04AE69022B37A2674CD7586F (void);
// 0x0000071E System.Void BNG.ControllerOffsetHelper/<checkForController>d__7::System.IDisposable.Dispose()
extern void U3CcheckForControllerU3Ed__7_System_IDisposable_Dispose_m27369273FD3B20AF697737AFFB806A631193C220 (void);
// 0x0000071F System.Boolean BNG.ControllerOffsetHelper/<checkForController>d__7::MoveNext()
extern void U3CcheckForControllerU3Ed__7_MoveNext_mDB0A3594718D5553B38BA59E8AAB82C5AFA3385F (void);
// 0x00000720 System.Object BNG.ControllerOffsetHelper/<checkForController>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcheckForControllerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21B9ED608FA7D8674CD950E7C90C5958B137F32A (void);
// 0x00000721 System.Void BNG.ControllerOffsetHelper/<checkForController>d__7::System.Collections.IEnumerator.Reset()
extern void U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_Reset_mC1A7223AE1F47973069023B639DA4E17BA68AB89 (void);
// 0x00000722 System.Object BNG.ControllerOffsetHelper/<checkForController>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_get_Current_mBD2E65EF7F83B9268E52DF06325244A4D611314D (void);
// 0x00000723 System.String BNG.ControllerOffset::get_ControllerName()
extern void ControllerOffset_get_ControllerName_m1F823F45DC505BBD6D28F22F1B17D3F840337FCB (void);
// 0x00000724 System.Void BNG.ControllerOffset::set_ControllerName(System.String)
extern void ControllerOffset_set_ControllerName_m3A9EE57B31DE9083FF0B042CEE8CFEFCCDA8E295 (void);
// 0x00000725 UnityEngine.Vector3 BNG.ControllerOffset::get_LeftControllerPositionOffset()
extern void ControllerOffset_get_LeftControllerPositionOffset_m75FA410FC43E163C19CDD5D858EC3F13196454DC (void);
// 0x00000726 System.Void BNG.ControllerOffset::set_LeftControllerPositionOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_LeftControllerPositionOffset_m3A25B37C99FD87B9FC0BB7F94C2BAB9ABAF59CA2 (void);
// 0x00000727 UnityEngine.Vector3 BNG.ControllerOffset::get_RightControllerPositionOffset()
extern void ControllerOffset_get_RightControllerPositionOffset_m4991647F97F239055F5886D7C59DF600DDE63139 (void);
// 0x00000728 System.Void BNG.ControllerOffset::set_RightControllerPositionOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_RightControllerPositionOffset_m9C2EAAE04EAB7F9CCB5C007A2D63C9689262A948 (void);
// 0x00000729 UnityEngine.Vector3 BNG.ControllerOffset::get_LeftControllerRotationOffset()
extern void ControllerOffset_get_LeftControllerRotationOffset_m2A7FBA8984A9ECDBCADC6FC37E91FD9D04FC665F (void);
// 0x0000072A System.Void BNG.ControllerOffset::set_LeftControllerRotationOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_LeftControllerRotationOffset_m07A66EEADBF858373E6079A63F1E6EC797707461 (void);
// 0x0000072B UnityEngine.Vector3 BNG.ControllerOffset::get_RightControlleRotationOffset()
extern void ControllerOffset_get_RightControlleRotationOffset_m9B6F98193FA898D7D2F060DD19175D4375F7B66B (void);
// 0x0000072C System.Void BNG.ControllerOffset::set_RightControlleRotationOffset(UnityEngine.Vector3)
extern void ControllerOffset_set_RightControlleRotationOffset_mA6D15F8FDFBBBC6FFC6920E5E18F98DF4C156A39 (void);
// 0x0000072D System.Void BNG.ControllerOffset::.ctor()
extern void ControllerOffset__ctor_m089C77D4E04C444345B9E8F97395D1FC525CB8C6 (void);
// 0x0000072E System.Void BNG.DetachableLimb::DoDismemberment(BNG.Grabber)
extern void DetachableLimb_DoDismemberment_mEA4B800B2894204054881B0CF8D5A7F6325197D5 (void);
// 0x0000072F System.Void BNG.DetachableLimb::ReverseDismemberment()
extern void DetachableLimb_ReverseDismemberment_mD2586AD48D00DB8ADF82E13E78DCCFF5F4E3B0B6 (void);
// 0x00000730 System.Void BNG.DetachableLimb::.ctor()
extern void DetachableLimb__ctor_mB18D18C6BE3DEB20C6D50094720E09BE54ABB37C (void);
// 0x00000731 System.Void BNG.DoorHelper::Start()
extern void DoorHelper_Start_m07F4B5C0FA21CA2485BC870D3AFFDD460BF4DD66 (void);
// 0x00000732 System.Void BNG.DoorHelper::Update()
extern void DoorHelper_Update_mB3284BAD3D12E32FDEA24A1DC188B63A4B18A381 (void);
// 0x00000733 System.Void BNG.DoorHelper::.ctor()
extern void DoorHelper__ctor_m03D96A1E0566C232EF2059833E2C3E6024064B98 (void);
// 0x00000734 System.Void BNG.GrabberArea::Update()
extern void GrabberArea_Update_m1B818C52A8934FAAB61CBD066DA7AAE07D88447C (void);
// 0x00000735 BNG.Grabber BNG.GrabberArea::GetOpenGrabber()
extern void GrabberArea_GetOpenGrabber_mCFAE6A8B88EC6889A886BFDB460A4C1F008F501C (void);
// 0x00000736 System.Void BNG.GrabberArea::OnTriggerEnter(UnityEngine.Collider)
extern void GrabberArea_OnTriggerEnter_m33FEBF579D07EFC76847CAE61074131BD12873CE (void);
// 0x00000737 System.Void BNG.GrabberArea::OnTriggerExit(UnityEngine.Collider)
extern void GrabberArea_OnTriggerExit_mA6BB9984E21B04F474B51E2BA39A395838EC4259 (void);
// 0x00000738 System.Void BNG.GrabberArea::.ctor()
extern void GrabberArea__ctor_m99E3658ADA126FEEC26924F4779CE38C59EB7B77 (void);
// 0x00000739 System.Void BNG.HandCollision::Start()
extern void HandCollision_Start_m5FB57150810F078E4F8E2A4613D1E93F466B4DE7 (void);
// 0x0000073A System.Void BNG.HandCollision::Update()
extern void HandCollision_Update_mD588C96A861F704812919ADB5265AD9275078721 (void);
// 0x0000073B System.Void BNG.HandCollision::.ctor()
extern void HandCollision__ctor_m98468794516D3AA8D87FD471AD24FE1B8882CCA6 (void);
// 0x0000073C UnityEngine.Vector3 BNG.HandController::get_offsetPosition()
extern void HandController_get_offsetPosition_m5624A45D4DD418E11B1EE14B73A60C4FAE51DC2E (void);
// 0x0000073D UnityEngine.Vector3 BNG.HandController::get_offsetRotation()
extern void HandController_get_offsetRotation_mD61B64DAE2701E5A8B1E95B93C9542F2F6DD9213 (void);
// 0x0000073E System.Void BNG.HandController::Start()
extern void HandController_Start_mF22290AFF284DD460D8BC1C27565AE9D89FEA0C0 (void);
// 0x0000073F System.Void BNG.HandController::Update()
extern void HandController_Update_m7C430E15A228AD6158E43BA7A2FE31E720E2569E (void);
// 0x00000740 System.Void BNG.HandController::CheckForGrabChange()
extern void HandController_CheckForGrabChange_mE2040F21D4EB59C6C273602A11AC1605EB70976A (void);
// 0x00000741 System.Void BNG.HandController::OnGrabChange(UnityEngine.GameObject)
extern void HandController_OnGrabChange_m2B89A4AAAF492C0B7B8B037676B9A9305D162202 (void);
// 0x00000742 System.Void BNG.HandController::OnGrabDrop()
extern void HandController_OnGrabDrop_m798C16D84548324651B85B4AC8C1C55DCC51CE66 (void);
// 0x00000743 System.Void BNG.HandController::SetHandAnimator()
extern void HandController_SetHandAnimator_m25CF0AF24552B92BDBE4039AB11B8735942A7679 (void);
// 0x00000744 System.Void BNG.HandController::UpdateFromInputs()
extern void HandController_UpdateFromInputs_m1D2A3D6B3C940EF06CA3A41602063E5D91FCCF09 (void);
// 0x00000745 System.Void BNG.HandController::UpdateAnimimationStates()
extern void HandController_UpdateAnimimationStates_m30D184C2374B647430235982AECF82EDAAD74524 (void);
// 0x00000746 System.Void BNG.HandController::setAnimatorBlend(System.Single,System.Single,System.Single,System.Int32)
extern void HandController_setAnimatorBlend_mEE341BBC98F0279EE92BF6E7A93159CF7F02E3CC (void);
// 0x00000747 System.Boolean BNG.HandController::IsAnimatorGrabbable()
extern void HandController_IsAnimatorGrabbable_m3544CBB3E994C051ABA2CFBF4BE7332E8E68D035 (void);
// 0x00000748 System.Void BNG.HandController::UpdateHandPoser()
extern void HandController_UpdateHandPoser_m621AAB64BED806F7741634759AB4BF62250150E9 (void);
// 0x00000749 System.Void BNG.HandController::EnableHandPoser()
extern void HandController_EnableHandPoser_mB1763E85D7C4ACA08C764880D2ED54E48A12D287 (void);
// 0x0000074A System.Void BNG.HandController::EnableAutoPoser(System.Boolean)
extern void HandController_EnableAutoPoser_mBD0BC613B55FE94BD35FA0E545224DA5075CA995 (void);
// 0x0000074B System.Void BNG.HandController::DisableAutoPoser()
extern void HandController_DisableAutoPoser_mF833266581823D3DD023B252B0EE84B9359CD112 (void);
// 0x0000074C System.Void BNG.HandController::EnableHandAnimator()
extern void HandController_EnableHandAnimator_m18FFDA120887E8C8176F65548949F2A0CC6A850D (void);
// 0x0000074D System.Void BNG.HandController::DisableHandAnimator()
extern void HandController_DisableHandAnimator_m8BF066B350402DAB9B18EBC4AD0973A4B0CC6963 (void);
// 0x0000074E System.Void BNG.HandController::OnGrabberGrabbed(BNG.Grabbable)
extern void HandController_OnGrabberGrabbed_m292904F59E6F14882A737113D553F296C3DFAA2A (void);
// 0x0000074F System.Void BNG.HandController::UpdateCurrentHandPose()
extern void HandController_UpdateCurrentHandPose_m325C10BEC9A3BE43C472DC2BBDE69EDB9A0D1814 (void);
// 0x00000750 System.Void BNG.HandController::OnGrabberReleased(BNG.Grabbable)
extern void HandController_OnGrabberReleased_m4A6987F9799C932A30E9305BA486EA677943F0A2 (void);
// 0x00000751 System.Void BNG.HandController::.ctor()
extern void HandController__ctor_mF6443126DB4D13E3787B9F0976CD8CAC6C199B4E (void);
// 0x00000752 System.Void BNG.HandleGFXHelper::Start()
extern void HandleGFXHelper_Start_mC7815A77FCD16AE6D72FD9AC4C338FBDCF963650 (void);
// 0x00000753 System.Void BNG.HandleGFXHelper::Update()
extern void HandleGFXHelper_Update_m0AE93D69DF44E5708FA3CF4B17A9E471436C1A3C (void);
// 0x00000754 System.Void BNG.HandleGFXHelper::.ctor()
extern void HandleGFXHelper__ctor_m258887EC717E2D5B661B728A801D14CFA3D4C3A1 (void);
// 0x00000755 System.Void BNG.HandleHelper::Start()
extern void HandleHelper_Start_mF5FE4010959FF22269E9823152D639A356FC885A (void);
// 0x00000756 System.Void BNG.HandleHelper::FixedUpdate()
extern void HandleHelper_FixedUpdate_m27EAF9F026E484F89154827F542FE31E3B012618 (void);
// 0x00000757 System.Void BNG.HandleHelper::OnCollisionEnter(UnityEngine.Collision)
extern void HandleHelper_OnCollisionEnter_mACB9A6062E02A927AE16B88C6491624A48F8CC79 (void);
// 0x00000758 System.Collections.IEnumerator BNG.HandleHelper::doRelease()
extern void HandleHelper_doRelease_mDAE6F1AAFAA97A45DF00B3993F893AC7995E0188 (void);
// 0x00000759 System.Void BNG.HandleHelper::.ctor()
extern void HandleHelper__ctor_m509BD114724BF3CC9E3CF319A4612C48CE6EA2AD (void);
// 0x0000075A System.Void BNG.HandleHelper/<doRelease>d__10::.ctor(System.Int32)
extern void U3CdoReleaseU3Ed__10__ctor_mD52054CD12101B675134D31D0F7856A16591F0CF (void);
// 0x0000075B System.Void BNG.HandleHelper/<doRelease>d__10::System.IDisposable.Dispose()
extern void U3CdoReleaseU3Ed__10_System_IDisposable_Dispose_m37096A88E7669C7365BF462029BD81C23C2767AA (void);
// 0x0000075C System.Boolean BNG.HandleHelper/<doRelease>d__10::MoveNext()
extern void U3CdoReleaseU3Ed__10_MoveNext_mF0B8715FD2165089E5BF4CE8F4171846D387491D (void);
// 0x0000075D System.Object BNG.HandleHelper/<doRelease>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoReleaseU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m319A13CB98EBF9D0EFE9DBC29FDA210815D0843F (void);
// 0x0000075E System.Void BNG.HandleHelper/<doRelease>d__10::System.Collections.IEnumerator.Reset()
extern void U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_Reset_mD3E08E149BAAC161D6CC5CA74F6AF7A16E2AF610 (void);
// 0x0000075F System.Object BNG.HandleHelper/<doRelease>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_get_Current_m59FD864B0226579CC0A62D3127FC15CA5F464148 (void);
// 0x00000760 System.Boolean BNG.HandPhysics::get_HoldingObject()
extern void HandPhysics_get_HoldingObject_mCB37A1AB20FA6D396AEBDE616FE704C8C00C490D (void);
// 0x00000761 System.Void BNG.HandPhysics::Start()
extern void HandPhysics_Start_m85071739095631372E9DAB02045298C47C7A4058 (void);
// 0x00000762 System.Void BNG.HandPhysics::Update()
extern void HandPhysics_Update_mBFF00D89F678514C06134D0195F168F0FB41C6DE (void);
// 0x00000763 System.Void BNG.HandPhysics::FixedUpdate()
extern void HandPhysics_FixedUpdate_m90B062911EF2F31ED68CFDEF2A2C928F92499EF8 (void);
// 0x00000764 System.Void BNG.HandPhysics::initHandColliders()
extern void HandPhysics_initHandColliders_m128E6AE3BF3A5658E221692A0EDB7F438A04D729 (void);
// 0x00000765 System.Void BNG.HandPhysics::checkRemoteCollision()
extern void HandPhysics_checkRemoteCollision_mF7A8D348BCCE41395E57D7F48843D549BC01BB6E (void);
// 0x00000766 System.Void BNG.HandPhysics::drawDistanceLine()
extern void HandPhysics_drawDistanceLine_m6EC82DAEC314424FABBFC772983867377C9280D4 (void);
// 0x00000767 System.Void BNG.HandPhysics::checkBreakDistance()
extern void HandPhysics_checkBreakDistance_m5F93990C2367F9818614BDA2BD83A0AEAC06A8FE (void);
// 0x00000768 System.Void BNG.HandPhysics::updateHandGraphics()
extern void HandPhysics_updateHandGraphics_m233A9ECDD4C18877C01A665332A1C1429A323532 (void);
// 0x00000769 System.Collections.IEnumerator BNG.HandPhysics::UnignoreAllCollisions()
extern void HandPhysics_UnignoreAllCollisions_m37F6A6D0FDFB10308987F2A81B9F941F0FA7ACF2 (void);
// 0x0000076A System.Void BNG.HandPhysics::IgnoreGrabbableCollisions(BNG.Grabbable,System.Boolean)
extern void HandPhysics_IgnoreGrabbableCollisions_m3AFC9F9AA8A2078C66F8C3539102641DF90F0EAE (void);
// 0x0000076B System.Void BNG.HandPhysics::DisableHandColliders()
extern void HandPhysics_DisableHandColliders_m9DC30516A05512F1D5EE6D8210635A1704851A02 (void);
// 0x0000076C System.Void BNG.HandPhysics::EnableHandColliders()
extern void HandPhysics_EnableHandColliders_mF1B931C9C317CE4EBBB28F27109A6407DFD3F799 (void);
// 0x0000076D System.Void BNG.HandPhysics::OnGrabbedObject(BNG.Grabbable)
extern void HandPhysics_OnGrabbedObject_mA26EB711E148CA76E1EBCEFE67F160E52360B996 (void);
// 0x0000076E System.Void BNG.HandPhysics::LockLocalPosition()
extern void HandPhysics_LockLocalPosition_m5B087EFE9D27158682E4D8C46685BCA8DC658948 (void);
// 0x0000076F System.Void BNG.HandPhysics::UnlockLocalPosition()
extern void HandPhysics_UnlockLocalPosition_m9852A6D8D9FFA57C6C39F8F99254FCAF1C184AC8 (void);
// 0x00000770 System.Void BNG.HandPhysics::OnReleasedObject(BNG.Grabbable)
extern void HandPhysics_OnReleasedObject_mE12BBD43D141AE7B0776C825780B3657D5D2FC15 (void);
// 0x00000771 System.Void BNG.HandPhysics::OnEnable()
extern void HandPhysics_OnEnable_mC725877061292A463463C8F67CE9FF7AD9CE9C6F (void);
// 0x00000772 System.Void BNG.HandPhysics::LockOffset()
extern void HandPhysics_LockOffset_mC1AFC2C3C88FEC905083039B089EE4A42524031F (void);
// 0x00000773 System.Void BNG.HandPhysics::UnlockOffset()
extern void HandPhysics_UnlockOffset_m431D610A14A64B13F98BDA849B26991AAD3F9303 (void);
// 0x00000774 System.Void BNG.HandPhysics::OnDisable()
extern void HandPhysics_OnDisable_m01D45F24C1D0EF9E34F2D9430DD34879FDF23555 (void);
// 0x00000775 System.Void BNG.HandPhysics::OnCollisionStay(UnityEngine.Collision)
extern void HandPhysics_OnCollisionStay_m0865027782CA13FA31E4B3DA7119BD228233EC76 (void);
// 0x00000776 System.Boolean BNG.HandPhysics::IsValidCollision(UnityEngine.Collider)
extern void HandPhysics_IsValidCollision_mD91B7B0F4F14DA630B45F6923308092DFE101067 (void);
// 0x00000777 System.Void BNG.HandPhysics::.ctor()
extern void HandPhysics__ctor_m5147044F40DFA05F5CF538ED36A9777C7D86A482 (void);
// 0x00000778 System.Void BNG.HandPhysics/<UnignoreAllCollisions>d__31::.ctor(System.Int32)
extern void U3CUnignoreAllCollisionsU3Ed__31__ctor_mDCA6B287F53616443BDE870782AF6712B0FC20A9 (void);
// 0x00000779 System.Void BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.IDisposable.Dispose()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_IDisposable_Dispose_mF39CF681E821B10567ABD469845B6FB4B35F31AB (void);
// 0x0000077A System.Boolean BNG.HandPhysics/<UnignoreAllCollisions>d__31::MoveNext()
extern void U3CUnignoreAllCollisionsU3Ed__31_MoveNext_mC8F6B483E77C259497B9655714365FF344E31096 (void);
// 0x0000077B System.Object BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B2BD3A00E4095840F65AB3430D100109A112176 (void);
// 0x0000077C System.Void BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.Collections.IEnumerator.Reset()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_Reset_m62955C6C576A05B908A26F98AD88C2C00BCB3830 (void);
// 0x0000077D System.Object BNG.HandPhysics/<UnignoreAllCollisions>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_get_Current_m68B3072808668BA3D5DCEB0D0796007F0C4BFFF1 (void);
// 0x0000077E System.Void BNG.HandRepresentationHelper::Update()
extern void HandRepresentationHelper_Update_m7043551AFE52F9E3F7FA13A3F9E31E6636D0A158 (void);
// 0x0000077F System.Void BNG.HandRepresentationHelper::.ctor()
extern void HandRepresentationHelper__ctor_m55377B674F450CB6FD8FFF9F5D251A8F7C54E539 (void);
// 0x00000780 System.Void BNG.IgnoreColliders::Start()
extern void IgnoreColliders_Start_mEC77ABF8090B8ABEC469BCC07443AC9CAA57F3AF (void);
// 0x00000781 System.Void BNG.IgnoreColliders::.ctor()
extern void IgnoreColliders__ctor_mDD47503F31E630126C55D172448DD23290FD8E25 (void);
// 0x00000782 System.Void BNG.InvalidTeleportArea::.ctor()
extern void InvalidTeleportArea__ctor_mB8E1FEE33E34EF06C23019C096E7D9564A41FD2E (void);
// 0x00000783 System.Void BNG.JointBreaker::Start()
extern void JointBreaker_Start_mE912068595AFA9D8E58517F9B03CFED4BEA14934 (void);
// 0x00000784 System.Void BNG.JointBreaker::Update()
extern void JointBreaker_Update_mC378419E3A344ABEBF35F489B11A53D7D26416AF (void);
// 0x00000785 System.Void BNG.JointBreaker::BreakJoint()
extern void JointBreaker_BreakJoint_m7CF1C18B64B54C3E6815B4FAEF448094241AF42C (void);
// 0x00000786 System.Void BNG.JointBreaker::.ctor()
extern void JointBreaker__ctor_m5D6F807A7A40F3EFDF62C1FCB656C067E1964CB9 (void);
// 0x00000787 System.Void BNG.JointHelper::Start()
extern void JointHelper_Start_m9FA780E22DAEE8EF48C13B701804ECBA6AF36E09 (void);
// 0x00000788 System.Void BNG.JointHelper::lockPosition()
extern void JointHelper_lockPosition_mCEC4A2983C0137BC90318E1123402A4FE3030E5F (void);
// 0x00000789 System.Void BNG.JointHelper::LateUpdate()
extern void JointHelper_LateUpdate_m60508106B58906A2532D918ED425F91A8D4EE7CE (void);
// 0x0000078A System.Void BNG.JointHelper::FixedUpdate()
extern void JointHelper_FixedUpdate_m33BC028162EB9C2B81935B7010C269B3FE9E6B23 (void);
// 0x0000078B System.Void BNG.JointHelper::.ctor()
extern void JointHelper__ctor_mDEF6BF5D7C6B89962FA1CAE8791EB6C0EEBE8F8C (void);
// 0x0000078C System.Void BNG.RagdollHelper::Start()
extern void RagdollHelper_Start_m811E7E97D2BC4BA42F3C77DCFA725C056F2D4683 (void);
// 0x0000078D System.Void BNG.RagdollHelper::.ctor()
extern void RagdollHelper__ctor_m27D1A018BF8F3C7F21CE1698722DD39280CB384F (void);
// 0x0000078E System.Void BNG.RingHelper::Start()
extern void RingHelper_Start_mE6BC07F626583A6613832F81CCC0F50058196DC8 (void);
// 0x0000078F System.Void BNG.RingHelper::Update()
extern void RingHelper_Update_mB5C6D1170A4DA5D13DFC165BD3B921F2D8331BBC (void);
// 0x00000790 System.Void BNG.RingHelper::AssignCamera()
extern void RingHelper_AssignCamera_m8B002DAFF59BED8180F52E93876EDEA2DCEE8B08 (void);
// 0x00000791 System.Void BNG.RingHelper::AssignGrabbers()
extern void RingHelper_AssignGrabbers_mA00EE89B040238CDD2D4CC73EE3FBE183D7FF729 (void);
// 0x00000792 UnityEngine.Color BNG.RingHelper::getSelectedColor()
extern void RingHelper_getSelectedColor_m7FD866BA83AE3CEE66E8A656DD806B24FDA5D8F1 (void);
// 0x00000793 System.Void BNG.RingHelper::.ctor()
extern void RingHelper__ctor_mE115485C5F52E43163A531A321FA092A3E73DD9D (void);
// 0x00000794 System.Void BNG.ScaleMaterialHelper::Start()
extern void ScaleMaterialHelper_Start_m4FB25C009AA06A10DBE653ABAC2083EC2CC5822C (void);
// 0x00000795 System.Void BNG.ScaleMaterialHelper::updateTexture()
extern void ScaleMaterialHelper_updateTexture_m5D04309F196D8BBE0CB50E26EFB7E8BCEA38C31C (void);
// 0x00000796 System.Void BNG.ScaleMaterialHelper::OnDrawGizmosSelected()
extern void ScaleMaterialHelper_OnDrawGizmosSelected_m59D6C55B1297E3FBE98E12957E7328F11331C573 (void);
// 0x00000797 System.Void BNG.ScaleMaterialHelper::.ctor()
extern void ScaleMaterialHelper__ctor_mAFC6D3E295FC19D404188FBCA6871604DD263460 (void);
// 0x00000798 System.Void BNG.StaticBatch::Start()
extern void StaticBatch_Start_m8C1C37C00DE626E31485181D791994CD64065CD3 (void);
// 0x00000799 System.Void BNG.StaticBatch::.ctor()
extern void StaticBatch__ctor_m82D66EC09859625BB62A271269C7671D89AA19D5 (void);
// 0x0000079A System.Void BNG.TeleportDestination::.ctor()
extern void TeleportDestination__ctor_m66A0E37CF76004559CA15430391CEC3AA933B64D (void);
// 0x0000079B System.Void BNG.TeleportPlayerOnEnter::OnTriggerEnter(UnityEngine.Collider)
extern void TeleportPlayerOnEnter_OnTriggerEnter_mE6FE233A5A0BDFB5581B25B0610C45AE2980E8BC (void);
// 0x0000079C System.Void BNG.TeleportPlayerOnEnter::.ctor()
extern void TeleportPlayerOnEnter__ctor_m8769053C2E465CEFA98ED4144594DE27380FF7D9 (void);
// 0x0000079D System.Void BNG.UITrigger::.ctor()
extern void UITrigger__ctor_m1AD800C6EE01FC69B679693336D7734875141FB9 (void);
// 0x0000079E System.Void BNG.VRIFGrabpointUpdater::Start()
extern void VRIFGrabpointUpdater_Start_m1B7BF1B7AE90765E9F435D8175D8C79AE822220B (void);
// 0x0000079F System.Void BNG.VRIFGrabpointUpdater::ApplyGrabPointUpdate()
extern void VRIFGrabpointUpdater_ApplyGrabPointUpdate_mDC7E28C110784795DFA3F45AAB5749A1E86159D8 (void);
// 0x000007A0 System.Void BNG.VRIFGrabpointUpdater::.ctor()
extern void VRIFGrabpointUpdater__ctor_mBFEBB64D042DF0E195C6A2A050E8D65683359F4C (void);
// 0x000007A1 System.Void BNG.UICanvasGroup::ActivateCanvas(System.Int32)
extern void UICanvasGroup_ActivateCanvas_m7EBC4F4BC737955867FDFD97FBFEF3FE99340B7E (void);
// 0x000007A2 System.Void BNG.UICanvasGroup::.ctor()
extern void UICanvasGroup__ctor_m417BF34E6536188B9F32EC4B5786115ADB133B62 (void);
// 0x000007A3 System.Void BNG.UIPointer::Awake()
extern void UIPointer_Awake_m0B32646D8D81938FFC27A3F149C26A37DCC44BCB (void);
// 0x000007A4 System.Void BNG.UIPointer::OnEnable()
extern void UIPointer_OnEnable_m04D1BDC2458847FDD7730854AA61871BB17E6C9D (void);
// 0x000007A5 System.Void BNG.UIPointer::Update()
extern void UIPointer_Update_mF4D408E197D06C7112B23CFBE5E83B4C8E7E5661 (void);
// 0x000007A6 System.Void BNG.UIPointer::HidePointer()
extern void UIPointer_HidePointer_m25160298FDB8284CBE764749C0E084B96BD9B9C2 (void);
// 0x000007A7 System.Void BNG.UIPointer::.ctor()
extern void UIPointer__ctor_mE211F8867B883C08B699601303D74F85F4C3DFCA (void);
// 0x000007A8 System.Void BNG.VRCanvas::Start()
extern void VRCanvas_Start_m0167BA329DBB85DF9764BA2A89BA6BBE39330C9D (void);
// 0x000007A9 System.Void BNG.VRCanvas::.ctor()
extern void VRCanvas__ctor_m3A58870925A307299F586ED793283B749F191AF6 (void);
// 0x000007AA System.Void BNG.VRKeyboard::Awake()
extern void VRKeyboard_Awake_mD8B8EEDFBCC76C5769AF2A18F4560B308C5F541F (void);
// 0x000007AB System.Void BNG.VRKeyboard::PressKey(System.String)
extern void VRKeyboard_PressKey_mA355DE6083DF48539883D9E2CCEB59372072FCDC (void);
// 0x000007AC System.Void BNG.VRKeyboard::UpdateInputField(System.String)
extern void VRKeyboard_UpdateInputField_m4670D5BB15BF60A10A385451FFCFABC7D2FD53FC (void);
// 0x000007AD System.Void BNG.VRKeyboard::PlayClickSound()
extern void VRKeyboard_PlayClickSound_mFEEEC2FF0E6893396FA820FBA6AF0FB3D2457C30 (void);
// 0x000007AE System.Void BNG.VRKeyboard::MoveCaretUp()
extern void VRKeyboard_MoveCaretUp_mBB107B868B40356F57B2D11124B73C89E9799693 (void);
// 0x000007AF System.Void BNG.VRKeyboard::MoveCaretBack()
extern void VRKeyboard_MoveCaretBack_m435A1542AA0C50C79D7469A631CD85EA49E80768 (void);
// 0x000007B0 System.Void BNG.VRKeyboard::ToggleShift()
extern void VRKeyboard_ToggleShift_m9CED9817B4D946E671403CB74CC7223DE06A244F (void);
// 0x000007B1 System.Collections.IEnumerator BNG.VRKeyboard::IncreaseInputFieldCareteRoutine()
extern void VRKeyboard_IncreaseInputFieldCareteRoutine_mA5A7B0469DB9CA340FCC61FE691E1D32B22330A7 (void);
// 0x000007B2 System.Collections.IEnumerator BNG.VRKeyboard::DecreaseInputFieldCareteRoutine()
extern void VRKeyboard_DecreaseInputFieldCareteRoutine_m99A480794A1376168F5CA7522609F608B6E79EFD (void);
// 0x000007B3 System.Void BNG.VRKeyboard::AttachToInputField(UnityEngine.UI.InputField)
extern void VRKeyboard_AttachToInputField_mDE13878C9A0496E8E3F50E5D7E55BFA895568277 (void);
// 0x000007B4 System.Void BNG.VRKeyboard::.ctor()
extern void VRKeyboard__ctor_mFE1DFB73614D0EC7AADE7F8BD4405367066F21AA (void);
// 0x000007B5 System.Void BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::.ctor(System.Int32)
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11__ctor_m6B52206D9AD68684635AECBA36C4A695BA139229 (void);
// 0x000007B6 System.Void BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.IDisposable.Dispose()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_IDisposable_Dispose_mC485916BEF71E98265213470EE03C3645D54FCA5 (void);
// 0x000007B7 System.Boolean BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::MoveNext()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_MoveNext_m66C8E0BB64C3BB98D7B7AF10B080BD15A036928B (void);
// 0x000007B8 System.Object BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFEBF070A20880A3593E17C006DF335B42455A04D (void);
// 0x000007B9 System.Void BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.Collections.IEnumerator.Reset()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_Reset_mF7DA7BB5F54D04238ADE99CC8F0E2E11C0DB2399 (void);
// 0x000007BA System.Object BNG.VRKeyboard/<IncreaseInputFieldCareteRoutine>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_get_Current_mCB0ED784DA9E42D74242188E0847008EDE8E9580 (void);
// 0x000007BB System.Void BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::.ctor(System.Int32)
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12__ctor_mC2D47F9D04A13D706CBB99AE40D96344477AEB70 (void);
// 0x000007BC System.Void BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.IDisposable.Dispose()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_IDisposable_Dispose_m1B79ABDEC1A0F05196D86FD802BCD0567D1CA9D6 (void);
// 0x000007BD System.Boolean BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::MoveNext()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_MoveNext_mCBAC7704D1BF428BC1E7031A585A8A4052DBEF5A (void);
// 0x000007BE System.Object BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D735647B49C6D7D9FCC55FEAD99026EC65EC6F8 (void);
// 0x000007BF System.Void BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_Reset_m6E4BBE3B605DAC51AA7CE1CB8CC7D32E31AA94D3 (void);
// 0x000007C0 System.Object BNG.VRKeyboard/<DecreaseInputFieldCareteRoutine>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_get_Current_m0AE9742C31A362120217031E32C2C9CB48F4E935 (void);
// 0x000007C1 System.Void BNG.VRKeyboardKey::Awake()
extern void VRKeyboardKey_Awake_m002435586360CC1FE9B132415E5A1EEC0DD99F8C (void);
// 0x000007C2 System.Void BNG.VRKeyboardKey::ToggleShift()
extern void VRKeyboardKey_ToggleShift_mF8609E926C6CC9C0B3DCA2FF0D439C6B39EB7F4F (void);
// 0x000007C3 System.Void BNG.VRKeyboardKey::OnKeyHit()
extern void VRKeyboardKey_OnKeyHit_m3C0A6B0AAE82E65FE550A4A8ECD74085D7A1F971 (void);
// 0x000007C4 System.Void BNG.VRKeyboardKey::OnKeyHit(System.String)
extern void VRKeyboardKey_OnKeyHit_m35193290DC2A8647553717CA033EC08887D76CA3 (void);
// 0x000007C5 System.Void BNG.VRKeyboardKey::.ctor()
extern void VRKeyboardKey__ctor_mC1711B5BD946F58D1ECC22DB45A8DED50DBD4BD2 (void);
// 0x000007C6 System.Void BNG.VRKeyboardKeyCollider::Awake()
extern void VRKeyboardKeyCollider_Awake_mA4AB4CA8827C31473C8661B8E05A6FF25611FA06 (void);
// 0x000007C7 System.Void BNG.VRKeyboardKeyCollider::Update()
extern void VRKeyboardKeyCollider_Update_m4DDC1780E8E28AD73F52BEBEB2179F5B85723A96 (void);
// 0x000007C8 System.Void BNG.VRKeyboardKeyCollider::OnTriggerEnter(UnityEngine.Collider)
extern void VRKeyboardKeyCollider_OnTriggerEnter_mC448D603938E9940857B2F2EF23BCE608E6CA3FA (void);
// 0x000007C9 System.Void BNG.VRKeyboardKeyCollider::OnTriggerExit(UnityEngine.Collider)
extern void VRKeyboardKeyCollider_OnTriggerExit_mD7958B8AECC1625DD4218B080C4D668421E4DA8A (void);
// 0x000007CA System.Void BNG.VRKeyboardKeyCollider::.ctor()
extern void VRKeyboardKeyCollider__ctor_mCC1A44E7F92A4D2D783B493FBB36C020C2B4E2CA (void);
// 0x000007CB System.Void BNG.VRTextInput::Awake()
extern void VRTextInput_Awake_mEE5B6FA863EB9DF999DAB7B6C42AB1B1D7E885D2 (void);
// 0x000007CC System.Void BNG.VRTextInput::Update()
extern void VRTextInput_Update_m61331218E12BC70C2658CCF802F41B17F750BEE8 (void);
// 0x000007CD System.Void BNG.VRTextInput::OnInputSelect()
extern void VRTextInput_OnInputSelect_mB8BA6A64DCD9F2A41820CBA8EC18E8C1AF6C3EE5 (void);
// 0x000007CE System.Void BNG.VRTextInput::OnInputDeselect()
extern void VRTextInput_OnInputDeselect_mE6A4B11FB36AF41C744667B2E369A80B6A57BF66 (void);
// 0x000007CF System.Void BNG.VRTextInput::.ctor()
extern void VRTextInput__ctor_m3AEA13D9A0FFC1F4F2184D30F824FEB4B1BB2E9D (void);
// 0x000007D0 UnityEngine.EventSystems.PointerEventData BNG.VRUISystem::get_EventData()
extern void VRUISystem_get_EventData_m5D6B0F0DA29D2DD290E0F7FC920BC8EFB36AA6BB (void);
// 0x000007D1 System.Void BNG.VRUISystem::set_EventData(UnityEngine.EventSystems.PointerEventData)
extern void VRUISystem_set_EventData_m2D416D932DDD915E647DE4BA7E29A5EB9FF38E22 (void);
// 0x000007D2 BNG.VRUISystem BNG.VRUISystem::get_Instance()
extern void VRUISystem_get_Instance_m590749E871D73F6C9DE03B401D888A2615E79496 (void);
// 0x000007D3 System.Void BNG.VRUISystem::Awake()
extern void VRUISystem_Awake_mCEBCA037FF370180DFED046E2F217D793095355A (void);
// 0x000007D4 System.Void BNG.VRUISystem::init()
extern void VRUISystem_init_m55FB34C998130C0FE544349399DA6D4F3B9465AB (void);
// 0x000007D5 System.Void BNG.VRUISystem::Process()
extern void VRUISystem_Process_mCB5B5D94CF894FF7E106FA4C062B73A7407DB102 (void);
// 0x000007D6 System.Boolean BNG.VRUISystem::InputReady()
extern void VRUISystem_InputReady_m2D6428E70DBCD8E96A70652328D4865503E3660A (void);
// 0x000007D7 System.Boolean BNG.VRUISystem::CameraCasterReady()
extern void VRUISystem_CameraCasterReady_m0FACD102086C468BC47C5D76312205C5B60FB0DD (void);
// 0x000007D8 System.Void BNG.VRUISystem::PressDown()
extern void VRUISystem_PressDown_m3526513AF8F6A5DC737A20DB5E3328422E236E5E (void);
// 0x000007D9 System.Void BNG.VRUISystem::Press()
extern void VRUISystem_Press_mCAE16ED6CE29CD07366735B87452CBD1250591E5 (void);
// 0x000007DA System.Void BNG.VRUISystem::Release()
extern void VRUISystem_Release_mC5B78B0A6F19C5974D0AD6A2BB0DD16B7078F615 (void);
// 0x000007DB System.Void BNG.VRUISystem::ClearAll()
extern void VRUISystem_ClearAll_m93E8E4AAB731E6B58D9FF233FD82A1CDB60858E5 (void);
// 0x000007DC System.Void BNG.VRUISystem::SetPressingObject(UnityEngine.GameObject)
extern void VRUISystem_SetPressingObject_m8CD82F308BA950DD47FD1C0F987C524248D4267E (void);
// 0x000007DD System.Void BNG.VRUISystem::SetDraggingObject(UnityEngine.GameObject)
extern void VRUISystem_SetDraggingObject_mB9311237F45902E063583C810E33156D5D9D6B88 (void);
// 0x000007DE System.Void BNG.VRUISystem::SetReleasingObject(UnityEngine.GameObject)
extern void VRUISystem_SetReleasingObject_m5D575F22EB68486808BCBD1FE1A83BF683731455 (void);
// 0x000007DF System.Void BNG.VRUISystem::AssignCameraToAllCanvases(UnityEngine.Camera)
extern void VRUISystem_AssignCameraToAllCanvases_m46ACE5FF414715B1C3F7177F43993185577D381C (void);
// 0x000007E0 System.Void BNG.VRUISystem::AddCanvas(UnityEngine.Canvas)
extern void VRUISystem_AddCanvas_m3F40B71FD3AE5C44D2C8F0C85964EBB391AB5D56 (void);
// 0x000007E1 System.Void BNG.VRUISystem::AddCanvasToCamera(UnityEngine.Canvas,UnityEngine.Camera)
extern void VRUISystem_AddCanvasToCamera_m53C643F65910CE353707B1D38C7C83A504FC0328 (void);
// 0x000007E2 System.Void BNG.VRUISystem::UpdateControllerHand(BNG.ControllerHand)
extern void VRUISystem_UpdateControllerHand_mE0A081EDC8CEA8E456330F8537D44630904AC7BA (void);
// 0x000007E3 System.Void BNG.VRUISystem::.ctor()
extern void VRUISystem__ctor_m75072FE13A3EB015DF3C94E29EA4D0D03CE3FD2C (void);
// 0x000007E4 System.Void BNG.DestroyIfPlayMode::Start()
extern void DestroyIfPlayMode_Start_m28902755321635A3BD97F4E1737410338456E192 (void);
// 0x000007E5 System.Void BNG.DestroyIfPlayMode::.ctor()
extern void DestroyIfPlayMode__ctor_m3194C7E4B7507A7D2797085A70C58FEB59F7FC58 (void);
// 0x000007E6 BNG.VRUtils BNG.VRUtils::get_Instance()
extern void VRUtils_get_Instance_m94182A01A486BC0F2AD143CF401F556BD13B434B (void);
// 0x000007E7 System.Void BNG.VRUtils::Awake()
extern void VRUtils_Awake_m7091F2326A1E05D337126F9114F4935DE10E3043 (void);
// 0x000007E8 System.Void BNG.VRUtils::Log(System.String)
extern void VRUtils_Log_mBD3DEBF0B0608FE7AF5B3AC4941B16E33707BDF3 (void);
// 0x000007E9 System.Void BNG.VRUtils::Warn(System.String)
extern void VRUtils_Warn_m938BDD4F95A011AB16F5B5F54227663FB6F26BAF (void);
// 0x000007EA System.Void BNG.VRUtils::Error(System.String)
extern void VRUtils_Error_mB76158A7F697C2F3978CD1F9F18B3E820FED24D1 (void);
// 0x000007EB System.Void BNG.VRUtils::VRDebugLog(System.String,UnityEngine.Color)
extern void VRUtils_VRDebugLog_m41A6D3A024A7AC597F515BF24C4D91DD648E9ADF (void);
// 0x000007EC System.Void BNG.VRUtils::CullDebugPanel()
extern void VRUtils_CullDebugPanel_m04032C0B80BDE82FC0BFFFAE5F1378756CD556CA (void);
// 0x000007ED UnityEngine.AudioSource BNG.VRUtils::PlaySpatialClipAt(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void VRUtils_PlaySpatialClipAt_mEDE911D70037C4CBBD46D1C35F33A5DFFA9A130F (void);
// 0x000007EE System.Single BNG.VRUtils::getRandomizedPitch(System.Single)
extern void VRUtils_getRandomizedPitch_m10060CDB4ACEF9DCDBE32353CA0F3D83F81FF521 (void);
// 0x000007EF System.Void BNG.VRUtils::.ctor()
extern void VRUtils__ctor_m3879D0E1EE4FBE789F93BE15EED97B7A9BF749EB (void);
// 0x000007F0 System.Void BNG.AmmoDispenser::Update()
extern void AmmoDispenser_Update_mB8B54D6326B23F8C99E280D4EBE2C42500FC508A (void);
// 0x000007F1 System.Boolean BNG.AmmoDispenser::grabberHasWeapon(BNG.Grabber)
extern void AmmoDispenser_grabberHasWeapon_mC6CF737CA1A8C85CF8E90E7B99E95EDD522DD851 (void);
// 0x000007F2 UnityEngine.GameObject BNG.AmmoDispenser::GetAmmo()
extern void AmmoDispenser_GetAmmo_m02D1985F661CA14031D598832742573C5ED3B312 (void);
// 0x000007F3 System.Void BNG.AmmoDispenser::GrabAmmo(BNG.Grabber)
extern void AmmoDispenser_GrabAmmo_m9B9DDF48AB202F224819475F5014D1CE8F3C0685 (void);
// 0x000007F4 System.Void BNG.AmmoDispenser::AddAmmo(System.String)
extern void AmmoDispenser_AddAmmo_mF21FB3ACC26A9DF1EAD3403B7696788EFDC108CF (void);
// 0x000007F5 System.Void BNG.AmmoDispenser::.ctor()
extern void AmmoDispenser__ctor_m4BBA84BB7D80A05C227E915C56199FD50CE333E5 (void);
// 0x000007F6 System.Void BNG.AmmoDisplay::OnGUI()
extern void AmmoDisplay_OnGUI_m5DF2897C61257501F96C18673BEEC6ABAC5B0483 (void);
// 0x000007F7 System.Void BNG.AmmoDisplay::.ctor()
extern void AmmoDisplay__ctor_mB8C3B93ABBA5B896803A893B34A4768EEEA83315 (void);
// 0x000007F8 System.Void BNG.Bullet::.ctor()
extern void Bullet__ctor_m8DB51B542362560AF62EFE06A8E95531EC9EC718 (void);
// 0x000007F9 System.Void BNG.BulletInsert::OnTriggerEnter(UnityEngine.Collider)
extern void BulletInsert_OnTriggerEnter_mDE079812D5BD35AB5DEA40E38DB8A518555774F1 (void);
// 0x000007FA System.Void BNG.BulletInsert::.ctor()
extern void BulletInsert__ctor_m7DFEDA31CE7D5C6B470F49B0378F44D0BA0071B7 (void);
// 0x000007FB System.Void BNG.MagazineSlide::Awake()
extern void MagazineSlide_Awake_m8D1C49511B2A62D35AE45D719764951C157E546F (void);
// 0x000007FC System.Void BNG.MagazineSlide::LateUpdate()
extern void MagazineSlide_LateUpdate_mF16905E6F0E2F1339096CF502A39E29B4D0B773D (void);
// 0x000007FD System.Boolean BNG.MagazineSlide::recentlyEjected()
extern void MagazineSlide_recentlyEjected_mB3DDBDB19430EC924D79C41877DC3A247C54802D (void);
// 0x000007FE System.Void BNG.MagazineSlide::moveMagazine(UnityEngine.Vector3)
extern void MagazineSlide_moveMagazine_mF5FC69D2F207FEDF01BD08EF1757501E0C109CEF (void);
// 0x000007FF System.Void BNG.MagazineSlide::CheckGrabClipInput()
extern void MagazineSlide_CheckGrabClipInput_m3AB6320E3F07F696D0D5793130187EE949C19433 (void);
// 0x00000800 System.Void BNG.MagazineSlide::attachMagazine()
extern void MagazineSlide_attachMagazine_m6936E6CE8DAA3775D9510D4E2941CEE791C75A13 (void);
// 0x00000801 BNG.Grabbable BNG.MagazineSlide::detachMagazine()
extern void MagazineSlide_detachMagazine_m5D065AFA55BF55CC2486BE2F8472712A4BF47E6C (void);
// 0x00000802 System.Void BNG.MagazineSlide::EjectMagazine()
extern void MagazineSlide_EjectMagazine_m08CC377A1E422738BE59A2A5C8C2F9D430BB526C (void);
// 0x00000803 System.Collections.IEnumerator BNG.MagazineSlide::EjectMagRoutine(BNG.Grabbable)
extern void MagazineSlide_EjectMagRoutine_m3EE0BD6CDC52A97688CCE2CA32D936FACB747749 (void);
// 0x00000804 System.Void BNG.MagazineSlide::OnGrabClipArea(BNG.Grabber)
extern void MagazineSlide_OnGrabClipArea_m2C9D5549571FF9046E692FDC97AD9A53C4733D98 (void);
// 0x00000805 System.Void BNG.MagazineSlide::AttachGrabbableMagazine(BNG.Grabbable,UnityEngine.Collider)
extern void MagazineSlide_AttachGrabbableMagazine_mA18A76D71F593359C6B3205D734D714236D79C37 (void);
// 0x00000806 System.Void BNG.MagazineSlide::OnTriggerEnter(UnityEngine.Collider)
extern void MagazineSlide_OnTriggerEnter_m50D0E5163406AF76261FD8CA1873239DC88CB6A6 (void);
// 0x00000807 System.Void BNG.MagazineSlide::.ctor()
extern void MagazineSlide__ctor_mE8C8A8A9C42D2C58330A03246F8B153611698F31 (void);
// 0x00000808 System.Void BNG.MagazineSlide/<EjectMagRoutine>d__23::.ctor(System.Int32)
extern void U3CEjectMagRoutineU3Ed__23__ctor_mC47FE0792B378CEB88C726EE89AE2149544C4158 (void);
// 0x00000809 System.Void BNG.MagazineSlide/<EjectMagRoutine>d__23::System.IDisposable.Dispose()
extern void U3CEjectMagRoutineU3Ed__23_System_IDisposable_Dispose_m762DE2D57D522FC69032965097B174B18F78B617 (void);
// 0x0000080A System.Boolean BNG.MagazineSlide/<EjectMagRoutine>d__23::MoveNext()
extern void U3CEjectMagRoutineU3Ed__23_MoveNext_m8ADC6C728DA66F5B81AFF2A48462C91ED433C15E (void);
// 0x0000080B System.Object BNG.MagazineSlide/<EjectMagRoutine>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEjectMagRoutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE577C7B45E07F18610ECF546B0FADEF8791813CB (void);
// 0x0000080C System.Void BNG.MagazineSlide/<EjectMagRoutine>d__23::System.Collections.IEnumerator.Reset()
extern void U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_Reset_m99AB62440CCCAA96008C830FC0CE747251E9B337 (void);
// 0x0000080D System.Object BNG.MagazineSlide/<EjectMagRoutine>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_get_Current_m509C9D094D357860F07778C459BD0D40B2FCF4B7 (void);
// 0x0000080E System.Void BNG.Projectile::OnCollisionEnter(UnityEngine.Collision)
extern void Projectile_OnCollisionEnter_m6C5549DA51BBC1F3DB8A6D709E53F17C2DDFACDC (void);
// 0x0000080F System.Void BNG.Projectile::OnCollisionEvent(UnityEngine.Collision)
extern void Projectile_OnCollisionEvent_mAAD1B38CBE92B1D902CD42B08FA0E647A751E380 (void);
// 0x00000810 System.Void BNG.Projectile::DoHitFX(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider)
extern void Projectile_DoHitFX_m341D4D73726CE78E401799026CD339F57FAE60DF (void);
// 0x00000811 System.Void BNG.Projectile::MarkAsRaycastBullet()
extern void Projectile_MarkAsRaycastBullet_m1CC9D4D021B2BDEF057B7D480FC9B60C6DC9CC0B (void);
// 0x00000812 System.Void BNG.Projectile::DoRayCastProjectile()
extern void Projectile_DoRayCastProjectile_m92F7D72DA06AAB705E28285A13B1AE8B91C56EFF (void);
// 0x00000813 System.Collections.IEnumerator BNG.Projectile::CheckForRaycast()
extern void Projectile_CheckForRaycast_m0CD37CDF0791960EE133837EBF0430C63F6D3654 (void);
// 0x00000814 System.Void BNG.Projectile::.ctor()
extern void Projectile__ctor_m36E68ADEB7FB7989C201F03EF1CDB3B7684C89B3 (void);
// 0x00000815 System.Void BNG.Projectile/<CheckForRaycast>d__13::.ctor(System.Int32)
extern void U3CCheckForRaycastU3Ed__13__ctor_mB1309B04CA1C1506370EE332A68E4D97B18155D3 (void);
// 0x00000816 System.Void BNG.Projectile/<CheckForRaycast>d__13::System.IDisposable.Dispose()
extern void U3CCheckForRaycastU3Ed__13_System_IDisposable_Dispose_m154AB76341BBF48AD0CC93DFDF25C4C45F382E08 (void);
// 0x00000817 System.Boolean BNG.Projectile/<CheckForRaycast>d__13::MoveNext()
extern void U3CCheckForRaycastU3Ed__13_MoveNext_mE65DB7217542E185FB37EBFD9ACCFC2E5CEAA2C0 (void);
// 0x00000818 System.Object BNG.Projectile/<CheckForRaycast>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckForRaycastU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m334BE549EC0FA1B97C64BF67D94DAACE56E9F9C7 (void);
// 0x00000819 System.Void BNG.Projectile/<CheckForRaycast>d__13::System.Collections.IEnumerator.Reset()
extern void U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_Reset_m64E96CDC9AE01E071A5B6CAC081F612C12FD67AC (void);
// 0x0000081A System.Object BNG.Projectile/<CheckForRaycast>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_get_Current_mEBE5BAF2030001B4C25C8CC2D6A2C05294FEBDF1 (void);
// 0x0000081B System.Void BNG.RaycastWeapon::Start()
extern void RaycastWeapon_Start_mF2986ED10374C7487BEF4D552E403843B29A026D (void);
// 0x0000081C System.Void BNG.RaycastWeapon::OnTrigger(System.Single)
extern void RaycastWeapon_OnTrigger_m21A4E5A7CA6E0376CAF07F68873685F7635841C4 (void);
// 0x0000081D System.Void BNG.RaycastWeapon::checkSlideInput()
extern void RaycastWeapon_checkSlideInput_m18DC0B579A0C7E9244235652C8F111B9EA2EEA8F (void);
// 0x0000081E System.Void BNG.RaycastWeapon::checkEjectInput()
extern void RaycastWeapon_checkEjectInput_mDE333C34BF70070A5EA41F8184BBFA6784BC75C5 (void);
// 0x0000081F System.Void BNG.RaycastWeapon::CheckReloadInput()
extern void RaycastWeapon_CheckReloadInput_mAAD52DC19DB7647113605D2F0911752D984DA49E (void);
// 0x00000820 System.Void BNG.RaycastWeapon::UnlockSlide()
extern void RaycastWeapon_UnlockSlide_m7FEF930CCDA6334E4D0C9D676D9859A7C009614F (void);
// 0x00000821 System.Void BNG.RaycastWeapon::EjectMagazine()
extern void RaycastWeapon_EjectMagazine_m7A52BD87B82059D0D72EC3B962F245D87C1337B7 (void);
// 0x00000822 System.Void BNG.RaycastWeapon::Shoot()
extern void RaycastWeapon_Shoot_m1924963A4CA2ECBBC3F7F044514CFC79ED167409 (void);
// 0x00000823 System.Void BNG.RaycastWeapon::ApplyRecoil()
extern void RaycastWeapon_ApplyRecoil_m7921462538DEC44081137C75AD24293E7DCD638B (void);
// 0x00000824 System.Void BNG.RaycastWeapon::OnRaycastHit(UnityEngine.RaycastHit)
extern void RaycastWeapon_OnRaycastHit_mA3F891D9AB1CA1F29920120BC77BAC4A722C0294 (void);
// 0x00000825 System.Void BNG.RaycastWeapon::ApplyParticleFX(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider)
extern void RaycastWeapon_ApplyParticleFX_m0EC7C9F96340A4DBB8F2EE0A564B1068AE31ED1A (void);
// 0x00000826 System.Void BNG.RaycastWeapon::OnAttachedAmmo()
extern void RaycastWeapon_OnAttachedAmmo_m232E0A50F49F971559A1696F5CC3DD860544BACF (void);
// 0x00000827 System.Void BNG.RaycastWeapon::OnDetachedAmmo()
extern void RaycastWeapon_OnDetachedAmmo_m95A4C81FA1D81B27A8613C8B40E3414EDEB33412 (void);
// 0x00000828 System.Int32 BNG.RaycastWeapon::GetBulletCount()
extern void RaycastWeapon_GetBulletCount_m7EC854B7ADE5E11F6A647F6A9BA3CFDA7345B0A8 (void);
// 0x00000829 System.Void BNG.RaycastWeapon::RemoveBullet()
extern void RaycastWeapon_RemoveBullet_m3F0C293A0B36D3D5000895D289C41216C4771417 (void);
// 0x0000082A System.Void BNG.RaycastWeapon::Reload()
extern void RaycastWeapon_Reload_m4E08783BD4326FA3AF5CA753A7674BF6915164EF (void);
// 0x0000082B System.Void BNG.RaycastWeapon::updateChamberedBullet()
extern void RaycastWeapon_updateChamberedBullet_mB35ADF14B8C8ADB654FD5650E0F9BAA7E8178EAB (void);
// 0x0000082C System.Void BNG.RaycastWeapon::chamberRound()
extern void RaycastWeapon_chamberRound_m0904098E756341305F10BBBB81B3E7ED9D2F6B1F (void);
// 0x0000082D System.Void BNG.RaycastWeapon::randomizeMuzzleFlashScaleRotation()
extern void RaycastWeapon_randomizeMuzzleFlashScaleRotation_mCAF0E4D6FD14E729B47F86786CE1B0F313EAA6FF (void);
// 0x0000082E System.Void BNG.RaycastWeapon::OnWeaponCharged(System.Boolean)
extern void RaycastWeapon_OnWeaponCharged_m020A5387212DB2CDE2A080B894685CB3A19FFF5E (void);
// 0x0000082F System.Void BNG.RaycastWeapon::ejectCasing()
extern void RaycastWeapon_ejectCasing_m352AE1FCA45E4164205F9A2CAA0100E6CA0742A2 (void);
// 0x00000830 System.Collections.IEnumerator BNG.RaycastWeapon::doMuzzleFlash()
extern void RaycastWeapon_doMuzzleFlash_mEDB43A57DACB83C55E97CCCDA9ADD9117CE0C8BB (void);
// 0x00000831 System.Collections.IEnumerator BNG.RaycastWeapon::animateSlideAndEject()
extern void RaycastWeapon_animateSlideAndEject_m19DE2E4031ED0AF85A3EE31E95A5902F0FF482F3 (void);
// 0x00000832 System.Void BNG.RaycastWeapon::.ctor()
extern void RaycastWeapon__ctor_mA45E21613E1FFF333F4A299107FCC670BC31FED1 (void);
// 0x00000833 System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::.ctor(System.Int32)
extern void U3CdoMuzzleFlashU3Ed__74__ctor_m262ED95928F86FE308BCA36503B1CF72BDEAA9B2 (void);
// 0x00000834 System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.IDisposable.Dispose()
extern void U3CdoMuzzleFlashU3Ed__74_System_IDisposable_Dispose_mF05C246FB314668C8D3D88CFB4CAD633245D5E6A (void);
// 0x00000835 System.Boolean BNG.RaycastWeapon/<doMuzzleFlash>d__74::MoveNext()
extern void U3CdoMuzzleFlashU3Ed__74_MoveNext_m8CE63ADF91C750A9B67FD036B7FBFB5B88707EEE (void);
// 0x00000836 System.Object BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdoMuzzleFlashU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC85468C57AC5E5B2FD0C5AB9153624FFD3397FF1 (void);
// 0x00000837 System.Void BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.IEnumerator.Reset()
extern void U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_Reset_mC91A6508DC6552A176052DC75E80915C65800083 (void);
// 0x00000838 System.Object BNG.RaycastWeapon/<doMuzzleFlash>d__74::System.Collections.IEnumerator.get_Current()
extern void U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_get_Current_m73BED4A42F6DB4BD0EC43421316E9DB09AC9A3C3 (void);
// 0x00000839 System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::.ctor(System.Int32)
extern void U3CanimateSlideAndEjectU3Ed__75__ctor_m52D4C4F0D32166633F5800ED0FA2F9102D9573FD (void);
// 0x0000083A System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.IDisposable.Dispose()
extern void U3CanimateSlideAndEjectU3Ed__75_System_IDisposable_Dispose_m73F851FA17249440950F54E0E3D12DCC41E3C881 (void);
// 0x0000083B System.Boolean BNG.RaycastWeapon/<animateSlideAndEject>d__75::MoveNext()
extern void U3CanimateSlideAndEjectU3Ed__75_MoveNext_mB2D1D8CBE242984B74CD60C88933D77C1252A0F7 (void);
// 0x0000083C System.Object BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CanimateSlideAndEjectU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B556AEA6646698B14C88F72971D1F0E22CB7506 (void);
// 0x0000083D System.Void BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.IEnumerator.Reset()
extern void U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_Reset_m042CF81A8EDB791453EADF7B62CEBBB35821BA90 (void);
// 0x0000083E System.Object BNG.RaycastWeapon/<animateSlideAndEject>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_get_Current_m62899C28A5BC0B43CF714B0E21DFDC21EF13D318 (void);
// 0x0000083F System.Void BNG.WeaponSlide::Start()
extern void WeaponSlide_Start_mC64514A27178319B231CF2DC78A2F4745C1B6848 (void);
// 0x00000840 System.Void BNG.WeaponSlide::OnEnable()
extern void WeaponSlide_OnEnable_mF2E8830A64E46555E5D33B4F0D26A28C41B7C6B5 (void);
// 0x00000841 System.Void BNG.WeaponSlide::OnDisable()
extern void WeaponSlide_OnDisable_m648C81D73CFADEA65B4C3934D21671A5C100A34C (void);
// 0x00000842 System.Void BNG.WeaponSlide::Update()
extern void WeaponSlide_Update_m374A94637F686151EECA902CF09F34B3F1646E87 (void);
// 0x00000843 System.Void BNG.WeaponSlide::FixedUpdate()
extern void WeaponSlide_FixedUpdate_mA417C3F95941F3137D7BEAAC4D7ED0FA59B00071 (void);
// 0x00000844 System.Void BNG.WeaponSlide::LockBack()
extern void WeaponSlide_LockBack_m9599590FBD04AB3210D29383C537D6BBF719673C (void);
// 0x00000845 System.Void BNG.WeaponSlide::UnlockBack()
extern void WeaponSlide_UnlockBack_m1752A520C33A66C8071658D02E9270490E8E41B6 (void);
// 0x00000846 System.Void BNG.WeaponSlide::onSlideBack()
extern void WeaponSlide_onSlideBack_mE014720A70E1C58447D3322E3680D2ADCB9115C4 (void);
// 0x00000847 System.Void BNG.WeaponSlide::onSlideForward()
extern void WeaponSlide_onSlideForward_m4D0BD2AF7091C4BC01A4325210DA5E977C3F2AAB (void);
// 0x00000848 System.Void BNG.WeaponSlide::LockSlidePosition()
extern void WeaponSlide_LockSlidePosition_m21C4B6EAE4D89B04CAA8AB8CCE296FD50CAD7E39 (void);
// 0x00000849 System.Void BNG.WeaponSlide::UnlockSlidePosition()
extern void WeaponSlide_UnlockSlidePosition_mCA73C09C492CC1C5C7ABA8172FE9EFD03D20A1E8 (void);
// 0x0000084A System.Collections.IEnumerator BNG.WeaponSlide::UnlockSlideRoutine()
extern void WeaponSlide_UnlockSlideRoutine_m91E0F77885CA06F79595EC40712EF1469F0F4427 (void);
// 0x0000084B System.Void BNG.WeaponSlide::playSoundInterval(System.Single,System.Single,System.Single)
extern void WeaponSlide_playSoundInterval_m941C21C22FA99E5C7972C6BDA65D973B810D9504 (void);
// 0x0000084C System.Void BNG.WeaponSlide::.ctor()
extern void WeaponSlide__ctor_mD9256125174C4D278F697F9FC6E1A11E7F8CD189 (void);
// 0x0000084D System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::.ctor(System.Int32)
extern void U3CUnlockSlideRoutineU3Ed__27__ctor_m2CBFD2C282CB8F21315E20BAD2F9AB8E9BC336E7 (void);
// 0x0000084E System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.IDisposable.Dispose()
extern void U3CUnlockSlideRoutineU3Ed__27_System_IDisposable_Dispose_m5257B510051CF3C319CADFB95624EB081D04AC7C (void);
// 0x0000084F System.Boolean BNG.WeaponSlide/<UnlockSlideRoutine>d__27::MoveNext()
extern void U3CUnlockSlideRoutineU3Ed__27_MoveNext_m1816811DF0963EEDFFBA6832607FB36A2CE09678 (void);
// 0x00000850 System.Object BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUnlockSlideRoutineU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28675E321BDCDA94330AAFB67629EC1178E1B882 (void);
// 0x00000851 System.Void BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.IEnumerator.Reset()
extern void U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_Reset_m080CAB29CBCEA62C742FB8CCDCD01008A73AAF38 (void);
// 0x00000852 System.Object BNG.WeaponSlide/<UnlockSlideRoutine>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_get_Current_m287CA3CCBB45A3B2F5E3F4F656C74EFBABA460AD (void);
// 0x00000853 System.Void DrunkMan.AutoRotate::Update()
extern void AutoRotate_Update_mB4CC663D513969C85FFA56286927A5F23A4E0B75 (void);
// 0x00000854 System.Void DrunkMan.AutoRotate::.ctor()
extern void AutoRotate__ctor_mB488C37BA5EC247F5F4E8BA1BE95F04F27B6F7AB (void);
// 0x00000855 System.Void DrunkMan.FreeCamera::Start()
extern void FreeCamera_Start_m884ACDAD9E4BFEFEAFEA38093E783F755B7CF56E (void);
// 0x00000856 System.Void DrunkMan.FreeCamera::Update()
extern void FreeCamera_Update_m45E25D344D55A52640F18034FFC27D7CE829D7B5 (void);
// 0x00000857 System.Void DrunkMan.FreeCamera::Move(UnityEngine.KeyCode,UnityEngine.Vector3&,UnityEngine.Vector3)
extern void FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88 (void);
// 0x00000858 System.Void DrunkMan.FreeCamera::.ctor()
extern void FreeCamera__ctor_m88DE606C2BE2621710E591509AD05962E68571AB (void);
// 0x00000859 System.Void DrunkMan.DrunkManFeature::Create()
extern void DrunkManFeature_Create_mE58D6E22C2001E91170E6EF35D37AD220D984D5A (void);
// 0x0000085A System.Void DrunkMan.DrunkManFeature::AddRenderPasses(UnityEngine.Rendering.Universal.ScriptableRenderer,UnityEngine.Rendering.Universal.RenderingData&)
extern void DrunkManFeature_AddRenderPasses_m62F67A9447ED874707E7E1852E3C4E4470F11F2E (void);
// 0x0000085B System.Void DrunkMan.DrunkManFeature::.ctor()
extern void DrunkManFeature__ctor_m54EFBB210A79E0A49EE8D6E84333A0AF5A42658A (void);
// 0x0000085C System.Void DrunkMan.DrunkManFeature/BlitPass::.ctor(System.String)
extern void BlitPass__ctor_m96534D9B89F7E34A0AADA29783F6F315D54F6F06 (void);
// 0x0000085D System.Void DrunkMan.DrunkManFeature/BlitPass::Setup(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material,System.Boolean,DrunkMan.DrunkManFeature/EType,System.Single)
extern void BlitPass_Setup_mBBA957EC6EE0D9F34629B37432FE068557DBCE9B (void);
// 0x0000085E System.Void DrunkMan.DrunkManFeature/BlitPass::Execute(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Rendering.Universal.RenderingData&)
extern void BlitPass_Execute_m95032E68BFEF0AF661662E86B18DDC9F069E33B3 (void);
// 0x0000085F System.Void DrunkMan.DrunkManFeature/BlitPass::FrameCleanup(UnityEngine.Rendering.CommandBuffer)
extern void BlitPass_FrameCleanup_m199FA46C75B328692D0070A4B846747D032F547C (void);
// 0x00000860 System.Void DrunkMan.DrunkManFeature/Settings::.ctor()
extern void Settings__ctor_mA5D76ECE115DEE0E6F4879C3772A4C79E274ECED (void);
// 0x00000861 System.Void DrunkMan.BuiltIn.Demo::Start()
extern void Demo_Start_mCA0735C8ED3F0D68DB44B56D443BADEEC881EDE2 (void);
// 0x00000862 System.Void DrunkMan.BuiltIn.Demo::Update()
extern void Demo_Update_m730C8AD00DCC914C56699CAEAE5ED493B5CE5FCD (void);
// 0x00000863 System.Void DrunkMan.BuiltIn.Demo::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Demo_OnRenderImage_mC8C1BEDC5055E92AD38455EE24150D20E46CDBD6 (void);
// 0x00000864 System.Void DrunkMan.BuiltIn.Demo::.ctor()
extern void Demo__ctor_mF247B44EB31DF71EA3ADB2E19A06917B1A1512C2 (void);
static Il2CppMethodPointer s_methodPointers[2148] = 
{
	CameraSettingFix_Start_mC20F9C0076E98D3409D20453516D3131EF51A1C0,
	CameraSettingFix_Update_m7A3616E042BD697FF33B78299D046930E795A728,
	CameraSettingFix__ctor_m3B04AFCB1646D580558F608DE0E3B1150E3A3C3F,
	DamageSystem_Start_mE92E95B82B6A1A3766CC08FA5A02BB526CA1D933,
	DamageSystem_Update_mC86FE409E436877828A465DBFB4B0EE2458C7059,
	DamageSystem_Died_mBC78BBBD505774B8B3F2BC3380648E7C89DAA51F,
	DamageSystem_ZombieHandDestroyed_mECB9F201A90AD49C3D37E38F69260826CD75EA62,
	DamageSystem_ZombieArmDestroyed_m30CE36CAA7AA9AC28FC3523C20A965E6623B2F43,
	DamageSystem__ctor_mC25F7D4C1BF8158098AE9611C4C0D15FB0A7466A,
	Drink_Start_m780C841889C98DAF0618FCDEF4030A4F7E129063,
	Drink_Update_mEB3A001CBAAB97ACA0609E1B1A67BD93C46A09DE,
	Drink_GetDrunk_m52D613B6181F0FAA01857356C449D66CBD11726B,
	Drink__ctor_m3A80DF38BA38D5706650D564B7D60496DB9A0720,
	MerchandiseManager_Start_m4C748531388613B6A252D72D5192CFDB53D9E0C3,
	MerchandiseManager_Update_m70E6438D6CCD10C7BE3D59D062A3F0F1568FA9AA,
	MerchandiseManager_OrderLightBeer_m85B2F56168FB43C2A6A993D91156BC58699E78FD,
	MerchandiseManager_OrderDarkBeer_mBE154564BCD1BD0E92AD6673C809E8BF4E5B6627,
	MerchandiseManager_OrderRedWine_m5A74D9D37E580C8391468273A35409D79FD42D5B,
	MerchandiseManager_OrderDessertWine_m42D85F5E942C64EC36BF6CD885987386D7757967,
	MerchandiseManager_OrderPistol_m5CD0EC0A2A3921B8A0AD85157B7EDBE47166B4B5,
	MerchandiseManager_OrderRifle_mA989587C20E5AA4C6642A51B53B4527AB0E7177A,
	MerchandiseManager_OrderShotgun_mA40E54F39B678811694B9358AC4AEFA82F8A4980,
	MerchandiseManager_OrderAutoRifle_mDB2E3C0204E574D1450E4BB15FA0026FD7A74A4E,
	MerchandiseManager_OrderPistolAmmo_mD11816C72E59CED181A077C8E408DCFE515D5479,
	MerchandiseManager_OrderRifleAmmo_mD6650E5088D300909CC3F80168F45B3761BB68AF,
	MerchandiseManager_OrderShotgunAmmo_mF0C129C97C4FC5A198BBA03FDFAF83112969211E,
	MerchandiseManager_OrderAutoRifleAmmo_m9AC893260AD5B40196770A6D57500322DD1C6FE6,
	MerchandiseManager__ctor_m7C20B7A581065B72808FA32B36CA041D17370836,
	PlayerHealth_Awake_m694ED0820668275A13034B552675DA946A50B9B6,
	PlayerHealth_Update_m5645A1624A67A8F9332B3DB67A4C2BAABF3DCD5A,
	PlayerHealth_Damage_m8C47E7E1C7E05412767C89110A0F4594A0CE9588,
	PlayerHealth_Heal_mD3007EA4440F16CA78A148830089AF07CFEAE9F0,
	PlayerHealth_Die_mB6EE0EAE72C9522BD349F87EB61AEDDFE7F0C44D,
	PlayerHealth__ctor_m6A07958FCBF285AA65AB66D48C3EB198068F37BE,
	rateRefresh_Start_m1A991442FF97EC78A3BB4FD84370FDE03E777C98,
	rateRefresh_Update_mB78885C08A68A43E1A85065B0AD80FE5428706BD,
	rateRefresh_RefreshFrameRate_m55B4CD49635057014734696F641695424E90D21F,
	rateRefresh__ctor_m044D158A12ABFAC6FE9E75661DEBCF13DC2722AF,
	U3CRefreshFrameRateU3Ed__3__ctor_m6BFEA59A53082AF0E44416F315C921971B1C2F37,
	U3CRefreshFrameRateU3Ed__3_System_IDisposable_Dispose_mC89D056F16A46DB9F4F0E96F8438D6C4D1C1F57C,
	U3CRefreshFrameRateU3Ed__3_MoveNext_m0715111D25C0A2CCA372E43510D443AAEFB48C1B,
	U3CRefreshFrameRateU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95122FB755946EAE5EFE02635F57A341276449B,
	U3CRefreshFrameRateU3Ed__3_System_Collections_IEnumerator_Reset_m34983B0C531DD9C3AD5A6FFD849A38803097E0F4,
	U3CRefreshFrameRateU3Ed__3_System_Collections_IEnumerator_get_Current_m27F7153420E1351D2BC5553E2A2AA47ABEF70E69,
	RaycastPaddingHelper__ctor_m7F99736727823DBAD45CAE3752F82149660451C9,
	SoberUp_Start_m6E24D684A5B86538FA8F2B914E85FADFC2F88D0A,
	SoberUp_Update_m580595E2EFCC84D569AE939FABF62B304CCF0B71,
	SoberUp_GetSober_mFDEB99943378C6D6FC6AF5380F47E769C275F126,
	SoberUp__ctor_m39CB212A0098C13896D7934946CD2680726026B9,
	SpawnManager_Start_m65C9EA67649948222CFF4FBBF77BE2319D813DAF,
	SpawnManager_Update_mD714BA3EADCC182FB7A93B9DE347D727E3A1BC53,
	SpawnManager_SpawnZombie_m2446C92EFF16E7B194941482D313077F2F6AD1DE,
	SpawnManager_ToggleGame_mDE3F11F210E0702BDAB79EEDCE0E6C5723B41DFB,
	SpawnManager_ZombieDie_m28DD8A967105A431DE692EF4FCF45158BCC8FA79,
	SpawnManager__ctor_m8DD503A0FFE79FA38CF0B7F013E54D24A04D166A,
	StatsManager_Start_m3DD113A1A278962CE9E455BDA7F1A1A470EAF61F,
	StatsManager_Update_m9A68EDC589B37447C635D102DCF09405A4AEC14A,
	StatsManager_EarnMoney_m6671D0FCBBEC1C4573C903B3A962C01FC07A4AB0,
	StatsManager_SpendMoney_m0F43759B158995CDBB23CF33911A5E6AED4F7740,
	StatsManager_KillZombie_m0F37952653461680946190BD73625E9B4C32A383,
	StatsManager_ResetStats_mF86A1AA707677E557D302606DBBF1FB4138D8BBB,
	StatsManager_SetABV_m839CA12BA14F066BDEB7EEB3ED381BC3186A64DF,
	StatsManager_GetABV_mEA7946CB824D7AE211EF9B8AD560C64AEFEFF8CD,
	StatsManager__ctor_mD0251ABB61521F5DE033975F0CB59C7E9F240564,
	TextFPSCounter_Start_mD48FD89899FFF6E0114499E47416C01BDCA1F9FB,
	TextFPSCounter_Update_mC01ED6AACF4266657E65ED6DE5D8F8A8FD776C13,
	TextFPSCounter__ctor_m7B261FE8D41B9C885E459962AF2884283453C388,
	idle_mocap_follow_player_Update_m555155208246194CF4F181C1239BF681A6F1DDF8,
	idle_mocap_follow_player__ctor_m33728B1BC494E53D794FF61D1DCE5CEF2BDBCC85,
	FixNonUniformScale_OnDrawGizmosSelected_mD29B982DC4CF3E56B4C1425D6F9962CE124AEE1C,
	FixNonUniformScale_MakeUniform_mBDE38273898560DAA90A8DC2CFB58048AA54D9F5,
	FixNonUniformScale__ctor_mA1ECA5647190B013D52C2E4AFDDCE9173D8DFA73,
	DamageAIByExplosion_Start_m676B4F9FD98A7F526EF16C90307EA1F5A47C06A4,
	DamageAIByExplosion_Explode_m7A08FF951561E246578CB21AE324A11394C0F0CA,
	DamageAIByExplosion_SpawnExplosionSound_m52521C5D9772E7D67272C7B6EFC5140BEB74DB3A,
	DamageAIByExplosion__ctor_m61277900686F59D0383C1D3098AC398EB1BC990C,
	DemoVisionFX_OnEnable_m202BA5155EA8D952022F4EE3BFA3A86FC20A1C37,
	DemoVisionFX_Update_m41A12E55E3FB611473EAA2573A719F13232D6A37,
	DemoVisionFX_OnGUI_m43B673A5C222FD566780393E51C25D8E4A537BE4,
	DemoVisionFX_DrawCustomAttributes_m369189C4A8DE28A5126CF7E5FD6AF85A6A60F886,
	DemoVisionFX_HorizontalSlider_mC6E2866BC60316129D6649A73AF0A8F0E257F46A,
	DemoVisionFX_HorizontalSlider_m37475E52CB6808E6FA95208419DDD892F1FB0F20,
	DemoVisionFX_HorizontalSlider_m9E658A021E97CEC6ED78E55AE0FC07D731EFEBE3,
	DemoVisionFX_ColorSlider_m093E50C267EDF651FF3012E7E4ABD1095DADD1AD,
	DemoVisionFX_MakeTex_m4D73A977C38516F324B9301E9C52DA570C93F9E7,
	DemoVisionFX_ChangeEffect_m26841A72045AC047013985E0480CADBB9D5732C0,
	DemoVisionFX_StarDelayCoroutine_m4E023D0E0E97D910CEB5736DBF149A006188A755,
	DemoVisionFX_ChangeEffectCoroutine_m545013A4CDF7D4F9A931C5DD35CE64506B166EE6,
	DemoVisionFX_EffectName_mE9387E6BCE76E44B7714611962DF63171C445D80,
	DemoVisionFX__ctor_mBB30AA019B7BC7B1E86EEDD03A516945CFF4FEBC,
	U3CStarDelayCoroutineU3Ed__37__ctor_mD9A49D668D1674E5C1F1C6B5846E13AE869E9DD3,
	U3CStarDelayCoroutineU3Ed__37_System_IDisposable_Dispose_m9577C27866FF94A549E234E7284A04035327F2DA,
	U3CStarDelayCoroutineU3Ed__37_MoveNext_m10DC61933276D8BAD540C7C3D41F5F18ECDC1448,
	U3CStarDelayCoroutineU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61C98399065F3BBDC9ABF1A8C89A479A1D9C00B1,
	U3CStarDelayCoroutineU3Ed__37_System_Collections_IEnumerator_Reset_m81491E24DB7541256303DBC631809771555E0BA4,
	U3CStarDelayCoroutineU3Ed__37_System_Collections_IEnumerator_get_Current_mF942584EA9B87D4D294D6D464928F95DF2701E90,
	U3CChangeEffectCoroutineU3Ed__38__ctor_m5D72F352E28860931FD618DC81CE2F261715410F,
	U3CChangeEffectCoroutineU3Ed__38_System_IDisposable_Dispose_mFFEBCC8B2F91FCEF661DD53A736C2909853506A7,
	U3CChangeEffectCoroutineU3Ed__38_MoveNext_m1445536ED3AD8588908C4501C04BDB89673ED77D,
	U3CChangeEffectCoroutineU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD33B0EED7303C11A669EB01DA79AA6BCBC5A714,
	U3CChangeEffectCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_m70852CB17099F795E077C6892EAE57A8A3527A0E,
	U3CChangeEffectCoroutineU3Ed__38_System_Collections_IEnumerator_get_Current_mF3B082D0285BC657CB8B09D8C9011B912F5C171D,
	SimpleFirstPersonController_add_OnStartMoving_mF4B4FEEE5A2E41FA1BE060AD85A323A2520A7844,
	SimpleFirstPersonController_remove_OnStartMoving_m3F255E89C90E7CAC67FDCF1635B531D801DC9D89,
	SimpleFirstPersonController_Footsteps_m50353376165FDB03457113D517B295BA5F90F63C,
	SimpleFirstPersonController_Awake_mD626E29CE91B41C73E18611A611F25F3605D439E,
	SimpleFirstPersonController_Update_m1D6DA8CA57099523BB78E89883264CABE5F334B4,
	SimpleFirstPersonController__ctor_m7B936AF21C67A108A3F4CFE63CF58BABEC28715B,
	StartMoving__ctor_mB127F7E935A5A96CBCDAFB28B90FFC70F79D6D27,
	StartMoving_Invoke_m5A084C820070E39B2C8B194C6EFB647F1A2EE52C,
	StartMoving_BeginInvoke_m725FDB6897AD3C81F0C8593614D8241D0DE36EC0,
	StartMoving_EndInvoke_m1806B2CB83CB0C81EE69288F0AB5F2091C81B113,
	U3CFootstepsU3Ed__22__ctor_m84B7716BA424E4483D50E631E72DA19E4324ABB2,
	U3CFootstepsU3Ed__22_System_IDisposable_Dispose_m8AC6C573ACCA30B5C24CCC3082CCB6730C6A2483,
	U3CFootstepsU3Ed__22_MoveNext_m77C0C036A9A9853F2558A87C240C6761944BECE8,
	U3CFootstepsU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36ABB485129AACDD1CCE6EC155030D592CECFB70,
	U3CFootstepsU3Ed__22_System_Collections_IEnumerator_Reset_m0A9DEECD934C4653E84EFD62956D0FF8970DC537,
	U3CFootstepsU3Ed__22_System_Collections_IEnumerator_get_Current_m4122A5D4AF8575305CD0462FB5587F90DA51B285,
	CombatTextSystem_Awake_mA056C2F3F39A60DDA50DE1E9DC2D99C2001E2D8A,
	CombatTextSystem_Initialize_m91656A5411AD8241C831E0DBEBC3A6B58F0CAFA0,
	CombatTextSystem_CreateCombatText_m3AB738EC5241DE33F5874AEBB33C020C777C7AE0,
	CombatTextSystem_CreateCombatTextAI_m12DC6C97BA19E9FEDD47B72FB382F9A615D11A6D,
	CombatTextSystem_AnimateBounceText_m12DC07A40F918EA9B16B26818FD364B0851C93B8,
	CombatTextSystem_AnimateUpwardsText_m1067CE954306F070CF038A7ECA034C56E2DE6443,
	CombatTextSystem_AnimateOutwardsText_mA6F589AAEC150DD6F5929523936531D9F0ED9E4F,
	CombatTextSystem_AnimateStationaryText_mB793B304EEFA64E626EAA595F2E0F7FD254EA65E,
	CombatTextSystem__ctor_mF2DCFE4E48AB43760EC8F72A18CCD5FBD64C757E,
	U3CAnimateBounceTextU3Ed__11__ctor_m774BA79859F86B4D3CD0C146F9ED281D94A7844F,
	U3CAnimateBounceTextU3Ed__11_System_IDisposable_Dispose_m2A0B8B6F18229F8CEFCF4AC9CDE586A463F5C1B4,
	U3CAnimateBounceTextU3Ed__11_MoveNext_m9727187E1FC4209661C49502783D922DA95A08F7,
	U3CAnimateBounceTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m555E6986491A7E0023957F576B4C34D81731098F,
	U3CAnimateBounceTextU3Ed__11_System_Collections_IEnumerator_Reset_mFFFE6A69D12A0C2C841577BF716DDD8344897F12,
	U3CAnimateBounceTextU3Ed__11_System_Collections_IEnumerator_get_Current_m666485FFA10569ED13D8F285EAAE0814DE144598,
	U3CAnimateUpwardsTextU3Ed__12__ctor_m2F07B6DCA11150CDEBB897B9B6D2B23ADDB5FD0F,
	U3CAnimateUpwardsTextU3Ed__12_System_IDisposable_Dispose_m15DC41DAA5C0E976E41320206E41B36BB23371FA,
	U3CAnimateUpwardsTextU3Ed__12_MoveNext_m96E92C7148EEAB92A4DA4C6EAD76D98653E1A3D0,
	U3CAnimateUpwardsTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE51A357754807E733BD1162616584EF94560D261,
	U3CAnimateUpwardsTextU3Ed__12_System_Collections_IEnumerator_Reset_m3FEAE8B5746BA901CC49221B5C3D1B55700B5EC7,
	U3CAnimateUpwardsTextU3Ed__12_System_Collections_IEnumerator_get_Current_mB5510E9288777C46505B7D08C120DDCD51410760,
	U3CAnimateOutwardsTextU3Ed__13__ctor_m32D9E13011D13A517956E5343B148ECB8C3A8BC1,
	U3CAnimateOutwardsTextU3Ed__13_System_IDisposable_Dispose_mAA1D18F6927C7418C43A235966C8A00DD6D8CBF3,
	U3CAnimateOutwardsTextU3Ed__13_MoveNext_m9EA59D4137D404747518188C33A69489FBC5C159,
	U3CAnimateOutwardsTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EE8D3F1E11CA19E1550DE025F5D5EE8CA2E2898,
	U3CAnimateOutwardsTextU3Ed__13_System_Collections_IEnumerator_Reset_mCFE69A34A072AC5F10DD943604D0CBC43EFE17E1,
	U3CAnimateOutwardsTextU3Ed__13_System_Collections_IEnumerator_get_Current_mBD6EB1815B0DCF661AAC698F8C4D95EBFF543190,
	U3CAnimateStationaryTextU3Ed__14__ctor_m38616AC4D420F6DC6EFFFACECF472ADE084D2B9A,
	U3CAnimateStationaryTextU3Ed__14_System_IDisposable_Dispose_m307AD6022E61DFDE1AC2A263B68DE62416965E8E,
	U3CAnimateStationaryTextU3Ed__14_MoveNext_m0D4A2382CAD00B16F14D5B30F2CB2773B89EFA65,
	U3CAnimateStationaryTextU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B92791CE1F3958F999A44EB988F38D7C7AE92BE,
	U3CAnimateStationaryTextU3Ed__14_System_Collections_IEnumerator_Reset_m61C21D94108BCB00F2E9FDA1A552D0921B72B2D5,
	U3CAnimateStationaryTextU3Ed__14_System_Collections_IEnumerator_get_Current_mB41CDE7E7F0FB0CFEDB2811B0E2C43FBC6AF04F0,
	EmeraldAIAbility__ctor_mF33E0D1438F4C87481AF79C77D5D1216E8D4F004,
	EmeraldAIBehaviors_Start_m8B32DF5A3D43B089868CC7A973CD4C261A6651F9,
	EmeraldAIBehaviors_AggressiveBehavior_m5DDCFD61A955F4CC835BD0E2940643D03B7A2B5B,
	EmeraldAIBehaviors_CheckForTargetDeath_m664819F0C78F6A5C9A45F27C956073F79ED615C1,
	EmeraldAIBehaviors_DeathReset_m38785CD878E5AEAAFFAF1FE8395702D3F56768B9,
	EmeraldAIBehaviors_CompanionBehavior_mDB41869E69CFD6765CB9F353D02112D07A58C6C4,
	EmeraldAIBehaviors_CowardBehavior_mB689D61F6221DAA0C064E13D7FEBAA3F80A1E43D,
	EmeraldAIBehaviors_CautiousBehavior_m4CE224C98010502DEEF994C63A8740E997316DA5,
	EmeraldAIBehaviors_DeadState_m062BD32E943F4F022A199457199A361661CF9162,
	EmeraldAIBehaviors_FleeState_m4FC6DE05A898F07675B002767581FA4D283D8BB4,
	EmeraldAIBehaviors_CheckAnimationStates_m35B6C55ACE320021197141B84CBF66FCAB39DCCB,
	EmeraldAIBehaviors_DefaultState_m48B28B99B3CCFDFB8C9971DF7C0CFDFA44279070,
	EmeraldAIBehaviors_TransitionIK_m7CEC3DAF85FBF535EBEAE50B1093891CC7D686B5,
	EmeraldAIBehaviors_DelayEquipAnimations_mC07369172B62213C0FE5CFB8BE9B72EF518A6BE4,
	EmeraldAIBehaviors_DelaySearch_mA91BE9D4E9C780E9965B39A46799221B028DEF43,
	EmeraldAIBehaviors_ReturnToStartComplete_m6581E603253BF8736A8CB196C148C32B9A671D4A,
	EmeraldAIBehaviors_RefillHeathOverTime_m3339F3251B6DAC14E7B475B80CF9217FD436E31E,
	EmeraldAIBehaviors_DelayReturnToDestination_mEC7825D55D658132A2F7AED474844DDACBCB83C1,
	EmeraldAIBehaviors_ActivateCombatState_m2892705D803491AC3D1324AA856583CCABB3B4D7,
	EmeraldAIBehaviors_AttackState_mB5DDF71D29B3C83977362DD8118AB3097EEDD51E,
	EmeraldAIBehaviors_ResetAttackGeneration_m65802B58C8515FE8FFE06400ABF44ACF63F29346,
	EmeraldAIBehaviors_BlockState_m78C2398EB9D4D06132CBD4727A2E3FAE485F3504,
	EmeraldAIBehaviors_ActivateBlockCooldown_m859A5E2C44D7B30B98B6466AD677082E08BA7F44,
	EmeraldAIBehaviors_BackupState_m0F15793142F3232B07F79A1DA0C09C308A3A74A1,
	EmeraldAIBehaviors_BackupDelay_m8293D9B933BE738315E528CAAA2922EB0E603030,
	EmeraldAIBehaviors_StopBackingUp_m803261C36EE176ABBC450F12B2135BD72FA29304,
	EmeraldAIBehaviors_CalculateBackupState_mA6B1C22CC659253963186DE9435E3B02717C2F78,
	EmeraldAIBehaviors__ctor_mDD4255615CA687A7B1519B21CA1F013924BF2792,
	EmeraldAIBehaviors_U3CDelayEquipAnimationsU3Eb__20_0_m2D0580FA86EE405FE65877E445F21EC4206A5AC8,
	EmeraldAIBehaviors_U3CReturnToStartCompleteU3Eb__22_0_m6CFAF1250E9641ECC90D10E41ADA5D0F48D6C8B1,
	EmeraldAIBehaviors_U3CDelayReturnToDestinationU3Eb__24_0_m2E13073FFF5E047880D7A5F896986EC113B41D86,
	U3CDelayEquipAnimationsU3Ed__20__ctor_mC28F7B9A0932814CA8067735C44FA22B7C9329E0,
	U3CDelayEquipAnimationsU3Ed__20_System_IDisposable_Dispose_mADB3B4F59F1F08139264A1F746518957D507D214,
	U3CDelayEquipAnimationsU3Ed__20_MoveNext_m63DF577CF141F1FAAEC056A97827A9A2BBC0F0F4,
	U3CDelayEquipAnimationsU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABF3F18C6DB36C3DF4685A2CE2ECC2B912F9D7C2,
	U3CDelayEquipAnimationsU3Ed__20_System_Collections_IEnumerator_Reset_mB783909125A81C8270D311F1D34F5A3EBB135C79,
	U3CDelayEquipAnimationsU3Ed__20_System_Collections_IEnumerator_get_Current_m379CDD31E2F4C677D9140A4D6AB989F289CD2050,
	U3CReturnToStartCompleteU3Ed__22__ctor_mCBD78CE7279158D92DF62D02ACB56E5F6FEC9545,
	U3CReturnToStartCompleteU3Ed__22_System_IDisposable_Dispose_m773ACEA685C87F6AE5F33BE84876E7FD56569F8A,
	U3CReturnToStartCompleteU3Ed__22_MoveNext_m8F6EFF4EF3497B9D7287D88A6343389B788048F5,
	U3CReturnToStartCompleteU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8BBFAFEF25ACD59187759170F0FC043BB2EBDE0,
	U3CReturnToStartCompleteU3Ed__22_System_Collections_IEnumerator_Reset_mAFF1F5A6A2A7E33BB20C0271AAA40A226B0FAB97,
	U3CReturnToStartCompleteU3Ed__22_System_Collections_IEnumerator_get_Current_m613A88DE4C81417931AD51CB11E9CBC7F545B211,
	U3CRefillHeathOverTimeU3Ed__23__ctor_mF7AACF292E855BE82BCD5D3BC6D2DE45F0CBD944,
	U3CRefillHeathOverTimeU3Ed__23_System_IDisposable_Dispose_m6DE30C83C3B577C316307E8FF810F51164A00B10,
	U3CRefillHeathOverTimeU3Ed__23_MoveNext_m34F6BC72752F56E06279F6A9F53165ABC50E240D,
	U3CRefillHeathOverTimeU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC5CA0AF7C6D7A8383D424D028EBA6543C9691D2,
	U3CRefillHeathOverTimeU3Ed__23_System_Collections_IEnumerator_Reset_m8B2EDED2F2588D715046E33E6A6DADD12FC774EA,
	U3CRefillHeathOverTimeU3Ed__23_System_Collections_IEnumerator_get_Current_mA072318E5B58EB87E77579E4C065308B5459A341,
	U3CDelayReturnToDestinationU3Ed__24__ctor_mA731A0F0E2B51D70B20698A336F663A9DF1EA914,
	U3CDelayReturnToDestinationU3Ed__24_System_IDisposable_Dispose_m961588FCFE99030517001764E2F0D2F6EAF71DAC,
	U3CDelayReturnToDestinationU3Ed__24_MoveNext_m431106726FA63DF16E527F62A2E21A3573047544,
	U3CDelayReturnToDestinationU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB453FF707E50EA919CCECF9B0FD2A8B003A7FE9,
	U3CDelayReturnToDestinationU3Ed__24_System_Collections_IEnumerator_Reset_mDC6B63C00D570C96E7B90E7EEF967EB016562BDD,
	U3CDelayReturnToDestinationU3Ed__24_System_Collections_IEnumerator_get_Current_m6E38B155A688B382A9DC4928D6FA90582783DCC1,
	EmeraldAICombatTextData__ctor_m9D5AF700019E873B6C6CED9CBC02D665FCA9D153,
	EmeraldAIEventsManager_Awake_m86247D8999FCCAB8FBCF401E1D37E8010575EFEB,
	EmeraldAIEventsManager_PlaySoundClip_m7B85028D1E8E5DAC76594F9CB2503B4C49E7951D,
	EmeraldAIEventsManager_PlayIdleSound_m427D11FCC5EE576517FB6CC17EF0D84507E0A79C,
	EmeraldAIEventsManager_PlayAttackSound_m78317319CDE6B1C99B7A1976794ECBA23BEB09C0,
	EmeraldAIEventsManager_PlayWarningSound_m21F9264BC9AE517F20FFB0D14121F3033EA6E21F,
	EmeraldAIEventsManager_PlayImpactSound_mC5809D0B592632F118AC79B82A6E3633AF68020C,
	EmeraldAIEventsManager_PlayCriticalHitSound_mA9F1DF9095F4E83C587137ED05513C9FB7F4A64F,
	EmeraldAIEventsManager_PlayBlockSound_m2C3AED6CD4F21748585037993A5804AA1071D4A2,
	EmeraldAIEventsManager_PlayInjuredSound_mA068DF9A68F7F6D7E7E45AA268A2931A33116AB5,
	EmeraldAIEventsManager_PlayDeathSound_m3FED169B0D07C2D8694268FCDB2230E37073B0B1,
	EmeraldAIEventsManager_WalkFootstepSound_mE3D74DDEF3C21E2A33B7D38BECEE481B95E14BBE,
	EmeraldAIEventsManager_RunFootstepSound_m9DDE618D9C449229AB50546027151C1E87ABA2BF,
	EmeraldAIEventsManager_PlayRandomSoundEffect_m6F27BCCD47E9E569A911826502ABBA95E6002860,
	EmeraldAIEventsManager_PlaySoundEffect_m3774F2EC2AA22B5E238DFAC8326C87FD4FFBB45B,
	EmeraldAIEventsManager_EnableItem_m756A8A6B617A4A5D5A422E0E4300D4BC0F79BA5E,
	EmeraldAIEventsManager_DisableItem_m4EC753151D2FF2C105F900A810A89EEA4871AE18,
	EmeraldAIEventsManager_DisableAllItems_m2928471527E6C91C08515A2C621770605C6A7FFE,
	EmeraldAIEventsManager_EquipWeapon_m56E545884430522B1A2BA2995BC3EAA6A7139406,
	EmeraldAIEventsManager_UnequipWeapon_m25B683C08DC00890EA9D6966402EB902ED119534,
	EmeraldAIEventsManager_CancelAttackAnimation_m52483F86B3AC8CF79C256CA4513D09A6069F0641,
	EmeraldAIEventsManager_PlayEmoteAnimation_mAE9EF0BA5F32BDD79DCF8F896C2B10FB3F750D8C,
	EmeraldAIEventsManager_LoopEmoteAnimation_mB8693D0A6293EF4EFA323F9C44E9398D74744F1A,
	EmeraldAIEventsManager_StopLoopEmoteAnimation_mF1E65CE5EB4647FC0466EA6C49EAEB25427F6BE7,
	EmeraldAIEventsManager_SpawnAdditionalEffect_mD181A8FE21B8EB7E3437800AD6AB66BDF5732833,
	EmeraldAIEventsManager_SpawnEffectOnTarget_m7B263068D49445DAAA5C5AE6566E8FCB318B3BFB,
	EmeraldAIEventsManager_SpawnBloodSplatEffect_mBE5B767EC330CCABF41F541B6B507679EEF736CA,
	EmeraldAIEventsManager_KillAI_mB23CD78E5D436AD31C6B4004D52D7607FAB844F8,
	EmeraldAIEventsManager_OverrideIdleAnimation_m5C0DC0B58806AD6FB032B049FCDB642110D4F67A,
	EmeraldAIEventsManager_DisableOverrideIdleAnimation_mF7AC9AF8A9CD8C73285B65B96B700755856795B4,
	EmeraldAIEventsManager_ChangeBehavior_m84795F14D5C26885C699EF9E808D3298038FDE12,
	EmeraldAIEventsManager_ChangeConfidence_m59C67437A9C818BAFE590853CC1E8524EF3AD314,
	EmeraldAIEventsManager_ChangeWanderType_m63C8ADBDCEC905C72E310D5FDEC6CB6AF1C6C64D,
	EmeraldAIEventsManager_CreateDroppableWeapon_m8D55033132AA00FDBA6BEE4E85A34BC8DE0B5EE2,
	EmeraldAIEventsManager_ClearTarget_mA76C9CBEB93218FA1A25E61BA67FB3C5C7B1E754,
	EmeraldAIEventsManager_ClearAllIgnoredTargets_m7B9926C565672DD3880256DB31C99162991DFA38,
	EmeraldAIEventsManager_SetIgnoredTarget_m05E874DB8E6A9D9FBB7D5F62A4D5B39E0F86E394,
	EmeraldAIEventsManager_ClearIgnoredTarget_m4F673E3A6E9F6D8490A18EEE7C2EC9711D5BABA5,
	EmeraldAIEventsManager_ReturnToStart_mB1A5D6F37F2CC0562CB240A3A83618DF7897B245,
	EmeraldAIEventsManager_DelayTargetReset_mDFFE09D09985396A60D95AC7693D11739C001B40,
	EmeraldAIEventsManager_GetDistanceFromTarget_m5E260BB8162380E9195D0082D023E7F5CCF462C0,
	EmeraldAIEventsManager_GetCombatTarget_m6B2D38B6E1DBAC00B64DCD233612728A7424603F,
	EmeraldAIEventsManager_SetCombatTarget_mF3AD01033C4D8C64164AB703F23A3A484BDC6D77,
	EmeraldAIEventsManager_OverrideCombatTarget_m4CBC8FDC9AD3022F91FA6320D01CC699CF27DBB5,
	EmeraldAIEventsManager_FleeFromTarget_m51918897B76DACB5FB330163D372341A27958B21,
	EmeraldAIEventsManager_SetFollowerTarget_mF09475C63A6592FE45EF00ABF8ADB16F30CA46A3,
	EmeraldAIEventsManager_TameAI_mEC09149A9834F8430BCE5C1CA21EAAC42CF24DE6,
	EmeraldAIEventsManager_GetAttacker_m8C215C39C2B4555CD22CA55DBA320693917300C1,
	EmeraldAIEventsManager_ChangeDetectionType_mE9047D9E5151A3598156BCEE264FA4724659854D,
	EmeraldAIEventsManager_UpdateUIHealthBarColor_m1242F8636A5188BAE63716AA14F8F99EB9A049EB,
	EmeraldAIEventsManager_UpdateUIHealthBarBackgroundColor_m4F4D14B8C40C0403E0E2C34D733A482900B4A2DF,
	EmeraldAIEventsManager_UpdateUINameColor_m69225FA94BA69A93BEBCEA7AFCDB73C544A91FDB,
	EmeraldAIEventsManager_UpdateUINameText_mAC618D101EBD21729BEFE79DD6A73E505801F51D,
	EmeraldAIEventsManager_UpdateHealth_m3345BDC1BB74C7CEBA43BE6612CF19198BA7BB0F,
	EmeraldAIEventsManager_UpdateDynamicWanderPosition_m2B96552D5844BAD1226B6E162ED8A73365C4C22B,
	EmeraldAIEventsManager_SetDynamicWanderPosition_m475ACFCAC27831FA004F59C7EB986680CE41BBCD,
	EmeraldAIEventsManager_UpdateStartingPosition_m3D71E887A5E79F4E9178984C3246DF5D31A079B5,
	EmeraldAIEventsManager_SetDestination_mCD9B689C6FCF34ABA09AE7F54EDA807F9C5B0D80,
	EmeraldAIEventsManager_SetDestinationPosition_m65E3987FEF6495A9CDFE1440D359C5155FF6F118,
	EmeraldAIEventsManager_AddWaypoint_mF6355664E4C40E4366DFB8B49D13EA987201C2F0,
	EmeraldAIEventsManager_RemoveWaypoint_m142C0B5AEA0109915C013BB88641411C9BFCF912,
	EmeraldAIEventsManager_ClearAllWaypoints_m60A2ADD5FC7DC66286D989142EE9DE235F77541D,
	EmeraldAIEventsManager_InstantlyRefillAIHealth_m49AE4C668EADDF5206A7F4F1EACB383166F51BED,
	EmeraldAIEventsManager_StopMovement_m0CB67B845482AE89F77D86FE7B9B3D071C40BCB3,
	EmeraldAIEventsManager_ResumeMovement_m19355C358E099909F5416A838C271A7C8734816A,
	EmeraldAIEventsManager_StopFollowing_m5C9FBFCD199E447A07742EE4A17B9B7DB1DC0658,
	EmeraldAIEventsManager_ResumeFollowing_m5F7835725A32B26B3CABD55145416E46CA1E2974,
	EmeraldAIEventsManager_CompanionGuardPosition_mE328AB3384C8D9C3B6322195B3B19F6BF57CA07A,
	EmeraldAIEventsManager_SearchForClosestTarget_mB43011770D083070EA70E7FCBAD5F932000BCF09,
	EmeraldAIEventsManager_SearchForRandomTarget_m410CC79342F8B1868B4004C5DF112C0A62891159,
	EmeraldAIEventsManager_UpdateAIMeleeDamage_m70AAED957438A5DD58E125D4B50FEE8B92AFBFD2,
	EmeraldAIEventsManager_UpdateAIMeleeAttackSpeed_m9E3B2C659A8E50725E575D518C2C9E3C5FD7CF3A,
	EmeraldAIEventsManager_UpdateAIRangedAttackSpeed_m0FA1C185ED0D860419D984F22CE5258E78DCD985,
	EmeraldAIEventsManager_RotateAITowardsTarget_m69028ABA7E850FDD04803DB8CAD57B7975D3757E,
	EmeraldAIEventsManager_CancelRotateAITowardsTarget_mB0427BDCE6C83E8BE99DAEE0C8EB92A3324781B6,
	EmeraldAIEventsManager_RotateAIAwayFromTarget_m147A0774AB353348CF0E27555EB8F90F00347849,
	EmeraldAIEventsManager_CancelRotateAIAwayFromTarget_m71B26B10AB85F013CE4C6C725B24173C0156BE18,
	EmeraldAIEventsManager_RotateAITowardsPosition_m65775B788D6B40B62404B4EBBEECFD1483CB8E32,
	EmeraldAIEventsManager_CancelRotateAITowardsPosition_m0DD37540500CA6D3E2C37510AAF12F2F3D0156B8,
	EmeraldAIEventsManager_RotateAIAwayFromPosition_m73501DA6647D09C85550071D93E4F0B076555BCF,
	EmeraldAIEventsManager_CancelRotateAIAwayFromPosition_mEC05AE9E6CBB729E80E37FC3A9354FD36B210884,
	EmeraldAIEventsManager_RotateTowards_mEDF8F201EF306ECF05FC674D04A7B648F8C29E7F,
	EmeraldAIEventsManager_SetFactionLevel_m32AA9F7FBFBBBEDBBE4C8B95D603278A2C4A1825,
	EmeraldAIEventsManager_AddFactionRelation_m1B9B0CBA9378806A973CB70FB9743801201AD510,
	EmeraldAIEventsManager_GetAIRelation_m30E821DEAD0A7C9204D4DDC4FBFDBED42AA49C6C,
	EmeraldAIEventsManager_GetPlayerRelation_mC3D0AEA72B2200312340DED9713E741C228A7CE9,
	EmeraldAIEventsManager_SetPlayerRelation_m11DF2C5E290A4C499992BA35CC02A0EA4BD0E1C8,
	EmeraldAIEventsManager_ChangeFaction_mADCB7005EBF13E9E2771E91B7621FF66685E5BC8,
	EmeraldAIEventsManager_CheckForPlayerDetection_m3766237587DD89A753B53783FA1DA9F518CB0947,
	EmeraldAIEventsManager_GetFaction_mD403D17CD4045A10F8539022F6428B9978A8521C,
	EmeraldAIEventsManager_ChangeOffsensiveAbilityObject_m8354A63DF46427AC77A878F443D00D31DD05FFCD,
	EmeraldAIEventsManager_ChangeSupportAbilityObject_m51699FCD4D5DB5D60EC7858B5FEFF57C68241614,
	EmeraldAIEventsManager_ChangeSummoningAbilityObject_m0306386D2FCDB6D15F351DF5A4DBAFA918D4CF3B,
	EmeraldAIEventsManager_ReturnToDefaultState_m9A75250B89682EE5598673B62362200DEC54FD90,
	EmeraldAIEventsManager_DebugLogMessage_mB50FD2000B2FFDDEAB79FB84B9C2E76677FCF4A7,
	EmeraldAIEventsManager_EnableObject_m073A78A9BC02A445E6D429548B3597D76CB4E4E5,
	EmeraldAIEventsManager_DisableObject_mE8CEFD888716D23DA3C6E2A17E192CD2FF8530BD,
	EmeraldAIEventsManager_ResetAI_mC11E557AF8ECF144DDAE4C5BE9A92677F52C780A,
	EmeraldAIEventsManager_HealOverTimeAbility_m07B7D4489FD90C63181E8127B309A05ECF844DBA,
	EmeraldAIEventsManager_HealOverTimeCoroutine_m196DB237B4A9E292AFB6BA5ACAC100439C319EE8,
	EmeraldAIEventsManager__ctor_m181C3FDE2ACCBCFE53C66ADB436F663D93F899AB,
	U3CRotateTowardsU3Ed__82__ctor_mB8363BE9CEAFA35F7E9D48B5F134D1C2AB811AC8,
	U3CRotateTowardsU3Ed__82_System_IDisposable_Dispose_m83400FFA02C47A3B2009ADFDCBDE28BC6E024A28,
	U3CRotateTowardsU3Ed__82_MoveNext_mA9007CC5400608FD7468189F7C9778978F13684A,
	U3CRotateTowardsU3Ed__82_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D92741BEB6563498FEA1C6FB68C5088F85D974,
	U3CRotateTowardsU3Ed__82_System_Collections_IEnumerator_Reset_mE09B4461BB700C19A39E4F823DEA3D296764CF28,
	U3CRotateTowardsU3Ed__82_System_Collections_IEnumerator_get_Current_mDA5816D2D0C0E13C44476B787F4C8E29596F1A6D,
	U3CHealOverTimeCoroutineU3Ed__100__ctor_m0447B4035A88E2621F9012ABD48468E8B0F2ED01,
	U3CHealOverTimeCoroutineU3Ed__100_System_IDisposable_Dispose_mAB3E040CF0DEDFC3225E821E850848AE3FE4EAA8,
	U3CHealOverTimeCoroutineU3Ed__100_MoveNext_m0EC92384110CB5084FB566A27655CD0D707FC8B6,
	U3CHealOverTimeCoroutineU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65D7B155EC73E69155DCEAE837E3563BDC9A8D80,
	U3CHealOverTimeCoroutineU3Ed__100_System_Collections_IEnumerator_Reset_mB7EE9E4C800DE4F09773EA5B589DE99CB8A6DB41,
	U3CHealOverTimeCoroutineU3Ed__100_System_Collections_IEnumerator_get_Current_m99B141F7F3F8B826DB8CDCFE3F5BAD53D0E6AB9F,
	EmeraldAIFactionData__ctor_mCB3FD267EB15776070339A6EC5A8B144B27EEA6E,
	EmeraldAINeedsSystem_Start_m84AAC58D673860CF625BD2D02E1299668D4A791F,
	EmeraldAINeedsSystem_UpdateWaypoints_mC107395B67276F5D42AC05D019DEE021814F5183,
	EmeraldAINeedsSystem_UpdateNeeds_mD9C2EFD4E744397F31968E3ED97F71FADA982C72,
	EmeraldAINeedsSystem_Update_mE9C16C4FDCB50F2167A7E48D72FB0C597CABBF54,
	EmeraldAINeedsSystem__ctor_m5424C12C5535817FE42D5F82704F668B7B420282,
	EmeraldAINonAIDamage_Start_mBAF0635B20866E81CFBAFAC37C0A5102347AAFCC,
	EmeraldAINonAIDamage_SendNonAIDamage_m65611DBD1CF751BB873C9BDBAB3826E443586204,
	EmeraldAINonAIDamage_DefaultDamage_mBA2F86897F4C7B41B4C19336D350F0793B7E27E7,
	EmeraldAINonAIDamage_ResetNonAITarget_mE0A4BB61BF4A610A8389BD962EDEB2E3F2970831,
	EmeraldAINonAIDamage__ctor_m383EC7C8571779669967488506A26CADF677C8A8,
	EmeraldAIPlayerDamage_SendPlayerDamage_mA3A71D6DD02B3A628BCC32A8AFB52B04A2725F0A,
	EmeraldAIPlayerDamage_DamagePlayerStandard_m4B3192C143C708F366F074C0A63DD4507EE4E612,
	EmeraldAIPlayerDamage__ctor_m9F84562342A9F354A208CBAFBB08E0583ACD3475,
	LocationBasedDamage_InitializeLocationBasedDamage_m05DC961850FF639319964A2992C7ED98825A159D,
	LocationBasedDamage__ctor_m696C8475F1037CBEB1BB1761F47AE21AA1740955,
	LocationBasedDamageClass__ctor_mFC1B3EAB2B6A26AC96208E248AD345B74BAC0990,
	LocationBasedDamageClass_Contains_m8FCB31DEBC3C3CBE7613287C9265A68E956D8A87,
	LocationBasedDamageArea_DamageArea_m3F17D0ED30BC77484FFFCCACF663E157131BD32B,
	LocationBasedDamageArea_CreateImpactEffect_m84AB60EF9ED0712E8D95D1E0859ACA948EC53AF9,
	LocationBasedDamageArea__ctor_m5A4BCBFDE715CD9FFB8C62E3BB2CDA2EA5C2AEDB,
	CameraShake_Awake_m70375D5FD523C97A5D139BD1C82ED86EC5FADCA3,
	CameraShake_ShakeCamera_mF94E989FA4062A7CA63CAB82A357FBAC780DAD2B,
	CameraShake_CameraShakeSequence_m4D4C8D8F04B1B2422067D73C500E09098946978D,
	CameraShake__ctor_mDE62F0B29C78575A2EC71860091E575B0A32799E,
	U3CCameraShakeSequenceU3Ed__6__ctor_m2E0E7E3322E5C59CCD18196F187F931331A14A97,
	U3CCameraShakeSequenceU3Ed__6_System_IDisposable_Dispose_m29854744E5496ABECF9BBEE1AEA52D8A7FA3A4ED,
	U3CCameraShakeSequenceU3Ed__6_MoveNext_m1CA4204CEDB7D7AED6E6585334D438249056646D,
	U3CCameraShakeSequenceU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6407F666190A1039A848156BE8AC376AA8E3C112,
	U3CCameraShakeSequenceU3Ed__6_System_Collections_IEnumerator_Reset_mB8EB7F1CB4D826A388A6D987EDF8772039AE2970,
	U3CCameraShakeSequenceU3Ed__6_System_Collections_IEnumerator_get_Current_m5DBF667B8D29F5B278A8133D3B18D9AB319EF0A9,
	EmeraldAISystem_Awake_m45B3D4F989520C76C60BA9413AFD25173F99F0DC,
	EmeraldAISystem_CheckPath_mBCD46C7C03BF7230CA8C4E7E57893D7F33C78DFF,
	EmeraldAISystem_CheckAIRenderers_mE9DBC0D3A48D5E6E563E2C7219D4BC612347696D,
	EmeraldAISystem_Update_mD094F518566CF23487A437095470453A46A7DF25,
	EmeraldAISystem_WeaponSwitchCooldown_m91248A675D9489D044FF8DB0BAAD6D3CA2D175EA,
	EmeraldAISystem_UpdateWeaponTypeState_mAA66FF07A82EDACEF4F1715F13F365899BCF4CD7,
	EmeraldAISystem_SwitchCurrentWeaponType_m9180394306E771B01631C5E6EF83F901F9C30EFC,
	EmeraldAISystem_MoveAINavMesh_m1A50B653AF82034E05C034C9750C40A92B9A5966,
	EmeraldAISystem_MoveAIRootMotion_m611969DC25407979553936A39AC017BFF125205B,
	EmeraldAISystem_AlignAIMoving_mBBF7F563218B6FADCDF6C149EBC508D6E4EC8842,
	EmeraldAISystem_AlignAIStationary_m98021C3FF5786937FF6E5F4175570C3B0596E8B5,
	EmeraldAISystem_RotateAIStationary_mDEBB79150240E8703DD98C0A958AA0849DF09276,
	EmeraldAISystem_Wander_m3974BB1EAF8D62EE91329E1FB0F024E7EA3CB669,
	EmeraldAISystem_ResetWanderTimer_m7E5FADCBE4286E74DC1B5D9CA701116A05F3FF5B,
	EmeraldAISystem_FollowCompanionTarget_mD2CA859337BE2121FCBA011275AE65F143369DAB,
	EmeraldAISystem_ReverseDelay_m77214AFAA2D035CDC78556A22BA185C3F8E62F64,
	EmeraldAISystem_NextWaypoint_m115040B952728C1E6910DDFA5B187288481728E4,
	EmeraldAISystem_GenerateWaypoint_m6E03635A06282A52684A309A53CC0042E949C4F1,
	EmeraldAISystem_CreateEmeraldProjectile_m4AA4035E50376D01A83749B22F79326797EA2E66,
	EmeraldAISystem_InitializeSummonedAI_mFD0FD121AF6F3D50394C13DA1633689F282137F2,
	EmeraldAISystem_InitializeAbility_mBD8390CE84825AD7F1106E6BB58300D2FA0E1495,
	EmeraldAISystem_EmeraldAttackEvent_mA94173550D5137D87E982264120A6B96EC323551,
	EmeraldAISystem_SendEmeraldDamage_mE1A8B6FB20BD879F3392D228C2A0C1A869FE4BA0,
	EmeraldAISystem_EndAttackEvent_m1F6AEFFFC557E445DF9B90E2192C36EFAC2C7640,
	EmeraldAISystem_Damage_m8E80B7DA340B257B668FD34548304F7A9349084E,
	EmeraldAISystem_Deactivate_m4679E47301E4B0A64AF37008C8CBDD3F7F227905,
	EmeraldAISystem_Activate_m25174B262F175AC2EE04566C8A1BE148BC66698E,
	EmeraldAISystem_GetDamageAmount_m6559B147A0BF83FCE7AC39C2A6A363359331D169,
	EmeraldAISystem_GenerateCritOdds_m4DE750CC1795C14DF45211E77930B82BC11AFF7E,
	EmeraldAISystem_SetUI_m6A7D3756A5E33BD2B97EB74531820585C2FF297D,
	EmeraldAISystem_TargetAngle_mE345C67AB20E2BA84C7B64D1359DDA5DBEABC209,
	EmeraldAISystem_DestinationAngle_mB58D51507640012FB126C213C0B6CDE0052EE22D,
	EmeraldAISystem_TransformAngle_m21AD2AC0B6E2277D8FFBF5125DB78F29D4285D24,
	EmeraldAISystem_HeadLookAngle_m391B90F477A6221B6E1B75081B8365853E160518,
	EmeraldAISystem__ctor_mC98D210619C37BE50F7ADE362C80640F7B0CF11F,
	EmeraldAISystem__cctor_m480FBAAF94DAD1353D237C7D43F640DE31389C0B,
	MeleeAttackClass__ctor_m40974F2D359B676AD0DF5A57D6438F03E17CE2E1,
	OffensiveAbilitiesClass__ctor_m89F7284516BF3A2D569F70B4F090E356590D8462,
	SupportAbilitiesClass__ctor_mAD4310C6B30D69DE5B125F968F4048FDE4DF0BFB,
	SummoningAbilitiesClass__ctor_m6AC5C9025EC5DBE3A9B58CE38EB01A4DE84FF30B,
	FactionsList__ctor_m9691B451DD02EC3C3D0289D56F077D29C6557F99,
	PlayerFactionClass__ctor_m68BD6F7813BF7FF5B0D9BFAAA6A4F645BFAAF2CC,
	InteractSoundClass__ctor_mEA1489AC1FB3C2BC21EE638F3BA88CE480BBFFAE,
	ItemClass__ctor_m1BCCC38C7EEED0DA1113EF676167EB04F2C2A8D8,
	EmoteAnimationClass__ctor_m6221F073B5AED04C2984BBD2782528B097D2C1D2,
	AnimationClass__ctor_m6B33A31E564D7A8AE23CC80EDE27B0E521A9D4F0,
	U3CSwitchCurrentWeaponTypeU3Ed__814__ctor_mF6774B9139279CCFAB6B1863145E63FF405F78AB,
	U3CSwitchCurrentWeaponTypeU3Ed__814_System_IDisposable_Dispose_mDB098D8EE8FEBBBCF37C1B3C1641F726B1D93ABD,
	U3CSwitchCurrentWeaponTypeU3Ed__814_MoveNext_mB6197CFB988EC5DCAAE0904D1512D8C494C3CFC0,
	U3CSwitchCurrentWeaponTypeU3Ed__814_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FA538ED4DAEA1AFCB781E2FE2B57F54E8A80305,
	U3CSwitchCurrentWeaponTypeU3Ed__814_System_Collections_IEnumerator_Reset_mA751413854C58655AF7400A81AD358BF4643663C,
	U3CSwitchCurrentWeaponTypeU3Ed__814_System_Collections_IEnumerator_get_Current_m17D5938563CC1519E066D82FE7B09AF1764AF673,
	U3CInitializeSummonedAIU3Ed__827__ctor_mDB62F81516374233006055D2327D9C1766C8E351,
	U3CInitializeSummonedAIU3Ed__827_System_IDisposable_Dispose_mA0B7889CAEFDD84A3739116AAE5E84294BABBFE4,
	U3CInitializeSummonedAIU3Ed__827_MoveNext_mCDE54FF5C901069E404C9B6BFEF7EB5C4ECCE2B3,
	U3CInitializeSummonedAIU3Ed__827_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57CEE7975CE173FFCDDC16C1AB4575D6ED5D8769,
	U3CInitializeSummonedAIU3Ed__827_System_Collections_IEnumerator_Reset_m1E63611C5F7DE140142E34E0B0506EE26735D5F4,
	U3CInitializeSummonedAIU3Ed__827_System_Collections_IEnumerator_get_Current_m243D6382187CB5228D5CC8BAD88FE93ECF1FE787,
	AISchedule_Start_mA149953BD378744FB288500D17FAF7C93EAF7190,
	AISchedule_UpdateAISchedule_m880E519C129C769550D016CA4E06331EED7FEF98,
	AISchedule__ctor_mF33B7CA399AF066314D0D335BAB9039A9ACF7033,
	BloodSplatterManager_Start_m1332EB40A6A0775D2E8F603CFC4CE15F37A0E99B,
	BloodSplatterManager_CreateBloodSplatter_mB64BC21BA105D20F9EA8F5432DCB1B35ECB5009B,
	BloodSplatterManager_DelayCreateBloodSplatter_mF9911AFA23B83EAF5F5795C895C178B94E625469,
	BloodSplatterManager__ctor_mE352AB978A71F814000082E591D71A09534E92EF,
	BloodSplatterManager_U3CStartU3Eb__6_0_mB00B8FB853AFD8CCAB48AAFF8FF0B7E8D458D5A4,
	DamageAIByCollision_OnCollisionEnter_mC4E5FF771621A760C0C4C474F20F761569F71841,
	DamageAIByCollision__ctor_m5B2BC311B6893F9AF3E9427FBB82A851708797D3,
	HUDHealthBar_Start_m461CB14EC9731E000398D76C93DC74E57AB4AB03,
	HUDHealthBar_FixedUpdate_m0EB68D6B1C9966BC944C84B2B1DCCC5297E48C6B,
	HUDHealthBar__ctor_m43F12653740E18603B1E3597F5EC0407EB447BC7,
	HUDHealthBarTopDown_Start_mAC2270FB8161D88390640F0A75E8693A29BC3E23,
	HUDHealthBarTopDown_FixedUpdate_m8D2362D060980BC0E1435A4E53CDA48BE125D83F,
	HUDHealthBarTopDown__ctor_mDE2458CF745352E970958BEDD88AA65D6810BE64,
	MoveToMousePosition_Start_m3CE289ABF0FACA937420532B0A44E3963149DDAF,
	MoveToMousePosition_Update_m38E30BCDA7DEB17048FC35FF539D04F0690EE109,
	MoveToMousePosition__ctor_m99F70414B0783BE302A974AA8194616A45D59C94,
	NeedsSystemDisplay_Start_m502AD38F4EE7D766E56DC961234EEC16EB471997,
	NeedsSystemDisplay_UpdateTextDisplay_m45625FBB001A2640F5C85ABCFD5CFE63CAB8F7B1,
	NeedsSystemDisplay__ctor_m539636C21CB4F3A9C857B7EF76921C8F2C125D98,
	PlayAIsEmote_Start_m5A9C64B7ACB55282385568521010AAD71628705E,
	PlayAIsEmote_Update_mF1479F71BDB2FB7509EE954265BAD64610961AD3,
	PlayAIsEmote__ctor_mDFE8DA8180648C626D00CA168787C01872F40DA5,
	RaycastHitAI_Start_m5DAE8B6850E9B4E45D4C0EC406809B8E25D7D83C,
	RaycastHitAI_Update_m53F51D9B89BF35766D3DC3378FD5771E792F66FE,
	RaycastHitAI__ctor_m10337320A94B93FC43033FFEEFE5EA8FFE643C22,
	SpawnCompanion_Update_m73F5CC3D3426E758DAEEAF99026335FF54055B4C,
	SpawnCompanion_RemoveAI_m6137362AEAA906838853C55F1A7F3C199D6F4EE1,
	SpawnCompanion__ctor_mE0678C1B4D782BE74B7CAA057DAF0247F74EF84F,
	SpawnCompanion_U3CUpdateU3Eb__3_0_m063284D43EBC9BB9401A9DE0A22728189B50D45F,
	SpawnObjectEvent_SpawnObject_m80C488D295A241FC2ACDD5485F2BCAC018C480DE,
	SpawnObjectEvent__ctor_m1D4F7FC399DC161B2C66FE7AE41517CA449AFB63,
	TameAI_Update_m3CB43810123B342D16692AEA5F6A8D000D856A64,
	TameAI__ctor_m08D2962707CFC257C116325213032C10FD8A6D41,
	UpdatePlayerHealthOrbUI_Start_m71F262F218BF63FF49F0FE6AAC74CD4ABF1F2E4C,
	UpdatePlayerHealthOrbUI_UpdateHealthUI_mDA48AB2E01F9C989C5011B252C6DD7674C1CF797,
	UpdatePlayerHealthOrbUI__ctor_m8D4CCA6FEF4952876490F5D74DDE6E4E5C1E3A25,
	UpdatePlayerHealthUI_Start_m55E381E3B827EBC56FD9F21D2D00F2E8F862ACA2,
	UpdatePlayerHealthUI_UpdateHealthUI_m683DD5CAF23AF187C5741F55C47EE9033FD3D460,
	UpdatePlayerHealthUI__ctor_mF7CE0757FCD893C564EAF5697124A450D4239103,
	EmeraldAIPlayerHealth_Start_mADD10B14D4CDC23C3662EE2A8FBD0A296028B42E,
	EmeraldAIPlayerHealth_Update_mFAF2A7517BCE1D9000D40812F4CB496D800B3E61,
	EmeraldAIPlayerHealth_DamagePlayer_mCB554B3C94D15543B530960B059448F25B28CEED,
	EmeraldAIPlayerHealth_HealPlayer_m1FFF385050E437281BB1DE1C1F23C5C3B4DB9FE0,
	EmeraldAIPlayerHealth_ResetPlayer_mEF6DA44E8B82A4B5A5B40184416C9CC0FB6E3D94,
	EmeraldAIPlayerHealth_PlayerDeath_m77D92DBAE0CF2513209EEE34F3B256F944F66657,
	EmeraldAIPlayerHealth__ctor_mA04B1422593D5B5B60DD4EA7C0EB9098E6D1863F,
	AnimationProfile__ctor_mDEE65B2258B5C14133F201E3E7A6D8B6DBA14592,
	EmoteAnimationClass__ctor_mC4B79C25A13E9F4882655BE35762CCE97C7B361F,
	AnimationClass__ctor_mBBDBFF565C0BE751837A9AECA58E7935013949F0,
	EmeraldAICombatManager_GenerateMeleeAttack_m2131824A860204CA187851525A92EAAA32255720,
	EmeraldAICombatManager_GenerateMeleeRunAttack_m87D7AEDC6F91DB141EC9B0149A51C3D5093333E7,
	EmeraldAICombatManager_GenerateOffensiveAbility_m6B96C56ACF9B0CBA08026564E3A194F1D11533F4,
	EmeraldAICombatManager_GenerateSupportAbility_m0149028E9F2954FBCB23B35EA478206F8DED82EF,
	EmeraldAICombatManager_GenerateSummoningAbility_mF1B52F48362564B24A707F3FE4978A9C5694320D,
	EmeraldAICombatManager_GenerateProbability_m9212DC8E8CE726D88324125611E584E3CEEB4FED,
	EmeraldAIDamageOverTime_Start_m97CB1873981E6B5890FAD923E48E291FF042D3F1,
	EmeraldAIDamageOverTime_Initialize_m479893F5BB8190FF17E80A31A0BAF9547CDC158B,
	EmeraldAIDamageOverTime_OnEnable_mD24F5CBFA0FF1DED40CC3D239F596413461C079C,
	EmeraldAIDamageOverTime_Update_m602C5D4C17A8C549E85506BB520B8DCEE86A2164,
	EmeraldAIDamageOverTime__ctor_mBE333A71ACAF03835E38B9E3E88E815ED53955AD,
	EmeraldAIDetection_Start_m0B70E2AC3C7A92BEBDF4145BE8EF7EC5DD8526FF,
	EmeraldAIDetection_Update_mDFD4D60BFBB1FA164603AA9ED1FFA188A390E6AF,
	EmeraldAIDetection_FixedUpdate_mD8875B1C1925BC8131EC9869A72AFB84950EEA2E,
	EmeraldAIDetection_DetectTarget_m205A232FDC39864B962F2DEF5D476F20C82620DB,
	EmeraldAIDetection_AIAvoidance_m15273A4A6DA3F748C869DA2BA56D23D9AB432A39,
	EmeraldAIDetection_UpdateAIUI_mFDD04E833CCAE2494483D0566F64E5DAF1A566ED,
	EmeraldAIDetection_UpdateAIDetection_m5B2AEA29C55E22CB5396C3CE20F7F9591FA048B0,
	EmeraldAIDetection_LineOfSightDetection_m9A44C352F0397A0CD6BC8A51F731C02476500224,
	EmeraldAIDetection_SetLineOfSightTarget_m8973F95A89D522B2291B5963D050E95EAF115281,
	EmeraldAIDetection_CheckForObstructions_m73802B5F48C752E771AF2E7FF5D52D4B8EB53D06,
	EmeraldAIDetection_SearchForTarget_mBA9CF85E510519243E1661E2362CE53145A7A675,
	EmeraldAIDetection_SetDetectedTarget_mEED5CB3A3A5769248F2F2356B6B91539F0A56757,
	EmeraldAIDetection_DetectTargetType_m0973A3D11A3B0ECB09ADB43C9919B13E861821F8,
	EmeraldAIDetection_StartRandomTarget_m081BC53F0ABD7CE3BEFB822B5CDACED451F27A1B,
	EmeraldAIDetection_GetTargetFaction_m1767C2E90C36503953F404B1EDA79FDFB209FF31,
	EmeraldAIDetection_GetTargetPositionModifier_mBF6D21537B2C1B28E77D102879E26532B2B9AA97,
	EmeraldAIDetection__ctor_m1660AEBA5CEBAAD0036A5FBDFC4826B4BEF9BF9F,
	EmeraldAIHandIK_Start_m5267EA51AA023BF51C27A94325314F837AF1CA74,
	EmeraldAIHandIK_OnDrawGizmos_mBD04BBAF653A6605DE2CE175618C58E70EE42128,
	EmeraldAIHandIK_InstantlyFadeInWeights_m0A9EB26C2C28FE38A8E0DD4D332A4CED47690150,
	EmeraldAIHandIK_FadeInHandWeights_m1F83B8BDA6D801B599B3A82A63252BBFCC104CD2,
	EmeraldAIHandIK_FadeInHandWeightsInternal_m7A5AF1D5A2BBCA241EF7E45E34C6BDA691893CB7,
	EmeraldAIHandIK_FadeOutHandWeights_mCFD9B9C43A53F6D2505F7F9CB155154C446F79CB,
	EmeraldAIHandIK_FadeOutHandWeightsInternal_m74A3842B06B72350B7B5C0F9B208E6497F797C8D,
	EmeraldAIHandIK_OnAnimatorIK_m97BC7049D567C9298A6C0AC1FA880ACDAEFBA026,
	EmeraldAIHandIK__ctor_mFF382942E5E25DF98A4ADA6FB844249742B8516E,
	U3CFadeInHandWeightsInternalU3Ed__14__ctor_mAE88A729F149B283C61C6DE22446A4BE4F719040,
	U3CFadeInHandWeightsInternalU3Ed__14_System_IDisposable_Dispose_m9E3A977CEEF76C8D48B070E1D4F13606833AA263,
	U3CFadeInHandWeightsInternalU3Ed__14_MoveNext_mE51620361ABBEB680EB780309E9DEF3518D40701,
	U3CFadeInHandWeightsInternalU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78BD2815DF875FB11125488CC9B332D4C30CDED9,
	U3CFadeInHandWeightsInternalU3Ed__14_System_Collections_IEnumerator_Reset_m91F6F5E0C6DF95D4B8C94F3D93A251630A1DEAAD,
	U3CFadeInHandWeightsInternalU3Ed__14_System_Collections_IEnumerator_get_Current_m1636832462E8490262CEDE81A697671DA6B899E9,
	U3CFadeOutHandWeightsInternalU3Ed__16__ctor_mB5F6CBF6D36BF6F3E74A0F1438253951B2179374,
	U3CFadeOutHandWeightsInternalU3Ed__16_System_IDisposable_Dispose_m605826D9DD1548895B738577068FDCC9AC4B5EF8,
	U3CFadeOutHandWeightsInternalU3Ed__16_MoveNext_m8CC08E3B61E44A788849FE58A4D9E55035101E9F,
	U3CFadeOutHandWeightsInternalU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F163CE5139D1E7E64A1017FD003ED4D151BD8CA,
	U3CFadeOutHandWeightsInternalU3Ed__16_System_Collections_IEnumerator_Reset_m656C7506885341887E64696E50ED405AC8EE657A,
	U3CFadeOutHandWeightsInternalU3Ed__16_System_Collections_IEnumerator_get_Current_mA2BED98C71D68703D224C5BA0E54A586ACA57D85,
	EmeraldAIInitializer_Initialize_m23B501440489AD2196BC13409CAE9D2C80E8CB2D,
	EmeraldAIInitializer_Start_m4BEE8D8330B227B23C68CA9C372CE5ECDBCE711D,
	EmeraldAIInitializer_CheckFactionRelations_mF77FA3D93644C5791CFB8A6C4244EDFBB299270B,
	EmeraldAIInitializer_OnEnable_m823C469A760687C3976A67874909D25C15FDB93F,
	EmeraldAIInitializer_AlignOnStart_m49CFA22A5F034AB64AECBD542BAF3DE1A23119CC,
	EmeraldAIInitializer_IntializeLookAtController_mC16ABC247748846C2E00D4C360D677E89EFACE80,
	EmeraldAIInitializer_DisableRagdoll_m1C3D832C82D87B439FE1F47FD10453180255953B,
	EmeraldAIInitializer_EnableRagdoll_m8538E505C379FB63C0239A5711E64D12454873CC,
	EmeraldAIInitializer_SetupEmeraldAIObjectPool_m2757B58199DBD1520E8F4D73C6C3223D12C18282,
	EmeraldAIInitializer_SetupAudio_m4069AA45C16E2D2FCF809299618F0D3E5F637428,
	EmeraldAIInitializer_SetupOptimizationSettings_mE034912071A81D179FA25B8E2C82F924289E6761,
	EmeraldAIInitializer_SetupFactions_m21DF4C92FD5648A92B4863FE20471A456B272D07,
	EmeraldAIInitializer_SetupAdditionalComponents_mA983E9A6C8EC07B0EF53DB2DF880F9025C7E6FA7,
	EmeraldAIInitializer_SetupNavMeshAgent_m7638E20841D89D94C2AC13472A81F0D7A2A0A3B0,
	EmeraldAIInitializer_SetupAnimator_m006B0F85530D496B6057D3AE7C06209527C77820,
	EmeraldAIInitializer_SetupHealthBar_m80952BB09E142B45707D0A4D2A55D5BA0D27E298,
	EmeraldAIInitializer_SetupCombatText_mE2D0B36F5E53CF750625A2FAA30FC26997C221F7,
	EmeraldAIInitializer_SetupEmeraldAISettings_mD45DAEF20520F0884B56AE0BB5EF7AD05B8D850F,
	EmeraldAIInitializer_CheckAnimationEvents_m24314877516694B59521D5296A36C8C6880A527E,
	EmeraldAIInitializer_CheckForMissingAnimations_m0AFAC4373FADFBE5763C5C86F9799D5CCED42BAB,
	EmeraldAIInitializer_InitializeWeaponTypeAnimationAndSettings_mD7432EA3C91D5E813F3F51CE344ADA82AC32914F,
	EmeraldAIInitializer_IniializeHandIK_m1F3B897364BDB0B88FEC91BC15F7FFB106E57400,
	EmeraldAIInitializer_InitializeDroppableWeapon_m7B0DFE6E9B3844EAF58DEBE43DA40FCE478CE1E9,
	EmeraldAIInitializer_InitializeAIDeath_mC39B43212A6B87C9A12DB1CB558CB538A5C128F3,
	EmeraldAIInitializer_Update_m03A7B9B5B6E62331DC8633A4233F369FFBDA067C,
	EmeraldAIInitializer_RagdollDeath_m2ADFD701F62BDB8A8B69CE256F77BAB31F4C43AF,
	EmeraldAIInitializer_AnimationDeath_mC892B63C345DAED627EE7E091DD336AEF38E7467,
	EmeraldAIInitializer__ctor_m15C4B84A22899125856698518B030965BC73EDDC,
	U3CAnimationDeathU3Ed__28__ctor_m98C8ACB372B8DD77893A1E3109B15BE60AD42927,
	U3CAnimationDeathU3Ed__28_System_IDisposable_Dispose_mBF2CE1D597149BB8992C20F80F6E44F69FB7F41C,
	U3CAnimationDeathU3Ed__28_MoveNext_mFB517346497CB8AEE5E4F633E9A9F2DD522EB9CE,
	U3CAnimationDeathU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B81F7CEB128D88A50C0A0E33BCB1A7C2790FF0A,
	U3CAnimationDeathU3Ed__28_System_Collections_IEnumerator_Reset_m9AA4B65FE39E42F947F2FB003F55F7114085A1E6,
	U3CAnimationDeathU3Ed__28_System_Collections_IEnumerator_get_Current_mD7ABD5C2B1EE8E19B8557582F4742656AFC57D3D,
	EmeraldAILookAtController_Initialize_m849E9A9C23BDBE1840EC7103C4BCD71FD96BBC67,
	EmeraldAILookAtController_ResetSettings_m9810EB8015E317D617098DAD7F26A8422EAD59A6,
	EmeraldAILookAtController_CalculateDistances_m5711271F63A2F8E9024B199F97B605C514AAEA0C,
	EmeraldAILookAtController_OnDrawGizmos_m5551353CEADDF8422A004E8E08FA99E66CA7531D,
	EmeraldAILookAtController_LookAtDirectionAngle_mDD1C577E70D9778B0954B4F886B93D2E30D4C3BB,
	EmeraldAILookAtController_UnityIKAngle_m3D7F807C948D8341534AB2475C8E33D871EC8262,
	EmeraldAILookAtController_OnAnimatorIK_mD9A6CE9C4E342543B8E62A9E00D563D91DF39311,
	EmeraldAILookAtController_FadeLookWeightInternal_m6209EB7582B76950A081CCB69FB71FDDA9348B8A,
	EmeraldAILookAtController_FadeLookWeight_m12D805BACC9DAB7D5B6724661BEF380C13D11DB4,
	EmeraldAILookAtController_FadeInBodyIK_m1E5053BA8EBDBD6F2786BCA542ED5E33B2600C41,
	EmeraldAILookAtController_FadeOutBodyIK_m6BE3D633AC3BF5FB70BCBBDCB97B4EAB6CF877C8,
	EmeraldAILookAtController_LateUpdate_m2F1FC102AD73B807B81B5FAC14E0C9B95EB899E7,
	EmeraldAILookAtController_FadeInBodyIKInternal_mEF97E6C7E22B1FF85A385D8DDC693C64DAF264F7,
	EmeraldAILookAtController_FadeOutBodyIKInternal_m96A83BD97E45B12A8B99B91061D2CA8BAB968B4C,
	EmeraldAILookAtController_LookAtTarget_m6C04068C2CFA266191A478E974CD1AFF3B06A8AC,
	EmeraldAILookAtController_SetPreviousLookAtInfo_m74C27F5EEC9A002548134204E55D451BB3A00BEC,
	EmeraldAILookAtController__ctor_m6B396A4AC0CD4CEFC5E4D52A7D8E7F0C3C93D58E,
	U3CFadeLookWeightInternalU3Ed__37__ctor_m9316952730CEED787A2E2BF8837BC1824BBAD2A1,
	U3CFadeLookWeightInternalU3Ed__37_System_IDisposable_Dispose_mAC5C657408E5BDA0CBCC2AB6E8AEE9279C31D715,
	U3CFadeLookWeightInternalU3Ed__37_MoveNext_mCFBF22903E0BAD2F4347C3781CE616A4A7037A64,
	U3CFadeLookWeightInternalU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m017B1AA4B4FEE165E432A9EA7210B02FAAD8D7F8,
	U3CFadeLookWeightInternalU3Ed__37_System_Collections_IEnumerator_Reset_mED04AE8C60F4F7B1AB48938083704EFA9BD7F2B6,
	U3CFadeLookWeightInternalU3Ed__37_System_Collections_IEnumerator_get_Current_mB6BD28C668E28C9FDDC98304DF032C0FBA253AF8,
	U3CFadeInBodyIKInternalU3Ed__42__ctor_m03838ED0C1B393F8B9151DFE20FB63DD71DC1DFA,
	U3CFadeInBodyIKInternalU3Ed__42_System_IDisposable_Dispose_mFE7497423ADE5E90DE7A065A5C188956DBA19788,
	U3CFadeInBodyIKInternalU3Ed__42_MoveNext_m7E7957B0F70495F9802E0C894C29F5E94E7446D9,
	U3CFadeInBodyIKInternalU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC203A9141392AC0EBE3808D0CC010AFE4269228A,
	U3CFadeInBodyIKInternalU3Ed__42_System_Collections_IEnumerator_Reset_mE0AEDF727A0B3E44D7C3B5E6AD9A5BEF39BFBF4F,
	U3CFadeInBodyIKInternalU3Ed__42_System_Collections_IEnumerator_get_Current_mBF9903AD75A042EC01EE79F97D8600F1B21FA355,
	U3CFadeOutBodyIKInternalU3Ed__43__ctor_mD5AC451D67E5514DE006705439032ED511E3300C,
	U3CFadeOutBodyIKInternalU3Ed__43_System_IDisposable_Dispose_mD06050A80C059F2D098ECFF6597547122AC0CC37,
	U3CFadeOutBodyIKInternalU3Ed__43_MoveNext_m957D7A7140614A6E4BCFDE5FA1ABE3C515846C07,
	U3CFadeOutBodyIKInternalU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6F021F70E667BFE3A62E4D90E6254FD24DB2201,
	U3CFadeOutBodyIKInternalU3Ed__43_System_Collections_IEnumerator_Reset_m4C2130811612412D90C8D0579F9F0EC19DA940B5,
	U3CFadeOutBodyIKInternalU3Ed__43_System_Collections_IEnumerator_get_Current_m01FE9D1887D16B1E70E78A162332B46F4CD3F968,
	EmeraldAIObjectPool_Init_m331E2EEC5E8B14ABF3305E12BEF55E5F889632DA,
	EmeraldAIObjectPool_Preload_m6ACC7E4F8B713BE0FE98BBE386D0B53B82188DC0,
	EmeraldAIObjectPool_Spawn_m521DF838B00EE821A161311689AC4AFA7188FDD2,
	EmeraldAIObjectPool_SpawnEffect_mDF3F7B87B95F6375481F08983C4C2488F37C1979,
	EmeraldAIObjectPool_Despawn_m8801CF2179E3D123AE022EB6D1E8251D53DB86BD,
	Pool__ctor_mA26737048C9787C8684DC871C7859F55616E770E,
	Pool_Spawn_m4BB445962F7E1123C8D0C60DEB2F78B0642E2CD6,
	Pool_SpawnEffect_m17F8514E1C282BE7CD0234F4E8491C4183109E71,
	Pool_Despawn_m91FAB15F5A6674AD5C0A71A277535A1B256C7EFD,
	PoolMember__ctor_mDFEEF36BE3B683923DCDF5A8F0BC4CEB1270ACAA,
	EmeraldAIProjectile_Awake_m541F97261D9A5E7F0F636C638C74D60870B5ECAD,
	EmeraldAIProjectile_Start_mD9EE46B200DDBFC51DD189A762A2B9F8E5FEC3D8,
	EmeraldAIProjectile_InitailizeAudioSource_m8F96232150BE06CADF9DDFD43D056E8F0F9AA5DF,
	EmeraldAIProjectile_InitializeProjectile_m31943F9BE26A9BAC154D81D03F055F9E021DCACC,
	EmeraldAIProjectile_GetHeatSeekingAngle_m3F8D2295DB55A7353F178D8DA8C17A5A2E4FDD6D,
	EmeraldAIProjectile_GetAngle_m13E3119AD654E0A7E93A50E8C1CFDEB989C6F00A,
	EmeraldAIProjectile_Update_m9F37882F9B1E67481458ABB19C27F2DD9713C3FC,
	EmeraldAIProjectile_OnTriggerEnter_m30D3C7CE1F6D7085EB12C452DFCCCDA81FB6457F,
	EmeraldAIProjectile_DamagePlayer_m55D0CC68AF0F507ABF53A131A7B40251F3393CBB,
	EmeraldAIProjectile_DamageNonAITarget_m15A216B5451A0AF10F97200EDC797DC39ACF2B36,
	EmeraldAIProjectile_AttachToCollider_m6854C6F2EFFC615F61EC7F3FAA8A14B682120AFD,
	EmeraldAIProjectile_PlayCollisionSound_m3B64DFF6F27432D2B868628BF88A2A211EC8F7F5,
	EmeraldAIProjectile__ctor_mAFE905AF86297F135315668ACC2C455D386E2490,
	EmeraldAIProjectileTimeout_Update_m76C9A02421304DC1873487B22E09670596EBC1C8,
	EmeraldAIProjectileTimeout_OnDisable_mCF2FC08C5AEB844050FF60FBBB58E3B924CE8828,
	EmeraldAIProjectileTimeout__ctor_mC179FFB97BF8E636751ADE406041F943584AEC1B,
	EmeraldAITimedDespawn_Update_m7987E2D0F93BA6DBADF4086D581F64A556A3146D,
	EmeraldAITimedDespawn_OnDisable_m897938B04CED2EC2CE10775A01C240DD473A2F4D,
	EmeraldAITimedDespawn__ctor_m20EC36AD7E747093B712F6B9FF8F722464B4D32D,
	HandIKProfile__ctor_m80AAE7993F21F3A639EB5ADB3AD82A53032EC9AC,
	SummonedAIComponent_IntitializeSummon_m3BEC35559E3A5D326F2AFFA0284317F03CAF1013,
	SummonedAIComponent_DespawnSummonedAI_mDC649AE08FB1354483ADEDF497A74430E978688B,
	SummonedAIComponent__ctor_mA9E9829D3429684BC4473422611E5585C2CA3A2C,
	U3CDespawnSummonedAIU3Ed__2__ctor_m3C7C61502E288F877E17FC32C81C11C9EB7D9118,
	U3CDespawnSummonedAIU3Ed__2_System_IDisposable_Dispose_mAE67CE37DCF3763CCF53736F9E3BDF59B444144C,
	U3CDespawnSummonedAIU3Ed__2_MoveNext_mDC8B1CB467937234B3EB0E3EB4DF5071E9F691A9,
	U3CDespawnSummonedAIU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m631E76C46F5D7BC0457C3F9EA5F76D45D72BE2D1,
	U3CDespawnSummonedAIU3Ed__2_System_Collections_IEnumerator_Reset_m1DFBE0EECED137260A1091251AC834AB73C8C18F,
	U3CDespawnSummonedAIU3Ed__2_System_Collections_IEnumerator_get_Current_m42F097180D3AB62A3D2201945A84ABC0356A9563,
	TargetPositionModifier_OnDrawGizmosSelected_mF508F24E8FED5CCCF3FA1C2A6321565D289192E6,
	TargetPositionModifier__ctor_mB44157621A03D1EF7099720B011FBEC5CD952363,
	TrailRendererHelper_Awake_m024630BD346F83071721B0E49DDFAC7B775FEB75,
	TrailRendererHelper_OnDisable_m82DC24FBE41E970CC5A219C14F1FE9E6921B39AB,
	TrailRendererHelper__ctor_m95C55E0CC7AAFC3BB7060BE5A8F3106A2C46E5C5,
	VisibilityCheck_Start_m14755D832F04E93C262C5699CE893D9AB2155702,
	VisibilityCheck_InitializeDelay_mDD8B25C008F5AA0E5B3BF9DEC65613F40E09EC3A,
	VisibilityCheck_OnBecameInvisible_mDDEA72303F2BB64E2280410A3471E8703F6E8083,
	VisibilityCheck_OnWillRenderObject_m0177F90B78EBF0FC80D070C52A975918A2DC6804,
	VisibilityCheck_OnBecameVisible_m46671C26EAC77F739D8DBDC048296D4C4A9BC526,
	VisibilityCheck__ctor_mDF6E2E7F00A67EE72B71C75C1E15DA909AC1A9D9,
	VisibilityCheckDelay_Start_m6566BFFF81A848DE93D177879814E2BC317D4EA5,
	VisibilityCheckDelay_InitializeDelay_mF0AAD81F8F44D122A60E2B1BFCB211FBD3B90795,
	VisibilityCheckDelay_OnWillRenderObject_mB9EF14F29A06C0C50BE99687B7CDCBDA7D85AFFC,
	VisibilityCheckDelay_OnBecameInvisible_m5FBA52667CF0CEB200F872EEE19A5DA8346D6220,
	VisibilityCheckDelay_DeactivateDelay_m0A142AAFFA1CE1493CD49254FF38D77547B24133,
	VisibilityCheckDelay_OnBecameVisible_m60F68642AA4A95054CC0F7665C9B7AB969C63948,
	VisibilityCheckDelay__ctor_mA9922B51BB372A768F3A84AB807455BAAFC7857D,
	DebugEvent_DebugEventTest_m9687F4ECBD86DDB94272EBD5F11D6567F45405DD,
	DebugEvent__ctor_mDA8E8290C8C13A2E43E4F28158864889C18D3B24,
	CombatText_Start_m7F41117168280E0A2A540B854CC9AF4D2F5A7C2B,
	CombatText_Update_mD5F47CCEE359B4EAA5516E6FF54AE744A7FECC69,
	CombatText__ctor_mB5C74740D05B0926F0830F42C3F7A020E83C6655,
	CombatTextAnimator_Start_m9DAE84B71E4EC259B67D2081D3EC0F5FE94DE2FE,
	CombatTextAnimator_Update_m00035DA5AE643E1752F37BC1B72F3600E771AC1E,
	CombatTextAnimator_OnEnable_m59E4C59CEF8D80772DE2468F831C3FD5C659F674,
	CombatTextAnimator_OnDisable_m0F65903702349916C7627A678E385DBE175764D3,
	CombatTextAnimator__ctor_mCC4A40AFF93E6FEBFD2F873617C0012104EB87E2,
	EmeraldAIHealthBar_Start_m5A8E7AD4B16A6A69EDF2A762B5E347FA9A532633,
	EmeraldAIHealthBar_UpdateUI_m8AFF5911F35780D0EA4D4F754FBD1EDA36F30B34,
	EmeraldAIHealthBar_Update_mBC659A0EBE8C01095E4FF8450D929ED3978C95E6,
	EmeraldAIHealthBar_CalculateUI_m8AD4E711C6DB491D0092F37FE6033B852F7FD450,
	EmeraldAIHealthBar_FadeOut_mE6C5172F84A440AC570CB4E28A75426876A9D456,
	EmeraldAIHealthBar_OnDisable_m2FAC2AAF8915FE3D0B0AED8C3994302B4AF6A9C3,
	EmeraldAIHealthBar_FadeTo_m94D5050A5724399C2015F5524BD946C15C040C58,
	EmeraldAIHealthBar__ctor_mEE8414DB8B5D45780DCC517D22EDE3C2C7089CDA,
	EmeraldAIHealthBar__cctor_m1BF6F0CF01191FBDC9FF5A7A4A4E9D7A8D7A322F,
	U3CFadeToU3Ed__17__ctor_m014EAF78D1A6721837081920CA2688C0889B80F9,
	U3CFadeToU3Ed__17_System_IDisposable_Dispose_m66BDEB12928B226D6125BE8B132D80138A217452,
	U3CFadeToU3Ed__17_MoveNext_m7C7D09DEC81384148BDB653D409DF1EA3BC10E8E,
	U3CFadeToU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65F555CB65E12F7EC1A62C836AFA6B97DEC9FF95,
	U3CFadeToU3Ed__17_System_Collections_IEnumerator_Reset_mA5C040D683D2E9552EA50C4DB1F8B5B933E04CCB,
	U3CFadeToU3Ed__17_System_Collections_IEnumerator_get_Current_mA3592636DFFDFE800DB48C7FB222B6A56A58F5CA,
	EmeraldAICharacterController_Awake_mE86036FB0B6BA4CCF0F1E1B4BD04EF193A6E0195,
	EmeraldAICharacterController_FixedUpdate_m2C8ACF38845D3ECFE3C1176F3CAA01710BFA6BA3,
	EmeraldAICharacterController_OnCollisionStay_mEE2335E9337A80ED2D2145453A8C0594CFF24508,
	EmeraldAICharacterController_CalculateJumpVerticalwalkSpeed_m6E82CABD6074603F0225E2F2871BC2EF87C22CCB,
	EmeraldAICharacterController__ctor_m06156EA548F49A766331FCFC384FD298C116C447,
	EmeraldAICharacterControllerTopDown_Start_m434980E84DC0CCF7775F3CD124071F05DD17997D,
	EmeraldAICharacterControllerTopDown_Update_mFF499F17DF21B7EBE8ECC780FFE9B565A69AF050,
	EmeraldAICharacterControllerTopDown_UpdateHUD_m6415AB9783BFC24CF7EB011B83A0151A2147E07B,
	EmeraldAICharacterControllerTopDown_StopHUDFade_m3B83475A9B273D19551654F23E2B96AA57DB16F3,
	EmeraldAICharacterControllerTopDown_FadeHUD_m71679D8B6A1656C20CB682A3578CEE3EC5DA75D8,
	EmeraldAICharacterControllerTopDown_GenerateCritOdds_mB15DAA42F71EAFA2CE7CC9701128FCA72F0A6157,
	EmeraldAICharacterControllerTopDown_DamagePlayer_m000849138FC62351E8E9A67CC81B3D8E3AD5B11F,
	EmeraldAICharacterControllerTopDown_PlayHitAnimation_m0C196B9438B0298EC08B4B5F90B79FD3699298C1,
	EmeraldAICharacterControllerTopDown_UpdateAttackSpeed_m6A0BDB147F4CB8D412A7D282F323CB67C5AC2BBC,
	EmeraldAICharacterControllerTopDown_PlayImpactSound_mD545FEABFBD85CCF5851890991448248512F7A98,
	EmeraldAICharacterControllerTopDown_PlayCriticalHitSound_m571560124DAD33A65F47C61AB4C4AB7A42DC865C,
	EmeraldAICharacterControllerTopDown_PlayAttackSound_mDBA9E600D50CF87B558231C7A7C8E3344FD19886,
	EmeraldAICharacterControllerTopDown_PlayInjuredSound_m424BC3D98C45FC207DF3A005DC0115A6D19D72C4,
	EmeraldAICharacterControllerTopDown_PlayFootstepSound_mD2DFB307FB4345D6DF448B302FC8633F477B22F7,
	EmeraldAICharacterControllerTopDown_DamageFlash_m8C07E808E8601607D4FFE12861B58CF0D38B0B33,
	EmeraldAICharacterControllerTopDown_AttackTarget_m4ED7442C6D20183A0E29397D5E7D50F7119B54C3,
	EmeraldAICharacterControllerTopDown__ctor_m0123EA7F80AAE6234E973CCDD608C85BF41646C5,
	U3CFadeHUDU3Ed__53__ctor_m1F22327858A076BE7DADBA6076EE9B15B3CBF2B9,
	U3CFadeHUDU3Ed__53_System_IDisposable_Dispose_m840376B660F810C3DABDD355D5247CDD5D91E726,
	U3CFadeHUDU3Ed__53_MoveNext_m46CB8E798845424F1FC7F74C1306182E8641B3E8,
	U3CFadeHUDU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66E53E0BD358A1085020F7D19B0C058651066DD1,
	U3CFadeHUDU3Ed__53_System_Collections_IEnumerator_Reset_m83C1788731B5BD4DBAE484741BCB2ED1D51C288E,
	U3CFadeHUDU3Ed__53_System_Collections_IEnumerator_get_Current_mCA148FDB72D0FF869E08C24AD64F01F275B26711,
	U3CDamageFlashU3Ed__63__ctor_m780BD742ED2DE2338314A5DBD2C246B0B3684EA0,
	U3CDamageFlashU3Ed__63_System_IDisposable_Dispose_mDA06D3301EB76A42C39293B7EFCA7D4358ED7095,
	U3CDamageFlashU3Ed__63_MoveNext_m71A7B91950273B3EE5ED7A545E34C0DAD3CE9DBF,
	U3CDamageFlashU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m046AE2B4611065C4EEA43F822CF0D39DAA6460C0,
	U3CDamageFlashU3Ed__63_System_Collections_IEnumerator_Reset_m866486D3113D700E944CDF0C0EA3C4C07D54C16A,
	U3CDamageFlashU3Ed__63_System_Collections_IEnumerator_get_Current_mF7EB101309A7B58D50BF053AC6B0720110945C0D,
	EmeraldAIHideMouse_Start_m02F80F68F17D4E0D93FFE124DC0BD713D869C508,
	EmeraldAIHideMouse_Update_mF2227B14D3C8F9B275969D945456C73F0ECE27BB,
	EmeraldAIHideMouse__ctor_mE0C076A08E8846FD583B99576217CFB207B8995A,
	EmeraldAIMouseLook_Start_mCAFF03EF8493638C229901963AF321E23E24E67F,
	EmeraldAIMouseLook_Update_mB1CE642BB82495B250C54E0EED69DBD9FE6ED136,
	EmeraldAIMouseLook__ctor_m7BA05B512870C3DACBB026A665EE85DD4AEB7455,
	EmeraldAISmoothFollow_Update_m2DBC137B5BE3365A7CC98C9A92B4A99C3306AF80,
	EmeraldAISmoothFollow__ctor_m2E4C593FD8B775A92953C32BB80E654430D58777,
	DamageAIByAngle_Update_mF39EB9806A52CEBA6F571A1C578EAD4369FC71C0,
	DamageAIByAngle__ctor_m6E3D92229711034E67164A864D5903DDA25FFDF7,
	DamageAISystem_Update_m61754498ED6FA6AAA7022DAE520B75484A90A115,
	DamageAISystem__ctor_mF252380CC616DD1F01FF9A4D664FE583CF924685,
	PlayerWeapon_Start_m93E59AA1E94422FE5794BEAA75A49972133A49AC,
	PlayerWeapon_FixedUpdate_m7CB71E473715B4B554706F26F6F2D9BEB921456E,
	PlayerWeapon_Update_m03AE70B30EE48C1EB7CFD07ACC9FFF8A7AA45F3A,
	PlayerWeapon__ctor_m1781F1ABB848314457CD8DEE1E86ABD50E12BE3F,
	PlayerWeapon3rdPerson_Start_mE072C768439080D908DC4C2C507AC518CF9C93CE,
	PlayerWeapon3rdPerson_FixedUpdate_m650DD8F9B34F33B0F9121D5EA077B3F7D0405E00,
	PlayerWeapon3rdPerson_Update_m27A19FBD3F32B5E7557435BA3D85C4D15EF7408F,
	PlayerWeapon3rdPerson__ctor_m00BA5506DCF8555D2ECAAB7681B93E89F2A98EA6,
	SwordAnimation_Start_m5F1971FF366E85A6F46ABECABAB6521595AB0434,
	SwordAnimation_Update_mD97DEE29589C086FE1C275205ED0086E65126231,
	SwordAnimation__ctor_mF92C7F59AC5E0DDAB1AA75EF41E1010B32D23143,
	HandPoseDefinitions__ctor_m5F7626E8103E354315DC7D044D49C82AD5B57D3E,
	AutoPoser_get_CollisionPose_mFD7CEBC5C26472A5CAE7DF6BE545260F5D0A88FC,
	AutoPoser_get_CollisionDetected_m76D17038180FC3E2D4213FE71ACAB8B9EFF36E55,
	AutoPoser_Start_m2E28E95650BC124EB9D7931791BE2EE6863920B6,
	AutoPoser_OnEnable_m07EAE503AA2E612BA41987692E771192B8D9462C,
	AutoPoser_Update_m97880FCD52B42261E35E5F83587DD4100D8750BA,
	AutoPoser_UpdateAutoPose_m7C1F7931694B4C0BC8AFBDAE2ECC9BC443BAC73D,
	AutoPoser_UpdateAutoPoseOnce_mE331050A984A06921D5A5C75E154F98636CFB4F8,
	AutoPoser_updateAutoPoseRoutine_mA568AFF75852EABC143310F452A324D39717EA76,
	AutoPoser_GetAutoPose_mC49251C2895DD178E3A0EC4D5CFC89A7F687D7F3,
	AutoPoser_CopyHandDefinition_mD138424B8DC3C157CDFF362ECC7F00325699B64F,
	AutoPoser_GetJointCopy_m32A3FC2E1F082832CF33952CACEDB6A1E7CEB1BF,
	AutoPoser_GetJointsCopy_mD188028C70433B512BAEF56DD4A009E627D76B04,
	AutoPoser_GetThumbHit_mA165431EBAAB69A7437A6D4C2EC35143E574ABD1,
	AutoPoser_GetIndexHit_m787A56FDC1C5D93EFFC067EA62E8C44836A8E7D0,
	AutoPoser_GetMiddleHit_m749DFDA04D272FAF895B4CBD33F5A771DADB7153,
	AutoPoser_GetRingHit_m4315BC9CAFD0594E5861E9CC454653B7D8B6CE4A,
	AutoPoser_GetPinkyHit_mF7BD80119D14A688CA69448E537C83B8810EDFEC,
	AutoPoser_LoopThroughJoints_m0731B3269A6A93D9295D20D6788ECF67AD823662,
	AutoPoser_IsValidCollision_mEA38253B10A562E2F811881AE419B599CB0B5AAD,
	AutoPoser_OnDrawGizmos_mB6FAC34D4D160AAF00179C159EAFB62053DD9C65,
	AutoPoser_DrawJointGizmo_m2272814DA7D218E2AB05EE278389AB25DBF7E274,
	AutoPoser__ctor_mB858FC79D1DCD13D16FDD6C999FA31C5907E4C0E,
	U3CupdateAutoPoseRoutineU3Ed__33__ctor_mAD8A88603C59CF96DB30FC6E119E824F9084E39A,
	U3CupdateAutoPoseRoutineU3Ed__33_System_IDisposable_Dispose_m0395046D9F98E17B064A10F1AABA37F3AC73659B,
	U3CupdateAutoPoseRoutineU3Ed__33_MoveNext_m311AFDB791EC1D8DA200C36E1F3E5F36ED298B71,
	U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17C7AC941F42D773378C8ABDB9293529E40042E0,
	U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_Reset_m8B6E046C21B47A3A95C74D2F6BAC596F9D029CE8,
	U3CupdateAutoPoseRoutineU3Ed__33_System_Collections_IEnumerator_get_Current_m3297388C60BA9B5948CF0248A00BF1DEBC7A789A,
	BoneMapping_Update_m262A5EB1ABF18C300259EE01D3825A6C3C3566AF,
	BoneMapping_OnDrawGizmos_mD7C23755C243C10085499CA7E571B8D3A85FC3A9,
	BoneMapping__ctor_mD81099307ADD9D81FD3C4C201E0AC92804063188,
	BoneObject__ctor_m3BBE42EC01D28044BB7BA83A4AA6FDE45EAC7C7F,
	EditorHandle__ctor_m243E8ED26997679667C4EF20744FECD79E827887,
	FingerJoint__ctor_m7C5DFEA8731BEEDE399163380C530900681DD271,
	FingerTipCollider__ctor_m5F2805B4BE9C1709172F9F39351F7AE6E1ED278B,
	HandPose__ctor_m27023493DEE451197034ECF17135BF050E03C57D,
	HandPoseDefinition__ctor_m4FE4530ED633AFA30C46B0AF444A07CDD673448F,
	HandPoser_get_HandPoseJoints_mFF4BA9CB1DD1BEB64DCDD2302881CEFA6033194F,
	HandPoser_Start_mD2A1DD6D58AAD36876B0BCF68F7DAA483D5D1812,
	HandPoser_Update_mB1413A3CD7A40D7EFD67CDDA6FFDCF2300E57842,
	HandPoser_CheckForPoseChange_m23B022D36A72D4AD70CC0E7B912645A0506216A9,
	HandPoser_OnPoseChanged_m1F8824EF7EA8CC831A3D242EC22FE70935C4E798,
	HandPoser_GetWristJoint_m79236962D3CA973430FBA5C7C8E6108C45667FCF,
	HandPoser_GetThumbJoints_m80E981DD695196885BE6474F6B1E3BF17241644E,
	HandPoser_GetIndexJoints_m30055F79008F819D787D9708A7C94F3E66503B9E,
	HandPoser_GetMiddleJoints_mB4BF924D4B0DB49DCF5266AAEB8596402B6149F9,
	HandPoser_GetRingJoints_m258285FF09BFF6A7CBF202AAAADBBB1662908B00,
	HandPoser_GetPinkyJoints_mF0534631B99F329FCD8098B1A4DE2BE95B564014,
	HandPoser_GetOtherJoints_mBE74E98EFE03A5FABA162BF7132ECE50174FADAF,
	HandPoser_GetTip_m2C746D611E6D9B51D977960391F7E3D9440E37E0,
	HandPoser_GetThumbTip_mBB0CC092BBB8FAAD637A65381F4A117EDB38C7AF,
	HandPoser_GetIndexTip_mD08B86F82BE63F0C805BADDC2A51C151E75A5FF2,
	HandPoser_GetMiddleTip_mDF0BAB0D869336B7816B07D4B49EDA79DA2416EC,
	HandPoser_GetRingTip_m755A492BDC5A42ED426019462D6D66E6C121DC1C,
	HandPoser_GetPinkyTip_m8261E2DFF930A4868663D9E85FDBAD35E692A968,
	HandPoser_GetFingerTipPositionWithOffset_m240B26211B19ED7B001645EE349B35E67F1AC7CE,
	HandPoser_GetJointsFromTransforms_mCF222AC89736CA720D66939BB3CE7AF69B0E5902,
	HandPoser_GetJointFromTransform_mE8BE23DB2CD68AC9E5A70E13A51E6BD4B6B467AE,
	HandPoser_UpdateHandPose_m769FFD5987253D4A49A7DC5B308CB0114D9CDBB3,
	HandPoser_UpdateJoint_m2F353A515F1EEA080BDADF8DC7836AD54654BE6C,
	HandPoser_UpdateJoints_m716D0080DD20E6A5C155E0219656712113EBA1FC,
	HandPoser_GetHandPoseDefinition_m6D98794EA288C14A65E8D46ED00DED79ABA25ABC,
	HandPoser_SavePoseAsScriptablObject_m294F471982D20BBADC8B19FAE02181A19AD0DE9E,
	HandPoser_CreateUniquePose_m39C74C01787DEB5D523614C5775D8B8528A677DD,
	HandPoser_GetHandPoseScriptableObject_mBA80CD5D9AE936F0A0581F4F97E1D1B2D51DD1E6,
	HandPoser_DoPoseUpdate_mB759A824D26D65C6060C7D673F8E163BAD2C1BC1,
	HandPoser_ResetEditorHandles_mD86EE4E9B9E0701D1320D200F36FD2D5AA1566D7,
	HandPoser_OnDrawGizmos_m7350BA582A8FAABF28E1FB8A370A60737D72DB2C,
	HandPoser__ctor_m0226DE25A0034E85FEE8A702A4A20AA736CA76AE,
	SkeletonVisualizer_OnApplicationQuit_m8DEF88D1FEC64014FC20EC3473F945DF93AB1941,
	SkeletonVisualizer_OnDestroy_m6BA90A48FB063156329CC12ECD7F690D86E51BDD,
	SkeletonVisualizer_IsTipOfBone_mD8C8D3693D6C081E5C8CDC0B1A5EAEF8A9FB0E0C,
	SkeletonVisualizer_ResetEditorHandles_m580893A2A886BDF9877DDAE9AB6D37F696FE9FBA,
	SkeletonVisualizer__ctor_m2B2F83013081AF7D0448C71AD67CF48187F825E6,
	DemoCube_Start_m313786E00669508DDCEC5F8496A1255F6885203E,
	DemoCube_SetActive_m2CB83F2B7FED427D33CD8322DBC5FAE000A03AF0,
	DemoCube_SetInactive_m5C733DABF7E321A94471FA3CB06EAD43510D89E9,
	DemoCube_SetHovering_mECC5166A9870D62227EADBECE3E49589459662E6,
	DemoCube_ResetHovering_mD2CFBE4466421186349E1EA554564F38395A6C14,
	DemoCube_UpdateMaterial_mD217B75914DA2D93FCF48B8546AAD628EF1B5CFA,
	DemoCube__ctor_m0A65014D4B3AB8D7ED3276CA1C6F0CEC0F019D56,
	DemoScript_Start_m6086A23CE32B7BBB8EC92184AD8C3B406249A544,
	DemoScript_Update_mB801A2C9FF309EB7191C0FD6A0E19170A3918FB5,
	DemoScript_UpdateSliderText_m16E308DC4F6BBC004FEC981824C05265F8B2E81C,
	DemoScript_UpdateJoystickText_m84A465C5613699B0B8201FB184B58A4C75E49E64,
	DemoScript_ResetGrabbables_m08DD75495CC88A96167154DF1B6A467244436F95,
	DemoScript_GrabAmmo_m2E5D68E61645AD6A61DAD0E0974024AB09E139A8,
	DemoScript_ShootLauncher_mC5F844768B7311DBCFD80A33CC5C988F39E18E09,
	DemoScript_initGravityCubes_m86C6C7943E08631271F7792DBA461453AA1F5A36,
	DemoScript_rotateGravityCubes_mF007314B2F0447795EE5A1C8AE7B47E2EB21434A,
	DemoScript__ctor_mB4444BF3CF1ACF39A6840CC3451743A87775D7D4,
	PosRot__ctor_m2B0C6FFE427D3B17A2539E3E25928EC17774BA5B,
	CharacterConstraint_Awake_m7C6C3568C74B35FBDDEEA74FA973B48763864B28,
	CharacterConstraint_Update_mFF296DDA0A335CB1CD9EA20BD49C0453FE7C62B6,
	CharacterConstraint_CheckCharacterCollisionMove_m384C622B943355759BA2EF867B50FF4086C8CEFF,
	CharacterConstraint__ctor_m80E38C975DB824973B6B37EDC4877E121A6C40D7,
	CharacterIK_Start_mDB9B36E187B68B36904E56F3D7B0EEB5FEA89C80,
	CharacterIK_Update_m45C111F5634216F35CCB8B3D91B6A277DDAB4EFC,
	CharacterIK_OnAnimatorIK_mC5E2E712264877D00280CDF79AC4D84EF7A91CD1,
	CharacterIK__ctor_m3825AC98059EAFACED0118FF2FA39BF31C501215,
	CharacterYOffset_LateUpdate_mC861A3D20C9B3FB556CF3562AED584470C16E49A,
	CharacterYOffset__ctor_m493A4601749D1B934C0783F0FAC4EDA03E4920A7,
	Climbable_Start_mCD2823B417C51EB85186DDDE9F8DE5F011F496F7,
	Climbable_GrabItem_mE1E573204754708E90B5BFEF23598F4E4C7B6325,
	Climbable_DropItem_mE6B7692CE7ECFEBFDACA59A252966030EB1C9615,
	Climbable__ctor_mD6CDB5061FE37E248994811638930404452A7F4E,
	CollisionSound_Start_mEB3F0F8BB2665E1152682C2A84A1C9C6FE543792,
	CollisionSound_OnCollisionEnter_mD426770D458A0C4161EA49FAF538D65D97565D3C,
	CollisionSound_resetLastPlayedSound_m3E7A945045A9196E2EBA975643E16CEB9212ADDB,
	CollisionSound__ctor_m779A5A39DD793BB0BFD8013BAF8032C55269E06E,
	ConstrainLocalPosition_Update_mA68B159667C818C087A19996FC4A42E356275CFA,
	ConstrainLocalPosition_doConstrain_m2C55CAA127CA23581C36DE52142F57F789E3A0BB,
	ConstrainLocalPosition__ctor_mC9EDAC4AD6994C5B59AF1C7C88D1DB312AB6AC7B,
	Damageable_Start_mDDBFEEEA339690F430A65798C42704CF87BB9B0E,
	Damageable_DealDamage_mB1ED4BE3B744F38C5E5E68C08B7F1C40A72F36A1,
	Damageable_DealDamage_m0FE0B42AFAB7A234BC3DE1C14813C89F00E8D818,
	Damageable_DestroyThis_m68C850E89EC69A2D42C800804A6757865EE8FE3B,
	Damageable_RespawnRoutine_m78721F293EA64A14BC1F33C41FB2AFC6C0320EEC,
	Damageable__ctor_m38FF94C20302CA97C5515231E56F267255056F90,
	U3CRespawnRoutineU3Ed__22__ctor_m4A5B5E34C2F9EAF1CE215064BCC9FC3CFABCC2A1,
	U3CRespawnRoutineU3Ed__22_System_IDisposable_Dispose_m1D9DF8E34DEE7A3450A0CCC830AD93B578A3784D,
	U3CRespawnRoutineU3Ed__22_MoveNext_m35CC953F738999C8DF50A5B6B0019B14AB839214,
	U3CRespawnRoutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD29EAE95323403A13217BF4204D82AC2D6D64B1E,
	U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_Reset_mE3E4292F55A1F6AF6248602B5DEE96E3D0D4775D,
	U3CRespawnRoutineU3Ed__22_System_Collections_IEnumerator_get_Current_m54DDFF628D211B2D245A5498C5300C6FE84B88BC,
	DamageCollider_Start_mA062404FA3F5421B29084E4C51F53E95D3362724,
	DamageCollider_OnCollisionEnter_m8E98F9B9E1E59E2A7AE217EE9A29D2C93464B396,
	DamageCollider_OnCollisionEvent_mA4D571674AB89DA471C8FA222F85B7CB9C2F299F,
	DamageCollider__ctor_m1B3D697CB98E5DCB5F8BDDFF6B2A7295964EE361,
	DestroyObjectWithDelay_Start_m1D8CAE2D18ABF5162D6DE075E60F3E87F95CA385,
	DestroyObjectWithDelay__ctor_m14F8A04ED3281EAD3EF016993869903AC477C23D,
	FollowRigidbody_Start_m213EDF53A0A66BED921F404806AB868BB358642E,
	FollowRigidbody_FixedUpdate_mD5CFA6B1C9B17184E4FB63558041B4362D276622,
	FollowRigidbody__ctor_m998ED9260CE6B74AF881B59E48836D87313705C1,
	FollowTransform_Update_m34E8C4C8D774820597029A9F6450AEF14DFD507F,
	FollowTransform__ctor_m033AEC8C823F4F0C42DD06AC1605D0C1C9D993BD,
	GrabAction_OnGrab_m6DB40FFF2F909945AE0CB8A5AFF9E5F1FA1381C6,
	GrabAction__ctor_m0FA35E5F134F677C075E16157C3E340774E29496,
	GrabbableBezierLine_Start_mBA5EB14C7B8ADA00382D9FC60C303EA5045009D2,
	GrabbableBezierLine_LateUpdate_m29C1CD4EFBC8231C9806286FB51A542A1DEA3B4E,
	GrabbableBezierLine_OnGrab_m557867C6E4FB2994248CB91A68747128D3FB8941,
	GrabbableBezierLine_OnBecomesClosestGrabbable_mA4C8D69BE0545F0466674A9DD2F843B73261AEF4,
	GrabbableBezierLine_OnNoLongerClosestGrabbable_m56BB8FD91619BE83040079FF85D0C421DDF805B6,
	GrabbableBezierLine_OnBecomesClosestRemoteGrabbable_mFD620FD3D1E347796D4E852F1273066E66B638AB,
	GrabbableBezierLine_OnNoLongerClosestRemoteGrabbable_m110F1275EEB561436D1D8424698B98002DB1B232,
	GrabbableBezierLine_HighlightItem_m6CA30504AA1FC651C2E6A33B255C51FF5E73CA1F,
	GrabbableBezierLine_UnhighlightItem_m19DB822A03D76B6CB83A06735EF26B5631986FD8,
	GrabbableBezierLine_DrawBezierCurve_m31FB9992D949FEAEB0F7B9EA690E145AC2ABE23C,
	GrabbableBezierLine__ctor_m232F783D66D478AE60F2BA2214FE1D204635A39A,
	GrabbableHaptics_OnGrab_m657CC7C1758ADAE62A8DA13DFF7FCB86BC71A051,
	GrabbableHaptics_OnRelease_mA2C8EF8C254EA3DBA6240DD210DFD7CB7EDE6A93,
	GrabbableHaptics_OnBecomesClosestGrabbable_mFAB2A23E2D1624F043D47815B0048A425A1CDCD4,
	GrabbableHaptics_OnBecomesClosestRemoteGrabbable_mF67D0802E835D8155D49289DEE6CBD126799ABF6,
	GrabbableHaptics_doHaptics_mDCDA0424680C71B564582FB0B637C1E3BE4A52B4,
	GrabbableHaptics_OnCollisionEnter_mE64B9EEA00CCBA0B5C3075C0439D3D92DAE40C59,
	GrabbableHaptics__ctor_mE068B84CE38B924CE8CFD7342A905691224C2D39,
	GrabbableHighlight_OnGrab_m1007CF6D05470B73586BA373B3256CBAE13C93B2,
	GrabbableHighlight_OnBecomesClosestGrabbable_m1A95A8178B943D1443B22C83AC5D5477CDB6705F,
	GrabbableHighlight_OnNoLongerClosestGrabbable_m9F648A500ABDAEC06E8F83E63A90EE3FF8590EB4,
	GrabbableHighlight_OnBecomesClosestRemoteGrabbable_mE1015F14C621104BD43DB182BD0ADB4FC31740CE,
	GrabbableHighlight_OnNoLongerClosestRemoteGrabbable_mD7FD1D66D63D0ED0FC525C8D52C8E4BD23F59B01,
	GrabbableHighlight_HighlightItem_mE6F301DFDAEFC849E75769E13B5F437F6E5B731B,
	GrabbableHighlight_UnhighlightItem_m0FF547BF4491BC93757633B3E4CDDEBD3E4F44CA,
	GrabbableHighlight__ctor_m987C4356A00DCCBFC422B7271B5F328BD2EB69A3,
	GrabbableHighlightMaterial_Start_mB5E864253382767E0FE1C2E50A47011C0FC191AF,
	GrabbableHighlightMaterial_OnGrab_mB325CC85EBB8A6E5B25281EF741595E40792AF14,
	GrabbableHighlightMaterial_OnBecomesClosestGrabbable_m0F2BFD390E29160BE704AD98EA1453CAF2195BE0,
	GrabbableHighlightMaterial_OnNoLongerClosestGrabbable_mC5A4C2540873C646806FCBDF7F690DEC2247B8B7,
	GrabbableHighlightMaterial_OnBecomesClosestRemoteGrabbable_m354C395FD96A370645FA997D579F86337EA32F56,
	GrabbableHighlightMaterial_OnNoLongerClosestRemoteGrabbable_m753936C06F20E6D419AF2552B4C2B4EC57B41694,
	GrabbableHighlightMaterial_HighlightItem_mBB397D9A2FD8BCEC774239B7468D406833A43C95,
	GrabbableHighlightMaterial_UnhighlightItem_mCDBF9F4FEF83E66C60701C23DDAB3DF421BC6DBC,
	GrabbableHighlightMaterial__ctor_mCA74737F36D4B64DA227D9C48AA55C6DC74E2014,
	GrabbableRingHelper_Start_m89D91650357F443715677D7DBBFC0E42B73C4810,
	GrabbableRingHelper__ctor_m8280A192D2E0CEB3DB8F716E591AA8C82C6058FB,
	GrabPointTrigger_Start_mEC96AA3097CB2240FBC78545983CAA5529CC99BE,
	GrabPointTrigger_Update_m908400E88683C44F04F22AA5A6AB111BD40A1077,
	GrabPointTrigger_UpdateGrabPoint_m6EAE83D2266F823610ED1D4DF643968C4C485DE6,
	GrabPointTrigger_OnTriggerEnter_m31C6E967847EB13139FD9D3641A53B14176FF5B5,
	GrabPointTrigger_OnTriggerExit_m743CFC285B50019F37518AAAA24F2B79F8992B0B,
	GrabPointTrigger_setGrabber_m47B41C158C6316EB8C6B2D45C0FBB58690C9ECF5,
	GrabPointTrigger_ReleaseGrabber_m8306CB460C423DB77E80F8BC162CDC5CB7218D14,
	GrabPointTrigger__ctor_m0471170C6C07C9739F91FD4BCC60D9429654F9AE,
	LineToTransform_Start_m8286C2D8B6FAFC3E08A8B099207B46AB0BE79064,
	LineToTransform_LateUpdate_m6459380B1536ED84621C708544CDEB544FE30452,
	LineToTransform_UpdateLine_m3D9C76EDBC4469F81E9CB62846EB2131932C8723,
	LineToTransform_OnDrawGizmos_m18EC7BDE763DAA6F62AA4EF494EE38CE9FEC9B97,
	LineToTransform__ctor_m1E6F978DFC78C2B31CEC9946FD03E5786CCECCD8,
	LookAtTransform_Update_m4FFCB9239285D9AE9C39151499C1AF77A959D7DF,
	LookAtTransform_LateUpdate_m396F34BE195131626C5FD3D972CE854A1AB71D10,
	LookAtTransform_lookAt_m4A9F640EEFCF15C4C3A5755BFAD2C8D3A400A5CD,
	LookAtTransform__ctor_m156B84A52A633C83347FE1212706947B7F2C1B9D,
	PlaySoundOnGrab_OnGrab_m620659C1A6A8A9D2D241D8B426A4D1170D6F9C1D,
	PlaySoundOnGrab__ctor_m9C859F36210B3A0723BAE0D5697560C6A8A71524,
	PunctureCollider_Start_mAA9A5474461BA993284C8C15774D7BDFC02E31F6,
	PunctureCollider_FixedUpdate_mB32D20C3C71707D8755443702560845B3E0E1883,
	PunctureCollider_UpdatePunctureValue_m5B83D98DF912194DA12D41F9E429345DC2EC1C83,
	PunctureCollider_MovePunctureUp_m33ABDFA2DFB980BD526E3123A38DB564A49752F8,
	PunctureCollider_MovePunctureDown_mB1558A30E372466E5898C6700B6A6664E6C337CA,
	PunctureCollider_CheckBreakDistance_mEF21698DD5FD5AFF1D618628011B97A0FA638D51,
	PunctureCollider_CheckPunctureRelease_m25CEDA062B8F873F5CE04DDA37E53523A59878AD,
	PunctureCollider_AdjustJointMass_mE6F4BCEA30552A6CA2179E500FE6534781F5158B,
	PunctureCollider_ApplyResistanceForce_m80FC1F8941C0403CB84BB3735B9D8CA7AEC3A8A2,
	PunctureCollider_DoPuncture_m757BB2B1A4A6845FCA7F61B5A4FD34AC06458115,
	PunctureCollider_SetPenetration_m04968869D12F3F4CE8BFF599B68B2798EBC42E26,
	PunctureCollider_ReleasePuncture_mCA96E2D7FB9F919026A2C1091F1E0C52DD961880,
	PunctureCollider_CanPunctureObject_mA892F4D38F17822D455EDEC5F93660AD637288AB,
	PunctureCollider_OnCollisionEnter_m048DCC12B182C72D2B75E63F6D1E57E60ABEC62C,
	PunctureCollider__ctor_m34FDCED85315549A0348DABAAB1AC1DC36980DD9,
	ReturnToSnapZone_Start_m684139D3B2BB571D5486DCCA1CDD0BFB095FD5A5,
	ReturnToSnapZone_Update_m08CAAA0D8698A3E45DF081DE21577FA98212320B,
	ReturnToSnapZone_moveToSnapZone_m5E3761692D8A728E7D491B9F1FCD7F99A779C753,
	ReturnToSnapZone__ctor_m57B90DA376011F73D778E732C4D785FE3463FF40,
	RotateTowards_Update_m4BA61079B9006BC99C91F0C039CCCFB420AE83CB,
	RotateTowards__ctor_m386D3EEDB7ED85012D955727A2BB726BF0638C49,
	RotateWithHMD_Start_m09F20AC0C5C98B97D3EEDB8642BA9ADC6091F576,
	RotateWithHMD_LateUpdate_mA89BE3ED774F9558A7A5B66179F273D3F2250F75,
	RotateWithHMD_UpdatePosition_m0B512FDBC70EA87102AB1A3C1A79911E6619918D,
	RotateWithHMD__ctor_m723FACC9F67608168090DF84852BC197697C8472,
	ScaleBetweenPoints_Update_mF3178776C587EBEFE1464CC9BC4B4926AA1EE50D,
	ScaleBetweenPoints_LateUpdate_mE9C3F17E522BC41C3A72CA5F29BE7D7F88D8600B,
	ScaleBetweenPoints_doScale_m473720A6EE5C5B0FAF50117244F9E43E2B4EEDF8,
	ScaleBetweenPoints__ctor_mC077DFB6E85BFD134EA0DBDBE3C716B2A3243A04,
	ScreenFader_Awake_mFBEA23FF00BFB040351ED4F86380E33D9A7D88A2,
	ScreenFader_initialize_m431121577A642714C65D9096615B0EAC5037F170,
	ScreenFader_OnEnable_m6A362F2D0E13B0D58F63521A805BADC766B372E4,
	ScreenFader_OnDisable_mDEA5F108C9D216D6D0F9A4AE9010B6ED5C126398,
	ScreenFader_OnSceneLoaded_m95DDFAB82D3F2FDF756C6B040BC1F7B3E98E79C8,
	ScreenFader_fadeOutWithDelay_m53951A18F2152E8B1E0F1951A2523DAB2A756D20,
	ScreenFader_DoFadeIn_mC07232114D4939255F1D654402DCF6C279302723,
	ScreenFader_DoFadeOut_m43AD39BE869DEE70983EF4B39FC0151A2EF01FA7,
	ScreenFader_SetFadeLevel_m042672C736D5015E3E4A97D249C96F61D0AF4F93,
	ScreenFader_doFade_m3E285F49F89908024BDAE0F053AFB57961A506A8,
	ScreenFader_updateImageAlpha_mA5F6B6D255BDE8ECEA3CF27C64AD3CB0E9EC3AAE,
	ScreenFader__ctor_m89793487BB8F1D2AB01A65C56993AB7BAADFDAED,
	U3CfadeOutWithDelayU3Ed__17__ctor_m0783290B8F5D44EA9845212C9310F78765D4E9E4,
	U3CfadeOutWithDelayU3Ed__17_System_IDisposable_Dispose_mD9137679F58B4DD95181EEA4FDDEDCACDC553900,
	U3CfadeOutWithDelayU3Ed__17_MoveNext_m56EAF744D3A1841F297CDB7392893CA1E50319D6,
	U3CfadeOutWithDelayU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m097EC0B0E7F7C1BD449AE4A65A2D0EBF7DC4C724,
	U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_Reset_mDEE33FD796CFC26C9FB4F8682E7B86568959C357,
	U3CfadeOutWithDelayU3Ed__17_System_Collections_IEnumerator_get_Current_mBB5F6F8948BFD52938304D68962FBC5EB95ACCD2,
	U3CdoFadeU3Ed__21__ctor_m0EF25937570F9BFE4B01F9E86C0E3C759A1B3915,
	U3CdoFadeU3Ed__21_System_IDisposable_Dispose_m1134553ECACC5F99C96D0C23C7BE4CC071354522,
	U3CdoFadeU3Ed__21_MoveNext_m49CF2CFFBFCF58DE026E503C4C731EB6B41FC399,
	U3CdoFadeU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B7FCDF797CE5D2C923EEA7964B24E79B39C7910,
	U3CdoFadeU3Ed__21_System_Collections_IEnumerator_Reset_mECF69CF3CB1269B1834EAAD41D9F690E7D188F20,
	U3CdoFadeU3Ed__21_System_Collections_IEnumerator_get_Current_m678822C431A5AE4415DF5C2FE0FCF7104467F101,
	SnapZoneRingHelper_Start_mB1DD77D8E8019698CF71C26F0F2A7AAF0E67F1D2,
	SnapZoneRingHelper_Update_m61176AA35113D533B379E9426CBC0AF6AA39263C,
	SnapZoneRingHelper_checkIsValidSnap_mC107FF00CE560244A4AEE1A3654D0E7BFFD128BD,
	SnapZoneRingHelper__ctor_mC3AFC5A0A2188776A8CCAC63EA71B98A7B3C1205,
	Tooltip_Start_mDA858ED921604E444B5FA5B0B6B2174B2598B495,
	Tooltip_Update_mC93EA8B2F37017C93BD91D5AE9F6DEDD01CB34D9,
	Tooltip_UpdateTooltipPosition_m383A805D317F65CE7C55FA6ED04F893D265B7C53,
	Tooltip__ctor_m221DA5EA4042FA4485400C3531A13CB4222C8882,
	VelocityTracker_Start_mCB78D075052557F3A40569B485C0E14D8CDFA6F2,
	VelocityTracker_FixedUpdate_mFC66EA58877D11CCF5D94F9039E923E4BF2BD164,
	VelocityTracker_UpdateVelocities_m322E72CC27DA11D596DA4FB587515007D6506392,
	VelocityTracker_UpdateVelocity_m862DDDED1F081EA8B79B007D3A3796D5DAA7BB96,
	VelocityTracker_UpdateAngularVelocity_mBA86A767624FCDAC8271216028EC11E8BA8F9F53,
	VelocityTracker_GetVelocity_m7A8692F37D0E9A97BF083493054C56AFC0328600,
	VelocityTracker_GetAveragedVelocity_mAF452490E36912E0EC202E4914D82EAE562E797F,
	VelocityTracker_GetAngularVelocity_m085429A90A9EB6C446461D10BB94F928074D91AF,
	VelocityTracker_GetAveragedAngularVelocity_m88ED73DA87BC7263E7DC9CFB5E36A343CC44BEA0,
	VelocityTracker_GetAveragedVector_mE85850F479A56C5E9B804D10C257DFCA50FEA386,
	VelocityTracker__ctor_mB68141CBCD3E0689FC1EACC01AC3D5149BEB93B1,
	BNGPlayerController_Start_m6EDBDD17752CCD6B2C5248E5135F49B2CE69905B,
	BNGPlayerController_Update_m20473CB812FE8FDAD5FFB3662056FBAA15910C4E,
	BNGPlayerController_FixedUpdate_mB1EB011C83E5752645F0573F7B6E10D15A13456C,
	BNGPlayerController_CheckPlayerElevationRespawn_m2A9597AB63A47FE58D3BD47070A180BA8B1924F3,
	BNGPlayerController_UpdateDistanceFromGround_m578B1D5175D1FF660940B7FD8A091207CB26C1C9,
	BNGPlayerController_RotateTrackingSpaceToCamera_m495686B2227222E0CB5DF751BCB3D9546BFBD6ED,
	BNGPlayerController_UpdateCameraRigPosition_m56A6E6A93A4FB974AC3B4B2DF46561108523C375,
	BNGPlayerController_UpdateCharacterHeight_mE20AEDBFA50913B3112761F4EA02B6BD6A0567A4,
	BNGPlayerController_UpdateCameraHeight_m22E91B9FFBDF0C5DBC4844DA207375B4BE771984,
	BNGPlayerController_CheckCharacterCollisionMove_m689BBBDC605822CEF6D49F8C4CB9BEA910BF9340,
	BNGPlayerController_CheckRigidbodyCapsuleMove_mB7E084B62CBDB9CC6AD6D7EEE7A47D0ED806EE7B,
	BNGPlayerController_IsGrounded_m5CD2FF88A68DFB712B6CA47599655A038D1A6D7A,
	BNGPlayerController_OnClimbingChange_mBAACC863919E35207DEAAC58E7072DB7CCF96A94,
	BNGPlayerController__ctor_m914FA631CF3FF5B3FE5D1A6376A1405C61ADDF84,
	Button_Start_mA14EEEAB2816FB74E88B9D5ED95D412C8AC0DE1D,
	Button_Update_m6B1D87839FB5B2B5DF9A49C709B05CC13BF10EE7,
	Button_GetButtonUpPosition_m90F904314D0BA76F9C9446579F5AD27A49A07F01,
	Button_GetButtonDownPosition_m3A85D9C7C84B518315EC900C7F6B219A0A2FE4EF,
	Button_OnButtonDown_m73D79A1537F3245E5016D725AF7F61BB4739176C,
	Button_OnButtonUp_m68BDC9B1A9B1BFCBA8CEA00D8754B64B68324BB8,
	Button_OnTriggerEnter_m96231825E1BEBDD4F1A4B1A04F372991D968C990,
	Button_OnTriggerExit_m2D8A06688B9A3071E61ED183A1166DFE646130EE,
	Button_OnDrawGizmosSelected_m32D1D52E748ADE78130A55F939FAD2C5D904A58F,
	Button__ctor_m17AD44130FC0B8C753C07E65730CCCFC04AA390F,
	ControllerModelSelector_OnEnable_m1CEB672F47478EF99E20B9BA7B96F0A1F9D0590D,
	ControllerModelSelector_UpdateControllerModel_m4BC219B9F58619AC59EAD57A2E26737EE1FF0C17,
	ControllerModelSelector_EnableChildController_m0E3B7C085CA3AB2C9088B74FCA257716831CAC68,
	ControllerModelSelector_OnDisable_m62D25AE6B82D7F716C5A27E9B69E91CD663E3B5E,
	ControllerModelSelector_OnApplicationQuit_m16F8BA315E31F3A5FDD3171FB349BF4E14BB4D0E,
	ControllerModelSelector__ctor_m1AC85F7E5617A6416CE1979243D102D70526EB4B,
	Grabbable_get_BeingHeldWithTwoHands_mC18CCBC40B9AC5F770BDEBC2304B575FDEA25AE2,
	Grabbable_get_HeldByGrabbers_mE545511C8BFF53688B9ECF33D8A1B70425F86D6F,
	Grabbable_get_RemoteGrabbing_m57E2C699C07A06750C1DDF062B6BD541A158F8C7,
	Grabbable_get_OriginalScale_m62D7A8D44E23ED0A9C83B1BAFB360A7602B74374,
	Grabbable_set_OriginalScale_mE3925D35C030F8D3F87A0E5816D2EA1EDFF82A84,
	Grabbable_get_lastCollisionSeconds_mA62EC9A7CD525C6B70FC2A2C82697E00FCD6BEBF,
	Grabbable_set_lastCollisionSeconds_m247A4E5CA70448320002CF49E370E66B2C1D80CC,
	Grabbable_get_lastNoCollisionSeconds_m7DA91F99CE1FFF88AC7626C8E8541B0AEBAFFFD9,
	Grabbable_set_lastNoCollisionSeconds_m83842BABB5A49F0D9C5348A166A2BDBC61B79D05,
	Grabbable_get_RecentlyCollided_m8F523BD48F54A127E4D717F0E1343E5FD991786B,
	Grabbable_get_requestSpringTime_m173B51A48CBB098BC2CBCA0A41275DA005678870,
	Grabbable_set_requestSpringTime_m9893F9325AED1E9A35967B39D4808BCC9F298565,
	Grabbable_get_grabPosition_mFCA90DBABB3F7BA17D81AC722A00750772317C10,
	Grabbable_get_GrabPositionOffset_m335314B6B9C382A0946840A7E045F2DCA44E6180,
	Grabbable_get_GrabRotationOffset_m52679902E615DEED3781E7F06599C3E2094F9CA3,
	Grabbable_get_grabTransform_mC7EC3681D1E1B89F3C7F70F8E012CC6209FB8992,
	Grabbable_get_grabTransformSecondary_m1EACE30F9FB97AD855DBCDC18C2B938CEF69AF4F,
	Grabbable_get_CanBeMoved_mC711A75961F68542A92322BEDF8742BD6E207439,
	Grabbable_get_player_m2A56382D2CF331C0261CC5FB7F8AAF3FAAECB365,
	Grabbable_get_FlyingToGrabber_mCD936AAE88112B52238C5A8934A500BADB45B5EA,
	Grabbable_get_DidParentHands_m5D20F8F41E0376B84A18211BD0EC0CBB1B8E1630,
	Grabbable_Awake_m8613B3E5DE0448B9FA77A424431262896088614C,
	Grabbable_Update_m3DA69708B84A8C900D27D84FFECBBD5E801847D7,
	Grabbable_FixedUpdate_m544B6AA86BA5688CFDF4A7CBA2AFD0E028E6B50B,
	Grabbable_GetGrabberWithGrabPointOffset_mFF0B39CEB08A3594AADD3386510901191580A497,
	Grabbable_GetGrabberWithOffsetWorldRotation_m7C402D851DD472E2B2AB4388002B548E6B3A0E44,
	Grabbable_positionHandGraphics_m443B79E6F8C82F1702443423B9F87CB22B7B3C7B,
	Grabbable_IsGrabbable_m8DCFCA00877BA811D7C3712137D24A72AEF00E86,
	Grabbable_UpdateRemoteGrab_m4319AA4E71F6C191A9343A8EEC17E0723A798F49,
	Grabbable_CheckRemoteGrabLinear_m1A97B016176E6815E60F7289D6525418816E7611,
	Grabbable_CheckRemoteGrabVelocity_m7A0E4917A8B5533E974DE228586017BE0A70C69C,
	Grabbable_InitiateFlick_mE654EF5AD24B11FC8BF96DC80E50E9D568C75124,
	Grabbable_GetVelocityToHitTargetByTime_m369C6FB2E90B62DF8D3DD6423B98F77E36A74EE0,
	Grabbable_CheckRemoteGrabFlick_m1144E6D457DA70D14987D666FB4F74938B4E466D,
	Grabbable_UpdateFixedJoints_mAFA24656A763CA61C7A14538F7167513C293F4CC,
	Grabbable_UpdatePhysicsJoints_m378607DF0E6AF2C1ABE277548004D467E3716C4A,
	Grabbable_setPositionSpring_mC78CDEBC62E6DA40CF754580A790DA3EA89659E5,
	Grabbable_setSlerpDrive_m6DBB17C5883C5443CE2EA52212756AC327B432DC,
	Grabbable_GetGrabberVector3_mE3681753AE82CCB5892999850BD4BED9CC104209,
	Grabbable_GetGrabberQuaternion_mBAF84AB737154C95B3A17E7F9D8248AAE1F4D99F,
	Grabbable_moveWithVelocity_mE2F33FD8767EBF2D4CA2F4C7CC217933EF30804D,
	Grabbable_rotateWithVelocity_mE0EED11ABE78A8DD7580215B157279F39BFAEF41,
	Grabbable_GetGrabbersAveragedPosition_mE32868EFC544B300510958968312BC571D48EAF2,
	Grabbable_GetGrabbersAveragedRotation_m912C277D9F6A1783088F03E6468EF19B2BC3A115,
	Grabbable_UpdateKinematicPhysics_mDCD7C9C19A83E73901E5B6FFFB779CDDED041AB0,
	Grabbable_UpdateVelocityPhysics_mD3ACAAD7CAE06AFBDDF50BFA8DF5530D61FEEE39,
	Grabbable_checkParentHands_m1FA00F63137ACABB60E905B63CF3674563B2304F,
	Grabbable_canBeMoved_m9166115CBD421F06FDD6F0AE99D42F4DFA8434F5,
	Grabbable_checkSecondaryLook_mBB63E80172DDEF59FADA6D75D35D507F69107095,
	Grabbable_rotateGrabber_m8DE0E7671A4FB60E4223F7ED6606F05754DCC5B7,
	Grabbable_GetGrabPoint_m9430BEBA2EE684508B2F0C21E02FB5641A958535,
	Grabbable_GrabItem_m23150BBF0F3A40C53389D9768745D4C3C3026EB5,
	Grabbable_setupConfigJointGrab_mFA2DFD0D954A1AFD469880CDA4DF76D53C2F6157,
	Grabbable_setupFixedJointGrab_m814B344CA1E8801AF9F294AB8C993F1220AC88D7,
	Grabbable_setupKinematicGrab_m29834371E98B9CC5E1C704CD725718BD186D9EE1,
	Grabbable_setupVelocityGrab_mF7C687CE0BF79B495FF1AF4E52F08B8BD46B2076,
	Grabbable_GrabRemoteItem_m4BA3581762428C7AD2F632D832A5ECE0DC24B8CA,
	Grabbable_ResetGrabbing_mFD0B9740C847D32312376AD2704FEA56155C1CA3,
	Grabbable_DropItem_m0F47D9C7A603DA97F801A86512C14B87E00F1F08,
	Grabbable_clearLookAtTransform_m0387FDD6740102691EFA1DF230A26BE995BB412F,
	Grabbable_callEvents_mD8E194F7ACAF616B7CAA5C419C5E0BB26B897FF6,
	Grabbable_DropItem_m384A9F9A279E21DCB1F36F0F70C48CF81B0409C6,
	Grabbable_DropItem_m355347FAD6DC4B575179DDB966C58AF3DBEAD5F7,
	Grabbable_ResetScale_m21E155F7F720AFEBD9D0BAF851E89A6D5B4B5494,
	Grabbable_ResetParent_mF0A8EE1688D6F46DC78DEF921E798BF8B6595304,
	Grabbable_UpdateOriginalParent_m93401AED4044FF34834C363FB1421EF3779BAAB0,
	Grabbable_UpdateOriginalParent_m4F41DB593626979EDF61900CCF8467C12401B0AF,
	Grabbable_GetControllerHand_mB4866ABE69BFB3609C49CC659A43273CA26B801B,
	Grabbable_GetPrimaryGrabber_mF6744E89E67311D08D9E681D9B2B44037AA32C4D,
	Grabbable_GetClosestGrabber_m18B4C9D309A64435E1725DB26D4423F7D2571BBE,
	Grabbable_GetClosestGrabPoint_m605E6CB6ABE609C29225A591CD05140FDEAD8929,
	Grabbable_Release_m30C12E3213524FF6A2DF25CB4885135C5878A112,
	Grabbable_IsValidCollision_mBB75D1F99676A97A4DF5D7741728ED8594F299C0,
	Grabbable_IsValidCollision_m1AB863644F9D2C3B60A7C13A52E46A4DAA47E676,
	Grabbable_parentHandGraphics_mF05CA45822C4BED94076E3B55604C8833509E912,
	Grabbable_setupConfigJoint_m2F66E76C8E7352711A8BE0D80EF4D8FDB42E5D22,
	Grabbable_removeConfigJoint_m99EF5F30E4384EB745E6FC2777537A48BAF3811F,
	Grabbable_addGrabber_mA7B27DD37C5255C77CC13765F6F7191D97B8FF91,
	Grabbable_removeGrabber_m3AB1DE4F60BB064930FA3F0A7E9D82CEA7B5B79C,
	Grabbable_movePosition_m8606A65DA48C20B1C9CDE33814591BE227CA6F90,
	Grabbable_moveRotation_mBB552B728A898A5650A9A3B7C817415F601F6EBF,
	Grabbable_getRemotePosition_m117ABD661B5ACC76386DA16E6E894F240082CC19,
	Grabbable_getRemoteRotation_m40378F0BF5A43AB4171851E03F84E5A348771F6C,
	Grabbable_filterCollisions_mE9F39A1C1FBC8BB2C3E113B4DB51FDFD8104B374,
	Grabbable_GetBNGPlayerController_mD601344D1822FA255A88656594A0558C16E3FED6,
	Grabbable_RequestSpringTime_mC449467620555F5B22740FE1EF9C8F30717CFBBD,
	Grabbable_AddValidGrabber_m93D4B048D299E1CA250A356B5E8C53AD05A871FA,
	Grabbable_RemoveValidGrabber_mE5313EAA945177A039E02417D8E1C7342542C491,
	Grabbable_SubscribeToMoveEvents_m3737ABD04A901900C6F83809221143B063E0F8CF,
	Grabbable_UnsubscribeFromMoveEvents_mE71F12FA5609AEDA8292A65A058033B6FB1D1CB5,
	Grabbable_LockGrabbable_mC96CCD4767739F5E7E4A7EA401B97496087E5646,
	Grabbable_LockGrabbableWithRotation_m6E85F925DE19FFA4D932732717FDBAD885524B72,
	Grabbable_RequestLockGrabbable_mB7481FDB608BB75A0BDF844FDA8CEFC79805D776,
	Grabbable_RequestUnlockGrabbable_mBC04E1DFC1574BA3374889EB192DD69C8B28D42D,
	Grabbable_ResetLockResets_m916C34D0310DDF1AFECF721C0C18AA519F87F39C,
	Grabbable_LockGrabbable_m45DE61A4332033199D3467ABB78C00EA359FDB42,
	Grabbable_UnlockGrabbable_mAF8A92DCAE1B1906DC8FE92592815623D39C13A9,
	Grabbable_OnCollisionStay_m6359C76365E207B2651C63DC6753A911FAA14FEF,
	Grabbable_OnCollisionEnter_m1FAF3B24CD8B1D6C979022EEBFEA702706E4C8F2,
	Grabbable_OnCollisionExit_m3E3A47175938D17E6BAE1239468A1C8FE743F973,
	Grabbable_OnApplicationQuit_m6BE9944040ED03B0393FEAB97CFCDF9778773171,
	Grabbable_OnDestroy_m2191279974FB96A50B50C51E7BCEB59D1EA2BDAD,
	Grabbable_OnDrawGizmosSelected_m4F72287E81C37A12B2989FEF6F282FDDC9885A58,
	Grabbable__ctor_mDD5DA95466F9823C086B131AABAE3974F0BDB7D3,
	GrabbableChild__ctor_m10BF1B9419913890AB68C58B4C6E3B4582ED88FD,
	GrabbableEvents_Awake_m9F5618AD8F3966F75E93CA7639F63C9E4E95F309,
	GrabbableEvents_OnGrab_mDF1C020E9A4D31F92A34ADC84C2B0291690407C8,
	GrabbableEvents_OnRelease_m698B868AA5D18F08E617397B5BE529A79C8F024C,
	GrabbableEvents_OnBecomesClosestGrabbable_m47FECAB518361C6100F4F154F2C3574A9D5E3C9F,
	GrabbableEvents_OnBecomesClosestGrabbable_m5857AA36EA45690C32CF062DBAD43E3862F56E8F,
	GrabbableEvents_OnNoLongerClosestGrabbable_mE70BD10C77000C137432ACAFBD2390FF32FC49D4,
	GrabbableEvents_OnNoLongerClosestGrabbable_mC2ACCA1D4822F371F12FBC9B4B6CE91D1136AF2E,
	GrabbableEvents_OnBecomesClosestRemoteGrabbable_mB421DEF316E15D87A52946CDE39F3A5A483E6D85,
	GrabbableEvents_OnBecomesClosestRemoteGrabbable_mC43C260A790DA63F1F4955472490903BDF7A694D,
	GrabbableEvents_OnNoLongerClosestRemoteGrabbable_m0C4E8DBA9655A6C86FB56D321615BFA0F25FB70E,
	GrabbableEvents_OnNoLongerClosestRemoteGrabbable_m5395F8747359099AEDF5101D92DEBEC83B588B3B,
	GrabbableEvents_OnGrip_m78DE466A19AB3F13FE6DA22E452160DA6CC90CAB,
	GrabbableEvents_OnTrigger_m5D42C47CA379E1E8502D7DD01E0C4F8D5C058E56,
	GrabbableEvents_OnTriggerDown_m46C0199A200DB3BD7C6B5984C75DB783AFC26517,
	GrabbableEvents_OnTriggerUp_m6F390D3DA419400E9125EBBECF3D9BAF76876A0C,
	GrabbableEvents_OnButton1_mB9B488C584EF6CE0A01C59520E9756DDC06B513D,
	GrabbableEvents_OnButton1Down_m0050426182DBF5ADE2BABF3129587B8F073F13A9,
	GrabbableEvents_OnButton1Up_mB1FAAC2F28447B04C82E4054F2E279F906C538A6,
	GrabbableEvents_OnButton2_m400115103B7ACA2AE5CD43EAF7C9479A505198DF,
	GrabbableEvents_OnButton2Down_m03F239CE72DE4F42DB6ABC95EF54EC9D7291667F,
	GrabbableEvents_OnButton2Up_m1F459E0C89F43083A36F1F9DB8C2D5CBD3ED17C1,
	GrabbableEvents_OnSnapZoneEnter_m35A896C3EBC854791C0CDDFC448A186EAB309C83,
	GrabbableEvents_OnSnapZoneExit_m1FFB3F76C5E4459069FAD33187C8FC2A3B72184F,
	GrabbableEvents__ctor_mF47EE9A8DE1824F2DC070FE888F6201056419298,
	FloatEvent__ctor_mD92724B159D5101666EA300A0F68A05873B6527E,
	FloatFloatEvent__ctor_m635B39D4D22058C9ED99E337A5C28106EA2B8B5A,
	GrabberEvent__ctor_m4633675758D48B26B0EAD1382E6B53DB553107C7,
	GrabbableEvent__ctor_m51134B6B1BA88079C1F5681E2B03EF0EAE416A5A,
	RaycastHitEvent__ctor_mDEC456179F740D20446E6EA198E87C2A5CA192CC,
	Vector2Event__ctor_mBD14A9AE398A8BFAE8421690F3DDB5EB712C095C,
	Vector3Event__ctor_m1127B8209FFD7D0A973CDD8CFCEC75EF1C3F932D,
	PointerEventDataEvent__ctor_mE0D65709D4A0794036D0C2B1B97B4580C2B158D1,
	GrabbablesInTrigger_Start_m3FCEBEBD82F7E7AC216AEA09E99DE70A1C4D13FF,
	GrabbablesInTrigger_Update_mFF090C29C7519A8AAE48D69B857762B0946F42AD,
	GrabbablesInTrigger_updateClosestGrabbable_mCC7B9AB1923AC3802851DAE51E4BA5B70ACBCC10,
	GrabbablesInTrigger_updateClosestRemoteGrabbables_m7A0E9C7EA63726B95E82561B5444685592E061D3,
	GrabbablesInTrigger_GetClosestGrabbable_m8C03DD9E306F50E2388A3A055AC9B560AB2EC3D4,
	GrabbablesInTrigger_GetValidGrabbables_m9461614902302A9F04A4CFDF0117F351FA35202B,
	GrabbablesInTrigger_isValidGrabbable_m4DD4B8B9975780239B7E7BDBAE488AD7440068E5,
	GrabbablesInTrigger_SanitizeGrabbables_m4417EA2A0D79EC77547BB28EF7D58DCD5F41C12D,
	GrabbablesInTrigger_AddNearbyGrabbable_m5BA1B342F70C7ACB1702F1D839F5429FCA80223F,
	GrabbablesInTrigger_RemoveNearbyGrabbable_m2416DA65AE7FCC9F1F8D35BB80941A1FADB0F9D3,
	GrabbablesInTrigger_RemoveNearbyGrabbable_m84A308414BF1A6EEB5FD23A465A08E7A4247C48F,
	GrabbablesInTrigger_AddValidRemoteGrabbable_mD4AAC2F22CE0433F2AC12CD8AB0BC9F76CDA0051,
	GrabbablesInTrigger_RemoveValidRemoteGrabbable_mB8712ED87647A4F9D30D88A0FD24EAA0760A5FB4,
	GrabbablesInTrigger_OnTriggerEnter_m9AD0BF63351800ECD6370B51529B270524AA8025,
	GrabbablesInTrigger_OnTriggerExit_mAA56D6386A6E9D813DF6DF296A03C342F6207969,
	GrabbablesInTrigger__ctor_m55CD46A49A024F2D12BCE73C8BBE8D5AF2B27ACF,
	GrabbableUnityEvents_OnGrab_m7238A79C74E453D47C46A984EB56322471694E08,
	GrabbableUnityEvents_OnRelease_m601EACA1DC17418A44F9318418B73502B673F82E,
	GrabbableUnityEvents_OnBecomesClosestGrabbable_mF03823B3307C28E89580D13EF5A17CD16940554D,
	GrabbableUnityEvents_OnNoLongerClosestGrabbable_mA05388792BC4B5B638D490EA71F74CB8A42A5457,
	GrabbableUnityEvents_OnBecomesClosestRemoteGrabbable_mF4F5050553C6C7155CECE7F6ED50AB3F3AD95C0B,
	GrabbableUnityEvents_OnNoLongerClosestRemoteGrabbable_m4DAE6BEBBFF6D44142EF5105765AF77BFB3179FF,
	GrabbableUnityEvents_OnGrip_mAA2572A476B835A9F8E91903CD2CCF0074DD0A7C,
	GrabbableUnityEvents_OnTrigger_m66CAA020EB5DFEAFE3018946FD7C972C22718A6E,
	GrabbableUnityEvents_OnTriggerDown_m4F26B42BE20FEAD6F72812A6DB25E9F999461F0F,
	GrabbableUnityEvents_OnTriggerUp_m02B69ADBAE6C1F292F8A441BF2D7EF049FB953AD,
	GrabbableUnityEvents_OnButton1_m07AC70A2D1960FC0B1AA4C92CBB3844FFF76638B,
	GrabbableUnityEvents_OnButton1Down_m34799B35B8B878BFFC3084FBD66631A583CC1A29,
	GrabbableUnityEvents_OnButton1Up_m1AEF1BFC531FB76A6502E6C7BDF6FB7CA0FB2BB7,
	GrabbableUnityEvents_OnButton2_mB6CA75B90B57CCC41515DABA3EF7619075329036,
	GrabbableUnityEvents_OnButton2Down_mE807900C19BC8AE4A3D8110AEEB7D80C8CAA4913,
	GrabbableUnityEvents_OnButton2Up_m443018BCC2793373D9CF643FA3F65CB77A625EE6,
	GrabbableUnityEvents_OnSnapZoneEnter_m17B815D9BB390D7191A580A7FE919A98BB1FDBEF,
	GrabbableUnityEvents_OnSnapZoneExit_m0B787CD68EE44210183215796C6384D508E12141,
	GrabbableUnityEvents__ctor_m469444FB3D8E0C64B5115F33D76393A271A9C4F2,
	Grabber_get_HoldingItem_m0DA4989F8955BE513E91B01587BF156C8B6DBEA3,
	Grabber_get_RemoteGrabbingItem_m28DAB10406322D385C288BE21E3F11E6C146C2A2,
	Grabber_get_GrabsInTrigger_mA64C03645067D8DF722A9E3D2D784692794CAF23,
	Grabber_get_RemoteGrabbingGrabbable_m5959DAD8E93885DFE52C404C724D16C187BE05FE,
	Grabber_get_handsGraphicsGrabberOffset_m2C77CA6C5965FACCDD7A3AA5EE0075E01D122E3C,
	Grabber_set_handsGraphicsGrabberOffset_mF31F3471BF3236A494F5FEB44E807CAE4EB6B4D3,
	Grabber_get_handsGraphicsGrabberOffsetRotation_m4A5D929FB9B1BC22AE0F65125310B7DCF07AE4DD,
	Grabber_set_handsGraphicsGrabberOffsetRotation_m747E203057A64DDF2CF0FBEF13A90E31317544A4,
	Grabber_Start_m684DC290F24114C73D06E5591254713FE5CE6A74,
	Grabber_Update_m6752AE1F01A0AA0C9A14B8DA486A5166CBEACBCC,
	Grabber_updateFreshGrabStatus_mE21C92B5D1ECA9F439AA17BC83B9CEE7C334B31A,
	Grabber_checkGrabbableEvents_m1D2EFD21D8CD6C508C5DBBDC80EE696704EEFBCD,
	Grabber_InputCheckGrab_mAF353C801FA6C8E50530BC97F8D68857172571B3,
	Grabber_GetInputDownForGrabbable_m19F28C060CD4EAA3BAB24ED36CA157335A7BAA35,
	Grabber_getHoldType_mE3D5AE3185E8C447CD6AB0436AB86FB40158F5E3,
	Grabber_GetGrabButton_m3B65A72752AA4C0C7E46AF167BA81CA10A6A382E,
	Grabber_getClosestOrRemote_m2938A5C903F25486053626A8984CB36F7015B3FD,
	Grabber_inputCheckRelease_m1E23B0744934A7F94E62EB802BD51F5D0CB9823D,
	Grabber_getGrabInput_m3FE1A9AE32FE9A25B9AC5FBE92F6545724851ED5,
	Grabber_getToggleInput_m22148348474B18138FEF8F70E65F2ED84BD11819,
	Grabber_TryGrab_m374781F158E1370646BEA336F4173622CDEA5402,
	Grabber_GrabGrabbable_m032064C125E640CBA2E10780B8D0D388918792EC,
	Grabber_DidDrop_m3D8737763E432EB8615F3BA285E5F7ED2EFD7579,
	Grabber_HideHandGraphics_mE422C482325E80ED7CF37B2B3DAD3B72FD0550BF,
	Grabber_ResetHandGraphics_m59C551B05EB5BE037B99943CE71CBD14C55702D9,
	Grabber_TryRelease_m3217B651CE7F54031B3A770D2D39A526A9B8AC7B,
	Grabber_resetFlyingGrabbable_m18AE676E06A2A1EDC2E319A9515A30E93C3C3568,
	Grabber_GetGrabberAveragedVelocity_mB146CA6AB378DB424F53900E56E6F52B68E3850C,
	Grabber_GetGrabberAveragedAngularVelocity_m55F8779AC4C25FC662774813098098DFCBD64147,
	Grabber__ctor_m9C4BEA7B6E56E1A96E1186F0B1514D832A28BA3F,
	GrabPoint_UpdatePreviewTransforms_m7F07C7AA56EBAFCEC4E7EFA40C4C7596CF39FC59,
	GrabPoint_UpdateHandPosePreview_mD4B9F7CBC4BB854CE72D43B754C7CAE26F0580D3,
	GrabPoint_UpdateAutoPoserPreview_m4621AEB68A646D7322A7FBF2111734AD787522D6,
	GrabPoint_UpdateChildAnimators_m481BF02DBE265B03ED365F1CA3956E874F329EDA,
	GrabPoint__ctor_m38804D16AEA4EEDBBE09CECF95A7F5CAE7DC1F7B,
	HandModelSelector_Start_mD8EDDE9C873ED33D6ED83F7354CC089555566E2D,
	HandModelSelector_Update_mAA81420297D421F7507CDA8E017BF15E53818520,
	HandModelSelector_CacheHandModels_m602B5BF04DD6FD4F06F7D955DA1D0169D13326C5,
	HandModelSelector_ChangeHandsModel_mBEBD227C11A42E104E34277A707C050465D664E8,
	HandModelSelector__ctor_m8E24F5EA91FC99C5A0CB590861C390CA3F049876,
	HeadCollisionFade_Start_mC22778EFAFB26879A103FCCD185925D66425F843,
	HeadCollisionFade_LateUpdate_m31086264798C8FC41593DEA71FF7908747293F74,
	HeadCollisionFade_OnCollisionEnter_m65C01DE4F25F92899881FC2A2320B500C3E48707,
	HeadCollisionFade_OnCollisionExit_m24368A5065376C7914EFA7C77B96A2C322790EDF,
	HeadCollisionFade__ctor_m0B21DCE83E4493284529A06D740E4A307EB8C28C,
	HingeHelper_Start_m3A2AAD126115AAC504036ECCBA5B550D55F205A8,
	HingeHelper_Update_m45DB334B8242D80FC778C4593764DE00FB5E4D86,
	HingeHelper_OnSnapChange_m1712E0DDA56A6669524C262A107551B6828B9636,
	HingeHelper_OnRelease_m4408ED7693C96F0E015241586D41DDE18DD60620,
	HingeHelper_OnHingeChange_mBE3B1CB9BF349C8BD0B83134BBF325D7DE75C5AF,
	HingeHelper_getSmoothedValue_m602CD65F8572131C63F85B0B8F0D99EB5D6D1B13,
	HingeHelper__ctor_mEFB877A986D040A9D69992E043A1ACFE31482DC3,
	InputBridge_get_Instance_m71E90D14104F9FBD48661F41B85E6D289634128D,
	InputBridge_get_DownThreshold_m0A12769107BA70D3E1A092EE251EA4B7B43BE533,
	InputBridge_get_LoadedSDK_mA7B15361CE284E7BFDC726F36F7680AF16B095A9,
	InputBridge_set_LoadedSDK_mD3A957E9643B1987DB89DF8B01FCC1B8CA7C3FFC,
	InputBridge_get_IsOculusDevice_mB548727A7ED803DF3D54FD9018498E86620E08BC,
	InputBridge_set_IsOculusDevice_m4E6810E32B1335BD0567F9C510C62F9EEB2ECFF2,
	InputBridge_get_IsOculusQuest_mC6EA6FD0E4F2B9FA1DDEEC8A306C06FD681A1802,
	InputBridge_set_IsOculusQuest_m4C98364A9F557FBDB8E15A450825737674625A5E,
	InputBridge_get_IsHTCDevice_m226ED6AC3933788701F8A6C4CB9704879B22A157,
	InputBridge_set_IsHTCDevice_mE97AA136CA066F7F54C59CD5D7FC1B4CC9FB3066,
	InputBridge_get_IsPicoDevice_mF8983C3C0341E0BA4C615A8356C3E9E15484B57A,
	InputBridge_set_IsPicoDevice_m62297968B8E4F002FB37C1FB8209865BC32EDCE1,
	InputBridge_get_IsValveIndexController_m361F985A424F5F0B2E256311F10112B0515E7578,
	InputBridge_set_IsValveIndexController_m3E18AD4E7352CD921AF7EFEE20BFDC8487F4FB94,
	InputBridge_add_OnInputsUpdated_m47936B75C00042DCE00BA573C9F81593C236D2A3,
	InputBridge_remove_OnInputsUpdated_m1FD0FCE9A53727914562F42B6235D62141175F14,
	InputBridge_add_OnControllerFound_mB2E45D6082E721F037F02CF175850148EF913091,
	InputBridge_remove_OnControllerFound_mA448CD69EB6C68FBDB1AA4A7A5EA1CB6DF1E23DE,
	InputBridge_Awake_mA2D9499A389185B92C561BC9C083B6743535E29D,
	InputBridge_Start_mCA1B8F4AAA3F32C24F4129A6F7DCBFC0DCC057DE,
	InputBridge_OnEnable_mE0090169C1789A8065226D8ECB1847C34D6E24B8,
	InputBridge_OnDisable_mFD2F5D92145A91C970C9FA42BA25F01EC32EE956,
	InputBridge_Update_mE1791849A3908E37C3946C15AEE3924082232A53,
	InputBridge_UpdateInputs_mCFE1CF0F0147793036EDA78FCE445EE47F9A0A2E,
	InputBridge_UpdateSteamInput_m12A08CA2F5269D1D8C70A8B29B62DE6F15B68FFD,
	InputBridge_UpdateXRInput_m1A87D75C6F55A672C368603CFEA4160BC4314C92,
	InputBridge_UpdateUnityInput_m9EC65681307756857CBA54933CDB249E405EFBC4,
	InputBridge_CreateUnityInputActions_m5A8F5241C3641D078500FADCE49C51ACD377BD73,
	InputBridge_EnableActions_m07F2744EF6B4A1235C87364B2248A8D99F497071,
	InputBridge_DisableActions_m7BDD89C2BF319485D30315FD2CB4DCE843E13BD5,
	InputBridge_CreateInputAction_m25683D4A7E07054EEC1440682FB6847A9934CFF8,
	InputBridge_UpdateOVRInput_m56DA1124B336C9156E50244C31FCA46444AD7DAA,
	InputBridge_UpdatePicoInput_m6DAFF4C75A5C0CDD1BBA6D76E0BAD6C7F674E373,
	InputBridge_UpdateDeviceActive_m9352C145C5C95F1770C10DCF8AEE85D43335F0C5,
	InputBridge_correctValue_mA5FBFF1AB93AC7021FF359ED128B12E5587CA2A7,
	InputBridge_GetControllerBindingValue_m07A3152C0B22E686A900F0757B661030D455AE26,
	InputBridge_GetGrabbedControllerBinding_m159190D760A8D3F454DEBB245A3DD042C478E73A,
	InputBridge_GetInputAxisValue_mC39B86EC8C4D42A4E5A2A1BD8CB1DFBB205D7D4E,
	InputBridge_ApplyDeadZones_m5B904E35A029F47B6D66EDB033E8E81619FDB9C6,
	InputBridge_onDeviceChanged_m18ED7F8F2E84929CB1407F8F030ED7E66D8F4B76,
	InputBridge_setDeviceProperties_mBA8E3BCCFF994C4448D38570453A973D021B50F7,
	InputBridge_GetSupportsIndexTouch_mAAA457A6BFE26952595924F9066EED1301F93B56,
	InputBridge_GetLoadedSDK_mA7070D8B563CED25B49CB228E7422FA93195AB04,
	InputBridge_GetSupportsThumbTouch_mE9167083B163BFCD8B20B2699D7435454F995ADA,
	InputBridge_GetIsOculusDevice_mC3E917E2ABE12517F7A791F7FDCA9D382E0886FD,
	InputBridge_GetIsOculusQuest_m6ECF1DEA3FC5D019B89091804F05DCA5C4F879C4,
	InputBridge_GetIsHTCDevice_mFC6024745F11A84B64473C4F5960BC44D242C906,
	InputBridge_GetIsPicoDevice_m49CE872BA505B7019C729F515D387D657A837A16,
	InputBridge_GetHMD_m49D54E850CFE44ED97900BEFA52C582E2D5222E0,
	InputBridge_GetHMDName_mF2D89D4B9B70CC5F8B4278861813BCF254B7385D,
	InputBridge_GetHMDLocalPosition_m7647C28BCDD62809323C54285AA1E0A8B91F70EF,
	InputBridge_GetHMDLocalRotation_mD279A46F7E9AD50D6CC5BB59E8E6AE4BD11F18C4,
	InputBridge_GetLeftController_m989E19A30F39B5E160A84BB8DA0CCEC40A649C97,
	InputBridge_GetRightController_m42FDF08DEF79C178A34EC6FEBA21189BFDC48582,
	InputBridge_GetControllerLocalPosition_mB4B0FF0E1E1A45D92B7A4DA72A759FA2D80A3504,
	InputBridge_GetControllerLocalRotation_m67D864C04E0BE31773F0C66268769DD3B780D71C,
	InputBridge_GetControllerType_mAED7CEF5C409788CA13E9AC6B312B426A6FCF9E8,
	InputBridge_GetControllerVelocity_m7A01A62BD88C2BED3673812B6FB79CA4A2DF07F7,
	InputBridge_GetControllerAngularVelocity_m7ED306C1388381F07FCFE334628DC8043D4497AD,
	InputBridge_GetControllerName_mE862E78ADC082FCCE4C7477318D83D4F617C0BED,
	InputBridge_GetIsValveIndexController_m56DC783A7F85FD1D822335723F62E2FCE3FFF6E5,
	InputBridge_getFeatureUsage_m434F9D60BFC69445F8D4A53DA4E6CD0A392AF80B,
	InputBridge_getFeatureUsage_m9E70F7B8F42E8CE97908AE5055531FBD908EB6F1,
	InputBridge_getFeatureUsage_mA997A4BFF2FDE7B3374AE6E9A2CC3B6B69D8836A,
	InputBridge_getFeatureUsage_mFE17C292F940C0B11AC498566EF2479F360641F5,
	InputBridge_SetTrackingOriginMode_m6DC2D27EA9383AB706A3EDF487F373E7B09D59FC,
	InputBridge_changeOriginModeRoutine_mE049B21FD5040978BC79B40F7F2F3A9D4B3F7D0D,
	InputBridge_VibrateController_mEBABD84505D3B27040CD1270A944B1CDB1370D24,
	InputBridge_Vibrate_mA6AE668E28F7566738991738A550094BF13FAB31,
	InputBridge__ctor_m74F6352F4E3583619D9080EAF20B2941143CDB16,
	InputBridge__cctor_m332B8E441C603AB40A24606C699DC397DAECEAB5,
	InputsUpdatedAction__ctor_m360F6975296F536063F73AB29E0C63300882A3A5,
	InputsUpdatedAction_Invoke_mC2B73312B9B94114BE87444C4E24D62CC452B73E,
	InputsUpdatedAction_BeginInvoke_m9C82DDA52ABFB7A0A422FC7A0E73322E28A6DADA,
	InputsUpdatedAction_EndInvoke_mF72AEF555E870969FC90239B53173F3A225A3501,
	ControllerFoundAction__ctor_m21AE2636C135E39538AD2E38C2F417F3EC78954E,
	ControllerFoundAction_Invoke_m7F67E9C2FF2391C3186F120510962E66DCC399D4,
	ControllerFoundAction_BeginInvoke_mB34A751473298D43E61F581649805881217CC815,
	ControllerFoundAction_EndInvoke_mDF8EC6F114A3558E0EB14D258AFA3A5519B88A85,
	U3CchangeOriginModeRoutineU3Ed__167__ctor_m19D66D7A27ED416DA759633845159E97D0DCEA02,
	U3CchangeOriginModeRoutineU3Ed__167_System_IDisposable_Dispose_m09B2DCD72D31BDB49B74BB60C06D01502E535701,
	U3CchangeOriginModeRoutineU3Ed__167_MoveNext_m8BFD7892CB70501AE3F9C8B3AB8918C15F3CC2EE,
	U3CchangeOriginModeRoutineU3Ed__167_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m571780F0E4B72EA3A9D605ABE2795B135A4C4740,
	U3CchangeOriginModeRoutineU3Ed__167_System_Collections_IEnumerator_Reset_mC66980BCC2C2CBFEDF6327C33AB04DF84C0B0520,
	U3CchangeOriginModeRoutineU3Ed__167_System_Collections_IEnumerator_get_Current_m8A6A271228407362740AD6981CEDB8BA72ADFCC4,
	U3CVibrateU3Ed__169__ctor_m28E5CB17BA3E444881EA7F13B4EA29FAA1979595,
	U3CVibrateU3Ed__169_System_IDisposable_Dispose_mEEDC5671731499C44319ACADD3B1BF1D424DA293,
	U3CVibrateU3Ed__169_MoveNext_m24E772D3ACBA5C8464AC6FA008475F8B97022F93,
	U3CVibrateU3Ed__169_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A3A6BA47441B7C74B19B37610E845E49CB0587B,
	U3CVibrateU3Ed__169_System_Collections_IEnumerator_Reset_m025EF535DCCA4475D4DD979DF4E520B9E3FFCD4A,
	U3CVibrateU3Ed__169_System_Collections_IEnumerator_get_Current_mB0B6650017C2A3833AEEFE87CA9185456EDFFB81,
	JoystickControl_Start_m0820A032E321BA7CDEC9E6D3DE5092E51F236AE0,
	JoystickControl_Update_m897F2A367792D6D8016142D89BC49994D8D74CAB,
	JoystickControl_FixedUpdate_m1766D5FC87C6F42FD7017F6DFC6DF85664B10779,
	JoystickControl_doJoystickLook_m442D08E200B8BB5F2BE718CE40ED4EA693ECF79E,
	JoystickControl_OnJoystickChange_m382439CBB64D911B225FD13E935540686D17C724,
	JoystickControl_OnJoystickChange_mCCBE1BD07BE518145C1A93E861D83F5DABF12A39,
	JoystickControl__ctor_m31B70DA765DB49D71B12256F9AD3E000AED9AD87,
	JoystickVehicleControl_Update_mC2F3B1AB33A32C5D4B1F0EBEA85E95535B0518BA,
	JoystickVehicleControl_CallJoystickEvents_m79E1BA73723D2CB1E2A5AC22A792B435911011A3,
	JoystickVehicleControl_OnJoystickChange_mB80DADB5AC389C4F53160523F827ABADD7A01673,
	JoystickVehicleControl_OnJoystickChange_mC6FF2E24344D9E49774ABDD2269727649B784704,
	JoystickVehicleControl__ctor_m8BB283D15C374BDD82A95BD117A363F090C2C759,
	Lever_Start_m53227282DEF0F2B6C4BB951B8AE9693F36985D2B,
	Lever_Awake_mD541F2BD89B857B774B139DAE81A6180E2A8FB81,
	Lever_Update_m43BD025BFB2F66B5CC63CEFE65B919FEFA677730,
	Lever_GetAnglePercentage_m7512BABDF0E90207A7C715EED7C76CAC52AEBF65,
	Lever_FixedUpdate_mE36457E118FA726A62951D85056EC60FA3F87302,
	Lever_doLeverLook_m348D3FBF91F66463124BD3547773BCB18F541E30,
	Lever_SetLeverAngle_m37A96D5D1BF8DB364BD9C45A6DC71F4A9EC4D44E,
	Lever_OnLeverChange_m21DF343D12C33DA5B9AA19D66F009D4ED6FEF196,
	Lever_OnLeverDown_m4D76CB7DA3C5A2C614ADA934F889347CF5C645D5,
	Lever_OnLeverUp_m0B3290543E96A2DBBB9D9132801D015EA6F41F87,
	Lever__ctor_mD33714113CDDCF44D4D3504C4ACCBC37EE583AD3,
	LocomotionManager_get_SelectedLocomotion_mB295CB8B23987D006F23175C0FFF9AE19F1A835B,
	LocomotionManager_Start_mE4A8CC4B5FB1300E38258BBBEF10A21DF82311BF,
	LocomotionManager_Update_m7C73D1369E0CB7C3E31539046D02897BEC65A962,
	LocomotionManager_CheckControllerToggleInput_m7CDB086FCA4CDDC39273B4EE9472D3BD2686F98E,
	LocomotionManager_OnEnable_mA3F2167FA7A93427F07C8B60B3C27D78725DE8D0,
	LocomotionManager_OnDisable_m0AA68FFC363A95A5C73F1AC1D0C39441CB2A1B0C,
	LocomotionManager_OnLocomotionToggle_mA52DEE8F160FFE374CAACC4B282954A91B2CF9C0,
	LocomotionManager_LocomotionToggle_m9E1306FB627284F2DC98D1EF825C1DDF05B7C350,
	LocomotionManager_UpdateTeleportStatus_m3E860ED4E78B3E055E1C77B47AFD2BABF4B83680,
	LocomotionManager_ChangeLocomotion_m299799C1A7780B923CE56F4C64CD280046874EAA,
	LocomotionManager_ChangeLocomotionType_m0FA767E9D8CD6538565236B55AA1DB4F1769BC9E,
	LocomotionManager_toggleTeleport_m0AFC12FBDD60ED900A503A28D10DCCC72DB185D7,
	LocomotionManager_toggleSmoothLocomotion_mD27E85771E24CDADC2C0D9E8D6E1D5EB66C4EB2B,
	LocomotionManager_ToggleLocomotionType_m7302FFF1199E9C9CBA4F3EC8D1B4E4BBBFBF9C7A,
	LocomotionManager__ctor_mE330809BA37C393E52AE3818F16B918B34FC5769,
	PlayerClimbing_get_IsRigidbodyPlayer_m6396F6BF644D9E7AC298C5CA6D41664337D03588,
	PlayerClimbing_Start_mB8FF07D00D98FE89FB7E54FACCFE7EBD4D4D20D4,
	PlayerClimbing_LateUpdate_m88A9D6C21F6A4DED6A8B6E7A11D1F0EFF945A173,
	PlayerClimbing_AddClimber_m179B7830F814B556D7A20765BBE9C402E76A4BC9,
	PlayerClimbing_RemoveClimber_mA994DB6BDEAF53BFAE21E37E935243248033A3DE,
	PlayerClimbing_GrippingAtLeastOneClimbable_m06F651E863E090AF5D8F254607A2189BABA5A970,
	PlayerClimbing_checkClimbing_mA3B785215BEE062E080E57CDD8EBB4E5ED506BC1,
	PlayerClimbing_DoPhysicalClimbing_m9FB735E387E3B1AC6B5A1E31E5BF27C3808296B4,
	PlayerClimbing_onGrabbedClimbable_m4D61F329DF96A6C9A5FF04F0764D7C66C79B82EE,
	PlayerClimbing_onReleasedClimbable_m74F8FC514F1B81371EB627A4F166FCF2F4877082,
	PlayerClimbing__ctor_m3A86793E9DCE779DCAED703E73670B86F19481D8,
	PlayerGravity_Start_m9F4334F375B7C8AD2BCE8B30228E53AA2D5EDC87,
	PlayerGravity_LateUpdate_mCD21DFCF41BC9F1A59B46CD193A2277491B8A868,
	PlayerGravity_FixedUpdate_m9F6A208F1E74C46DD094DADAD07EF91F7EA5C1FA,
	PlayerGravity_ToggleGravity_mE2B524AA026443B4733C23593D3978304CA6419C,
	PlayerGravity__ctor_mAA2A73A9AE36928E8769896B26E1FE26BED4AF84,
	PlayerMovingPlatformSupport_Start_mC59934B81E2B7C5231CD337BD717EF934FF09C20,
	PlayerMovingPlatformSupport_Update_m0EE2883881477E2B6B737BEA8C215181B8695504,
	PlayerMovingPlatformSupport_FixedUpdate_m4AE812CFF6D278B044E21CAC02D09D2C7D83BA46,
	PlayerMovingPlatformSupport_CheckMovingPlatform_m1979E87F9D9019EE0DE446C9E19B863A1CFD3778,
	PlayerMovingPlatformSupport_UpdateCurrentPlatform_m750B525028BD41EB74826C7AD79B0C70209EE73A,
	PlayerMovingPlatformSupport_UpdateDistanceFromGround_m7D4A2E502702929F247B354A706F54A573528278,
	PlayerMovingPlatformSupport__ctor_m22F084302C7E5A9DD1CA709D9F3C6717D14765C6,
	PlayerRotation_add_OnBeforeRotate_mF0223343545F987770547880405BE64F32C0D7E5,
	PlayerRotation_remove_OnBeforeRotate_mE41DCBB9998C196033359C547F1CB69184E39F7B,
	PlayerRotation_add_OnAfterRotate_m09A85C75DA4B2B38F1B3D4B7406EBC9CCE5F757D,
	PlayerRotation_remove_OnAfterRotate_m390BBB4B34F913B77180F0419E3CF8FDB274E5AD,
	PlayerRotation_Update_m92ADD5BA99D93CBFD61B6A1BF05093A0A5686730,
	PlayerRotation_GetAxisInput_m7096600D15E9350D8A8ECE421701AD64BEB6BB67,
	PlayerRotation_DoSnapRotation_m5DCD72E35D4DD75FA2BCA71FA7F24ACBBFF1539A,
	PlayerRotation_RecentlySnapTurned_m13E4CC485969CC38B8A253EDF3D1681B8F23D629,
	PlayerRotation_DoSmoothRotation_m730E3BFB00ED2DB3A491717A3BBD84C4A1E29B9E,
	PlayerRotation__ctor_mBB17C60513064A3B901DDA4D9E14C2702D481318,
	OnBeforeRotateAction__ctor_m1EF78B6B9790E23C8DCB93C51E8C59714FEF2ECF,
	OnBeforeRotateAction_Invoke_m4DEEFF738582D5E9664D4D2376E99229361FB259,
	OnBeforeRotateAction_BeginInvoke_m76CF95B37C5930D8E594AF978513E3F9AB665C70,
	OnBeforeRotateAction_EndInvoke_m40D2450181DA0D1E110888C76537A8C53BFA1239,
	OnAfterRotateAction__ctor_m5A2321B790478FA7068CE835FAF1A465C85C2F7D,
	OnAfterRotateAction_Invoke_m7C5013254BB6103D77AAA90044C05F5B827B02AF,
	OnAfterRotateAction_BeginInvoke_m6EA0E6C2CD692AF66B377D7433A293DA429A3B7E,
	OnAfterRotateAction_EndInvoke_m06864B0931CCFC57BE6AA827955AD7E9691AEBCB,
	PlayerTeleport_get_teleportTransform_mE9570AB261F46715F6592FE7E40F98E492FED84B,
	PlayerTeleport_get_handedThumbstickAxis_m7FEC1BBB2859464779C62524C8334536A92616C6,
	PlayerTeleport_get_AimingTeleport_m6F40D70E70E6C17C8DB873A4DDBCAFF2DAD6DA84,
	PlayerTeleport_add_OnBeforeTeleportFade_m6E90AFAA42EC45F096A28BA098B6A44661325A4F,
	PlayerTeleport_remove_OnBeforeTeleportFade_m2B21D90D43482C6125CEF69A41D00863B7B64308,
	PlayerTeleport_add_OnBeforeTeleport_mADD5036E955F131399261947C6BB1885283AF1F0,
	PlayerTeleport_remove_OnBeforeTeleport_m00615730F0C789127763E8C79CB6B70CC8EE421D,
	PlayerTeleport_add_OnAfterTeleport_mA89DC1B2A5721EC3990DCBA3C02BE92BD219D718,
	PlayerTeleport_remove_OnAfterTeleport_mB4ABB6321EDB753429829E2742CE7131A82C14EB,
	PlayerTeleport_Start_m8A19BF3BF47D1F80219F2E35F55960BCDD875388,
	PlayerTeleport_OnEnable_m77933504C92F801BDB848EECFF7578992E36DB70,
	PlayerTeleport_setupVariables_mFE3F78B783231F0511D18518AAC0AFD7770433E8,
	PlayerTeleport_LateUpdate_mB347E25FA62B083213CFA887A8E7ECD0A5F8F5B1,
	PlayerTeleport_DoCheckTeleport_mD5912FFD81D75164880411E7B58C29BD383F0CBF,
	PlayerTeleport_TryOrHideTeleport_mB3D6C91B337E5D40D37630E748ABDCCDE5C13277,
	PlayerTeleport_EnableTeleportation_m34940426FC024639A9C699675D551913CD88A3CC,
	PlayerTeleport_DisableTeleportation_mEAE2FCF6F28F1D66FAC11654C9F98E9F35164914,
	PlayerTeleport_calculateParabola_mF3021B306ADE88A709D53FECCCEB2B084FA0D27C,
	PlayerTeleport_teleportClear_m1186D6435563C80ED32D6A064350F42DAD270817,
	PlayerTeleport_hideTeleport_m3FF93CC9322500ECC69E4D82672C6D8F82586DE6,
	PlayerTeleport_updateTeleport_mE35F2659925341B052DAF63D340D8391563F3B7F,
	PlayerTeleport_rotateMarker_m005A3D67D39F97380DDCFCBAD3D661CA0B4DF794,
	PlayerTeleport_tryTeleport_mD912EE5D43441CA0582940B7CD8B1860613737AC,
	PlayerTeleport_BeforeTeleportFade_m595A747488784286EE9BC2F88711DF3EE910B8A5,
	PlayerTeleport_BeforeTeleport_mC805DA63FB6FB339A754A7728CFD46963F153EB4,
	PlayerTeleport_AfterTeleport_m5D91A8A2534550AF02B935E0401146EDDCB4D0DF,
	PlayerTeleport_doTeleport_m7D3C540F4C773ADD52EA928D57BEFEAB3E6AE388,
	PlayerTeleport_TeleportPlayer_m04B08166527888A474A4DE65B71958952172A318,
	PlayerTeleport_TeleportPlayerToTransform_m01628DF190E72B1EF425D94171E427C5602173F6,
	PlayerTeleport_KeyDownForTeleport_m82AAEBF761EC19213A8CA35AA6590E0FDD0F3D70,
	PlayerTeleport_KeyUpFromTeleport_m55480200F029950509EFC7CDC0A6765311AB5148,
	PlayerTeleport_OnDrawGizmosSelected_m440EB558303BA1223B48213462B302A2A5658EA7,
	PlayerTeleport__ctor_m0160DD2F0975AB7432D418537525A954712F4BBC,
	OnBeforeTeleportFadeAction__ctor_m58E84EF8F920BFB61F4BF4FE14F2503E8589870B,
	OnBeforeTeleportFadeAction_Invoke_m3D6B92F5B8EE8E2704A52FF215EF6964585AD575,
	OnBeforeTeleportFadeAction_BeginInvoke_m39902D62F50A3385BC148E12512626EFD8CE836E,
	OnBeforeTeleportFadeAction_EndInvoke_m24771503B84C0CC01DA7C9105952F05DD125051E,
	OnBeforeTeleportAction__ctor_m7933C0CE981D771BF0BBB37FBF4FF7C14D0CCE35,
	OnBeforeTeleportAction_Invoke_m25262B7301500D484F305403BB82B376E0E54C90,
	OnBeforeTeleportAction_BeginInvoke_m019C9567AB49D9D5645EDB1C5B6EC8FB656AF75F,
	OnBeforeTeleportAction_EndInvoke_mD0FB8661E01DFF8B60C954E68029E5A4F02760BD,
	OnAfterTeleportAction__ctor_m981EB16D1E3F1CDD181FED4E7D5A30F2DA3B8AEF,
	OnAfterTeleportAction_Invoke_mDAAD110BCAA810D51D72339FD48854846798F50B,
	OnAfterTeleportAction_BeginInvoke_mF4BD7E7F8CC8DD09820763EC70FB320EC6EEA305,
	OnAfterTeleportAction_EndInvoke_m56720E4B0A578197EF411B2F2007C9CE95F6AE75,
	U3CdoTeleportU3Ed__83__ctor_mEDE331D0F4E85B954678CDA880D1DE450BD2FE1F,
	U3CdoTeleportU3Ed__83_System_IDisposable_Dispose_m11A5D2437156E9F49F3E4D5A9B532054992CFAF3,
	U3CdoTeleportU3Ed__83_MoveNext_m925233F800A515CB7BBEEBA2CF36AC9BDDD7AAEA,
	U3CdoTeleportU3Ed__83_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0842B151A77FAD1DEBB5C7B33D5AFAB99CE92DC,
	U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_Reset_mEC4A1F9D50285E26078D76CF648C4A5C6570A9DC,
	U3CdoTeleportU3Ed__83_System_Collections_IEnumerator_get_Current_m9A8D085E00E487C0F81CBB6825FAD59A4B5CEDBF,
	PointerEvents_OnPointerClick_m14DBC43D0859A50E2B74486AD406E58271DF1463,
	PointerEvents_OnPointerEnter_m9433D65B24BDD23F4ADEDC6F639AAC2CB9CE6F2A,
	PointerEvents_OnPointerExit_mB6C97F3D1BFFD7336D3A439B8C70FB90D1E6971F,
	PointerEvents_OnPointerDown_mE9CD08107B76FA6B6130B04ED0D5ADD7B6DBA4FA,
	PointerEvents_OnPointerUp_m8279725F17687544AF0850B80B729F55499C7DC6,
	PointerEvents_DistanceExceeded_m0735A61C4D72234D3831B1843791C789C66663D4,
	PointerEvents__ctor_m4C675AD8A01C5367543C75942A7A8776ED1EB5C2,
	RemoteGrabber_Start_m2D6F32E14B84D9285176CD7E1E95AE0CAFDB0C78,
	RemoteGrabber_Update_m5215B10FE1DC83CA2B4B7C50282CC1EA15E41445,
	RemoteGrabber_ObjectHit_mC54104C19B29069449CDC6A1CDB801D297622545,
	RemoteGrabber_RemovePreviousHitObject_mD5B70C28DE6CFB59EDCCECAC58EFDF48EA8EE3BA,
	RemoteGrabber_OnTriggerEnter_m65FB61A488F74554051038ABA996942546D155DB,
	RemoteGrabber_OnTriggerExit_m19E38A5EA061205A5C29F155118D15E2E2FCBC75,
	RemoteGrabber__ctor_mDCDA505857F7EB0EB5DEB4D383B89581D1BDD37F,
	Slider_get_SlidePercentage_mF29A1EE4A3164F73347B05E6D542CA2D27CBBE95,
	Slider_Start_m812EAC6E84AB2C0948D43DF13235107269C49DED,
	Slider_Update_m22DA18F86E9D3FF3F0032C92FFC1B371166DB058,
	Slider_OnSliderChange_m3CEB58E364A82769A0A913F1E52476DF8574F946,
	Slider__ctor_mE178070494FD1A1F478CECE79E0E60F49BEB7EBE,
	SmoothLocomotion_add_OnBeforeMove_m63EE22A194AA2ECD3237B4321D83D484BDC55825,
	SmoothLocomotion_remove_OnBeforeMove_mED6FEEFF2505156E24FD9CED269047AE32189DD2,
	SmoothLocomotion_add_OnAfterMove_mDC172891FD65D0B7C4335C8778B4411856DB08E2,
	SmoothLocomotion_remove_OnAfterMove_m4486DDD989B77E04EF1AF92261EE7075C1A6DB5C,
	SmoothLocomotion_Update_m90B6B8D91AFB2CDCF2D5B9B3262D1076AD6737DB,
	SmoothLocomotion_FixedUpdate_mD9E67A34E5F0AD29159A876312316D376087228B,
	SmoothLocomotion_CheckControllerReferences_m4F807A80DC3D4A602DCBDB1995CC126C619E3D45,
	SmoothLocomotion_UpdateInputs_m168FC2356C4BA9C07E54429815BF49843988C383,
	SmoothLocomotion_DoRigidBodyJump_m8B8605781AD73FEEC552C930F9035E2C9A502166,
	SmoothLocomotion_GetMovementAxis_m4ACC5905C9EBDD80BFFB59A05238C56AA4FA5B2A,
	SmoothLocomotion_MoveCharacter_m959EC6BD930EFA8FCFB641AECA7A88F8DA6AD33A,
	SmoothLocomotion_MoveRigidCharacter_m6474FC7BC3526EC3AA29D51CFE32A0AC075A82A6,
	SmoothLocomotion_MoveRigidCharacter_mF1FDE03E2A079C1EE40CE4857E0FC733874A59B7,
	SmoothLocomotion_MoveCharacter_m66F9DE70BBAC765420C46E5227B297CC74CD193C,
	SmoothLocomotion_CheckJump_m5EB58D0BC6DDECCE8AB3F4F6BC14DC62D16F93D9,
	SmoothLocomotion_CheckSprint_m840017CDF6575B8BBE14EDF7BB0A89799598ED6E,
	SmoothLocomotion_IsGrounded_mC758B1DDE98E2070390E3EE4586211FD8697434B,
	SmoothLocomotion_SetupCharacterController_m561A6C9485CEEBF86E68784A1BC93AA899E60CBF,
	SmoothLocomotion_SetupRigidbodyPlayer_mBCD7B93D7D402855230E8989AEDE79AFD80EFC24,
	SmoothLocomotion_EnableMovement_mE76DB7F298DDF877664703D16E90036AA303E1A8,
	SmoothLocomotion_DisableMovement_m02F086195A6023B44C0877C81EFF9BA42FD64FD6,
	SmoothLocomotion_OnCollisionStay_m0A48BB66E362D621A7DF6D3CDE6FE9397169279D,
	SmoothLocomotion__ctor_mCF65BDBF089B640DE01DB611914BA1DF249EDA3D,
	OnBeforeMoveAction__ctor_mE8A0AD160F79C84EBEAD9C25DAAD9EABA079EC10,
	OnBeforeMoveAction_Invoke_m243908BA1879AF8C9BB598CAA32877C4E3359C99,
	OnBeforeMoveAction_BeginInvoke_m973305C8B6BF2E55D2ECB0ED489B2E440EFB39A2,
	OnBeforeMoveAction_EndInvoke_mC4DC77E6CBE41260558A4A0383E239D3C9A6ACFE,
	OnAfterMoveAction__ctor_m97AA891F1437CE4D6FAFBF3C8ED260BF22BD19D6,
	OnAfterMoveAction_Invoke_m3F4653B0C0C12CA77DCA73A19766342DABCF8942,
	OnAfterMoveAction_BeginInvoke_mDC227426388B1073A1A280FAAC5A88B835E23610,
	OnAfterMoveAction_EndInvoke_mBCA95530CD02462DB25951379626A613E70D2A5A,
	SnapZone_Start_m266BDC7277ECD25647B1D5C711BDA170C78AC91D,
	SnapZone_Update_m38CF27C99424E8FDC86178BCEC4408C6041A9051,
	SnapZone_getClosestGrabbable_mD26711F454AAF0A501D529050D39CE0B6457588D,
	SnapZone_GrabGrabbable_mB2AEF06DF0D2BFA1DA9F54BA7EE07BCEADA6A7DA,
	SnapZone_disableGrabbable_mC3D0216677F185C655DA1FC506B4B211165BA663,
	SnapZone_GrabEquipped_mAA6C7941786AA87B354DBBC226F987A2F65D9731,
	SnapZone_CanBeRemoved_m235EB1685E7C9AA0764B5FDB973EA819F06206DC,
	SnapZone_ReleaseAll_mA3E67BD987899E7ADB02B57A4A723F7F5B3B3761,
	SnapZone__ctor_m0122511BDF7C8D2CBCFECCA151F15D703E2061AC,
	SnapZoneOffset__ctor_m6BA00E9C75C814BCA94A21643BC0D5022DF9F044,
	SnapZoneScale__ctor_m96101E1B18646B6D657D5E468390B7710DDCF71E,
	SteeringWheel_get_Angle_m54BEB74DD3F6D5529BA5637D037C0D5701219C02,
	SteeringWheel_get_RawAngle_mE346528B562FD1CF1B3D116A1ABEB388BCCD8C74,
	SteeringWheel_get_ScaleValue_m57B7CF1532C54D66F1C99531B6051C8AF6385344,
	SteeringWheel_get_ScaleValueInverted_mECADF6E592C0FE3983665D7B409D13B9F7629A24,
	SteeringWheel_get_AngleInverted_mAC6494C71788DEEF51969E3EDFCA165E1E9FE53B,
	SteeringWheel_get_PrimaryGrabber_mC77AAABA4A10A3971DBBFCE1774BB31991F58425,
	SteeringWheel_get_SecondaryGrabber_m661DB062D94D9E28FDC5C601214110B6D0581283,
	SteeringWheel_Update_mB5092DE9E146883FE1E0D273D8AF272778896A06,
	SteeringWheel_UpdateAngleCalculations_mFBC23E1B3F5500AEB52D1107E1E8FB39BF8EFFD2,
	SteeringWheel_GetRelativeAngle_m72DF665C5F2C1145CECB7760A57703568033E453,
	SteeringWheel_ApplyAngleToSteeringWheel_mBB0DBAD23F6D557A1E327571BF94711215936062,
	SteeringWheel_UpdatePreviewText_m64A80BB563C5647A6949BB1565D06CE2CF737FCD,
	SteeringWheel_CallEvents_m178FF63BE16181B3083C39BB4F2CA3DB1A56AB06,
	SteeringWheel_OnGrab_m76B48382769A6693CE17BA15D191FFD6CD88F7B7,
	SteeringWheel_ReturnToCenterAngle_m4264C0C18B5B40608A78E5F9918F2E6DF684CFF2,
	SteeringWheel_GetPrimaryGrabber_mED43B391BE5F8CA81C1DBB0E6C91622F81614348,
	SteeringWheel_GetSecondaryGrabber_mE213281DFF0CAAA48A00C223C4B017DBBCE592EB,
	SteeringWheel_UpdatePreviousAngle_m22539AB9F2E5A7A709DFEADE6E6CB79CC27928C1,
	SteeringWheel_GetScaledValue_m65F2272C8FD3C836E5E66301311A8D532F8730EE,
	SteeringWheel__ctor_m3E00C3A01B62F18A224CD4652A2F5CC3D339F476,
	TrackedDevice_Awake_m94E6139828EE42F9AB29B2B8E1AD89F8B7265B3E,
	TrackedDevice_OnEnable_mD350292B3867A08371AC93AD6DBC02A7AE17B53F,
	TrackedDevice_OnDisable_m13F0416CC075753E06CABE4C17590489F1558CDF,
	TrackedDevice_Update_m192EE24FF4B7349ECD2A1530160DA59BDA491E8C,
	TrackedDevice_FixedUpdate_mC8425C0F3DA6C50C291323151E802B01C024D545,
	TrackedDevice_RefreshDeviceStatus_m0D28613283A67F4D5293F613DA57C392308528DC,
	TrackedDevice_UpdateDevice_m8957BFEC907C8133873A935432F72C98DAC0805F,
	TrackedDevice_OnBeforeRender_m7F859DBEFABF62FEC8A97E54827EE861FB99332F,
	TrackedDevice__ctor_mFF55F50204D1E31B44AEC513B331EA44D5AAE67C,
	Arrow_Start_m369BD4F9440CAC6554556E92A4208552B43DBD33,
	Arrow_FixedUpdate_m0ED2616B69589E5EFC61C2DC887E8BD768C8390D,
	Arrow_ShootArrow_mDFBADFAF67FF0D13876A20F484F9574E720DD0CB,
	Arrow_QueueDestroy_m6962FB6EDF77C45137857703E4D31E8C7259BE1B,
	Arrow_ReEnableCollider_mF9B6965D9E1050D3B20585D6C4DD42C60FB2C0F6,
	Arrow_OnCollisionEnter_mC1C2911D92E0B9CA1245F292FABF1A5ACDB45A13,
	Arrow_tryStickArrow_m120FAF98726B446DC5F7D0645D6EAD81B0B16316,
	Arrow_playSoundInterval_m736F604F188A2E2C8E72F24551D04ACE185C341D,
	Arrow__ctor_m5A6B7526907DCB77A7D58B8BE24537DB3C5FB6FC,
	U3CQueueDestroyU3Ed__14__ctor_m057E16209611CDEA313CB571B315E6E3872C11EE,
	U3CQueueDestroyU3Ed__14_System_IDisposable_Dispose_m136CC33066329DA31B7262BC5701195EAB1A1489,
	U3CQueueDestroyU3Ed__14_MoveNext_m7610D645B5E65F72B83DC4DD265221CB13C9FEB7,
	U3CQueueDestroyU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DCA8900F7C5819CB797E7545B564A3A4F257F76,
	U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_Reset_mE8F44E19DD9E1BCAC0A7451D209CCB7DFF7A71EF,
	U3CQueueDestroyU3Ed__14_System_Collections_IEnumerator_get_Current_m51A9FB84AACEFC7A261528C4F8674A7B6663F3A9,
	U3CReEnableColliderU3Ed__15__ctor_mBE7E20FF9BDF79A3A1F76E37AD657AB43DC0F97C,
	U3CReEnableColliderU3Ed__15_System_IDisposable_Dispose_mFFC6AA53994F19B3B9E075466435F6CE18E57425,
	U3CReEnableColliderU3Ed__15_MoveNext_m032E3ED6B97D11B351DC9806B080B91743A8BAF5,
	U3CReEnableColliderU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E00FF427DF499A881DB0AD4F010EE6AB738DD1C,
	U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_Reset_m3750244D88E0EEC37C9305D8D06EF31F468EA722,
	U3CReEnableColliderU3Ed__15_System_Collections_IEnumerator_get_Current_mFE1B99262B023F12F689D3513CF3C71B6041FE80,
	ArrowGrabArea_Start_m89D77DE14E477B9D4E6AD6AF022A4705A1D1DF2F,
	ArrowGrabArea_OnTriggerEnter_m6CB8396ECB37DDCA56E0765CD6F568825BB114C3,
	ArrowGrabArea_OnTriggerExit_m5C01C75B92D4CC55A7654A410AF44DD6FDE190C2,
	ArrowGrabArea__ctor_mF367590A0BD62435E258A3F66DD7573A83F00059,
	AutoGrabGrabbable_OnBecomesClosestGrabbable_m649BFD8E3A7F6213B6BBFA1B7539FB12C6FE0BDE,
	AutoGrabGrabbable__ctor_m4570A045DA6F1F36BA22838BE0ED410576AE52F8,
	Bow_get_DrawPercent_mA245CBCDF2220863A29B460693226910906F1117,
	Bow_set_DrawPercent_m6AB36EBAFE115379B50791DB25BF21FBAC730C9E,
	Bow_Start_m54F6E59BBDBFE28F31F9D74A493E4EA0ABCEAF99,
	Bow_Update_mC12D334E86156C8A1095458BAC1F5F2E04D36015,
	Bow_getArrowRest_mFE77E644948CAC7FB0A946B0F7EF7440133060A1,
	Bow_canGrabArrowFromKnock_m8CF5E8AD032C44EEE41B58C067235F4A8F7996D7,
	Bow_getGrabArrowInput_m04831F34F8581BB2286A9489385C303AE70206F8,
	Bow_getGripInput_mC5EA1264E90304F5B127BE75190571063275D357,
	Bow_getTriggerInput_mFEE68D5650460F97114C997D548F0989EDA95D5A,
	Bow_setKnockPosition_m72544009E619E031706CD90553A2A306C615FF5A,
	Bow_checkDrawSound_m94D28BC594B0CE5DFC97651B7FE31ED16254BC18,
	Bow_updateDrawDistance_m274400F1DC2A47445A077DE465B47F65711DD23A,
	Bow_checkBowHaptics_mDE4AB7398E84BC97A72BDB461282CD51E9A423B6,
	Bow_resetStringPosition_mCA5AB4BE04E2A4058F425F975D85DE2640597D88,
	Bow_alignArrow_m18411182E83DC77FC8DAB583D3A28E9D80411044,
	Bow_alignBow_mBBE700C5FCC2C02DE7B5BC4EDB9AE4FB3A243568,
	Bow_ResetBowAlignment_m21BCC814ECF6282DC4BBB39FB8E506271A2F7C92,
	Bow_GrabArrow_m0A26EE98CFCFED8708B9C1973B853ACE6D0CE5E7,
	Bow_ReleaseArrow_mF5E6F59FD2003E28092E8715BEBFF87D3CC07EB3,
	Bow_OnRelease_m09E3793BEE259881BF7DB831C1894D02F63F40E0,
	Bow_resetArrowValues_mFF4D39A10576D8D8A8563CBF7CA0C7758E0E15CE,
	Bow_playSoundInterval_m0F1E0F14D3D4CEE7B34CCB8B061D6594A67277E5,
	Bow_playBowDraw_m6F9E91E654628E17057FF0842CAC7A4F681B1618,
	Bow_playBowRelease_m4C636519D1D539A15FE72D856E98152BE307081F,
	Bow__ctor_mB4B86E7ED02F4EF59CFC06E0AC302E57750AF4D2,
	Bow_U3CcheckBowHapticsU3Eb__43_0_m0A2987A1491DAA22CF361F901134358DAAB72063,
	DrawDefinition_get_DrawPercentage_m45C2F217150EC4067DA9BBC16CE60005F18CAF07,
	DrawDefinition_set_DrawPercentage_mA6AC50D291294845783DB7D157509782169F96CB,
	DrawDefinition_get_HapticAmplitude_m5446166ED809012EF8ED5D450630181B0D58D5BB,
	DrawDefinition_set_HapticAmplitude_m8CBD7DF51921E5D2641ACCAB11F7ED891D6E517A,
	DrawDefinition_get_HapticFrequency_m6A87F9C470190D7B4FC1B3C883D1A7009C705F10,
	DrawDefinition_set_HapticFrequency_m9BB7B82B8C1E4089F8E3A9B07B5A560E590BF41E,
	DrawDefinition__ctor_mBC1D4F1F54F067D5792EF99D4089F223359D71B8,
	BowArm_Start_m78188325B43D03799CB97D09E3FCD16373F1DADC,
	BowArm_Update_m1CCA3BE0DF637FB9643D35BC9478DC0C6EAB7BE1,
	BowArm__ctor_mF4A99CAF8AF4E6DCD8CDA06C7CF3BD11BB0B96DC,
	BulletHole_Start_mCA0F04EF922F06AC48087742F1BB26BCBDE2C5F3,
	BulletHole_TryAttachTo_m1C7BE563FAD2622D9A268E906218C7685DF4611E,
	BulletHole_transformIsEqualScale_mC150EF6449AC1745934CDB45C262D6BA3DB08815,
	BulletHole_DestroySelf_mF1246C09F12114C0791B366CCDE73B4CF3DCC63F,
	BulletHole__ctor_mD25368D634CD8FC2ECA858A5064A9739667D3FFA,
	CalibratePlayerHeight_Start_m15E64735188427890BDD77BA5749AE92565226EC,
	CalibratePlayerHeight_CalibrateHeight_m05476ACE240D3FD3A5A495A8E7920CC79DAA00A4,
	CalibratePlayerHeight_CalibrateHeight_mA0F48DEF8D0422E3D8EE6CF8965B406C30C58735,
	CalibratePlayerHeight_ResetPlayerHeight_m69CAAEFE48BC9FA2AD44F7365105D236F0FD228D,
	CalibratePlayerHeight_GetCurrentPlayerHeight_m0E3F317D85D6802DDBAA2FAEEFD998CAF68E6A4B,
	CalibratePlayerHeight__ctor_m4E89FB31114BC031971534089DF1628A847BB35B,
	CalibratePlayerHeight_U3CStartU3Eb__5_0_mAD6086CF5F1AA804CF53225063D065F9B405E105,
	CustomCenterOfMass_Start_m7382A37160CBC4754645DC037131A5BB33D60977,
	CustomCenterOfMass_SetCenterOfMass_m9C08DB2EB067AA41CF83F38A7FEE711043139C76,
	CustomCenterOfMass_getThisCenterOfMass_m27E1146D17189D57E82A4D46CDEA8E2950DF057B,
	CustomCenterOfMass_OnDrawGizmos_m0CBCF0E8A8B05AC4A974D72058CE272B1B98D78C,
	CustomCenterOfMass__ctor_m989EE81CB480B4964216950488D4C69BB787A40C,
	DrawerSound_OnDrawerUpdate_m7D3E5A843DD67757C24195826BF4A75057D53421,
	DrawerSound__ctor_mD69A625B82C2F0E1E850BAEC20C949731F68EBA1,
	Explosive_DoExplosion_mE1DBADF2293CFE036F5B2E7FF0E3BBF9BC909DF9,
	Explosive_explosionRoutine_mD239D37C5BA2FE8902EEA9467F16EAB1B31C1DF5,
	Explosive_dealDelayedDamaged_mAB7DFA3D4E6DBB480595FABD1B3EA677D2CF59C8,
	Explosive_OnDrawGizmosSelected_m4075C422BFB651736C7DA5F199701E316BEEE8A9,
	Explosive__ctor_mA4CBFD2D7CAEACDEAA379EC6991530E0FB4EE4BA,
	U3CexplosionRoutineU3Ed__6__ctor_mDFFAB2903F51377E6BCCF588589922D9D443F5D8,
	U3CexplosionRoutineU3Ed__6_System_IDisposable_Dispose_m982D56CAA96DF3801A98927040399DA7BF911760,
	U3CexplosionRoutineU3Ed__6_MoveNext_m9EC54D6B0D8CD3CDC3CA08268907D59BFCF92E72,
	U3CexplosionRoutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08EE4D79C46D01492D59547363DF06AB6EBA4C87,
	U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_Reset_m86C3EFCC9BC2B0CD9122FC7E70D9E23DF66D50C8,
	U3CexplosionRoutineU3Ed__6_System_Collections_IEnumerator_get_Current_m39F65CDA7A4B4AB69025BAF99683C7A949C0E5F4,
	U3CdealDelayedDamagedU3Ed__7__ctor_m9FC5588AB1474D3FAA8CC048F86F7144BDB7F241,
	U3CdealDelayedDamagedU3Ed__7_System_IDisposable_Dispose_m0026A42505DA57837E245BFF8B8DC40C7CE59A1D,
	U3CdealDelayedDamagedU3Ed__7_MoveNext_m281425012A04C638E86655F3B20ABFB6451F2158,
	U3CdealDelayedDamagedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11224EB9E0AB95E1461DB17BC8EDE5911C6665E9,
	U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_Reset_mF6145B9EC048A323E19921E1D615D1AE50453896,
	U3CdealDelayedDamagedU3Ed__7_System_Collections_IEnumerator_get_Current_mAF5AE639BD37D5D8696882067634ACC9E167C579,
	ExtensionMethods_GetDown_mB04A7DFE714826178D5F37C40D39610916EA63B1,
	Flashlight_Start_m840D3AE413832CE48CA0B065B3076C235F81326E,
	Flashlight_OnTrigger_m27B6A84F910866A6E9AFFFA6ADEBEDC3623D8294,
	Flashlight_OnTriggerUp_mDD2D00C6998F0338702D9ABFAA9381937C9A2FF9,
	Flashlight__ctor_mE2574722386836E68FB2E290B41EC1FEAE494BE1,
	FPSText_Start_mB1780EE03B114F325A3FA4DE74641E2054F98C13,
	FPSText_Update_m1547E69868AF63D941563349F6A1E3FE661D436D,
	FPSText_OnGUI_m162B8B566846D404FAAD216A89AC73BB074D6460,
	FPSText__ctor_m9021403420ABB413253B55A1F1342F8DD834A6FA,
	GrappleShot_Start_m3D4FCE0211DB8E2FBD55C40E059F1867AC922126,
	GrappleShot_LateUpdate_m6CA39EF1F06FDD21E92BBB3DDAB27F0314C78AD7,
	GrappleShot_OnTrigger_m11F1A2371CE9AC3BB76ABF9ECEA42C44ED6D48B7,
	GrappleShot_updateGrappleDistance_m22F3AF9099E92EF77BA5E8B25E8E7B8829D2433D,
	GrappleShot_OnGrab_m7F51C29363FE21C5D9BF4493F8CF1E3FC64FF99E,
	GrappleShot_OnRelease_mF5AEE21BC815D486525189E43FC2E0F58C700FD8,
	GrappleShot_onReleaseGrapple_m7E071B0705D9E50511511445EB24CEDF668066F5,
	GrappleShot_drawGrappleHelper_m24E7FE45C431CEB7E71BFC6481A9EF67F862F500,
	GrappleShot_drawGrappleLine_mB39BAAB494ADC21FCB5ADDF39DFD5192F18AFF00,
	GrappleShot_hideGrappleLine_m9E2CC5D14A3662CF9E66364FADFF085BE7E53249,
	GrappleShot_showGrappleHelper_mF9D2592FC007732EEB15FC4679688B458DBDA7FD,
	GrappleShot_hideGrappleHelper_m48BF55228AE59BB81590626D219A9EB84F054A44,
	GrappleShot_reelInGrapple_mDB9753E051EC9171BD3D1693150864941837D515,
	GrappleShot_shootGrapple_mD37E4C93B130D771C40B5846B86F4207CC8F77DA,
	GrappleShot_dropGrapple_m5FE7B8BBE4E950349B3867301D8318DCE5C49C5B,
	GrappleShot_changeGravity_m37DF3AA60C317C78A7DFC1635A332CA8482B067E,
	GrappleShot__ctor_mEB6466849CB0FF9751181E867F08DC458C2655F4,
	HandJet_Start_mFA64BC0916E0D9B302BF4E1912EC5E73DA12069E,
	HandJet_OnTrigger_mB5803926FDF3C4BEF696A58CD3A559F204AA289F,
	HandJet_FixedUpdate_m1C59F2FCBF6A0CA781624AF83B7D9BE15A1A15C0,
	HandJet_OnGrab_mE3A546F7C9BB718C8C6E2E4E5071F7481CFFA3C3,
	HandJet_ChangeGravity_m7F2C6636EAFCC89A89C0E85074DBBA0F0C1F471D,
	HandJet_OnRelease_m9CD2992F2A3100E2328ABAA22E4468B987584986,
	HandJet_doJet_m7D6762F945ACFF9386A318A9C3B379ABCAD98A12,
	HandJet_stopJet_m004E0E556254782B8887C4201B1BCAAA81EE6C55,
	HandJet_OnTriggerUp_m0186AB6828D80207AC9B6A1809E0469E6C7B20EA,
	HandJet__ctor_m759B0EF93A96CDA7C693172E6683DD34F9286011,
	IKDummy_Start_m1A716BF2C9890778F08A9064C84BD88FA98E85D9,
	IKDummy_SetParentAndLocalPosRot_m0616A319EE14C9F8BCBEFFC5913D0CB14DE4120E,
	IKDummy_LateUpdate_m6168FFF503B5486B8261AA525164C63091EAB9EE,
	IKDummy_OnAnimatorIK_m8E9652DD068AE2E2E7C016EA45B0FBD7A161BE18,
	IKDummy__ctor_mE22F8474C654644D32B7D213CD8445EDF215F90D,
	LaserPointer_Start_m878EC89A9404C56A5CFA69B7E64364BE79A75637,
	LaserPointer_LateUpdate_m4ECDAA616AB03645E38983F08035470BF3B94595,
	LaserPointer__ctor_mD9CDDD18BCA03A53D66AA3C2BBEABAA11414AF2A,
	LaserSword_Start_mF4900540460CFA3583134B6CDDCB3D7CB52024A5,
	LaserSword_Update_mC71FF04F7E2BE402F60347710B7417618E180ACD,
	LaserSword_OnTrigger_m764D84D6111D9CEEE9AA60D95465672DF6847DBC,
	LaserSword_checkCollision_m7EA288CC9837341CCF62984680BE157D4C921C6A,
	LaserSword_OnDrawGizmosSelected_m4F8AB66F80ED17EB3EF2B03C6DD1E4B02D98A55D,
	LaserSword__ctor_m0220DEB403117548EE4BD7F6DA057A97A284571A,
	LiquidWobble_Start_mEC1A98B89387F125EFB2FD4BF350BBBBBE94E26C,
	LiquidWobble_Update_m32F7596D730529AB2E6585DB8ABF2A5968D86C43,
	LiquidWobble__ctor_m5A74CC12F505E2FEDAD9F119DDD9E474194C465D,
	Marker_OnGrab_m2B17F7233B9DEABEA387B6F582885A8A404AA593,
	Marker_OnRelease_mC9D284802C6EB33152F12F40EF0DEF963B606541,
	Marker_WriteRoutine_m2BB16CBFA35A54D722A2A0BC8DC34DDEE0EDA69B,
	Marker_InitDraw_mD4989A7B7FC63B0BAB3D7741B2E03292A37DFFFD,
	Marker_DrawPoint_mC41778597EE4E90C27BA01F6ACDFFB807B841488,
	Marker_OnDrawGizmosSelected_m545931102EA6AE9ABF2B293D5505D46193A51A89,
	Marker__ctor_mA5B6DEA9968C34A296D2572099F1F6FBA8B5D10A,
	U3CWriteRoutineU3Ed__18__ctor_m79949B4EC19837A5E5A2488DCA069DBC715A9C0E,
	U3CWriteRoutineU3Ed__18_System_IDisposable_Dispose_m51140FDF31E4331F1977F044C38FF8D024FEF503,
	U3CWriteRoutineU3Ed__18_MoveNext_mD918569D82B5E2C0695E17E9A4D150A7BEC01DA8,
	U3CWriteRoutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C1B917AA6EC6397CD43B141C80FC7031463C59A,
	U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_Reset_m7B13C3D754A65F48DACEDB9EC2E0B6B977A4F478,
	U3CWriteRoutineU3Ed__18_System_Collections_IEnumerator_get_Current_mDB6DA0CADC4FB49EA0C8F9BA9820E22AD0192489,
	MoveToWaypoint_Start_mAC357B32496B636BB76AAF4F76E118ACE9F4D418,
	MoveToWaypoint_Update_mA212DA37A43C8D1923F29332DA10ACE06747594B,
	MoveToWaypoint_FixedUpdate_m4362C8E42BB68AAF17EE77EC021C4C6DDB7677BD,
	MoveToWaypoint_movePlatform_m985DC153D7858EEDF11440905AE1190A57C23E08,
	MoveToWaypoint_resetDelayStatus_m6B1FABCBF4C6048CDB43969289AFAA3C8D22D46A,
	MoveToWaypoint__ctor_m83AC7BBE1D86798B66E38B7B4B16362D87870E6C,
	MovingPlatform_Update_mF782E2421E8034BCDF9EF2DC98C82BB2FABF5F65,
	MovingPlatform__ctor_m7E5675E146175CA4E4C8724AAD34DC80C8EAFF64,
	PlayerScaler_Update_m2A659831B2BC4EAFB4D627874D8FFC6E22457017,
	PlayerScaler__ctor_mC1FC8E00688DF5332B614A2B84D84876A3A04663,
	ProjectileLauncher_Start_m18F0A290E26697740B7C0E872D6D525AB7ED4034,
	ProjectileLauncher_ShootProjectile_mC4A2D2FC97C2BA225D47CEA3BF20902C4B7EC5F5,
	ProjectileLauncher_ShootProjectile_m2805359B11D15DC572D9BA314BD2E74A4852FE42,
	ProjectileLauncher_SetForce_mC511FDD1F4B5FDF2F502A017FD02A2C7657BF77A,
	ProjectileLauncher_GetInitialProjectileForce_m176BC6F5F91AACC18412AC666C336EA0503FDD94,
	ProjectileLauncher__ctor_m1AAC5DD23769DD4A4717AB6334984C6375A3B148,
	SceneLoader_LoadScene_m98601994EBDB10A5D38BF6333B1DADCC4974E925,
	SceneLoader_FadeThenLoadScene_mFE49A2505629FD3F890A10C18B36CF2C1FD73DFC,
	SceneLoader__ctor_m6018485AA6CA6C83A5B8196B80A24234D1864522,
	U3CFadeThenLoadSceneU3Ed__6__ctor_m665C34B2C21B1D96F8A3B5F9D98425F5E28BD43B,
	U3CFadeThenLoadSceneU3Ed__6_System_IDisposable_Dispose_mBCAAA635FF9B8DFDBA4FAC2260C25FEF0BEF2C75,
	U3CFadeThenLoadSceneU3Ed__6_MoveNext_m70063270612CC214CAB02F71475D26627CAD1FC7,
	U3CFadeThenLoadSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25BB244B140EF45517B4AAFBFF18A348C61C2BD2,
	U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_Reset_mD45EED76F0AD1FD50A915769767D948D9BC1B69C,
	U3CFadeThenLoadSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m1F25442075E510F0DF886C2541CC13C4DCE4CF86,
	TimeController_get_TimeSlowing_m2D71DC4C2F25132E2F54C550FFC83EB18A15CFE1,
	TimeController_Start_m60A442E254217802FE43165960EFB93150A87F8A,
	TimeController_Update_m6CD501992FAE8DDB0397616BF71B53B56FA57638,
	TimeController_SlowTimeInputDown_m9E58D0863941623B0B3D1C4A1AD166BE6846ACF7,
	TimeController_SlowTime_m3D84A9D9964FB3415258F31216B4678A76815A7D,
	TimeController_ResumeTime_m1F02A3BC2EBE44BB70AE55EA2E8C2968F1163973,
	TimeController_resumeTimeRoutine_mBDFC67C33FCED200CC1EF9619972E7B0040F9E0A,
	TimeController__ctor_m92C119A246CE1E0FD41CA362B2C8BB900E06E994,
	U3CresumeTimeRoutineU3Ed__20__ctor_mF0D28EB0D68B68DD84B4698F8849E6499ED1F0EC,
	U3CresumeTimeRoutineU3Ed__20_System_IDisposable_Dispose_m04F5E277DA40AFB0328AE08C4B20DDCC732D04A3,
	U3CresumeTimeRoutineU3Ed__20_MoveNext_m77A449D4B9AF21B62FC12AC2784AB33452059A77,
	U3CresumeTimeRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9B46623FC41BDD8E513BC5B0E0EE3BC62C5CE2F,
	U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m32735CF094C2902B7FD912B720B44A13FA70C667,
	U3CresumeTimeRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_mF4DFE8F97966AADE13487E79DEADD188707C1DD5,
	ToggleActiveOnInputAction_OnEnable_mEC56DA25F61A422AED70E4C2997342A97062F350,
	ToggleActiveOnInputAction_OnDisable_m4F718CC6CE4214BFF7CE6E98C5679AB2B5AA2D28,
	ToggleActiveOnInputAction_ToggleActive_mA83C7DD52B158734B4C1E345B47043886E3A36C5,
	ToggleActiveOnInputAction__ctor_mD17C2FD97A9945C0AD8537258CFD8FC1F055CC5F,
	VehicleController_Start_mEB27CB47857651B6EC4232E30C84E9ACBF81370B,
	VehicleController_Update_m495738FF24BD0EA87263656018BABE9EA23362AC,
	VehicleController_CrankEngine_mBBF4C7DAABE814056286B98F81265D2645943D66,
	VehicleController_crankEngine_m26A4833D4BB24F8B6D7B77C411A92E14B1D6B1AD,
	VehicleController_CheckOutOfBounds_m42DCCB3AE2529411F9ED36BAFB8AAC6FAE185675,
	VehicleController_GetTorqueInputFromTriggers_m98E32C51065E03FCCF0A5631E42E4E8E7251DCBA,
	VehicleController_FixedUpdate_m1CFD9BFE3DFFF764AC650DBC7C5B8D81BFA6906F,
	VehicleController_UpdateWheelTorque_m9EB1F7A9995950FFD3D4F40629124E686FF4BA83,
	VehicleController_SetSteeringAngle_mD9D0EFBB489F406841751A408FCF63A81F1FAEE9,
	VehicleController_SetSteeringAngleInverted_mBD0C48F0A8336C48412B4A6F7D5325317BF9F1C6,
	VehicleController_SetSteeringAngle_m75064814F5786DC8193CFC3C0A269587097A95A4,
	VehicleController_SetSteeringAngleInverted_m2505183062A25DD59225127C0A9A5781CF7C54F7,
	VehicleController_SetMotorTorqueInput_m9F734E6BB8E359B80A572E75B515BDAF922001B4,
	VehicleController_SetMotorTorqueInputInverted_m64FBDEFBA4136778282BC8BD9D2CFDB1D93A3649,
	VehicleController_SetMotorTorqueInput_mB9D29EB1190E4622F43FF3DBC7648D07151CEC99,
	VehicleController_SetMotorTorqueInputInverted_mDFA7FBB51DE28BF7824F8A7F4D81CE7E1F27D8C0,
	VehicleController_UpdateWheelVisuals_m8FA49D0C72D108FF58ACBFFA76B7879DB890A2B4,
	VehicleController_UpdateEngineAudio_mA62644C7CE31FC08A4258B299D7E6FE7C218CDC6,
	VehicleController_OnCollisionEnter_m7BE2465CB14AF491A323C6A21A8E8B5C6E61DC11,
	VehicleController_correctValue_m76CAEE280C812D358729DAD6BAC7F645497B99E0,
	VehicleController__ctor_mB12845EFB05AADD403D7C17E397EA3F29CAE8329,
	U3CcrankEngineU3Ed__24__ctor_m19A7F161AB0546D91A6E0134A4F7934DDCF3E766,
	U3CcrankEngineU3Ed__24_System_IDisposable_Dispose_mE5E8694978801629B264AEF3F4F5881F2EF1F506,
	U3CcrankEngineU3Ed__24_MoveNext_mFFB22F12A1E8E8E2D31212D4068FE1926B0C7A16,
	U3CcrankEngineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD339DAF69F45B1291E701E6820564476DDA27D61,
	U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_Reset_m7C0127B190B227D41B70C140B3537089E4C9556E,
	U3CcrankEngineU3Ed__24_System_Collections_IEnumerator_get_Current_mC101083271693C5DE2B78C90392A29468AF958A5,
	WheelObject__ctor_m809E73E8227DE93BF89BD56230E3E81FFCB55EBC,
	VREmulator_Start_mACAFF1935D3CAAA0996C3E088E638E69D340E1BE,
	VREmulator_OnBeforeRender_m7C7ABAE1C4F491CF4EEB2FDD5CE127617C429CA6,
	VREmulator_onFirstActivate_m23E23241F5ADF0E09824E3303A5F116DC7B35D51,
	VREmulator_Update_m6FD432970596CC5C9161BA9824A98A9F3F1517EC,
	VREmulator_HasRequiredFocus_m837C60992E30438A6E7FF90E65434488714E9EDE,
	VREmulator_CheckHeadControls_m76CDCBA761303DA6FFA1035BEAB982AC2D753708,
	VREmulator_UpdateInputs_m21B3409C1172CC4294927360A686E6F7ABE1825F,
	VREmulator_CheckPlayerControls_m002E1FF243A0622091E507CE2C35EA4B4CCDCF15,
	VREmulator_FixedUpdate_m6F618997B974D8FA1F2C83D15357A6866800A158,
	VREmulator_UpdateControllerPositions_mAC2FF5D486A274D57DAD9653E13B10B4405AD530,
	VREmulator_checkGrabbers_mE6EF4B55FA0744E2967F4EE737B39FAA22B86CD4,
	VREmulator_ResetHands_m53A8C649005241519C40A92977CBB5F90DC36AFD,
	VREmulator_ResetAll_m775EF3B5E68E6000CCC2BEAB17416D9544977B81,
	VREmulator_OnEnable_m27BAFA120768B64BD0A0CC0B54E717738ECCD66B,
	VREmulator_OnDisable_m114B7AEA6E1ADB70B981E752F0065EA603AAB9E2,
	VREmulator_OnApplicationQuit_m016DE469111FB8BE29A200BA176CEC1044CE44C2,
	VREmulator__ctor_m4FF677FD505F2C2563486429695C04F3ACC9BDBD,
	Waypoint_OnDrawGizmosSelected_m89DBE667FE7A8D3A67F755D15BA0C9FC244C6D03,
	Waypoint__ctor_m0E4B80942B70F603D7BCEC137EBDC3C9143707F8,
	Zipline_Start_mA62FDD07845CE2A513F416A91E2CA41B6C0A71E8,
	Zipline_Update_m01749931F4689A7720DFA6E1157C900499322E2B,
	Zipline_OnDrawGizmosSelected_m1C1AE3F98C10B329E2B3836A59E23BC616664410,
	Zipline_OnTrigger_m493C37F23897EC3818AFC925E44592A1E94C9C1A,
	Zipline_OnButton1_mB8EE2F0E69BA858F539551264306FC8677B85E47,
	Zipline_OnButton2_mA6963DD9741A11FDD5EFFF67C48307D6F65226A9,
	Zipline_moveTowards_mFB2EE2434AD8B92C964C42A17ABD89E1DEDCDC60,
	Zipline__ctor_m5EE67BC7402CD1B1654B3910D5638E204B48CBC5,
	ControllerOffsetHelper_Start_mB803A95118152DA6B6C10A4FE16B7605718782C8,
	ControllerOffsetHelper_checkForController_mDEF21DA6D2CD0C9DA8DA6E5565558E78085FFAEC,
	ControllerOffsetHelper_OnControllerFound_m09EFBC996D68CD3095263DCE7BD232BFFED00DAD,
	ControllerOffsetHelper_GetControllerOffset_m10CDC2657E9B6D3C2D6A8F998EC6B089BE97F196,
	ControllerOffsetHelper_DefineControllerOffsets_m1AA9BCDC96702EF7405E65B49A40DAA3D1A87917,
	ControllerOffsetHelper_GetOpenXROffset_mD35E74417870CD0B5B6AEACFE70D5A429D6B7347,
	ControllerOffsetHelper__ctor_m2FD11B657A0D7CC44F946D42597F2F8472A6A93C,
	ControllerOffsetHelper_U3CGetControllerOffsetU3Eb__9_0_m65D2E817A0C9AB3FCF5FA9BEC21BAD2282891630,
	U3CcheckForControllerU3Ed__7__ctor_mFC051541FAA77CEA04AE69022B37A2674CD7586F,
	U3CcheckForControllerU3Ed__7_System_IDisposable_Dispose_m27369273FD3B20AF697737AFFB806A631193C220,
	U3CcheckForControllerU3Ed__7_MoveNext_mDB0A3594718D5553B38BA59E8AAB82C5AFA3385F,
	U3CcheckForControllerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21B9ED608FA7D8674CD950E7C90C5958B137F32A,
	U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_Reset_mC1A7223AE1F47973069023B639DA4E17BA68AB89,
	U3CcheckForControllerU3Ed__7_System_Collections_IEnumerator_get_Current_mBD2E65EF7F83B9268E52DF06325244A4D611314D,
	ControllerOffset_get_ControllerName_m1F823F45DC505BBD6D28F22F1B17D3F840337FCB,
	ControllerOffset_set_ControllerName_m3A9EE57B31DE9083FF0B042CEE8CFEFCCDA8E295,
	ControllerOffset_get_LeftControllerPositionOffset_m75FA410FC43E163C19CDD5D858EC3F13196454DC,
	ControllerOffset_set_LeftControllerPositionOffset_m3A25B37C99FD87B9FC0BB7F94C2BAB9ABAF59CA2,
	ControllerOffset_get_RightControllerPositionOffset_m4991647F97F239055F5886D7C59DF600DDE63139,
	ControllerOffset_set_RightControllerPositionOffset_m9C2EAAE04EAB7F9CCB5C007A2D63C9689262A948,
	ControllerOffset_get_LeftControllerRotationOffset_m2A7FBA8984A9ECDBCADC6FC37E91FD9D04FC665F,
	ControllerOffset_set_LeftControllerRotationOffset_m07A66EEADBF858373E6079A63F1E6EC797707461,
	ControllerOffset_get_RightControlleRotationOffset_m9B6F98193FA898D7D2F060DD19175D4375F7B66B,
	ControllerOffset_set_RightControlleRotationOffset_mA6D15F8FDFBBBC6FFC6920E5E18F98DF4C156A39,
	ControllerOffset__ctor_m089C77D4E04C444345B9E8F97395D1FC525CB8C6,
	DetachableLimb_DoDismemberment_mEA4B800B2894204054881B0CF8D5A7F6325197D5,
	DetachableLimb_ReverseDismemberment_mD2586AD48D00DB8ADF82E13E78DCCFF5F4E3B0B6,
	DetachableLimb__ctor_mB18D18C6BE3DEB20C6D50094720E09BE54ABB37C,
	DoorHelper_Start_m07F4B5C0FA21CA2485BC870D3AFFDD460BF4DD66,
	DoorHelper_Update_mB3284BAD3D12E32FDEA24A1DC188B63A4B18A381,
	DoorHelper__ctor_m03D96A1E0566C232EF2059833E2C3E6024064B98,
	GrabberArea_Update_m1B818C52A8934FAAB61CBD066DA7AAE07D88447C,
	GrabberArea_GetOpenGrabber_mCFAE6A8B88EC6889A886BFDB460A4C1F008F501C,
	GrabberArea_OnTriggerEnter_m33FEBF579D07EFC76847CAE61074131BD12873CE,
	GrabberArea_OnTriggerExit_mA6BB9984E21B04F474B51E2BA39A395838EC4259,
	GrabberArea__ctor_m99E3658ADA126FEEC26924F4779CE38C59EB7B77,
	HandCollision_Start_m5FB57150810F078E4F8E2A4613D1E93F466B4DE7,
	HandCollision_Update_mD588C96A861F704812919ADB5265AD9275078721,
	HandCollision__ctor_m98468794516D3AA8D87FD471AD24FE1B8882CCA6,
	HandController_get_offsetPosition_m5624A45D4DD418E11B1EE14B73A60C4FAE51DC2E,
	HandController_get_offsetRotation_mD61B64DAE2701E5A8B1E95B93C9542F2F6DD9213,
	HandController_Start_mF22290AFF284DD460D8BC1C27565AE9D89FEA0C0,
	HandController_Update_m7C430E15A228AD6158E43BA7A2FE31E720E2569E,
	HandController_CheckForGrabChange_mE2040F21D4EB59C6C273602A11AC1605EB70976A,
	HandController_OnGrabChange_m2B89A4AAAF492C0B7B8B037676B9A9305D162202,
	HandController_OnGrabDrop_m798C16D84548324651B85B4AC8C1C55DCC51CE66,
	HandController_SetHandAnimator_m25CF0AF24552B92BDBE4039AB11B8735942A7679,
	HandController_UpdateFromInputs_m1D2A3D6B3C940EF06CA3A41602063E5D91FCCF09,
	HandController_UpdateAnimimationStates_m30D184C2374B647430235982AECF82EDAAD74524,
	HandController_setAnimatorBlend_mEE341BBC98F0279EE92BF6E7A93159CF7F02E3CC,
	HandController_IsAnimatorGrabbable_m3544CBB3E994C051ABA2CFBF4BE7332E8E68D035,
	HandController_UpdateHandPoser_m621AAB64BED806F7741634759AB4BF62250150E9,
	HandController_EnableHandPoser_mB1763E85D7C4ACA08C764880D2ED54E48A12D287,
	HandController_EnableAutoPoser_mBD0BC613B55FE94BD35FA0E545224DA5075CA995,
	HandController_DisableAutoPoser_mF833266581823D3DD023B252B0EE84B9359CD112,
	HandController_EnableHandAnimator_m18FFDA120887E8C8176F65548949F2A0CC6A850D,
	HandController_DisableHandAnimator_m8BF066B350402DAB9B18EBC4AD0973A4B0CC6963,
	HandController_OnGrabberGrabbed_m292904F59E6F14882A737113D553F296C3DFAA2A,
	HandController_UpdateCurrentHandPose_m325C10BEC9A3BE43C472DC2BBDE69EDB9A0D1814,
	HandController_OnGrabberReleased_m4A6987F9799C932A30E9305BA486EA677943F0A2,
	HandController__ctor_mF6443126DB4D13E3787B9F0976CD8CAC6C199B4E,
	HandleGFXHelper_Start_mC7815A77FCD16AE6D72FD9AC4C338FBDCF963650,
	HandleGFXHelper_Update_m0AE93D69DF44E5708FA3CF4B17A9E471436C1A3C,
	HandleGFXHelper__ctor_m258887EC717E2D5B661B728A801D14CFA3D4C3A1,
	HandleHelper_Start_mF5FE4010959FF22269E9823152D639A356FC885A,
	HandleHelper_FixedUpdate_m27EAF9F026E484F89154827F542FE31E3B012618,
	HandleHelper_OnCollisionEnter_mACB9A6062E02A927AE16B88C6491624A48F8CC79,
	HandleHelper_doRelease_mDAE6F1AAFAA97A45DF00B3993F893AC7995E0188,
	HandleHelper__ctor_m509BD114724BF3CC9E3CF319A4612C48CE6EA2AD,
	U3CdoReleaseU3Ed__10__ctor_mD52054CD12101B675134D31D0F7856A16591F0CF,
	U3CdoReleaseU3Ed__10_System_IDisposable_Dispose_m37096A88E7669C7365BF462029BD81C23C2767AA,
	U3CdoReleaseU3Ed__10_MoveNext_mF0B8715FD2165089E5BF4CE8F4171846D387491D,
	U3CdoReleaseU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m319A13CB98EBF9D0EFE9DBC29FDA210815D0843F,
	U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_Reset_mD3E08E149BAAC161D6CC5CA74F6AF7A16E2AF610,
	U3CdoReleaseU3Ed__10_System_Collections_IEnumerator_get_Current_m59FD864B0226579CC0A62D3127FC15CA5F464148,
	HandPhysics_get_HoldingObject_mCB37A1AB20FA6D396AEBDE616FE704C8C00C490D,
	HandPhysics_Start_m85071739095631372E9DAB02045298C47C7A4058,
	HandPhysics_Update_mBFF00D89F678514C06134D0195F168F0FB41C6DE,
	HandPhysics_FixedUpdate_m90B062911EF2F31ED68CFDEF2A2C928F92499EF8,
	HandPhysics_initHandColliders_m128E6AE3BF3A5658E221692A0EDB7F438A04D729,
	HandPhysics_checkRemoteCollision_mF7A8D348BCCE41395E57D7F48843D549BC01BB6E,
	HandPhysics_drawDistanceLine_m6EC82DAEC314424FABBFC772983867377C9280D4,
	HandPhysics_checkBreakDistance_m5F93990C2367F9818614BDA2BD83A0AEAC06A8FE,
	HandPhysics_updateHandGraphics_m233A9ECDD4C18877C01A665332A1C1429A323532,
	HandPhysics_UnignoreAllCollisions_m37F6A6D0FDFB10308987F2A81B9F941F0FA7ACF2,
	HandPhysics_IgnoreGrabbableCollisions_m3AFC9F9AA8A2078C66F8C3539102641DF90F0EAE,
	HandPhysics_DisableHandColliders_m9DC30516A05512F1D5EE6D8210635A1704851A02,
	HandPhysics_EnableHandColliders_mF1B931C9C317CE4EBBB28F27109A6407DFD3F799,
	HandPhysics_OnGrabbedObject_mA26EB711E148CA76E1EBCEFE67F160E52360B996,
	HandPhysics_LockLocalPosition_m5B087EFE9D27158682E4D8C46685BCA8DC658948,
	HandPhysics_UnlockLocalPosition_m9852A6D8D9FFA57C6C39F8F99254FCAF1C184AC8,
	HandPhysics_OnReleasedObject_mE12BBD43D141AE7B0776C825780B3657D5D2FC15,
	HandPhysics_OnEnable_mC725877061292A463463C8F67CE9FF7AD9CE9C6F,
	HandPhysics_LockOffset_mC1AFC2C3C88FEC905083039B089EE4A42524031F,
	HandPhysics_UnlockOffset_m431D610A14A64B13F98BDA849B26991AAD3F9303,
	HandPhysics_OnDisable_m01D45F24C1D0EF9E34F2D9430DD34879FDF23555,
	HandPhysics_OnCollisionStay_m0865027782CA13FA31E4B3DA7119BD228233EC76,
	HandPhysics_IsValidCollision_mD91B7B0F4F14DA630B45F6923308092DFE101067,
	HandPhysics__ctor_m5147044F40DFA05F5CF538ED36A9777C7D86A482,
	U3CUnignoreAllCollisionsU3Ed__31__ctor_mDCA6B287F53616443BDE870782AF6712B0FC20A9,
	U3CUnignoreAllCollisionsU3Ed__31_System_IDisposable_Dispose_mF39CF681E821B10567ABD469845B6FB4B35F31AB,
	U3CUnignoreAllCollisionsU3Ed__31_MoveNext_mC8F6B483E77C259497B9655714365FF344E31096,
	U3CUnignoreAllCollisionsU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B2BD3A00E4095840F65AB3430D100109A112176,
	U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_Reset_m62955C6C576A05B908A26F98AD88C2C00BCB3830,
	U3CUnignoreAllCollisionsU3Ed__31_System_Collections_IEnumerator_get_Current_m68B3072808668BA3D5DCEB0D0796007F0C4BFFF1,
	HandRepresentationHelper_Update_m7043551AFE52F9E3F7FA13A3F9E31E6636D0A158,
	HandRepresentationHelper__ctor_m55377B674F450CB6FD8FFF9F5D251A8F7C54E539,
	IgnoreColliders_Start_mEC77ABF8090B8ABEC469BCC07443AC9CAA57F3AF,
	IgnoreColliders__ctor_mDD47503F31E630126C55D172448DD23290FD8E25,
	InvalidTeleportArea__ctor_mB8E1FEE33E34EF06C23019C096E7D9564A41FD2E,
	JointBreaker_Start_mE912068595AFA9D8E58517F9B03CFED4BEA14934,
	JointBreaker_Update_mC378419E3A344ABEBF35F489B11A53D7D26416AF,
	JointBreaker_BreakJoint_m7CF1C18B64B54C3E6815B4FAEF448094241AF42C,
	JointBreaker__ctor_m5D6F807A7A40F3EFDF62C1FCB656C067E1964CB9,
	JointHelper_Start_m9FA780E22DAEE8EF48C13B701804ECBA6AF36E09,
	JointHelper_lockPosition_mCEC4A2983C0137BC90318E1123402A4FE3030E5F,
	JointHelper_LateUpdate_m60508106B58906A2532D918ED425F91A8D4EE7CE,
	JointHelper_FixedUpdate_m33BC028162EB9C2B81935B7010C269B3FE9E6B23,
	JointHelper__ctor_mDEF6BF5D7C6B89962FA1CAE8791EB6C0EEBE8F8C,
	RagdollHelper_Start_m811E7E97D2BC4BA42F3C77DCFA725C056F2D4683,
	RagdollHelper__ctor_m27D1A018BF8F3C7F21CE1698722DD39280CB384F,
	RingHelper_Start_mE6BC07F626583A6613832F81CCC0F50058196DC8,
	RingHelper_Update_mB5C6D1170A4DA5D13DFC165BD3B921F2D8331BBC,
	RingHelper_AssignCamera_m8B002DAFF59BED8180F52E93876EDEA2DCEE8B08,
	RingHelper_AssignGrabbers_mA00EE89B040238CDD2D4CC73EE3FBE183D7FF729,
	RingHelper_getSelectedColor_m7FD866BA83AE3CEE66E8A656DD806B24FDA5D8F1,
	RingHelper__ctor_mE115485C5F52E43163A531A321FA092A3E73DD9D,
	ScaleMaterialHelper_Start_m4FB25C009AA06A10DBE653ABAC2083EC2CC5822C,
	ScaleMaterialHelper_updateTexture_m5D04309F196D8BBE0CB50E26EFB7E8BCEA38C31C,
	ScaleMaterialHelper_OnDrawGizmosSelected_m59D6C55B1297E3FBE98E12957E7328F11331C573,
	ScaleMaterialHelper__ctor_mAFC6D3E295FC19D404188FBCA6871604DD263460,
	StaticBatch_Start_m8C1C37C00DE626E31485181D791994CD64065CD3,
	StaticBatch__ctor_m82D66EC09859625BB62A271269C7671D89AA19D5,
	TeleportDestination__ctor_m66A0E37CF76004559CA15430391CEC3AA933B64D,
	TeleportPlayerOnEnter_OnTriggerEnter_mE6FE233A5A0BDFB5581B25B0610C45AE2980E8BC,
	TeleportPlayerOnEnter__ctor_m8769053C2E465CEFA98ED4144594DE27380FF7D9,
	UITrigger__ctor_m1AD800C6EE01FC69B679693336D7734875141FB9,
	VRIFGrabpointUpdater_Start_m1B7BF1B7AE90765E9F435D8175D8C79AE822220B,
	VRIFGrabpointUpdater_ApplyGrabPointUpdate_mDC7E28C110784795DFA3F45AAB5749A1E86159D8,
	VRIFGrabpointUpdater__ctor_mBFEBB64D042DF0E195C6A2A050E8D65683359F4C,
	UICanvasGroup_ActivateCanvas_m7EBC4F4BC737955867FDFD97FBFEF3FE99340B7E,
	UICanvasGroup__ctor_m417BF34E6536188B9F32EC4B5786115ADB133B62,
	UIPointer_Awake_m0B32646D8D81938FFC27A3F149C26A37DCC44BCB,
	UIPointer_OnEnable_m04D1BDC2458847FDD7730854AA61871BB17E6C9D,
	UIPointer_Update_mF4D408E197D06C7112B23CFBE5E83B4C8E7E5661,
	UIPointer_HidePointer_m25160298FDB8284CBE764749C0E084B96BD9B9C2,
	UIPointer__ctor_mE211F8867B883C08B699601303D74F85F4C3DFCA,
	VRCanvas_Start_m0167BA329DBB85DF9764BA2A89BA6BBE39330C9D,
	VRCanvas__ctor_m3A58870925A307299F586ED793283B749F191AF6,
	VRKeyboard_Awake_mD8B8EEDFBCC76C5769AF2A18F4560B308C5F541F,
	VRKeyboard_PressKey_mA355DE6083DF48539883D9E2CCEB59372072FCDC,
	VRKeyboard_UpdateInputField_m4670D5BB15BF60A10A385451FFCFABC7D2FD53FC,
	VRKeyboard_PlayClickSound_mFEEEC2FF0E6893396FA820FBA6AF0FB3D2457C30,
	VRKeyboard_MoveCaretUp_mBB107B868B40356F57B2D11124B73C89E9799693,
	VRKeyboard_MoveCaretBack_m435A1542AA0C50C79D7469A631CD85EA49E80768,
	VRKeyboard_ToggleShift_m9CED9817B4D946E671403CB74CC7223DE06A244F,
	VRKeyboard_IncreaseInputFieldCareteRoutine_mA5A7B0469DB9CA340FCC61FE691E1D32B22330A7,
	VRKeyboard_DecreaseInputFieldCareteRoutine_m99A480794A1376168F5CA7522609F608B6E79EFD,
	VRKeyboard_AttachToInputField_mDE13878C9A0496E8E3F50E5D7E55BFA895568277,
	VRKeyboard__ctor_mFE1DFB73614D0EC7AADE7F8BD4405367066F21AA,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11__ctor_m6B52206D9AD68684635AECBA36C4A695BA139229,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_IDisposable_Dispose_mC485916BEF71E98265213470EE03C3645D54FCA5,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_MoveNext_m66C8E0BB64C3BB98D7B7AF10B080BD15A036928B,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFEBF070A20880A3593E17C006DF335B42455A04D,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_Reset_mF7DA7BB5F54D04238ADE99CC8F0E2E11C0DB2399,
	U3CIncreaseInputFieldCareteRoutineU3Ed__11_System_Collections_IEnumerator_get_Current_mCB0ED784DA9E42D74242188E0847008EDE8E9580,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12__ctor_mC2D47F9D04A13D706CBB99AE40D96344477AEB70,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_IDisposable_Dispose_m1B79ABDEC1A0F05196D86FD802BCD0567D1CA9D6,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_MoveNext_mCBAC7704D1BF428BC1E7031A585A8A4052DBEF5A,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D735647B49C6D7D9FCC55FEAD99026EC65EC6F8,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_Reset_m6E4BBE3B605DAC51AA7CE1CB8CC7D32E31AA94D3,
	U3CDecreaseInputFieldCareteRoutineU3Ed__12_System_Collections_IEnumerator_get_Current_m0AE9742C31A362120217031E32C2C9CB48F4E935,
	VRKeyboardKey_Awake_m002435586360CC1FE9B132415E5A1EEC0DD99F8C,
	VRKeyboardKey_ToggleShift_mF8609E926C6CC9C0B3DCA2FF0D439C6B39EB7F4F,
	VRKeyboardKey_OnKeyHit_m3C0A6B0AAE82E65FE550A4A8ECD74085D7A1F971,
	VRKeyboardKey_OnKeyHit_m35193290DC2A8647553717CA033EC08887D76CA3,
	VRKeyboardKey__ctor_mC1711B5BD946F58D1ECC22DB45A8DED50DBD4BD2,
	VRKeyboardKeyCollider_Awake_mA4AB4CA8827C31473C8661B8E05A6FF25611FA06,
	VRKeyboardKeyCollider_Update_m4DDC1780E8E28AD73F52BEBEB2179F5B85723A96,
	VRKeyboardKeyCollider_OnTriggerEnter_mC448D603938E9940857B2F2EF23BCE608E6CA3FA,
	VRKeyboardKeyCollider_OnTriggerExit_mD7958B8AECC1625DD4218B080C4D668421E4DA8A,
	VRKeyboardKeyCollider__ctor_mCC1A44E7F92A4D2D783B493FBB36C020C2B4E2CA,
	VRTextInput_Awake_mEE5B6FA863EB9DF999DAB7B6C42AB1B1D7E885D2,
	VRTextInput_Update_m61331218E12BC70C2658CCF802F41B17F750BEE8,
	VRTextInput_OnInputSelect_mB8BA6A64DCD9F2A41820CBA8EC18E8C1AF6C3EE5,
	VRTextInput_OnInputDeselect_mE6A4B11FB36AF41C744667B2E369A80B6A57BF66,
	VRTextInput__ctor_m3AEA13D9A0FFC1F4F2184D30F824FEB4B1BB2E9D,
	VRUISystem_get_EventData_m5D6B0F0DA29D2DD290E0F7FC920BC8EFB36AA6BB,
	VRUISystem_set_EventData_m2D416D932DDD915E647DE4BA7E29A5EB9FF38E22,
	VRUISystem_get_Instance_m590749E871D73F6C9DE03B401D888A2615E79496,
	VRUISystem_Awake_mCEBCA037FF370180DFED046E2F217D793095355A,
	VRUISystem_init_m55FB34C998130C0FE544349399DA6D4F3B9465AB,
	VRUISystem_Process_mCB5B5D94CF894FF7E106FA4C062B73A7407DB102,
	VRUISystem_InputReady_m2D6428E70DBCD8E96A70652328D4865503E3660A,
	VRUISystem_CameraCasterReady_m0FACD102086C468BC47C5D76312205C5B60FB0DD,
	VRUISystem_PressDown_m3526513AF8F6A5DC737A20DB5E3328422E236E5E,
	VRUISystem_Press_mCAE16ED6CE29CD07366735B87452CBD1250591E5,
	VRUISystem_Release_mC5B78B0A6F19C5974D0AD6A2BB0DD16B7078F615,
	VRUISystem_ClearAll_m93E8E4AAB731E6B58D9FF233FD82A1CDB60858E5,
	VRUISystem_SetPressingObject_m8CD82F308BA950DD47FD1C0F987C524248D4267E,
	VRUISystem_SetDraggingObject_mB9311237F45902E063583C810E33156D5D9D6B88,
	VRUISystem_SetReleasingObject_m5D575F22EB68486808BCBD1FE1A83BF683731455,
	VRUISystem_AssignCameraToAllCanvases_m46ACE5FF414715B1C3F7177F43993185577D381C,
	VRUISystem_AddCanvas_m3F40B71FD3AE5C44D2C8F0C85964EBB391AB5D56,
	VRUISystem_AddCanvasToCamera_m53C643F65910CE353707B1D38C7C83A504FC0328,
	VRUISystem_UpdateControllerHand_mE0A081EDC8CEA8E456330F8537D44630904AC7BA,
	VRUISystem__ctor_m75072FE13A3EB015DF3C94E29EA4D0D03CE3FD2C,
	DestroyIfPlayMode_Start_m28902755321635A3BD97F4E1737410338456E192,
	DestroyIfPlayMode__ctor_m3194C7E4B7507A7D2797085A70C58FEB59F7FC58,
	VRUtils_get_Instance_m94182A01A486BC0F2AD143CF401F556BD13B434B,
	VRUtils_Awake_m7091F2326A1E05D337126F9114F4935DE10E3043,
	VRUtils_Log_mBD3DEBF0B0608FE7AF5B3AC4941B16E33707BDF3,
	VRUtils_Warn_m938BDD4F95A011AB16F5B5F54227663FB6F26BAF,
	VRUtils_Error_mB76158A7F697C2F3978CD1F9F18B3E820FED24D1,
	VRUtils_VRDebugLog_m41A6D3A024A7AC597F515BF24C4D91DD648E9ADF,
	VRUtils_CullDebugPanel_m04032C0B80BDE82FC0BFFFAE5F1378756CD556CA,
	VRUtils_PlaySpatialClipAt_mEDE911D70037C4CBBD46D1C35F33A5DFFA9A130F,
	VRUtils_getRandomizedPitch_m10060CDB4ACEF9DCDBE32353CA0F3D83F81FF521,
	VRUtils__ctor_m3879D0E1EE4FBE789F93BE15EED97B7A9BF749EB,
	AmmoDispenser_Update_mB8B54D6326B23F8C99E280D4EBE2C42500FC508A,
	AmmoDispenser_grabberHasWeapon_mC6CF737CA1A8C85CF8E90E7B99E95EDD522DD851,
	AmmoDispenser_GetAmmo_m02D1985F661CA14031D598832742573C5ED3B312,
	AmmoDispenser_GrabAmmo_m9B9DDF48AB202F224819475F5014D1CE8F3C0685,
	AmmoDispenser_AddAmmo_mF21FB3ACC26A9DF1EAD3403B7696788EFDC108CF,
	AmmoDispenser__ctor_m4BBA84BB7D80A05C227E915C56199FD50CE333E5,
	AmmoDisplay_OnGUI_m5DF2897C61257501F96C18673BEEC6ABAC5B0483,
	AmmoDisplay__ctor_mB8C3B93ABBA5B896803A893B34A4768EEEA83315,
	Bullet__ctor_m8DB51B542362560AF62EFE06A8E95531EC9EC718,
	BulletInsert_OnTriggerEnter_mDE079812D5BD35AB5DEA40E38DB8A518555774F1,
	BulletInsert__ctor_m7DFEDA31CE7D5C6B470F49B0378F44D0BA0071B7,
	MagazineSlide_Awake_m8D1C49511B2A62D35AE45D719764951C157E546F,
	MagazineSlide_LateUpdate_mF16905E6F0E2F1339096CF502A39E29B4D0B773D,
	MagazineSlide_recentlyEjected_mB3DDBDB19430EC924D79C41877DC3A247C54802D,
	MagazineSlide_moveMagazine_mF5FC69D2F207FEDF01BD08EF1757501E0C109CEF,
	MagazineSlide_CheckGrabClipInput_m3AB6320E3F07F696D0D5793130187EE949C19433,
	MagazineSlide_attachMagazine_m6936E6CE8DAA3775D9510D4E2941CEE791C75A13,
	MagazineSlide_detachMagazine_m5D065AFA55BF55CC2486BE2F8472712A4BF47E6C,
	MagazineSlide_EjectMagazine_m08CC377A1E422738BE59A2A5C8C2F9D430BB526C,
	MagazineSlide_EjectMagRoutine_m3EE0BD6CDC52A97688CCE2CA32D936FACB747749,
	MagazineSlide_OnGrabClipArea_m2C9D5549571FF9046E692FDC97AD9A53C4733D98,
	MagazineSlide_AttachGrabbableMagazine_mA18A76D71F593359C6B3205D734D714236D79C37,
	MagazineSlide_OnTriggerEnter_m50D0E5163406AF76261FD8CA1873239DC88CB6A6,
	MagazineSlide__ctor_mE8C8A8A9C42D2C58330A03246F8B153611698F31,
	U3CEjectMagRoutineU3Ed__23__ctor_mC47FE0792B378CEB88C726EE89AE2149544C4158,
	U3CEjectMagRoutineU3Ed__23_System_IDisposable_Dispose_m762DE2D57D522FC69032965097B174B18F78B617,
	U3CEjectMagRoutineU3Ed__23_MoveNext_m8ADC6C728DA66F5B81AFF2A48462C91ED433C15E,
	U3CEjectMagRoutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE577C7B45E07F18610ECF546B0FADEF8791813CB,
	U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_Reset_m99AB62440CCCAA96008C830FC0CE747251E9B337,
	U3CEjectMagRoutineU3Ed__23_System_Collections_IEnumerator_get_Current_m509C9D094D357860F07778C459BD0D40B2FCF4B7,
	Projectile_OnCollisionEnter_m6C5549DA51BBC1F3DB8A6D709E53F17C2DDFACDC,
	Projectile_OnCollisionEvent_mAAD1B38CBE92B1D902CD42B08FA0E647A751E380,
	Projectile_DoHitFX_m341D4D73726CE78E401799026CD339F57FAE60DF,
	Projectile_MarkAsRaycastBullet_m1CC9D4D021B2BDEF057B7D480FC9B60C6DC9CC0B,
	Projectile_DoRayCastProjectile_m92F7D72DA06AAB705E28285A13B1AE8B91C56EFF,
	Projectile_CheckForRaycast_m0CD37CDF0791960EE133837EBF0430C63F6D3654,
	Projectile__ctor_m36E68ADEB7FB7989C201F03EF1CDB3B7684C89B3,
	U3CCheckForRaycastU3Ed__13__ctor_mB1309B04CA1C1506370EE332A68E4D97B18155D3,
	U3CCheckForRaycastU3Ed__13_System_IDisposable_Dispose_m154AB76341BBF48AD0CC93DFDF25C4C45F382E08,
	U3CCheckForRaycastU3Ed__13_MoveNext_mE65DB7217542E185FB37EBFD9ACCFC2E5CEAA2C0,
	U3CCheckForRaycastU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m334BE549EC0FA1B97C64BF67D94DAACE56E9F9C7,
	U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_Reset_m64E96CDC9AE01E071A5B6CAC081F612C12FD67AC,
	U3CCheckForRaycastU3Ed__13_System_Collections_IEnumerator_get_Current_mEBE5BAF2030001B4C25C8CC2D6A2C05294FEBDF1,
	RaycastWeapon_Start_mF2986ED10374C7487BEF4D552E403843B29A026D,
	RaycastWeapon_OnTrigger_m21A4E5A7CA6E0376CAF07F68873685F7635841C4,
	RaycastWeapon_checkSlideInput_m18DC0B579A0C7E9244235652C8F111B9EA2EEA8F,
	RaycastWeapon_checkEjectInput_mDE333C34BF70070A5EA41F8184BBFA6784BC75C5,
	RaycastWeapon_CheckReloadInput_mAAD52DC19DB7647113605D2F0911752D984DA49E,
	RaycastWeapon_UnlockSlide_m7FEF930CCDA6334E4D0C9D676D9859A7C009614F,
	RaycastWeapon_EjectMagazine_m7A52BD87B82059D0D72EC3B962F245D87C1337B7,
	RaycastWeapon_Shoot_m1924963A4CA2ECBBC3F7F044514CFC79ED167409,
	RaycastWeapon_ApplyRecoil_m7921462538DEC44081137C75AD24293E7DCD638B,
	RaycastWeapon_OnRaycastHit_mA3F891D9AB1CA1F29920120BC77BAC4A722C0294,
	RaycastWeapon_ApplyParticleFX_m0EC7C9F96340A4DBB8F2EE0A564B1068AE31ED1A,
	RaycastWeapon_OnAttachedAmmo_m232E0A50F49F971559A1696F5CC3DD860544BACF,
	RaycastWeapon_OnDetachedAmmo_m95A4C81FA1D81B27A8613C8B40E3414EDEB33412,
	RaycastWeapon_GetBulletCount_m7EC854B7ADE5E11F6A647F6A9BA3CFDA7345B0A8,
	RaycastWeapon_RemoveBullet_m3F0C293A0B36D3D5000895D289C41216C4771417,
	RaycastWeapon_Reload_m4E08783BD4326FA3AF5CA753A7674BF6915164EF,
	RaycastWeapon_updateChamberedBullet_mB35ADF14B8C8ADB654FD5650E0F9BAA7E8178EAB,
	RaycastWeapon_chamberRound_m0904098E756341305F10BBBB81B3E7ED9D2F6B1F,
	RaycastWeapon_randomizeMuzzleFlashScaleRotation_mCAF0E4D6FD14E729B47F86786CE1B0F313EAA6FF,
	RaycastWeapon_OnWeaponCharged_m020A5387212DB2CDE2A080B894685CB3A19FFF5E,
	RaycastWeapon_ejectCasing_m352AE1FCA45E4164205F9A2CAA0100E6CA0742A2,
	RaycastWeapon_doMuzzleFlash_mEDB43A57DACB83C55E97CCCDA9ADD9117CE0C8BB,
	RaycastWeapon_animateSlideAndEject_m19DE2E4031ED0AF85A3EE31E95A5902F0FF482F3,
	RaycastWeapon__ctor_mA45E21613E1FFF333F4A299107FCC670BC31FED1,
	U3CdoMuzzleFlashU3Ed__74__ctor_m262ED95928F86FE308BCA36503B1CF72BDEAA9B2,
	U3CdoMuzzleFlashU3Ed__74_System_IDisposable_Dispose_mF05C246FB314668C8D3D88CFB4CAD633245D5E6A,
	U3CdoMuzzleFlashU3Ed__74_MoveNext_m8CE63ADF91C750A9B67FD036B7FBFB5B88707EEE,
	U3CdoMuzzleFlashU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC85468C57AC5E5B2FD0C5AB9153624FFD3397FF1,
	U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_Reset_mC91A6508DC6552A176052DC75E80915C65800083,
	U3CdoMuzzleFlashU3Ed__74_System_Collections_IEnumerator_get_Current_m73BED4A42F6DB4BD0EC43421316E9DB09AC9A3C3,
	U3CanimateSlideAndEjectU3Ed__75__ctor_m52D4C4F0D32166633F5800ED0FA2F9102D9573FD,
	U3CanimateSlideAndEjectU3Ed__75_System_IDisposable_Dispose_m73F851FA17249440950F54E0E3D12DCC41E3C881,
	U3CanimateSlideAndEjectU3Ed__75_MoveNext_mB2D1D8CBE242984B74CD60C88933D77C1252A0F7,
	U3CanimateSlideAndEjectU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B556AEA6646698B14C88F72971D1F0E22CB7506,
	U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_Reset_m042CF81A8EDB791453EADF7B62CEBBB35821BA90,
	U3CanimateSlideAndEjectU3Ed__75_System_Collections_IEnumerator_get_Current_m62899C28A5BC0B43CF714B0E21DFDC21EF13D318,
	WeaponSlide_Start_mC64514A27178319B231CF2DC78A2F4745C1B6848,
	WeaponSlide_OnEnable_mF2E8830A64E46555E5D33B4F0D26A28C41B7C6B5,
	WeaponSlide_OnDisable_m648C81D73CFADEA65B4C3934D21671A5C100A34C,
	WeaponSlide_Update_m374A94637F686151EECA902CF09F34B3F1646E87,
	WeaponSlide_FixedUpdate_mA417C3F95941F3137D7BEAAC4D7ED0FA59B00071,
	WeaponSlide_LockBack_m9599590FBD04AB3210D29383C537D6BBF719673C,
	WeaponSlide_UnlockBack_m1752A520C33A66C8071658D02E9270490E8E41B6,
	WeaponSlide_onSlideBack_mE014720A70E1C58447D3322E3680D2ADCB9115C4,
	WeaponSlide_onSlideForward_m4D0BD2AF7091C4BC01A4325210DA5E977C3F2AAB,
	WeaponSlide_LockSlidePosition_m21C4B6EAE4D89B04CAA8AB8CCE296FD50CAD7E39,
	WeaponSlide_UnlockSlidePosition_mCA73C09C492CC1C5C7ABA8172FE9EFD03D20A1E8,
	WeaponSlide_UnlockSlideRoutine_m91E0F77885CA06F79595EC40712EF1469F0F4427,
	WeaponSlide_playSoundInterval_m941C21C22FA99E5C7972C6BDA65D973B810D9504,
	WeaponSlide__ctor_mD9256125174C4D278F697F9FC6E1A11E7F8CD189,
	U3CUnlockSlideRoutineU3Ed__27__ctor_m2CBFD2C282CB8F21315E20BAD2F9AB8E9BC336E7,
	U3CUnlockSlideRoutineU3Ed__27_System_IDisposable_Dispose_m5257B510051CF3C319CADFB95624EB081D04AC7C,
	U3CUnlockSlideRoutineU3Ed__27_MoveNext_m1816811DF0963EEDFFBA6832607FB36A2CE09678,
	U3CUnlockSlideRoutineU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28675E321BDCDA94330AAFB67629EC1178E1B882,
	U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_Reset_m080CAB29CBCEA62C742FB8CCDCD01008A73AAF38,
	U3CUnlockSlideRoutineU3Ed__27_System_Collections_IEnumerator_get_Current_m287CA3CCBB45A3B2F5E3F4F656C74EFBABA460AD,
	AutoRotate_Update_mB4CC663D513969C85FFA56286927A5F23A4E0B75,
	AutoRotate__ctor_mB488C37BA5EC247F5F4E8BA1BE95F04F27B6F7AB,
	FreeCamera_Start_m884ACDAD9E4BFEFEAFEA38093E783F755B7CF56E,
	FreeCamera_Update_m45E25D344D55A52640F18034FFC27D7CE829D7B5,
	FreeCamera_Move_mF9AB52B5BE1B80929827B8A9488F0F46859EEF88,
	FreeCamera__ctor_m88DE606C2BE2621710E591509AD05962E68571AB,
	DrunkManFeature_Create_mE58D6E22C2001E91170E6EF35D37AD220D984D5A,
	DrunkManFeature_AddRenderPasses_m62F67A9447ED874707E7E1852E3C4E4470F11F2E,
	DrunkManFeature__ctor_m54EFBB210A79E0A49EE8D6E84333A0AF5A42658A,
	BlitPass__ctor_m96534D9B89F7E34A0AADA29783F6F315D54F6F06,
	BlitPass_Setup_mBBA957EC6EE0D9F34629B37432FE068557DBCE9B,
	BlitPass_Execute_m95032E68BFEF0AF661662E86B18DDC9F069E33B3,
	BlitPass_FrameCleanup_m199FA46C75B328692D0070A4B846747D032F547C,
	Settings__ctor_mA5D76ECE115DEE0E6F4879C3772A4C79E274ECED,
	Demo_Start_mCA0735C8ED3F0D68DB44B56D443BADEEC881EDE2,
	Demo_Update_m730C8AD00DCC914C56699CAEAE5ED493B5CE5FCD,
	Demo_OnRenderImage_mC8C1BEDC5055E92AD38455EE24150D20E46CDBD6,
	Demo__ctor_mF247B44EB31DF71EA3ADB2E19A06917B1A1512C2,
};
static const int32_t s_InvokerIndices[2148] = 
{
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	4967,
	4967,
	4967,
	4967,
	4967,
	4967,
	4967,
	4967,
	4967,
	4967,
	4967,
	4967,
	6341,
	6341,
	6341,
	5052,
	4967,
	6341,
	6341,
	6341,
	6341,
	3778,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	2760,
	6341,
	6341,
	6341,
	6341,
	6341,
	4967,
	4967,
	4967,
	6341,
	5052,
	6281,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	675,
	866,
	868,
	1564,
	1107,
	4967,
	6226,
	3765,
	3772,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	5000,
	5000,
	6226,
	6341,
	6341,
	6341,
	2755,
	6341,
	1875,
	5000,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	460,
	925,
	102,
	102,
	102,
	102,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	6341,
	6226,
	6226,
	3778,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6271,
	6271,
	6271,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	4967,
	4967,
	4967,
	6341,
	5000,
	5000,
	6341,
	4967,
	4967,
	4967,
	5000,
	5000,
	5000,
	6341,
	4967,
	6341,
	2547,
	2547,
	4967,
	6341,
	4819,
	6341,
	5000,
	5000,
	6341,
	6341,
	6281,
	6226,
	5000,
	5000,
	5000,
	5000,
	5000,
	6226,
	4967,
	4901,
	4901,
	4901,
	5000,
	2491,
	6341,
	5000,
	6341,
	5000,
	5095,
	5000,
	4967,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5095,
	6341,
	6341,
	1332,
	2491,
	2491,
	2752,
	6341,
	2752,
	6341,
	2837,
	6341,
	2837,
	6341,
	814,
	2760,
	2760,
	3508,
	6192,
	4967,
	5000,
	6271,
	6226,
	2520,
	2520,
	2520,
	6341,
	5000,
	5000,
	5000,
	6341,
	5000,
	1109,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	1343,
	2520,
	6341,
	6341,
	924,
	4967,
	6341,
	6341,
	6341,
	2752,
	8922,
	437,
	5095,
	6341,
	6341,
	2816,
	1882,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	5095,
	6341,
	6341,
	6341,
	6341,
	3772,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	3772,
	2760,
	6341,
	6341,
	6341,
	437,
	6341,
	6341,
	6341,
	6271,
	5041,
	6281,
	6281,
	4539,
	6281,
	6341,
	11271,
	6341,
	6341,
	6341,
	6341,
	2491,
	2752,
	6341,
	6341,
	2520,
	2814,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	4967,
	4967,
	6341,
	6341,
	6341,
	6341,
	2520,
	2814,
	10466,
	10466,
	10466,
	10466,
	10466,
	10391,
	6341,
	12,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	5000,
	2736,
	6341,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	6341,
	6226,
	4967,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6281,
	6281,
	4967,
	6226,
	6341,
	6341,
	6341,
	6341,
	6226,
	6226,
	1463,
	6341,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	9248,
	9248,
	8026,
	7510,
	10466,
	2752,
	1885,
	1139,
	5000,
	6341,
	6341,
	6341,
	6341,
	2760,
	6341,
	6341,
	6341,
	5000,
	4967,
	4967,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	4967,
	6226,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	1882,
	6341,
	11271,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	5000,
	6281,
	6341,
	6341,
	6341,
	6341,
	6341,
	1882,
	6271,
	4967,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	3772,
	6341,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	6271,
	6341,
	6341,
	6341,
	5041,
	6341,
	6226,
	6226,
	3772,
	3772,
	3772,
	4230,
	4230,
	4230,
	4230,
	4230,
	860,
	4230,
	6341,
	1010,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6226,
	6226,
	6226,
	6226,
	6226,
	6226,
	6226,
	3772,
	6226,
	6226,
	6226,
	6226,
	6226,
	9059,
	3772,
	3772,
	2767,
	1404,
	1404,
	6226,
	5000,
	5000,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	4230,
	6341,
	6341,
	6341,
	5000,
	5000,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	5052,
	2816,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	5000,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5052,
	260,
	6341,
	3778,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	5000,
	5000,
	5000,
	5000,
	5000,
	5000,
	6341,
	1037,
	6341,
	5000,
	6341,
	4967,
	4967,
	4967,
	5000,
	6341,
	5000,
	4967,
	4967,
	4967,
	4967,
	6341,
	6341,
	6341,
	6341,
	5000,
	4967,
	4967,
	4967,
	4967,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	5000,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	2774,
	5052,
	6341,
	4230,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	2806,
	3778,
	6341,
	6341,
	5052,
	1882,
	5052,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6271,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6331,
	6331,
	6331,
	6331,
	4617,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5095,
	6271,
	6341,
	6341,
	6341,
	6341,
	6331,
	6331,
	6341,
	6341,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	4967,
	6341,
	6341,
	6341,
	6271,
	6226,
	6271,
	6331,
	5095,
	6281,
	5052,
	6281,
	5052,
	6271,
	6281,
	5052,
	6331,
	6331,
	6331,
	6226,
	6226,
	6271,
	6226,
	6226,
	6271,
	6341,
	6341,
	6341,
	2210,
	3813,
	5000,
	6271,
	6341,
	6341,
	6341,
	6341,
	870,
	6341,
	6341,
	6341,
	2816,
	2816,
	2211,
	1890,
	6341,
	6341,
	6331,
	6242,
	6341,
	6341,
	5000,
	6271,
	6341,
	5041,
	6226,
	5000,
	2752,
	2752,
	2752,
	2752,
	5000,
	6341,
	1417,
	6341,
	5000,
	5000,
	2801,
	6341,
	6341,
	5000,
	6341,
	3508,
	6226,
	6226,
	3772,
	2841,
	4230,
	4230,
	5000,
	5000,
	6341,
	5000,
	5000,
	5095,
	5016,
	4617,
	3813,
	6341,
	6226,
	5052,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	1443,
	6341,
	5000,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	4967,
	5000,
	4967,
	5000,
	4967,
	5000,
	4967,
	5000,
	5052,
	5052,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	1876,
	3772,
	2021,
	3772,
	2760,
	2760,
	5000,
	2760,
	2760,
	5000,
	5000,
	6341,
	5000,
	6341,
	4967,
	4967,
	4967,
	4967,
	5052,
	5052,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6271,
	6271,
	6226,
	6226,
	6331,
	5095,
	6331,
	5095,
	6341,
	6341,
	6341,
	6341,
	6271,
	4230,
	3508,
	3508,
	6226,
	6271,
	4538,
	4195,
	6271,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6331,
	6331,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	2547,
	6341,
	6341,
	6341,
	5000,
	5000,
	6341,
	6341,
	6341,
	5052,
	6341,
	5052,
	4540,
	6341,
	11230,
	6281,
	6192,
	4967,
	6271,
	5041,
	6271,
	5041,
	6271,
	5041,
	6271,
	5041,
	6271,
	5041,
	10466,
	10466,
	10466,
	10466,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	1126,
	6341,
	6341,
	6341,
	4540,
	4195,
	1973,
	4607,
	1249,
	4958,
	6341,
	6271,
	6192,
	6271,
	6271,
	6271,
	6271,
	6271,
	6182,
	6226,
	6331,
	6242,
	6182,
	6182,
	4616,
	3812,
	6192,
	4616,
	4616,
	6226,
	6271,
	1236,
	1958,
	2200,
	2209,
	4967,
	3765,
	1029,
	831,
	6341,
	11271,
	2755,
	6341,
	1875,
	5000,
	2755,
	6341,
	1875,
	5000,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	2816,
	5093,
	6341,
	6341,
	6341,
	2816,
	5093,
	6341,
	6341,
	6341,
	6341,
	4540,
	6341,
	6341,
	5052,
	5052,
	6341,
	6341,
	6341,
	6192,
	6341,
	6341,
	6341,
	6341,
	6341,
	5204,
	6341,
	6341,
	2547,
	4967,
	5041,
	5041,
	6341,
	6341,
	6271,
	6341,
	6341,
	2760,
	5000,
	6271,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5041,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	10466,
	10466,
	10466,
	10466,
	6341,
	6281,
	5052,
	6271,
	5052,
	6341,
	2755,
	6341,
	1875,
	5000,
	2755,
	6341,
	1875,
	5000,
	6226,
	6329,
	6271,
	10466,
	10466,
	10466,
	10466,
	10466,
	10466,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6271,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	1138,
	2838,
	5000,
	6271,
	6271,
	6341,
	6341,
	2755,
	6341,
	1875,
	5000,
	2755,
	6341,
	1875,
	5000,
	2755,
	6341,
	1875,
	5000,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	5000,
	5000,
	5000,
	5000,
	5000,
	4230,
	6341,
	6341,
	6341,
	5000,
	6341,
	5000,
	5000,
	6341,
	6281,
	6341,
	6341,
	5052,
	6341,
	10466,
	10466,
	10466,
	10466,
	6341,
	6341,
	6341,
	6341,
	6341,
	6329,
	6341,
	5095,
	6341,
	5095,
	6271,
	6271,
	6271,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	2755,
	6341,
	1875,
	5000,
	2755,
	6341,
	1875,
	5000,
	6341,
	6341,
	6226,
	5000,
	5000,
	5000,
	6271,
	6341,
	6341,
	6341,
	6341,
	6281,
	6281,
	6281,
	6281,
	6281,
	6226,
	6226,
	6341,
	6341,
	2189,
	5052,
	6341,
	6341,
	5000,
	6341,
	6226,
	6226,
	5052,
	1242,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5095,
	6226,
	6226,
	5000,
	5000,
	2816,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	5000,
	5000,
	6341,
	5000,
	6341,
	6281,
	5052,
	6341,
	6341,
	6226,
	6271,
	6281,
	4538,
	4538,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	1456,
	6341,
	6341,
	6341,
	4230,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	4230,
	6341,
	6341,
	6341,
	6341,
	5052,
	6341,
	6281,
	6341,
	5204,
	6341,
	5095,
	6331,
	6341,
	6341,
	5052,
	6341,
	6341,
	6226,
	1877,
	6341,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	10347,
	6341,
	5052,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5052,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	2838,
	6341,
	5052,
	6341,
	6341,
	5041,
	6341,
	6341,
	5052,
	6341,
	5000,
	5041,
	6341,
	5052,
	6341,
	6341,
	6341,
	6341,
	1875,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5052,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6226,
	1036,
	421,
	6341,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	5052,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	3778,
	6341,
	5052,
	6281,
	6341,
	5000,
	6226,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6271,
	6341,
	6341,
	6271,
	6341,
	6341,
	6226,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	5204,
	6341,
	6341,
	6341,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	5052,
	5052,
	5093,
	5093,
	5052,
	5052,
	5093,
	5093,
	5000,
	6341,
	5000,
	4540,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6271,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5052,
	6341,
	6341,
	2839,
	6341,
	6341,
	6226,
	6341,
	3772,
	6341,
	6226,
	6341,
	4230,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6226,
	5000,
	6331,
	5095,
	6331,
	5095,
	6331,
	5095,
	6331,
	5095,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6331,
	6331,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	1029,
	6271,
	6341,
	6341,
	5041,
	6341,
	6341,
	6341,
	5000,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6226,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6271,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	2767,
	6341,
	6341,
	5000,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	5000,
	4230,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6125,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	4967,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6226,
	6226,
	5000,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	5000,
	11230,
	6341,
	6341,
	6341,
	6271,
	6271,
	6341,
	6341,
	6341,
	6341,
	5000,
	5000,
	5000,
	5000,
	5000,
	2760,
	4967,
	6341,
	6341,
	6341,
	11230,
	6341,
	5000,
	5000,
	5000,
	2744,
	6341,
	402,
	4540,
	6341,
	6341,
	4230,
	6226,
	5000,
	5000,
	6341,
	6341,
	6341,
	6341,
	5000,
	6341,
	6341,
	6341,
	6271,
	5095,
	6341,
	6341,
	6226,
	6341,
	3772,
	5000,
	2760,
	5000,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	5000,
	5000,
	1464,
	6341,
	6341,
	6226,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	5052,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	5019,
	1464,
	6341,
	6341,
	6192,
	6341,
	6341,
	6341,
	6341,
	6341,
	5041,
	6341,
	6226,
	6226,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6341,
	6226,
	1456,
	6341,
	4967,
	6341,
	6271,
	6226,
	6341,
	6226,
	6341,
	6341,
	6341,
	6341,
	1325,
	6341,
	6341,
	2740,
	6341,
	5000,
	506,
	2808,
	5000,
	6341,
	6341,
	6341,
	2760,
	6341,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	2148,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
