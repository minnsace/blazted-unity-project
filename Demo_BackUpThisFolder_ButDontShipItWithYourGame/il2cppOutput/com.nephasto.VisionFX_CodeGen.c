﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean Nephasto.VisionFXAsset.AnimeVision::get_Aspect()
extern void AnimeVision_get_Aspect_mB073503C7B7CFE404902B09ACFB256065E6E2796 (void);
// 0x00000002 System.Void Nephasto.VisionFXAsset.AnimeVision::set_Aspect(System.Boolean)
extern void AnimeVision_set_Aspect_m83341060905D80FA9BDCB5A73941FCC8D4A746A7 (void);
// 0x00000003 System.Single Nephasto.VisionFXAsset.AnimeVision::get_Radius()
extern void AnimeVision_get_Radius_m6A4E0E1E903752851EEE834BE3BFA750716AE949 (void);
// 0x00000004 System.Void Nephasto.VisionFXAsset.AnimeVision::set_Radius(System.Single)
extern void AnimeVision_set_Radius_mEC02789CFB79BE90410B1F2D250AC0458430B072 (void);
// 0x00000005 System.Single Nephasto.VisionFXAsset.AnimeVision::get_Length()
extern void AnimeVision_get_Length_mD9AE86F7C3729A3766367BB6C5974B720BD36CE2 (void);
// 0x00000006 System.Void Nephasto.VisionFXAsset.AnimeVision::set_Length(System.Single)
extern void AnimeVision_set_Length_m1478ED2D15BE9772756E9FD934BC9C151B62B984 (void);
// 0x00000007 System.Single Nephasto.VisionFXAsset.AnimeVision::get_Speed()
extern void AnimeVision_get_Speed_m66384BB4102F8FA6909C34A1CDD1F52A955D806B (void);
// 0x00000008 System.Void Nephasto.VisionFXAsset.AnimeVision::set_Speed(System.Single)
extern void AnimeVision_set_Speed_m99A49F6FB16DC300EE862271AC9291E97597A487 (void);
// 0x00000009 System.Single Nephasto.VisionFXAsset.AnimeVision::get_Frequency()
extern void AnimeVision_get_Frequency_m85476250961107AD83F21CE580316F404B1DB334 (void);
// 0x0000000A System.Void Nephasto.VisionFXAsset.AnimeVision::set_Frequency(System.Single)
extern void AnimeVision_set_Frequency_mD771398CC9AEC974CE526E300271460344823125 (void);
// 0x0000000B System.Single Nephasto.VisionFXAsset.AnimeVision::get_Softness()
extern void AnimeVision_get_Softness_m050ADED0114E6EF60E6D202406C5B578B102D646 (void);
// 0x0000000C System.Void Nephasto.VisionFXAsset.AnimeVision::set_Softness(System.Single)
extern void AnimeVision_set_Softness_m7DD19CDF249ED896FC1E0432419810962A6C8C6A (void);
// 0x0000000D System.Single Nephasto.VisionFXAsset.AnimeVision::get_Noise()
extern void AnimeVision_get_Noise_mC039F94FBC114B22C28C6D9E778B2313C966C2A0 (void);
// 0x0000000E System.Void Nephasto.VisionFXAsset.AnimeVision::set_Noise(System.Single)
extern void AnimeVision_set_Noise_m14F4BB38234AD397CF1DA74AECFFA1376CA3F600 (void);
// 0x0000000F UnityEngine.Color Nephasto.VisionFXAsset.AnimeVision::get_Color()
extern void AnimeVision_get_Color_mF4EF319EC229346A9C98BE9BDECDD34257A22003 (void);
// 0x00000010 System.Void Nephasto.VisionFXAsset.AnimeVision::set_Color(UnityEngine.Color)
extern void AnimeVision_set_Color_mA089C79B327F9BB242A6ACCE6C0B4EDDA8ECCC39 (void);
// 0x00000011 System.String Nephasto.VisionFXAsset.AnimeVision::ToString()
extern void AnimeVision_ToString_mB60B816FA2E1FD42091DA8EC2C37E9A2E73DA71D (void);
// 0x00000012 System.Void Nephasto.VisionFXAsset.AnimeVision::ResetDefaultValues()
extern void AnimeVision_ResetDefaultValues_m0BC537B00F913B57DDF7794BF788F5E417012920 (void);
// 0x00000013 System.Void Nephasto.VisionFXAsset.AnimeVision::UpdateCustomValues()
extern void AnimeVision_UpdateCustomValues_m204F25B9FFF7DE1E4CACD355E87105F1909B4FF4 (void);
// 0x00000014 System.Void Nephasto.VisionFXAsset.AnimeVision::.ctor()
extern void AnimeVision__ctor_m28BCA3626742E7EF5E2E63832FBB26DA30698C96 (void);
// 0x00000015 System.Single Nephasto.VisionFXAsset.BaseVision::get_Amount()
extern void BaseVision_get_Amount_mA9FE6409D906BEAA982D5437F3AF0A32E4CE4137 (void);
// 0x00000016 System.Void Nephasto.VisionFXAsset.BaseVision::set_Amount(System.Single)
extern void BaseVision_set_Amount_m8C4649A6348E5360407D7CF2A7186696FC9AA17D (void);
// 0x00000017 System.Boolean Nephasto.VisionFXAsset.BaseVision::get_EnableColorControls()
extern void BaseVision_get_EnableColorControls_m808CE4D9098FC170665E5C3611DD808C552A85D1 (void);
// 0x00000018 System.Void Nephasto.VisionFXAsset.BaseVision::set_EnableColorControls(System.Boolean)
extern void BaseVision_set_EnableColorControls_m90B1805A3BD4A7BD6297A110D0FE51885491E0F2 (void);
// 0x00000019 System.Single Nephasto.VisionFXAsset.BaseVision::get_Brightness()
extern void BaseVision_get_Brightness_m1F87DA0E273C13EFABC2FC97EB92EC7CADB3E3B5 (void);
// 0x0000001A System.Void Nephasto.VisionFXAsset.BaseVision::set_Brightness(System.Single)
extern void BaseVision_set_Brightness_m7F37E0C2419EEDD986F8D60A8FD90CF9F6117031 (void);
// 0x0000001B System.Single Nephasto.VisionFXAsset.BaseVision::get_Contrast()
extern void BaseVision_get_Contrast_m67F6D3EF430A9A4D552CD2CBEF9BB3E4A3248A2B (void);
// 0x0000001C System.Void Nephasto.VisionFXAsset.BaseVision::set_Contrast(System.Single)
extern void BaseVision_set_Contrast_m1D95AAE06CD95847EAA3BFC4EE43A62DA5E61DD7 (void);
// 0x0000001D System.Single Nephasto.VisionFXAsset.BaseVision::get_Gamma()
extern void BaseVision_get_Gamma_m301B80B2EF3702359747DDB1ADADC860F01053F7 (void);
// 0x0000001E System.Void Nephasto.VisionFXAsset.BaseVision::set_Gamma(System.Single)
extern void BaseVision_set_Gamma_m1D429D39789A17796CBBE912B7BFDA885823AEFD (void);
// 0x0000001F System.Single Nephasto.VisionFXAsset.BaseVision::get_Hue()
extern void BaseVision_get_Hue_m9B64478243F9ED1FF56D188FD13E747F0D8401DA (void);
// 0x00000020 System.Void Nephasto.VisionFXAsset.BaseVision::set_Hue(System.Single)
extern void BaseVision_set_Hue_mBF12E8C8A207F7E1AC1A0F62E312D6942746CDA9 (void);
// 0x00000021 System.Single Nephasto.VisionFXAsset.BaseVision::get_Saturation()
extern void BaseVision_get_Saturation_m00B5C8CBEFDE08DCDB9B1281318D19F4A7E140FC (void);
// 0x00000022 System.Void Nephasto.VisionFXAsset.BaseVision::set_Saturation(System.Single)
extern void BaseVision_set_Saturation_m1A59BCDDEA6865F7CC5DE85EC10CFB94708915FE (void);
// 0x00000023 System.String Nephasto.VisionFXAsset.BaseVision::ToString()
extern void BaseVision_ToString_m23C6C56A339882F33A2B70726FDD7438A5E4CED9 (void);
// 0x00000024 System.String Nephasto.VisionFXAsset.BaseVision::ShaderPath()
extern void BaseVision_ShaderPath_mB97266F3BD1DC2961F8F3FD19C837288259199E3 (void);
// 0x00000025 System.Boolean Nephasto.VisionFXAsset.BaseVision::IsSupported()
extern void BaseVision_IsSupported_m23ECCD564B858B8DD17E0B8C2B23FCC2BC338A5C (void);
// 0x00000026 System.Void Nephasto.VisionFXAsset.BaseVision::ResetDefaultValues()
extern void BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55 (void);
// 0x00000027 System.Boolean Nephasto.VisionFXAsset.BaseVision::LoadCustomResources()
extern void BaseVision_LoadCustomResources_mA23FD8A6CF9199FEE77C73F29CA140F5488168F3 (void);
// 0x00000028 System.Void Nephasto.VisionFXAsset.BaseVision::UpdateCustomValues()
extern void BaseVision_UpdateCustomValues_m4A342E05C24748D3FC0345D1732D182434D1747D (void);
// 0x00000029 System.Void Nephasto.VisionFXAsset.BaseVision::OnRenderImageCustom(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BaseVision_OnRenderImageCustom_m7B49EB592EE54AA044F8E50C7CF87653DF79CD77 (void);
// 0x0000002A System.Void Nephasto.VisionFXAsset.BaseVision::Start()
extern void BaseVision_Start_m2DA036D3871AD440488E8BE10BEAF96B19F75A64 (void);
// 0x0000002B System.Void Nephasto.VisionFXAsset.BaseVision::OnDestroy()
extern void BaseVision_OnDestroy_mF9212175388E2F458A4A99CD997762CCA34AA5F2 (void);
// 0x0000002C System.Void Nephasto.VisionFXAsset.BaseVision::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BaseVision_OnRenderImage_m35FC86D73A700A42DD62ED0BB2828254561A653C (void);
// 0x0000002D System.Void Nephasto.VisionFXAsset.BaseVision::UpdateAmount(System.Single)
extern void BaseVision_UpdateAmount_mBAFE0A79B0786D5F9C2BE9C10E1D87F544FB91BE (void);
// 0x0000002E System.Void Nephasto.VisionFXAsset.BaseVision::.ctor()
extern void BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0 (void);
// 0x0000002F System.Int32 Nephasto.VisionFXAsset.BlurryVision::get_Frames()
extern void BlurryVision_get_Frames_mBDD377E7682F16FFAE5252B9A85E52DAFBA7C34D (void);
// 0x00000030 System.Void Nephasto.VisionFXAsset.BlurryVision::set_Frames(System.Int32)
extern void BlurryVision_set_Frames_mEDE05C04107132EBC3BE7556C70B03B00908757A (void);
// 0x00000031 System.Int32 Nephasto.VisionFXAsset.BlurryVision::get_FrameStep()
extern void BlurryVision_get_FrameStep_m1710CC85257CB696A50BA43C100D6A7606DCE5CF (void);
// 0x00000032 System.Void Nephasto.VisionFXAsset.BlurryVision::set_FrameStep(System.Int32)
extern void BlurryVision_set_FrameStep_mC8A92B55246EDC92DAE743C5FC99E1C50E393C42 (void);
// 0x00000033 System.Single Nephasto.VisionFXAsset.BlurryVision::get_FrameResolution()
extern void BlurryVision_get_FrameResolution_m5248545B4E9A02D2A8D0F1515303809561ED6BED (void);
// 0x00000034 System.Void Nephasto.VisionFXAsset.BlurryVision::set_FrameResolution(System.Single)
extern void BlurryVision_set_FrameResolution_m9D14A1EED55628F641EB6D71EA953F74811C4C4A (void);
// 0x00000035 System.String Nephasto.VisionFXAsset.BlurryVision::ToString()
extern void BlurryVision_ToString_m9BE1E2C2A5E9B3EB1455A0B5DC3A31440CA093A7 (void);
// 0x00000036 System.Void Nephasto.VisionFXAsset.BlurryVision::ResetDefaultValues()
extern void BlurryVision_ResetDefaultValues_mAC1FA901AF7594F5D92684BF08052CB112C7BA95 (void);
// 0x00000037 System.Void Nephasto.VisionFXAsset.BlurryVision::UpdateCustomValues()
extern void BlurryVision_UpdateCustomValues_m2F1BA05DB9C82246E7C115AD9212D1E93C5B0347 (void);
// 0x00000038 System.Void Nephasto.VisionFXAsset.BlurryVision::OnRenderImageCustom(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BlurryVision_OnRenderImageCustom_m17CF0AA8D8349C1AC08410CBC017C76D41ED7072 (void);
// 0x00000039 System.Void Nephasto.VisionFXAsset.BlurryVision::AddFrame(UnityEngine.RenderTexture)
extern void BlurryVision_AddFrame_m888550A7CDD3C07690FBF52DAF1EF3CC99579DA6 (void);
// 0x0000003A System.Void Nephasto.VisionFXAsset.BlurryVision::OnDisable()
extern void BlurryVision_OnDisable_mAC925BA4D6D079AE7B34CC0B5D26AB1FB6ED13CC (void);
// 0x0000003B System.Void Nephasto.VisionFXAsset.BlurryVision::.ctor()
extern void BlurryVision__ctor_mB7375F100600AFF4CDE0118FA0B6ED5F4214039F (void);
// 0x0000003C System.Void Nephasto.VisionFXAsset.BlurryVision::.cctor()
extern void BlurryVision__cctor_mCDA96F243D035F825CB2025D8D375EDBA7C64863 (void);
// 0x0000003D System.Single Nephasto.VisionFXAsset.DamageVision::get_Damage()
extern void DamageVision_get_Damage_m3B389F9545D5E6B3E7449D615EC32185BC5A8653 (void);
// 0x0000003E System.Void Nephasto.VisionFXAsset.DamageVision::set_Damage(System.Single)
extern void DamageVision_set_Damage_mE5091A0BCC674DCDCD01D5A5213F5502DB680B58 (void);
// 0x0000003F System.Single Nephasto.VisionFXAsset.DamageVision::get_Definition()
extern void DamageVision_get_Definition_m9EC66FD30DB6C7E30C3159C7AB3F805262707AD7 (void);
// 0x00000040 System.Void Nephasto.VisionFXAsset.DamageVision::set_Definition(System.Single)
extern void DamageVision_set_Definition_mA588E3382EEC0A47F781ED8C03F0ACCF28777C6A (void);
// 0x00000041 System.Single Nephasto.VisionFXAsset.DamageVision::get_DamageBrightness()
extern void DamageVision_get_DamageBrightness_mC352DB8757B02B7584C22A4E878922EF130BB318 (void);
// 0x00000042 System.Void Nephasto.VisionFXAsset.DamageVision::set_DamageBrightness(System.Single)
extern void DamageVision_set_DamageBrightness_mF3DF2E8B65D29DD6020239E4C25DBFF465098D61 (void);
// 0x00000043 System.Single Nephasto.VisionFXAsset.DamageVision::get_Distortion()
extern void DamageVision_get_Distortion_m8FD64ADE103A82FFF9809C51956A8BF79A7DA5AB (void);
// 0x00000044 System.Void Nephasto.VisionFXAsset.DamageVision::set_Distortion(System.Single)
extern void DamageVision_set_Distortion_m3C879334D11B6AD279EA0836E2D8E0A181793211 (void);
// 0x00000045 Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.DamageVision::get_BlendOp()
extern void DamageVision_get_BlendOp_m10552EF529E51F1DAE8B4DFB1902463711663ABC (void);
// 0x00000046 System.Void Nephasto.VisionFXAsset.DamageVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
extern void DamageVision_set_BlendOp_mE920DBF5873CEA81E501C4F2857DA68DF716B482 (void);
// 0x00000047 UnityEngine.Color Nephasto.VisionFXAsset.DamageVision::get_Color()
extern void DamageVision_get_Color_mFF98D32F5ED974D353B74BACCD8AD9C8BC271956 (void);
// 0x00000048 System.Void Nephasto.VisionFXAsset.DamageVision::set_Color(UnityEngine.Color)
extern void DamageVision_set_Color_m1BEC4CE7523844F95A7D199514D67E6555AE66C3 (void);
// 0x00000049 System.String Nephasto.VisionFXAsset.DamageVision::ToString()
extern void DamageVision_ToString_m261DF2105A8333DC45C91CEF78B33658F11C02B1 (void);
// 0x0000004A System.Void Nephasto.VisionFXAsset.DamageVision::ResetDefaultValues()
extern void DamageVision_ResetDefaultValues_m31DA397339F31942D18066FC8966AC46C64BC855 (void);
// 0x0000004B System.Boolean Nephasto.VisionFXAsset.DamageVision::LoadCustomResources()
extern void DamageVision_LoadCustomResources_mD3A2182F44DC2F392B2F212ADA3FBDFC081D1674 (void);
// 0x0000004C System.Void Nephasto.VisionFXAsset.DamageVision::UpdateCustomValues()
extern void DamageVision_UpdateCustomValues_m93BA301691BCB93197C020AA31D09C17B1773D9F (void);
// 0x0000004D System.Void Nephasto.VisionFXAsset.DamageVision::.ctor()
extern void DamageVision__ctor_mB674BCE67E8C42EDD08A1C22BBEA969C4A3BA218 (void);
// 0x0000004E UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::get_Strength()
extern void DoubleVision_get_Strength_mB9E9A9E73A0314F82698341278A5D97A2AE92CE5 (void);
// 0x0000004F System.Void Nephasto.VisionFXAsset.DoubleVision::set_Strength(UnityEngine.Vector2)
extern void DoubleVision_set_Strength_m13BDA79AD7DDAB8640CB09BD299455073BC240F9 (void);
// 0x00000050 UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::get_Speed()
extern void DoubleVision_get_Speed_mFC79A64332AA5A2EB6891BE7D8F1C70257339CF9 (void);
// 0x00000051 System.Void Nephasto.VisionFXAsset.DoubleVision::set_Speed(UnityEngine.Vector2)
extern void DoubleVision_set_Speed_m118646F710BEAEB6980D7D7C054EE2E99B092192 (void);
// 0x00000052 System.String Nephasto.VisionFXAsset.DoubleVision::ToString()
extern void DoubleVision_ToString_mA9E878C475CE68B48DEE9C4EAE1E93B762F1607D (void);
// 0x00000053 System.Void Nephasto.VisionFXAsset.DoubleVision::ResetDefaultValues()
extern void DoubleVision_ResetDefaultValues_m54A36A8861090336297E02A63B4A1084DDAA703C (void);
// 0x00000054 System.Void Nephasto.VisionFXAsset.DoubleVision::UpdateCustomValues()
extern void DoubleVision_UpdateCustomValues_m1CAD383C98437D047D768FD09AECA31E2477FB43 (void);
// 0x00000055 System.Void Nephasto.VisionFXAsset.DoubleVision::.ctor()
extern void DoubleVision__ctor_mE79DF73D8C9C23C94DE033103BDA67EEFE1D8F9F (void);
// 0x00000056 System.Void Nephasto.VisionFXAsset.DoubleVision::.cctor()
extern void DoubleVision__cctor_m3A0A0E1B92D2D67C72AE41BA083F915FED9DE273 (void);
// 0x00000057 System.Single Nephasto.VisionFXAsset.DrunkVision::get_Drunkenness()
extern void DrunkVision_get_Drunkenness_m4B016A097A788F736B5DE622D9BA46E97D925AD1 (void);
// 0x00000058 System.Void Nephasto.VisionFXAsset.DrunkVision::set_Drunkenness(System.Single)
extern void DrunkVision_set_Drunkenness_m22A0CCB96479F6D472435D98002D61A21D21D08B (void);
// 0x00000059 System.Single Nephasto.VisionFXAsset.DrunkVision::get_DrunkSpeed()
extern void DrunkVision_get_DrunkSpeed_m069B97829B457AE4EEAEEFDF476516EF9EE717BB (void);
// 0x0000005A System.Void Nephasto.VisionFXAsset.DrunkVision::set_DrunkSpeed(System.Single)
extern void DrunkVision_set_DrunkSpeed_m8CC31C58AB23DD8E35CF96AA5AE0480363F186FD (void);
// 0x0000005B System.Single Nephasto.VisionFXAsset.DrunkVision::get_DrunkAmplitude()
extern void DrunkVision_get_DrunkAmplitude_mB90389424BB01FB4AE19B8F22928E7C9306C8AE2 (void);
// 0x0000005C System.Void Nephasto.VisionFXAsset.DrunkVision::set_DrunkAmplitude(System.Single)
extern void DrunkVision_set_DrunkAmplitude_mFA64482EC551D6D182DD7D819ADF29D0168D5F77 (void);
// 0x0000005D System.Single Nephasto.VisionFXAsset.DrunkVision::get_Swinging()
extern void DrunkVision_get_Swinging_m3C366DAF83D069A8CD16524F6E3E12EBB229D1BB (void);
// 0x0000005E System.Void Nephasto.VisionFXAsset.DrunkVision::set_Swinging(System.Single)
extern void DrunkVision_set_Swinging_mA9E15E8F56EEA55A95B80771E6336E04EE20946D (void);
// 0x0000005F System.Single Nephasto.VisionFXAsset.DrunkVision::get_SwingingSpeed()
extern void DrunkVision_get_SwingingSpeed_m093AB6ED7DD44808C1FDADCEC8451EFD1A32726B (void);
// 0x00000060 System.Void Nephasto.VisionFXAsset.DrunkVision::set_SwingingSpeed(System.Single)
extern void DrunkVision_set_SwingingSpeed_m15921BEA569B0C9FE0714963F02EFB8E2AB20749 (void);
// 0x00000061 System.Single Nephasto.VisionFXAsset.DrunkVision::get_Aberration()
extern void DrunkVision_get_Aberration_mDBB055641D7215504F04CFB2B425F1AC71F171F2 (void);
// 0x00000062 System.Void Nephasto.VisionFXAsset.DrunkVision::set_Aberration(System.Single)
extern void DrunkVision_set_Aberration_m6807E76B95BEC0A3AA35233B3AEB1AD4C57FB0B0 (void);
// 0x00000063 System.Single Nephasto.VisionFXAsset.DrunkVision::get_AberrationSpeed()
extern void DrunkVision_get_AberrationSpeed_m3A9B650322165AA14CC45E6C21BD597C56B08BE2 (void);
// 0x00000064 System.Void Nephasto.VisionFXAsset.DrunkVision::set_AberrationSpeed(System.Single)
extern void DrunkVision_set_AberrationSpeed_m238F0D49A3DCD809EECFC66E67C2D23A5132B5E9 (void);
// 0x00000065 System.Single Nephasto.VisionFXAsset.DrunkVision::get_VignetteAmount()
extern void DrunkVision_get_VignetteAmount_mEB5EBF619ECD1C3FE22D03E64273AB54E528D1E4 (void);
// 0x00000066 System.Void Nephasto.VisionFXAsset.DrunkVision::set_VignetteAmount(System.Single)
extern void DrunkVision_set_VignetteAmount_mF06F4693F902C822AD6031D981509BB9D7440D10 (void);
// 0x00000067 System.Single Nephasto.VisionFXAsset.DrunkVision::get_VignetteSpeed()
extern void DrunkVision_get_VignetteSpeed_m12B22964A7387C240F8794AC387BDD079AC952F1 (void);
// 0x00000068 System.Void Nephasto.VisionFXAsset.DrunkVision::set_VignetteSpeed(System.Single)
extern void DrunkVision_set_VignetteSpeed_m30B905F789F8F8AA4DD1F82A23FCB6B07F752E11 (void);
// 0x00000069 System.String Nephasto.VisionFXAsset.DrunkVision::ToString()
extern void DrunkVision_ToString_m5E2397D205484F5602471F383E43B84AC16D297E (void);
// 0x0000006A System.Void Nephasto.VisionFXAsset.DrunkVision::ResetDefaultValues()
extern void DrunkVision_ResetDefaultValues_mCBAE8D64F6831E8D00AFD8D1BD56DB06430A659A (void);
// 0x0000006B System.Void Nephasto.VisionFXAsset.DrunkVision::UpdateCustomValues()
extern void DrunkVision_UpdateCustomValues_m0E49E4633C48B1EADF2549E1ABA288E84391EC81 (void);
// 0x0000006C System.Void Nephasto.VisionFXAsset.DrunkVision::.ctor()
extern void DrunkVision__ctor_m65844EE71BF1E3090842E3BB534EE7BF5F3B45F8 (void);
// 0x0000006D System.Single Nephasto.VisionFXAsset.FisheyeVision::get_Barrel()
extern void FisheyeVision_get_Barrel_mAAD9A51149E28D4A00A3DFE9B63BA6A361D8B5D9 (void);
// 0x0000006E System.Void Nephasto.VisionFXAsset.FisheyeVision::set_Barrel(System.Single)
extern void FisheyeVision_set_Barrel_m37B83913C6FA9D7AC390A0E1780DD1BD78EA8E81 (void);
// 0x0000006F UnityEngine.Vector2 Nephasto.VisionFXAsset.FisheyeVision::get_Center()
extern void FisheyeVision_get_Center_mEE360783F0434E068537CCA0E2D18152A23F342A (void);
// 0x00000070 System.Void Nephasto.VisionFXAsset.FisheyeVision::set_Center(UnityEngine.Vector2)
extern void FisheyeVision_set_Center_mE3CD2BEE1DB645FC2F0952663F0EC7CDFB02F436 (void);
// 0x00000071 UnityEngine.Color Nephasto.VisionFXAsset.FisheyeVision::get_BackgroundColor()
extern void FisheyeVision_get_BackgroundColor_m4C1638597D45BAABE293DFC7CF5F4BF8076C74A4 (void);
// 0x00000072 System.Void Nephasto.VisionFXAsset.FisheyeVision::set_BackgroundColor(UnityEngine.Color)
extern void FisheyeVision_set_BackgroundColor_mF4E697ED60D439AA4D84014864D0B2A6ABFA1E7D (void);
// 0x00000073 System.String Nephasto.VisionFXAsset.FisheyeVision::ToString()
extern void FisheyeVision_ToString_m0A40DC40AF2061064EE417C7F0467DA2C5F8259B (void);
// 0x00000074 System.Void Nephasto.VisionFXAsset.FisheyeVision::ResetDefaultValues()
extern void FisheyeVision_ResetDefaultValues_m1F34DADA7875EA98DDC98EA355E86A09B39629F1 (void);
// 0x00000075 System.Void Nephasto.VisionFXAsset.FisheyeVision::UpdateCustomValues()
extern void FisheyeVision_UpdateCustomValues_m74569483C1F3CA0A4F85A2CBE5F53157867F671A (void);
// 0x00000076 System.Void Nephasto.VisionFXAsset.FisheyeVision::.ctor()
extern void FisheyeVision__ctor_m88459E2AF52EC50B48DB49523E9ACB1219EDB675 (void);
// 0x00000077 System.Void Nephasto.VisionFXAsset.FisheyeVision::.cctor()
extern void FisheyeVision__cctor_m503B13CA7B03D80598AA7908C2B1D65CC180684F (void);
// 0x00000078 UnityEngine.Vector2 Nephasto.VisionFXAsset.GhostVision::get_Focus()
extern void GhostVision_get_Focus_m83D84B2DC62539643769BBF5DE8BEA4AB63320B9 (void);
// 0x00000079 System.Void Nephasto.VisionFXAsset.GhostVision::set_Focus(UnityEngine.Vector2)
extern void GhostVision_set_Focus_m1361CF231D3A8CA22A0BD0332EF648328FB396CD (void);
// 0x0000007A System.Single Nephasto.VisionFXAsset.GhostVision::get_Speed()
extern void GhostVision_get_Speed_m5C00C31696069669807AB4D0B029088A5C0B9776 (void);
// 0x0000007B System.Void Nephasto.VisionFXAsset.GhostVision::set_Speed(System.Single)
extern void GhostVision_set_Speed_m2FB5AE8C17718F9E7210D7F08C5D28E8359C2803 (void);
// 0x0000007C System.Single Nephasto.VisionFXAsset.GhostVision::get_Aperture()
extern void GhostVision_get_Aperture_m1DD5C011D9C4AFC7B7A3C8ABAC480D1788C1034D (void);
// 0x0000007D System.Void Nephasto.VisionFXAsset.GhostVision::set_Aperture(System.Single)
extern void GhostVision_set_Aperture_mBE8788395C1780BF42E70E638C26DBE0AC3769C9 (void);
// 0x0000007E System.Single Nephasto.VisionFXAsset.GhostVision::get_Zoom()
extern void GhostVision_get_Zoom_m63F6446A85E4C76142E037CC263BD826488AA34E (void);
// 0x0000007F System.Void Nephasto.VisionFXAsset.GhostVision::set_Zoom(System.Single)
extern void GhostVision_set_Zoom_m49EA436E0F80507BB10CE1804CABDBE5CA27848B (void);
// 0x00000080 System.Boolean Nephasto.VisionFXAsset.GhostVision::get_ChangeFOV()
extern void GhostVision_get_ChangeFOV_m17E79258473DECCB1257AD41277383426DF1F8A5 (void);
// 0x00000081 System.Void Nephasto.VisionFXAsset.GhostVision::set_ChangeFOV(System.Boolean)
extern void GhostVision_set_ChangeFOV_m23B07FEBD0292E7196DCA1F75C2F29C8DC9BADB1 (void);
// 0x00000082 System.Single Nephasto.VisionFXAsset.GhostVision::get_FOV()
extern void GhostVision_get_FOV_mF978D1FF6DBF582DC970192D968D2899BDBACF83 (void);
// 0x00000083 System.Void Nephasto.VisionFXAsset.GhostVision::set_FOV(System.Single)
extern void GhostVision_set_FOV_mFA1A48F5B512AFA399F0A217FCD67728A592B2DD (void);
// 0x00000084 System.Boolean Nephasto.VisionFXAsset.GhostVision::get_AspectRatio()
extern void GhostVision_get_AspectRatio_m2187C3B59300892B3F309D7586A3739216955765 (void);
// 0x00000085 System.Void Nephasto.VisionFXAsset.GhostVision::set_AspectRatio(System.Boolean)
extern void GhostVision_set_AspectRatio_mD74BF7C02D42D96BBF3C5ADB9EBC14FC1BCA1599 (void);
// 0x00000086 UnityEngine.Color Nephasto.VisionFXAsset.GhostVision::get_InnerTint()
extern void GhostVision_get_InnerTint_m8969F191DE9F23C5E19F4AA785992C623F2109D8 (void);
// 0x00000087 System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerTint(UnityEngine.Color)
extern void GhostVision_set_InnerTint_m7BC7A5641027B43326A85B983DBE271B3010D840 (void);
// 0x00000088 System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerSaturation()
extern void GhostVision_get_InnerSaturation_mD37E162D3523CC815CB6778E88CE5D570DC7758B (void);
// 0x00000089 System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerSaturation(System.Single)
extern void GhostVision_set_InnerSaturation_mA994685C317B066B30D5ED1D3EDBD13B9860827A (void);
// 0x0000008A System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerBrightness()
extern void GhostVision_get_InnerBrightness_m96A4A295088DC3B47C3C35E6BBBDE96569E2DE99 (void);
// 0x0000008B System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerBrightness(System.Single)
extern void GhostVision_set_InnerBrightness_mBCFC4C6195C3CE3237FD8F799D6948FA1C1667C5 (void);
// 0x0000008C System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerContrast()
extern void GhostVision_get_InnerContrast_mEDD2E7BC9B5664B5FE3FD49DF2268FDDA5105D33 (void);
// 0x0000008D System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerContrast(System.Single)
extern void GhostVision_set_InnerContrast_m71C82DA577CCA4CEE6F0D2ED6420B2569D99A737 (void);
// 0x0000008E System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerGamma()
extern void GhostVision_get_InnerGamma_m85598BBD7343801AD22ADA1BC0F11394B862478A (void);
// 0x0000008F System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerGamma(System.Single)
extern void GhostVision_set_InnerGamma_mB04F93900371B0CE63392305A6664EEC38523C81 (void);
// 0x00000090 UnityEngine.Color Nephasto.VisionFXAsset.GhostVision::get_OuterTint()
extern void GhostVision_get_OuterTint_mFAE2D10C6BC94043F3176FCB973722AA14482728 (void);
// 0x00000091 System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterTint(UnityEngine.Color)
extern void GhostVision_set_OuterTint_m243D017D05941624AF21A59AEEFEC60EE6F69913 (void);
// 0x00000092 System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterSaturation()
extern void GhostVision_get_OuterSaturation_mB27D4E2BF9374B1295A410B384A28C0C06DC8D45 (void);
// 0x00000093 System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterSaturation(System.Single)
extern void GhostVision_set_OuterSaturation_m419E899A64DD9003997EB5B4DD68042818FAC982 (void);
// 0x00000094 System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterBrightness()
extern void GhostVision_get_OuterBrightness_m5A5B9513D6260849865FE58BC07F66AC120F3B4B (void);
// 0x00000095 System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterBrightness(System.Single)
extern void GhostVision_set_OuterBrightness_m7ABB33C407071C70C543D9969B39EE296F400B7E (void);
// 0x00000096 System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterContrast()
extern void GhostVision_get_OuterContrast_m075E41572DB0668B7EED3F0CED2D93E39B261CB4 (void);
// 0x00000097 System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterContrast(System.Single)
extern void GhostVision_set_OuterContrast_m9AF72FEE6A20EAC7D8FE2090005468887BA5A0D6 (void);
// 0x00000098 System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterGamma()
extern void GhostVision_get_OuterGamma_m88EB076011A9B3117840B8487BC0B6151E5B5AE1 (void);
// 0x00000099 System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterGamma(System.Single)
extern void GhostVision_set_OuterGamma_m67CEA335601A0DA541354594039005417F664B0F (void);
// 0x0000009A System.String Nephasto.VisionFXAsset.GhostVision::ToString()
extern void GhostVision_ToString_mA30CA772F008A8A6C7023C4621090F198089A8EA (void);
// 0x0000009B System.Void Nephasto.VisionFXAsset.GhostVision::ResetDefaultValues()
extern void GhostVision_ResetDefaultValues_m67FF09673E901C5B10FE4952C11F4B91D235E42B (void);
// 0x0000009C System.Void Nephasto.VisionFXAsset.GhostVision::UpdateCustomValues()
extern void GhostVision_UpdateCustomValues_mFC2177522A0CAD865C3D389C17C44C55A849ED68 (void);
// 0x0000009D System.Void Nephasto.VisionFXAsset.GhostVision::OnEnable()
extern void GhostVision_OnEnable_m251F916A1ED1AE8EF7AC8DFE62CBEF17555A47B9 (void);
// 0x0000009E System.Void Nephasto.VisionFXAsset.GhostVision::OnDisable()
extern void GhostVision_OnDisable_m2C826469B02BC9FF6165AE9DFAA766A00356E8C5 (void);
// 0x0000009F System.Void Nephasto.VisionFXAsset.GhostVision::.ctor()
extern void GhostVision__ctor_m8EA9624E9A09B44D6051AA5BE3C64CF357891E60 (void);
// 0x000000A0 System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Size()
extern void HalftoneVision_get_Size_mFCEDB2CA788D25C089EDF59686B27C3B8A993D3A (void);
// 0x000000A1 System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Size(System.Single)
extern void HalftoneVision_set_Size_mBAA26CC671B6A526F8750FDB79D895E57553EFAF (void);
// 0x000000A2 System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Angle()
extern void HalftoneVision_get_Angle_m34FEC663E7EEC7C323A502A6B086B1F9EE730DBA (void);
// 0x000000A3 System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Angle(System.Single)
extern void HalftoneVision_set_Angle_m44821AB81DC4949B95BA450BB7D1726436BB67FC (void);
// 0x000000A4 System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Strength()
extern void HalftoneVision_get_Strength_m8997579AE96AECB574927F0B7260A688036A59DA (void);
// 0x000000A5 System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Strength(System.Single)
extern void HalftoneVision_set_Strength_mE4FECF7189436D3F67B0471FC636EE82DC180DB0 (void);
// 0x000000A6 System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Sensitivity()
extern void HalftoneVision_get_Sensitivity_mEC20D7774E7358F9C9C953F77C9E5AAE5B09743B (void);
// 0x000000A7 System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Sensitivity(System.Single)
extern void HalftoneVision_set_Sensitivity_m59110CEE59A294F100A71EAEFA8E3D18027E53FD (void);
// 0x000000A8 Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.HalftoneVision::get_BlendOp()
extern void HalftoneVision_get_BlendOp_m493F1ED9FCDBB9A453869020BB7C4CAFA36DA729 (void);
// 0x000000A9 System.Void Nephasto.VisionFXAsset.HalftoneVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
extern void HalftoneVision_set_BlendOp_m5B2929BB9E1B99933A0E5E64A913BC6A284B36F2 (void);
// 0x000000AA System.String Nephasto.VisionFXAsset.HalftoneVision::ToString()
extern void HalftoneVision_ToString_m4F175FBF603612DF2E8D0DD11180908956AF201D (void);
// 0x000000AB System.Void Nephasto.VisionFXAsset.HalftoneVision::ResetDefaultValues()
extern void HalftoneVision_ResetDefaultValues_m6B490D287DF7D3D83C54F9D4AB915ECD76148741 (void);
// 0x000000AC System.Void Nephasto.VisionFXAsset.HalftoneVision::UpdateCustomValues()
extern void HalftoneVision_UpdateCustomValues_m757839AF3E6B76D3CE9579D3E3FB53BDC8AF2B47 (void);
// 0x000000AD System.Void Nephasto.VisionFXAsset.HalftoneVision::.ctor()
extern void HalftoneVision__ctor_mD4FBB79CF42EBC1ED43E2C690F352C9570F9BAFE (void);
// 0x000000AE System.Single Nephasto.VisionFXAsset.LegoVision::get_Size()
extern void LegoVision_get_Size_mCE50096D5A20222DA3B3513106F46E5DD2B052D3 (void);
// 0x000000AF System.Void Nephasto.VisionFXAsset.LegoVision::set_Size(System.Single)
extern void LegoVision_set_Size_m6B1ABF493EB865C916D1E322B9406513003825B5 (void);
// 0x000000B0 System.String Nephasto.VisionFXAsset.LegoVision::ToString()
extern void LegoVision_ToString_mF819F287C76F37760F4F5006E02309F8290E10FB (void);
// 0x000000B1 System.Void Nephasto.VisionFXAsset.LegoVision::ResetDefaultValues()
extern void LegoVision_ResetDefaultValues_m19E12C5BA193F3F4EE11DDE3B76EBB678DAB75E1 (void);
// 0x000000B2 System.Void Nephasto.VisionFXAsset.LegoVision::UpdateCustomValues()
extern void LegoVision_UpdateCustomValues_mB85AF7F250A205E53AE512DD2CA1373BD17C4661 (void);
// 0x000000B3 System.Void Nephasto.VisionFXAsset.LegoVision::.ctor()
extern void LegoVision__ctor_mAC100C80832FD5D6EA9657D46C87AA4A352DBF23 (void);
// 0x000000B4 System.Single Nephasto.VisionFXAsset.NeonVision::get_Edge()
extern void NeonVision_get_Edge_m42C8DE5EBAA9A4A081430C3E946B5BC7A3992E41 (void);
// 0x000000B5 System.Void Nephasto.VisionFXAsset.NeonVision::set_Edge(System.Single)
extern void NeonVision_set_Edge_m9FE4E4670324C3DB06E0236E1F74C674FB94EE24 (void);
// 0x000000B6 UnityEngine.Color Nephasto.VisionFXAsset.NeonVision::get_EdgeColor()
extern void NeonVision_get_EdgeColor_mFB1C5552E8FE52E0DBC7981DD973D960C8A8A7A0 (void);
// 0x000000B7 System.Void Nephasto.VisionFXAsset.NeonVision::set_EdgeColor(UnityEngine.Color)
extern void NeonVision_set_EdgeColor_m9E47E5E6F3152124A6448A44C88ECABDCDA456B8 (void);
// 0x000000B8 Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.NeonVision::get_BlendOp()
extern void NeonVision_get_BlendOp_m35F3D7D34D8DB6501285AF64B836BAE208EE6781 (void);
// 0x000000B9 System.Void Nephasto.VisionFXAsset.NeonVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
extern void NeonVision_set_BlendOp_m9132DF988C8E327BFA8C01B6EDF1634385FB108D (void);
// 0x000000BA System.String Nephasto.VisionFXAsset.NeonVision::ToString()
extern void NeonVision_ToString_mB480DB66FCEE0ECE89D1978D7D454134B50AB076 (void);
// 0x000000BB System.Void Nephasto.VisionFXAsset.NeonVision::ResetDefaultValues()
extern void NeonVision_ResetDefaultValues_m3AC702468BDABB0D8176D6A52CA77DE8C6E695B7 (void);
// 0x000000BC System.Void Nephasto.VisionFXAsset.NeonVision::UpdateCustomValues()
extern void NeonVision_UpdateCustomValues_m472BD8333BAFC14C769BAA57CE3C72C5C7577BAC (void);
// 0x000000BD System.Void Nephasto.VisionFXAsset.NeonVision::.ctor()
extern void NeonVision__ctor_mB34F5BDCED971C25D45E9D60CC656BA793F66607 (void);
// 0x000000BE UnityEngine.Color Nephasto.VisionFXAsset.ScannerVision::get_Tint()
extern void ScannerVision_get_Tint_m8A5FA01103095ACDA617B5DC0ECDFE0240FF5D6B (void);
// 0x000000BF System.Void Nephasto.VisionFXAsset.ScannerVision::set_Tint(UnityEngine.Color)
extern void ScannerVision_set_Tint_m6AC479A0D97C41F3E54A82CD2BB7BF65C06DC68A (void);
// 0x000000C0 System.Int32 Nephasto.VisionFXAsset.ScannerVision::get_LinesCount()
extern void ScannerVision_get_LinesCount_m803AA34A8D5D9BFC26990EA264DE0878F3DEAB11 (void);
// 0x000000C1 System.Void Nephasto.VisionFXAsset.ScannerVision::set_LinesCount(System.Int32)
extern void ScannerVision_set_LinesCount_m1F455A4E4010DCB3CD0547B722801B0A75A253C1 (void);
// 0x000000C2 System.Single Nephasto.VisionFXAsset.ScannerVision::get_LinesStrength()
extern void ScannerVision_get_LinesStrength_mB034D696CEFBC6A01EADCE83554F27F9051CDF8F (void);
// 0x000000C3 System.Void Nephasto.VisionFXAsset.ScannerVision::set_LinesStrength(System.Single)
extern void ScannerVision_set_LinesStrength_m1A7BB0E7DE3C6E3A96A4A94D8C3ADE33908FBDE0 (void);
// 0x000000C4 System.Single Nephasto.VisionFXAsset.ScannerVision::get_ScanLineStrength()
extern void ScannerVision_get_ScanLineStrength_m2F3ECAC627497CC0308F2A37386EF0DAFF99A260 (void);
// 0x000000C5 System.Void Nephasto.VisionFXAsset.ScannerVision::set_ScanLineStrength(System.Single)
extern void ScannerVision_set_ScanLineStrength_m95525F7037CE5BAF401F05405FDA506B1FC9A2C0 (void);
// 0x000000C6 System.Single Nephasto.VisionFXAsset.ScannerVision::get_ScanLineWidth()
extern void ScannerVision_get_ScanLineWidth_m450296C6F54ADBE4D99A798D0DBE26D9373833AF (void);
// 0x000000C7 System.Void Nephasto.VisionFXAsset.ScannerVision::set_ScanLineWidth(System.Single)
extern void ScannerVision_set_ScanLineWidth_m2AE6E2AF895BBFD97A6A2DB78FEE0F04AF47DBCE (void);
// 0x000000C8 System.Single Nephasto.VisionFXAsset.ScannerVision::get_ScanLineSpeed()
extern void ScannerVision_get_ScanLineSpeed_m679F75FA1E982DE43337AD3ED7D93774D5104A12 (void);
// 0x000000C9 System.Void Nephasto.VisionFXAsset.ScannerVision::set_ScanLineSpeed(System.Single)
extern void ScannerVision_set_ScanLineSpeed_m66831366B4A4A9BEF602F09D7C7CCBC48AC8898E (void);
// 0x000000CA System.Single Nephasto.VisionFXAsset.ScannerVision::get_NoiseBandStrength()
extern void ScannerVision_get_NoiseBandStrength_mFBCC432C88BDD14E73C0A599D3837A9F7491DCE5 (void);
// 0x000000CB System.Void Nephasto.VisionFXAsset.ScannerVision::set_NoiseBandStrength(System.Single)
extern void ScannerVision_set_NoiseBandStrength_m1E309EB96416F8253ED69E667D526B96F176810C (void);
// 0x000000CC System.Single Nephasto.VisionFXAsset.ScannerVision::get_NoiseBandWidth()
extern void ScannerVision_get_NoiseBandWidth_mD404013579053FD58F0C02A13B096798DA48B386 (void);
// 0x000000CD System.Void Nephasto.VisionFXAsset.ScannerVision::set_NoiseBandWidth(System.Single)
extern void ScannerVision_set_NoiseBandWidth_m3AC0A73C590D995E2E42D2B8D0BAA756E49D13B3 (void);
// 0x000000CE System.Single Nephasto.VisionFXAsset.ScannerVision::get_NoiseBandSpeed()
extern void ScannerVision_get_NoiseBandSpeed_m7FE18A144797F466A9A1483F358D996F219C9F16 (void);
// 0x000000CF System.Void Nephasto.VisionFXAsset.ScannerVision::set_NoiseBandSpeed(System.Single)
extern void ScannerVision_set_NoiseBandSpeed_m827BCDA9342733A5076893D7A3A948684FF32F58 (void);
// 0x000000D0 System.String Nephasto.VisionFXAsset.ScannerVision::ToString()
extern void ScannerVision_ToString_m0758A0067B8A06FA6D5A6AD4974BC918331FC849 (void);
// 0x000000D1 System.Void Nephasto.VisionFXAsset.ScannerVision::ResetDefaultValues()
extern void ScannerVision_ResetDefaultValues_mF7E752B62686817877EA9C5BA6D66E8F7A95E323 (void);
// 0x000000D2 System.Void Nephasto.VisionFXAsset.ScannerVision::UpdateCustomValues()
extern void ScannerVision_UpdateCustomValues_m8CF4E59541E0D347C5602D2112783A45DB2E33FE (void);
// 0x000000D3 System.Void Nephasto.VisionFXAsset.ScannerVision::.ctor()
extern void ScannerVision__ctor_m24ECD54BBC133724409A898E009041AFC3B99496 (void);
// 0x000000D4 System.Void Nephasto.VisionFXAsset.ScannerVision::.cctor()
extern void ScannerVision__cctor_mA22B50F07A749249141A3AA9A6034FC688640B46 (void);
// 0x000000D5 System.Single Nephasto.VisionFXAsset.ShakeVision::get_Magnitude()
extern void ShakeVision_get_Magnitude_mC5F2F64E60EEF3DC757C48E52892D327319F1F17 (void);
// 0x000000D6 System.Void Nephasto.VisionFXAsset.ShakeVision::set_Magnitude(System.Single)
extern void ShakeVision_set_Magnitude_m5BC8E3D3DD30B9427439055BEE41DA3AAF65EAFF (void);
// 0x000000D7 System.Single Nephasto.VisionFXAsset.ShakeVision::get_Intensity()
extern void ShakeVision_get_Intensity_m9DE22868C08E9E37112C7AEFBDCE9E7B42F0E505 (void);
// 0x000000D8 System.Void Nephasto.VisionFXAsset.ShakeVision::set_Intensity(System.Single)
extern void ShakeVision_set_Intensity_m8EFD7E03A511C4E3A5EADE01B342FC72EE151912 (void);
// 0x000000D9 System.String Nephasto.VisionFXAsset.ShakeVision::ToString()
extern void ShakeVision_ToString_mC8D917949F9129F63C72236901BB8D9BD9438B4A (void);
// 0x000000DA System.Void Nephasto.VisionFXAsset.ShakeVision::ResetDefaultValues()
extern void ShakeVision_ResetDefaultValues_mBB1E2CB30A1ACB942CD1BD7848F4B06E970F5A3E (void);
// 0x000000DB System.Void Nephasto.VisionFXAsset.ShakeVision::UpdateCustomValues()
extern void ShakeVision_UpdateCustomValues_m349D9B220DC4CE42E4294794DC30D9CD8BF6FAB4 (void);
// 0x000000DC System.Void Nephasto.VisionFXAsset.ShakeVision::.ctor()
extern void ShakeVision__ctor_m11476BC8D9922AD957A0E115BCFD323D96D3791E (void);
// 0x000000DD System.Single Nephasto.VisionFXAsset.TrippyVision::get_Speed()
extern void TrippyVision_get_Speed_mDDA9986288A14B4B8C31FA7F307BCB85CDD7DD96 (void);
// 0x000000DE System.Void Nephasto.VisionFXAsset.TrippyVision::set_Speed(System.Single)
extern void TrippyVision_set_Speed_m9AF9D58778160902659421450A25FE9A0021BFA6 (void);
// 0x000000DF System.Single Nephasto.VisionFXAsset.TrippyVision::get_Definition()
extern void TrippyVision_get_Definition_m6A362447827F8CE95E6CC173B75384E2945375F4 (void);
// 0x000000E0 System.Void Nephasto.VisionFXAsset.TrippyVision::set_Definition(System.Single)
extern void TrippyVision_set_Definition_m4E40DE8EEEA6664B72D52AECEA94932760D91BBE (void);
// 0x000000E1 System.Single Nephasto.VisionFXAsset.TrippyVision::get_Displacement()
extern void TrippyVision_get_Displacement_m23A13B5E3BD48ABCD1B8DD95011D17CA92B57800 (void);
// 0x000000E2 System.Void Nephasto.VisionFXAsset.TrippyVision::set_Displacement(System.Single)
extern void TrippyVision_set_Displacement_m48C6DBAC391F70AB8852F47CB3E159EA1312AEFB (void);
// 0x000000E3 Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.TrippyVision::get_BlendOp()
extern void TrippyVision_get_BlendOp_m04C4C0BE35096EA7C7F22B7EFEEE0A566242DE04 (void);
// 0x000000E4 System.Void Nephasto.VisionFXAsset.TrippyVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
extern void TrippyVision_set_BlendOp_m74142BBF899E15422A0D4190D4724D346EF2A273 (void);
// 0x000000E5 System.String Nephasto.VisionFXAsset.TrippyVision::ToString()
extern void TrippyVision_ToString_m854B36DA0B7B01C5D2BD77FB1F508B9B8DBC4066 (void);
// 0x000000E6 System.Void Nephasto.VisionFXAsset.TrippyVision::ResetDefaultValues()
extern void TrippyVision_ResetDefaultValues_m5D1698934B7568161537E570E3D093264776BFA5 (void);
// 0x000000E7 System.Void Nephasto.VisionFXAsset.TrippyVision::UpdateCustomValues()
extern void TrippyVision_UpdateCustomValues_mDA2FF586E662851B3DEE77FE8D834AA10E0CDFC4 (void);
// 0x000000E8 System.Void Nephasto.VisionFXAsset.TrippyVision::.ctor()
extern void TrippyVision__ctor_mBF61D5ED0F8CACC066A08BDC52290091A8DAFCA3 (void);
static Il2CppMethodPointer s_methodPointers[232] = 
{
	AnimeVision_get_Aspect_mB073503C7B7CFE404902B09ACFB256065E6E2796,
	AnimeVision_set_Aspect_m83341060905D80FA9BDCB5A73941FCC8D4A746A7,
	AnimeVision_get_Radius_m6A4E0E1E903752851EEE834BE3BFA750716AE949,
	AnimeVision_set_Radius_mEC02789CFB79BE90410B1F2D250AC0458430B072,
	AnimeVision_get_Length_mD9AE86F7C3729A3766367BB6C5974B720BD36CE2,
	AnimeVision_set_Length_m1478ED2D15BE9772756E9FD934BC9C151B62B984,
	AnimeVision_get_Speed_m66384BB4102F8FA6909C34A1CDD1F52A955D806B,
	AnimeVision_set_Speed_m99A49F6FB16DC300EE862271AC9291E97597A487,
	AnimeVision_get_Frequency_m85476250961107AD83F21CE580316F404B1DB334,
	AnimeVision_set_Frequency_mD771398CC9AEC974CE526E300271460344823125,
	AnimeVision_get_Softness_m050ADED0114E6EF60E6D202406C5B578B102D646,
	AnimeVision_set_Softness_m7DD19CDF249ED896FC1E0432419810962A6C8C6A,
	AnimeVision_get_Noise_mC039F94FBC114B22C28C6D9E778B2313C966C2A0,
	AnimeVision_set_Noise_m14F4BB38234AD397CF1DA74AECFFA1376CA3F600,
	AnimeVision_get_Color_mF4EF319EC229346A9C98BE9BDECDD34257A22003,
	AnimeVision_set_Color_mA089C79B327F9BB242A6ACCE6C0B4EDDA8ECCC39,
	AnimeVision_ToString_mB60B816FA2E1FD42091DA8EC2C37E9A2E73DA71D,
	AnimeVision_ResetDefaultValues_m0BC537B00F913B57DDF7794BF788F5E417012920,
	AnimeVision_UpdateCustomValues_m204F25B9FFF7DE1E4CACD355E87105F1909B4FF4,
	AnimeVision__ctor_m28BCA3626742E7EF5E2E63832FBB26DA30698C96,
	BaseVision_get_Amount_mA9FE6409D906BEAA982D5437F3AF0A32E4CE4137,
	BaseVision_set_Amount_m8C4649A6348E5360407D7CF2A7186696FC9AA17D,
	BaseVision_get_EnableColorControls_m808CE4D9098FC170665E5C3611DD808C552A85D1,
	BaseVision_set_EnableColorControls_m90B1805A3BD4A7BD6297A110D0FE51885491E0F2,
	BaseVision_get_Brightness_m1F87DA0E273C13EFABC2FC97EB92EC7CADB3E3B5,
	BaseVision_set_Brightness_m7F37E0C2419EEDD986F8D60A8FD90CF9F6117031,
	BaseVision_get_Contrast_m67F6D3EF430A9A4D552CD2CBEF9BB3E4A3248A2B,
	BaseVision_set_Contrast_m1D95AAE06CD95847EAA3BFC4EE43A62DA5E61DD7,
	BaseVision_get_Gamma_m301B80B2EF3702359747DDB1ADADC860F01053F7,
	BaseVision_set_Gamma_m1D429D39789A17796CBBE912B7BFDA885823AEFD,
	BaseVision_get_Hue_m9B64478243F9ED1FF56D188FD13E747F0D8401DA,
	BaseVision_set_Hue_mBF12E8C8A207F7E1AC1A0F62E312D6942746CDA9,
	BaseVision_get_Saturation_m00B5C8CBEFDE08DCDB9B1281318D19F4A7E140FC,
	BaseVision_set_Saturation_m1A59BCDDEA6865F7CC5DE85EC10CFB94708915FE,
	BaseVision_ToString_m23C6C56A339882F33A2B70726FDD7438A5E4CED9,
	BaseVision_ShaderPath_mB97266F3BD1DC2961F8F3FD19C837288259199E3,
	BaseVision_IsSupported_m23ECCD564B858B8DD17E0B8C2B23FCC2BC338A5C,
	BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55,
	BaseVision_LoadCustomResources_mA23FD8A6CF9199FEE77C73F29CA140F5488168F3,
	BaseVision_UpdateCustomValues_m4A342E05C24748D3FC0345D1732D182434D1747D,
	BaseVision_OnRenderImageCustom_m7B49EB592EE54AA044F8E50C7CF87653DF79CD77,
	BaseVision_Start_m2DA036D3871AD440488E8BE10BEAF96B19F75A64,
	BaseVision_OnDestroy_mF9212175388E2F458A4A99CD997762CCA34AA5F2,
	BaseVision_OnRenderImage_m35FC86D73A700A42DD62ED0BB2828254561A653C,
	BaseVision_UpdateAmount_mBAFE0A79B0786D5F9C2BE9C10E1D87F544FB91BE,
	BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0,
	BlurryVision_get_Frames_mBDD377E7682F16FFAE5252B9A85E52DAFBA7C34D,
	BlurryVision_set_Frames_mEDE05C04107132EBC3BE7556C70B03B00908757A,
	BlurryVision_get_FrameStep_m1710CC85257CB696A50BA43C100D6A7606DCE5CF,
	BlurryVision_set_FrameStep_mC8A92B55246EDC92DAE743C5FC99E1C50E393C42,
	BlurryVision_get_FrameResolution_m5248545B4E9A02D2A8D0F1515303809561ED6BED,
	BlurryVision_set_FrameResolution_m9D14A1EED55628F641EB6D71EA953F74811C4C4A,
	BlurryVision_ToString_m9BE1E2C2A5E9B3EB1455A0B5DC3A31440CA093A7,
	BlurryVision_ResetDefaultValues_mAC1FA901AF7594F5D92684BF08052CB112C7BA95,
	BlurryVision_UpdateCustomValues_m2F1BA05DB9C82246E7C115AD9212D1E93C5B0347,
	BlurryVision_OnRenderImageCustom_m17CF0AA8D8349C1AC08410CBC017C76D41ED7072,
	BlurryVision_AddFrame_m888550A7CDD3C07690FBF52DAF1EF3CC99579DA6,
	BlurryVision_OnDisable_mAC925BA4D6D079AE7B34CC0B5D26AB1FB6ED13CC,
	BlurryVision__ctor_mB7375F100600AFF4CDE0118FA0B6ED5F4214039F,
	BlurryVision__cctor_mCDA96F243D035F825CB2025D8D375EDBA7C64863,
	DamageVision_get_Damage_m3B389F9545D5E6B3E7449D615EC32185BC5A8653,
	DamageVision_set_Damage_mE5091A0BCC674DCDCD01D5A5213F5502DB680B58,
	DamageVision_get_Definition_m9EC66FD30DB6C7E30C3159C7AB3F805262707AD7,
	DamageVision_set_Definition_mA588E3382EEC0A47F781ED8C03F0ACCF28777C6A,
	DamageVision_get_DamageBrightness_mC352DB8757B02B7584C22A4E878922EF130BB318,
	DamageVision_set_DamageBrightness_mF3DF2E8B65D29DD6020239E4C25DBFF465098D61,
	DamageVision_get_Distortion_m8FD64ADE103A82FFF9809C51956A8BF79A7DA5AB,
	DamageVision_set_Distortion_m3C879334D11B6AD279EA0836E2D8E0A181793211,
	DamageVision_get_BlendOp_m10552EF529E51F1DAE8B4DFB1902463711663ABC,
	DamageVision_set_BlendOp_mE920DBF5873CEA81E501C4F2857DA68DF716B482,
	DamageVision_get_Color_mFF98D32F5ED974D353B74BACCD8AD9C8BC271956,
	DamageVision_set_Color_m1BEC4CE7523844F95A7D199514D67E6555AE66C3,
	DamageVision_ToString_m261DF2105A8333DC45C91CEF78B33658F11C02B1,
	DamageVision_ResetDefaultValues_m31DA397339F31942D18066FC8966AC46C64BC855,
	DamageVision_LoadCustomResources_mD3A2182F44DC2F392B2F212ADA3FBDFC081D1674,
	DamageVision_UpdateCustomValues_m93BA301691BCB93197C020AA31D09C17B1773D9F,
	DamageVision__ctor_mB674BCE67E8C42EDD08A1C22BBEA969C4A3BA218,
	DoubleVision_get_Strength_mB9E9A9E73A0314F82698341278A5D97A2AE92CE5,
	DoubleVision_set_Strength_m13BDA79AD7DDAB8640CB09BD299455073BC240F9,
	DoubleVision_get_Speed_mFC79A64332AA5A2EB6891BE7D8F1C70257339CF9,
	DoubleVision_set_Speed_m118646F710BEAEB6980D7D7C054EE2E99B092192,
	DoubleVision_ToString_mA9E878C475CE68B48DEE9C4EAE1E93B762F1607D,
	DoubleVision_ResetDefaultValues_m54A36A8861090336297E02A63B4A1084DDAA703C,
	DoubleVision_UpdateCustomValues_m1CAD383C98437D047D768FD09AECA31E2477FB43,
	DoubleVision__ctor_mE79DF73D8C9C23C94DE033103BDA67EEFE1D8F9F,
	DoubleVision__cctor_m3A0A0E1B92D2D67C72AE41BA083F915FED9DE273,
	DrunkVision_get_Drunkenness_m4B016A097A788F736B5DE622D9BA46E97D925AD1,
	DrunkVision_set_Drunkenness_m22A0CCB96479F6D472435D98002D61A21D21D08B,
	DrunkVision_get_DrunkSpeed_m069B97829B457AE4EEAEEFDF476516EF9EE717BB,
	DrunkVision_set_DrunkSpeed_m8CC31C58AB23DD8E35CF96AA5AE0480363F186FD,
	DrunkVision_get_DrunkAmplitude_mB90389424BB01FB4AE19B8F22928E7C9306C8AE2,
	DrunkVision_set_DrunkAmplitude_mFA64482EC551D6D182DD7D819ADF29D0168D5F77,
	DrunkVision_get_Swinging_m3C366DAF83D069A8CD16524F6E3E12EBB229D1BB,
	DrunkVision_set_Swinging_mA9E15E8F56EEA55A95B80771E6336E04EE20946D,
	DrunkVision_get_SwingingSpeed_m093AB6ED7DD44808C1FDADCEC8451EFD1A32726B,
	DrunkVision_set_SwingingSpeed_m15921BEA569B0C9FE0714963F02EFB8E2AB20749,
	DrunkVision_get_Aberration_mDBB055641D7215504F04CFB2B425F1AC71F171F2,
	DrunkVision_set_Aberration_m6807E76B95BEC0A3AA35233B3AEB1AD4C57FB0B0,
	DrunkVision_get_AberrationSpeed_m3A9B650322165AA14CC45E6C21BD597C56B08BE2,
	DrunkVision_set_AberrationSpeed_m238F0D49A3DCD809EECFC66E67C2D23A5132B5E9,
	DrunkVision_get_VignetteAmount_mEB5EBF619ECD1C3FE22D03E64273AB54E528D1E4,
	DrunkVision_set_VignetteAmount_mF06F4693F902C822AD6031D981509BB9D7440D10,
	DrunkVision_get_VignetteSpeed_m12B22964A7387C240F8794AC387BDD079AC952F1,
	DrunkVision_set_VignetteSpeed_m30B905F789F8F8AA4DD1F82A23FCB6B07F752E11,
	DrunkVision_ToString_m5E2397D205484F5602471F383E43B84AC16D297E,
	DrunkVision_ResetDefaultValues_mCBAE8D64F6831E8D00AFD8D1BD56DB06430A659A,
	DrunkVision_UpdateCustomValues_m0E49E4633C48B1EADF2549E1ABA288E84391EC81,
	DrunkVision__ctor_m65844EE71BF1E3090842E3BB534EE7BF5F3B45F8,
	FisheyeVision_get_Barrel_mAAD9A51149E28D4A00A3DFE9B63BA6A361D8B5D9,
	FisheyeVision_set_Barrel_m37B83913C6FA9D7AC390A0E1780DD1BD78EA8E81,
	FisheyeVision_get_Center_mEE360783F0434E068537CCA0E2D18152A23F342A,
	FisheyeVision_set_Center_mE3CD2BEE1DB645FC2F0952663F0EC7CDFB02F436,
	FisheyeVision_get_BackgroundColor_m4C1638597D45BAABE293DFC7CF5F4BF8076C74A4,
	FisheyeVision_set_BackgroundColor_mF4E697ED60D439AA4D84014864D0B2A6ABFA1E7D,
	FisheyeVision_ToString_m0A40DC40AF2061064EE417C7F0467DA2C5F8259B,
	FisheyeVision_ResetDefaultValues_m1F34DADA7875EA98DDC98EA355E86A09B39629F1,
	FisheyeVision_UpdateCustomValues_m74569483C1F3CA0A4F85A2CBE5F53157867F671A,
	FisheyeVision__ctor_m88459E2AF52EC50B48DB49523E9ACB1219EDB675,
	FisheyeVision__cctor_m503B13CA7B03D80598AA7908C2B1D65CC180684F,
	GhostVision_get_Focus_m83D84B2DC62539643769BBF5DE8BEA4AB63320B9,
	GhostVision_set_Focus_m1361CF231D3A8CA22A0BD0332EF648328FB396CD,
	GhostVision_get_Speed_m5C00C31696069669807AB4D0B029088A5C0B9776,
	GhostVision_set_Speed_m2FB5AE8C17718F9E7210D7F08C5D28E8359C2803,
	GhostVision_get_Aperture_m1DD5C011D9C4AFC7B7A3C8ABAC480D1788C1034D,
	GhostVision_set_Aperture_mBE8788395C1780BF42E70E638C26DBE0AC3769C9,
	GhostVision_get_Zoom_m63F6446A85E4C76142E037CC263BD826488AA34E,
	GhostVision_set_Zoom_m49EA436E0F80507BB10CE1804CABDBE5CA27848B,
	GhostVision_get_ChangeFOV_m17E79258473DECCB1257AD41277383426DF1F8A5,
	GhostVision_set_ChangeFOV_m23B07FEBD0292E7196DCA1F75C2F29C8DC9BADB1,
	GhostVision_get_FOV_mF978D1FF6DBF582DC970192D968D2899BDBACF83,
	GhostVision_set_FOV_mFA1A48F5B512AFA399F0A217FCD67728A592B2DD,
	GhostVision_get_AspectRatio_m2187C3B59300892B3F309D7586A3739216955765,
	GhostVision_set_AspectRatio_mD74BF7C02D42D96BBF3C5ADB9EBC14FC1BCA1599,
	GhostVision_get_InnerTint_m8969F191DE9F23C5E19F4AA785992C623F2109D8,
	GhostVision_set_InnerTint_m7BC7A5641027B43326A85B983DBE271B3010D840,
	GhostVision_get_InnerSaturation_mD37E162D3523CC815CB6778E88CE5D570DC7758B,
	GhostVision_set_InnerSaturation_mA994685C317B066B30D5ED1D3EDBD13B9860827A,
	GhostVision_get_InnerBrightness_m96A4A295088DC3B47C3C35E6BBBDE96569E2DE99,
	GhostVision_set_InnerBrightness_mBCFC4C6195C3CE3237FD8F799D6948FA1C1667C5,
	GhostVision_get_InnerContrast_mEDD2E7BC9B5664B5FE3FD49DF2268FDDA5105D33,
	GhostVision_set_InnerContrast_m71C82DA577CCA4CEE6F0D2ED6420B2569D99A737,
	GhostVision_get_InnerGamma_m85598BBD7343801AD22ADA1BC0F11394B862478A,
	GhostVision_set_InnerGamma_mB04F93900371B0CE63392305A6664EEC38523C81,
	GhostVision_get_OuterTint_mFAE2D10C6BC94043F3176FCB973722AA14482728,
	GhostVision_set_OuterTint_m243D017D05941624AF21A59AEEFEC60EE6F69913,
	GhostVision_get_OuterSaturation_mB27D4E2BF9374B1295A410B384A28C0C06DC8D45,
	GhostVision_set_OuterSaturation_m419E899A64DD9003997EB5B4DD68042818FAC982,
	GhostVision_get_OuterBrightness_m5A5B9513D6260849865FE58BC07F66AC120F3B4B,
	GhostVision_set_OuterBrightness_m7ABB33C407071C70C543D9969B39EE296F400B7E,
	GhostVision_get_OuterContrast_m075E41572DB0668B7EED3F0CED2D93E39B261CB4,
	GhostVision_set_OuterContrast_m9AF72FEE6A20EAC7D8FE2090005468887BA5A0D6,
	GhostVision_get_OuterGamma_m88EB076011A9B3117840B8487BC0B6151E5B5AE1,
	GhostVision_set_OuterGamma_m67CEA335601A0DA541354594039005417F664B0F,
	GhostVision_ToString_mA30CA772F008A8A6C7023C4621090F198089A8EA,
	GhostVision_ResetDefaultValues_m67FF09673E901C5B10FE4952C11F4B91D235E42B,
	GhostVision_UpdateCustomValues_mFC2177522A0CAD865C3D389C17C44C55A849ED68,
	GhostVision_OnEnable_m251F916A1ED1AE8EF7AC8DFE62CBEF17555A47B9,
	GhostVision_OnDisable_m2C826469B02BC9FF6165AE9DFAA766A00356E8C5,
	GhostVision__ctor_m8EA9624E9A09B44D6051AA5BE3C64CF357891E60,
	HalftoneVision_get_Size_mFCEDB2CA788D25C089EDF59686B27C3B8A993D3A,
	HalftoneVision_set_Size_mBAA26CC671B6A526F8750FDB79D895E57553EFAF,
	HalftoneVision_get_Angle_m34FEC663E7EEC7C323A502A6B086B1F9EE730DBA,
	HalftoneVision_set_Angle_m44821AB81DC4949B95BA450BB7D1726436BB67FC,
	HalftoneVision_get_Strength_m8997579AE96AECB574927F0B7260A688036A59DA,
	HalftoneVision_set_Strength_mE4FECF7189436D3F67B0471FC636EE82DC180DB0,
	HalftoneVision_get_Sensitivity_mEC20D7774E7358F9C9C953F77C9E5AAE5B09743B,
	HalftoneVision_set_Sensitivity_m59110CEE59A294F100A71EAEFA8E3D18027E53FD,
	HalftoneVision_get_BlendOp_m493F1ED9FCDBB9A453869020BB7C4CAFA36DA729,
	HalftoneVision_set_BlendOp_m5B2929BB9E1B99933A0E5E64A913BC6A284B36F2,
	HalftoneVision_ToString_m4F175FBF603612DF2E8D0DD11180908956AF201D,
	HalftoneVision_ResetDefaultValues_m6B490D287DF7D3D83C54F9D4AB915ECD76148741,
	HalftoneVision_UpdateCustomValues_m757839AF3E6B76D3CE9579D3E3FB53BDC8AF2B47,
	HalftoneVision__ctor_mD4FBB79CF42EBC1ED43E2C690F352C9570F9BAFE,
	LegoVision_get_Size_mCE50096D5A20222DA3B3513106F46E5DD2B052D3,
	LegoVision_set_Size_m6B1ABF493EB865C916D1E322B9406513003825B5,
	LegoVision_ToString_mF819F287C76F37760F4F5006E02309F8290E10FB,
	LegoVision_ResetDefaultValues_m19E12C5BA193F3F4EE11DDE3B76EBB678DAB75E1,
	LegoVision_UpdateCustomValues_mB85AF7F250A205E53AE512DD2CA1373BD17C4661,
	LegoVision__ctor_mAC100C80832FD5D6EA9657D46C87AA4A352DBF23,
	NeonVision_get_Edge_m42C8DE5EBAA9A4A081430C3E946B5BC7A3992E41,
	NeonVision_set_Edge_m9FE4E4670324C3DB06E0236E1F74C674FB94EE24,
	NeonVision_get_EdgeColor_mFB1C5552E8FE52E0DBC7981DD973D960C8A8A7A0,
	NeonVision_set_EdgeColor_m9E47E5E6F3152124A6448A44C88ECABDCDA456B8,
	NeonVision_get_BlendOp_m35F3D7D34D8DB6501285AF64B836BAE208EE6781,
	NeonVision_set_BlendOp_m9132DF988C8E327BFA8C01B6EDF1634385FB108D,
	NeonVision_ToString_mB480DB66FCEE0ECE89D1978D7D454134B50AB076,
	NeonVision_ResetDefaultValues_m3AC702468BDABB0D8176D6A52CA77DE8C6E695B7,
	NeonVision_UpdateCustomValues_m472BD8333BAFC14C769BAA57CE3C72C5C7577BAC,
	NeonVision__ctor_mB34F5BDCED971C25D45E9D60CC656BA793F66607,
	ScannerVision_get_Tint_m8A5FA01103095ACDA617B5DC0ECDFE0240FF5D6B,
	ScannerVision_set_Tint_m6AC479A0D97C41F3E54A82CD2BB7BF65C06DC68A,
	ScannerVision_get_LinesCount_m803AA34A8D5D9BFC26990EA264DE0878F3DEAB11,
	ScannerVision_set_LinesCount_m1F455A4E4010DCB3CD0547B722801B0A75A253C1,
	ScannerVision_get_LinesStrength_mB034D696CEFBC6A01EADCE83554F27F9051CDF8F,
	ScannerVision_set_LinesStrength_m1A7BB0E7DE3C6E3A96A4A94D8C3ADE33908FBDE0,
	ScannerVision_get_ScanLineStrength_m2F3ECAC627497CC0308F2A37386EF0DAFF99A260,
	ScannerVision_set_ScanLineStrength_m95525F7037CE5BAF401F05405FDA506B1FC9A2C0,
	ScannerVision_get_ScanLineWidth_m450296C6F54ADBE4D99A798D0DBE26D9373833AF,
	ScannerVision_set_ScanLineWidth_m2AE6E2AF895BBFD97A6A2DB78FEE0F04AF47DBCE,
	ScannerVision_get_ScanLineSpeed_m679F75FA1E982DE43337AD3ED7D93774D5104A12,
	ScannerVision_set_ScanLineSpeed_m66831366B4A4A9BEF602F09D7C7CCBC48AC8898E,
	ScannerVision_get_NoiseBandStrength_mFBCC432C88BDD14E73C0A599D3837A9F7491DCE5,
	ScannerVision_set_NoiseBandStrength_m1E309EB96416F8253ED69E667D526B96F176810C,
	ScannerVision_get_NoiseBandWidth_mD404013579053FD58F0C02A13B096798DA48B386,
	ScannerVision_set_NoiseBandWidth_m3AC0A73C590D995E2E42D2B8D0BAA756E49D13B3,
	ScannerVision_get_NoiseBandSpeed_m7FE18A144797F466A9A1483F358D996F219C9F16,
	ScannerVision_set_NoiseBandSpeed_m827BCDA9342733A5076893D7A3A948684FF32F58,
	ScannerVision_ToString_m0758A0067B8A06FA6D5A6AD4974BC918331FC849,
	ScannerVision_ResetDefaultValues_mF7E752B62686817877EA9C5BA6D66E8F7A95E323,
	ScannerVision_UpdateCustomValues_m8CF4E59541E0D347C5602D2112783A45DB2E33FE,
	ScannerVision__ctor_m24ECD54BBC133724409A898E009041AFC3B99496,
	ScannerVision__cctor_mA22B50F07A749249141A3AA9A6034FC688640B46,
	ShakeVision_get_Magnitude_mC5F2F64E60EEF3DC757C48E52892D327319F1F17,
	ShakeVision_set_Magnitude_m5BC8E3D3DD30B9427439055BEE41DA3AAF65EAFF,
	ShakeVision_get_Intensity_m9DE22868C08E9E37112C7AEFBDCE9E7B42F0E505,
	ShakeVision_set_Intensity_m8EFD7E03A511C4E3A5EADE01B342FC72EE151912,
	ShakeVision_ToString_mC8D917949F9129F63C72236901BB8D9BD9438B4A,
	ShakeVision_ResetDefaultValues_mBB1E2CB30A1ACB942CD1BD7848F4B06E970F5A3E,
	ShakeVision_UpdateCustomValues_m349D9B220DC4CE42E4294794DC30D9CD8BF6FAB4,
	ShakeVision__ctor_m11476BC8D9922AD957A0E115BCFD323D96D3791E,
	TrippyVision_get_Speed_mDDA9986288A14B4B8C31FA7F307BCB85CDD7DD96,
	TrippyVision_set_Speed_m9AF9D58778160902659421450A25FE9A0021BFA6,
	TrippyVision_get_Definition_m6A362447827F8CE95E6CC173B75384E2945375F4,
	TrippyVision_set_Definition_m4E40DE8EEEA6664B72D52AECEA94932760D91BBE,
	TrippyVision_get_Displacement_m23A13B5E3BD48ABCD1B8DD95011D17CA92B57800,
	TrippyVision_set_Displacement_m48C6DBAC391F70AB8852F47CB3E159EA1312AEFB,
	TrippyVision_get_BlendOp_m04C4C0BE35096EA7C7F22B7EFEEE0A566242DE04,
	TrippyVision_set_BlendOp_m74142BBF899E15422A0D4190D4724D346EF2A273,
	TrippyVision_ToString_m854B36DA0B7B01C5D2BD77FB1F508B9B8DBC4066,
	TrippyVision_ResetDefaultValues_m5D1698934B7568161537E570E3D093264776BFA5,
	TrippyVision_UpdateCustomValues_mDA2FF586E662851B3DEE77FE8D834AA10E0CDFC4,
	TrippyVision__ctor_mBF61D5ED0F8CACC066A08BDC52290091A8DAFCA3,
};
static const int32_t s_InvokerIndices[232] = 
{
	6271,
	5041,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6125,
	4901,
	6226,
	6341,
	6341,
	6341,
	6281,
	5052,
	6271,
	5041,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6226,
	6226,
	6271,
	6341,
	6271,
	6341,
	2760,
	6341,
	6341,
	2760,
	5052,
	6341,
	6192,
	4967,
	6192,
	4967,
	6281,
	5052,
	6226,
	6341,
	6341,
	2760,
	5000,
	6341,
	6341,
	11271,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6192,
	4967,
	6125,
	4901,
	6226,
	6341,
	6271,
	6341,
	6341,
	6329,
	5093,
	6329,
	5093,
	6226,
	6341,
	6341,
	6341,
	11271,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6226,
	6341,
	6341,
	6341,
	6281,
	5052,
	6329,
	5093,
	6125,
	4901,
	6226,
	6341,
	6341,
	6341,
	11271,
	6329,
	5093,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6271,
	5041,
	6281,
	5052,
	6271,
	5041,
	6125,
	4901,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6125,
	4901,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6226,
	6341,
	6341,
	6341,
	6341,
	6341,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6192,
	4967,
	6226,
	6341,
	6341,
	6341,
	6281,
	5052,
	6226,
	6341,
	6341,
	6341,
	6281,
	5052,
	6125,
	4901,
	6192,
	4967,
	6226,
	6341,
	6341,
	6341,
	6125,
	4901,
	6192,
	4967,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6226,
	6341,
	6341,
	6341,
	11271,
	6281,
	5052,
	6281,
	5052,
	6226,
	6341,
	6341,
	6341,
	6281,
	5052,
	6281,
	5052,
	6281,
	5052,
	6192,
	4967,
	6226,
	6341,
	6341,
	6341,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_com_nephasto_VisionFX_CodeGenModule;
const Il2CppCodeGenModule g_com_nephasto_VisionFX_CodeGenModule = 
{
	"com.nephasto.VisionFX.dll",
	232,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
