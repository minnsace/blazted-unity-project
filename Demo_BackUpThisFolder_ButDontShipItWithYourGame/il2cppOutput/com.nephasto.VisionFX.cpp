﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtualActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<UnityEngine.RenderTexture>
struct List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// Nephasto.VisionFXAsset.AnimeVision
struct AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B;
// Nephasto.VisionFXAsset.BaseVision
struct BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5;
// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// Nephasto.VisionFXAsset.BlurryVision
struct BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// Nephasto.VisionFXAsset.DamageVision
struct DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B;
// Nephasto.VisionFXAsset.DoubleVision
struct DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D;
// Nephasto.VisionFXAsset.DrunkVision
struct DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721;
// Nephasto.VisionFXAsset.FisheyeVision
struct FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// Nephasto.VisionFXAsset.GhostVision
struct GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8;
// Nephasto.VisionFXAsset.HalftoneVision
struct HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB;
// Nephasto.VisionFXAsset.LegoVision
struct LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// Nephasto.VisionFXAsset.NeonVision
struct NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
// Nephasto.VisionFXAsset.ScannerVision
struct ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D;
// UnityEngine.Shader
struct Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692;
// Nephasto.VisionFXAsset.ShakeVision
struct ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// Nephasto.VisionFXAsset.TrippyVision
struct TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562;
// System.Type
struct Type_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD;

IL2CPP_EXTERN_C RuntimeClass* BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral04273BAC62A956C2EEF06CFD0A70976AD2916A53;
IL2CPP_EXTERN_C String_t* _stringLiteral064CA8962C00CA877F6164CAF986CDF79BB90B2F;
IL2CPP_EXTERN_C String_t* _stringLiteral08F73AC307A278B2744E30B89E9C4C2A97C43325;
IL2CPP_EXTERN_C String_t* _stringLiteral1118AFABE1103004CB863B78763034F9F50EA9E3;
IL2CPP_EXTERN_C String_t* _stringLiteral14D9322CF96A4FE0E169C0D11305DBC8706092E4;
IL2CPP_EXTERN_C String_t* _stringLiteral14F4FFFE4680A2B7D9EB6BD6180E9097EACB7BDA;
IL2CPP_EXTERN_C String_t* _stringLiteral19D5E3679B306565C2EE15BC76016508CC80BD96;
IL2CPP_EXTERN_C String_t* _stringLiteral1E55BC8EC00B694817EDB2E59B23EC798FEE4C6B;
IL2CPP_EXTERN_C String_t* _stringLiteral224074E0D1F5E22925D4812636CDA330DF2AD236;
IL2CPP_EXTERN_C String_t* _stringLiteral279E9DAEE8430F8DA304C3E3D7D1A0E3EBB41E05;
IL2CPP_EXTERN_C String_t* _stringLiteral2ABEEC7903CD21A4DE31201A5A876A061D112BB1;
IL2CPP_EXTERN_C String_t* _stringLiteral2C31C686E0C84134B4E5C55FE160ABE4C0D5031B;
IL2CPP_EXTERN_C String_t* _stringLiteral2D05D4C6010455CF7DA97873EF3F1AC6B9504472;
IL2CPP_EXTERN_C String_t* _stringLiteral2D67459F19AB5AE7690FA37D805F279C14A14A81;
IL2CPP_EXTERN_C String_t* _stringLiteral3078FE4A752791FDA320A7C597D705164ECAB614;
IL2CPP_EXTERN_C String_t* _stringLiteral3492589282A6553D7A5D2193C5BA34B27EA87D61;
IL2CPP_EXTERN_C String_t* _stringLiteral34DBF2F9982594656913F0861C8E8F05CA2CB5F0;
IL2CPP_EXTERN_C String_t* _stringLiteral39E3629B886CB412720ADA081113F5133F78CE75;
IL2CPP_EXTERN_C String_t* _stringLiteral3AA10C2FCFFCD4A77470F2CC06E2B11F1ED772C1;
IL2CPP_EXTERN_C String_t* _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE;
IL2CPP_EXTERN_C String_t* _stringLiteral4A01F66901FD2FF9020FDBA4575B3CBEF98F22D1;
IL2CPP_EXTERN_C String_t* _stringLiteral4C29B0DC949688A38E911584746B509F58451F2C;
IL2CPP_EXTERN_C String_t* _stringLiteral4E207A1E776F6188653FF9228A95BFD3A17B492E;
IL2CPP_EXTERN_C String_t* _stringLiteral4F0E52BEEBAF3EF55547FBF053786CFF88BF6033;
IL2CPP_EXTERN_C String_t* _stringLiteral543BE502C8CE19D4D92897D3D3DDE32FB6563EBF;
IL2CPP_EXTERN_C String_t* _stringLiteral59B8BDA4E9611A9CC62C51C6532874CBF5BEC632;
IL2CPP_EXTERN_C String_t* _stringLiteral5EA6233810A41959161DE2291ED95019779143DC;
IL2CPP_EXTERN_C String_t* _stringLiteral5F57584D841BB0295BB4448AA948A9C130EA0EC6;
IL2CPP_EXTERN_C String_t* _stringLiteral6084B2BBB2C5FCC546818FFB9CDA1D3FD6A9FCF1;
IL2CPP_EXTERN_C String_t* _stringLiteral63BAD6EA5E5AB548045F7637E204F29F523ED954;
IL2CPP_EXTERN_C String_t* _stringLiteral64A6F97402A358B21F61FB07E26E6BD2DF1B8F5A;
IL2CPP_EXTERN_C String_t* _stringLiteral68D4A36825993A8577CF44620C403255105DC4BE;
IL2CPP_EXTERN_C String_t* _stringLiteral6976D94E970B572A49ADA3FC7AC12958BA079D3A;
IL2CPP_EXTERN_C String_t* _stringLiteral6BC4E8615279BB8565C8685C50E2B11EC89E47FF;
IL2CPP_EXTERN_C String_t* _stringLiteral6C5577D68A98EC6D7EE033290FEAA4FBFB520027;
IL2CPP_EXTERN_C String_t* _stringLiteral709E50200C27F9835679989FC5ADD0F1ADF14EC2;
IL2CPP_EXTERN_C String_t* _stringLiteral70C25592109D1296FB144FC169E0F31AE2533CA8;
IL2CPP_EXTERN_C String_t* _stringLiteral77F3383F9FF6D01CDD48AFF65AE7F93BEF1E4AA6;
IL2CPP_EXTERN_C String_t* _stringLiteral787984D270B549500FD6EE450785085D7058DF70;
IL2CPP_EXTERN_C String_t* _stringLiteral7B91B5FB0B511B2B078924DF85478132B50E3667;
IL2CPP_EXTERN_C String_t* _stringLiteral7EB1308A70D2205642092FE39384B0EAF8993A2D;
IL2CPP_EXTERN_C String_t* _stringLiteral8255FBADDC7358D4D3FB7875A39A1DDD61115D22;
IL2CPP_EXTERN_C String_t* _stringLiteral877B02515584E67B57946246F931E4FB9A50C8C4;
IL2CPP_EXTERN_C String_t* _stringLiteral8AB0A32AA70CEEEDF68589AA0B93CB1349239D55;
IL2CPP_EXTERN_C String_t* _stringLiteral8AE933F3053DE01242D43ABE7E2D8F9913D69F31;
IL2CPP_EXTERN_C String_t* _stringLiteral8BA48E25549E546BA07E0E2DCCA3614AF77655B2;
IL2CPP_EXTERN_C String_t* _stringLiteral8E5251C270515473520155396D1E117B2866B75C;
IL2CPP_EXTERN_C String_t* _stringLiteral930831AD69587F365337F7BD5E2D296692E0D130;
IL2CPP_EXTERN_C String_t* _stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E;
IL2CPP_EXTERN_C String_t* _stringLiteral96A5287C5C28694ED3FDEE4D1E8260C1D05BEB34;
IL2CPP_EXTERN_C String_t* _stringLiteral982614CA66F05802D4C005AA52F592157FABE7C5;
IL2CPP_EXTERN_C String_t* _stringLiteral996763F27BDBC420F9E68D5638DD77E3D840FD32;
IL2CPP_EXTERN_C String_t* _stringLiteral9AD9AC2A179FDCC5DF15DA875A0DAF5F51C43BCA;
IL2CPP_EXTERN_C String_t* _stringLiteral9D0E132481EE2A501A93D38AFFDCE49F9BAB411F;
IL2CPP_EXTERN_C String_t* _stringLiteral9E697C3514FCD429A08318C830A7FBC7F581D117;
IL2CPP_EXTERN_C String_t* _stringLiteral9E7B8EB481D432B24624CC9573E7AC2E516E7B43;
IL2CPP_EXTERN_C String_t* _stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F;
IL2CPP_EXTERN_C String_t* _stringLiteralA0F2ACB0B3F377939DE4177970C04C49B4898FE1;
IL2CPP_EXTERN_C String_t* _stringLiteralA4A5866BFA572A345EAE030A102601C627F2188E;
IL2CPP_EXTERN_C String_t* _stringLiteralA963E2F2B155C8743D4F8B4ACA239D9A76D29864;
IL2CPP_EXTERN_C String_t* _stringLiteralAA4D198597F2B72EF6D2A733B4F656C7D8ED45E0;
IL2CPP_EXTERN_C String_t* _stringLiteralAAC72F9935ADA683270704D6D9A3FD76302D8486;
IL2CPP_EXTERN_C String_t* _stringLiteralAC01AD67122E2D4C7D3238187DCE19D4B5623BE1;
IL2CPP_EXTERN_C String_t* _stringLiteralACA83AF7A62BB74E1867497F20E27DDA4AA09286;
IL2CPP_EXTERN_C String_t* _stringLiteralB328E9896615295D972D16BB5455163121D8592C;
IL2CPP_EXTERN_C String_t* _stringLiteralB5696D802E93F47F31F1E298FB7F74857D3CC019;
IL2CPP_EXTERN_C String_t* _stringLiteralB578CD2995766CACFF1C04975FE80041345AF9EB;
IL2CPP_EXTERN_C String_t* _stringLiteralB963B5280FC457D3D26C1A238E40134D80F48A39;
IL2CPP_EXTERN_C String_t* _stringLiteralB97DDA10212FE943A960F63E3B37BD26F1CF5A86;
IL2CPP_EXTERN_C String_t* _stringLiteralB9A1D99FCEB48E9EA569B020096B8F46208388A7;
IL2CPP_EXTERN_C String_t* _stringLiteralBD6F44625BEFB043FCCB42FD18E782DC3393475B;
IL2CPP_EXTERN_C String_t* _stringLiteralBFDB4B83E8F624B1E36286AB9AD4DBA5CB4D16B5;
IL2CPP_EXTERN_C String_t* _stringLiteralC041B83A217B198F836EC71B5B995A22F4B6738A;
IL2CPP_EXTERN_C String_t* _stringLiteralC088354B64B509F0C56825791A871020EB85B388;
IL2CPP_EXTERN_C String_t* _stringLiteralC78BA9E4E8D721124AD624C314C85968C659273C;
IL2CPP_EXTERN_C String_t* _stringLiteralCDE32AD10A2BADFD3344D8084D96E81419B04EE6;
IL2CPP_EXTERN_C String_t* _stringLiteralD401F59E8E1E4B3F003996F58B0812ED01CE6CC5;
IL2CPP_EXTERN_C String_t* _stringLiteralD4991844ED24CE6E2F134FB97A3708C79877E6BE;
IL2CPP_EXTERN_C String_t* _stringLiteralD4ACFAB09591C2A1F263839B86F2E7CEBC1792EB;
IL2CPP_EXTERN_C String_t* _stringLiteralDB299F9A83A42E3F18FC199F4F086A2559F322DB;
IL2CPP_EXTERN_C String_t* _stringLiteralDE2DD33BCD541DA280E529743F65CB84C9541BC9;
IL2CPP_EXTERN_C String_t* _stringLiteralDE9FAF482EBF07DB48E259D5B8D2B04A5EF5EFA9;
IL2CPP_EXTERN_C String_t* _stringLiteralDEC9DF998711C9677E152E279E1932D746A66EDC;
IL2CPP_EXTERN_C String_t* _stringLiteralE042C8EC1462638570B1B4320DC6ACC2FF0FF026;
IL2CPP_EXTERN_C String_t* _stringLiteralE20F6A9EA5C3CD8AB1008454765AC8A0748565F6;
IL2CPP_EXTERN_C String_t* _stringLiteralE5A1D96CE41BD597A30172C890508C2BB5810152;
IL2CPP_EXTERN_C String_t* _stringLiteralE78D830BB90BEE5985357C495C2DCB038A1AC60B;
IL2CPP_EXTERN_C String_t* _stringLiteralE8D178C06750E0D79C8AD3A8B0DFB2D6A8751A50;
IL2CPP_EXTERN_C String_t* _stringLiteralE9A91171153CFB6728156E1D688BD478DF0CEA09;
IL2CPP_EXTERN_C String_t* _stringLiteralECA169E2C11BA4F71637D2E1BF1D8ECAA03E8BF6;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCamera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_m3B3C11550E48AA36AFF82788636EB163CC51FEE6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m251977459109D48D068772EA3624051434EB6DB0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m164E41C28FA43F25F51C417135CF61976ECF3B69_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mD0891E1C91B860AD22385685DAAFF9988CA4CB49_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisTexture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_m301E95E824CB214DD0BA6D04221CE97B30BE9ACD_RuntimeMethod_var;

struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tA12149CFD6F4025759DFFCDCFF323C6405812024 
{
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.RenderTexture>
struct List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6* ___s_emptyArray_5;
};
struct Il2CppArrayBounds;

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};

struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_StaticFields
{
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___enumSeperatorCharArray_0;
};
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// System.Reflection.BindingFlags
struct BindingFlags_t5DC2835E4AE9C1862B3AD172EF35B6A5F4F1812C 
{
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;
};

// UnityEngine.HideFlags
struct HideFlags_tC514182ACEFD3B847988C45D5DB812FF6DB1BF4A 
{
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// Nephasto.VisionFXAsset.VisionBlendOps
struct VisionBlendOps_tE7D38B317ABF8B28836D2824A6967B8444500E2C 
{
	// System.Int32 Nephasto.VisionFXAsset.VisionBlendOps::value__
	int32_t ___value___2;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Shader
struct Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_StaticFields
{
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPostRender_6;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// Nephasto.VisionFXAsset.BaseVision
struct BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single Nephasto.VisionFXAsset.BaseVision::amount
	float ___amount_5;
	// System.Boolean Nephasto.VisionFXAsset.BaseVision::enableColorControls
	bool ___enableColorControls_6;
	// System.Single Nephasto.VisionFXAsset.BaseVision::brightness
	float ___brightness_7;
	// System.Single Nephasto.VisionFXAsset.BaseVision::contrast
	float ___contrast_8;
	// System.Single Nephasto.VisionFXAsset.BaseVision::gamma
	float ___gamma_9;
	// System.Single Nephasto.VisionFXAsset.BaseVision::hue
	float ___hue_10;
	// System.Single Nephasto.VisionFXAsset.BaseVision::saturation
	float ___saturation_11;
	// UnityEngine.Shader Nephasto.VisionFXAsset.BaseVision::shader
	Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* ___shader_12;
	// UnityEngine.Material Nephasto.VisionFXAsset.BaseVision::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_13;
	// System.Int32 Nephasto.VisionFXAsset.BaseVision::variableAmount
	int32_t ___variableAmount_14;
	// System.Int32 Nephasto.VisionFXAsset.BaseVision::variableBrightness
	int32_t ___variableBrightness_15;
	// System.Int32 Nephasto.VisionFXAsset.BaseVision::variableContrast
	int32_t ___variableContrast_16;
	// System.Int32 Nephasto.VisionFXAsset.BaseVision::variableGamma
	int32_t ___variableGamma_17;
	// System.Int32 Nephasto.VisionFXAsset.BaseVision::variableHue
	int32_t ___variableHue_18;
	// System.Int32 Nephasto.VisionFXAsset.BaseVision::variableSaturation
	int32_t ___variableSaturation_19;
};

// Nephasto.VisionFXAsset.AnimeVision
struct AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Boolean Nephasto.VisionFXAsset.AnimeVision::aspect
	bool ___aspect_21;
	// System.Single Nephasto.VisionFXAsset.AnimeVision::radius
	float ___radius_22;
	// System.Single Nephasto.VisionFXAsset.AnimeVision::length
	float ___length_23;
	// System.Single Nephasto.VisionFXAsset.AnimeVision::speed
	float ___speed_24;
	// System.Single Nephasto.VisionFXAsset.AnimeVision::frequency
	float ___frequency_25;
	// System.Single Nephasto.VisionFXAsset.AnimeVision::softness
	float ___softness_26;
	// System.Single Nephasto.VisionFXAsset.AnimeVision::noise
	float ___noise_27;
	// UnityEngine.Color Nephasto.VisionFXAsset.AnimeVision::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_28;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableAspect
	int32_t ___variableAspect_29;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableRadius
	int32_t ___variableRadius_30;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableLength
	int32_t ___variableLength_31;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableSpeed
	int32_t ___variableSpeed_32;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableFrequency
	int32_t ___variableFrequency_33;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableSoftness
	int32_t ___variableSoftness_34;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableNoise
	int32_t ___variableNoise_35;
	// System.Int32 Nephasto.VisionFXAsset.AnimeVision::variableColor
	int32_t ___variableColor_36;
};

// Nephasto.VisionFXAsset.BlurryVision
struct BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Int32 Nephasto.VisionFXAsset.BlurryVision::frameCount
	int32_t ___frameCount_21;
	// System.Int32 Nephasto.VisionFXAsset.BlurryVision::frameStep
	int32_t ___frameStep_22;
	// System.Single Nephasto.VisionFXAsset.BlurryVision::frameResolution
	float ___frameResolution_23;
	// System.Collections.Generic.List`1<UnityEngine.RenderTexture> Nephasto.VisionFXAsset.BlurryVision::frames
	List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* ___frames_24;
	// System.Int32 Nephasto.VisionFXAsset.BlurryVision::lastFrameAdded
	int32_t ___lastFrameAdded_25;
	// System.Int32 Nephasto.VisionFXAsset.BlurryVision::variableFrameCount
	int32_t ___variableFrameCount_27;
};

struct BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_StaticFields
{
	// System.Int32 Nephasto.VisionFXAsset.BlurryVision::MaxFrames
	int32_t ___MaxFrames_26;
};

// Nephasto.VisionFXAsset.DamageVision
struct DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.DamageVision::damage
	float ___damage_21;
	// System.Single Nephasto.VisionFXAsset.DamageVision::definition
	float ___definition_22;
	// System.Single Nephasto.VisionFXAsset.DamageVision::damageBrightness
	float ___damageBrightness_23;
	// System.Single Nephasto.VisionFXAsset.DamageVision::distortion
	float ___distortion_24;
	// UnityEngine.Color Nephasto.VisionFXAsset.DamageVision::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_25;
	// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.DamageVision::blendOp
	int32_t ___blendOp_26;
	// UnityEngine.Texture2D Nephasto.VisionFXAsset.DamageVision::textureBlood
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___textureBlood_27;
	// System.Int32 Nephasto.VisionFXAsset.DamageVision::variableDamage
	int32_t ___variableDamage_28;
	// System.Int32 Nephasto.VisionFXAsset.DamageVision::variableDefinition
	int32_t ___variableDefinition_29;
	// System.Int32 Nephasto.VisionFXAsset.DamageVision::variableDistortion
	int32_t ___variableDistortion_30;
	// System.Int32 Nephasto.VisionFXAsset.DamageVision::variableBrightness
	int32_t ___variableBrightness_31;
	// System.Int32 Nephasto.VisionFXAsset.DamageVision::variableBlendOp
	int32_t ___variableBlendOp_32;
	// System.Int32 Nephasto.VisionFXAsset.DamageVision::variableColor
	int32_t ___variableColor_33;
	// System.Int32 Nephasto.VisionFXAsset.DamageVision::variableBloodTex
	int32_t ___variableBloodTex_34;
};

// Nephasto.VisionFXAsset.DoubleVision
struct DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::strength
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___strength_21;
	// UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::speed
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___speed_22;
	// System.Int32 Nephasto.VisionFXAsset.DoubleVision::variableStrength
	int32_t ___variableStrength_25;
	// System.Int32 Nephasto.VisionFXAsset.DoubleVision::variableSpeed
	int32_t ___variableSpeed_26;
};

struct DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_StaticFields
{
	// UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::DefaultStrength
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___DefaultStrength_23;
	// UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::DefaultSpeed
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___DefaultSpeed_24;
};

// Nephasto.VisionFXAsset.DrunkVision
struct DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.DrunkVision::drunkenness
	float ___drunkenness_21;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::drunkSpeed
	float ___drunkSpeed_22;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::drunkAmplitude
	float ___drunkAmplitude_23;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::swinging
	float ___swinging_24;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::swingingSpeed
	float ___swingingSpeed_25;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::aberration
	float ___aberration_26;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::aberrationSpeed
	float ___aberrationSpeed_27;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::vignetteSpeed
	float ___vignetteSpeed_28;
	// System.Single Nephasto.VisionFXAsset.DrunkVision::vignetteAmount
	float ___vignetteAmount_29;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableDrunkenness
	int32_t ___variableDrunkenness_30;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableDrunkSpeed
	int32_t ___variableDrunkSpeed_31;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableDrunkAmplitude
	int32_t ___variableDrunkAmplitude_32;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableSwinging
	int32_t ___variableSwinging_33;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableSwingingSpeed
	int32_t ___variableSwingingSpeed_34;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableAberration
	int32_t ___variableAberration_35;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableAberrationSpeed
	int32_t ___variableAberrationSpeed_36;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableVignetteAmount
	int32_t ___variableVignetteAmount_37;
	// System.Int32 Nephasto.VisionFXAsset.DrunkVision::variableVignetteSpeed
	int32_t ___variableVignetteSpeed_38;
};

// Nephasto.VisionFXAsset.FisheyeVision
struct FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.FisheyeVision::barrel
	float ___barrel_21;
	// UnityEngine.Vector2 Nephasto.VisionFXAsset.FisheyeVision::center
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___center_22;
	// UnityEngine.Color Nephasto.VisionFXAsset.FisheyeVision::backgroundColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___backgroundColor_23;
	// System.Int32 Nephasto.VisionFXAsset.FisheyeVision::variableBarrel
	int32_t ___variableBarrel_25;
	// System.Int32 Nephasto.VisionFXAsset.FisheyeVision::variableCenter
	int32_t ___variableCenter_26;
	// System.Int32 Nephasto.VisionFXAsset.FisheyeVision::variableBackgroundColor
	int32_t ___variableBackgroundColor_27;
};

struct FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_StaticFields
{
	// UnityEngine.Vector2 Nephasto.VisionFXAsset.FisheyeVision::DefaultCenter
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___DefaultCenter_24;
};

// Nephasto.VisionFXAsset.GhostVision
struct GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// UnityEngine.Vector2 Nephasto.VisionFXAsset.GhostVision::focus
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___focus_21;
	// System.Single Nephasto.VisionFXAsset.GhostVision::aperture
	float ___aperture_22;
	// System.Single Nephasto.VisionFXAsset.GhostVision::zoom
	float ___zoom_23;
	// System.Boolean Nephasto.VisionFXAsset.GhostVision::changeFOV
	bool ___changeFOV_24;
	// System.Single Nephasto.VisionFXAsset.GhostVision::fov
	float ___fov_25;
	// System.Single Nephasto.VisionFXAsset.GhostVision::speed
	float ___speed_26;
	// System.Boolean Nephasto.VisionFXAsset.GhostVision::aspectRatio
	bool ___aspectRatio_27;
	// UnityEngine.Color Nephasto.VisionFXAsset.GhostVision::innerTint
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___innerTint_28;
	// System.Single Nephasto.VisionFXAsset.GhostVision::innerSaturation
	float ___innerSaturation_29;
	// System.Single Nephasto.VisionFXAsset.GhostVision::innerBrightness
	float ___innerBrightness_30;
	// System.Single Nephasto.VisionFXAsset.GhostVision::innerContrast
	float ___innerContrast_31;
	// System.Single Nephasto.VisionFXAsset.GhostVision::innerGamma
	float ___innerGamma_32;
	// UnityEngine.Color Nephasto.VisionFXAsset.GhostVision::outerTint
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___outerTint_33;
	// System.Single Nephasto.VisionFXAsset.GhostVision::outerSaturation
	float ___outerSaturation_34;
	// System.Single Nephasto.VisionFXAsset.GhostVision::outerBrightness
	float ___outerBrightness_35;
	// System.Single Nephasto.VisionFXAsset.GhostVision::outerContrast
	float ___outerContrast_36;
	// System.Single Nephasto.VisionFXAsset.GhostVision::outerGamma
	float ___outerGamma_37;
	// UnityEngine.Camera Nephasto.VisionFXAsset.GhostVision::cam
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam_38;
	// System.Single Nephasto.VisionFXAsset.GhostVision::originalFOV
	float ___originalFOV_39;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableStrength
	int32_t ___variableStrength_40;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableFocus
	int32_t ___variableFocus_41;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableSpeed
	int32_t ___variableSpeed_42;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableAperture
	int32_t ___variableAperture_43;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableZoom
	int32_t ___variableZoom_44;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableInnerColor
	int32_t ___variableInnerColor_45;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableInnerSaturation
	int32_t ___variableInnerSaturation_46;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableInnerBrightness
	int32_t ___variableInnerBrightness_47;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableInnerContrast
	int32_t ___variableInnerContrast_48;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableInnerGamma
	int32_t ___variableInnerGamma_49;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableOuterColor
	int32_t ___variableOuterColor_50;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableOuterSaturation
	int32_t ___variableOuterSaturation_51;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableOuterBrightness
	int32_t ___variableOuterBrightness_52;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableOuterContrast
	int32_t ___variableOuterContrast_53;
	// System.Int32 Nephasto.VisionFXAsset.GhostVision::variableOuterGamma
	int32_t ___variableOuterGamma_54;
};

// Nephasto.VisionFXAsset.HalftoneVision
struct HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.HalftoneVision::size
	float ___size_21;
	// System.Single Nephasto.VisionFXAsset.HalftoneVision::angle
	float ___angle_22;
	// System.Single Nephasto.VisionFXAsset.HalftoneVision::strength
	float ___strength_23;
	// System.Single Nephasto.VisionFXAsset.HalftoneVision::sensitivity
	float ___sensitivity_24;
	// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.HalftoneVision::blendOp
	int32_t ___blendOp_25;
	// System.Int32 Nephasto.VisionFXAsset.HalftoneVision::variableSize
	int32_t ___variableSize_26;
	// System.Int32 Nephasto.VisionFXAsset.HalftoneVision::variableAngle
	int32_t ___variableAngle_27;
	// System.Int32 Nephasto.VisionFXAsset.HalftoneVision::variableStrength
	int32_t ___variableStrength_28;
	// System.Int32 Nephasto.VisionFXAsset.HalftoneVision::variableSensitivity
	int32_t ___variableSensitivity_29;
	// System.Int32 Nephasto.VisionFXAsset.HalftoneVision::variableBlendOp
	int32_t ___variableBlendOp_30;
};

// Nephasto.VisionFXAsset.LegoVision
struct LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.LegoVision::size
	float ___size_21;
	// System.Int32 Nephasto.VisionFXAsset.LegoVision::variableSize
	int32_t ___variableSize_22;
};

// Nephasto.VisionFXAsset.NeonVision
struct NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.NeonVision::edge
	float ___edge_21;
	// UnityEngine.Color Nephasto.VisionFXAsset.NeonVision::edgeColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___edgeColor_22;
	// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.NeonVision::blendOp
	int32_t ___blendOp_23;
	// System.Int32 Nephasto.VisionFXAsset.NeonVision::variableEdge
	int32_t ___variableEdge_24;
	// System.Int32 Nephasto.VisionFXAsset.NeonVision::variableEdgeColor
	int32_t ___variableEdgeColor_25;
	// System.Int32 Nephasto.VisionFXAsset.NeonVision::variableBlendOp
	int32_t ___variableBlendOp_26;
};

// Nephasto.VisionFXAsset.ScannerVision
struct ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// UnityEngine.Color Nephasto.VisionFXAsset.ScannerVision::tint
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___tint_21;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::linesCount
	int32_t ___linesCount_22;
	// System.Single Nephasto.VisionFXAsset.ScannerVision::linesStrength
	float ___linesStrength_23;
	// System.Single Nephasto.VisionFXAsset.ScannerVision::scanlineStrength
	float ___scanlineStrength_24;
	// System.Single Nephasto.VisionFXAsset.ScannerVision::scanlineWidth
	float ___scanlineWidth_25;
	// System.Single Nephasto.VisionFXAsset.ScannerVision::scanLineSpeed
	float ___scanLineSpeed_26;
	// System.Single Nephasto.VisionFXAsset.ScannerVision::noiseBandStrength
	float ___noiseBandStrength_27;
	// System.Single Nephasto.VisionFXAsset.ScannerVision::noiseBandWidth
	float ___noiseBandWidth_28;
	// System.Single Nephasto.VisionFXAsset.ScannerVision::noiseBandSpeed
	float ___noiseBandSpeed_29;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableTint
	int32_t ___variableTint_31;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableLinesCount
	int32_t ___variableLinesCount_32;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableLinesStrength
	int32_t ___variableLinesStrength_33;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableScanLineStrength
	int32_t ___variableScanLineStrength_34;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableScanLineWidth
	int32_t ___variableScanLineWidth_35;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableScanLineSpeed
	int32_t ___variableScanLineSpeed_36;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableNoiseBandStrength
	int32_t ___variableNoiseBandStrength_37;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableNoiseBandWidth
	int32_t ___variableNoiseBandWidth_38;
	// System.Int32 Nephasto.VisionFXAsset.ScannerVision::variableNoiseBandSpeed
	int32_t ___variableNoiseBandSpeed_39;
};

struct ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_StaticFields
{
	// UnityEngine.Color Nephasto.VisionFXAsset.ScannerVision::DefaultTint
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___DefaultTint_30;
};

// Nephasto.VisionFXAsset.ShakeVision
struct ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.ShakeVision::magnitude
	float ___magnitude_21;
	// System.Single Nephasto.VisionFXAsset.ShakeVision::intensity
	float ___intensity_22;
	// System.Int32 Nephasto.VisionFXAsset.ShakeVision::variableMagnitude
	int32_t ___variableMagnitude_23;
	// System.Int32 Nephasto.VisionFXAsset.ShakeVision::variableIntensity
	int32_t ___variableIntensity_24;
};

// Nephasto.VisionFXAsset.TrippyVision
struct TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562  : public BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5
{
	// System.Single Nephasto.VisionFXAsset.TrippyVision::speed
	float ___speed_21;
	// System.Single Nephasto.VisionFXAsset.TrippyVision::definition
	float ___definition_22;
	// System.Single Nephasto.VisionFXAsset.TrippyVision::displacement
	float ___displacement_23;
	// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.TrippyVision::blendOp
	int32_t ___blendOp_24;
	// System.Int32 Nephasto.VisionFXAsset.TrippyVision::variableSpeed
	int32_t ___variableSpeed_25;
	// System.Int32 Nephasto.VisionFXAsset.TrippyVision::variableDefinition
	int32_t ___variableDefinition_26;
	// System.Int32 Nephasto.VisionFXAsset.TrippyVision::variableDisplacement
	int32_t ___variableDisplacement_27;
	// System.Int32 Nephasto.VisionFXAsset.TrippyVision::variableBlendOp
	int32_t ___variableBlendOp_28;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T UnityEngine.Resources::Load<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Resources_Load_TisRuntimeObject_m8B40A11CE62A4E445DADC28C81BD73922A4D4B65_gshared (String_t* ___path0, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Insert_m9C9559248941FED50561DB029D55DF08DEF3B094_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, RuntimeObject* ___item1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m54F62297ADEE4D4FDA697F49ED807BF901201B54_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;

// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline (float ___a0, float ___b1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline (float ___value0, const RuntimeMethod* method) ;
// System.Void Nephasto.VisionFXAsset.BaseVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetInt_m9C05C9D7C152CFCC48F8572710F89DDAC9277E39 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, int32_t ___nameID0, float ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, int32_t ___nameID0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value1, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB (String_t* ___name0, const RuntimeMethod* method) ;
// System.Void Nephasto.VisionFXAsset.BaseVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method) ;
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// T UnityEngine.Resources::Load<UnityEngine.Shader>(System.String)
inline Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF (String_t* ___path0, const RuntimeMethod* method)
{
	return ((  Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m8B40A11CE62A4E445DADC28C81BD73922A4D4B65_gshared)(___path0, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Shader::get_isSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Shader_get_isSupported_m21C3D0F1819054101DFE0C0C062A24464FA5CAE5 (Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Resources::UnloadAsset(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Resources_UnloadAsset_mEA84C20996BC20D1EB485333583FB96F2F487F09 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___assetToUnload0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material__ctor_m7FDF47105D66D19591BE505A0C42B0F90D88C9BF (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* ___shader0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_mACB8BFC903FB3B01BBD427753E791BF28B5E33D4 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A (Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___values0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_shaderKeywords_mD650CF82B2DBB75F001E373E2E1ACA30876F3AB8 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::EnableKeyword(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_EnableKeyword_mE8523EF6CF694284DF976D47ADEDE9363A1174AC (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___keyword0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50 (Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___dest1, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat2, int32_t ___pass3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_Blit_m066854D684B6042B266D306E8E012D2C6C1787BE (Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___dest1, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30 (String_t* ___format0, RuntimeObject* ___arg01, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<UnityEngine.RenderTexture>::get_Item(System.Int32)
inline RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081 (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* (*) (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetTexture_m06083C3F52EF02FFB1177901D9907314F280F9A5 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___name0, Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___value1, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RenderTexture>::get_Count()
inline int32_t List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_inline (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Int32 UnityEngine.Time::get_frameCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Time_get_frameCount_m88E5008FE9451A892DE1F43DC8587213075890A8 (const RuntimeMethod* method) ;
// System.Void Nephasto.VisionFXAsset.BlurryVision::AddFrame(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_AddFrame_m888550A7CDD3C07690FBF52DAF1EF3CC99579DA6 (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___source0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8 (const RuntimeMethod* method) ;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* RenderTexture_GetTemporary_m47DF6AA5AB3A4360AF9CB62BE0180AE9505C6C66 (int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.RenderTexture>::Insert(System.Int32,T)
inline void List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* __this, int32_t ___index0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___item1, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B*, int32_t, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27*, const RuntimeMethod*))List_1_Insert_m9C9559248941FED50561DB029D55DF08DEF3B094_gshared)(__this, ___index0, ___item1, method);
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___temp0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.RenderTexture>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m164E41C28FA43F25F51C417135CF61976ECF3B69 (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B*, int32_t, const RuntimeMethod*))List_1_RemoveAt_m54F62297ADEE4D4FDA697F49ED807BF901201B54_gshared)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.RenderTexture>::Clear()
inline void List_1_Clear_m251977459109D48D068772EA3624051434EB6DB0_inline (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B*, const RuntimeMethod*))List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.RenderTexture>::.ctor()
inline void List_1__ctor_mD0891E1C91B860AD22385685DAAFF9988CA4CB49 (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline (const RuntimeMethod* method) ;
// T UnityEngine.Resources::Load<UnityEngine.Texture2D>(System.String)
inline Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* Resources_Load_TisTexture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_m301E95E824CB214DD0BA6D04221CE97B30BE9ACD (String_t* ___path0, const RuntimeMethod* method)
{
	return ((  Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m8B40A11CE62A4E445DADC28C81BD73922A4D4B65_gshared)(___path0, method);
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetTexture_mA9F8461850AAB88F992E9C6FA6F24C2E050B83FD (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, int32_t ___nameID0, Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___value1, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_op_Implicit_m6162D8136CFE97A5A8BD3B764F9074DB96AA5CD0_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetVector_m44CD02D4555E2AF391C30700F0AEC36BA04CFEA7 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, int32_t ___nameID0, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_m0B3B501E1093739F8887A0DAC5F61D9CB49CC337 (const RuntimeMethod* method) ;
// System.Single Nephasto.VisionFXAsset.BaseVision::get_Amount()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float BaseVision_get_Amount_mA9FE6409D906BEAA982D5437F3AF0A32E4CE4137_inline (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_fieldOfView_m5AA9EED4D1603A1DEDBF883D9C42814B2BDEB777 (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* __this, float ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m232E885D3C7BB6A96D5FEF4494709BA170447604_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
inline Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* GameObject_GetComponent_TisCamera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_m3B3C11550E48AA36AFF82788636EB163CC51FEE6 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Single UnityEngine.Camera::get_fieldOfView()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_fieldOfView_m9A93F17BBF89F496AE231C21817AFD1C1E833FBB (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* __this, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_cyan()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_cyan_m1257FED4776F2A33BD7250357D024B3FA3E592EB_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) ;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B (RuntimeArray* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Nephasto.VisionFXAsset.AnimeVision::get_Aspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AnimeVision_get_Aspect_mB073503C7B7CFE404902B09ACFB256065E6E2796 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return aspect; }
		bool L_0 = __this->___aspect_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Aspect(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Aspect_m83341060905D80FA9BDCB5A73941FCC8D4A746A7 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// set { aspect = value; }
		bool L_0 = ___value0;
		__this->___aspect_21 = L_0;
		// set { aspect = value; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.AnimeVision::get_Radius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimeVision_get_Radius_m6A4E0E1E903752851EEE834BE3BFA750716AE949 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return radius; }
		float L_0 = __this->___radius_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Radius(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Radius_mEC02789CFB79BE90410B1F2D250AC0458430B072 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { radius = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___radius_22 = L_1;
		// set { radius = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.AnimeVision::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimeVision_get_Length_mD9AE86F7C3729A3766367BB6C5974B720BD36CE2 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return length; }
		float L_0 = __this->___length_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Length(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Length_m1478ED2D15BE9772756E9FD934BC9C151B62B984 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { length = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___length_23 = L_1;
		// set { length = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.AnimeVision::get_Speed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimeVision_get_Speed_m66384BB4102F8FA6909C34A1CDD1F52A955D806B (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return speed; }
		float L_0 = __this->___speed_24;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Speed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Speed_m99A49F6FB16DC300EE862271AC9291E97597A487 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { speed = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___speed_24 = L_1;
		// set { speed = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.AnimeVision::get_Frequency()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimeVision_get_Frequency_m85476250961107AD83F21CE580316F404B1DB334 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return frequency; }
		float L_0 = __this->___frequency_25;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Frequency(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Frequency_mD771398CC9AEC974CE526E300271460344823125 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { frequency = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___frequency_25 = L_1;
		// set { frequency = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.AnimeVision::get_Softness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimeVision_get_Softness_m050ADED0114E6EF60E6D202406C5B578B102D646 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return softness; }
		float L_0 = __this->___softness_26;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Softness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Softness_m7DD19CDF249ED896FC1E0432419810962A6C8C6A (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { softness = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___softness_26 = L_1;
		// set { softness = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.AnimeVision::get_Noise()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimeVision_get_Noise_mC039F94FBC114B22C28C6D9E778B2313C966C2A0 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return noise; }
		float L_0 = __this->___noise_27;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Noise(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Noise_m14F4BB38234AD397CF1DA74AECFFA1376CA3F600 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { noise = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___noise_27 = L_1;
		// set { noise = Mathf.Clamp01(value); }
		return;
	}
}
// UnityEngine.Color Nephasto.VisionFXAsset.AnimeVision::get_Color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F AnimeVision_get_Color_mF4EF319EC229346A9C98BE9BDECDD34257A22003 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// get { return color; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___color_28;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::set_Color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_set_Color_mA089C79B327F9BB242A6ACCE6C0B4EDDA8ECCC39 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) 
{
	{
		// set { color = value; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___value0;
		__this->___color_28 = L_0;
		// set { color = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.AnimeVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AnimeVision_ToString_mB60B816FA2E1FD42091DA8EC2C37E9A2E73DA71D (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral19D5E3679B306565C2EE15BC76016508CC80BD96);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Classic anime effect to accentuate speed.";
		return _stringLiteral19D5E3679B306565C2EE15BC76016508CC80BD96;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_ResetDefaultValues_m0BC537B00F913B57DDF7794BF788F5E417012920 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// aspect = false;
		__this->___aspect_21 = (bool)0;
		// radius = 1.25f;
		__this->___radius_22 = (1.25f);
		// length = 2.0f;
		__this->___length_23 = (2.0f);
		// speed = 1.0f;
		__this->___speed_24 = (1.0f);
		// frequency = 10.0f;
		__this->___frequency_25 = (10.0f);
		// softness = 0.1f;
		__this->___softness_26 = (0.100000001f);
		// noise = 0.1f;
		__this->___noise_27 = (0.100000001f);
		// color = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		__this->___color_28 = L_0;
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision_UpdateCustomValues_m204F25B9FFF7DE1E4CACD355E87105F1909B4FF4 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	int32_t G_B2_0 = 0;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* G_B3_2 = NULL;
	{
		// material.SetInt(variableAspect, aspect == true ? 1 : 0);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableAspect_29;
		bool L_2 = __this->___aspect_21;
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_0017;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0018:
	{
		NullCheck(G_B3_2);
		Material_SetInt_m9C05C9D7C152CFCC48F8572710F89DDAC9277E39(G_B3_2, G_B3_1, G_B3_0, NULL);
		// material.SetFloat(variableRadius, radius);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableRadius_30;
		float L_5 = __this->___radius_22;
		NullCheck(L_3);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_3, L_4, L_5, NULL);
		// material.SetFloat(variableLength, length);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_7 = __this->___variableLength_31;
		float L_8 = __this->___length_23;
		NullCheck(L_6);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_6, L_7, L_8, NULL);
		// material.SetFloat(variableSpeed, speed * 5.0f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_10 = __this->___variableSpeed_32;
		float L_11 = __this->___speed_24;
		NullCheck(L_9);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_9, L_10, ((float)il2cpp_codegen_multiply(L_11, (5.0f))), NULL);
		// material.SetFloat(variableFrequency, frequency);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_12 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_13 = __this->___variableFrequency_33;
		float L_14 = __this->___frequency_25;
		NullCheck(L_12);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_12, L_13, L_14, NULL);
		// material.SetFloat(variableSoftness, softness);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_16 = __this->___variableSoftness_34;
		float L_17 = __this->___softness_26;
		NullCheck(L_15);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_15, L_16, L_17, NULL);
		// material.SetFloat(variableNoise, noise);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_19 = __this->___variableNoise_35;
		float L_20 = __this->___noise_27;
		NullCheck(L_18);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_18, L_19, L_20, NULL);
		// material.SetColor(variableColor, color);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_22 = __this->___variableColor_36;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_23 = __this->___color_28;
		NullCheck(L_21);
		Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A(L_21, L_22, L_23, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.AnimeVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimeVision__ctor_m28BCA3626742E7EF5E2E63832FBB26DA30698C96 (AnimeVision_t8AA5847FB89E1B9243900FC30FF24AECDC761E8B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14D9322CF96A4FE0E169C0D11305DBC8706092E4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2D67459F19AB5AE7690FA37D805F279C14A14A81);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9AD9AC2A179FDCC5DF15DA875A0DAF5F51C43BCA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA4D198597F2B72EF6D2A733B4F656C7D8ED45E0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5A1D96CE41BD597A30172C890508C2BB5810152);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float radius = 1.25f;
		__this->___radius_22 = (1.25f);
		// private float length = 2.0f;
		__this->___length_23 = (2.0f);
		// private float speed = 1.0f;
		__this->___speed_24 = (1.0f);
		// private float frequency = 10.0f;
		__this->___frequency_25 = (10.0f);
		// private float softness = 0.1f;
		__this->___softness_26 = (0.100000001f);
		// private float noise = 0.1f;
		__this->___noise_27 = (0.100000001f);
		// private Color color = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		__this->___color_28 = L_0;
		// private readonly int variableAspect = Shader.PropertyToID("_Aspect");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral2D67459F19AB5AE7690FA37D805F279C14A14A81, NULL);
		__this->___variableAspect_29 = L_1;
		// private readonly int variableRadius = Shader.PropertyToID("_Radius");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralE5A1D96CE41BD597A30172C890508C2BB5810152, NULL);
		__this->___variableRadius_30 = L_2;
		// private readonly int variableLength = Shader.PropertyToID("_Length");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral14D9322CF96A4FE0E169C0D11305DBC8706092E4, NULL);
		__this->___variableLength_31 = L_3;
		// private readonly int variableSpeed = Shader.PropertyToID("_Speed");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75, NULL);
		__this->___variableSpeed_32 = L_4;
		// private readonly int variableFrequency = Shader.PropertyToID("_Frequency");
		int32_t L_5;
		L_5 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral94BECA76A094C7D89661E8631A8075CC4A419B7E, NULL);
		__this->___variableFrequency_33 = L_5;
		// private readonly int variableSoftness = Shader.PropertyToID("_Softness");
		int32_t L_6;
		L_6 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9AD9AC2A179FDCC5DF15DA875A0DAF5F51C43BCA, NULL);
		__this->___variableSoftness_34 = L_6;
		// private readonly int variableNoise = Shader.PropertyToID("_Noise");
		int32_t L_7;
		L_7 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralAA4D198597F2B72EF6D2A733B4F656C7D8ED45E0, NULL);
		__this->___variableNoise_35 = L_7;
		// private readonly int variableColor = Shader.PropertyToID("_Color");
		int32_t L_8;
		L_8 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, NULL);
		__this->___variableColor_36 = L_8;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.BaseVision::get_Amount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BaseVision_get_Amount_mA9FE6409D906BEAA982D5437F3AF0A32E4CE4137 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return amount; }
		float L_0 = __this->___amount_5;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::set_Amount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_set_Amount_m8C4649A6348E5360407D7CF2A7186696FC9AA17D (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { amount = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___amount_5 = L_1;
		// set { amount = Mathf.Clamp01(value); }
		return;
	}
}
// System.Boolean Nephasto.VisionFXAsset.BaseVision::get_EnableColorControls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BaseVision_get_EnableColorControls_m808CE4D9098FC170665E5C3611DD808C552A85D1 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return enableColorControls; }
		bool L_0 = __this->___enableColorControls_6;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::set_EnableColorControls(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_set_EnableColorControls_m90B1805A3BD4A7BD6297A110D0FE51885491E0F2 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// set { enableColorControls = value; }
		bool L_0 = ___value0;
		__this->___enableColorControls_6 = L_0;
		// set { enableColorControls = value; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.BaseVision::get_Brightness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BaseVision_get_Brightness_m1F87DA0E273C13EFABC2FC97EB92EC7CADB3E3B5 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return brightness; }
		float L_0 = __this->___brightness_7;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::set_Brightness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_set_Brightness_m7F37E0C2419EEDD986F8D60A8FD90CF9F6117031 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { brightness = Mathf.Clamp(value, -1.0f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (-1.0f), (1.0f), NULL);
		__this->___brightness_7 = L_1;
		// set { brightness = Mathf.Clamp(value, -1.0f, 1.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.BaseVision::get_Contrast()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BaseVision_get_Contrast_m67F6D3EF430A9A4D552CD2CBEF9BB3E4A3248A2B (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return contrast; }
		float L_0 = __this->___contrast_8;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::set_Contrast(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_set_Contrast_m1D95AAE06CD95847EAA3BFC4EE43A62DA5E61DD7 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { contrast = Mathf.Clamp(value, -1.0f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (-1.0f), (1.0f), NULL);
		__this->___contrast_8 = L_1;
		// set { contrast = Mathf.Clamp(value, -1.0f, 1.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.BaseVision::get_Gamma()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BaseVision_get_Gamma_m301B80B2EF3702359747DDB1ADADC860F01053F7 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return gamma; }
		float L_0 = __this->___gamma_9;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::set_Gamma(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_set_Gamma_m1D429D39789A17796CBBE912B7BFDA885823AEFD (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { gamma = Mathf.Clamp(value, 0.1f, 10.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (0.100000001f), (10.0f), NULL);
		__this->___gamma_9 = L_1;
		// set { gamma = Mathf.Clamp(value, 0.1f, 10.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.BaseVision::get_Hue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BaseVision_get_Hue_m9B64478243F9ED1FF56D188FD13E747F0D8401DA (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return hue; }
		float L_0 = __this->___hue_10;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::set_Hue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_set_Hue_mBF12E8C8A207F7E1AC1A0F62E312D6942746CDA9 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { hue = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___hue_10 = L_1;
		// set { hue = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.BaseVision::get_Saturation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BaseVision_get_Saturation_m00B5C8CBEFDE08DCDB9B1281318D19F4A7E140FC (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return saturation; }
		float L_0 = __this->___saturation_11;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::set_Saturation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_set_Saturation_m1A59BCDDEA6865F7CC5DE85EC10CFB94708915FE (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { saturation = Mathf.Clamp(value, 0.0f, 2.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (0.0f), (2.0f), NULL);
		__this->___saturation_11 = L_1;
		// set { saturation = Mathf.Clamp(value, 0.0f, 2.0f); }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.BaseVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BaseVision_ToString_m23C6C56A339882F33A2B70726FDD7438A5E4CED9 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral996763F27BDBC420F9E68D5638DD77E3D840FD32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "No description.";
		return _stringLiteral996763F27BDBC420F9E68D5638DD77E3D840FD32;
	}
}
// System.String Nephasto.VisionFXAsset.BaseVision::ShaderPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BaseVision_ShaderPath_mB97266F3BD1DC2961F8F3FD19C837288259199E3 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD401F59E8E1E4B3F003996F58B0812ED01CE6CC5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected virtual string ShaderPath() => $"Shaders/{this.GetType().Name}";
		Type_t* L_0;
		L_0 = Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3(__this, NULL);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralD401F59E8E1E4B3F003996F58B0812ED01CE6CC5, L_1, NULL);
		return L_2;
	}
}
// System.Boolean Nephasto.VisionFXAsset.BaseVision::IsSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BaseVision_IsSupported_m23ECCD564B858B8DD17E0B8C2B23FCC2BC338A5C (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* V_1 = NULL;
	{
		// bool supported = false;
		V_0 = (bool)0;
		// string shaderPath = ShaderPath();
		String_t* L_0;
		L_0 = VirtualFuncInvoker0< String_t* >::Invoke(4 /* System.String Nephasto.VisionFXAsset.BaseVision::ShaderPath() */, __this);
		// Shader test = Resources.Load<Shader>(shaderPath);
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_1;
		L_1 = Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF(L_0, Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF_RuntimeMethod_var);
		V_1 = L_1;
		// if (test != null)
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_2 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		// supported = test.isSupported == true;
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_4 = V_1;
		NullCheck(L_4);
		bool L_5;
		L_5 = Shader_get_isSupported_m21C3D0F1819054101DFE0C0C062A24464FA5CAE5(L_4, NULL);
		V_0 = L_5;
		// Resources.UnloadAsset(test);
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_6 = V_1;
		Resources_UnloadAsset_mEA84C20996BC20D1EB485333583FB96F2F487F09(L_6, NULL);
	}

IL_0024:
	{
		// return supported;
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// amount = 1.0f;
		__this->___amount_5 = (1.0f);
		// }
		return;
	}
}
// System.Boolean Nephasto.VisionFXAsset.BaseVision::LoadCustomResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BaseVision_LoadCustomResources_mA23FD8A6CF9199FEE77C73F29CA140F5488168F3 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// protected virtual bool LoadCustomResources() { return true; }
		return (bool)1;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_UpdateCustomValues_m4A342E05C24748D3FC0345D1732D182434D1747D (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// protected virtual void UpdateCustomValues() { }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::OnRenderImageCustom(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_OnRenderImageCustom_m7B49EB592EE54AA044F8E50C7CF87653DF79CD77 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___destination1, const RuntimeMethod* method) 
{
	{
		// protected virtual void OnRenderImageCustom(RenderTexture source, RenderTexture destination) { }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_Start_m2DA036D3871AD440488E8BE10BEAF96B19F75A64 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59B8BDA4E9611A9CC62C51C6532874CBF5BEC632);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6BC4E8615279BB8565C8685C50E2B11EC89E47FF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7EB1308A70D2205642092FE39384B0EAF8993A2D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D0E132481EE2A501A93D38AFFDCE49F9BAB411F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA963E2F2B155C8743D4F8B4ACA239D9A76D29864);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB328E9896615295D972D16BB5455163121D8592C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE042C8EC1462638570B1B4320DC6ACC2FF0FF026);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		// string shaderPath = ShaderPath();
		String_t* L_0;
		L_0 = VirtualFuncInvoker0< String_t* >::Invoke(4 /* System.String Nephasto.VisionFXAsset.BaseVision::ShaderPath() */, __this);
		V_0 = L_0;
		// shader = Resources.Load<Shader>(shaderPath);
		String_t* L_1 = V_0;
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_2;
		L_2 = Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF(L_1, Resources_Load_TisShader_tADC867D36B7876EE22427FAA2CE485105F4EE692_m824C83EE33569CB1C565A45A57562A3486832BFF_RuntimeMethod_var);
		__this->___shader_12 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___shader_12), (void*)L_2);
		// if (shader != null)
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_3 = __this->___shader_12;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_00da;
		}
	}
	{
		// if (shader.isSupported == true)
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_5 = __this->___shader_12;
		NullCheck(L_5);
		bool L_6;
		L_6 = Shader_get_isSupported_m21C3D0F1819054101DFE0C0C062A24464FA5CAE5(L_5, NULL);
		if (!L_6)
		{
			goto IL_00bd;
		}
	}
	{
		// string materialName = this.GetType().Name;
		Type_t* L_7;
		L_7 = Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3(__this, NULL);
		NullCheck(L_7);
		String_t* L_8;
		L_8 = VirtualFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_7);
		V_1 = L_8;
		// material = new Material(shader);
		Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* L_9 = __this->___shader_12;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_10 = (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3*)il2cpp_codegen_object_new(Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Material__ctor_m7FDF47105D66D19591BE505A0C42B0F90D88C9BF(L_10, L_9, NULL);
		__this->___material_13 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___material_13), (void*)L_10);
		// if (material != null)
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_11 = __this->___material_13;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_11, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_12)
		{
			goto IL_00a0;
		}
	}
	{
		// material.name = materialName;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_13 = __this->___material_13;
		String_t* L_14 = V_1;
		NullCheck(L_13);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_13, L_14, NULL);
		// material.hideFlags = HideFlags.HideAndDontSave;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = __this->___material_13;
		NullCheck(L_15);
		Object_set_hideFlags_mACB8BFC903FB3B01BBD427753E791BF28B5E33D4(L_15, ((int32_t)61), NULL);
		// if (LoadCustomResources() == false)
		bool L_16;
		L_16 = VirtualFuncInvoker0< bool >::Invoke(6 /* System.Boolean Nephasto.VisionFXAsset.BaseVision::LoadCustomResources() */, __this);
		if (L_16)
		{
			goto IL_011b;
		}
	}
	{
		// Debug.LogError($"[Nephasto.VisionFX] '{materialName}' error loading custom resources. Please contact with 'hello@nephasto.com' and send the log file.");
		String_t* L_17 = V_1;
		String_t* L_18;
		L_18 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(_stringLiteral9D0E132481EE2A501A93D38AFFDCE49F9BAB411F, L_17, _stringLiteral7EB1308A70D2205642092FE39384B0EAF8993A2D, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_18, NULL);
		// this.enabled = false;
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(__this, (bool)0, NULL);
		return;
	}

IL_00a0:
	{
		// Debug.LogError($"[Nephasto.VisionFX] '{materialName}' material null. Please contact with 'hello@nephasto.com' and send the log file.");
		String_t* L_19 = V_1;
		String_t* L_20;
		L_20 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(_stringLiteral9D0E132481EE2A501A93D38AFFDCE49F9BAB411F, L_19, _stringLiteral59B8BDA4E9611A9CC62C51C6532874CBF5BEC632, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_20, NULL);
		// this.enabled = false;
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(__this, (bool)0, NULL);
		return;
	}

IL_00bd:
	{
		// Debug.LogWarning($"[Nephasto.VisionFX] '{shaderPath}' shader not supported. Please contact with 'hello@nephasto.com' and send the log file.");
		String_t* L_21 = V_0;
		String_t* L_22;
		L_22 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(_stringLiteral9D0E132481EE2A501A93D38AFFDCE49F9BAB411F, L_21, _stringLiteralA963E2F2B155C8743D4F8B4ACA239D9A76D29864, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28(L_22, NULL);
		// this.enabled = false;
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(__this, (bool)0, NULL);
		return;
	}

IL_00da:
	{
		// Debug.LogWarning($"[Nephasto.VisionFX] Shader 'Nephasto/VisionFX/Resources/{shaderPath}.shader' not found. '{this.GetType().Name}' disabled.");
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral6BC4E8615279BB8565C8685C50E2B11EC89E47FF);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral6BC4E8615279BB8565C8685C50E2B11EC89E47FF);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
		String_t* L_26 = V_0;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_26);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_25;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteralB328E9896615295D972D16BB5455163121D8592C);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB328E9896615295D972D16BB5455163121D8592C);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = L_27;
		Type_t* L_29;
		L_29 = Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3(__this, NULL);
		NullCheck(L_29);
		String_t* L_30;
		L_30 = VirtualFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_29);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_30);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_30);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_28;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteralE042C8EC1462638570B1B4320DC6ACC2FF0FF026);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralE042C8EC1462638570B1B4320DC6ACC2FF0FF026);
		String_t* L_32;
		L_32 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_mEF15C6B17CE4E1FA7E379CDB82CE40FCD89A3F28(L_32, NULL);
		// this.enabled = false;
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(__this, (bool)0, NULL);
	}

IL_011b:
	{
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_OnDestroy_mF9212175388E2F458A4A99CD997762CCA34AA5F2 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (material != null)
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = __this->___material_13;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// Destroy(material);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_2 = __this->___material_13;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_2, NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_OnRenderImage_m35FC86D73A700A42DD62ED0BB2828254561A653C (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___destination1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6084B2BBB2C5FCC546818FFB9CDA1D3FD6A9FCF1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (material != null && amount > 0.0f)
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = __this->___material_13;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0105;
		}
	}
	{
		float L_2 = __this->___amount_5;
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_0105;
		}
	}
	{
		// material.shaderKeywords = null;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = __this->___material_13;
		NullCheck(L_3);
		Material_set_shaderKeywords_mD650CF82B2DBB75F001E373E2E1ACA30876F3AB8(L_3, (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)NULL, NULL);
		// OnRenderImageCustom(source, destination);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_4 = ___source0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_5 = ___destination1;
		VirtualActionInvoker2< RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27*, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* >::Invoke(8 /* System.Void Nephasto.VisionFXAsset.BaseVision::OnRenderImageCustom(UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, __this, L_4, L_5);
		// material.SetFloat(variableAmount, amount);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = __this->___material_13;
		int32_t L_7 = __this->___variableAmount_14;
		float L_8 = __this->___amount_5;
		NullCheck(L_6);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_6, L_7, L_8, NULL);
		// if (enableColorControls == true && amount > 0.0f)
		bool L_9 = __this->___enableColorControls_6;
		if (!L_9)
		{
			goto IL_00f0;
		}
	}
	{
		float L_10 = __this->___amount_5;
		if ((!(((float)L_10) > ((float)(0.0f)))))
		{
			goto IL_00f0;
		}
	}
	{
		// material.EnableKeyword(keywordColorControls);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_11 = __this->___material_13;
		NullCheck(L_11);
		Material_EnableKeyword_mE8523EF6CF694284DF976D47ADEDE9363A1174AC(L_11, _stringLiteral6084B2BBB2C5FCC546818FFB9CDA1D3FD6A9FCF1, NULL);
		// material.SetFloat(variableBrightness, brightness);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_12 = __this->___material_13;
		int32_t L_13 = __this->___variableBrightness_15;
		float L_14 = __this->___brightness_7;
		NullCheck(L_12);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_12, L_13, L_14, NULL);
		// material.SetFloat(variableContrast, contrast);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = __this->___material_13;
		int32_t L_16 = __this->___variableContrast_16;
		float L_17 = __this->___contrast_8;
		NullCheck(L_15);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_15, L_16, L_17, NULL);
		// material.SetFloat(variableGamma, 1.0f / gamma);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = __this->___material_13;
		int32_t L_19 = __this->___variableGamma_17;
		float L_20 = __this->___gamma_9;
		NullCheck(L_18);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_18, L_19, ((float)((1.0f)/L_20)), NULL);
		// material.SetFloat(variableHue, hue);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = __this->___material_13;
		int32_t L_22 = __this->___variableHue_18;
		float L_23 = __this->___hue_10;
		NullCheck(L_21);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_21, L_22, L_23, NULL);
		// material.SetFloat(variableSaturation, saturation);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_24 = __this->___material_13;
		int32_t L_25 = __this->___variableSaturation_19;
		float L_26 = __this->___saturation_11;
		NullCheck(L_24);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_24, L_25, L_26, NULL);
	}

IL_00f0:
	{
		// UpdateCustomValues();
		VirtualActionInvoker0::Invoke(7 /* System.Void Nephasto.VisionFXAsset.BaseVision::UpdateCustomValues() */, __this);
		// Graphics.Blit(source, destination, material, 0);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_27 = ___source0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_28 = ___destination1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_29 = __this->___material_13;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m5A753341A113FB6501C64F73D62047F84F957E50(L_27, L_28, L_29, 0, NULL);
		return;
	}

IL_0105:
	{
		// Graphics.Blit(source, destination);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_30 = ___source0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_31 = ___destination1;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m066854D684B6042B266D306E8E012D2C6C1787BE(L_30, L_31, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::UpdateAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision_UpdateAmount_mBAFE0A79B0786D5F9C2BE9C10E1D87F544FB91BE (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, float ___strength0, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableAmount, (amount + strength));
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = __this->___material_13;
		int32_t L_1 = __this->___variableAmount_14;
		float L_2 = __this->___amount_5;
		float L_3 = ___strength0;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, ((float)il2cpp_codegen_add(L_2, L_3)), NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BaseVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0 (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E7B8EB481D432B24624CC9573E7AC2E516E7B43);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5696D802E93F47F31F1E298FB7F74857D3CC019);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB9A1D99FCEB48E9EA569B020096B8F46208388A7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE2DD33BCD541DA280E529743F65CB84C9541BC9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE9FAF482EBF07DB48E259D5B8D2B04A5EF5EFA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE9A91171153CFB6728156E1D688BD478DF0CEA09);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float amount = 1.0f;
		__this->___amount_5 = (1.0f);
		// private float gamma = 1.0f;
		__this->___gamma_9 = (1.0f);
		// private float saturation = 1.0f;
		__this->___saturation_11 = (1.0f);
		// private readonly int variableAmount = Shader.PropertyToID("_Amount");
		int32_t L_0;
		L_0 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralB9A1D99FCEB48E9EA569B020096B8F46208388A7, NULL);
		__this->___variableAmount_14 = L_0;
		// private readonly int variableBrightness = Shader.PropertyToID("_Brightness");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralB5696D802E93F47F31F1E298FB7F74857D3CC019, NULL);
		__this->___variableBrightness_15 = L_1;
		// private readonly int variableContrast = Shader.PropertyToID("_Contrast");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralDE2DD33BCD541DA280E529743F65CB84C9541BC9, NULL);
		__this->___variableContrast_16 = L_2;
		// private readonly int variableGamma = Shader.PropertyToID("_Gamma");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralE9A91171153CFB6728156E1D688BD478DF0CEA09, NULL);
		__this->___variableGamma_17 = L_3;
		// private readonly int variableHue = Shader.PropertyToID("_Hue");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9E7B8EB481D432B24624CC9573E7AC2E516E7B43, NULL);
		__this->___variableHue_18 = L_4;
		// private readonly int variableSaturation = Shader.PropertyToID("_Saturation");
		int32_t L_5;
		L_5 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralDE9FAF482EBF07DB48E259D5B8D2B04A5EF5EFA9, NULL);
		__this->___variableSaturation_19 = L_5;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Nephasto.VisionFXAsset.BlurryVision::get_Frames()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BlurryVision_get_Frames_mBDD377E7682F16FFAE5252B9A85E52DAFBA7C34D (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	{
		// get { return frameCount; }
		int32_t L_0 = __this->___frameCount_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::set_Frames(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_set_Frames_mEDE05C04107132EBC3BE7556C70B03B00908757A (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B2_0 = NULL;
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B1_0 = NULL;
	int32_t G_B5_0 = 0;
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B5_1 = NULL;
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B4_0 = NULL;
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B3_0 = NULL;
	{
		// set { frameCount = value > 0 ? (value < MaxFrames ? value : MaxFrames) : 0; }
		int32_t L_0 = ___value0;
		G_B1_0 = __this;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			G_B2_0 = __this;
			goto IL_0008;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B1_0;
		goto IL_0018;
	}

IL_0008:
	{
		int32_t L_1 = ___value0;
		il2cpp_codegen_runtime_class_init_inline(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		int32_t L_2 = ((BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_StaticFields*)il2cpp_codegen_static_fields_for(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var))->___MaxFrames_26;
		G_B3_0 = G_B2_0;
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			G_B4_0 = G_B2_0;
			goto IL_0017;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		int32_t L_3 = ((BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_StaticFields*)il2cpp_codegen_static_fields_for(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var))->___MaxFrames_26;
		G_B5_0 = L_3;
		G_B5_1 = G_B3_0;
		goto IL_0018;
	}

IL_0017:
	{
		int32_t L_4 = ___value0;
		G_B5_0 = L_4;
		G_B5_1 = G_B4_0;
	}

IL_0018:
	{
		NullCheck(G_B5_1);
		G_B5_1->___frameCount_21 = G_B5_0;
		// set { frameCount = value > 0 ? (value < MaxFrames ? value : MaxFrames) : 0; }
		return;
	}
}
// System.Int32 Nephasto.VisionFXAsset.BlurryVision::get_FrameStep()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BlurryVision_get_FrameStep_m1710CC85257CB696A50BA43C100D6A7606DCE5CF (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	{
		// get { return frameStep; }
		int32_t L_0 = __this->___frameStep_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::set_FrameStep(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_set_FrameStep_mC8A92B55246EDC92DAE743C5FC99E1C50E393C42 (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B2_0 = NULL;
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* G_B3_1 = NULL;
	{
		// set { frameStep = value > 0 ? value : 0; }
		int32_t L_0 = ___value0;
		G_B1_0 = __this;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			G_B2_0 = __this;
			goto IL_0008;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_1 = ___value0;
		G_B3_0 = L_1;
		G_B3_1 = G_B2_0;
	}

IL_0009:
	{
		NullCheck(G_B3_1);
		G_B3_1->___frameStep_22 = G_B3_0;
		// set { frameStep = value > 0 ? value : 0; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.BlurryVision::get_FrameResolution()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BlurryVision_get_FrameResolution_m5248545B4E9A02D2A8D0F1515303809561ED6BED (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	{
		// get { return frameResolution; }
		float L_0 = __this->___frameResolution_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::set_FrameResolution(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_set_FrameResolution_m9D14A1EED55628F641EB6D71EA953F74811C4C4A (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { frameResolution = Mathf.Clamp(value, 0.1f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (0.100000001f), (1.0f), NULL);
		__this->___frameResolution_23 = L_1;
		// set { frameResolution = Mathf.Clamp(value, 0.1f, 1.0f); }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.BlurryVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BlurryVision_ToString_m9BE1E2C2A5E9B3EB1455A0B5DC3A31440CA093A7 (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral04273BAC62A956C2EEF06CFD0A70976AD2916A53);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Blurry vision.";
		return _stringLiteral04273BAC62A956C2EEF06CFD0A70976AD2916A53;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_ResetDefaultValues_mAC1FA901AF7594F5D92684BF08052CB112C7BA95 (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// frameCount = 5;
		__this->___frameCount_21 = 5;
		// frameStep = 0;
		__this->___frameStep_22 = 0;
		// frameResolution = 1.0f;
		__this->___frameResolution_23 = (1.0f);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_UpdateCustomValues_m2F1BA05DB9C82246E7C115AD9212D1E93C5B0347 (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAAC72F9935ADA683270704D6D9A3FD76302D8486);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// material.SetInt(variableFrameCount, frameCount);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableFrameCount_27;
		int32_t L_2 = __this->___frameCount_21;
		NullCheck(L_0);
		Material_SetInt_m9C05C9D7C152CFCC48F8572710F89DDAC9277E39(L_0, L_1, L_2, NULL);
		// for (int i = 0; i < frameCount && i < frames.Count; ++i)
		V_0 = 0;
		goto IL_0046;
	}

IL_001b:
	{
		// material.SetTexture($"_RT{i}", frames[i]);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		RuntimeObject* L_6 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7;
		L_7 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(_stringLiteralAAC72F9935ADA683270704D6D9A3FD76302D8486, L_6, NULL);
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_8 = __this->___frames_24;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_10;
		L_10 = List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081(L_8, L_9, List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081_RuntimeMethod_var);
		NullCheck(L_3);
		Material_SetTexture_m06083C3F52EF02FFB1177901D9907314F280F9A5(L_3, L_7, L_10, NULL);
		// for (int i = 0; i < frameCount && i < frames.Count; ++i)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_11, 1));
	}

IL_0046:
	{
		// for (int i = 0; i < frameCount && i < frames.Count; ++i)
		int32_t L_12 = V_0;
		int32_t L_13 = __this->___frameCount_21;
		if ((((int32_t)L_12) >= ((int32_t)L_13)))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_14 = V_0;
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_15 = __this->___frames_24;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_inline(L_15, List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_001b;
		}
	}

IL_005d:
	{
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::OnRenderImageCustom(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_OnRenderImageCustom_m17CF0AA8D8349C1AC08410CBC017C76D41ED7072 (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___destination1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_1 = NULL;
	{
		// if (frames.Count > 0)
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_0 = __this->___frames_24;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_inline(L_0, List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		// if (lastFrameAdded != Time.frameCount)
		int32_t L_2 = __this->___lastFrameAdded_25;
		int32_t L_3;
		L_3 = Time_get_frameCount_m88E5008FE9451A892DE1F43DC8587213075890A8(NULL);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0091;
		}
	}
	{
		// if (frameStep == 0 || (Time.frameCount - lastFrameAdded >= frameStep))
		int32_t L_4 = __this->___frameStep_22;
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_5;
		L_5 = Time_get_frameCount_m88E5008FE9451A892DE1F43DC8587213075890A8(NULL);
		int32_t L_6 = __this->___lastFrameAdded_25;
		int32_t L_7 = __this->___frameStep_22;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract(L_5, L_6))) < ((int32_t)L_7)))
		{
			goto IL_0091;
		}
	}

IL_0037:
	{
		// AddFrame(source);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_8 = ___source0;
		BlurryVision_AddFrame_m888550A7CDD3C07690FBF52DAF1EF3CC99579DA6(__this, L_8, NULL);
		return;
	}

IL_003f:
	{
		// lastFrameAdded = Time.frameCount;
		int32_t L_9;
		L_9 = Time_get_frameCount_m88E5008FE9451A892DE1F43DC8587213075890A8(NULL);
		__this->___lastFrameAdded_25 = L_9;
		// for (int i = 0; i < MaxFrames; ++i)
		V_0 = 0;
		goto IL_0089;
	}

IL_004e:
	{
		// RenderTexture renderTexture = RenderTexture.GetTemporary((int)(Screen.width * frameResolution), (int)(Screen.height * frameResolution), 0);
		int32_t L_10;
		L_10 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		float L_11 = __this->___frameResolution_23;
		int32_t L_12;
		L_12 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		float L_13 = __this->___frameResolution_23;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_14;
		L_14 = RenderTexture_GetTemporary_m47DF6AA5AB3A4360AF9CB62BE0180AE9505C6C66(il2cpp_codegen_cast_double_to_int<int32_t>(((float)il2cpp_codegen_multiply(((float)L_10), L_11))), il2cpp_codegen_cast_double_to_int<int32_t>(((float)il2cpp_codegen_multiply(((float)L_12), L_13))), 0, NULL);
		V_1 = L_14;
		// Graphics.Blit(source, renderTexture);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_15 = ___source0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_16 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m066854D684B6042B266D306E8E012D2C6C1787BE(L_15, L_16, NULL);
		// frames.Insert(0, renderTexture);
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_17 = __this->___frames_24;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_18 = V_1;
		NullCheck(L_17);
		List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E(L_17, 0, L_18, List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E_RuntimeMethod_var);
		// for (int i = 0; i < MaxFrames; ++i)
		int32_t L_19 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_0089:
	{
		// for (int i = 0; i < MaxFrames; ++i)
		int32_t L_20 = V_0;
		il2cpp_codegen_runtime_class_init_inline(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		int32_t L_21 = ((BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_StaticFields*)il2cpp_codegen_static_fields_for(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var))->___MaxFrames_26;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_004e;
		}
	}

IL_0091:
	{
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::AddFrame(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_AddFrame_m888550A7CDD3C07690FBF52DAF1EF3CC99579DA6 (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___source0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_m164E41C28FA43F25F51C417135CF61976ECF3B69_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_0 = NULL;
	{
		// lastFrameAdded = Time.frameCount;
		int32_t L_0;
		L_0 = Time_get_frameCount_m88E5008FE9451A892DE1F43DC8587213075890A8(NULL);
		__this->___lastFrameAdded_25 = L_0;
		// RenderTexture renderTexture = RenderTexture.GetTemporary((int)(Screen.width * frameResolution), (int)(Screen.height * frameResolution), 0);
		int32_t L_1;
		L_1 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		float L_2 = __this->___frameResolution_23;
		int32_t L_3;
		L_3 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		float L_4 = __this->___frameResolution_23;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_5;
		L_5 = RenderTexture_GetTemporary_m47DF6AA5AB3A4360AF9CB62BE0180AE9505C6C66(il2cpp_codegen_cast_double_to_int<int32_t>(((float)il2cpp_codegen_multiply(((float)L_1), L_2))), il2cpp_codegen_cast_double_to_int<int32_t>(((float)il2cpp_codegen_multiply(((float)L_3), L_4))), 0, NULL);
		V_0 = L_5;
		// Graphics.Blit(source, renderTexture);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_6 = ___source0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_7 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m066854D684B6042B266D306E8E012D2C6C1787BE(L_6, L_7, NULL);
		// frames.Insert(0, renderTexture);
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_8 = __this->___frames_24;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_9 = V_0;
		NullCheck(L_8);
		List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E(L_8, 0, L_9, List_1_Insert_m4D325503A9B37C64635586A8D4B1FF9F3390D65E_RuntimeMethod_var);
		// if (frames.Count >= MaxFrames)
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_10 = __this->___frames_24;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_inline(L_10, List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		int32_t L_12 = ((BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_StaticFields*)il2cpp_codegen_static_fields_for(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var))->___MaxFrames_26;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0089;
		}
	}
	{
		// RenderTexture.ReleaseTemporary(frames[frames.Count - 1]);
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_13 = __this->___frames_24;
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_14 = __this->___frames_24;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_inline(L_14, List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		NullCheck(L_13);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_16;
		L_16 = List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081(L_13, ((int32_t)il2cpp_codegen_subtract(L_15, 1)), List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081_RuntimeMethod_var);
		RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F(L_16, NULL);
		// frames.RemoveAt(frames.Count - 1);
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_17 = __this->___frames_24;
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_18 = __this->___frames_24;
		NullCheck(L_18);
		int32_t L_19;
		L_19 = List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_inline(L_18, List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		NullCheck(L_17);
		List_1_RemoveAt_m164E41C28FA43F25F51C417135CF61976ECF3B69(L_17, ((int32_t)il2cpp_codegen_subtract(L_19, 1)), List_1_RemoveAt_m164E41C28FA43F25F51C417135CF61976ECF3B69_RuntimeMethod_var);
	}

IL_0089:
	{
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision_OnDisable_mAC925BA4D6D079AE7B34CC0B5D26AB1FB6ED13CC (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m251977459109D48D068772EA3624051434EB6DB0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < frames.Count; ++i)
		V_0 = 0;
		goto IL_0019;
	}

IL_0004:
	{
		// RenderTexture.ReleaseTemporary(frames[i]);
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_0 = __this->___frames_24;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_2;
		L_2 = List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081(L_0, L_1, List_1_get_Item_mFD8C82612D4654B884F2F68C8C1D93B56A1E0081_RuntimeMethod_var);
		RenderTexture_ReleaseTemporary_m7D9B385460ED0D0CF7BCC033605CEBD60A1A232F(L_2, NULL);
		// for (int i = 0; i < frames.Count; ++i)
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_3, 1));
	}

IL_0019:
	{
		// for (int i = 0; i < frames.Count; ++i)
		int32_t L_4 = V_0;
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_5 = __this->___frames_24;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_inline(L_5, List_1_get_Count_mDF53F5D42E96D7FDE797826D3902F54BB8B80221_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0004;
		}
	}
	{
		// frames.Clear();
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_7 = __this->___frames_24;
		NullCheck(L_7);
		List_1_Clear_m251977459109D48D068772EA3624051434EB6DB0_inline(L_7, List_1_Clear_m251977459109D48D068772EA3624051434EB6DB0_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision__ctor_mB7375F100600AFF4CDE0118FA0B6ED5F4214039F (BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD0891E1C91B860AD22385685DAAFF9988CA4CB49_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral96A5287C5C28694ED3FDEE4D1E8260C1D05BEB34);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private int frameCount = 5;
		__this->___frameCount_21 = 5;
		// private float frameResolution = 1.0f;
		__this->___frameResolution_23 = (1.0f);
		// private List<RenderTexture> frames = new List<RenderTexture>();
		List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B* L_0 = (List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B*)il2cpp_codegen_object_new(List_1_tF20988DD2863E9321EB7427D9B83C337053F1C8B_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_mD0891E1C91B860AD22385685DAAFF9988CA4CB49(L_0, List_1__ctor_mD0891E1C91B860AD22385685DAAFF9988CA4CB49_RuntimeMethod_var);
		__this->___frames_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___frames_24), (void*)L_0);
		// private int lastFrameAdded = -1;
		__this->___lastFrameAdded_25 = (-1);
		// private readonly int variableFrameCount = Shader.PropertyToID("_FrameCount");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral96A5287C5C28694ED3FDEE4D1E8260C1D05BEB34, NULL);
		__this->___variableFrameCount_27 = L_1;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.BlurryVision::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlurryVision__cctor_mCDA96F243D035F825CB2025D8D375EDBA7C64863 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly int MaxFrames = 10;
		((BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_StaticFields*)il2cpp_codegen_static_fields_for(BlurryVision_t9F29ECBF8781B7DC1E2FA404AFD817C12009124F_il2cpp_TypeInfo_var))->___MaxFrames_26 = ((int32_t)10);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.DamageVision::get_Damage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DamageVision_get_Damage_m3B389F9545D5E6B3E7449D615EC32185BC5A8653 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// get { return damage; }
		float L_0 = __this->___damage_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::set_Damage(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_set_Damage_mE5091A0BCC674DCDCD01D5A5213F5502DB680B58 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { damage = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___damage_21 = L_1;
		// set { damage = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DamageVision::get_Definition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DamageVision_get_Definition_m9EC66FD30DB6C7E30C3159C7AB3F805262707AD7 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// get { return definition; }
		float L_0 = __this->___definition_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::set_Definition(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_set_Definition_mA588E3382EEC0A47F781ED8C03F0ACCF28777C6A (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { definition = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___definition_22 = L_1;
		// set { definition = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DamageVision::get_DamageBrightness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DamageVision_get_DamageBrightness_mC352DB8757B02B7584C22A4E878922EF130BB318 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// get { return damageBrightness; }
		float L_0 = __this->___damageBrightness_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::set_DamageBrightness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_set_DamageBrightness_mF3DF2E8B65D29DD6020239E4C25DBFF465098D61 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { damageBrightness = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___damageBrightness_23 = L_1;
		// set { damageBrightness = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DamageVision::get_Distortion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DamageVision_get_Distortion_m8FD64ADE103A82FFF9809C51956A8BF79A7DA5AB (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// get { return distortion; }
		float L_0 = __this->___distortion_24;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::set_Distortion(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_set_Distortion_m3C879334D11B6AD279EA0836E2D8E0A181793211 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { distortion = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___distortion_24 = L_1;
		// set { distortion = Mathf.Max(value, 0.0f); }
		return;
	}
}
// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.DamageVision::get_BlendOp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DamageVision_get_BlendOp_m10552EF529E51F1DAE8B4DFB1902463711663ABC (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// get { return blendOp; }
		int32_t L_0 = __this->___blendOp_26;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_set_BlendOp_mE920DBF5873CEA81E501C4F2857DA68DF716B482 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// set { blendOp = value; }
		int32_t L_0 = ___value0;
		__this->___blendOp_26 = L_0;
		// set { blendOp = value; }
		return;
	}
}
// UnityEngine.Color Nephasto.VisionFXAsset.DamageVision::get_Color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F DamageVision_get_Color_mFF98D32F5ED974D353B74BACCD8AD9C8BC271956 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// get { return color; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___color_25;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::set_Color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_set_Color_m1BEC4CE7523844F95A7D199514D67E6555AE66C3 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) 
{
	{
		// set { color = value; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___value0;
		__this->___color_25 = L_0;
		// set { color = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.DamageVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DamageVision_ToString_m261DF2105A8333DC45C91CEF78B33658F11C02B1 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCDE32AD10A2BADFD3344D8084D96E81419B04EE6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "A very visual way of communicating the damage the player has suffered.";
		return _stringLiteralCDE32AD10A2BADFD3344D8084D96E81419B04EE6;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_ResetDefaultValues_m31DA397339F31942D18066FC8966AC46C64BC855 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// damage = 0.7f;
		__this->___damage_21 = (0.699999988f);
		// definition = 0.5f;
		__this->___definition_22 = (0.5f);
		// distortion = 0.2f;
		__this->___distortion_24 = (0.200000003f);
		// damageBrightness = 3.0f;
		__this->___damageBrightness_23 = (3.0f);
		// blendOp = VisionBlendOps.Darken;
		__this->___blendOp_26 = 3;
		// color = Color.red;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
		__this->___color_25 = L_0;
		// }
		return;
	}
}
// System.Boolean Nephasto.VisionFXAsset.DamageVision::LoadCustomResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DamageVision_LoadCustomResources_mD3A2182F44DC2F392B2F212ADA3FBDFC081D1674 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisTexture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_m301E95E824CB214DD0BA6D04221CE97B30BE9ACD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8E5251C270515473520155396D1E117B2866B75C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// textureBlood = Resources.Load<Texture2D>("Textures/Blood");
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_0;
		L_0 = Resources_Load_TisTexture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_m301E95E824CB214DD0BA6D04221CE97B30BE9ACD(_stringLiteral8E5251C270515473520155396D1E117B2866B75C, Resources_Load_TisTexture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_m301E95E824CB214DD0BA6D04221CE97B30BE9ACD_RuntimeMethod_var);
		__this->___textureBlood_27 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___textureBlood_27), (void*)L_0);
		// return textureBlood != null;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_1 = __this->___textureBlood_27;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		return L_2;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision_UpdateCustomValues_m93BA301691BCB93197C020AA31D09C17B1773D9F (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableDamage, damage);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableDamage_28;
		float L_2 = __this->___damage_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, L_2, NULL);
		// material.SetFloat(variableBrightness, damageBrightness);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableBrightness_31;
		float L_5 = __this->___damageBrightness_23;
		NullCheck(L_3);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_3, L_4, L_5, NULL);
		// material.SetFloat(variableDefinition, definition * 15.0f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_7 = __this->___variableDefinition_29;
		float L_8 = __this->___definition_22;
		NullCheck(L_6);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_6, L_7, ((float)il2cpp_codegen_multiply(L_8, (15.0f))), NULL);
		// material.SetFloat(variableDistortion, distortion * 0.2f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_10 = __this->___variableDistortion_30;
		float L_11 = __this->___distortion_24;
		NullCheck(L_9);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_9, L_10, ((float)il2cpp_codegen_multiply(L_11, (0.200000003f))), NULL);
		// material.SetColor(variableColor, color);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_12 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_13 = __this->___variableColor_33;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_14 = __this->___color_25;
		NullCheck(L_12);
		Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A(L_12, L_13, L_14, NULL);
		// material.SetInt(variableBlendOp, (int)blendOp);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_16 = __this->___variableBlendOp_32;
		int32_t L_17 = __this->___blendOp_26;
		NullCheck(L_15);
		Material_SetInt_m9C05C9D7C152CFCC48F8572710F89DDAC9277E39(L_15, L_16, L_17, NULL);
		// material.SetTexture(variableBloodTex, textureBlood);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_19 = __this->___variableBloodTex_34;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_20 = __this->___textureBlood_27;
		NullCheck(L_18);
		Material_SetTexture_mA9F8461850AAB88F992E9C6FA6F24C2E050B83FD(L_18, L_19, L_20, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.DamageVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DamageVision__ctor_mB674BCE67E8C42EDD08A1C22BBEA969C4A3BA218 (DamageVision_tA832E6D0ACBC73738A2390FBC7C6F12356E1B37B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1118AFABE1103004CB863B78763034F9F50EA9E3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1E55BC8EC00B694817EDB2E59B23EC798FEE4C6B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8BA48E25549E546BA07E0E2DCCA3614AF77655B2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC088354B64B509F0C56825791A871020EB85B388);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDEC9DF998711C9677E152E279E1932D746A66EDC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float damage = 0.7f;
		__this->___damage_21 = (0.699999988f);
		// private float definition = 0.5f;
		__this->___definition_22 = (0.5f);
		// private float damageBrightness = 3.0f;
		__this->___damageBrightness_23 = (3.0f);
		// private float distortion = 0.2f;
		__this->___distortion_24 = (0.200000003f);
		// private Color color = Color.red;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
		__this->___color_25 = L_0;
		// private VisionBlendOps blendOp = VisionBlendOps.Darken;
		__this->___blendOp_26 = 3;
		// private readonly int variableDamage = Shader.PropertyToID("_Damage");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral8BA48E25549E546BA07E0E2DCCA3614AF77655B2, NULL);
		__this->___variableDamage_28 = L_1;
		// private readonly int variableDefinition = Shader.PropertyToID("_Definition");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral1E55BC8EC00B694817EDB2E59B23EC798FEE4C6B, NULL);
		__this->___variableDefinition_29 = L_2;
		// private readonly int variableDistortion = Shader.PropertyToID("_Distortion");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral1118AFABE1103004CB863B78763034F9F50EA9E3, NULL);
		__this->___variableDistortion_30 = L_3;
		// private readonly int variableBrightness = Shader.PropertyToID("_DamageBrightness");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralDEC9DF998711C9677E152E279E1932D746A66EDC, NULL);
		__this->___variableBrightness_31 = L_4;
		// private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");
		int32_t L_5;
		L_5 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F, NULL);
		__this->___variableBlendOp_32 = L_5;
		// private readonly int variableColor = Shader.PropertyToID("_Color");
		int32_t L_6;
		L_6 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, NULL);
		__this->___variableColor_33 = L_6;
		// private readonly int variableBloodTex = Shader.PropertyToID("_BloodTex");
		int32_t L_7;
		L_7 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralC088354B64B509F0C56825791A871020EB85B388, NULL);
		__this->___variableBloodTex_34 = L_7;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::get_Strength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 DoubleVision_get_Strength_mB9E9A9E73A0314F82698341278A5D97A2AE92CE5 (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, const RuntimeMethod* method) 
{
	{
		// get { return strength; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___strength_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DoubleVision::set_Strength(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoubleVision_set_Strength_m13BDA79AD7DDAB8640CB09BD299455073BC240F9 (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) 
{
	{
		// set { strength = value; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___value0;
		__this->___strength_21 = L_0;
		// set { strength = value; }
		return;
	}
}
// UnityEngine.Vector2 Nephasto.VisionFXAsset.DoubleVision::get_Speed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 DoubleVision_get_Speed_mFC79A64332AA5A2EB6891BE7D8F1C70257339CF9 (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, const RuntimeMethod* method) 
{
	{
		// get { return speed; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___speed_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DoubleVision::set_Speed(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoubleVision_set_Speed_m118646F710BEAEB6980D7D7C054EE2E99B092192 (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) 
{
	{
		// set { speed = value; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___value0;
		__this->___speed_22 = L_0;
		// set { speed = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.DoubleVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DoubleVision_ToString_mA9E878C475CE68B48DEE9C4EAE1E93B762F1607D (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8255FBADDC7358D4D3FB7875A39A1DDD61115D22);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Double vision.";
		return _stringLiteral8255FBADDC7358D4D3FB7875A39A1DDD61115D22;
	}
}
// System.Void Nephasto.VisionFXAsset.DoubleVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoubleVision_ResetDefaultValues_m54A36A8861090336297E02A63B4A1084DDAA703C (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// strength = DefaultStrength;
		il2cpp_codegen_runtime_class_init_inline(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_StaticFields*)il2cpp_codegen_static_fields_for(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var))->___DefaultStrength_23;
		__this->___strength_21 = L_0;
		// speed = DefaultSpeed;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ((DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_StaticFields*)il2cpp_codegen_static_fields_for(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var))->___DefaultSpeed_24;
		__this->___speed_22 = L_1;
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.DoubleVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoubleVision_UpdateCustomValues_m1CAD383C98437D047D768FD09AECA31E2477FB43 (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, const RuntimeMethod* method) 
{
	{
		// material.SetVector(variableStrength, strength * 0.1f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableStrength_25;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = __this->___strength_21;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		L_3 = Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline(L_2, (0.100000001f), NULL);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_4;
		L_4 = Vector4_op_Implicit_m6162D8136CFE97A5A8BD3B764F9074DB96AA5CD0_inline(L_3, NULL);
		NullCheck(L_0);
		Material_SetVector_m44CD02D4555E2AF391C30700F0AEC36BA04CFEA7(L_0, L_1, L_4, NULL);
		// material.SetVector(variableSpeed, speed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_5 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_6 = __this->___variableSpeed_26;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = __this->___speed_22;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_8;
		L_8 = Vector4_op_Implicit_m6162D8136CFE97A5A8BD3B764F9074DB96AA5CD0_inline(L_7, NULL);
		NullCheck(L_5);
		Material_SetVector_m44CD02D4555E2AF391C30700F0AEC36BA04CFEA7(L_5, L_6, L_8, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.DoubleVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoubleVision__ctor_mE79DF73D8C9C23C94DE033103BDA67EEFE1D8F9F (DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5EA6233810A41959161DE2291ED95019779143DC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Vector2 strength = DefaultStrength;
		il2cpp_codegen_runtime_class_init_inline(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_StaticFields*)il2cpp_codegen_static_fields_for(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var))->___DefaultStrength_23;
		__this->___strength_21 = L_0;
		// private Vector2 speed = DefaultSpeed;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ((DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_StaticFields*)il2cpp_codegen_static_fields_for(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var))->___DefaultSpeed_24;
		__this->___speed_22 = L_1;
		// private readonly int variableStrength = Shader.PropertyToID("_Strength");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral5EA6233810A41959161DE2291ED95019779143DC, NULL);
		__this->___variableStrength_25 = L_2;
		// private readonly int variableSpeed = Shader.PropertyToID("_Speed");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75, NULL);
		__this->___variableSpeed_26 = L_3;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.DoubleVision::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoubleVision__cctor_m3A0A0E1B92D2D67C72AE41BA083F915FED9DE273 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Vector2 DefaultStrength = new Vector2(0.1f, 0.0f);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_0), (0.100000001f), (0.0f), /*hidden argument*/NULL);
		((DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_StaticFields*)il2cpp_codegen_static_fields_for(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var))->___DefaultStrength_23 = L_0;
		// public static Vector2 DefaultSpeed = new Vector2(2.0f, 0.0f);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_1), (2.0f), (0.0f), /*hidden argument*/NULL);
		((DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_StaticFields*)il2cpp_codegen_static_fields_for(DoubleVision_tA3DA50F7DC89DABE22074D9770CA09E04F56D94D_il2cpp_TypeInfo_var))->___DefaultSpeed_24 = L_1;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_Drunkenness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_Drunkenness_m4B016A097A788F736B5DE622D9BA46E97D925AD1 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return drunkenness; }
		float L_0 = __this->___drunkenness_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_Drunkenness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_Drunkenness_m22A0CCB96479F6D472435D98002D61A21D21D08B (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { drunkenness = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___drunkenness_21 = L_1;
		// set { drunkenness = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_DrunkSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_DrunkSpeed_m069B97829B457AE4EEAEEFDF476516EF9EE717BB (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return drunkSpeed; }
		float L_0 = __this->___drunkSpeed_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_DrunkSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_DrunkSpeed_m8CC31C58AB23DD8E35CF96AA5AE0480363F186FD (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { drunkSpeed = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___drunkSpeed_22 = L_1;
		// set { drunkSpeed = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_DrunkAmplitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_DrunkAmplitude_mB90389424BB01FB4AE19B8F22928E7C9306C8AE2 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return drunkAmplitude; }
		float L_0 = __this->___drunkAmplitude_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_DrunkAmplitude(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_DrunkAmplitude_mFA64482EC551D6D182DD7D819ADF29D0168D5F77 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { drunkAmplitude = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___drunkAmplitude_23 = L_1;
		// set { drunkAmplitude = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_Swinging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_Swinging_m3C366DAF83D069A8CD16524F6E3E12EBB229D1BB (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return swinging; }
		float L_0 = __this->___swinging_24;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_Swinging(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_Swinging_mA9E15E8F56EEA55A95B80771E6336E04EE20946D (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { swinging = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___swinging_24 = L_1;
		// set { swinging = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_SwingingSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_SwingingSpeed_m093AB6ED7DD44808C1FDADCEC8451EFD1A32726B (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return swingingSpeed; }
		float L_0 = __this->___swingingSpeed_25;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_SwingingSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_SwingingSpeed_m15921BEA569B0C9FE0714963F02EFB8E2AB20749 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { swingingSpeed = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___swingingSpeed_25 = L_1;
		// set { swingingSpeed = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_Aberration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_Aberration_mDBB055641D7215504F04CFB2B425F1AC71F171F2 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return aberration; }
		float L_0 = __this->___aberration_26;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_Aberration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_Aberration_m6807E76B95BEC0A3AA35233B3AEB1AD4C57FB0B0 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { aberration = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___aberration_26 = L_1;
		// set { aberration = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_AberrationSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_AberrationSpeed_m3A9B650322165AA14CC45E6C21BD597C56B08BE2 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return aberrationSpeed; }
		float L_0 = __this->___aberrationSpeed_27;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_AberrationSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_AberrationSpeed_m238F0D49A3DCD809EECFC66E67C2D23A5132B5E9 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { aberrationSpeed = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___aberrationSpeed_27 = L_1;
		// set { aberrationSpeed = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_VignetteAmount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_VignetteAmount_mEB5EBF619ECD1C3FE22D03E64273AB54E528D1E4 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return vignetteAmount; }
		float L_0 = __this->___vignetteAmount_29;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_VignetteAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_VignetteAmount_mF06F4693F902C822AD6031D981509BB9D7440D10 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { vignetteAmount = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___vignetteAmount_29 = L_1;
		// set { vignetteAmount = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.DrunkVision::get_VignetteSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DrunkVision_get_VignetteSpeed_m12B22964A7387C240F8794AC387BDD079AC952F1 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// get { return vignetteSpeed; }
		float L_0 = __this->___vignetteSpeed_28;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::set_VignetteSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_set_VignetteSpeed_m30B905F789F8F8AA4DD1F82A23FCB6B07F752E11 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { vignetteSpeed = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___vignetteSpeed_28 = L_1;
		// set { vignetteSpeed = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.DrunkVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DrunkVision_ToString_m5E2397D205484F5602471F383E43B84AC16D297E (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD4991844ED24CE6E2F134FB97A3708C79877E6BE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "You've had too much to drink, go home.";
		return _stringLiteralD4991844ED24CE6E2F134FB97A3708C79877E6BE;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_ResetDefaultValues_mCBAE8D64F6831E8D00AFD8D1BD56DB06430A659A (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// drunkenness = 0.1f;
		__this->___drunkenness_21 = (0.100000001f);
		// drunkSpeed = 1.0f;
		__this->___drunkSpeed_22 = (1.0f);
		// drunkAmplitude = 0.75f;
		__this->___drunkAmplitude_23 = (0.75f);
		// swinging = 0.25f;
		__this->___swinging_24 = (0.25f);
		// swingingSpeed = 2.0f;
		__this->___swingingSpeed_25 = (2.0f);
		// aberration = 1.0f;
		__this->___aberration_26 = (1.0f);
		// aberrationSpeed = 1.0f;
		__this->___aberrationSpeed_27 = (1.0f);
		// vignetteAmount = 0.75f;
		__this->___vignetteAmount_29 = (0.75f);
		// vignetteSpeed = 1.0f;
		__this->___vignetteSpeed_28 = (1.0f);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision_UpdateCustomValues_m0E49E4633C48B1EADF2549E1ABA288E84391EC81 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableDrunkenness, 1.0f - drunkenness);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableDrunkenness_30;
		float L_2 = __this->___drunkenness_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, ((float)il2cpp_codegen_subtract((1.0f), L_2)), NULL);
		// material.SetFloat(variableDrunkSpeed, drunkSpeed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableDrunkSpeed_31;
		float L_5 = __this->___drunkSpeed_22;
		NullCheck(L_3);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_3, L_4, L_5, NULL);
		// material.SetFloat(variableDrunkAmplitude, 0.3f * drunkAmplitude);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_7 = __this->___variableDrunkAmplitude_32;
		float L_8 = __this->___drunkAmplitude_23;
		NullCheck(L_6);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_6, L_7, ((float)il2cpp_codegen_multiply((0.300000012f), L_8)), NULL);
		// material.SetFloat(variableSwinging, 0.01f * swinging);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_10 = __this->___variableSwinging_33;
		float L_11 = __this->___swinging_24;
		NullCheck(L_9);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_9, L_10, ((float)il2cpp_codegen_multiply((0.00999999978f), L_11)), NULL);
		// material.SetFloat(variableSwingingSpeed, swingingSpeed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_12 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_13 = __this->___variableSwingingSpeed_34;
		float L_14 = __this->___swingingSpeed_25;
		NullCheck(L_12);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_12, L_13, L_14, NULL);
		// material.SetFloat(variableAberration, aberration);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_16 = __this->___variableAberration_35;
		float L_17 = __this->___aberration_26;
		NullCheck(L_15);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_15, L_16, L_17, NULL);
		// material.SetFloat(variableAberrationSpeed, aberrationSpeed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_19 = __this->___variableAberrationSpeed_36;
		float L_20 = __this->___aberrationSpeed_27;
		NullCheck(L_18);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_18, L_19, L_20, NULL);
		// material.SetFloat(variableVignetteAmount, vignetteAmount);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_22 = __this->___variableVignetteAmount_37;
		float L_23 = __this->___vignetteAmount_29;
		NullCheck(L_21);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_21, L_22, L_23, NULL);
		// material.SetFloat(variableVignetteSpeed, vignetteSpeed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_24 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_25 = __this->___variableVignetteSpeed_38;
		float L_26 = __this->___vignetteSpeed_28;
		NullCheck(L_24);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_24, L_25, L_26, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.DrunkVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrunkVision__ctor_m65844EE71BF1E3090842E3BB534EE7BF5F3B45F8 (DrunkVision_t244D1B1F60A298C0EBA0F15CCCB03D33E7715721* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral08F73AC307A278B2744E30B89E9C4C2A97C43325);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C29B0DC949688A38E911584746B509F58451F2C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral63BAD6EA5E5AB548045F7637E204F29F523ED954);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68D4A36825993A8577CF44620C403255105DC4BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70C25592109D1296FB144FC169E0F31AE2533CA8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77F3383F9FF6D01CDD48AFF65AE7F93BEF1E4AA6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB97DDA10212FE943A960F63E3B37BD26F1CF5A86);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD4ACFAB09591C2A1F263839B86F2E7CEBC1792EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE20F6A9EA5C3CD8AB1008454765AC8A0748565F6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float drunkenness = 0.1f;
		__this->___drunkenness_21 = (0.100000001f);
		// private float drunkSpeed = 1.0f;
		__this->___drunkSpeed_22 = (1.0f);
		// private float drunkAmplitude = 0.75f;
		__this->___drunkAmplitude_23 = (0.75f);
		// private float swinging = 0.25f;
		__this->___swinging_24 = (0.25f);
		// private float swingingSpeed = 2.0f;
		__this->___swingingSpeed_25 = (2.0f);
		// private float aberration = 1.0f;
		__this->___aberration_26 = (1.0f);
		// private float aberrationSpeed = 1.0f;
		__this->___aberrationSpeed_27 = (1.0f);
		// private float vignetteSpeed = 1.0f;
		__this->___vignetteSpeed_28 = (1.0f);
		// private float vignetteAmount = 0.75f;
		__this->___vignetteAmount_29 = (0.75f);
		// private readonly int variableDrunkenness = Shader.PropertyToID("_Drunkenness");
		int32_t L_0;
		L_0 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral4C29B0DC949688A38E911584746B509F58451F2C, NULL);
		__this->___variableDrunkenness_30 = L_0;
		// private readonly int variableDrunkSpeed = Shader.PropertyToID("_DrunkSpeed");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral68D4A36825993A8577CF44620C403255105DC4BE, NULL);
		__this->___variableDrunkSpeed_31 = L_1;
		// private readonly int variableDrunkAmplitude = Shader.PropertyToID("_DrunkAmplitude");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralB97DDA10212FE943A960F63E3B37BD26F1CF5A86, NULL);
		__this->___variableDrunkAmplitude_32 = L_2;
		// private readonly int variableSwinging = Shader.PropertyToID("_Swinging");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral63BAD6EA5E5AB548045F7637E204F29F523ED954, NULL);
		__this->___variableSwinging_33 = L_3;
		// private readonly int variableSwingingSpeed = Shader.PropertyToID("_SwingingSpeed");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral77F3383F9FF6D01CDD48AFF65AE7F93BEF1E4AA6, NULL);
		__this->___variableSwingingSpeed_34 = L_4;
		// private readonly int variableAberration = Shader.PropertyToID("_Aberration");
		int32_t L_5;
		L_5 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral70C25592109D1296FB144FC169E0F31AE2533CA8, NULL);
		__this->___variableAberration_35 = L_5;
		// private readonly int variableAberrationSpeed = Shader.PropertyToID("_AberrationSpeed");
		int32_t L_6;
		L_6 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralE20F6A9EA5C3CD8AB1008454765AC8A0748565F6, NULL);
		__this->___variableAberrationSpeed_36 = L_6;
		// private readonly int variableVignetteAmount = Shader.PropertyToID("_VignetteAmount");
		int32_t L_7;
		L_7 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral08F73AC307A278B2744E30B89E9C4C2A97C43325, NULL);
		__this->___variableVignetteAmount_37 = L_7;
		// private readonly int variableVignetteSpeed = Shader.PropertyToID("_VignetteSpeed");
		int32_t L_8;
		L_8 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralD4ACFAB09591C2A1F263839B86F2E7CEBC1792EB, NULL);
		__this->___variableVignetteSpeed_38 = L_8;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.FisheyeVision::get_Barrel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FisheyeVision_get_Barrel_mAAD9A51149E28D4A00A3DFE9B63BA6A361D8B5D9 (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, const RuntimeMethod* method) 
{
	{
		// get { return barrel; }
		float L_0 = __this->___barrel_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.FisheyeVision::set_Barrel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FisheyeVision_set_Barrel_m37B83913C6FA9D7AC390A0E1780DD1BD78EA8E81 (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { barrel = Mathf.Clamp(value, -1.0f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (-1.0f), (1.0f), NULL);
		__this->___barrel_21 = L_1;
		// set { barrel = Mathf.Clamp(value, -1.0f, 1.0f); }
		return;
	}
}
// UnityEngine.Vector2 Nephasto.VisionFXAsset.FisheyeVision::get_Center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 FisheyeVision_get_Center_mEE360783F0434E068537CCA0E2D18152A23F342A (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, const RuntimeMethod* method) 
{
	{
		// get { return center; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___center_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.FisheyeVision::set_Center(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FisheyeVision_set_Center_mE3CD2BEE1DB645FC2F0952663F0EC7CDFB02F436 (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) 
{
	{
		// set { center = value; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___value0;
		__this->___center_22 = L_0;
		// set { center = value; }
		return;
	}
}
// UnityEngine.Color Nephasto.VisionFXAsset.FisheyeVision::get_BackgroundColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F FisheyeVision_get_BackgroundColor_m4C1638597D45BAABE293DFC7CF5F4BF8076C74A4 (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, const RuntimeMethod* method) 
{
	{
		// get { return backgroundColor; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___backgroundColor_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.FisheyeVision::set_BackgroundColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FisheyeVision_set_BackgroundColor_mF4E697ED60D439AA4D84014864D0B2A6ABFA1E7D (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) 
{
	{
		// set { backgroundColor = value; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___value0;
		__this->___backgroundColor_23 = L_0;
		// set { backgroundColor = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.FisheyeVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FisheyeVision_ToString_m0A40DC40AF2061064EE417C7F0467DA2C5F8259B (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC041B83A217B198F836EC71B5B995A22F4B6738A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Fisheye / anti-fisheye deformation.";
		return _stringLiteralC041B83A217B198F836EC71B5B995A22F4B6738A;
	}
}
// System.Void Nephasto.VisionFXAsset.FisheyeVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FisheyeVision_ResetDefaultValues_m1F34DADA7875EA98DDC98EA355E86A09B39629F1 (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// barrel = 0.0f;
		__this->___barrel_21 = (0.0f);
		// center = DefaultCenter;
		il2cpp_codegen_runtime_class_init_inline(FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_StaticFields*)il2cpp_codegen_static_fields_for(FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var))->___DefaultCenter_24;
		__this->___center_22 = L_0;
		// backgroundColor = Color.black;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
		__this->___backgroundColor_23 = L_1;
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.FisheyeVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FisheyeVision_UpdateCustomValues_m74569483C1F3CA0A4F85A2CBE5F53157867F671A (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableBarrel, barrel + 0.5f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableBarrel_25;
		float L_2 = __this->___barrel_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, ((float)il2cpp_codegen_add(L_2, (0.5f))), NULL);
		// material.SetVector(variableCenter, center);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableCenter_26;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = __this->___center_22;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_6;
		L_6 = Vector4_op_Implicit_m6162D8136CFE97A5A8BD3B764F9074DB96AA5CD0_inline(L_5, NULL);
		NullCheck(L_3);
		Material_SetVector_m44CD02D4555E2AF391C30700F0AEC36BA04CFEA7(L_3, L_4, L_6, NULL);
		// material.SetColor(variableBackgroundColor, backgroundColor);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_7 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_8 = __this->___variableBackgroundColor_27;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_9 = __this->___backgroundColor_23;
		NullCheck(L_7);
		Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A(L_7, L_8, L_9, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.FisheyeVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FisheyeVision__ctor_m88459E2AF52EC50B48DB49523E9ACB1219EDB675 (FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral279E9DAEE8430F8DA304C3E3D7D1A0E3EBB41E05);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral34DBF2F9982594656913F0861C8E8F05CA2CB5F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral787984D270B549500FD6EE450785085D7058DF70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Vector2 center = DefaultCenter;
		il2cpp_codegen_runtime_class_init_inline(FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_StaticFields*)il2cpp_codegen_static_fields_for(FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var))->___DefaultCenter_24;
		__this->___center_22 = L_0;
		// private Color backgroundColor = Color.black;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
		__this->___backgroundColor_23 = L_1;
		// private readonly int variableBarrel = Shader.PropertyToID("_Barrel");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral34DBF2F9982594656913F0861C8E8F05CA2CB5F0, NULL);
		__this->___variableBarrel_25 = L_2;
		// private readonly int variableCenter = Shader.PropertyToID("_Center");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral787984D270B549500FD6EE450785085D7058DF70, NULL);
		__this->___variableCenter_26 = L_3;
		// private readonly int variableBackgroundColor = Shader.PropertyToID("_BackgroundColor");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral279E9DAEE8430F8DA304C3E3D7D1A0E3EBB41E05, NULL);
		__this->___variableBackgroundColor_27 = L_4;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.FisheyeVision::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FisheyeVision__cctor_m503B13CA7B03D80598AA7908C2B1D65CC180684F (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Vector2 DefaultCenter = new Vector2(0.5f, 0.5f);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_0), (0.5f), (0.5f), /*hidden argument*/NULL);
		((FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_StaticFields*)il2cpp_codegen_static_fields_for(FisheyeVision_tAEFFE2E06E83AC9B9DA009A33DB94189D00DC73F_il2cpp_TypeInfo_var))->___DefaultCenter_24 = L_0;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 Nephasto.VisionFXAsset.GhostVision::get_Focus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GhostVision_get_Focus_m83D84B2DC62539643769BBF5DE8BEA4AB63320B9 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return focus; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___focus_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_Focus(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_Focus_m1361CF231D3A8CA22A0BD0332EF648328FB396CD (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) 
{
	{
		// set { focus = value; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___value0;
		__this->___focus_21 = L_0;
		// set { focus = value; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_Speed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_Speed_m5C00C31696069669807AB4D0B029088A5C0B9776 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return speed; }
		float L_0 = __this->___speed_26;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_Speed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_Speed_m2FB5AE8C17718F9E7210D7F08C5D28E8359C2803 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { speed = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___speed_26 = L_1;
		// set { speed = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_Aperture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_Aperture_m1DD5C011D9C4AFC7B7A3C8ABAC480D1788C1034D (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return aperture; }
		float L_0 = __this->___aperture_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_Aperture(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_Aperture_mBE8788395C1780BF42E70E638C26DBE0AC3769C9 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { aperture = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___aperture_22 = L_1;
		// set { aperture = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_Zoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_Zoom_m63F6446A85E4C76142E037CC263BD826488AA34E (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return zoom; }
		float L_0 = __this->___zoom_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_Zoom(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_Zoom_m49EA436E0F80507BB10CE1804CABDBE5CA27848B (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { zoom = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___zoom_23 = L_1;
		// set { zoom = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Boolean Nephasto.VisionFXAsset.GhostVision::get_ChangeFOV()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GhostVision_get_ChangeFOV_m17E79258473DECCB1257AD41277383426DF1F8A5 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return changeFOV; }
		bool L_0 = __this->___changeFOV_24;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_ChangeFOV(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_ChangeFOV_m23B07FEBD0292E7196DCA1F75C2F29C8DC9BADB1 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// set { changeFOV = value; }
		bool L_0 = ___value0;
		__this->___changeFOV_24 = L_0;
		// set { changeFOV = value; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_FOV()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_FOV_mF978D1FF6DBF582DC970192D968D2899BDBACF83 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return fov; }
		float L_0 = __this->___fov_25;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_FOV(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_FOV_mFA1A48F5B512AFA399F0A217FCD67728A592B2DD (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { fov = Mathf.Clamp(value, 1.0f, 179.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (1.0f), (179.0f), NULL);
		__this->___fov_25 = L_1;
		// set { fov = Mathf.Clamp(value, 1.0f, 179.0f); }
		return;
	}
}
// System.Boolean Nephasto.VisionFXAsset.GhostVision::get_AspectRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GhostVision_get_AspectRatio_m2187C3B59300892B3F309D7586A3739216955765 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return aspectRatio; }
		bool L_0 = __this->___aspectRatio_27;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_AspectRatio(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_AspectRatio_mD74BF7C02D42D96BBF3C5ADB9EBC14FC1BCA1599 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// set { aspectRatio = value; }
		bool L_0 = ___value0;
		__this->___aspectRatio_27 = L_0;
		// set { aspectRatio = value; }
		return;
	}
}
// UnityEngine.Color Nephasto.VisionFXAsset.GhostVision::get_InnerTint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GhostVision_get_InnerTint_m8969F191DE9F23C5E19F4AA785992C623F2109D8 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return innerTint; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___innerTint_28;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerTint(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_InnerTint_m7BC7A5641027B43326A85B983DBE271B3010D840 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) 
{
	{
		// set { innerTint = value; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___value0;
		__this->___innerTint_28 = L_0;
		// set { innerTint = value; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerSaturation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_InnerSaturation_mD37E162D3523CC815CB6778E88CE5D570DC7758B (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return innerSaturation; }
		float L_0 = __this->___innerSaturation_29;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerSaturation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_InnerSaturation_mA994685C317B066B30D5ED1D3EDBD13B9860827A (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { innerSaturation = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___innerSaturation_29 = L_1;
		// set { innerSaturation = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerBrightness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_InnerBrightness_m96A4A295088DC3B47C3C35E6BBBDE96569E2DE99 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return innerBrightness; }
		float L_0 = __this->___innerBrightness_30;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerBrightness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_InnerBrightness_mBCFC4C6195C3CE3237FD8F799D6948FA1C1667C5 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { innerBrightness = Mathf.Clamp(value, -1.0f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (-1.0f), (1.0f), NULL);
		__this->___innerBrightness_30 = L_1;
		// set { innerBrightness = Mathf.Clamp(value, -1.0f, 1.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerContrast()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_InnerContrast_mEDD2E7BC9B5664B5FE3FD49DF2268FDDA5105D33 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return innerContrast; }
		float L_0 = __this->___innerContrast_31;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerContrast(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_InnerContrast_m71C82DA577CCA4CEE6F0D2ED6420B2569D99A737 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { innerContrast = Mathf.Clamp(value, -1.0f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (-1.0f), (1.0f), NULL);
		__this->___innerContrast_31 = L_1;
		// set { innerContrast = Mathf.Clamp(value, -1.0f, 1.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_InnerGamma()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_InnerGamma_m85598BBD7343801AD22ADA1BC0F11394B862478A (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return innerGamma; }
		float L_0 = __this->___innerGamma_32;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_InnerGamma(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_InnerGamma_mB04F93900371B0CE63392305A6664EEC38523C81 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { innerGamma = Mathf.Clamp(value, 0.1f, 5.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (0.100000001f), (5.0f), NULL);
		__this->___innerGamma_32 = L_1;
		// set { innerGamma = Mathf.Clamp(value, 0.1f, 5.0f); }
		return;
	}
}
// UnityEngine.Color Nephasto.VisionFXAsset.GhostVision::get_OuterTint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GhostVision_get_OuterTint_mFAE2D10C6BC94043F3176FCB973722AA14482728 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return outerTint; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___outerTint_33;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterTint(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_OuterTint_m243D017D05941624AF21A59AEEFEC60EE6F69913 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) 
{
	{
		// set { outerTint = value; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___value0;
		__this->___outerTint_33 = L_0;
		// set { outerTint = value; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterSaturation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_OuterSaturation_mB27D4E2BF9374B1295A410B384A28C0C06DC8D45 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return outerSaturation; }
		float L_0 = __this->___outerSaturation_34;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterSaturation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_OuterSaturation_m419E899A64DD9003997EB5B4DD68042818FAC982 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { outerSaturation = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___outerSaturation_34 = L_1;
		// set { outerSaturation = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterBrightness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_OuterBrightness_m5A5B9513D6260849865FE58BC07F66AC120F3B4B (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return outerBrightness; }
		float L_0 = __this->___outerBrightness_35;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterBrightness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_OuterBrightness_m7ABB33C407071C70C543D9969B39EE296F400B7E (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { outerBrightness = Mathf.Clamp(value, -1.0f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (-1.0f), (1.0f), NULL);
		__this->___outerBrightness_35 = L_1;
		// set { outerBrightness = Mathf.Clamp(value, -1.0f, 1.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterContrast()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_OuterContrast_m075E41572DB0668B7EED3F0CED2D93E39B261CB4 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return outerContrast; }
		float L_0 = __this->___outerContrast_36;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterContrast(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_OuterContrast_m9AF72FEE6A20EAC7D8FE2090005468887BA5A0D6 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { outerContrast = Mathf.Clamp(value, -1.0f, 1.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (-1.0f), (1.0f), NULL);
		__this->___outerContrast_36 = L_1;
		// set { outerContrast = Mathf.Clamp(value, -1.0f, 1.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.GhostVision::get_OuterGamma()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GhostVision_get_OuterGamma_m88EB076011A9B3117840B8487BC0B6151E5B5AE1 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// get { return outerGamma; }
		float L_0 = __this->___outerGamma_37;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::set_OuterGamma(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_set_OuterGamma_m67CEA335601A0DA541354594039005417F664B0F (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { outerGamma = Mathf.Clamp(value, 0.1f, 5.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (0.100000001f), (5.0f), NULL);
		__this->___outerGamma_37 = L_1;
		// set { outerGamma = Mathf.Clamp(value, 0.1f, 5.0f); }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.GhostVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GhostVision_ToString_mA30CA772F008A8A6C7023C4621090F198089A8EA (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB578CD2995766CACFF1C04975FE80041345AF9EB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Mimics the first person view of strange creatures like aliens or ghosts.";
		return _stringLiteralB578CD2995766CACFF1C04975FE80041345AF9EB;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_ResetDefaultValues_m67FF09673E901C5B10FE4952C11F4B91D235E42B (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// focus = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		__this->___focus_21 = L_0;
		// speed = 0.5f;
		__this->___speed_26 = (0.5f);
		// aperture = 1.0f;
		__this->___aperture_22 = (1.0f);
		// zoom = 1.0f;
		__this->___zoom_23 = (1.0f);
		// fov = 60.0f;
		__this->___fov_25 = (60.0f);
		// aspectRatio = true;
		__this->___aspectRatio_27 = (bool)1;
		// innerTint = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		__this->___innerTint_28 = L_1;
		// innerSaturation = 1.0f;
		__this->___innerSaturation_29 = (1.0f);
		// innerBrightness = 0.0f;
		__this->___innerBrightness_30 = (0.0f);
		// innerContrast = 0.0f;
		__this->___innerContrast_31 = (0.0f);
		// innerGamma = 1.0f;
		__this->___innerGamma_32 = (1.0f);
		// outerTint = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2;
		L_2 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		__this->___outerTint_33 = L_2;
		// outerSaturation = 1.0f;
		__this->___outerSaturation_34 = (1.0f);
		// outerBrightness = 0.0f;
		__this->___outerBrightness_35 = (0.0f);
		// outerContrast = 0.0f;
		__this->___outerContrast_36 = (0.0f);
		// outerGamma = 1.0f;
		__this->___outerGamma_37 = (1.0f);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_UpdateCustomValues_mFC2177522A0CAD865C3D389C17C44C55A849ED68 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral64A6F97402A358B21F61FB07E26E6BD2DF1B8F5A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (cam != null && changeFOV == true && Application.isPlaying == true)
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_0 = __this->___cam_38;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		bool L_2 = __this->___changeFOV_24;
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		bool L_3;
		L_3 = Application_get_isPlaying_m0B3B501E1093739F8887A0DAC5F61D9CB49CC337(NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		// cam.fieldOfView = Mathf.Lerp(originalFOV, fov, Amount);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_4 = __this->___cam_38;
		float L_5 = __this->___originalFOV_39;
		float L_6 = __this->___fov_25;
		float L_7;
		L_7 = BaseVision_get_Amount_mA9FE6409D906BEAA982D5437F3AF0A32E4CE4137_inline(__this, NULL);
		float L_8;
		L_8 = Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline(L_5, L_6, L_7, NULL);
		NullCheck(L_4);
		Camera_set_fieldOfView_m5AA9EED4D1603A1DEDBF883D9C42814B2BDEB777(L_4, L_8, NULL);
	}

IL_003f:
	{
		// material.SetVector(variableFocus, focus + Vector2.one);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_10 = __this->___variableFocus_41;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_11 = __this->___focus_21;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		L_12 = Vector2_get_one_m232E885D3C7BB6A96D5FEF4494709BA170447604_inline(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_13;
		L_13 = Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline(L_11, L_12, NULL);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_14;
		L_14 = Vector4_op_Implicit_m6162D8136CFE97A5A8BD3B764F9074DB96AA5CD0_inline(L_13, NULL);
		NullCheck(L_9);
		Material_SetVector_m44CD02D4555E2AF391C30700F0AEC36BA04CFEA7(L_9, L_10, L_14, NULL);
		// material.SetFloat(variableSpeed, speed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_16 = __this->___variableSpeed_42;
		float L_17 = __this->___speed_26;
		NullCheck(L_15);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_15, L_16, L_17, NULL);
		// material.SetFloat(variableAperture, aperture);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_19 = __this->___variableAperture_43;
		float L_20 = __this->___aperture_22;
		NullCheck(L_18);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_18, L_19, L_20, NULL);
		// material.SetFloat(variableZoom, zoom + 0.05f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_22 = __this->___variableZoom_44;
		float L_23 = __this->___zoom_23;
		NullCheck(L_21);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_21, L_22, ((float)il2cpp_codegen_add(L_23, (0.0500000007f))), NULL);
		// if (aspectRatio == true)
		bool L_24 = __this->___aspectRatio_27;
		if (!L_24)
		{
			goto IL_00c8;
		}
	}
	{
		// material.EnableKeyword(keywordAspectRatio);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_25 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		NullCheck(L_25);
		Material_EnableKeyword_mE8523EF6CF694284DF976D47ADEDE9363A1174AC(L_25, _stringLiteral64A6F97402A358B21F61FB07E26E6BD2DF1B8F5A, NULL);
	}

IL_00c8:
	{
		// material.SetColor(variableInnerColor, innerTint);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_26 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_27 = __this->___variableInnerColor_45;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_28 = __this->___innerTint_28;
		NullCheck(L_26);
		Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A(L_26, L_27, L_28, NULL);
		// material.SetFloat(variableInnerSaturation, innerSaturation);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_29 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_30 = __this->___variableInnerSaturation_46;
		float L_31 = __this->___innerSaturation_29;
		NullCheck(L_29);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_29, L_30, L_31, NULL);
		// material.SetFloat(variableInnerBrightness, innerBrightness);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_32 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_33 = __this->___variableInnerBrightness_47;
		float L_34 = __this->___innerBrightness_30;
		NullCheck(L_32);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_32, L_33, L_34, NULL);
		// material.SetFloat(variableInnerContrast, innerContrast + 1.0f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_35 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_36 = __this->___variableInnerContrast_48;
		float L_37 = __this->___innerContrast_31;
		NullCheck(L_35);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_35, L_36, ((float)il2cpp_codegen_add(L_37, (1.0f))), NULL);
		// material.SetFloat(variableInnerGamma, 1.0f / innerGamma);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_38 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_39 = __this->___variableInnerGamma_49;
		float L_40 = __this->___innerGamma_32;
		NullCheck(L_38);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_38, L_39, ((float)((1.0f)/L_40)), NULL);
		// material.SetColor(variableOuterColor, outerTint);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_41 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_42 = __this->___variableOuterColor_50;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_43 = __this->___outerTint_33;
		NullCheck(L_41);
		Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A(L_41, L_42, L_43, NULL);
		// material.SetFloat(variableOuterSaturation, outerSaturation);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_44 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_45 = __this->___variableOuterSaturation_51;
		float L_46 = __this->___outerSaturation_34;
		NullCheck(L_44);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_44, L_45, L_46, NULL);
		// material.SetFloat(variableOuterBrightness, outerBrightness);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_47 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_48 = __this->___variableOuterBrightness_52;
		float L_49 = __this->___outerBrightness_35;
		NullCheck(L_47);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_47, L_48, L_49, NULL);
		// material.SetFloat(variableOuterContrast, outerContrast + 1.0f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_50 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_51 = __this->___variableOuterContrast_53;
		float L_52 = __this->___outerContrast_36;
		NullCheck(L_50);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_50, L_51, ((float)il2cpp_codegen_add(L_52, (1.0f))), NULL);
		// material.SetFloat(variableOuterGamma, 1.0f / outerGamma);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_53 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_54 = __this->___variableOuterGamma_54;
		float L_55 = __this->___outerGamma_37;
		NullCheck(L_53);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_53, L_54, ((float)((1.0f)/L_55)), NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_OnEnable_m251F916A1ED1AE8EF7AC8DFE62CBEF17555A47B9 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCamera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_m3B3C11550E48AA36AFF82788636EB163CC51FEE6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cam = this.gameObject.GetComponent<Camera>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_0);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_1;
		L_1 = GameObject_GetComponent_TisCamera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_m3B3C11550E48AA36AFF82788636EB163CC51FEE6(L_0, GameObject_GetComponent_TisCamera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_m3B3C11550E48AA36AFF82788636EB163CC51FEE6_RuntimeMethod_var);
		__this->___cam_38 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cam_38), (void*)L_1);
		// if (cam != null)
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_2 = __this->___cam_38;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		// originalFOV = cam.fieldOfView;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_4 = __this->___cam_38;
		NullCheck(L_4);
		float L_5;
		L_5 = Camera_get_fieldOfView_m9A93F17BBF89F496AE231C21817AFD1C1E833FBB(L_4, NULL);
		__this->___originalFOV_39 = L_5;
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision_OnDisable_m2C826469B02BC9FF6165AE9DFAA766A00356E8C5 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (cam != null && changeFOV == true)
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_0 = __this->___cam_38;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		bool L_2 = __this->___changeFOV_24;
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		// cam.fieldOfView = originalFOV;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_3 = __this->___cam_38;
		float L_4 = __this->___originalFOV_39;
		NullCheck(L_3);
		Camera_set_fieldOfView_m5AA9EED4D1603A1DEDBF883D9C42814B2BDEB777(L_3, L_4, NULL);
	}

IL_0027:
	{
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.GhostVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GhostVision__ctor_m8EA9624E9A09B44D6051AA5BE3C64CF357891E60 (GhostVision_tC1E82287C555C4BD8F9E527B30286C0B0E7D98D8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral064CA8962C00CA877F6164CAF986CDF79BB90B2F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2ABEEC7903CD21A4DE31201A5A876A061D112BB1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2D05D4C6010455CF7DA97873EF3F1AC6B9504472);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3492589282A6553D7A5D2193C5BA34B27EA87D61);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3AA10C2FCFFCD4A77470F2CC06E2B11F1ED772C1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5EA6233810A41959161DE2291ED95019779143DC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5F57584D841BB0295BB4448AA948A9C130EA0EC6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral877B02515584E67B57946246F931E4FB9A50C8C4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8AE933F3053DE01242D43ABE7E2D8F9913D69F31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAC01AD67122E2D4C7D3238187DCE19D4B5623BE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB963B5280FC457D3D26C1A238E40134D80F48A39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBD6F44625BEFB043FCCB42FD18E782DC3393475B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDB299F9A83A42E3F18FC199F4F086A2559F322DB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE8D178C06750E0D79C8AD3A8B0DFB2D6A8751A50);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Vector2 focus = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		__this->___focus_21 = L_0;
		// private float aperture = 1.0f;
		__this->___aperture_22 = (1.0f);
		// private float zoom = 1.0f;
		__this->___zoom_23 = (1.0f);
		// private float fov = 60.0f;
		__this->___fov_25 = (60.0f);
		// private float speed = 0.5f;
		__this->___speed_26 = (0.5f);
		// private bool aspectRatio = true;
		__this->___aspectRatio_27 = (bool)1;
		// private Color innerTint = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		__this->___innerTint_28 = L_1;
		// private float innerSaturation = 1.0f;
		__this->___innerSaturation_29 = (1.0f);
		// private float innerGamma = 1.0f;
		__this->___innerGamma_32 = (1.0f);
		// private Color outerTint = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2;
		L_2 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		__this->___outerTint_33 = L_2;
		// private float outerSaturation = 1.0f;
		__this->___outerSaturation_34 = (1.0f);
		// private float outerGamma = 1.0f;
		__this->___outerGamma_37 = (1.0f);
		// private float originalFOV = 60.0f;
		__this->___originalFOV_39 = (60.0f);
		// private readonly int variableStrength = Shader.PropertyToID("_Strength");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral5EA6233810A41959161DE2291ED95019779143DC, NULL);
		__this->___variableStrength_40 = L_3;
		// private readonly int variableFocus = Shader.PropertyToID("_Focus");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralB963B5280FC457D3D26C1A238E40134D80F48A39, NULL);
		__this->___variableFocus_41 = L_4;
		// private readonly int variableSpeed = Shader.PropertyToID("_Speed");
		int32_t L_5;
		L_5 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75, NULL);
		__this->___variableSpeed_42 = L_5;
		// private readonly int variableAperture = Shader.PropertyToID("_Aperture");
		int32_t L_6;
		L_6 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralDB299F9A83A42E3F18FC199F4F086A2559F322DB, NULL);
		__this->___variableAperture_43 = L_6;
		// private readonly int variableZoom = Shader.PropertyToID("_Zoom");
		int32_t L_7;
		L_7 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral877B02515584E67B57946246F931E4FB9A50C8C4, NULL);
		__this->___variableZoom_44 = L_7;
		// private readonly int variableInnerColor = Shader.PropertyToID("_InnerColor");
		int32_t L_8;
		L_8 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral3492589282A6553D7A5D2193C5BA34B27EA87D61, NULL);
		__this->___variableInnerColor_45 = L_8;
		// private readonly int variableInnerSaturation = Shader.PropertyToID("_InnerSaturation");
		int32_t L_9;
		L_9 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral8AE933F3053DE01242D43ABE7E2D8F9913D69F31, NULL);
		__this->___variableInnerSaturation_46 = L_9;
		// private readonly int variableInnerBrightness = Shader.PropertyToID("_InnerBrightness");
		int32_t L_10;
		L_10 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralBD6F44625BEFB043FCCB42FD18E782DC3393475B, NULL);
		__this->___variableInnerBrightness_47 = L_10;
		// private readonly int variableInnerContrast = Shader.PropertyToID("_InnerContrast");
		int32_t L_11;
		L_11 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral5F57584D841BB0295BB4448AA948A9C130EA0EC6, NULL);
		__this->___variableInnerContrast_48 = L_11;
		// private readonly int variableInnerGamma = Shader.PropertyToID("_InnerGamma");
		int32_t L_12;
		L_12 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralAC01AD67122E2D4C7D3238187DCE19D4B5623BE1, NULL);
		__this->___variableInnerGamma_49 = L_12;
		// private readonly int variableOuterColor = Shader.PropertyToID("_OuterColor");
		int32_t L_13;
		L_13 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralE8D178C06750E0D79C8AD3A8B0DFB2D6A8751A50, NULL);
		__this->___variableOuterColor_50 = L_13;
		// private readonly int variableOuterSaturation = Shader.PropertyToID("_OuterSaturation");
		int32_t L_14;
		L_14 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral3AA10C2FCFFCD4A77470F2CC06E2B11F1ED772C1, NULL);
		__this->___variableOuterSaturation_51 = L_14;
		// private readonly int variableOuterBrightness = Shader.PropertyToID("_OuterBrightness");
		int32_t L_15;
		L_15 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral064CA8962C00CA877F6164CAF986CDF79BB90B2F, NULL);
		__this->___variableOuterBrightness_52 = L_15;
		// private readonly int variableOuterContrast = Shader.PropertyToID("_OuterContrast");
		int32_t L_16;
		L_16 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral2ABEEC7903CD21A4DE31201A5A876A061D112BB1, NULL);
		__this->___variableOuterContrast_53 = L_16;
		// private readonly int variableOuterGamma = Shader.PropertyToID("_OuterGamma");
		int32_t L_17;
		L_17 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral2D05D4C6010455CF7DA97873EF3F1AC6B9504472, NULL);
		__this->___variableOuterGamma_54 = L_17;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HalftoneVision_get_Size_mFCEDB2CA788D25C089EDF59686B27C3B8A993D3A (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	{
		// get { return size; }
		float L_0 = __this->___size_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Size(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision_set_Size_mBAA26CC671B6A526F8750FDB79D895E57553EFAF (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { size = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___size_21 = L_1;
		// set { size = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Angle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HalftoneVision_get_Angle_m34FEC663E7EEC7C323A502A6B086B1F9EE730DBA (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	{
		// get { return angle; }
		float L_0 = __this->___angle_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Angle(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision_set_Angle_m44821AB81DC4949B95BA450BB7D1726436BB67FC (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { angle = Mathf.Clamp(value, 0.0f, 180.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (0.0f), (180.0f), NULL);
		__this->___angle_22 = L_1;
		// set { angle = Mathf.Clamp(value, 0.0f, 180.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Strength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HalftoneVision_get_Strength_m8997579AE96AECB574927F0B7260A688036A59DA (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	{
		// get { return strength; }
		float L_0 = __this->___strength_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Strength(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision_set_Strength_mE4FECF7189436D3F67B0471FC636EE82DC180DB0 (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { strength = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___strength_23 = L_1;
		// set { strength = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.HalftoneVision::get_Sensitivity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HalftoneVision_get_Sensitivity_mEC20D7774E7358F9C9C953F77C9E5AAE5B09743B (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	{
		// get { return sensitivity; }
		float L_0 = __this->___sensitivity_24;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::set_Sensitivity(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision_set_Sensitivity_m59110CEE59A294F100A71EAEFA8E3D18027E53FD (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { sensitivity = Mathf.Clamp(value, 0.0f, 10.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(L_0, (0.0f), (10.0f), NULL);
		__this->___sensitivity_24 = L_1;
		// set { sensitivity = Mathf.Clamp(value, 0.0f, 10.0f); }
		return;
	}
}
// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.HalftoneVision::get_BlendOp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HalftoneVision_get_BlendOp_m493F1ED9FCDBB9A453869020BB7C4CAFA36DA729 (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	{
		// get { return blendOp; }
		int32_t L_0 = __this->___blendOp_25;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision_set_BlendOp_m5B2929BB9E1B99933A0E5E64A913BC6A284B36F2 (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// set { blendOp = value; }
		int32_t L_0 = ___value0;
		__this->___blendOp_25 = L_0;
		// set { blendOp = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.HalftoneVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HalftoneVision_ToString_m4F175FBF603612DF2E8D0DD11180908956AF201D (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4F0E52BEEBAF3EF55547FBF053786CFF88BF6033);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Imitates the reprographic technique for printing newspapers and comics.";
		return _stringLiteral4F0E52BEEBAF3EF55547FBF053786CFF88BF6033;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision_ResetDefaultValues_m6B490D287DF7D3D83C54F9D4AB915ECD76148741 (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// size = 2.0f;
		__this->___size_21 = (2.0f);
		// angle = 15.0f;
		__this->___angle_22 = (15.0f);
		// strength = 10.0f;
		__this->___strength_23 = (10.0f);
		// sensitivity = 8.5f;
		__this->___sensitivity_24 = (8.5f);
		// blendOp = VisionBlendOps.Color;
		__this->___blendOp_25 = 2;
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision_UpdateCustomValues_m757839AF3E6B76D3CE9579D3E3FB53BDC8AF2B47 (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableSize, size * 100.0f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableSize_26;
		float L_2 = __this->___size_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, ((float)il2cpp_codegen_multiply(L_2, (100.0f))), NULL);
		// material.SetFloat(variableAngle, -angle * Mathf.Deg2Rad);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableAngle_27;
		float L_5 = __this->___angle_22;
		NullCheck(L_3);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_3, L_4, ((float)il2cpp_codegen_multiply(((-L_5)), (0.0174532924f))), NULL);
		// material.SetFloat(variableStrength, strength);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_7 = __this->___variableStrength_28;
		float L_8 = __this->___strength_23;
		NullCheck(L_6);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_6, L_7, L_8, NULL);
		// material.SetFloat(variableSensitivity, 10.0f - sensitivity);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_10 = __this->___variableSensitivity_29;
		float L_11 = __this->___sensitivity_24;
		NullCheck(L_9);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_9, L_10, ((float)il2cpp_codegen_subtract((10.0f), L_11)), NULL);
		// material.SetInt(variableBlendOp, (int)blendOp);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_12 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_13 = __this->___variableBlendOp_30;
		int32_t L_14 = __this->___blendOp_25;
		NullCheck(L_12);
		Material_SetInt_m9C05C9D7C152CFCC48F8572710F89DDAC9277E39(L_12, L_13, L_14, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.HalftoneVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalftoneVision__ctor_mD4FBB79CF42EBC1ED43E2C690F352C9570F9BAFE (HalftoneVision_t0C322119B8CECD7D5559D6ADE346D256A612A7BB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2C31C686E0C84134B4E5C55FE160ABE4C0D5031B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5EA6233810A41959161DE2291ED95019779143DC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E697C3514FCD429A08318C830A7FBC7F581D117);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralACA83AF7A62BB74E1867497F20E27DDA4AA09286);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float size = 2.0f;
		__this->___size_21 = (2.0f);
		// private float angle = 15.0f;
		__this->___angle_22 = (15.0f);
		// private float strength = 10.0f;
		__this->___strength_23 = (10.0f);
		// private float sensitivity = 8.5f;
		__this->___sensitivity_24 = (8.5f);
		// private VisionBlendOps blendOp = VisionBlendOps.Color;
		__this->___blendOp_25 = 2;
		// private readonly int variableSize = Shader.PropertyToID("_Size");
		int32_t L_0;
		L_0 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9E697C3514FCD429A08318C830A7FBC7F581D117, NULL);
		__this->___variableSize_26 = L_0;
		// private readonly int variableAngle = Shader.PropertyToID("_Angle");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralACA83AF7A62BB74E1867497F20E27DDA4AA09286, NULL);
		__this->___variableAngle_27 = L_1;
		// private readonly int variableStrength = Shader.PropertyToID("_Strength");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral5EA6233810A41959161DE2291ED95019779143DC, NULL);
		__this->___variableStrength_28 = L_2;
		// private readonly int variableSensitivity = Shader.PropertyToID("_Sensitivity");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral2C31C686E0C84134B4E5C55FE160ABE4C0D5031B, NULL);
		__this->___variableSensitivity_29 = L_3;
		// private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F, NULL);
		__this->___variableBlendOp_30 = L_4;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.LegoVision::get_Size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LegoVision_get_Size_mCE50096D5A20222DA3B3513106F46E5DD2B052D3 (LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195* __this, const RuntimeMethod* method) 
{
	{
		// get { return size; }
		float L_0 = __this->___size_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.LegoVision::set_Size(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LegoVision_set_Size_m6B1ABF493EB865C916D1E322B9406513003825B5 (LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { size = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___size_21 = L_1;
		// set { size = Mathf.Clamp01(value); }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.LegoVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LegoVision_ToString_mF819F287C76F37760F4F5006E02309F8290E10FB (LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE78D830BB90BEE5985357C495C2DCB038A1AC60B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "LEGO vision.";
		return _stringLiteralE78D830BB90BEE5985357C495C2DCB038A1AC60B;
	}
}
// System.Void Nephasto.VisionFXAsset.LegoVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LegoVision_ResetDefaultValues_m19E12C5BA193F3F4EE11DDE3B76EBB678DAB75E1 (LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// size = 0.5f;
		__this->___size_21 = (0.5f);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.LegoVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LegoVision_UpdateCustomValues_mB85AF7F250A205E53AE512DD2CA1373BD17C4661 (LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableSize, (1.001f - size) * 0.175f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableSize_22;
		float L_2 = __this->___size_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract((1.00100005f), L_2)), (0.174999997f))), NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.LegoVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LegoVision__ctor_mAC100C80832FD5D6EA9657D46C87AA4A352DBF23 (LegoVision_tD5C7F64080CA683C80D56A183B0AB22DA1BD0195* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E697C3514FCD429A08318C830A7FBC7F581D117);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float size = 0.5f;
		__this->___size_21 = (0.5f);
		// private readonly int variableSize = Shader.PropertyToID("_Size");
		int32_t L_0;
		L_0 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9E697C3514FCD429A08318C830A7FBC7F581D117, NULL);
		__this->___variableSize_22 = L_0;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.NeonVision::get_Edge()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NeonVision_get_Edge_m42C8DE5EBAA9A4A081430C3E946B5BC7A3992E41 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, const RuntimeMethod* method) 
{
	{
		// get { return edge; }
		float L_0 = __this->___edge_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.NeonVision::set_Edge(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NeonVision_set_Edge_m9FE4E4670324C3DB06E0236E1F74C674FB94EE24 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { edge = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___edge_21 = L_1;
		// set { edge = Mathf.Max(0.0f, value); }
		return;
	}
}
// UnityEngine.Color Nephasto.VisionFXAsset.NeonVision::get_EdgeColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F NeonVision_get_EdgeColor_mFB1C5552E8FE52E0DBC7981DD973D960C8A8A7A0 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, const RuntimeMethod* method) 
{
	{
		// get { return edgeColor; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___edgeColor_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.NeonVision::set_EdgeColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NeonVision_set_EdgeColor_m9E47E5E6F3152124A6448A44C88ECABDCDA456B8 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) 
{
	{
		// set { edgeColor = value; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___value0;
		__this->___edgeColor_22 = L_0;
		// set { edgeColor = value; }
		return;
	}
}
// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.NeonVision::get_BlendOp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NeonVision_get_BlendOp_m35F3D7D34D8DB6501285AF64B836BAE208EE6781 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, const RuntimeMethod* method) 
{
	{
		// get { return blendOp; }
		int32_t L_0 = __this->___blendOp_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.NeonVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NeonVision_set_BlendOp_m9132DF988C8E327BFA8C01B6EDF1634385FB108D (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// set { blendOp = value; }
		int32_t L_0 = ___value0;
		__this->___blendOp_23 = L_0;
		// set { blendOp = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.NeonVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NeonVision_ToString_mB480DB66FCEE0ECE89D1978D7D454134B50AB076 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral982614CA66F05802D4C005AA52F592157FABE7C5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Color the edges of the image.";
		return _stringLiteral982614CA66F05802D4C005AA52F592157FABE7C5;
	}
}
// System.Void Nephasto.VisionFXAsset.NeonVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NeonVision_ResetDefaultValues_m3AC702468BDABB0D8176D6A52CA77DE8C6E695B7 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// edge = 2.0f;
		__this->___edge_21 = (2.0f);
		// edgeColor = Color.cyan;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_cyan_m1257FED4776F2A33BD7250357D024B3FA3E592EB_inline(NULL);
		__this->___edgeColor_22 = L_0;
		// blendOp = VisionBlendOps.Additive;
		__this->___blendOp_23 = 0;
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.NeonVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NeonVision_UpdateCustomValues_m472BD8333BAFC14C769BAA57CE3C72C5C7577BAC (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableEdge, edge);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableEdge_24;
		float L_2 = __this->___edge_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, L_2, NULL);
		// material.SetColor(variableEdgeColor, edgeColor);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableEdgeColor_25;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_5 = __this->___edgeColor_22;
		NullCheck(L_3);
		Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A(L_3, L_4, L_5, NULL);
		// material.SetInt(variableBlendOp, (int)blendOp);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_7 = __this->___variableBlendOp_26;
		int32_t L_8 = __this->___blendOp_23;
		NullCheck(L_6);
		Material_SetInt_m9C05C9D7C152CFCC48F8572710F89DDAC9277E39(L_6, L_7, L_8, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.NeonVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NeonVision__ctor_mB34F5BDCED971C25D45E9D60CC656BA793F66607 (NeonVision_tADC82EFDE5B7D58E673C854F110B697A8C97B387* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6976D94E970B572A49ADA3FC7AC12958BA079D3A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFDB4B83E8F624B1E36286AB9AD4DBA5CB4D16B5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float edge = 2.0f;
		__this->___edge_21 = (2.0f);
		// private Color edgeColor = Color.cyan;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_cyan_m1257FED4776F2A33BD7250357D024B3FA3E592EB_inline(NULL);
		__this->___edgeColor_22 = L_0;
		// private readonly int variableEdge = Shader.PropertyToID("_Edge");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralBFDB4B83E8F624B1E36286AB9AD4DBA5CB4D16B5, NULL);
		__this->___variableEdge_24 = L_1;
		// private readonly int variableEdgeColor = Shader.PropertyToID("_EdgeColor");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral6976D94E970B572A49ADA3FC7AC12958BA079D3A, NULL);
		__this->___variableEdgeColor_25 = L_2;
		// private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F, NULL);
		__this->___variableBlendOp_26 = L_3;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Color Nephasto.VisionFXAsset.ScannerVision::get_Tint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ScannerVision_get_Tint_m8A5FA01103095ACDA617B5DC0ECDFE0240FF5D6B (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return tint; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___tint_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_Tint(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_Tint_m6AC479A0D97C41F3E54A82CD2BB7BF65C06DC68A (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) 
{
	{
		// set { tint = value; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___value0;
		__this->___tint_21 = L_0;
		// set { tint = value; }
		return;
	}
}
// System.Int32 Nephasto.VisionFXAsset.ScannerVision::get_LinesCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ScannerVision_get_LinesCount_m803AA34A8D5D9BFC26990EA264DE0878F3DEAB11 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return linesCount; }
		int32_t L_0 = __this->___linesCount_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_LinesCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_LinesCount_m1F455A4E4010DCB3CD0547B722801B0A75A253C1 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* G_B2_0 = NULL;
	ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* G_B3_1 = NULL;
	{
		// set { linesCount = value > 0 ? value : 0; }
		int32_t L_0 = ___value0;
		G_B1_0 = __this;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			G_B2_0 = __this;
			goto IL_0008;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_1 = ___value0;
		G_B3_0 = L_1;
		G_B3_1 = G_B2_0;
	}

IL_0009:
	{
		NullCheck(G_B3_1);
		G_B3_1->___linesCount_22 = G_B3_0;
		// set { linesCount = value > 0 ? value : 0; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ScannerVision::get_LinesStrength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ScannerVision_get_LinesStrength_mB034D696CEFBC6A01EADCE83554F27F9051CDF8F (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return linesStrength; }
		float L_0 = __this->___linesStrength_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_LinesStrength(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_LinesStrength_m1A7BB0E7DE3C6E3A96A4A94D8C3ADE33908FBDE0 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { linesStrength = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___linesStrength_23 = L_1;
		// set { linesStrength = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ScannerVision::get_ScanLineStrength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ScannerVision_get_ScanLineStrength_m2F3ECAC627497CC0308F2A37386EF0DAFF99A260 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return scanlineStrength; }
		float L_0 = __this->___scanlineStrength_24;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_ScanLineStrength(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_ScanLineStrength_m95525F7037CE5BAF401F05405FDA506B1FC9A2C0 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { scanlineStrength = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___scanlineStrength_24 = L_1;
		// set { scanlineStrength = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ScannerVision::get_ScanLineWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ScannerVision_get_ScanLineWidth_m450296C6F54ADBE4D99A798D0DBE26D9373833AF (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return scanlineWidth; }
		float L_0 = __this->___scanlineWidth_25;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_ScanLineWidth(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_ScanLineWidth_m2AE6E2AF895BBFD97A6A2DB78FEE0F04AF47DBCE (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { scanlineWidth = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___scanlineWidth_25 = L_1;
		// set { scanlineWidth = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ScannerVision::get_ScanLineSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ScannerVision_get_ScanLineSpeed_m679F75FA1E982DE43337AD3ED7D93774D5104A12 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return scanLineSpeed; }
		float L_0 = __this->___scanLineSpeed_26;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_ScanLineSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_ScanLineSpeed_m66831366B4A4A9BEF602F09D7C7CCBC48AC8898E (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { scanLineSpeed = value; }
		float L_0 = ___value0;
		__this->___scanLineSpeed_26 = L_0;
		// set { scanLineSpeed = value; }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ScannerVision::get_NoiseBandStrength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ScannerVision_get_NoiseBandStrength_mFBCC432C88BDD14E73C0A599D3837A9F7491DCE5 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return noiseBandStrength; }
		float L_0 = __this->___noiseBandStrength_27;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_NoiseBandStrength(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_NoiseBandStrength_m1E309EB96416F8253ED69E667D526B96F176810C (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { noiseBandStrength = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_0, NULL);
		__this->___noiseBandStrength_27 = L_1;
		// set { noiseBandStrength = Mathf.Clamp01(value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ScannerVision::get_NoiseBandWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ScannerVision_get_NoiseBandWidth_mD404013579053FD58F0C02A13B096798DA48B386 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return noiseBandWidth; }
		float L_0 = __this->___noiseBandWidth_28;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_NoiseBandWidth(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_NoiseBandWidth_m3AC0A73C590D995E2E42D2B8D0BAA756E49D13B3 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { noiseBandWidth = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___noiseBandWidth_28 = L_1;
		// set { noiseBandWidth = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ScannerVision::get_NoiseBandSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ScannerVision_get_NoiseBandSpeed_m7FE18A144797F466A9A1483F358D996F219C9F16 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// get { return noiseBandSpeed; }
		float L_0 = __this->___noiseBandSpeed_29;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::set_NoiseBandSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_set_NoiseBandSpeed_m827BCDA9342733A5076893D7A3A948684FF32F58 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { noiseBandSpeed = value; }
		float L_0 = ___value0;
		__this->___noiseBandSpeed_29 = L_0;
		// set { noiseBandSpeed = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.ScannerVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ScannerVision_ToString_m0758A0067B8A06FA6D5A6AD4974BC918331FC849 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4A01F66901FD2FF9020FDBA4575B3CBEF98F22D1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Scanner vision.";
		return _stringLiteral4A01F66901FD2FF9020FDBA4575B3CBEF98F22D1;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_ResetDefaultValues_mF7E752B62686817877EA9C5BA6D66E8F7A95E323 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// tint = DefaultTint;
		il2cpp_codegen_runtime_class_init_inline(ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ((ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_StaticFields*)il2cpp_codegen_static_fields_for(ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var))->___DefaultTint_30;
		__this->___tint_21 = L_0;
		// linesCount = 700;
		__this->___linesCount_22 = ((int32_t)700);
		// linesStrength = 0.1f;
		__this->___linesStrength_23 = (0.100000001f);
		// scanlineStrength = 0.5f;
		__this->___scanlineStrength_24 = (0.5f);
		// scanlineWidth = 0.2f;
		__this->___scanlineWidth_25 = (0.200000003f);
		// scanLineSpeed = 0.5f;
		__this->___scanLineSpeed_26 = (0.5f);
		// noiseBandStrength = 0.2f;
		__this->___noiseBandStrength_27 = (0.200000003f);
		// noiseBandWidth = 0.2f;
		__this->___noiseBandWidth_28 = (0.200000003f);
		// noiseBandSpeed = 0.2f;
		__this->___noiseBandSpeed_29 = (0.200000003f);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision_UpdateCustomValues_m8CF4E59541E0D347C5602D2112783A45DB2E33FE (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	{
		// material.SetColor(variableTint, tint);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableTint_31;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2 = __this->___tint_21;
		NullCheck(L_0);
		Material_SetColor_m573C88F2FB1B5A978C53A197B414F9E9C6AC5B9A(L_0, L_1, L_2, NULL);
		// material.SetFloat(variableLinesCount, linesCount);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableLinesCount_32;
		int32_t L_5 = __this->___linesCount_22;
		NullCheck(L_3);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_3, L_4, ((float)L_5), NULL);
		// material.SetFloat(variableLinesStrength, linesStrength);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_7 = __this->___variableLinesStrength_33;
		float L_8 = __this->___linesStrength_23;
		NullCheck(L_6);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_6, L_7, L_8, NULL);
		// material.SetFloat(variableScanLineStrength, scanlineStrength);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_10 = __this->___variableScanLineStrength_34;
		float L_11 = __this->___scanlineStrength_24;
		NullCheck(L_9);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_9, L_10, L_11, NULL);
		// material.SetFloat(variableScanLineWidth, scanlineWidth * 0.01f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_12 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_13 = __this->___variableScanLineWidth_35;
		float L_14 = __this->___scanlineWidth_25;
		NullCheck(L_12);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_12, L_13, ((float)il2cpp_codegen_multiply(L_14, (0.00999999978f))), NULL);
		// material.SetFloat(variableScanLineSpeed, scanLineSpeed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_16 = __this->___variableScanLineSpeed_36;
		float L_17 = __this->___scanLineSpeed_26;
		NullCheck(L_15);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_15, L_16, L_17, NULL);
		// material.SetFloat(variableNoiseBandStrength, noiseBandStrength);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_18 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_19 = __this->___variableNoiseBandStrength_37;
		float L_20 = __this->___noiseBandStrength_27;
		NullCheck(L_18);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_18, L_19, L_20, NULL);
		// material.SetFloat(variableNoiseBandWidth, noiseBandWidth);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_22 = __this->___variableNoiseBandWidth_38;
		float L_23 = __this->___noiseBandWidth_28;
		NullCheck(L_21);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_21, L_22, L_23, NULL);
		// material.SetFloat(variableNoiseBandSpeed, noiseBandSpeed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_24 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_25 = __this->___variableNoiseBandSpeed_39;
		float L_26 = __this->___noiseBandSpeed_29;
		NullCheck(L_24);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_24, L_25, L_26, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision__ctor_m24ECD54BBC133724409A898E009041AFC3B99496 (ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14F4FFFE4680A2B7D9EB6BD6180E9097EACB7BDA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral543BE502C8CE19D4D92897D3D3DDE32FB6563EBF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7B91B5FB0B511B2B078924DF85478132B50E3667);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8AB0A32AA70CEEEDF68589AA0B93CB1349239D55);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral930831AD69587F365337F7BD5E2D296692E0D130);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA0F2ACB0B3F377939DE4177970C04C49B4898FE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA4A5866BFA572A345EAE030A102601C627F2188E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC78BA9E4E8D721124AD624C314C85968C659273C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralECA169E2C11BA4F71637D2E1BF1D8ECAA03E8BF6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Color tint = DefaultTint;
		il2cpp_codegen_runtime_class_init_inline(ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ((ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_StaticFields*)il2cpp_codegen_static_fields_for(ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var))->___DefaultTint_30;
		__this->___tint_21 = L_0;
		// private int linesCount = 700;
		__this->___linesCount_22 = ((int32_t)700);
		// private float linesStrength = 0.1f;
		__this->___linesStrength_23 = (0.100000001f);
		// private float scanlineStrength = 0.5f;
		__this->___scanlineStrength_24 = (0.5f);
		// private float scanlineWidth = 0.2f;
		__this->___scanlineWidth_25 = (0.200000003f);
		// private float scanLineSpeed = 0.5f;
		__this->___scanLineSpeed_26 = (0.5f);
		// private float noiseBandStrength = 0.2f;
		__this->___noiseBandStrength_27 = (0.200000003f);
		// private float noiseBandWidth = 0.2f;
		__this->___noiseBandWidth_28 = (0.200000003f);
		// private float noiseBandSpeed = 0.2f;
		__this->___noiseBandSpeed_29 = (0.200000003f);
		// private readonly int variableTint = Shader.PropertyToID("_Tint");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralECA169E2C11BA4F71637D2E1BF1D8ECAA03E8BF6, NULL);
		__this->___variableTint_31 = L_1;
		// private readonly int variableLinesCount = Shader.PropertyToID("_LinesCount");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralA0F2ACB0B3F377939DE4177970C04C49B4898FE1, NULL);
		__this->___variableLinesCount_32 = L_2;
		// private readonly int variableLinesStrength = Shader.PropertyToID("_LinesStrength");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral8AB0A32AA70CEEEDF68589AA0B93CB1349239D55, NULL);
		__this->___variableLinesStrength_33 = L_3;
		// private readonly int variableScanLineStrength = Shader.PropertyToID("_ScanlineStrength");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral930831AD69587F365337F7BD5E2D296692E0D130, NULL);
		__this->___variableScanLineStrength_34 = L_4;
		// private readonly int variableScanLineWidth = Shader.PropertyToID("_ScanlineWidth");
		int32_t L_5;
		L_5 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral543BE502C8CE19D4D92897D3D3DDE32FB6563EBF, NULL);
		__this->___variableScanLineWidth_35 = L_5;
		// private readonly int variableScanLineSpeed = Shader.PropertyToID("_ScanlineSpeed");
		int32_t L_6;
		L_6 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralC78BA9E4E8D721124AD624C314C85968C659273C, NULL);
		__this->___variableScanLineSpeed_36 = L_6;
		// private readonly int variableNoiseBandStrength = Shader.PropertyToID("_NoiseBandStrength");
		int32_t L_7;
		L_7 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral7B91B5FB0B511B2B078924DF85478132B50E3667, NULL);
		__this->___variableNoiseBandStrength_37 = L_7;
		// private readonly int variableNoiseBandWidth = Shader.PropertyToID("_NoiseBandWidth");
		int32_t L_8;
		L_8 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteralA4A5866BFA572A345EAE030A102601C627F2188E, NULL);
		__this->___variableNoiseBandWidth_38 = L_8;
		// private readonly int variableNoiseBandSpeed = Shader.PropertyToID("_NoiseBandSpeed");
		int32_t L_9;
		L_9 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral14F4FFFE4680A2B7D9EB6BD6180E9097EACB7BDA, NULL);
		__this->___variableNoiseBandSpeed_39 = L_9;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.ScannerVision::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScannerVision__cctor_mA22B50F07A749249141A3AA9A6034FC688640B46 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly  Color DefaultTint = new Color(0.5f, 1.0f, 0.5f);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline((&L_0), (0.5f), (1.0f), (0.5f), /*hidden argument*/NULL);
		((ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_StaticFields*)il2cpp_codegen_static_fields_for(ScannerVision_t866B9B9B5426E13C8148D9A238FCFAA4921F8E4D_il2cpp_TypeInfo_var))->___DefaultTint_30 = L_0;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.ShakeVision::get_Magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ShakeVision_get_Magnitude_mC5F2F64E60EEF3DC757C48E52892D327319F1F17 (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, const RuntimeMethod* method) 
{
	{
		// get { return magnitude; }
		float L_0 = __this->___magnitude_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ShakeVision::set_Magnitude(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShakeVision_set_Magnitude_m5BC8E3D3DD30B9427439055BEE41DA3AAF65EAFF (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { magnitude = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___magnitude_21 = L_1;
		// set { magnitude = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.ShakeVision::get_Intensity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ShakeVision_get_Intensity_m9DE22868C08E9E37112C7AEFBDCE9E7B42F0E505 (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, const RuntimeMethod* method) 
{
	{
		// get { return intensity; }
		float L_0 = __this->___intensity_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.ShakeVision::set_Intensity(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShakeVision_set_Intensity_m8EFD7E03A511C4E3A5EADE01B342FC72EE151912 (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { intensity = Mathf.Max(0.0f, value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline((0.0f), L_0, NULL);
		__this->___intensity_22 = L_1;
		// set { intensity = Mathf.Max(0.0f, value); }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.ShakeVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ShakeVision_ToString_mC8D917949F9129F63C72236901BB8D9BD9438B4A (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3078FE4A752791FDA320A7C597D705164ECAB614);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "Shakes on the screen.";
		return _stringLiteral3078FE4A752791FDA320A7C597D705164ECAB614;
	}
}
// System.Void Nephasto.VisionFXAsset.ShakeVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShakeVision_ResetDefaultValues_mBB1E2CB30A1ACB942CD1BD7848F4B06E970F5A3E (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// magnitude = 0.1f;
		__this->___magnitude_21 = (0.100000001f);
		// intensity = 1.0f;
		__this->___intensity_22 = (1.0f);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.ShakeVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShakeVision_UpdateCustomValues_m349D9B220DC4CE42E4294794DC30D9CD8BF6FAB4 (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableMagnitude, magnitude);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableMagnitude_23;
		float L_2 = __this->___magnitude_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, L_2, NULL);
		// material.SetFloat(variableIntensity, intensity);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableIntensity_24;
		float L_5 = __this->___intensity_22;
		NullCheck(L_3);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_3, L_4, L_5, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.ShakeVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShakeVision__ctor_m11476BC8D9922AD957A0E115BCFD323D96D3791E (ShakeVision_t39DB12750DE1A5554907000F4F14C1807EE117C7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4E207A1E776F6188653FF9228A95BFD3A17B492E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6C5577D68A98EC6D7EE033290FEAA4FBFB520027);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float magnitude = 0.1f;
		__this->___magnitude_21 = (0.100000001f);
		// private float intensity = 1.0f;
		__this->___intensity_22 = (1.0f);
		// private readonly int variableMagnitude = Shader.PropertyToID("_Magnitude");
		int32_t L_0;
		L_0 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral6C5577D68A98EC6D7EE033290FEAA4FBFB520027, NULL);
		__this->___variableMagnitude_23 = L_0;
		// private readonly int variableIntensity = Shader.PropertyToID("_Intensity");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral4E207A1E776F6188653FF9228A95BFD3A17B492E, NULL);
		__this->___variableIntensity_24 = L_1;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Nephasto.VisionFXAsset.TrippyVision::get_Speed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TrippyVision_get_Speed_mDDA9986288A14B4B8C31FA7F307BCB85CDD7DD96 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	{
		// get { return speed; }
		float L_0 = __this->___speed_21;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.TrippyVision::set_Speed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrippyVision_set_Speed_m9AF9D58778160902659421450A25FE9A0021BFA6 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { speed = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___speed_21 = L_1;
		// set { speed = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.TrippyVision::get_Definition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TrippyVision_get_Definition_m6A362447827F8CE95E6CC173B75384E2945375F4 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	{
		// get { return definition; }
		float L_0 = __this->___definition_22;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.TrippyVision::set_Definition(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrippyVision_set_Definition_m4E40DE8EEEA6664B72D52AECEA94932760D91BBE (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { definition = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___definition_22 = L_1;
		// set { definition = Mathf.Max(value, 0.0f); }
		return;
	}
}
// System.Single Nephasto.VisionFXAsset.TrippyVision::get_Displacement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TrippyVision_get_Displacement_m23A13B5E3BD48ABCD1B8DD95011D17CA92B57800 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	{
		// get { return displacement; }
		float L_0 = __this->___displacement_23;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.TrippyVision::set_Displacement(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrippyVision_set_Displacement_m48C6DBAC391F70AB8852F47CB3E159EA1312AEFB (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { displacement = Mathf.Max(value, 0.0f); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline(L_0, (0.0f), NULL);
		__this->___displacement_23 = L_1;
		// set { displacement = Mathf.Max(value, 0.0f); }
		return;
	}
}
// Nephasto.VisionFXAsset.VisionBlendOps Nephasto.VisionFXAsset.TrippyVision::get_BlendOp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrippyVision_get_BlendOp_m04C4C0BE35096EA7C7F22B7EFEEE0A566242DE04 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	{
		// get { return blendOp; }
		int32_t L_0 = __this->___blendOp_24;
		return L_0;
	}
}
// System.Void Nephasto.VisionFXAsset.TrippyVision::set_BlendOp(Nephasto.VisionFXAsset.VisionBlendOps)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrippyVision_set_BlendOp_m74142BBF899E15422A0D4190D4724D346EF2A273 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// set { blendOp = value; }
		int32_t L_0 = ___value0;
		__this->___blendOp_24 = L_0;
		// set { blendOp = value; }
		return;
	}
}
// System.String Nephasto.VisionFXAsset.TrippyVision::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TrippyVision_ToString_m854B36DA0B7B01C5D2BD77FB1F508B9B8DBC4066 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral224074E0D1F5E22925D4812636CDA330DF2AD236);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() => "When the player abuses hallucinogenic substances.";
		return _stringLiteral224074E0D1F5E22925D4812636CDA330DF2AD236;
	}
}
// System.Void Nephasto.VisionFXAsset.TrippyVision::ResetDefaultValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrippyVision_ResetDefaultValues_m5D1698934B7568161537E570E3D093264776BFA5 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	{
		// base.ResetDefaultValues();
		BaseVision_ResetDefaultValues_mE60D1E5FCF80F0565488B4F4689A93BAEA08FB55(__this, NULL);
		// speed = 0.5f;
		__this->___speed_21 = (0.5f);
		// definition = 2.5f;
		__this->___definition_22 = (2.5f);
		// displacement = 0.2f;
		__this->___displacement_23 = (0.200000003f);
		// blendOp = VisionBlendOps.Lighten;
		__this->___blendOp_24 = ((int32_t)11);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.TrippyVision::UpdateCustomValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrippyVision_UpdateCustomValues_mDA2FF586E662851B3DEE77FE8D834AA10E0CDFC4 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	{
		// material.SetFloat(variableSpeed, speed);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_1 = __this->___variableSpeed_25;
		float L_2 = __this->___speed_21;
		NullCheck(L_0);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_0, L_1, L_2, NULL);
		// material.SetFloat(variableDefinition, definition);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_4 = __this->___variableDefinition_26;
		float L_5 = __this->___definition_22;
		NullCheck(L_3);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_3, L_4, L_5, NULL);
		// material.SetFloat(variableDisplacement, displacement * 0.1f);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_7 = __this->___variableDisplacement_27;
		float L_8 = __this->___displacement_23;
		NullCheck(L_6);
		Material_SetFloat_m3ECFD92072347A8620254F014865984FA68211A8(L_6, L_7, ((float)il2cpp_codegen_multiply(L_8, (0.100000001f))), NULL);
		// material.SetInt(variableBlendOp, (int)blendOp);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ((BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5*)__this)->___material_13;
		int32_t L_10 = __this->___variableBlendOp_28;
		int32_t L_11 = __this->___blendOp_24;
		NullCheck(L_9);
		Material_SetInt_m9C05C9D7C152CFCC48F8572710F89DDAC9277E39(L_9, L_10, L_11, NULL);
		// }
		return;
	}
}
// System.Void Nephasto.VisionFXAsset.TrippyVision::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrippyVision__ctor_mBF61D5ED0F8CACC066A08BDC52290091A8DAFCA3 (TrippyVision_tDE8B7F0E7F140315447D6B92D7F4920274A53562* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1E55BC8EC00B694817EDB2E59B23EC798FEE4C6B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral709E50200C27F9835679989FC5ADD0F1ADF14EC2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float speed = 0.5f;
		__this->___speed_21 = (0.5f);
		// private float definition = 2.5f;
		__this->___definition_22 = (2.5f);
		// private float displacement = 0.2f;
		__this->___displacement_23 = (0.200000003f);
		// private VisionBlendOps blendOp = VisionBlendOps.Lighten;
		__this->___blendOp_24 = ((int32_t)11);
		// private readonly int variableSpeed = Shader.PropertyToID("_Speed");
		int32_t L_0;
		L_0 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral39E3629B886CB412720ADA081113F5133F78CE75, NULL);
		__this->___variableSpeed_25 = L_0;
		// private readonly int variableDefinition = Shader.PropertyToID("_Definition");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral1E55BC8EC00B694817EDB2E59B23EC798FEE4C6B, NULL);
		__this->___variableDefinition_26 = L_1;
		// private readonly int variableDisplacement = Shader.PropertyToID("_Displacement");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral709E50200C27F9835679989FC5ADD0F1ADF14EC2, NULL);
		__this->___variableDisplacement_27 = L_2;
		// private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mF5F7BA2EFF23D83482ECDE4C34227145D817B1EB(_stringLiteral9EB3C32AD3B76FD126254C6B1979A0E5E8C5123F, NULL);
		__this->___variableBlendOp_28 = L_3;
		BaseVision__ctor_m387B01A553B36A77BE38B97D095FCD5CA72763C0(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Max_mA9DCA91E87D6D27034F56ABA52606A9090406016_inline (float ___a0, float ___b1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((((float)L_0) > ((float)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		float L_2 = ___b1;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		float L_3 = ___a0;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline (float ___value0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		float L_0 = ___value0;
		V_0 = (bool)((((float)L_0) < ((float)(0.0f)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_002d;
	}

IL_0015:
	{
		float L_2 = ___value0;
		V_2 = (bool)((((float)L_2) > ((float)(1.0f)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		V_1 = (1.0f);
		goto IL_002d;
	}

IL_0029:
	{
		float L_4 = ___value0;
		V_1 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		float L_5 = V_1;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		V_0 = (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		float L_3 = ___min1;
		___value0 = L_3;
		goto IL_0019;
	}

IL_000e:
	{
		float L_4 = ___value0;
		float L_5 = ___max2;
		V_1 = (bool)((((float)L_4) > ((float)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0019;
		}
	}
	{
		float L_7 = ___max2;
		___value0 = L_7;
	}

IL_0019:
	{
		float L_8 = ___value0;
		V_2 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		float L_9 = V_2;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		float L_2 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___a0;
		float L_4 = L_3.___y_1;
		float L_5 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_op_Implicit_m6162D8136CFE97A5A8BD3B764F9074DB96AA5CD0_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) 
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___v0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___v0;
		float L_3 = L_2.___y_1;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_4), L_1, L_3, (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___zeroVector_2;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float BaseVision_get_Amount_mA9FE6409D906BEAA982D5437F3AF0A32E4CE4137_inline (BaseVision_tB3ECAB88F516267EB4D03C07CEF55EC57A7AE8B5* __this, const RuntimeMethod* method) 
{
	{
		// get { return amount; }
		float L_0 = __this->___amount_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		float L_4;
		L_4 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_3, NULL);
		V_0 = ((float)il2cpp_codegen_add(L_0, ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_1, L_2)), L_4))));
		goto IL_0010;
	}

IL_0010:
	{
		float L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m232E885D3C7BB6A96D5FEF4494709BA170447604_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___oneVector_3;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___b1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___a0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___b1;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_cyan_m1257FED4776F2A33BD7250357D024B3FA3E592EB_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		__this->___a_3 = (1.0f);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!true)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_1 = L_0;
		float L_1 = ___y1;
		__this->___y_2 = L_1;
		float L_2 = ___z2;
		__this->___z_3 = L_2;
		float L_3 = ___w3;
		__this->___w_4 = L_3;
		return;
	}
}
