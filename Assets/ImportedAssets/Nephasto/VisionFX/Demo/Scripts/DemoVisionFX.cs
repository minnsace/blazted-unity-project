﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using UnityEngine;
using UnityEngine.UI;

using Nephasto.VisionFXAsset;

/// <summary>
/// UI for the demo.
/// </summary>
public class DemoVisionFX : MonoBehaviour
{
  [SerializeField]
  private bool guiShow = true;

  [SerializeField]
  private bool showEffectName = false;

  [SerializeField]
  private float startDelay = 2.0f;

  [SerializeField]
  private float slideEffectTime = 0.0f;

  [SerializeField]
  private float changeEffectTime = 0.25f;

  [SerializeField]
  private AudioClip musicClip = null;

  [SerializeField]
  private Text message = null;

  [SerializeField]
  private string credits = "";

  private float effectTime = 0.0f;

  private List<BaseVision> visions = new List<BaseVision>();

  private int currentEffect = -1;
  private bool showCustomProperties = false;

  private bool menuOpen = false;

  private const float guiMargen = 10.0f;
  private const float guiWidth = 300.0f;
  private const float horizontalWidth = 80.0f;
  private const string guiTab = "   ";

  private Vector2 scrollPosition = Vector2.zero;

  private float updateInterval = 0.5f;
  private float accum = 0.0f;
  private int frames = 0;
  private float timeleft;
  private float fps = 0.0f;

  private GUIStyle effectNameStyle;
  private GUIStyle menuStyle;
  private GUIStyle boxStyle;
  private GUIStyle creditsStyle;

  private void OnEnable()
  {
    timeleft = updateInterval;

    Camera selectedCamera = null;
    Camera[] cameras = GameObject.FindObjectsOfType<Camera>();

    for (int i = 0; i < cameras.Length; ++i)
    {
      if (cameras[i].enabled == true)
      {
        selectedCamera = cameras[i];

        break;
      }
    }

    if (selectedCamera != null)
    {
      BaseVision[] effects = selectedCamera.gameObject.GetComponents<BaseVision>();
      if (effects.Length > 0)
      {
        for (int i = 0; i < effects.Length; ++i)
        {
          if (effects[i].IsSupported() == true)
            visions.Add(effects[i]);
          else
            effects[i].enabled = false;
        }
      }
      else
      {
        Type[] types = Assembly.GetAssembly(typeof(BaseVision)).GetTypes();
        for (int i = 0; i < types.Length; ++i)
        {
          if (types[i].IsClass == true && types[i].IsAbstract == false && types[i].IsSubclassOf(typeof(BaseVision)) == true)
          {
            BaseVision effect = selectedCamera.gameObject.AddComponent(types[i]) as BaseVision;
            if (effect.IsSupported() == true)
              visions.Add(effect);
            else
              Destroy(effect);
          }
        }
      }

      //Shader.WarmupAllShaders();

      for (int i = 0; i < visions.Count; ++i)
      {
        visions[i].enabled = true;
        visions[i].Amount = 0.0f;
      }

      if (musicClip != null)
      {
        AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
        audioSource.clip = musicClip;
        audioSource.volume = 0.5f;
        audioSource.loop = (slideEffectTime > 0.0f);
        audioSource.PlayDelayed(startDelay);

        if (slideEffectTime == 0.0f)
          slideEffectTime = musicClip.length / visions.Count;
      }

      this.StartCoroutine(StarDelayCoroutine());
    }
    else
      Debug.LogWarning("No camera found.");
  }

  private void Update()
  {
    timeleft -= Time.deltaTime;
    accum += Time.timeScale / Time.deltaTime;
    frames++;

    if (timeleft <= 0.0f)
    {
      fps = accum / frames;
      timeleft = updateInterval;
      accum = 0.0f;
      frames = 0;
    }

    if (Input.GetKeyUp(KeyCode.Tab) == true)
      guiShow = !guiShow;

    if (currentEffect != -1)
    {
      if (slideEffectTime > 0.0f && visions.Count > 0)
      {
        effectTime += Time.deltaTime;

        if (effectTime >= slideEffectTime)
        {
          ChangeEffect((currentEffect < (visions.Count - 1) ? currentEffect + 1 : 0));

          effectTime = 0.0f;
        }
      }

      if (Input.GetKeyUp(KeyCode.KeypadPlus) == true ||
          Input.GetKeyUp(KeyCode.KeypadMinus) == true ||
          Input.GetKeyUp(KeyCode.PageUp) == true ||
          Input.GetKeyUp(KeyCode.PageDown) == true)
      {
        slideEffectTime = 0.0f;

        if (Input.GetKeyUp(KeyCode.KeypadPlus) == true || Input.GetKeyUp(KeyCode.PageDown) == true)
          ChangeEffect(currentEffect < visions.Count - 1 ? currentEffect + 1 : 0);

        if (Input.GetKeyUp(KeyCode.KeypadMinus) == true || Input.GetKeyUp(KeyCode.PageUp) == true)
          ChangeEffect(currentEffect > 0 ? currentEffect - 1 : visions.Count - 1);
      }      
    }
  }

  private void OnGUI()
  {
    if (visions.Count == 0)
      return;

    if (effectNameStyle == null)
    {
      effectNameStyle = new GUIStyle(GUI.skin.textArea);
      effectNameStyle.normal.background = MakeTex(2, 2, new Color(0.25f, 0.25f, 0.25f, 0.6f));
      effectNameStyle.alignment = TextAnchor.MiddleCenter;
      effectNameStyle.fontSize = 22;
    }

    if (boxStyle == null)
    {
      boxStyle = new GUIStyle(GUI.skin.box);
      boxStyle.normal.background = MakeTex(2, 2, new Color(0.3f, 0.3f, 0.3f, 0.6f));
    }

    if (menuStyle == null)
    {
      menuStyle = new GUIStyle(GUI.skin.textArea);
      menuStyle.normal.background = MakeTex(2, 2, new Color(0.3f, 0.3f, 0.3f, 0.6f));
      menuStyle.alignment = TextAnchor.MiddleCenter;
      menuStyle.fontSize = 22;
    }

    if (creditsStyle == null)
    {
      creditsStyle = new GUIStyle(GUI.skin.label);
      creditsStyle.wordWrap = true;
    }

    if (showEffectName == true && guiShow == false)
    {
      string effectName = EffectName(visions[currentEffect].GetType().ToString());

      GUILayout.Space(guiMargen);
      GUILayout.BeginHorizontal(GUILayout.Width(Screen.width));
      {
        GUILayout.FlexibleSpace();
        GUILayout.Label(effectName.ToUpper(), effectNameStyle);
        GUILayout.FlexibleSpace();
      }
      GUILayout.EndHorizontal();
    }

    GUILayout.BeginVertical(GUILayout.Height(Screen.height));
    {
      if (guiShow == true && currentEffect != -1)
      {
        GUILayout.BeginHorizontal(boxStyle, GUILayout.Width(Screen.width));
        {
          GUILayout.Space(guiMargen);

          if (GUILayout.Button("MENU", menuStyle, GUILayout.Width(80.0f)) == true)
            menuOpen = !menuOpen;

          GUILayout.FlexibleSpace();

          if (GUILayout.Button("<<<", menuStyle) == true)
          {
            showCustomProperties = false;

            slideEffectTime = 0.0f;

            ChangeEffect(currentEffect > 0 ? currentEffect - 1 : visions.Count - 1);

            Event.current.Use();
          }

          GUI.contentColor = Color.white;

          string effectName = EffectName(visions[currentEffect].GetType().ToString());

          GUILayout.Label(effectName.ToUpper(), menuStyle, GUILayout.Width(325.0f));

          if (GUILayout.Button(">>>", menuStyle) == true)
          {
            showCustomProperties = false;

            slideEffectTime = 0.0f;

            ChangeEffect(currentEffect < visions.Count - 1 ? currentEffect + 1 : 0);
          }

          GUILayout.FlexibleSpace();

          if (musicClip != null && GUILayout.Button("MUTE", menuStyle) == true)
            AudioListener.volume = 1.0f - AudioListener.volume;

          if (fps < 24.0f)
            GUI.contentColor = Color.yellow;
          else if (fps < 15.0f)
            GUI.contentColor = Color.red;
          else
            GUI.contentColor = Color.green;

          GUILayout.Label(fps.ToString("000"), menuStyle, GUILayout.Width(50.0f));

          GUI.contentColor = Color.white;

          GUILayout.Space(guiMargen);
        }
        GUILayout.EndHorizontal();

        if (menuOpen == true)
        {
          GUILayout.BeginVertical(boxStyle, GUILayout.ExpandHeight(true), GUILayout.Width(guiWidth));
          {
            GUILayout.Space(guiMargen);

            if (visions.Count > 0)
            {
              scrollPosition = GUILayout.BeginScrollView(scrollPosition, "box");
              {
                for (int i = 0; i < visions.Count; ++i)
                {
                  BaseVision visionEffect = visions[i];

                  if (currentEffect == i)
                    GUILayout.BeginVertical("box");
                  else
                    GUILayout.BeginVertical();
                  {
                    GUILayout.BeginHorizontal();
                    {
                      bool enableChanged = GUILayout.Toggle(visionEffect.enabled, guiTab + EffectName(visionEffect.GetType().ToString()));
                      if (enableChanged != visionEffect.enabled)
                      {
                        showCustomProperties = false;

                        slideEffectTime = 0.0f;

                        ChangeEffect(i);
                      }

                      GUILayout.FlexibleSpace();

                      if (currentEffect == i && GUILayout.Button(showCustomProperties == true ? "-" : "+", GUILayout.Width(30.0f)) == true)
                      {
                        slideEffectTime = 0.0f;

                        showCustomProperties = !showCustomProperties;
                      }
                    }
                    GUILayout.EndHorizontal();

                    if (currentEffect == i && showCustomProperties == true)
                      DrawCustomAttributes(visionEffect);
                  }
                  GUILayout.EndVertical();

                  GUILayout.Space(guiMargen * 0.5f);
                }
              }
              GUILayout.EndScrollView();
            }
            else
              GUILayout.Label("No 'Vision FX' effects found.");

            GUILayout.Space(guiMargen);

            GUILayout.BeginVertical("box");
            {
              GUILayout.Label("TAB - Hide/Show gui.");
              GUILayout.Label("PageUp/Down - Change effects.");

              if (string.IsNullOrEmpty(credits) == false)
              {
                GUILayout.Space(guiMargen);

                GUILayout.Label(credits.Replace("|", "\n"), creditsStyle);
              }
            }
            GUILayout.EndVertical();

            GUILayout.Space(guiMargen);

            if (GUILayout.Button("Open Web") == true)
              Application.OpenURL(BaseVision.Documentation);

            GUILayout.FlexibleSpace();
          }
          GUILayout.EndVertical();
        }
      }
    }
    GUILayout.EndVertical();
  }

  private void DrawCustomAttributes(BaseVision visionEffect)
  {
    if (visionEffect is AnimeVision)
    {
      AnimeVision effect = (AnimeVision)visionEffect;

      effect.Radius = HorizontalSlider("Radius", effect.Radius, 0.0f, 2.0f);
      effect.Length = HorizontalSlider("Length", effect.Length, 0.0f, 5.0f);
      effect.Speed = HorizontalSlider("Speed", effect.Speed, 0.0f, 2.0f);
      effect.Frequency = HorizontalSlider("Frequency", effect.Frequency, 0.0f, 50.0f);
      effect.Softness = HorizontalSlider("Softness", effect.Softness, 0.0f, 1.0f);
      effect.Noise = HorizontalSlider("Noise", effect.Noise, 0.0f, 1.0f);
    }
    else if (visionEffect is BlurryVision)
    {
      BlurryVision effect = (BlurryVision)visionEffect;

      effect.Frames = HorizontalSlider("Frames", effect.Frames, 0, 10);
    }
    else if (visionEffect is DamageVision)
    {
      DamageVision effect = (DamageVision)visionEffect;

      effect.Damage = HorizontalSlider("Damage", effect.Damage, 0.0f, 1.0f);
      effect.Definition = HorizontalSlider("Definition", effect.Definition, 0.0f, 1.0f);
      effect.DamageBrightness = HorizontalSlider("Brightness", effect.DamageBrightness, 0.0f, 10.0f);
      effect.Distortion = HorizontalSlider("Distortion", effect.Distortion, 0.0f, 1.0f);
      effect.Color = ColorSlider("Color", effect.Color);
    }
    else if (visionEffect is DoubleVision)
    {
      DoubleVision effect = (DoubleVision)visionEffect;

      effect.Strength = HorizontalSlider("Strength", effect.Strength, 0.0f, 1.0f);
      effect.Speed = HorizontalSlider("Speed", effect.Speed, 0.0f, 2.0f);
    }
    else if (visionEffect is DrunkVision)
    {
      DrunkVision effect = (DrunkVision)visionEffect;

      effect.Drunkenness = HorizontalSlider("Drunkenness", effect.Drunkenness, 0.0f, 1.0f);
      effect.DrunkSpeed = HorizontalSlider("Speed", effect.DrunkSpeed, 0.0f, 10.0f);
      effect.DrunkAmplitude = HorizontalSlider("Amplitude", effect.DrunkAmplitude, 0.0f, 1.0f);
      effect.Swinging = HorizontalSlider("Swinging", effect.Swinging, 0.0f, 1.0f);
      effect.Aberration = HorizontalSlider("Aberration", effect.Aberration, 0.0f, 10.0f);
      effect.VignetteAmount = HorizontalSlider("Blink strength", effect.VignetteAmount, 0.0f, 10.0f);
      effect.VignetteSpeed = HorizontalSlider("Blink speed", effect.VignetteSpeed, 0.0f, 10.0f);
    }
    else if (visionEffect is FisheyeVision)
    {
      FisheyeVision effect = (FisheyeVision)visionEffect;

      effect.Barrel = HorizontalSlider("Barrel", effect.Barrel, -1.0f, 1.0f);
    }
    else if (visionEffect is GhostVision)
    {
      GhostVision effect = (GhostVision)visionEffect;

      effect.Aperture = HorizontalSlider("Aperture", effect.Aperture, 0.0f, 10.0f);
      effect.Zoom = HorizontalSlider("Zoom", effect.Zoom, 0.0f, 2.0f);
      effect.Speed = HorizontalSlider("Speed", effect.Speed, 0.0f, 2.0f);
    }
    else if (visionEffect is HalftoneVision)
    {
      HalftoneVision effect = (HalftoneVision)visionEffect;

      effect.Size = HorizontalSlider("Size", effect.Size, 0.0f, 5.0f);
      effect.Angle = HorizontalSlider("Angle", effect.Angle, 0.0f, 180.0f);
      effect.Strength = HorizontalSlider("Strength", effect.Strength, 0.0f, 50.0f);
      effect.Sensitivity = HorizontalSlider("Sensitivity", effect.Sensitivity, 0.0f, 10.0f);
    }
    else if (visionEffect is LegoVision)
    {
      LegoVision effect = (LegoVision)visionEffect;

      effect.Size = HorizontalSlider("Size", effect.Size, 0.0f, 1.0f);
    }
    else if (visionEffect is NeonVision)
    {
      NeonVision effect = (NeonVision)visionEffect;

      effect.Edge = HorizontalSlider("Edge", effect.Edge, 0.0f, 4.0f);
      effect.EdgeColor = ColorSlider("Color", effect.EdgeColor);
    }
    else if (visionEffect is ScannerVision)
    {
      ScannerVision effect = (ScannerVision)visionEffect;

      effect.Tint = ColorSlider("Tint", effect.Tint);
      effect.LinesCount = HorizontalSlider("Lines count", effect.LinesCount, 0, 1000);
      effect.LinesStrength = HorizontalSlider("Lines strength", effect.LinesStrength, 0.0f, 1.0f);
      effect.ScanLineStrength = HorizontalSlider("Scanline strength", effect.ScanLineStrength, 0.0f, 1.0f);
      effect.ScanLineWidth = HorizontalSlider("Scanline width", effect.ScanLineWidth, 0.0f, 5.0f);
      effect.ScanLineSpeed = HorizontalSlider("Scanline speed", effect.ScanLineSpeed, -5.0f, 5.0f);
      effect.NoiseBandStrength = HorizontalSlider("Noise strength", effect.NoiseBandStrength, 0.0f, 1.0f);
      effect.NoiseBandWidth = HorizontalSlider("Noise width", effect.NoiseBandWidth, 0.0f, 5.0f);
      effect.NoiseBandSpeed = HorizontalSlider("Noise speed", effect.NoiseBandSpeed, -5.0f, 5.0f);
    }
    else if (visionEffect is ShakeVision)
    {
      ShakeVision effect = (ShakeVision)visionEffect;

      effect.Magnitude = HorizontalSlider("Magnitude", effect.Magnitude, 0.0f, 2.0f);
      effect.Intensity = HorizontalSlider("Intensity", effect.Intensity, 0.0f, 10.0f);
    }
    else if (visionEffect is TrippyVision)
    {
      TrippyVision effect = (TrippyVision)visionEffect;

      effect.Speed = HorizontalSlider("Speed", effect.Speed, 0.0f, 10.0f);
      effect.Definition = HorizontalSlider("Definition", effect.Definition, 0.0f, 20.0f);
      effect.Displacement = HorizontalSlider("Displacement", effect.Displacement, 0.0f, 2.0f);
    }

    GUILayout.BeginHorizontal();
    {
      GUILayout.Label("", GUILayout.Width(horizontalWidth));

      if (GUILayout.Button("Reset") == true)
        visionEffect.ResetDefaultValues();
    }
    GUILayout.EndHorizontal();
  }

  private int HorizontalSlider(string label, int value, int min, int max)
  {
    GUILayout.BeginHorizontal();
    {
      GUILayout.Label(label, GUILayout.Width(horizontalWidth));

      value = (int)GUILayout.HorizontalSlider(value, min, max);
    }
    GUILayout.EndHorizontal();

    return value;
  }

  private float HorizontalSlider(string label, float value, float min, float max)
  {
    GUILayout.BeginHorizontal();
    {
      GUILayout.Label(label, GUILayout.Width(horizontalWidth));

      value = GUILayout.HorizontalSlider(value, min, max);
    }
    GUILayout.EndHorizontal();

    return value;
  }

  private Vector2 HorizontalSlider(string label, Vector2 value, float min, float max)
  {
    float x = value.x, y = value.y;

    GUILayout.BeginHorizontal();
    {
      GUILayout.Label(label, GUILayout.Width(horizontalWidth));

      x = GUILayout.HorizontalSlider(value.x, min, max);
    }
    GUILayout.EndHorizontal();

    GUILayout.BeginHorizontal();
    {
      GUILayout.Label("", GUILayout.Width(horizontalWidth));

      y = GUILayout.HorizontalSlider(value.y, min, max);
    }
    GUILayout.EndHorizontal();

    return new Vector2(x, y);
  }

  private Color ColorSlider(string label, Color value)
  {
    GUILayout.BeginHorizontal();
    {
      GUILayout.Label(label, GUILayout.Width(horizontalWidth));

      float h, s, v;
      Color.RGBToHSV(value, out h, out s, out v);

      h = GUILayout.HorizontalSlider(h, 0.0f, 1.0f);

      value = Color.HSVToRGB(h, s, v);
    }
    GUILayout.EndHorizontal();

    return value;
  }

  private Texture2D MakeTex(int width, int height, Color col)
  {
    Color[] pix = new Color[width * height];
    for (int i = 0; i < pix.Length; ++i)
      pix[i] = col;

    Texture2D result = new Texture2D(width, height);
    result.SetPixels(pix);
    result.Apply();

    return result;
  }

  private void ChangeEffect(int effectIndex)
  {
    this.StartCoroutine(ChangeEffectCoroutine(effectIndex));
  }

  private IEnumerator StarDelayCoroutine()
  {
    yield return new WaitForSeconds(startDelay);

    if (message != null)
    {
      float time = changeEffectTime;
      while (time > 0.0f)
      {
        message.color = Color.white * (time / changeEffectTime);

        time -= Time.deltaTime;

        yield return null;
      }

      message.enabled = false;
    }

    ChangeEffect(0);
  }

  private IEnumerator ChangeEffectCoroutine(int effectIndex)
  {
    int oldEffect = currentEffect;
    currentEffect = effectIndex;

    if (oldEffect != -1)
    {
      visions[oldEffect].enabled = true;
      visions[oldEffect].Amount = 1.0f;
    }

    visions[currentEffect].enabled = true;
    visions[currentEffect].Amount = 0.0f;

    float time = changeEffectTime;
    while (time > 0.0f)
    {
      if (oldEffect != -1)
        visions[oldEffect].Amount = time / changeEffectTime;

      visions[currentEffect].Amount = 1.0f - (time / changeEffectTime);

      time -= Time.deltaTime;

      yield return null;
    }

    if (oldEffect != -1)
    {
      visions[oldEffect].enabled = false;
      visions[oldEffect].Amount = 0.0f;
    }
    else
      guiShow = true;

    visions[currentEffect].Amount = 1.0f;
  }

  private string EffectName(string name)
  {
    name = name.Replace("Nephasto.VisionFXAsset.", string.Empty);
    name = System.Text.RegularExpressions.Regex.Replace(name, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");

    return name;
  }
}
