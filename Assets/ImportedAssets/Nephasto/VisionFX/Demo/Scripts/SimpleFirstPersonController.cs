﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// (Very) Simple fps controller.
/// </summary>
[RequireComponent(typeof(CharacterController))]
public class SimpleFirstPersonController : MonoBehaviour
{
  [SerializeField]
  private Camera eye = null;
  
  [SerializeField]
  private string horizontalInputName = "Horizontal";
  
  [SerializeField]
  private string verticalInputName = "Vertical";
  
  [SerializeField]
  private float walkSpeed = 1.0f;

  [SerializeField]
  private float rotationSpeed = 75.0f;
  
  [SerializeField]
  private float runBuildUpSpeed = 2.0f;

  [SerializeField]
  private float slopeForce = 1.0f;

  [SerializeField]
  private float walkingBobbingSpeed = 10.0f;

  [SerializeField]
  private float bobbingAmount = 0.025f;

  [SerializeField]
  private AudioClip[] footStepClips = null;

  public event StartMoving OnStartMoving;

  public delegate void StartMoving();

  private bool startMoving = false;

  private bool isMoving = false;
  
  private float movementSpeed = 0.0f;
  
  private const float stepDelay = 0.75f;

  private float defaultPosY = 0.0f;
  private float timer = 0.0f;

  private CharacterController characterController;

  private AudioSource audioSource;

  private IEnumerator Footsteps()
  {
    if (isMoving == true)
    {
      audioSource.clip = footStepClips[UnityEngine.Random.Range(0, footStepClips.Length)];
      audioSource.pitch = UnityEngine.Random.Range(0.8f, 1.2f);
      audioSource.Play();
    }
    
    yield return new WaitForSeconds(stepDelay);

    StartCoroutine(Footsteps());
  }
  
  private void Awake()
  {
    characterController = GetComponent<CharacterController>();

    if (eye != null)
    {
      eye.transform.parent = this.transform;
      eye.transform.localPosition = this.transform.up * characterController.height * 0.4f;

      defaultPosY = eye.transform.localPosition.y;
    }

    if (footStepClips.Length > 0)
    {
      audioSource = this.transform.gameObject.AddComponent<AudioSource>();
      audioSource.volume = 0.1f;
      
      this.StartCoroutine(Footsteps());
    }

    this.enabled = string.IsNullOrEmpty(horizontalInputName) == false && string.IsNullOrEmpty(verticalInputName) == false && eye != null;
  }
  
  private void Update()
  {
    float horizontalInput = Input.GetAxis(horizontalInputName);
    float verticalInput = Input.GetAxis(verticalInputName);

    isMoving = Math.Abs(verticalInput) > float.Epsilon;

    if (isMoving == true && startMoving == false)
    {
      startMoving = true;

      OnStartMoving?.Invoke();
    }

    characterController.SimpleMove(Vector3.ClampMagnitude(transform.forward * verticalInput, 1.0f) * movementSpeed);

    if ((Math.Abs(verticalInput) > float.Epsilon || Math.Abs(horizontalInput) > float.Epsilon) && characterController.isGrounded == true)
      characterController.Move(Vector3.down * characterController.height * 0.5f * slopeForce * Time.deltaTime);

    this.transform.Rotate(new Vector3(0.0f, horizontalInput * rotationSpeed * Time.deltaTime, 0.0f));

    movementSpeed = Mathf.Lerp(movementSpeed, walkSpeed, Time.deltaTime * runBuildUpSpeed);

    if (isMoving == true)
    {
      timer += Time.deltaTime * walkingBobbingSpeed;
      eye.transform.localPosition = new Vector3(eye.transform.localPosition.x, defaultPosY + Mathf.Sin(timer) * bobbingAmount, eye.transform.localPosition.z);
    }
    else
    {
      timer = 0.0f;
      eye.transform.localPosition = new Vector3(eye.transform.localPosition.x, Mathf.Lerp(eye.transform.localPosition.y, defaultPosY, Time.deltaTime * walkingBobbingSpeed), eye.transform.localPosition.z);
    }
  }
}