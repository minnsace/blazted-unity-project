///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Neon Vision.
/// </summary>
Shader "Hidden/VisionFX/NeonVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float _Edge;
  float3 _EdgeColor;
  int _BlendOp;

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    float offset = _Edge * _MainTex_TexelSize.x;

    float gx = 0.0;
    gx += -1.0 * Luminance(SAMPLE_MAINTEX(uv + float2(-offset, -offset)));
    gx += -2.0 * Luminance(SAMPLE_MAINTEX(uv + float2(-offset,  0.0)));
    gx += -1.0 * Luminance(SAMPLE_MAINTEX(uv + float2(-offset,  offset)));
    gx +=  1.0 * Luminance(SAMPLE_MAINTEX(uv + float2( offset, -offset)));
    gx +=  2.0 * Luminance(SAMPLE_MAINTEX(uv + float2( offset,  0.0)));
    gx +=  1.0 * Luminance(SAMPLE_MAINTEX(uv + float2( offset,  offset)));

    offset = _Edge * _MainTex_TexelSize.y;

    float gy = 0.0;
    gy += -1.0 * Luminance(SAMPLE_MAINTEX(uv + float2(-offset,   -offset)));
    gy += -2.0 * Luminance(SAMPLE_MAINTEX(uv + float2( 0.0,      -offset)));
    gy += -1.0 * Luminance(SAMPLE_MAINTEX(uv + float2( offset,   -offset)));
    gy +=  1.0 * Luminance(SAMPLE_MAINTEX(uv + float2(-offset,    offset)));
    gy +=  2.0 * Luminance(SAMPLE_MAINTEX(uv + float2( 0.0,       offset)));
    gy +=  1.0 * Luminance(SAMPLE_MAINTEX(uv + float2( offset,    offset)));

    float edge = gx * gx + gy * gy;

    return BlendOp(_BlendOp, pixel, _EdgeColor * edge);
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
