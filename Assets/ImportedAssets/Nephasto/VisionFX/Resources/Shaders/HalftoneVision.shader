///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Halftone Vision.
/// </summary>
Shader "Hidden/VisionFX/HalftoneVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float _Size;
  float _Angle;
  float _Strength;
  float _Sensitivity;
  int _BlendOp;

  inline float2 Rotate2D(float2 uv, float angle)
  {
    uv -= (float2)0.5;
    uv = mul(uv, float2x2(cos(angle), -sin(angle),
                          sin(angle),  cos(angle)));
    uv += (float2)0.5;

    return uv;
  }

  inline float Circle(float2 uv, float radius)
  {
    float2 dist = uv - (float2)(0.5);
    
    return 1.0 - smoothstep(radius - (radius * 0.1), radius + (radius * 0.1), dot(dist, dist) * 4.0);
  }

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    float2 squareUV = Rotate2D(float2(uv.x, uv.y * (_ScreenParams.y / _ScreenParams.x)), _PIHALF * _Angle);

    float3 halftone = (float3)Circle(frac(squareUV * _Size), _Strength * pow(Luminance(pixel), _Sensitivity));

    return BlendOp(_BlendOp, pixel, halftone);
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
