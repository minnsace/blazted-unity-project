///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Shake Vision.
/// </summary>
Shader "Hidden/VisionFX/ShakeVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float _Magnitude;
  float _Intensity;

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    float offset = 0.1 * _Magnitude;
    float rotation = _Intensity * 2.0 * _PI * Rand(float2(_Magnitude, _Time.y));
    float yOffset = offset * sin(_Time.y * cos(_Time.y * _Intensity) + rotation) * Rand(float2(_Magnitude, _Time.y));
    float xOffset = offset * cos(_Time.y * cos(_Time.y * _Intensity) + rotation) * Rand(float2(1.0 - _Magnitude, _Time.y));

    float zoom = 1.0 + offset;

    uv = (uv - 0.5) / zoom + 0.5;
    uv.x += xOffset;
    uv.y += yOffset;

    return SAMPLE_MAINTEX(uv);
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      //#pragma multi_compile ___ DEMO
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
