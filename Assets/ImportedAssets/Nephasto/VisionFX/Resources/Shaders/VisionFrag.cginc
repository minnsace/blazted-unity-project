///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef VISION_FRAG_CGINC
#define VISION_FRAG_CGINC

inline float4 FragVision(v2f_img i) : SV_Target
{
  UNITY_SETUP_INSTANCE_ID(i);

  float3 pixel = SAMPLE_MAINTEX(i.uv);
  float3 final = pixel;

  final = EffectVision(pixel, i.uv);

#ifdef COLOR_CONTROLS
  final = ColorAdjust(final);
#endif

  final = lerp(pixel, final, _Amount);

#if DEMO
  final = PixelDemo(pixel, final, i.uv);
#endif

  return float4(final, 1.0);
}

#endif // VISION_FRAG_CGINC