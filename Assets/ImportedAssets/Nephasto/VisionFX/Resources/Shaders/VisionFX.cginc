///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef VISIONFX_CGINC
#define VISIONFX_CGINC

#define _PI  3.141592653589
#define _PI2 6.283185307178
#define _PIHALF 1.57079632679

#include "Color.cginc"

float _Amount;

UNITY_DECLARE_SCREENSPACE_TEXTURE(_MainTex);
float4 _MainTex_ST;
float4 _MainTex_TexelSize;
float4 _MaskTex_TexelSize;

#ifdef UNITY_SINGLE_PASS_STEREO
  #define SAMPLE_TEX(tex, uv)           tex2D(tex, UnityStereoScreenSpaceUVAdjust(uv, _MainTex_ST)).rgb
  #define SAMPLE_MAINTEX(uv)            tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(uv, _MainTex_ST)).rgb
#else
  #define SAMPLE_TEX(tex, uv)           UNITY_SAMPLE_SCREENSPACE_TEXTURE(tex, uv).rgb
  #define SAMPLE_MAINTEX(uv)            UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, uv).rgb
#endif

#if SHADER_API_PS4
inline float2 lerp(float2 a, float2 b, float t)
{
  return lerp(a, b, (float2)t);
}

inline float3 lerp(float3 a, float3 b, float t)
{
  return lerp(a, b, (float3)t);
}

inline float4 lerp(float4 a, float4 b, float t)
{
  return lerp(a, b, (float4)t);
}
#endif

inline float mod(float x, float y)
{
  return x - y * floor(x / y);
}

inline float2 mod(float2 a, float2 b)
{
  return a - floor(a / b) * b;
}

inline float Rand(float c)
{
  return frac(sin(dot(float2(c, 1.0 - c), float2(12.9898, 78.233))) * 43758.5453);
}

inline float Rand(float2 c)
{
  return frac(sin(dot(c, float2(12.9898, 78.233))) * 43758.5453);
}

inline float2 Rand2(float2 c)
{
  float j = 4906.0 * sin(dot(c, float2(169.7, 5.8)));
  float2 r;
  r.x = frac(512.0 * j);
  j *= 0.125;
  
  r.y = frac(512.0 * j);
  
  return r - 0.5;
}

/// Noise by Ian McEwan, Ashima Arts.
float3 mod289(float3 x)  { return x - floor(x * (1.0 / 289.0)) * 289.0; }
float2 mod289(float2 x)  { return x - floor(x * (1.0 / 289.0)) * 289.0; }
float3 permute(float3 x) { return mod289(((x * 34.0) + 1.0) * x); }
float snoise(float2 v)
{
  const float4 C = float4(0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439);
  
  float2 i  = floor(v + dot(v, C.yy) );
  float2 x0 = v -   i + dot(i, C.xx);
  
  float2 i1;
  i1 = (x0.x > x0.y) ? float2(1.0, 0.0) : float2(0.0, 1.0);
  
  float4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;
  
  i = mod289(i);
  float3 p = permute(permute(i.y + float3(0.0, i1.y, 1.0)) + i.x + float3(0.0, i1.x, 1.0));
  
  float3 m = max(0.5 - float3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m;
  m = m*m;
  
  float3 x = 2.0 * frac(p * C.www) - 1.0;
  float3 h = abs(x) - 0.5;
  float3 ox = floor(x + 0.5);
  float3 a0 = x - ox;
  
  m *= 1.79284291400159 - 0.85373472095314 * (a0 * a0 + h * h);
  
  float3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;

  return 130.0 * dot(m, g);
}

// Do not use.
inline float3 PixelDemo(float3 pixel, float3 final, float2 uv)
{
  float separator = (sin(_Time.x * 15.0) * 0.25) + 0.7;
  const float separatorWidth = _MainTex_TexelSize.x * 4.0;

  if (uv.x > separator)
    final = pixel;
  else if (abs(uv.x - separator) < separatorWidth)
    final = pixel * float3(1.0, 0.0, 0.0);

  return final;
}
/*
struct appdata_t
{
  float4 vertex : POSITION;
  float2 texcoord : TEXCOORD0;
  UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct v2f
{
  float4 vertex : SV_POSITION;
  float2 texcoord : TEXCOORD0;
  UNITY_VERTEX_INPUT_INSTANCE_ID
  UNITY_VERTEX_OUTPUT_STEREO    
};

v2f VertVision(appdata_t v)
{
  v2f o;
  UNITY_SETUP_INSTANCE_ID(v);
  UNITY_TRANSFER_INSTANCE_ID(v, o);
  UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
  o.vertex = UnityObjectToClipPos(v.vertex);

#if UNITY_UV_STARTS_AT_TOP
  if (_MainTex_TexelSize.y < 0)
    o.texcoord.y = 1.0 - o.texcoord.y;
#endif

#if UNITY_HALF_TEXEL_OFFSET
  o.texcoord.xy += _MaskTex_TexelSize.xy * float2(-0.5, 0.5);
#endif

  o.texcoord = v.texcoord;

  return o;
}
*/
#endif // VISIONFX_CGINC