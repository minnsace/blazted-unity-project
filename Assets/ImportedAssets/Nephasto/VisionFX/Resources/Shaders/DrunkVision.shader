///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Drunk Vision.
/// </summary>
Shader "Hidden/VisionFX/DrunkVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float _Drunkenness;
  float _DrunkSpeed;
  float _DrunkAmplitude;
  
  float _Swinging;
  float _SwingingSpeed;

  float _Aberration;
  float _AberrationSpeed;

  float _VignetteAmount;
  float _VignetteSpeed;

  inline float2x2 Rotate(float angle)
  {
    return float2x2(cos(angle), -sin(angle), sin(angle), cos(angle));
  }  

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    float speed = _Time.y * _DrunkSpeed;

    float2 center = float2(sin(speed * 1.25 + 75.0  + uv.y * 0.5) +  cos(speed * 2.75 - 18.0 - uv.x * 0.25),
                           sin(speed * 1.75 - 125.0 + uv.x * 0.25) + cos(speed * 2.25 + 4.0  - uv.y * 0.5)) * _DrunkAmplitude + 0.5;

    uv = mul(Rotate(sin(_Time.y * _SwingingSpeed) * _PI * _Swinging), uv);

    float aberrationSpeed = _Time.y * _AberrationSpeed;
    float2 aberration = float2(sin(aberrationSpeed), cos(aberrationSpeed)) * _MainTex_TexelSize * _Aberration;

    float2 uv2 = (uv - center) * _Drunkenness + center;
    pixel.r = SAMPLE_MAINTEX(float2(uv2.x + aberration.x, uv2.y + aberration.y)).r;
    pixel.g = SAMPLE_MAINTEX(float2(uv2.x - aberration.y, uv2.y + aberration.x)).g;
    pixel.b = SAMPLE_MAINTEX(float2(uv2.x - aberration.x, uv2.y - aberration.y)).b;

    float vignette = 1.0 - distance(uv, (float2)0.5);

    speed = _Time.y * _VignetteSpeed;

    return lerp(pixel, pixel * vignette, sin((speed + 80.0) * 2.0) * _VignetteAmount);
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
