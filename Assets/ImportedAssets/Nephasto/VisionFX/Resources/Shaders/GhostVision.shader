///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Ghost Vision.
/// </summary>
Shader "Hidden/VisionFX/GhostVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float _Speed;
  float2 _Focus;
  float _Aperture;
  float _Zoom;

  float3 _InnerColor;
  float _InnerSaturation;
  float _InnerBrightness;
  float _InnerContrast;
  float _InnerGamma;
  
  float3 _OuterColor;
  float _OuterSaturation;
  float _OuterBrightness;
  float _OuterContrast;
  float _OuterGamma;

  inline float2x2 Rotate2D(float angle)
  {
    return float2x2(cos(angle), -sin(angle), sin(angle), cos(angle));
  }

  inline float RandR(float a, float b)
  {
    return frac(sin(dot(float2(a, b), float2(12.9898, 78.233))) * 43758.5453);
  }

  inline float RandH(float a)
  {
    return frac(sin(dot(a, dot(12.9898, 78.233))) * 43758.5453);
  }
  
  inline float Noise3(float3 x)
  {
    float3 p = floor(x);
    float3 f = frac(x);
    
    f = f * f * (3.0 - 2.0 * f);

    float n = p.x + p.y * 57.0 + 113.0 * p.z;

    return lerp(lerp(lerp(RandH(n +   0.0), RandH(n +   1.0), f.x),
                     lerp(RandH(n +  57.0), RandH(n +  58.0), f.x), f.y),
                lerp(lerp(RandH(n + 113.0), RandH(n + 114.0), f.x),
                     lerp(RandH(n + 170.0), RandH(n + 171.0), f.x), f.y), f.z);
  }
  
  // http://www.iquilezles.org/www/articles/morenoise/morenoise.htm
  inline float3 DNoise2f(float2 p)
  {
    float i = floor(p.x), j = floor(p.y);
    float u = p.x - i, v = p.y - j;
    float du = 30.0 * u * u * (u * (u - 2.0) + 1.0);
    float dv = 30.0 * v * v * (v * (v - 2.0) + 1.0);
  
    u = u * u * u * (u * (u * 6.0 - 15.0) + 10.0);
    v = v * v * v * (v * (v * 6.0 - 15.0) + 10.0);
  
    float a = RandR(i,       j);
    float b = RandR(i + 1.0, j);
    float c = RandR(i      , j + 1.0);
    float d = RandR(i + 1.0, j + 1.0);
    
    float k0 = a;
    float k1 = b - a;
    float k2 = c - a;
    float k3 = a - b - c + d;
  
    return float3(k0 + k1 * u + k2 * v + k3 * u * v, du * (k1 + k3 * v), dv * (k2 + k3 * u));
  }
  
  inline float fbm(float2 uv)
  {               
    float2 p = uv;
	float f, dx, dz, w = 0.5;
    
    f = dx = dz = 0.0;
    for (int i = 0; i < 28; ++i)
    {
      float3 n = DNoise2f(uv);
      dx += n.y;
      dz += n.z;
      f += w * n.x / (1.0 + dx * dx + dz * dz);
      w *= 0.9;
      
      uv *= (float2)1.16;
      uv = mul(uv, Rotate2D(1.25 * Noise3(float3(p.x * 0.1, 0.24 * _Time.y * _Speed, 0.0) + 0.75 * Noise3(float3(p.y * 0.1, 0.4 * _Time.y * _Speed, 0.0)))));
    }

    return f;
  }

  inline float fbmLow(float2 uv)
  {
    float f, dx, dz, w = 0.5;
    f = dx = dz = 0.0;
    
    [unroll]
    for (int i = 0; i < 4; ++i)
    {        
      float3 n = DNoise2f(uv);
      dx += n.y;
      dz += n.z;
      f += w * n.x / (1.0 + dx * dx + dz * dz);
      w *= 0.75;
      
      uv *= (float2)1.5;
    }
    
    return f;
  }

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    float time = _Time.y * _Speed;
    
    float2 uv2 = _Focus - (2.0 * uv);

#if ASPECT_RATIO
    uv2.y /= _ScreenParams.x / _ScreenParams.y;
#endif
    uv2 /= _Zoom;
    
    float2 rv = pow(sqrt(dot(uv2, uv2)), _Aperture);
    
    uv2 = mul(uv2, Rotate2D(0.25 * time) * rv.x);

    float val = 0.5 * fbm(uv * 2.0 * fbmLow(length(uv2) + rv - time));
    uv2 = mul(uv2, Rotate2D(-0.25 * time) * rv.x);
    
    float3 final = SAMPLE_MAINTEX(uv * 1.0 - fbm(uv2 * val * 5.0) * _Amount);

    float3 innerColor = final.rgb, outerColor = final.rgb;
    
    innerColor *= _InnerColor;
    innerColor = lerp(dot(innerColor, float3(0.299, 0.587, 0.114)), innerColor, _InnerSaturation);
    innerColor = (innerColor - 0.5) * _InnerContrast + 0.5 + _InnerBrightness;
    innerColor = clamp(innerColor, 0.0, 1.0);
    innerColor = pow(innerColor, _InnerGamma);

    innerColor = lerp(final, innerColor, _Amount);
    final = lerp(innerColor, final, rv.x);

    outerColor *= _OuterColor;
    outerColor = lerp(dot(outerColor, float3(0.299, 0.587, 0.114)), outerColor, _OuterSaturation);
    outerColor = (outerColor - 0.5) * _OuterContrast + 0.5 + _OuterBrightness;
    outerColor = clamp(outerColor, 0.0, 1.0);
    outerColor = pow(outerColor, _OuterGamma);

    outerColor = lerp(final, outerColor, _Amount);
    final = lerp(final, outerColor, rv.x);

    return final;
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      #pragma multi_compile ___ ASPECT_RATIO
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
