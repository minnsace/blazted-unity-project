///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Trippy Vision.
/// </summary>
Shader "Hidden/VisionFX/TrippyVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float _Speed;
  float _Definition;
  float _Displacement;
  int _BlendOp;

  inline float2 SinCos(float x)
  {
    return float2(sin(x), cos(x));
  }

  inline float Simplex2D(float2 p)
  {
    const float F2 =  0.3660254;
    const float G2 = -0.2113249;

    float2 s = floor(p + (p.x + p.y) * F2),
           x = p - s - (s.x + s.y) * G2;
    
    float e = step(0.0, x.x - x.y);
    float2 i1 = float2(e, 1.0-e),
           x1 = x - i1 - G2,
           x2 = x - 1.0 - 2.0 * G2;
           
    float3 w, d;
    w.x = dot(x, x);
    w.y = dot(x1, x1);
    w.z = dot(x2, x2);
    w = max(0.5 - w, 0.0);
    d.x = dot(Rand2(s + 0.0), x);
    d.y = dot(Rand2(s + i1), x1);
    d.z = dot(Rand2(s + 1.0), x2);
    
    w *= w;
    w *= w;
    d *= w;
    
    return dot(d, (float3)70.0);
  }

  inline float3 Rotate3D(float3 p, float3 v, float phi)
  {
    v = normalize(v);
    float2 t = SinCos(-phi);
    float s = t.x, c = t.y, x = -v.x, y = -v.y, z = -v.z;
    
    float4x4 M = float4x4(x * x * (1.0 - c) + c,     x * y * (1.0 - c) - z * s, x * z * (1.0 - c) + y * s, 0.0,
                          y * x * (1.0 - c) + z * s, y * y * (1.0 - c) + c,     y * z * (1.0 - c) - x * s, 0.0,
                          z * x * (1.0 - c) - y * s, z * y * (1.0 - c) + x * s, z * z * (1.0 - c) + c,     0.0,
                          0.0,                       0.0,                       0.0,                       1.0);
    
    return mul(M, float4(p, 1.0)).xyz;
  }

  inline float Trippy(float2 position, float time)
  {
    float color = 0.0;
    float t = 2.0 * time;
    color += sin(position.x * cos(t / 10.0) * 20.0) + cos(position.x * cos(t / 15.0) * 10.0);
    color += sin(position.y * sin(t / 5.0)  * 15.0) + cos(position.x * sin(t / 25.0) * 20.0);
    color += sin(position.x * sin(t / 10.0) * 0.2)  + sin(position.y * sin(t / 35.0) * 10.0);
    color *= sin(t * 0.1) * 0.5;

    return color;
  }

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    float speed = _Speed * _Time.y;
    float2 uv2 = (uv - (float2)0.5) * 2.0;

    float3 lsd = float3(1.0, 1.0, 1.0);
    lsd = Rotate3D(lsd, float3(1.0, 1.0, 0.0), speed);
    lsd = Rotate3D(lsd, float3(1.0, 1.0, 0.0), speed);
    lsd = Rotate3D(lsd, float3(1.0, 1.0, 0.0), speed);

    float2 v0 = _Speed * 0.5 * SinCos(0.3457 * speed + 0.3423) - Simplex2D(uv2 * 0.917),
           v1 = _Speed * 0.5 * SinCos(0.7435 * speed + 0.4565) - Simplex2D(uv2 * 0.521),
           v2 = _Speed * 0.5 * SinCos(0.5345 * speed + 0.3434) - Simplex2D(uv2 * 0.759);

    lsd = float3(dot(uv2 - v0, lsd.xy), dot(uv2 - v1, lsd.yz), dot(uv2 - v2, lsd.zx));

    lsd *= 0.2 + _Definition * float3((16.0 * Simplex2D(uv2 + v0) + 8.0 * Simplex2D((uv2 + v0) * 2.0) + 4.0 * Simplex2D((uv2 + v0) * 4.0) + 2.0 * Simplex2D((uv2 + v0) * 8.0) + Simplex2D((v0 + uv2) * 16.0)) / 32.0,
                                      (16.0 * Simplex2D(uv2 + v1) + 8.0 * Simplex2D((uv2 + v1) * 2.0) + 4.0 * Simplex2D((uv2 + v1) * 4.0) + 2.0 * Simplex2D((uv2 + v1) * 8.0) + Simplex2D((v1 + uv2) * 16.0)) / 32.0,
                                      (16.0 * Simplex2D(uv2 + v2) + 8.0 * Simplex2D((uv2 + v2) * 2.0) + 4.0 * Simplex2D((uv2 + v2) * 4.0) + 2.0 * Simplex2D((uv2 + v2) * 8.0) + Simplex2D((v2 + uv2) * 16.0)) / 32.0);

    lsd = YIQ2RGB(lsd);

    lsd *= (float3)1.0 - 0.25 * float3(Trippy(uv2 * 0.25, speed + 0.5),
                                       Trippy(uv2 * 0.7,  speed + 0.2),
                                       Trippy(uv2 * 0.4,  speed + 0.7));
    lsd = RGB2YIQ(lsd);

    float2 displacement = (float2)(Luminance(lsd) * _Displacement);
    pixel.r = SAMPLE_MAINTEX(uv + displacement).r;
    pixel.g = SAMPLE_MAINTEX(uv + float2(displacement.y, displacement.x)).g;
    pixel.b = SAMPLE_MAINTEX(uv - displacement).b;

    return BlendOp(_BlendOp, pixel, lsd);
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
