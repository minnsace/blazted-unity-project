///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Double Vision.
/// </summary>
Shader "Hidden/VisionFX/DoubleVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float2 _Strength;
  float2 _Speed;

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    return (pixel + SAMPLE_MAINTEX(uv + float2(sin(_Time.y * _Speed.x) * _Strength.x - cos(_Time.y) * _Strength.x,
                                               cos(_Time.y * _Speed.y) * _Strength.y + sin(_Time.y) * _Strength.y)).rgb) * 0.5;
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
