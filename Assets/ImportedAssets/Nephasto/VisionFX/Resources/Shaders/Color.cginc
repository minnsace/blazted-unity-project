﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COLOR_CGINC
#define COLOR_CGINC

// Color control.
#ifdef COLOR_CONTROLS
  float _Brightness;
  float _Contrast;
  float _Gamma;
  float _Hue;
  float _Saturation;
#endif

// Luminance.
inline half Luminance601(half3 pixel)
{
  return dot(half3(0.299f, 0.587f, 0.114f), pixel);
}

// RGB -> HSV http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
inline half3 RGB2HSV(half3 c)
{
  const half4 K = half4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
  const half Epsilon = 1.0e-10;

  half4 p = lerp(half4(c.bg, K.wz), half4(c.gb, K.xy), step(c.b, c.g));
  half4 q = lerp(half4(p.xyw, c.r), half4(c.r, p.yzx), step(p.x, c.r));

  half d = q.x - min(q.w, q.y);

  return half3(abs(q.z + (q.w - q.y) / (6.0 * d + Epsilon)), d / (q.x + Epsilon), q.x);
}

// HSV -> RGB http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
inline half3 HSV2RGB(half3 c)
{
  const half4 K = half4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  half3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);

  return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// RGB -> YIQ.
inline float3 RGB2YIQ(float3 c)
{
  return mul(float3x3(0.299, 0.587, 0.114,
                      0.596,-0.274,-0.321,
                      0.211,-0.523, 0.311), c);
}

// YIQ -> RGB.
inline float3 YIQ2RGB(float3 c)
{
  return mul(float3x3(1.0, 0.956, 0.621,
                      1.0,-0.272,-0.647,
                      1.0,-1.107, 1.705), c);
}

#ifdef COLOR_CONTROLS

// Color adjust.
inline float3 ColorAdjust(float3 pixel)
{
  // Brightness.
  pixel += _Brightness;

  // Contrast.
  pixel = (pixel - 0.5) * ((1.015 * (_Contrast + 1.0)) / (1.015 - _Contrast)) + 0.5;

  // Hue & saturation.
  float3 hsv = RGB2HSV(pixel);

  hsv.x += _Hue;
  hsv.y *= _Saturation;

  pixel = HSV2RGB(hsv);

  // Gamma.
  pixel = pow(pixel, _Gamma);

  return pixel;
}

#endif // COLOR_CONTROLS

// Additive.
inline half3 Additive(half3 s, half3 d)
{
  return s + d;
}

// Color burn.
inline half3 Burn(half3 s, half3 d)
{
  return 1.0 - (1.0 - d) / s;
}

// Color.
half3 Color(half3 s, half3 d)
{
  s = RGB2HSV(s);
  s.z = RGB2HSV(d).z;

  return HSV2RGB(s);
}

// Darken.
inline half3 Darken(half3 s, half3 d)
{
  return min(s, d);
}

// Darker color.
inline half3 Darker(half3 s, half3 d)
{
  return (Luminance601(s) < Luminance601(d)) ? s : d;
}

// Difference.
inline half3 Difference(half3 s, half3 d)
{
  return abs(d - s);
}

// Divide.
inline half3 Divide(half3 s, half3 d)
{
  return (d > 0.0) ? s / d : s;
}

// Color dodge.
inline half3 Dodge(half3 s, half3 d)
{
  return (s < 1.0) ? d / (1.0 - s) : s;
}

// HardMix.
inline half3 HardMix(half3 s, half3 d)
{
  return floor(s + d);
}

// Hue.
half3 Hue(half3 s, half3 d)
{
  d = RGB2HSV(d);
  d.x = RGB2HSV(s).x;

  return HSV2RGB(d);
}

// HardLight.
half3 HardLight(half3 s, half3 d)
{
  return (s < 0.5) ? 2.0 * s * d : 1.0 - 2.0 * (1.0 - s) * (1.0 - d);
}

// Lighten.
inline half3 Lighten(half3 s, half3 d)
{
  return max(s, d);
}

// Lighter color.
inline half3 Lighter(half3 s, half3 d)
{
  return (Luminance601(s) > Luminance601(d)) ? s : d;
}

// Multiply.
inline half3 Multiply(half3 s, half3 d)
{
  return s * d;
}

// Overlay.
inline half3 Overlay(half3 s, half3 d)
{
  return (s > 0.5) ? 1.0 - 2.0 * (1.0 - s) * (1.0 - d) : 2.0 * s * d;
}

// Screen.
inline half3 Screen(half3 s, half3 d)
{
  return s + d - s * d;
}

// Solid.
inline half3 Solid(half3 s, half3 d)
{
  return d;
}

// Soft light.
half3 SoftLight(half3 s, half3 d)
{
  return (1.0 - s) * s * d + s * (1.0 - (1.0 - s) * (1.0 - d));
}

// Pin light.
half3 PinLight(half3 s, half3 d)
{
  return (2.0 * s - 1.0 > d) ? 2.0 * s - 1.0 : (s < 0.5 * d) ? 2.0 * s : d;
}

// Saturation.
half3 Saturation(half3 s, half3 d)
{
  d = RGB2HSV(d);
  d.y = RGB2HSV(s).y;

  return HSV2RGB(d);
}

// Subtract.
inline half3 Subtract(half3 s, half3 d)
{
  return s - d;
}

// VividLight.
half3 VividLight(half3 s, half3 d)
{
  return (s < 0.5) ? (s > 0.0 ? 1.0 - (1.0 - d) / (2.0 * s) : s) : (s < 1.0 ? d / (2.0 * (1.0 - s)) : s);
}

// Luminosity.
half3 Luminosity(half3 s, half3 d)
{
  half dLum = Luminance601(s);
  half sLum = Luminance601(d);

  half lum = sLum - dLum;

  half3 c = d + lum;
  half minC = min(min(c.r, c.g), c.b);
  half maxC = max(max(c.r, c.b), c.b);

  if (minC < 0.0)
    return sLum + ((c - sLum) * sLum) / (sLum - minC);
  else if (maxC > 1.0)
    return sLum + ((c - sLum) * (1.0 - sLum)) / (maxC - sLum);

  return c;
}

inline half3 BlendOp(int blendOp, half3 s, half3 d)
{
  switch (blendOp)
  {
    case 0:  return Additive(s, d);
    case 1:  return Burn(s, d);
    case 2:  return Color(s, d);
    case 3:  return Darken(s, d);
    case 4:  return Darker(s, d);  
    case 5:  return Difference(s, d);
    case 6:  return Divide(s, d);
    case 7:  return Dodge(s, d);
    case 8:  return HardMix(s, d);
    case 9:  return Hue(s, d);
    case 10: return HardLight(s, d);
    case 11: return Lighten(s, d);
    case 12: return Lighter(s, d);
    case 13: return Multiply(s, d);
    case 14: return Overlay(s, d);
    case 15: return Screen(s, d);
    case 17: return SoftLight(s, d);
    case 18: return PinLight(s, d);
    case 19: return Saturation(s, d);
    case 20: return Subtract(s, d);
    case 21: return VividLight(s, d);
    case 22: return Luminosity(s, d);
    default: return d;
  }
}

#endif