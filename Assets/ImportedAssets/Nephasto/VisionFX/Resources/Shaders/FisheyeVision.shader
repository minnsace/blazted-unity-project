///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Fisheye Vision.
/// </summary>
Shader "Hidden/VisionFX/FisheyeVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float _Barrel;
  float2 _Center;
  float3 _BackgroundColor;

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    float prop = _ScreenParams.x / _ScreenParams.y;

    float r1 = sqrt(dot(_Center, _Center));

		float2 d2 = uv - _Center;
		float r2 = sqrt(dot(d2, d2));

		float power = (_PI2 / (2.0 * r1) ) * (_Barrel - 0.5);

		float bind = 1.0;
		if (power > 0.0)
      bind = r1;
		else
      bind = prop < 1.0 ? _Center.x : _Center.y;

		// Based on Paul Bourke formulas: http://paulbourke.net/dome/fisheyecorrect/
		if (power > 0.0)
			uv = _Center + normalize(d2) * tan(r2 * power * 0.5) * bind / tan(bind * power * 0.5);
		else if (power < 0.0)
			uv = _Center + normalize(d2) * atan(r2 * -power * 5.0) * bind / atan(-power * bind * 5.0);

    if (uv.x > 1.0 || uv.y > 1.0 || uv.x < 0.0 || uv.y < 0.0)
      return _BackgroundColor;

    return SAMPLE_MAINTEX(uv);
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
