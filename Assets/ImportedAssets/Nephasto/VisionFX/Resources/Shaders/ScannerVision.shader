///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Scanner Vision.
/// </summary>
Shader "Hidden/VisionFX/ScannerVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  float3 _Tint;

  float _LinesCount;
  float _LinesStrength;

  float _ScanlineStrength;
  float _ScanlineWidth;
  float _ScanlineSpeed;

  float _NoiseBandStrength;
  float _NoiseBandSpeed;
  float _NoiseBandWidth;

  inline float noise(float2 p)
  {
    float2 ip = floor(p);
    float2 u = frac(p);
    u = u * u * (3.0 - 2.0 * u);

    float res = lerp(lerp(Rand(ip), Rand(ip+ float2(1.0, 0.0)), u.x),
                     lerp(Rand(ip + float2(0.0, 1.0)), Rand(ip + float2(1.0, 1.0)), u.x), u.y);
    return res * res;
  }

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    pixel += sin(uv.y * _LinesCount) * _LinesStrength;

    float y = frac(_Time.y * _ScanlineSpeed);
    pixel += (smoothstep(y - _ScanlineWidth, y, uv.y) - smoothstep(y, y + _ScanlineWidth, uv.y)) * abs(sin((uv.y + _Time.y))) * _ScanlineStrength;

    float p = frac(-_Time.y * _NoiseBandSpeed);
    float d = smoothstep(p - _NoiseBandWidth, p, uv.y) - smoothstep(p, p + _NoiseBandWidth, uv.y);
    pixel += noise((uv * 1000.0 + _Time.y * 10.0) * 5.0) * _NoiseBandStrength * d;

    pixel *= _Tint;

    return pixel;
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      //#pragma multi_compile ___ DEMO
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
