///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Blurry Vision.
/// </summary>
Shader "Hidden/VisionFX/BlurryVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  int _FrameCount;

  sampler2D _RT0;
  sampler2D _RT1;
  sampler2D _RT2;
  sampler2D _RT3;
  sampler2D _RT4;
  sampler2D _RT5;
  sampler2D _RT6;
  sampler2D _RT7;
  sampler2D _RT8;
  sampler2D _RT9;

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    switch (_FrameCount)
    {
      case 1:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.5);
        break;

      case 2:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.75);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.25);
        break;

      case 3:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.75);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.50);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.25);
        break;

      case 4:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.64);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.48);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.32);
        pixel = lerp(pixel, tex2D(_RT3, uv).rgb, 0.16);
        break;

      case 5:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.70);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.56);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.42);
        pixel = lerp(pixel, tex2D(_RT3, uv).rgb, 0.28);
        pixel = lerp(pixel, tex2D(_RT4, uv).rgb, 0.14);
        break;

      case 6:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.750);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.625);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.500);
        pixel = lerp(pixel, tex2D(_RT3, uv).rgb, 0.375);
        pixel = lerp(pixel, tex2D(_RT4, uv).rgb, 0.250);
        pixel = lerp(pixel, tex2D(_RT5, uv).rgb, 0.125);
        break;

      case 7:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.777);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.666);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.555);
        pixel = lerp(pixel, tex2D(_RT3, uv).rgb, 0.444);
        pixel = lerp(pixel, tex2D(_RT4, uv).rgb, 0.333);
        pixel = lerp(pixel, tex2D(_RT5, uv).rgb, 0.222);
        pixel = lerp(pixel, tex2D(_RT6, uv).rgb, 0.111);
        break;

      case 8:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.8);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.7);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.6);
        pixel = lerp(pixel, tex2D(_RT3, uv).rgb, 0.5);
        pixel = lerp(pixel, tex2D(_RT4, uv).rgb, 0.4);
        pixel = lerp(pixel, tex2D(_RT5, uv).rgb, 0.3);
        pixel = lerp(pixel, tex2D(_RT6, uv).rgb, 0.2);
        pixel = lerp(pixel, tex2D(_RT7, uv).rgb, 0.1);
        break;

      case 9:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.81);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.72);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.63);
        pixel = lerp(pixel, tex2D(_RT3, uv).rgb, 0.54);
        pixel = lerp(pixel, tex2D(_RT4, uv).rgb, 0.45);
        pixel = lerp(pixel, tex2D(_RT5, uv).rgb, 0.36);
        pixel = lerp(pixel, tex2D(_RT6, uv).rgb, 0.27);
        pixel = lerp(pixel, tex2D(_RT7, uv).rgb, 0.18);
        pixel = lerp(pixel, tex2D(_RT8, uv).rgb, 0.09);
        break;

      case 10:
        pixel = lerp(pixel, tex2D(_RT0, uv).rgb, 0.83);
        pixel = lerp(pixel, tex2D(_RT1, uv).rgb, 0.74);
        pixel = lerp(pixel, tex2D(_RT2, uv).rgb, 0.66);
        pixel = lerp(pixel, tex2D(_RT3, uv).rgb, 0.58);
        pixel = lerp(pixel, tex2D(_RT4, uv).rgb, 0.50);
        pixel = lerp(pixel, tex2D(_RT5, uv).rgb, 0.42);
        pixel = lerp(pixel, tex2D(_RT6, uv).rgb, 0.33);
        pixel = lerp(pixel, tex2D(_RT7, uv).rgb, 0.25);
        pixel = lerp(pixel, tex2D(_RT8, uv).rgb, 0.16);
        pixel = lerp(pixel, tex2D(_RT9, uv).rgb, 0.08);
        break;
    }

    return pixel;
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
