///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// Anime Vision.
/// </summary>
Shader "Hidden/VisionFX/AnimeVision"
{
  Properties
  {
    _MainTex ("Texture", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "VisionFX.cginc"

  int _Aspect;
  float4 _Color;
  float _Radius;
  float _Length;
  float _Speed;
  float _Frequency;
  float _Softness;
  float _Noise;

  #define LOFI(x, d) (floor((x) / (d)) * (d))

  inline float Line(float2 v, float2 r)
  {
    float4 h = float4(Rand(float2(floor(v * r + float2(0.0, 0.0)) / r)),
                      Rand(float2(floor(v * r + float2(0.0, 1.0)) / r)),
                      Rand(float2(floor(v * r + float2(1.0, 0.0)) / r)),
                      Rand(float2(floor(v * r + float2(1.0, 1.0)) / r)));

    float2 ip = float2(smoothstep((float2)0.0, (float2)1.0, mod(v * r, 1.0)));
    
    return lerp(lerp(h.x, h.y, ip.y), lerp(h.z, h.w, ip.y), ip.x);
  }

  inline float LineNoise(float2 v)
  {
    float sum = 0.0;

    sum += Line(v + (float2)1.0, (float2)4.0)   / 2.0;
    sum += Line(v + (float2)2.0, (float2)8.0)   / 4.0;
    sum += Line(v + (float2)3.0, (float2)16.0)  / 8.0;
    sum += Line(v + (float2)4.0, (float2)32.0)  / 16.0;
    sum += Line(v + (float2)5.0, (float2)64.0)  / 32.0;
    sum += Line(v + (float2)6.0, (float2)128.0) / 64.0;

    return sum;
  }

  inline float3 EffectVision(float3 pixel, float2 uv)
  {
    uv = (uv - 0.5) * 2.0;

    if (_Aspect == 1)
      uv.x *= _ScreenParams.x / _ScreenParams.y;

    float2 puv = float2(_Noise * length(uv) + _Speed * LOFI(_Time.y, _Speed / 200.0), _Frequency * atan2(uv.x, uv.y));

    float value = LineNoise(puv);
    value = length(uv) - _Radius - _Length * (value - 0.5);
    value = smoothstep(-_Softness, _Softness, value);

    pixel = lerp(pixel, _Color.rgb, value * _Color.a);

    return pixel;
  }

  #include "VisionFrag.cginc"
  ENDCG

  SubShader
  {
    Cull Off
    ZWrite Off
    ZTest Always

    // Pass 0: Effect.
    Pass
    {
      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      #pragma exclude_renderers d3d9 d3d11_9x ps3 flash
      
      #pragma multi_compile ___ COLOR_CONTROLS
      
      //#pragma vertex VertVision
      #pragma vertex vert_img
      #pragma fragment FragVision
      ENDCG
    }
  }
  
  FallBack off
}
