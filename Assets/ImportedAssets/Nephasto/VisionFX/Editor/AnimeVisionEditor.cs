///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Anime Vision editor.
  /// </summary>
  [CustomEditor(typeof(AnimeVision))]
  public sealed class AnimeVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      AnimeVision effect = (AnimeVision)target;

      effect.Aspect = Toggle("Aspect ratio", "Respect or not the aspect ratio.", effect.Aspect, false);

      effect.Radius = SliderField("Radius", "Effect radius.", effect.Radius, 0.0f, 2.0f, 1.25f);

      effect.Length = SliderField("Length", "Length of the lines.", effect.Length, 0.0f, 5.0f, 2.0f);

      effect.Speed = SliderField("Speed", "Speed in the movement of the lines.", effect.Speed, 0.0f, 2.0f, 1.0f);

      effect.Frequency = SliderField("Frequency", "Edge of the lines.", effect.Frequency, 0.0f, 50.0f, 10.0f);

      effect.Softness = SliderField("Softness", "Smoothness of the lines.", effect.Softness, 0.0f, 1.0f, 0.1f);
      
      effect.Noise = SliderField("Noise", "Geometry of the shape of the lines.", effect.Noise, 0.0f, 1.0f, 0.1f);

      Separator();

      effect.Color = ColorField("Color", "Color of the lines. Use the alpha channel to define its transparency.", effect.Color, Color.white);
    }
  }
}
