///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Halftone Vision editor.
  /// </summary>
  [CustomEditor(typeof(HalftoneVision))]
  public sealed class HalftoneVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      HalftoneVision effect = (HalftoneVision)target;

      effect.Size = SliderField("Size", "Point size.", effect.Size, 0.0f, 5.0f, 2.0f);

      effect.Angle = SliderField("Angle", "Angle of the pattern.", effect.Angle, 0.0f, 180.0f, 15.0f);

      effect.Strength = SliderField("Strength", "Pattern strength.", effect.Strength, 0.0f, 50.0f, 10.0f);

      effect.Sensitivity = SliderField("Sensitivity", "Sensitivity used to create the pattern.", effect.Sensitivity, 0.0f, 10.0f, 8.5f);

      effect.BlendOp = (VisionBlendOps)EnumPopupField("Blend", "Blend operation with the background.", effect.BlendOp, VisionBlendOps.Color);
    }
  }
}
