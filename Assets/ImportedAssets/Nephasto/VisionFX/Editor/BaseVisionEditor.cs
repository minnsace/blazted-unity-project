///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Base Vision editor.
  /// </summary>
  [CustomEditor(typeof(BaseVision))]
  public abstract class BaseVisionEditor : Inspector
  {
    private bool displayColorControls = false;

    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected virtual void VisionInspector()
    {
    }

    /// <summary>
    /// OnInspectorGUI.
    /// </summary>
    protected override void InspectorGUI()
    {
      BaseVision baseTarget = this.target as BaseVision;

      ResetGUI(0, 0.0f, 125.0f);

      Undo.RecordObject(baseTarget, baseTarget.GetType().Name);

      BeginVertical();
      {
        /////////////////////////////////////////////////
        // Common.
        /////////////////////////////////////////////////

        Separator();

        baseTarget.Amount = SliderField("Amount", "The strength of the effect.\nFrom 0.0 (no effect) to 1.0 (full effect).", baseTarget.Amount, 0.0f, 1.0f, 1.0f);

        /////////////////////////////////////////////////
        // Effect GUI.
        /////////////////////////////////////////////////

        Separator();

        VisionInspector();

        /////////////////////////////////////////////////
        // Color controls.
        /////////////////////////////////////////////////

        Separator();

        baseTarget.EnableColorControls = ToogleFoldout("Color", baseTarget.EnableColorControls, ref displayColorControls);
        if (displayColorControls == true)
        {
          EnableGUI = baseTarget.EnableColorControls;

          IndentLevel++;

          baseTarget.Brightness = SliderField("Brightness", "Brightness [-1.0, 1.0]. Default 0.", baseTarget.Brightness, -1.0f, 1.0f, 0.0f);

          baseTarget.Contrast = SliderField("Contrast", "Contrast [-1.0, 1.0]. Default 0.", baseTarget.Contrast, -1.0f, 1.0f, 0.0f);

          baseTarget.Gamma = SliderField("Gamma", "Gamma [0.1, 10.0]. Default 1.", baseTarget.Gamma, 0.01f, 10.0f, 1.0f);

          baseTarget.Hue = SliderField("Hue", "The color wheel [0.0, 1.0]. Default 0.", baseTarget.Hue, 0.0f, 1.0f, 0.0f);

          baseTarget.Saturation = SliderField("Saturation", "Intensity of a colors [0.0, 2.0]. Default 1.", baseTarget.Saturation, 0.0f, 2.0f, 1.0f);

          IndentLevel--;

          EnableGUI = true;
        }

        /////////////////////////////////////////////////
        // Description.
        /////////////////////////////////////////////////

        Separator();

        EditorGUILayout.HelpBox(baseTarget.ToString(), MessageType.Info);

        /////////////////////////////////////////////////
        // Misc.
        /////////////////////////////////////////////////

        Separator();

        BeginHorizontal();
        {
          if (GUILayout.Button(new GUIContent("[doc]", "Online documentation"), GUI.skin.label) == true)
            Application.OpenURL(BaseVision.Documentation);

          FlexibleSpace();

          if (Button("Reset") == true)
            baseTarget.ResetDefaultValues();
        }
        EndHorizontal();
      }
      EndVertical();

      Separator();
    }
  }
}
