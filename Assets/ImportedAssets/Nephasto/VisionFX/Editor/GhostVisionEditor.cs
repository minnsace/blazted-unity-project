///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Ghost Vision editor.
  /// </summary>
  [CustomEditor(typeof(GhostVision))]
  public sealed class GhostVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      GhostVision effect = (GhostVision)target;

      effect.Focus = Vector2Field("Focus", "Focus point.", effect.Focus, UnityEngine.Vector2.zero);

      effect.Aperture = SliderField("Aperture", "Focus aperture.", effect.Aperture, 0.0f, 10.0f, 1.0f);

      effect.Zoom = SliderField("Zoom", "Zoom aperture.", effect.Zoom, 0.0f, 2.0f, 1.0f);

      effect.ChangeFOV = Toggle("Change FOV", "Change the field of view?", effect.ChangeFOV, true);
      if (effect.ChangeFOV == true)
      {
        IndentLevel++;

        effect.FOV = SliderField("FOV", "Field Of View.", effect.FOV, 1.0f, 179.0f, 60.0f);

        IndentLevel--;
      }

      effect.Speed = SliderField("Speed", "Speed of turns.", effect.Speed, 0.0f, 2.0f, 0.5f);

      effect.AspectRatio = Toggle("Aspect ratio", "Focus aspect ratio.", effect.AspectRatio, true);

      Separator();

      Label("Inner color");

      IndentLevel++;

      effect.InnerTint = ColorField("Tint", "Inner tint.", effect.InnerTint, Color.white);
      effect.InnerSaturation = SliderField("Saturation", "Color saturation [0, 1].", effect.InnerSaturation, 0.0f, 1.0f, 1.0f);
      effect.InnerBrightness = SliderField("Brightness", "Color brightness.", effect.InnerBrightness, -1.0f, 1.0f, 0.0f);
      effect.InnerContrast = SliderField("Contrast", "The difference in color and brightness.", effect.InnerContrast, -1.0f, 1.0f, 0.0f);
      effect.InnerGamma = SliderField("Gamma", "Optimizes the contrast and brightness in the midtones.", effect.InnerGamma, 0.01f, 5.0f, 1.0f);

      IndentLevel--;

      Separator();

      Label("Outer color");

      IndentLevel++;

      effect.OuterTint = ColorField("Tint", "Outer tint.", effect.OuterTint, Color.white);
      effect.OuterSaturation = SliderField("Saturation", "Color saturation [0, 1].", effect.OuterSaturation, 0.0f, 1.0f, 1.0f);
      effect.OuterBrightness = SliderField("Brightness", "Color brightness.", effect.OuterBrightness, -1.0f, 1.0f, 0.0f);
      effect.OuterContrast = SliderField("Contrast", "The difference in color and brightness.", effect.OuterContrast, -1.0f, 1.0f, 0.0f);
      effect.OuterGamma = SliderField("Gamma", "Optimizes the contrast and brightness in the midtones.", effect.OuterGamma, 0.01f, 5.0f, 1.0f);

      IndentLevel--;      
    }
  }
}
