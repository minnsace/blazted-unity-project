///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Damage Vision editor.
  /// </summary>
  [CustomEditor(typeof(DamageVision))]
  public sealed class DamageVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      DamageVision effect = (DamageVision)target;

      effect.Damage = SliderField("Damage", "Amount of damage.", effect.Damage, 0.0f, 1.0f, 0.7f);

      effect.Definition = SliderField("Definition", "Border definition of the liquid.", effect.Definition, 0.0f, 1.0f, 0.5f);

      effect.DamageBrightness = SliderField("Brightness", "brightness of the liquid.", effect.DamageBrightness, 0.0f, 10.0f, 3.0f);

      effect.Distortion = SliderField("Distortion", "Distortion of the liquid in the background.", effect.Distortion, 0.0f, 1.0f, 0.2f);

      Separator();

      effect.BlendOp = (VisionBlendOps)EnumPopupField("Blend", "Blend operation with the background.", effect.BlendOp, VisionBlendOps.Darken);

      effect.Color = ColorField("Color", "Liquid color.", effect.Color, Color.red);
    }
  }
}
