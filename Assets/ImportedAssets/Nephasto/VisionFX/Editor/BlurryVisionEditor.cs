///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Blurry Vision editor.
  /// </summary>
  [CustomEditor(typeof(BlurryVision))]
  public sealed class BlurryVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      BlurryVision effect = (BlurryVision)target;

      effect.Frames = SliderField("Frames", "Number of frames used. The more you use, the worse the performance.", effect.Frames, 0, BlurryVision.MaxFrames, 5);

      effect.FrameStep = SliderField("Step", "Step in the use of frames.", effect.FrameStep, 0, 10, 0);

      effect.FrameResolution = SliderField("Resolution", "resolution of the frames used, if it is 1 the original size will be used.", effect.FrameResolution, 0.1f, 1.0f, 1.0f);
    }
  }
}
