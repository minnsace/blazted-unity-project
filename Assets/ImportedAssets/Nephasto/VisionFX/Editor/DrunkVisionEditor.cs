///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Drunk Vision editor.
  /// </summary>
  [CustomEditor(typeof(DrunkVision))]
  public sealed class DrunkVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      DrunkVision effect = (DrunkVision)target;

      effect.Drunkenness = SliderField("Drunkenness", "Drunkenness level. Values above 3 can cause dizziness in the player.", effect.Drunkenness, 0.0f, 1.0f, 0.1f);

      IndentLevel++;

      effect.DrunkSpeed = SliderField("Speed", "Speed of the oscillations.", effect.DrunkSpeed, 0.0f, 10.0f, 1.0f);
      effect.DrunkAmplitude = SliderField("Amplitude", "Amplitude of the oscillations.", effect.DrunkAmplitude, 0.0f, 1.0f, 0.75f);

      IndentLevel--;

      Separator();

      effect.Swinging = SliderField("Swinging", "Head swing.", effect.Swinging, 0.0f, 1.0f, 0.25f);

      IndentLevel++;

      effect.SwingingSpeed = SliderField("Speed", "Head swing speed.", effect.SwingingSpeed, 0.0f, 10.0f, 2.0f);

      IndentLevel--;

      Separator();

      Separator();

      effect.Aberration = SliderField("Aberration", "Chromatic aberration.", effect.Aberration, 0.0f, 10.0f, 1.0f);

      IndentLevel++;

      effect.AberrationSpeed = SliderField("Speed", "Chromatic aberration speed.", effect.AberrationSpeed, 0.0f, 10.0f, 1.0f);

      IndentLevel--;

      Separator();

      effect.VignetteAmount = SliderField("Vignette", "Blink strength.", effect.VignetteAmount, 0.0f, 10.0f, 1.0f);

      IndentLevel++;

      effect.VignetteSpeed = SliderField("Speed", "Blink speed.", effect.VignetteSpeed, 0.0f, 10.0f, 1.0f);

      IndentLevel--;
    }
  }
}
