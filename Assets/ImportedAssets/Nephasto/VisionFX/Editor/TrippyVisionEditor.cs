///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Trippy Vision editor.
  /// </summary>
  [CustomEditor(typeof(TrippyVision))]
  public sealed class TrippyVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      TrippyVision effect = (TrippyVision)target;

      effect.Speed = SliderField("Speed", "Effect speed.", effect.Speed, 0.0f, 10.0f, 0.5f);

      effect.Definition = SliderField("Definition", "Definition of contours.", effect.Definition, 0.0f, 20.0f, 2.5f);

      effect.Displacement = SliderField("Displacement", "Chromatic deformation of the background.", effect.Displacement, 0.0f, 2.0f, 0.2f);

      effect.BlendOp = (VisionBlendOps)EnumPopupField("Blend", "Blend operation with the background.", effect.BlendOp, VisionBlendOps.Lighten);
    }
  }
}
