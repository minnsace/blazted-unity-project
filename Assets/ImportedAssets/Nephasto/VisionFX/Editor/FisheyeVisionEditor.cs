///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Fisheye Vision editor.
  /// </summary>
  [CustomEditor(typeof(FisheyeVision))]
  public sealed class FisheyeVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      FisheyeVision effect = (FisheyeVision)target;

      effect.Barrel = SliderField("Barrel", "Deformation.", effect.Barrel, -1.0f, 1.0f, 0.0f);

      effect.Center = Vector2Field("Center", "Center.", effect.Center, FisheyeVision.DefaultCenter);

      effect.BackgroundColor = ColorField("Background", "Background color.", effect.BackgroundColor, Color.black);
    }
  }
}
