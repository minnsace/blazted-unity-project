///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEditor;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Scanner Vision editor.
  /// </summary>
  [CustomEditor(typeof(ScannerVision))]
  public sealed class ScannerVisionEditor : BaseVisionEditor
  {
    /// <summary>
    /// Custom inspector.
    /// </summary>
    protected override void VisionInspector()
    {
      ScannerVision effect = (ScannerVision)target;

      effect.Tint = ColorField("Tint", "Screen tint.", effect.Tint, ScannerVision.DefaultTint);

      Separator();

      Label("Lines");

      IndentLevel++;

      effect.LinesStrength = SliderField("Strength", "Lines strength.", effect.LinesStrength, 0.0f, 1.0f, 0.1f);
      effect.LinesCount = SliderField("Count", "Lines count.", effect.LinesCount, 0, 1000, 700);

      IndentLevel--;

      Separator();

      Label("Scanlines");

      IndentLevel++;

      effect.ScanLineStrength = SliderField("Strength", "Scanline strength.", effect.ScanLineStrength, 0.0f, 1.0f, 0.5f);
      effect.ScanLineWidth = SliderField("Width", "Scanline width.", effect.ScanLineWidth, 0.0f, 5.0f, 0.2f);
      effect.ScanLineSpeed = SliderField("Speed", "Scanline speed.", effect.ScanLineSpeed, -5.0f, 5.0f, 0.5f);

      IndentLevel--;

      Separator();

      Label("Noise band");

      IndentLevel++;

      effect.NoiseBandStrength = SliderField("Strength", "Noise band strength.", effect.NoiseBandStrength, 0.0f, 1.0f, 0.2f);
      effect.NoiseBandWidth = SliderField("Width", "Noise band width.", effect.NoiseBandWidth, 0.0f, 5.0f, 0.2f);
      effect.NoiseBandSpeed = SliderField("Speed", "Noise band speed.", effect.NoiseBandSpeed, -5.0f, 5.0f, 0.2f);

      IndentLevel--;
    }
  }
}
