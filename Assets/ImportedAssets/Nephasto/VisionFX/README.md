# Vision FX

A collection of screen effects to increase the juice of your games quickly and easily.

## Getting Started

After install, read the documentation at folder Nephasto/VisionFX/Documentation or visit the [online version](https://www.nephasto.com/store/vision-fx.html).

## Requisites

Unity 2019.4 and higher.

## Running the demo

Open the scene at folder Nephasto/VisionFX/Demo/Scene. You can delete the forder if you want.

## Bugs and/or questions

Please send us an email to hello@nephasto.com.

## Authors

* **Martin Bustos** - [@Nephasto](https://twitter.com/Nephasto)

## Thanks to

* Austin McGaugh.

## License

See the [Asset Store Terms of Service and EULA](https://unity3d.com/es/legal/as_terms) file for details.

**THIS ASSET CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.**
