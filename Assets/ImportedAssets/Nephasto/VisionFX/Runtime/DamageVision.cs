///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Damage Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Damage vision")]
  public sealed class DamageVision : BaseVision
  {
    /// <summary>
    /// Amount of damage [0 - 1]. Default 0.7.
    /// </summary>
    public float Damage
    {
      get { return damage; }
      set { damage = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Border definition of the liquid [0 - 1]. Default 0.5.
    /// </summary>
    public float Definition
    {
      get { return definition; }
      set { definition = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Brightness of the liquid. [0 - 10]. Default 3.
    /// </summary>
    public float DamageBrightness
    {
      get { return damageBrightness; }
      set { damageBrightness = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Distortion of the liquid in the background [0 - 1]. Default 0.2.
    /// </summary>
    public float Distortion
    {
      get { return distortion; }
      set { distortion = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Blend operation with the background. Default 'Darken'.
    /// </summary>
    public VisionBlendOps BlendOp
    {
      get { return blendOp; }
      set { blendOp = value; }
    }

    /// <summary>
    /// Liquid color. Default 'red'.
    /// </summary>
    public Color Color
    {
      get { return color; }
      set { color = value; }
    }

    [SerializeField]
    private float damage = 0.7f;

    [SerializeField]
    private float definition = 0.5f;

    [SerializeField]
    private float damageBrightness = 3.0f;

    [SerializeField]
    private float distortion = 0.2f;

    [SerializeField]
    private Color color = Color.red;

    [SerializeField]
    private VisionBlendOps blendOp = VisionBlendOps.Darken;

    private Texture2D textureBlood;

    private readonly int variableDamage = Shader.PropertyToID("_Damage");
    private readonly int variableDefinition = Shader.PropertyToID("_Definition");
    private readonly int variableDistortion = Shader.PropertyToID("_Distortion");
    private readonly int variableBrightness = Shader.PropertyToID("_DamageBrightness");
    private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");
    private readonly int variableColor = Shader.PropertyToID("_Color");

    private readonly int variableBloodTex = Shader.PropertyToID("_BloodTex");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "A very visual way of communicating the damage the player has suffered.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      damage = 0.7f;
      definition = 0.5f;
      distortion = 0.2f;
      damageBrightness = 3.0f;
      blendOp = VisionBlendOps.Darken;
      color = Color.red;
    }

    /// <summary>
    /// Load custom resources.
    /// </summary>
    protected override bool LoadCustomResources()
    {
      textureBlood = Resources.Load<Texture2D>("Textures/Blood");

      return textureBlood != null;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetFloat(variableDamage, damage);
      material.SetFloat(variableBrightness, damageBrightness);
      material.SetFloat(variableDefinition, definition * 15.0f);
      material.SetFloat(variableDistortion, distortion * 0.2f);
      material.SetColor(variableColor, color);
      material.SetInt(variableBlendOp, (int)blendOp);

      material.SetTexture(variableBloodTex, textureBlood);
    }
  }
}

