///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Trippy Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Trippy vision")]
  public sealed class TrippyVision : BaseVision
  {
    /// <summary>
    /// Effect speed [0 - 10]. Default 0.5.
    /// </summary>
    public float Speed
    {
      get { return speed; }
      set { speed = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Definition of contours [0 - 20]. Default 2.5.
    /// </summary>
    public float Definition
    {
      get { return definition; }
      set { definition = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Chromatic deformation of the background [0 - 2]. Default 0.2.
    /// </summary>
    public float Displacement
    {
      get { return displacement; }
      set { displacement = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Blend operation with the background. Default 'Lighten'.
    /// </summary>
    public VisionBlendOps BlendOp
    {
      get { return blendOp; }
      set { blendOp = value; }
    }

    [SerializeField]
    private float speed = 0.5f;

    [SerializeField]
    private float definition = 2.5f;

    [SerializeField]
    private float displacement = 0.2f;

    [SerializeField]
    private VisionBlendOps blendOp = VisionBlendOps.Lighten;

    private readonly int variableSpeed = Shader.PropertyToID("_Speed");
    private readonly int variableDefinition = Shader.PropertyToID("_Definition");
    private readonly int variableDisplacement = Shader.PropertyToID("_Displacement");
    private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "When the player abuses hallucinogenic substances.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      speed = 0.5f;
      definition = 2.5f;
      displacement = 0.2f;
      blendOp = VisionBlendOps.Lighten;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetFloat(variableSpeed, speed);
      material.SetFloat(variableDefinition, definition);
      material.SetFloat(variableDisplacement, displacement * 0.1f);
      material.SetInt(variableBlendOp, (int)blendOp);
    }
  }
}

