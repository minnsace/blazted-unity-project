///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Blurry Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Blurry vision")]
  public sealed class BlurryVision : BaseVision
  {
    /// <summary>
    /// Number of frames used. The more you use, the worse the performance [0 - 10]. Default 5.
    /// </summary>
    public int Frames
    {
      get { return frameCount; }
      set { frameCount = value > 0 ? (value < MaxFrames ? value : MaxFrames) : 0; }
    }

    /// <summary>
    /// Step in the use of frames [0 - 10]. Default 0.
    /// </summary>
    public int FrameStep
    {
      get { return frameStep; }
      set { frameStep = value > 0 ? value : 0; }
    }

    /// <summary>
    /// Resolution of the frames used, if it is 1 the original size will be used [0.1 - 1]. Default 1.
    /// </summary>
    public float FrameResolution
    {
      get { return frameResolution; }
      set { frameResolution = Mathf.Clamp(value, 0.1f, 1.0f); }
    }

    [SerializeField]
    private int frameCount = 5;

    [SerializeField]
    private int frameStep = 0;

    [SerializeField]
    private float frameResolution = 1.0f;

    private List<RenderTexture> frames = new List<RenderTexture>();

    private int lastFrameAdded = -1;

    public static readonly int MaxFrames = 10;

    private readonly int variableFrameCount = Shader.PropertyToID("_FrameCount");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Blurry vision.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      frameCount = 5;
      frameStep = 0;
      frameResolution = 1.0f;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetInt(variableFrameCount, frameCount);

      for (int i = 0; i < frameCount && i < frames.Count; ++i)
        material.SetTexture($"_RT{i}", frames[i]);
    }

    /// <summary>
    /// Custom OnRenderImage.
    /// </summary>
    protected override void OnRenderImageCustom(RenderTexture source, RenderTexture destination)
    {
      if (frames.Count > 0)
      {
        if (lastFrameAdded != Time.frameCount)
        {
          if (frameStep == 0 || (Time.frameCount - lastFrameAdded >= frameStep))
            AddFrame(source);
        }
      }
      else
      {
        lastFrameAdded = Time.frameCount;

        for (int i = 0; i < MaxFrames; ++i)
        {
          RenderTexture renderTexture = RenderTexture.GetTemporary((int)(Screen.width * frameResolution), (int)(Screen.height * frameResolution), 0);
          Graphics.Blit(source, renderTexture);
          frames.Insert(0, renderTexture);
        }
      }
    }

    private void AddFrame(RenderTexture source)
    {
      lastFrameAdded = Time.frameCount;

      RenderTexture renderTexture = RenderTexture.GetTemporary((int)(Screen.width * frameResolution), (int)(Screen.height * frameResolution), 0);
      Graphics.Blit(source, renderTexture);
      frames.Insert(0, renderTexture);

      if (frames.Count >= MaxFrames)
      {
        RenderTexture.ReleaseTemporary(frames[frames.Count - 1]);
        frames.RemoveAt(frames.Count - 1);
      }
    }

    private void OnDisable()
    {
      for (int i = 0; i < frames.Count; ++i)
        RenderTexture.ReleaseTemporary(frames[i]);

      frames.Clear();
    }
  }
}

