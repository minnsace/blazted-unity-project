///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Scanner Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Scanner vision")]
  public sealed class ScannerVision : BaseVision
  {
    /// <summary>
    /// Tint.
    /// </summary>
    public Color Tint
    {
      get { return tint; }
      set { tint = value; }
    }

    /// <summary>
    /// Lines count [0 - 1000]. Default 700.
    /// </summary>
    public int LinesCount
    {
      get { return linesCount; }
      set { linesCount = value > 0 ? value : 0; }
    }

    /// <summary>
    /// Lines strength [0 - 1]. Default 0.1.
    /// </summary>
    public float LinesStrength
    {
      get { return linesStrength; }
      set { linesStrength = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Scan line strength [0 - 1]. Default 0.5.
    /// </summary>
    public float ScanLineStrength
    {
      get { return scanlineStrength; }
      set { scanlineStrength = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Scan line wigth [0 - 5]. Default 0.2.
    /// </summary>
    public float ScanLineWidth
    {
      get { return scanlineWidth; }
      set { scanlineWidth = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Scan line speed [-5 - 5]. Default 0.5.
    /// </summary>
    public float ScanLineSpeed
    {
      get { return scanLineSpeed; }
      set { scanLineSpeed = value; }
    }

    /// <summary>
    /// Noise band strength [0 - 1]. Default 0.2.
    /// </summary>
    public float NoiseBandStrength
    {
      get { return noiseBandStrength; }
      set { noiseBandStrength = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Noise band wigth [0 - 5]. Default 0.2.
    /// </summary>
    public float NoiseBandWidth
    {
      get { return noiseBandWidth; }
      set { noiseBandWidth = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Noise band speed [-5 - 5]. Default 0.2.
    /// </summary>
    public float NoiseBandSpeed
    {
      get { return noiseBandSpeed; }
      set { noiseBandSpeed = value; }
    }

    [SerializeField]
    private Color tint = DefaultTint;

    [SerializeField]
    private int linesCount = 700;

    [SerializeField]
    private float linesStrength = 0.1f;

    [SerializeField]
    private float scanlineStrength = 0.5f;

    [SerializeField]
    private float scanlineWidth = 0.2f;

    [SerializeField]
    private float scanLineSpeed = 0.5f;

    [SerializeField]
    private float noiseBandStrength = 0.2f;

    [SerializeField]
    private float noiseBandWidth = 0.2f;

    [SerializeField]
    private float noiseBandSpeed = 0.2f;

    public static readonly  Color DefaultTint = new Color(0.5f, 1.0f, 0.5f);

    private readonly int variableTint = Shader.PropertyToID("_Tint");
    private readonly int variableLinesCount = Shader.PropertyToID("_LinesCount");
    private readonly int variableLinesStrength = Shader.PropertyToID("_LinesStrength");
    private readonly int variableScanLineStrength = Shader.PropertyToID("_ScanlineStrength");
    private readonly int variableScanLineWidth = Shader.PropertyToID("_ScanlineWidth");
    private readonly int variableScanLineSpeed = Shader.PropertyToID("_ScanlineSpeed");
    private readonly int variableNoiseBandStrength = Shader.PropertyToID("_NoiseBandStrength");
    private readonly int variableNoiseBandWidth = Shader.PropertyToID("_NoiseBandWidth");
    private readonly int variableNoiseBandSpeed = Shader.PropertyToID("_NoiseBandSpeed");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Scanner vision.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      tint = DefaultTint;
      linesCount = 700;
      linesStrength = 0.1f;
      scanlineStrength = 0.5f;
      scanlineWidth = 0.2f;
      scanLineSpeed = 0.5f;
      noiseBandStrength = 0.2f;
      noiseBandWidth = 0.2f;
      noiseBandSpeed = 0.2f;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetColor(variableTint, tint);

      material.SetFloat(variableLinesCount, linesCount);
      material.SetFloat(variableLinesStrength, linesStrength);

      material.SetFloat(variableScanLineStrength, scanlineStrength);
      material.SetFloat(variableScanLineWidth, scanlineWidth * 0.01f);
      material.SetFloat(variableScanLineSpeed, scanLineSpeed);

      material.SetFloat(variableNoiseBandStrength, noiseBandStrength);
      material.SetFloat(variableNoiseBandWidth, noiseBandWidth);
      material.SetFloat(variableNoiseBandSpeed, noiseBandSpeed);
    }
  }
}

