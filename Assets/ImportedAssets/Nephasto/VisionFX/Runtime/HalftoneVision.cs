///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Halftone Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Halftone vision")]
  public sealed class HalftoneVision : BaseVision
  {
    /// <summary>
    /// Point size [0 - 5]. Default 2.
    /// </summary>
    public float Size
    {
      get { return size; }
      set { size = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Angle of the pattern [0 - 180]. Default 15.
    /// </summary>
    public float Angle
    {
      get { return angle; }
      set { angle = Mathf.Clamp(value, 0.0f, 180.0f); }
    }

    /// <summary>
    /// Pattern strength [0 - 50]. Default 10.
    /// </summary>
    public float Strength
    {
      get { return strength; }
      set { strength = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Sensitivity used to create the pattern [0 - 10]. Default 8.5.
    /// </summary>
    public float Sensitivity
    {
      get { return sensitivity; }
      set { sensitivity = Mathf.Clamp(value, 0.0f, 10.0f); }
    }

    /// <summary>
    /// Blend operation with the background. Default 'Color'.
    /// </summary>
    public VisionBlendOps BlendOp
    {
      get { return blendOp; }
      set { blendOp = value; }
    }

    [SerializeField]
    private float size = 2.0f;

    [SerializeField]
    private float angle = 15.0f;

    [SerializeField]
    private float strength = 10.0f;

    [SerializeField]
    private float sensitivity = 8.5f;

    [SerializeField]
    private VisionBlendOps blendOp = VisionBlendOps.Color;

    private readonly int variableSize = Shader.PropertyToID("_Size");
    private readonly int variableAngle = Shader.PropertyToID("_Angle");
    private readonly int variableStrength = Shader.PropertyToID("_Strength");
    private readonly int variableSensitivity = Shader.PropertyToID("_Sensitivity");
    private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Imitates the reprographic technique for printing newspapers and comics.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      size = 2.0f;
      angle = 15.0f;
      strength = 10.0f;
      sensitivity = 8.5f;
      blendOp = VisionBlendOps.Color;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetFloat(variableSize, size * 100.0f);
      material.SetFloat(variableAngle, -angle * Mathf.Deg2Rad);
      material.SetFloat(variableStrength, strength);
      material.SetFloat(variableSensitivity, 10.0f - sensitivity);
      material.SetInt(variableBlendOp, (int)blendOp);
    }
  }
}

