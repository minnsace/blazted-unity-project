///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Base vision.
  /// </summary>
  [HelpURL(Documentation)]
  public abstract class BaseVision : MonoBehaviour
  {
    /// <summary>
    /// Strength of the effect [0, 1]. Default 1.
    /// </summary>
    public float Amount
    {
      get { return amount; }
      set { amount = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Enable color controls (Brightness, Contrast, Gamma, Hue and Saturation).
    /// </summary>
    public bool EnableColorControls
    {
      get { return enableColorControls; }
      set { enableColorControls = value; }
    }

    /// <summary>
    /// Brightness [-1.0, 1.0]. Default 0.
    /// </summary>
    public float Brightness
    {
      get { return brightness; }
      set { brightness = Mathf.Clamp(value, -1.0f, 1.0f); }
    }

    /// <summary>
    /// Contrast [-1.0, 1.0]. Default 0.
    /// </summary>
    public float Contrast
    {
      get { return contrast; }
      set { contrast = Mathf.Clamp(value, -1.0f, 1.0f); }
    }

    /// <summary>
    /// Gamma [0.1, 10.0]. Default 1.
    /// </summary>
    public float Gamma
    {
      get { return gamma; }
      set { gamma = Mathf.Clamp(value, 0.1f, 10.0f); }
    }

    /// <summary>
    /// The color wheel [0.0, 1.0]. Default 0.
    /// </summary>
    public float Hue
    {
      get { return hue; }
      set { hue = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Intensity of a colors [0.0, 2.0]. Default 1.
    /// </summary>
    public float Saturation
    {
      get { return saturation; }
      set { saturation = Mathf.Clamp(value, 0.0f, 2.0f); }
    }

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "No description.";

    public const string Documentation = "https://www.nephasto.com/store/vision-fx.html";

    [SerializeField]
    private float amount = 1.0f;

    [SerializeField]
    protected bool enableColorControls = false;

    [SerializeField]
    private float brightness = 0.0f;

    [SerializeField]
    private float contrast = 0.0f;

    [SerializeField]
    private float gamma = 1.0f;

    [SerializeField]
    private float hue = 0.0f;

    [SerializeField]
    private float saturation = 1.0f;

    private Shader shader;

    protected Material material;

    private readonly int variableAmount = Shader.PropertyToID("_Amount");

    private readonly int variableBrightness = Shader.PropertyToID("_Brightness");
    private readonly int variableContrast = Shader.PropertyToID("_Contrast");
    private readonly int variableGamma = Shader.PropertyToID("_Gamma");
    private readonly int variableHue = Shader.PropertyToID("_Hue");
    private readonly int variableSaturation = Shader.PropertyToID("_Saturation");

    private const string keywordColorControls = "COLOR_CONTROLS";

    protected virtual string ShaderPath() => $"Shaders/{this.GetType().Name}";

    /// <summary>
    /// Effect supported by the current hardware?
    /// </summary>
    public bool IsSupported()
    {
      bool supported = false;

      string shaderPath = ShaderPath();

      Shader test = Resources.Load<Shader>(shaderPath);
      if (test != null)
      {
        supported = test.isSupported == true;

        Resources.UnloadAsset(test);
      }

      return supported;
    }

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public virtual void ResetDefaultValues()
    {
      amount = 1.0f;
    }

    /// <summary>
    /// Load custom resources.
    /// </summary>
    protected virtual bool LoadCustomResources() { return true; }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected virtual void UpdateCustomValues() { }

    /// <summary>
    /// Custom OnRenderImage.
    /// </summary>
    protected virtual void OnRenderImageCustom(RenderTexture source, RenderTexture destination) { }

    /// <summary>
    /// Called on the frame when a script is enabled just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
      string shaderPath = ShaderPath();

      shader = Resources.Load<Shader>(shaderPath);
      if (shader != null)
      {
        if (shader.isSupported == true)
        {
          string materialName = this.GetType().Name;

          material = new Material(shader);
          if (material != null)
          {
            material.name = materialName;
            material.hideFlags = HideFlags.HideAndDontSave;

            if (LoadCustomResources() == false)
            {
              Debug.LogError($"[Nephasto.VisionFX] '{materialName}' error loading custom resources. Please contact with 'hello@nephasto.com' and send the log file.");

              this.enabled = false;
            }
          }
          else
          {
            Debug.LogError($"[Nephasto.VisionFX] '{materialName}' material null. Please contact with 'hello@nephasto.com' and send the log file.");

            this.enabled = false;
          }
        }
        else
        {
          Debug.LogWarning($"[Nephasto.VisionFX] '{shaderPath}' shader not supported. Please contact with 'hello@nephasto.com' and send the log file.");

          this.enabled = false;
        }
      }
      else
      {
        Debug.LogWarning($"[Nephasto.VisionFX] Shader 'Nephasto/VisionFX/Resources/{shaderPath}.shader' not found. '{this.GetType().Name}' disabled.");

        this.enabled = false;
      }
    }

    /// <summary>
    /// When the MonoBehaviour will be destroyed.
    /// </summary>
    private void OnDestroy()
    {
      if (material != null)
#if UNITY_EDITOR
        DestroyImmediate(material);
#else
        Destroy(material);
#endif
    }

    /// <summary>
    /// Called after all rendering is complete to render image.
    /// </summary>
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
      if (material != null && amount > 0.0f)
      {
        material.shaderKeywords = null;

        OnRenderImageCustom(source, destination);

        material.SetFloat(variableAmount, amount);

        if (enableColorControls == true && amount > 0.0f)
        {
          material.EnableKeyword(keywordColorControls);

          material.SetFloat(variableBrightness, brightness);
          material.SetFloat(variableContrast, contrast);
          material.SetFloat(variableGamma, 1.0f / gamma);
          material.SetFloat(variableHue, hue);
          material.SetFloat(variableSaturation, saturation);
        }

        UpdateCustomValues();

        Graphics.Blit(source, destination, material, 0);
      }
      else
        Graphics.Blit(source, destination);
    }

    public void UpdateAmount(float strength)
    {
        material.SetFloat(variableAmount, (amount + strength));
    }

  }
}

