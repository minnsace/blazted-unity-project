///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Anime Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Anime vision")]
  public sealed class AnimeVision : BaseVision
  {
    /// <summary>
    /// Respect or not the aspect ratio. Default 'false'.
    /// </summary>
    public bool Aspect
    {
      get { return aspect; }
      set { aspect = value; }
    }

    /// <summary>
    /// Effect radius [0 - 2]. Default 1.25.
    /// </summary>
    public float Radius
    {
      get { return radius; }
      set { radius = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Length of the lines [0 - 5]. Default 2.
    /// </summary>
    public float Length
    {
      get { return length; }
      set { length = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Speed in the movement of the lines [0 - 2]. Default 1.
    /// </summary>
    public float Speed
    {
      get { return speed; }
      set { speed = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Edge of the lines [0 - 50]. Default 10.
    /// </summary>
    public float Frequency
    {
      get { return frequency; }
      set { frequency = Mathf.Max(value, 0.0f); }
    }

    /// <summary>
    /// Smoothness of the lines [0 - 1]. Default 0.1.
    /// </summary>
    public float Softness
    {
      get { return softness; }
      set { softness = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Geometry of the shape of the lines [0 - 1]. Default 0.1.
    /// </summary>
    public float Noise
    {
      get { return noise; }
      set { noise = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Color of the lines. Use the alpha channel to define its transparency. Default 'white'.
    /// </summary>
    public Color Color
    {
      get { return color; }
      set { color = value; }
    }

    [SerializeField]
    private bool aspect = false;

    [SerializeField]
    private float radius = 1.25f;

    [SerializeField]
    private float length = 2.0f;

    [SerializeField]
    private float speed = 1.0f;

    [SerializeField]
    private float frequency = 10.0f;

    [SerializeField]
    private float softness = 0.1f;

    [SerializeField]
    private float noise = 0.1f;

    [SerializeField]
    private Color color = Color.white;

    private readonly int variableAspect = Shader.PropertyToID("_Aspect");
    private readonly int variableRadius = Shader.PropertyToID("_Radius");
    private readonly int variableLength = Shader.PropertyToID("_Length");
    private readonly int variableSpeed = Shader.PropertyToID("_Speed");
    private readonly int variableFrequency = Shader.PropertyToID("_Frequency");
    private readonly int variableSoftness = Shader.PropertyToID("_Softness");
    private readonly int variableNoise = Shader.PropertyToID("_Noise");
    private readonly int variableColor = Shader.PropertyToID("_Color");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Classic anime effect to accentuate speed.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      aspect = false;
      radius = 1.25f;
      length = 2.0f;
      speed = 1.0f;
      frequency = 10.0f;
      softness = 0.1f;
      noise = 0.1f;
      color = Color.white;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetInt(variableAspect, aspect == true ? 1 : 0);
      material.SetFloat(variableRadius, radius);
      material.SetFloat(variableLength, length);
      material.SetFloat(variableSpeed, speed * 5.0f);
      material.SetFloat(variableFrequency, frequency);
      material.SetFloat(variableSoftness, softness);
      material.SetFloat(variableNoise, noise);
      material.SetColor(variableColor, color);
    }
  }
}

