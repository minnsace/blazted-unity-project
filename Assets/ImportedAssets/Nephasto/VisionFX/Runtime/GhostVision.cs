///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Ghost Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Ghost vision")]
  public sealed class GhostVision : BaseVision
  {
    /// <summary>
    /// Focus point, (-1, -1) to (1, 1). Default (0, 0)
    /// </summary>
    public Vector2 Focus
    {
      get { return focus; }
      set { focus = value; }
    }

    /// <summary>
    /// Speed of turns [0 - 2]. Default 1.
    /// </summary>
    public float Speed
    {
      get { return speed; }
      set { speed = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Focus aperture [0 - 10]. Default 1.
    /// </summary>
    public float Aperture
    {
      get { return aperture; }
      set { aperture = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Zoom aperture [0 - 2]. Default 1.
    /// </summary>
    public float Zoom
    {
      get { return zoom; }
      set { zoom = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Change the FOV?
    /// </summary>
    public bool ChangeFOV
    {
      get { return changeFOV; }
      set { changeFOV = value; }
    }

    /// <summary>
    /// Camera FOV [1 - 179]. Default 60.
    /// </summary>
    public float FOV
    {
      get { return fov; }
      set { fov = Mathf.Clamp(value, 1.0f, 179.0f); }
    }

    /// <summary>
    /// Aspect ratio?.
    /// </summary>
    public bool AspectRatio
    {
      get { return aspectRatio; }
      set { aspectRatio = value; }
    }

    /// <summary>
    /// Inner tint.
    /// </summary>
    public Color InnerTint
    {
      get { return innerTint; }
      set { innerTint = value; }
    }

    /// <summary>
    /// Color saturation [0.0, 1.0]. Default 1.
    /// </summary>
    public float InnerSaturation
    {
      get { return innerSaturation; }
      set { innerSaturation = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Brightness [-1.0, 1.0]. Default 0.
    /// </summary>
    public float InnerBrightness
    {
      get { return innerBrightness; }
      set { innerBrightness = Mathf.Clamp(value, -1.0f, 1.0f); }
    }

    /// <summary>
    /// Contrast [-1.0, 1.0). Default 0.
    /// </summary>
    public float InnerContrast
    {
      get { return innerContrast; }
      set { innerContrast = Mathf.Clamp(value, -1.0f, 1.0f); }
    }

    /// <summary>
    /// Gamma [0.1, 5.0]. Default 1.
    /// </summary>
    public float InnerGamma
    {
      get { return innerGamma; }
      set { innerGamma = Mathf.Clamp(value, 0.1f, 5.0f); }
    }

    /// <summary>
    /// Outer tint.
    /// </summary>
    public Color OuterTint
    {
      get { return outerTint; }
      set { outerTint = value; }
    }

    /// <summary>
    /// Color saturation [0.0, 1.0]. Default 1.
    /// </summary>
    public float OuterSaturation
    {
      get { return outerSaturation; }
      set { outerSaturation = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Brightness [-1.0, 1.0]. Default 0.
    /// </summary>
    public float OuterBrightness
    {
      get { return outerBrightness; }
      set { outerBrightness = Mathf.Clamp(value, -1.0f, 1.0f); }
    }

    /// <summary>
    /// Contrast [-1.0, 1.0). Default 0.
    /// </summary>
    public float OuterContrast
    {
      get { return outerContrast; }
      set { outerContrast = Mathf.Clamp(value, -1.0f, 1.0f); }
    }

    /// <summary>
    /// Gamma [0.1, 5.0]. Default 1.
    /// </summary>
    public float OuterGamma
    {
      get { return outerGamma; }
      set { outerGamma = Mathf.Clamp(value, 0.1f, 5.0f); }
    }

    [SerializeField]
    private Vector2 focus = Vector2.zero;

    [SerializeField]
    private float aperture = 1.0f;

    [SerializeField]
    private float zoom = 1.0f;

    [SerializeField]
    private bool changeFOV = false;

    [SerializeField]
    private float fov = 60.0f;

    [SerializeField]
    private float speed = 0.5f;

    [SerializeField]
    private bool aspectRatio = true;

    [SerializeField]
    private Color innerTint = Color.white;

    [SerializeField]
    private float innerSaturation = 1.0f;

    [SerializeField]
    private float innerBrightness = 0.0f;

    [SerializeField]
    private float innerContrast = 0.0f;

    [SerializeField]
    private float innerGamma = 1.0f;

    [SerializeField]
    private Color outerTint = Color.white;

    [SerializeField]
    private float outerSaturation = 1.0f;

    [SerializeField]
    private float outerBrightness = 0.0f;

    [SerializeField]
    private float outerContrast = 0.0f;

    [SerializeField]
    private float outerGamma = 1.0f;

    private Camera cam;

    private float originalFOV = 60.0f;

    private readonly int variableStrength = Shader.PropertyToID("_Strength");
    private readonly int variableFocus = Shader.PropertyToID("_Focus");
    private readonly int variableSpeed = Shader.PropertyToID("_Speed");
    private readonly int variableAperture = Shader.PropertyToID("_Aperture");
    private readonly int variableZoom = Shader.PropertyToID("_Zoom");
    private readonly int variableInnerColor = Shader.PropertyToID("_InnerColor");
    private readonly int variableInnerSaturation = Shader.PropertyToID("_InnerSaturation");
    private readonly int variableInnerBrightness = Shader.PropertyToID("_InnerBrightness");
    private readonly int variableInnerContrast = Shader.PropertyToID("_InnerContrast");
    private readonly int variableInnerGamma = Shader.PropertyToID("_InnerGamma");
    private readonly int variableOuterColor = Shader.PropertyToID("_OuterColor");
    private readonly int variableOuterSaturation = Shader.PropertyToID("_OuterSaturation");
    private readonly int variableOuterBrightness = Shader.PropertyToID("_OuterBrightness");
    private readonly int variableOuterContrast = Shader.PropertyToID("_OuterContrast");
    private readonly int variableOuterGamma = Shader.PropertyToID("_OuterGamma");

    private const string keywordAspectRatio = "ASPECT_RATIO";

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Mimics the first person view of strange creatures like aliens or ghosts.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      focus = Vector2.zero;
      speed = 0.5f;
      aperture = 1.0f;
      zoom = 1.0f;
      fov = 60.0f;
      aspectRatio = true;

      innerTint = Color.white;
      innerSaturation = 1.0f;
      innerBrightness = 0.0f;
      innerContrast = 0.0f;
      innerGamma = 1.0f;

      outerTint = Color.white;
      outerSaturation = 1.0f;
      outerBrightness = 0.0f;
      outerContrast = 0.0f;
      outerGamma = 1.0f;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      if (cam != null && changeFOV == true && Application.isPlaying == true)
        cam.fieldOfView = Mathf.Lerp(originalFOV, fov, Amount);

      material.SetVector(variableFocus, focus + Vector2.one);
      material.SetFloat(variableSpeed, speed);
      material.SetFloat(variableAperture, aperture);
      material.SetFloat(variableZoom, zoom + 0.05f);

      if (aspectRatio == true)
        material.EnableKeyword(keywordAspectRatio);

      material.SetColor(variableInnerColor, innerTint);
      material.SetFloat(variableInnerSaturation, innerSaturation);
      material.SetFloat(variableInnerBrightness, innerBrightness);
      material.SetFloat(variableInnerContrast, innerContrast + 1.0f);
      material.SetFloat(variableInnerGamma, 1.0f / innerGamma);

      material.SetColor(variableOuterColor, outerTint);
      material.SetFloat(variableOuterSaturation, outerSaturation);
      material.SetFloat(variableOuterBrightness, outerBrightness);
      material.SetFloat(variableOuterContrast, outerContrast + 1.0f);
      material.SetFloat(variableOuterGamma, 1.0f / outerGamma);
    }

    private void OnEnable()
    {
      cam = this.gameObject.GetComponent<Camera>();
      if (cam != null)
        originalFOV = cam.fieldOfView;
    }

    private void OnDisable()
    {
      if (cam != null && changeFOV == true)
        cam.fieldOfView = originalFOV;
    }
  }
}

