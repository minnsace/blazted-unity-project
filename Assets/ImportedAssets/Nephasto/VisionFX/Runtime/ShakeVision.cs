///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Shake Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Shake vision")]
  public sealed class ShakeVision : BaseVision
  {
    /// <summary>
    /// Effect offset [0 - 2]. Default 0.1.
    /// </summary>
    public float Magnitude
    {
      get { return magnitude; }
      set { magnitude = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Shake strength [0 - 10]. Default 1.
    /// </summary>
    public float Intensity
    {
      get { return intensity; }
      set { intensity = Mathf.Max(0.0f, value); }
    }

    [SerializeField]
    private float magnitude = 0.1f;

    [SerializeField]
    private float intensity = 1.0f;

    private readonly int variableMagnitude = Shader.PropertyToID("_Magnitude");
    private readonly int variableIntensity = Shader.PropertyToID("_Intensity");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Shakes on the screen.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      magnitude = 0.1f;
      intensity = 1.0f;     
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetFloat(variableMagnitude, magnitude);
      material.SetFloat(variableIntensity, intensity);
    }
  }
}

