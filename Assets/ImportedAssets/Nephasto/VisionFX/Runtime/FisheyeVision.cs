///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Fisheye Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Fisheye vision")]
  public sealed class FisheyeVision : BaseVision
  {
    /// <summary>
    /// Deformation [-1 - 1]. Default 0.
    /// </summary>
    public float Barrel
    {
      get { return barrel; }
      set { barrel = Mathf.Clamp(value, -1.0f, 1.0f); }
    }

    /// <summary>
    /// Center. Default (0.5, 0.5).
    /// </summary>
    public Vector2 Center
    {
      get { return center; }
      set { center = value; }
    }

    /// <summary>
    /// Background color. Default 'black'.
    /// </summary>
    public Color BackgroundColor
    {
      get { return backgroundColor; }
      set { backgroundColor = value; }
    }

    [SerializeField]
    private float barrel = 0.0f;

    [SerializeField]
    private Vector2 center = DefaultCenter;

    [SerializeField]
    private Color backgroundColor = Color.black;

    public static Vector2 DefaultCenter = new Vector2(0.5f, 0.5f);

    private readonly int variableBarrel = Shader.PropertyToID("_Barrel");
    private readonly int variableCenter = Shader.PropertyToID("_Center");
    private readonly int variableBackgroundColor = Shader.PropertyToID("_BackgroundColor");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Fisheye / anti-fisheye deformation.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      barrel = 0.0f;
      center = DefaultCenter;
      backgroundColor = Color.black;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetFloat(variableBarrel, barrel + 0.5f);
      material.SetVector(variableCenter, center);
      material.SetColor(variableBackgroundColor, backgroundColor);
    }
  }
}

