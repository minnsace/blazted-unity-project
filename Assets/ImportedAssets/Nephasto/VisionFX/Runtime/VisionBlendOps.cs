﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Color blend operations https://en.wikipedia.org/wiki/Blend_modes.
  /// </summary>
  public enum VisionBlendOps
  {
    Additive,
    Burn,
    Color,
    Darken,
    Darker,
    Difference,
    Divide,
    Dodge,
    HardMix,
    Hue,
    HardLight,
    Lighten,
    Lighter,
    Multiply,
    Overlay,
    Screen,
    Solid,
    SoftLight,
    PinLight,
    Saturation,
    Subtract,
    VividLight,
    Luminosity,
  }
}
