///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Drunk vision.
  /// </summary>
  [ExecuteAlways]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Drunk Vision")]
  public sealed class DrunkVision : BaseVision
  {
    /// <summary>
    /// Drunkenness level. Warning: values above 0.3 can cause dizziness in the player [0.0 - 1.0]. Default 0.1.
    /// </summary>
    public float Drunkenness
    {
      get { return drunkenness; }
      set { drunkenness = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Speed of the oscillations [0.0 - 10.0]. Default 1.
    /// </summary>
    public float DrunkSpeed
    {
      get { return drunkSpeed; }
      set { drunkSpeed = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Amplitude of the oscillations [0.0 - 1.0]. Default 0.75.
    /// </summary>
    public float DrunkAmplitude
    {
      get { return drunkAmplitude; }
      set { drunkAmplitude = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Head swing [0.0 - 1.0]. Default 0.25.
    /// </summary>
    public float Swinging
    {
      get { return swinging; }
      set { swinging = Mathf.Clamp01(value); }
    }

    /// <summary>
    /// Head swing speed [0.0 - 10.0]. Default 2.0.
    /// </summary>
    public float SwingingSpeed
    {
      get { return swingingSpeed; }
      set { swingingSpeed = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Chromatic aberration. [0.0 - 10.0]. Default 1.
    /// </summary>
    public float Aberration
    {
      get { return aberration; }
      set { aberration = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Chromatic aberration speed [0.0 - 10.0]. Default 1.0.
    /// </summary>
    public float AberrationSpeed
    {
      get { return aberrationSpeed; }
      set { aberrationSpeed = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Blink strength [0.0 - 10.0]. Default 0.75.
    /// </summary>
    public float VignetteAmount
    {
      get { return vignetteAmount; }
      set { vignetteAmount = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Blink speed [0.0 - 10.0]. Default 1.
    /// </summary>
    public float VignetteSpeed
    {
      get { return vignetteSpeed; }
      set { vignetteSpeed = Mathf.Max(0.0f, value); }
    }

    [SerializeField]
    private float drunkenness = 0.1f;

    [SerializeField]
    private float drunkSpeed = 1.0f;

    [SerializeField]
    private float drunkAmplitude = 0.75f;

    [SerializeField]
    private float swinging = 0.25f;

    [SerializeField]
    private float swingingSpeed = 2.0f;

    [SerializeField]
    private float aberration = 1.0f;

    [SerializeField]
    private float aberrationSpeed = 1.0f;

    [SerializeField]
    private float vignetteSpeed = 1.0f;

    [SerializeField]
    private float vignetteAmount = 0.75f;

    private readonly int variableDrunkenness = Shader.PropertyToID("_Drunkenness");
    private readonly int variableDrunkSpeed = Shader.PropertyToID("_DrunkSpeed");
    private readonly int variableDrunkAmplitude = Shader.PropertyToID("_DrunkAmplitude");
    private readonly int variableSwinging = Shader.PropertyToID("_Swinging");
    private readonly int variableSwingingSpeed = Shader.PropertyToID("_SwingingSpeed");
    private readonly int variableAberration = Shader.PropertyToID("_Aberration");
    private readonly int variableAberrationSpeed = Shader.PropertyToID("_AberrationSpeed");
    private readonly int variableVignetteAmount = Shader.PropertyToID("_VignetteAmount");
    private readonly int variableVignetteSpeed = Shader.PropertyToID("_VignetteSpeed");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "You've had too much to drink, go home.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      drunkenness = 0.1f;
      drunkSpeed = 1.0f;
      drunkAmplitude = 0.75f;
      
      swinging = 0.25f;
      swingingSpeed = 2.0f;

      aberration = 1.0f;
      aberrationSpeed = 1.0f;

      vignetteAmount = 0.75f;
      vignetteSpeed = 1.0f;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetFloat(variableDrunkenness, 1.0f - drunkenness);
      material.SetFloat(variableDrunkSpeed, drunkSpeed);
      material.SetFloat(variableDrunkAmplitude, 0.3f * drunkAmplitude);
      
      material.SetFloat(variableSwinging, 0.01f * swinging);
      material.SetFloat(variableSwingingSpeed, swingingSpeed);

      material.SetFloat(variableAberration, aberration);
      material.SetFloat(variableAberrationSpeed, aberrationSpeed);

      material.SetFloat(variableVignetteAmount, vignetteAmount);
      material.SetFloat(variableVignetteSpeed, vignetteSpeed);
    }
  }
}
