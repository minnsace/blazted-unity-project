///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Neon Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Neon vision")]
  public sealed class NeonVision : BaseVision
  {
    /// <summary>
    /// Border size [0 - 10]. Default 2.
    /// </summary>
    public float Edge
    {
      get { return edge; }
      set { edge = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Border color. Default 'cyan'.
    /// </summary>
    public Color EdgeColor
    {
      get { return edgeColor; }
      set { edgeColor = value; }
    }

    /// <summary>
    /// Blend operation with the background. Default 'Additive'.
    /// </summary>
    public VisionBlendOps BlendOp
    {
      get { return blendOp; }
      set { blendOp = value; }
    }

    [SerializeField]
    private float edge = 2.0f;

    [SerializeField]
    private Color edgeColor = Color.cyan;

    [SerializeField]
    private VisionBlendOps blendOp = VisionBlendOps.Additive;

    private readonly int variableEdge = Shader.PropertyToID("_Edge");
    private readonly int variableEdgeColor = Shader.PropertyToID("_EdgeColor");
    private readonly int variableBlendOp = Shader.PropertyToID("_BlendOp");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Color the edges of the image.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      edge = 2.0f;
      edgeColor = Color.cyan;
      blendOp = VisionBlendOps.Additive;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetFloat(variableEdge, edge);
      material.SetColor(variableEdgeColor, edgeColor);
      material.SetInt(variableBlendOp, (int)blendOp);
    }
  }
}

