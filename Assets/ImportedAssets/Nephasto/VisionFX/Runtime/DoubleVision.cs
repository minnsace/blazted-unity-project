///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Martin Bustos Roman @Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// THIS SOURCE CODE CAN NOT BE HOSTED IN PUBLIC REPOSITORIES.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.VisionFXAsset
{
  /// <summary>
  /// Double Vision.
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Vision FX/Double vision")]
  public sealed class DoubleVision : BaseVision
  {
    /// <summary>
    /// Strength and direction of effect. Default (0.1, 0.0).
    /// </summary>
    public Vector2 Strength
    {
      get { return strength; }
      set { strength = value; }
    }

    /// <summary>
    /// Oscillation speed on each axis. Default (2.0, 0.0).
    /// </summary>
    public Vector2 Speed
    {
      get { return speed; }
      set { speed = value; }
    }

    [SerializeField]
    private Vector2 strength = DefaultStrength;

    [SerializeField]
    private Vector2 speed = DefaultSpeed;

    public static Vector2 DefaultStrength = new Vector2(0.1f, 0.0f);
    public static Vector2 DefaultSpeed = new Vector2(2.0f, 0.0f);

    private readonly int variableStrength = Shader.PropertyToID("_Strength");
    private readonly int variableSpeed = Shader.PropertyToID("_Speed");

    /// <summary>
    /// Effect description.
    /// </summary>
    public override string ToString() => "Double vision.";

    /// <summary>
    /// Reset to default values.
    /// </summary>
    public override void ResetDefaultValues()
    {
      base.ResetDefaultValues();

      strength = DefaultStrength;
      speed = DefaultSpeed;
    }

    /// <summary>
    /// Set the values to shader.
    /// </summary>
    protected override void UpdateCustomValues()
    {
      material.SetVector(variableStrength, strength * 0.1f);
      material.SetVector(variableSpeed, speed);
    }
  }
}

