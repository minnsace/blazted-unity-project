﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using EmeraldAI.CharacterController;
using UnityEngine.UI;

namespace EmeraldAI.Example
{
    /// <summary>
    /// An example health script that the EmeraldAIPlayerDamage script calls.
    /// Various events can be created and used to cause damage to a 3rd party character controllers via the inspector.
    /// You can also edit the EmeraldAIPlayerDamage script directly and add custom functions.
    /// </summary>
    public class EmeraldAIPlayerHealth : MonoBehaviour
    {
        public int CurrentHealth = 100; [Space]
        public int TotalHealth = 100;
        public int abv = 0;
        public UnityEvent DamageEvent;
        public UnityEvent DeathEvent;

        public GameObject player;
        public PlayerHealth playerHealth;

        public Text abvCounter;

        public Text hpCounter;

        [HideInInspector]
        public int StartingHealth;

        private void Start()
        {
            StartingHealth = CurrentHealth;

            player = GameObject.FindGameObjectWithTag("Player");
            playerHealth = player.GetComponent<PlayerHealth>();

            abvCounter.text = (abv + "%");
            abvCounter.color = (abv > (100 - 50) ? Color.red :
                                 (abv > (100 - 75) ? Color.yellow :
                                  Color.green));

            hpCounter.text = (CurrentHealth + "%");
            hpCounter.color = (CurrentHealth > (100 - 50) ? Color.green :
                                 (CurrentHealth > (100 - 75) ? Color.yellow :
                                  Color.red));

        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void DamagePlayer (int DamageAmount)
        {
            if ((abv - DamageAmount) >= 0)
            {
                abv = abv - DamageAmount;

                abvCounter.text = (abv + "%");
                abvCounter.color = (abv > (100 - 50) ? Color.red :
                                     (abv > (100 - 75) ? Color.yellow :
                                      Color.green));
            }
            else
            {
                CurrentHealth = CurrentHealth - (DamageAmount - abv);

                hpCounter.text = (CurrentHealth + "%");
                hpCounter.color = (CurrentHealth > (100 - 50) ? Color.green :
                                     (CurrentHealth > (100 - 75) ? Color.yellow :
                                      Color.red));
            }
            

            TotalHealth = abv + CurrentHealth;

            DamageEvent.Invoke();


            if (GetComponent<EmeraldAICharacterControllerTopDown>() != null)
            {
                GetComponent<EmeraldAICharacterControllerTopDown>().DamagePlayer(DamageAmount);
            }

            if (CurrentHealth <= 0)
            {
                PlayerDeath();
            }
        }

        public void HealPlayer(int HealAmount)
        {
            abv = abv + HealAmount;
            TotalHealth = abv + CurrentHealth;

            abvCounter.text = (abv + "%");
            abvCounter.color = (abv > (100 - 50) ? Color.red :
                                 (abv > (100 - 70) ? Color.yellow :
                                  Color.green));
        }

        public void ResetPlayer()
        {
            abv = 0;
            abvCounter.text = (abv + "%");
            abvCounter.color = (abv > (100 - 50) ? Color.red :
                                 (abv > (100 - 70) ? Color.yellow :
                                  Color.green));

            CurrentHealth = 100;
            hpCounter.text = (CurrentHealth + "%");
            hpCounter.color = (CurrentHealth > (100 - 50) ? Color.green :
                                 (CurrentHealth > (100 - 75) ? Color.yellow :
                                  Color.red));
        }

        public void PlayerDeath ()
        {
            DeathEvent.Invoke();
            SceneManager.LoadScene(0);
        }
    }
}
