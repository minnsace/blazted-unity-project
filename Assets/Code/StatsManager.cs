﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{
    public int currentCash = 0;
    public int earnedCash = 0;
    public int zombiesKilled = 0;

    public float abv = 0.0f;
    public Text abvCounter;

    public Text textCurrentCash;
    public Text textEarnedCash;
    public Text textZombiesKilled;

    // Start is called before the first frame update
    void Start()
    {
        Unity.XR.Oculus.Performance.TrySetDisplayRefreshRate(72f);
    }

    // Update is called once per frame
    void Update()
    {
        textCurrentCash.text = ("CURRENT CASH:$" + currentCash);
        textEarnedCash.text = ("CASH EARNED:$" + earnedCash);
        textZombiesKilled.text = ("ZOMBIES KILLED:" + zombiesKilled);
    }

    public void EarnMoney(int amount)
    {
        currentCash = currentCash + amount;
        earnedCash = earnedCash + amount;
    }

    public void SpendMoney(int amount)
    {
        if (currentCash >= amount)
        {
            currentCash = currentCash - amount;
        }
    }

    public void KillZombie(int amount)
    {
        zombiesKilled++;
        currentCash = currentCash + amount;
        earnedCash = earnedCash + amount;
    }

    public void ResetStats()
    {
        currentCash = 0;
        earnedCash = 0;
        zombiesKilled = 0;
    }

    public void SetABV(float drunkLevel)
    {
        abv = abv + drunkLevel;

    }

    public float GetABV()
    {
        return abv;
    }
}
