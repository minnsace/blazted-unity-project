﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EmeraldAI.Example;

public class PlayerHealth : MonoBehaviour
{
    public StatsManager sm;
    public GameObject manager;

    public EmeraldAIPlayerHealth health;

    public float abv = 0.0f;
    
    // Start is called before the first frame update
    void Awake()
    {
        manager = GameObject.FindGameObjectWithTag("Bartender");
        sm = manager.GetComponent<StatsManager>();

        health = gameObject.GetComponentInChildren<EmeraldAIPlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Damage(float dam)
    {
        
    }

    public void Heal(int strength)
    {
        abv = abv + strength;
    }

    public void Die()
    {
        SceneManager.LoadScene(0);
    }
}
