using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettingFix : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Camera>().forceIntoRenderTexture = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
