using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EmeraldAI;

public class DamageSystem : MonoBehaviour
{
    public EmeraldAIEventsManager EventsManager;

    public GameObject bartender;
    public StatsManager register;

    public GameObject spawner;
    public SpawnManager spawnManager;

    public int despawnTime = 6;

    public Collider headCollider;
    public Collider bodyCollider;
    public Collider leftArmCollider;
    public Collider leftHandCollider;
    public Collider rightArmCollider;
    public Collider rightHandCollider;

    // Start is called before the first frame update
    void Start()
    {
        EventsManager = GetComponent<EmeraldAIEventsManager>();

        bartender = GameObject.FindGameObjectWithTag("Bartender");
        register = bartender.GetComponent<StatsManager>();

        spawner = GameObject.FindGameObjectWithTag("SpawnManager");
        spawnManager = spawner.GetComponent<SpawnManager>();

        headCollider.enabled = true;
        bodyCollider.enabled = true;
        leftArmCollider.enabled = true;
        leftHandCollider.enabled = true;
        rightArmCollider.enabled = true;
        rightHandCollider.enabled = true;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Died()
    {
        register.KillZombie(10);
        spawnManager.ZombieDie();

        Destroy(gameObject, despawnTime);
    }

    public void ZombieHandDestroyed(string side)
    {
        if (side == "left")
        {
            EventsManager.UpdateAIMeleeDamage(1, 1, 2);
        }

        if (side == "right")
        {
            EventsManager.UpdateAIMeleeDamage(2, 1, 2);
        }
    }

    public void ZombieArmDestroyed(string side)
    {
        if (side == "left")
        {
            EventsManager.UpdateAIMeleeDamage(1, 0, 0);
        }

        if (side == "right")
        {
            EventsManager.UpdateAIMeleeDamage(2, 0, 0);
        }
    }
}
