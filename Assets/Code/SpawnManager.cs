﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EmeraldAI.Example;
using EmeraldAI.Utility;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour
{
    public Transform playerTransform;
    public GameObject playerObject;
    public PlayerHealth ph;
    public SoberUp su;
    public EmeraldAIPlayerHealth health;

    public Image startEndBtnImg;
    public Sprite openTabSprite;
    public Sprite closeTabSprite;

    public float respawnTime = 1.0f;
    public bool gameSarted = false;
    public float timer;

    public int dificulty = 5;

    public GameObject[] zombies;
    public Transform[] spawnPoints;

    public int zombieCount = 0;

    public GameObject bartender;
    public StatsManager register;

    void Start()
    {
        playerObject = GameObject.FindGameObjectWithTag("Player");
        ph = playerObject.GetComponent<PlayerHealth>();
        su = playerObject.GetComponent<SoberUp>();
        health = playerObject.GetComponentInChildren<EmeraldAIPlayerHealth>();
        playerTransform = playerObject.transform;
        bartender = GameObject.FindGameObjectWithTag("Bartender");
        register = bartender.GetComponent<StatsManager>();
    }

    void Update()
    {
        if (gameSarted && zombieCount < dificulty)
        {
            int zombieIndex = Random.Range(0, zombies.Length - 1);

            int spawnIndex = Random.Range(0, spawnPoints.Length - 1);

            timer = timer + Time.deltaTime;

            if (timer > respawnTime)
            {
                timer = 0;
                SpawnZombie(zombies[zombieIndex], spawnPoints[spawnIndex]);
            }
        }
    }

    public void SpawnZombie(GameObject zombie, Transform spawn)
    {
        GameObject SpawnedAI = EmeraldAIObjectPool.Spawn(zombie, spawn.position, Quaternion.identity);
        zombieCount++;
    }

    public void ToggleGame()
    {
        gameSarted = !gameSarted;

        //health.ResetPlayer();

        if (gameSarted)
        {
            //register.ResetStats();
            startEndBtnImg.sprite = closeTabSprite;
        }
        else
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("zombie");

            for (var i = 0; i < gameObjects.Length; i++)
            {
                Destroy(gameObjects[i]);
            }

            zombieCount = 0;

            startEndBtnImg.sprite = openTabSprite;
        }
    }

    public void ZombieDie()
    {
        zombieCount--;
    }
}
