﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EmeraldAI.Example;
using Nephasto.VisionFXAsset;
using UnityEngine.UI;

public class Drink : MonoBehaviour
{
    public GameObject player;
    public PlayerHealth playerHealth;
    public EmeraldAIPlayerHealth health;

    public StatsManager sm;
    public GameObject manager;


    public float drunkLevel = 0.0f;

    public int strength = 10;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        health = player.GetComponentInChildren<EmeraldAIPlayerHealth>();

        manager = GameObject.FindGameObjectWithTag("Bartender");
        sm = manager.GetComponent<StatsManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetDrunk()
    {
        health.HealPlayer(strength);

        sm.SetABV(strength);

        Destroy(gameObject);
    }
}
