﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MerchandiseManager : MonoBehaviour
{
    //manager system
    public GameObject bartender;
    public StatsManager register;

    //Drink Objects
    public GameObject lightBeer;
    public GameObject darkBeer;
    public GameObject redWine;
    public GameObject dessertWine;

    //Gun Objects
    public GameObject pistol;
    public GameObject rifle;
    public GameObject shotgun;
    public GameObject autoRifle;

    //Ammo Objects
    public GameObject pistolAmmo;
    public GameObject rifleAmmo;
    public GameObject shotgunAmmo;
    public GameObject autoRifleAmmo;

    public GameObject beerSpawn;
    public GameObject wineSpawn;
    public GameObject gunSpawn;
    public GameObject ammoSpawn;

    // Start is called before the first frame update
    void Start()
    {
        bartender = GameObject.FindGameObjectWithTag("Bartender");
        register = bartender.GetComponent<StatsManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OrderLightBeer(int price)
    {
        if(register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(lightBeer, beerSpawn.transform.position, beerSpawn.transform.rotation);
        }
    }

    public void OrderDarkBeer(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(darkBeer, beerSpawn.transform.position, beerSpawn.transform.rotation);
        }
    }

    public void OrderRedWine(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(redWine, wineSpawn.transform.position, wineSpawn.transform.rotation);
        }
    }

    public void OrderDessertWine(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(dessertWine, wineSpawn.transform.position, wineSpawn.transform.rotation);
        }
    }

    public void OrderPistol(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(pistol, gunSpawn.transform.position, gunSpawn.transform.rotation);
        }
    }

    public void OrderRifle(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(rifle, gunSpawn.transform.position, gunSpawn.transform.rotation);
        }
    }

    public void OrderShotgun(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(shotgun, gunSpawn.transform.position, gunSpawn.transform.rotation);
        }
    }

    public void OrderAutoRifle(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(autoRifle, gunSpawn.transform.position, gunSpawn.transform.rotation);
        }
    }

    public void OrderPistolAmmo(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(pistolAmmo, ammoSpawn.transform.position, ammoSpawn.transform.rotation);
        }
    }

    public void OrderRifleAmmo(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(rifleAmmo, ammoSpawn.transform.position, ammoSpawn.transform.rotation);
        }
    }

    public void OrderShotgunAmmo(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(shotgunAmmo, ammoSpawn.transform.position, ammoSpawn.transform.rotation);
        }
    }

    public void OrderAutoRifleAmmo(int price)
    {
        if (register.currentCash >= price)
        {
            register.SpendMoney(price);
            Instantiate(autoRifleAmmo, ammoSpawn.transform.position, ammoSpawn.transform.rotation);
        }
    }
}
