using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nephasto.VisionFXAsset;

public class SoberUp : MonoBehaviour
{
    public float timer;
    public float abv = 0.0f;
    public StatsManager sm;
    public GameObject manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("Bartender");
        sm = manager.GetComponent<StatsManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //abv = sm.GetABV();
        //if (abv > 7)
        //{
        //    timer = timer + Time.deltaTime;
        //    if (timer > 1.0f)
        //    {
        //        timer = 0;
        //        sm.SetABV(-0.00292f);
        //    }
        //}
        //else
        //{
        //    timer = 0;
        //}
    }

    public void GetSober()
    {
        sm.SetABV(0);
    }
}
